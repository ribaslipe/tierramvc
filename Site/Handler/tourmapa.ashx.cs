﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace NewTierra.Pages.ashx
{
    /// <summary>
    /// Summary description for tourmapa
    /// </summary>
    public class tourmapa : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string fontSize = "13";
            Font font = new Font("'Lato', Helvetica, Arial, sans-serif", float.Parse(fontSize), FontStyle.Bold);
            Bitmap bitmap;
            if (context.Request.QueryString.Get("TYPE") == "0")
            {
                bitmap = new Bitmap(HttpContext.Current.Server.MapPath("~/images/marker-small.png"));
            }
            else
            {
                bitmap = new Bitmap(HttpContext.Current.Server.MapPath("~/images/marker.png"));

            }

            Graphics graphic = Graphics.FromImage(bitmap);

            HatchBrush hatchBrush = new HatchBrush(
                       HatchStyle.Sphere,
                       Color.White,
                       Color.White);

            if (context.Request.QueryString.Get("TYPE") == "0")
            {
                graphic.DrawString(context.Request.QueryString.Get("ID").PadLeft(2).PadRight(2), font, hatchBrush, new PointF(0, 2));
            }
            else
            {
                graphic.DrawString(context.Request.QueryString.Get("ID"), font, hatchBrush, new PointF(5, 12));
            }


            hatchBrush.Dispose();
            graphic.Dispose();

            context.Response.ContentType = "Image/png";
            bitmap.Save(context.Response.OutputStream, ImageFormat.Png);
            bitmap.Dispose();

            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}