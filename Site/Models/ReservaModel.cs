﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Site.Models
{
    public class ReservaModel
    {
        public string CodReferencia { get; set; }
        public string IdHotelXml { get; set; }
        public int IdTarifa { get; set; }
        public int IdCategoriaXml { get; set; }
        public DateTime DtFrom { get; set; }
        public DateTime DtTo { get; set; }
        public int Singles { get; set; }
        public int DoubleMatrimonial { get; set; }
        public int DoubleTwin { get; set; }
        public int Triple { get; set; }
        public decimal Preco { get; set; }
        public PassportModel Passports { get; set; }
        public List<PaxsDetailsModel> PaxsDetails { get; set; }
        public string Moeda { get; set; }
        public long? RatePlanId { get; set; }
       
        public bool OnRequest { get; set; }
    }
}
