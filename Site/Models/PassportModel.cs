﻿namespace Site.Models
{
    public class PassportModel
    {
        public string User { get; set; }
        public string Pass { get; set; }
        public int FlagServico { get; set; }
    }
}
