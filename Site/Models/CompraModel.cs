﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Site.Models
{
    public class CompraModel
    {
        public bool Reservado { get; set; }
        public string Status { get; set; }
        public string Mensagem { get; set; }
        public string CodCompra { get; set; }
        public string CodReferencia { get; set; }
        public string DescricaoReserva { get; set; }
        public string DescricaoCancelamento { get; set; }
        public string DescricaoPgto { get; set; }
        public bool BreakFast { get; set; }
    }
}
