﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class FileFlightsXML
    {
        public IEnumerable<HttpPostedFileBase> Files { get; set; }
        public string qcode { get; set; }
        public int IdQuotGrupo { get; set; }
        public int OptQuote { get; set; }        
    }
}