﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class UsuarioLogin
    {
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", ErrorMessage = "Por favor digite seu email corretamente.")]
        [Required(ErrorMessage = "Por favor digite seu email.")]
        public string email { get; set; }


        //[RegularExpression("^[A-Za-zÀ-Üà-ü0-9\\s]{3,14}$", ErrorMessage = "Por favor digite sua senha corretamente. Não é permitido caracteres especiais, apenas letras e números . Mínimo de 7 até 14 caracteres.")]
        [Required(ErrorMessage = "Senha obrigatória.")]
        public string senha { get; set; }
    }
}