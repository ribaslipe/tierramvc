﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Site.Models
{
    public class HotelsProDestinationsModel
    {
        [JsonProperty("regions")]
        public IList<object> Regions { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("parent")]
        public object Parent { get; set; }
    }
}