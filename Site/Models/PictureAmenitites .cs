﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class PictureAmenities
    {
        public IEnumerable<HttpPostedFileBase> FilesAmenities { get; set; }
        public string S_Amenities_nome { get; set; }
        public int S_GroupAmenities_ID { get; set; }
        public int S_Amenities_id { get; set; }
    }
}