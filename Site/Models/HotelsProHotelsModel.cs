﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Site.Models
{

    public class ThumbnailImages
    {

        [JsonProperty("mid")]
        public string Mid { get; set; }

        [JsonProperty("large")]
        public string Large { get; set; }

        [JsonProperty("small")]
        public string Small { get; set; }
    }

    public class Image
    {

        [JsonProperty("original")]
        public string Original { get; set; }

        [JsonProperty("thumbnail_images")]
        public ThumbnailImages ThumbnailImages { get; set; }

        [JsonProperty("tag")]
        public string Tag { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }
    }

    public class Descriptions
    {

        [JsonProperty("hotel_information")]
        public string HotelInformation { get; set; }
    }

    public class HotelsProHotelsModel
    {

        [JsonProperty("nr_rooms")]
        public int? NrRooms { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("availability_score")]
        public double? AvailabilityScore { get; set; }

        [JsonProperty("master")]
        public object Master { get; set; }

        [JsonProperty("destination")]
        public string Destination { get; set; }

        [JsonProperty("nr_halls")]
        public int? NrHalls { get; set; }

        [JsonProperty("zipcode")]
        public string Zipcode { get; set; }

        [JsonProperty("facilities")]
        public IList<string> Facilities { get; set; }

        [JsonProperty("images")]
        public IList<Image> Images { get; set; }

        [JsonProperty("themes")]
        public IList<string> Themes { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("checkin_from")]
        public string CheckinFrom { get; set; }

        [JsonProperty("stars")]
        public double? Stars { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("hotel_type")]
        public string HotelType { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("regions")]
        public IList<string> Regions { get; set; }

        [JsonProperty("year_built")]
        public int? YearBuilt { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("checkin_to")]
        public string CheckinTo { get; set; }

        [JsonProperty("checkout_from")]
        public string CheckoutFrom { get; set; }

        [JsonProperty("descriptions")]
        public Descriptions Descriptions { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("checkout_to")]
        public string CheckoutTo { get; set; }

        [JsonProperty("nr_restaurants")]
        public int? NrRestaurants { get; set; }

        [JsonProperty("currencycode")]
        public string Currencycode { get; set; }

        [JsonProperty("nr_bars")]
        public int? NrBars { get; set; }
    }

}