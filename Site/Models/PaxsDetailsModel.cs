namespace Site.Models
{
    public class PaxsDetailsModel
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public bool? Owner { get; set; }
        public string Nationality { get; set; }
        public string PassportNumber { get; set; }
        public int FlightNumber { get; set; }

    }
}
