﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Site.Models
{
    public class Categoria
    {

        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public object Description { get; set; }
    }

    public class HotelsProHotelsCategories
    {

        [JsonProperty("Code")]
        public string CodeHotel { get; set; }

        [JsonProperty("Name")]
        public string NomeHotel { get; set; }

        [JsonProperty("Categorias")]
        public IList<Categoria> Categorias { get; set; }
    }

}