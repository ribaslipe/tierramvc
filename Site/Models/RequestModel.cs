﻿using System;
using System.Collections.Generic;

namespace Site.Models
{
    public class RequestModel
    {
        public DateTime DtFrom { get; set; }
        public DateTime DtTo { get; set; }
        public List<PaxsDetailsModel> PaxsDetails { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
        //public string FlagServicos { get; set; }
        public List<HotelRequestModel> Hotel { get; set; }
        public List<CidadeModel> Cidade { get; set; }
        public List<PassportModel> Passports { get; set; }
        public int Qtd { get; set; }
        public string RoomType { get; set; }
        public bool OnRequest { get; set; }
    }
}
