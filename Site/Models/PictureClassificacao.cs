﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class PictureClassificacao
    {
        public IEnumerable<HttpPostedFileBase> FilesClassificacao { get; set; }
        public string classificacao { get; set; }
        public int Categ_id { get; set; }
    }
}