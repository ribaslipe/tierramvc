﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Site.Models
{
    public class PaxsOmniNamesModel
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public bool Primary { get; set; }
    }
}
