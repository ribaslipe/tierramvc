﻿namespace Site.Models
{
    public class CidadeModel
    {
        public int IdTierra { get; set; }
        public int Id { get; set; }
        public int FlagServico { get; set; }
        public string Nome { get; set; }
        public int IdPais { get; set; }
    }
}
