﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Site.Models
{
    public class HotelsProRoomCategoriesModel
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}