﻿using System;
using System.Collections.Generic;

namespace Site.Models
{
    public class HotelModel
    {
        public string Nome { get; set; }
        public string IdHotelXml { get; set; }
        public int IdHotelTierra { get; set; }
        public string Status { get; set; }
        public string Categoria { get; set; }
        public int CategoriaId { get; set; }
        public string TipoHabitacao { get; set; }
        public int IdTipoTarifa { get; set; }
        public string Disponibilidade { get; set; }
        public string Setor { get; set; }
        public DateTime DataLimiteAnulacao { get; set; }
        public decimal Valor { get; set; }
        public decimal ValorSingle { get; set; }
        public decimal ValorSingle2 { get; set; }
        public decimal ValorSingle3 { get; set; }
        public decimal ValorSingle4 { get; set; }
        public decimal ValorSingleRounded { get; set; }
        public decimal ValorSingleRounded2 { get; set; }
        public decimal ValorSingleRounded3 { get; set; }
        public decimal ValorSingleRounded4 { get; set; }
        public decimal ValorDoubleMatrimonial { get; set; }
        public decimal ValorDoubleMatrimonial2 { get; set; }
        public decimal ValorDoubleMatrimonial3 { get; set; }
        public decimal ValorDoubleMatrimonial4 { get; set; }
        public decimal ValorDoubleMatrimonialRounded { get; set; }
        public decimal ValorDoubleMatrimonialRounded2 { get; set; }
        public decimal ValorDoubleMatrimonialRounded3 { get; set; }
        public decimal ValorDoubleMatrimonialRounded4 { get; set; }
        public decimal ValorDoubleTwin { get; set; }
        public decimal ValorDoubleTwinRounded { get; set; }
        public string SingleDisponibilidade { get; set; }
        public string DoubleDisponibilidade { get; set; }
        public string DoubleTwinDisponibilidade { get; set; }
        public string TrippleDisponibilidade { get; set; }
        public decimal ValorQuad { get; set; }
        public decimal ValorQuadRounded { get; set; }
        public decimal ValorFamily { get; set; }
        public decimal ValorFamilyRounded { get; set; }
        public decimal ValorTripple { get; set; }
        public decimal ValorTripple2 { get; set; }
        public decimal ValorTripple3 { get; set; }
        public decimal ValorTripple4 { get; set; }
        public decimal ValorTrippleRounded { get; set; }
        public decimal ValorTrippleRounded2 { get; set; }
        public decimal ValorTrippleRounded3 { get; set; }
        public decimal ValorTrippleRounded4 { get; set; }
        public string Refeicao { get; set; }
        public string Provedor { get; set; }
        public int CidadeId { get; set; }

        public string PoliticaCancelamento { get; set; }
        public string PoliticaCancelamento2 { get; set; }
        public string PoliticaCancelamento3 { get; set; }
        public string PoliticaCancelamento4 { get; set; }
        public string PoliticasGerais { get; set; }

        public string RatePlanName { get; set; }
        public long? RatePlanId { get; set; }
        public string RatePlanName2 { get; set; }
        public long? RatePlanId2 { get; set; }
        public string RatePlanName3 { get; set; }
        public long? RatePlanId3 { get; set; }
        public string RatePlanName4 { get; set; }
        public long? RatePlanId4 { get; set; }

        public bool block { get; set; }
        public bool block2 { get; set; }
        public bool block3 { get; set; }
        public bool block4 { get; set; }

        public DateTime dtPack { get; set; }
        public DateTime dtPack2 { get; set; }
        public DateTime dtPack3 { get; set; }
        public DateTime dtPack4 { get; set; }

        public int? QtdNumPacote { get; set; }
        public int? QtdNumPacote2 { get; set; }
        public int? QtdNumPacote3 { get; set; }
        public int? QtdNumPacote4 { get; set; }
        public int? NomePacote { get; set; }

        public string DisponibilidadeRooms { get; set; }
        public string DisponibilidadeRooms2 { get; set; }
        public string DisponibilidadeRooms3 { get; set; }
        public string DisponibilidadeRooms4 { get; set; }

        public string ConditionsRoomsXml { get; set; }
        public bool BreakfastRoomXml { get; set; }

        public bool? NR { get; set; }
        public bool? NR2 { get; set; }
        public bool? NR3 { get; set; }
        public bool? NR4 { get; set; }

        public string FotoUrl { get; set; }
        public string Moeda { get; set; }
        public bool? OnRequest { get; set; }
        public string QuoteId { get; set; }

        public List<LstRooms> lstrooms { get; set; }
    }

    public class LstRooms
    {
        public decimal ValorSingle { get; set; }
        public decimal ValorSingleRounded { get; set; }
        public decimal ValorDoubleMatrimonial { get; set; }
        public decimal ValorDoubleMatrimonialRounded { get; set; }
        public decimal ValorTripple { get; set; }
        public decimal ValorTrippleRounded { get; set; }

        public string PoliticaCancelamento { get; set; }
        public string RatePlanName { get; set; }
        public long? RatePlanId { get; set; }
        public bool? NR { get; set; }
        public int? QtdNumPacote { get; set; }
        public string DisponibilidadeRooms { get; set; }
        public bool? Refeicao { get; set; }
        public bool block { get; set; }
    }

}
