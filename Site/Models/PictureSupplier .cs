﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class PictureSupplier
    {
        public IEnumerable<HttpPostedFileBase> Files { get; set; }
        public int idSupplier { get; set; }
    }
}