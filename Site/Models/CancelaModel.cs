﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Site.Models
{
    public class CancelaModel
    {
        public string CodCompra { get; set; }
        public string Mensagem { get; set; }
        public bool Status { get; set; }
        public PassportModel Passaporte { get; set; }
    }
}
