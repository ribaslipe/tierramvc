﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Site.Models
{
    public class HotelRequestModel
    {
        public int IdTierra { get; set; }
        public string IdXml { get; set; }
        public int FlagServico { get; set; }
    }
}
