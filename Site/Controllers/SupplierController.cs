﻿using BLL.Utils;
using DAL.Entity;
using DAL.Models;
using DAL.Persistencia;
using Site.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Site.Controllers
{
    [Authorize]
    public class SupplierController : Controller
    {
        // GET: Supplier
        public ActionResult Index()
        {
            StartSupplier();
            return View("Suppliers");
        }

        public Usuarios ObterUser()
        {
            try
            {
                FormsIdentity id = (FormsIdentity)System.Web.HttpContext.Current.User.Identity;
                FormsAuthenticationTicket ticket = id.Ticket;
                return new UsuarioDAL().ObterPorEmail(ticket.Name);
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return null;
            }
        }

        #region Autocompletes

        public ActionResult ObterQuotations(string term, int user_id)
        {
            try
            {
                List<string> quots = new List<string>();

                //if (term.Equals("*"))
                //    quots = new QuotationDAL().ListarTodosString(term, user_id);
                //else
                quots = new QuotationDAL().ListarTodosString(term, user_id).Take(100).ToList();

                return Json(quots, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterQuotations_copia(string term)
        {
            try
            {
                List<string> quots = new List<string>();
                quots = new QuotationDAL().ListarTodosString(term).Take(100).ToList();

                return Json(quots, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterClientes(string term)
        {
            try
            {
                List<string> cls = new ClienteDAL().ListarTodos(term).Select(s => s.Cliente_nome).Distinct().Take(100).ToList();
                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterCidades(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                if (term.Equals("*"))
                    cls = new CidadeDAL().ListarTodos().OrderBy(s => s.CID_nome).Select(s => s.CID_nome).Distinct().ToList();
                else
                    cls = new CidadeDAL().ListarTodos(term).Select(s => s.CID_nome).Distinct().Take(100).ToList();


                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterTransfers(string term, string cidade)
        {
            try
            {
                ServicoDAL ftd = new ServicoDAL();
                List<string> cls = new List<string>();

                if (!cidade.Equals(""))
                    cls = ftd.ListarTodosTrf(term, new CidadeDAL().ObterPorNome(cidade).CID_id).Select(s => s.Servicos_Nome).Distinct().ToList();
                else
                    cls = ftd.ListarTodosTrf(term).Select(s => s.Servicos_Nome).Distinct().Take(200).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterTours(string term, string cidade)
        {
            try
            {
                ServicoDAL ftd = new ServicoDAL();
                List<string> cls = new List<string>();

                if (!cidade.Equals(""))
                    cls = ftd.ListarTodosTor(term, new CidadeDAL().ObterPorNome(cidade).CID_id).Select(s => s.Servicos_Nome).Distinct().ToList();
                else
                    cls = ftd.ListarTodosTor(term).Select(s => s.Servicos_Nome).Distinct().Take(200).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterSuppliers(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new SupplierDAL().ListarTodos(term).Select(s => s.S_nome).OrderBy(s => s).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterHoteis(string term, string cidade)
        {
            try
            {
                SupplierDAL ftd = new SupplierDAL();
                List<string> lst = new List<string>();

                if (!cidade.Equals(""))
                {
                    lst = ftd.FindAllTipo(term, 1, new CidadeDAL().ObterPorNome(cidade).CID_id).OrderBy(s => s.S_nome).Select(s => s.S_nome).ToList();
                }
                else
                {
                    lst = ftd.FindAllTipo(term, 1).OrderBy(s => s.S_nome).Select(s => s.S_nome).ToList();
                }

                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterPaxsGroups(string term, int user_id)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new QuotationGrupoDAL().ListarPaxsGroup(user_id, 0, term).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterSuppliers_Filtros(string term, int idStatus, int idTipoSupplier, int TipoPesquisa, int idCidade)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new SupplierDAL().ListarSupplier_Filtros(term, idStatus, idTipoSupplier, TipoPesquisa, idCidade);

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterCategorias(string term)
        {
            try
            {
                TarifCategoriaDAL t = new TarifCategoriaDAL();
                List<Tarif_Categoria> tc = t.ListarTodos(term);

                List<string> lista = new List<string>();

                foreach (Tarif_Categoria p in tc)
                {
                    lista.Add(p.Tarif_categoria_nome);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        #endregion

        #region Supplier

        public void StartSupplier()
        {

            SupplierDAL sppDal = new SupplierDAL();
            var lstSpp = sppDal.ListarTodos();

            var spp = new Supplier();
            spp.S_id = 0;
            ViewData["supplier"] = spp;
            ViewBag.ResponsavelSupplier = lstSpp;
            ViewBag.MoedaSupplier = new MoedaDAL().ListarTodos();
            ViewBag.TipoSupplier = new TipoSuplsDAL().ListarTodos();
            ViewBag.CategoriaEscSupplier = new CategoriaEscritaDAL().ListarTodos("");
            ViewBag.CategoriaSupplier = new CategoriaDAL().ListarTodos();
            ViewBag.RedeSupplier = new RedeDAL().ListarTodos();
            ViewBag.StatusSupplier = new S_StatusDAL().ListarTodos();

            ViewBag.classificacaoLodge = new ClassificacaoLodgeDAL().ListarTodos();

            PaisDAL pdal = new PaisDAL();
            var pais = pdal.ListarTodos();
            ViewBag.PaisSupplier = pais;

            var paisfiltro = pdal.ListarTodosComSupplier();
            ViewBag.PaisSupplierFiltro = paisfiltro;

            ViewBag.CidadeSupplier = new CidadeDAL().ListarTodos(pais.First().PAIS_id);

            ViewBag.CidadeSupplierFiltro = new CidadeDAL().ListarTodosSupplier(paisfiltro.First().PAIS_id);

            ViewBag.MercadoSppTarifas = new MercadoDAL().ListarTodos();
            ViewBag.BaseTarifariaSppTarifas = new BaseTarifariaDAL().ListarTodos();

            ViewBag.TipoTransporteBases = new TipoTransporteDAL().ListarTodos();
            ViewBag.TipoBases = new TipoBaseDAL().ListarTodos();

            ViewBag.ResponsavelSppTarifas = lstSpp;
        }

        public string ObterCidadesSupplierDrop(int idPais)
        {
            try
            {
                SupplierDAL sDal = new SupplierDAL();
                CidadeDAL cDal = new CidadeDAL();
                var cid = cDal.ListarTodos(idPais);

                string drop = "";
                drop = drop + "<option value=\"0\">Selecione...</option>";
                foreach (var item in cid)
                {
                    drop = drop + string.Format("<option value=\"{0}\">{1}</option>", item.CID_id, item.CID_nome);
                }

                return drop;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ObterCidadesSupplierDropFiltro(int idPais)
        {
            try
            {
                SupplierDAL sDal = new SupplierDAL();
                CidadeDAL cDal = new CidadeDAL();
                var cid = cDal.ListarTodosSupplier(idPais);

                string drop = "";
                drop = drop + "<option value=\"0\">Selecione...</option>";
                foreach (var item in cid)
                {
                    drop = drop + string.Format("<option value=\"{0}\">{1}</option>", item.CID_id, item.CID_nome);
                }

                return drop;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public JsonResult SalvarSupplier(Supplier s)
        {
            try
            {
                SupplierDAL d = new SupplierDAL();
                if (d.VerificaExiste(s.S_nome, 0))
                {
                    return Json(new { success = false, message = "Já existe supplier cadastrado com esse nome nessa cidade, tente outro." }, JsonRequestBehavior.AllowGet);
                }


                if (ModelState.IsValid)
                {

                    if (s.S_qtdpessoas == 0)
                        s.S_qtdpessoas = null;

                    if (s.S_pessoaOUapt == "0")
                        s.S_pessoaOUapt = null;

                    if (s.TIPOSUPL_id == 0)
                        return Json(new { success = false, message = "Favor selecionar tipo do supplier." }, JsonRequestBehavior.AllowGet);


                    if (s.CID_id == 0)
                        return Json(new { success = false, message = "Favor selecionar cidade do supplier." }, JsonRequestBehavior.AllowGet);

                    if (s.Rede_id == 0)
                        s.Rede_id = null;

                    if (s.Categ_id == 0)
                        s.Categ_id = null;

                    if (s.CategoriaEscrita_id == 0)
                        s.CategoriaEscrita_id = null;

                    if (s.ClassificacaoLodge_id == 0)
                        s.ClassificacaoLodge_id = null;

                    d.Salvar(s);

                    if (s.Responsavel_id == 0)
                    {
                        s.Responsavel_id = s.S_id;
                        d.Atualizar(s);
                    }

                    return Json(new { success = true, message = "Cadastrado efetuado com sucesso.", cod = s.S_id }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string messages = string.Join("<br /> ", ModelState.Values
                                         .SelectMany(x => x.Errors)
                                         .Select(x => x.ErrorMessage));

                    throw new Exception(messages);

                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Erro: " + ex.Message, cod = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizarSupplier(Supplier s)
        {
            try
            {
                SupplierDAL d = new SupplierDAL();

                var nomeantigo = d.ObterPorId(s.S_id).S_nome;

                if (ModelState.IsValid)
                {

                    if (s.S_qtdpessoas == 0)
                        s.S_qtdpessoas = null;

                    if (s.S_pessoaOUapt == "0")
                        s.S_pessoaOUapt = null;

                    if (s.TIPOSUPL_id == 0)
                        return Json(new { success = false, message = "Favor selecionar tipo do supplier." }, JsonRequestBehavior.AllowGet);


                    if (s.CID_id == 0)
                        return Json(new { success = false, message = "Favor selecionar cidade do supplier." }, JsonRequestBehavior.AllowGet);

                    if (s.Rede_id == 0)
                        s.Rede_id = null;

                    if (s.Categ_id == 0)
                        s.Categ_id = null;

                    if (s.CategoriaEscrita_id == 0)
                        s.CategoriaEscrita_id = null;

                    if (s.ClassificacaoLodge_id == 0)
                        s.ClassificacaoLodge_id = null;

                    d.Atualizar(s);

                    FileTarifasDAL ftd = new FileTarifasDAL();
                    QuoteTarifasDAL qtd = new QuoteTarifasDAL();
                    SubServicosFileDAL subSdal = new SubServicosFileDAL();
                    QuoteServExtDAL qsed = new QuoteServExtDAL();
                    FileServExtraDAL fsed = new FileServExtraDAL();

                    foreach (var item in subSdal.ListarTodosNome_supplier(nomeantigo))
                    {
                        item.NomeSupplier = s.S_nome;
                        subSdal.Atualiza(item);
                    }

                    foreach (var item in qtd.ListarPorNome(nomeantigo))
                    {
                        item.S_nome = s.S_nome;
                        qtd.AtualizarNome(item);
                    }

                    List<File_Tarifas> filesTar = ftd.ListarTodos(nomeantigo);
                    foreach (File_Tarifas item in filesTar)
                    {
                        File_Tarifas ft = new File_Tarifas();
                        ft.S_nome = s.S_nome;
                        ft.File_Tarifas_id = item.File_Tarifas_id;

                        ftd.AtualizarSNome(ft);
                    }

                    List<File_ServExtra> lstextraq = fsed.ListarTodosSpp(s.S_id);
                    foreach (var item in lstextraq)
                    {
                        item.S_id = s.S_id;
                        item.S_nome = s.S_nome;
                        fsed.AtualizarSupplier(item);
                    }

                    List<Quote_ServExtra> lstextra = qsed.ListarTodosSupplier(s.S_id);
                    foreach (var item in lstextra)
                    {
                        item.S_id = s.S_id;
                        item.S_nome = s.S_nome;
                        qsed.AtualizarSupplier(item);
                    }

                    return Json(new { success = true, message = "Atualizado com sucesso.", cod = s.S_id }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string messages = string.Join("<br /> ", ModelState.Values
                                         .SelectMany(x => x.Errors)
                                         .Select(x => x.ErrorMessage));

                    throw new Exception(messages);

                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Erro: " + ex.Message, cod = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObterSupplier(string supplier)
        {
            Supplier spp = new Supplier();
            try
            {
                SupplierDAL d = new SupplierDAL();
                CidadeDAL cid = new CidadeDAL();

                string[] pars = supplier.Split('°');

                string nomeS = pars[0].Remove(pars[0].Length - 1);
                string nomeCid = pars[1].Substring(1);

                int IdCidade = cid.ObterPorNome(nomeCid).CID_id;

                spp = d.ObterSupplierPorNome(nomeS, IdCidade);

                StartSupplier();
                return PartialView("_supplier_add", spp);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_supplier_add", spp);
            }
        }

        public ActionResult ObterTarifasSupplier(int Sid, int idMercado, int idBaseTarifaria, int idresponsavel)
        {
            List<TarifasSupplierModel> lst = new List<TarifasSupplierModel>();
            try
            {
                PeriodosDAL p = new PeriodosDAL();


                lst = p.ListarTodosFds_model(Sid, idMercado, idBaseTarifaria, idresponsavel == 0 ? Sid : idresponsavel);

                //if (GridTarifasPeriodo.PageCount > 0)
                //{
                //    Session["PeriodoIDTarifas"] = GridTarifasPeriodo.Rows[0].Cells[0].Text;
                //}

                return PartialView("_lst_supplierTarifas", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_supplierTarifas", lst);
            }
        }

        public ActionResult ObterPeriodoSpp_Empty()
        {
            var smp = new S_Mercado_Periodo();
            smp.S_mercado_estacao_id = 0;

            ViewBag.EstacaoPeriodoSpp = new SMercadoEstacaoDAL().ListarTodos();
            ViewBag.MercadoSppPeriodo = new MercadoDAL().ListarTodos();
            ViewBag.BaseTarifariaSppPeriodo = new BaseTarifariaDAL().ListarTodos();
            ViewBag.ResponsavelSppPeriodo = new SupplierDAL().ListarTodos().OrderBy(s => s.S_nome);
            ViewBag.MoedaPeriodoSpp = new MoedaDAL().ListarTodos();
            ViewBag.QtdNoitesPeriodoSpp = new MinNoitesDAL().ListarTodos();
            ViewBag.PromocaoPeriodoSpp = new PromocaoDAL().ListarTodos();

            return PartialView("_periodoSupplier", smp);
        }

        public JsonResult SalvarPeriodo(S_Mercado_Periodo sm)
        {
            try
            {
                if (sm.Pacote_nome != null)
                    if (sm.Pacote_nome.Length > 100)
                    {
                        return Json(new { success = false, message = "Nome do Pacote contém mais de 100 caracteres, o periodo não foi cadastrado." }, JsonRequestBehavior.AllowGet);
                    }


                if (sm.S_merc_remarks != null)
                    if (sm.S_merc_remarks.Length > 100)
                    {
                        return Json(new { success = false, message = "Remarks contém mais de 100 caracteres, o periodo não foi cadastrado." }, JsonRequestBehavior.AllowGet);
                    }



                if(sm.S_mercado_estacao_id == 0)
                {
                    return Json(new { success = false, message = "Favor escolher uma estação." }, JsonRequestBehavior.AllowGet);
                }

                if (ModelState.IsValid)
                {
                    PeriodosDAL d = new PeriodosDAL();

                    if (sm.S_merc_periodo_fdsFrom == "0")
                    { sm.S_merc_periodo_fdsFrom = null; }

                    if (sm.S_merc_periodo_fdsTo == "0")
                    { sm.S_merc_periodo_fdsTo = null; }


                    if (sm.MinimoNoites_id == 0)
                    {
                        sm.MinimoNoites_id = null;
                        sm.S_merc_periodo_minimo = null;
                        sm.S_merc_periodo_mandatorio = null;
                    }

                    if (sm.Promocao_id == 0)
                    { sm.Promocao_id = null; }


                    if (sm.Responsavel_id == 0)
                    { sm.Responsavel_id = sm.S_id; }

                    d.Salvar(sm);

                    return Json(new { success = true, message = "Cadastrado efetuado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string messages = string.Join("<br /> ", ModelState.Values
                                         .SelectMany(x => x.Errors)
                                         .Select(x => x.ErrorMessage));

                    throw new Exception(messages);

                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Erro: " + ex.Message, cod = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizarPeriodo(S_Mercado_Periodo sm)
        {
            try
            {
                if (sm.Pacote_nome != null)
                    if (sm.Pacote_nome.Length > 100)
                    {
                        return Json(new { success = false, message = "Nome do Pacote contém mais de 100 caracteres, o periodo não foi cadastrado." }, JsonRequestBehavior.AllowGet);
                    }


                if (sm.S_merc_remarks != null)
                    if (sm.S_merc_remarks.Length > 100)
                    {
                        return Json(new { success = false, message = "Remarks contém mais de 100 caracteres, o periodo não foi cadastrado." }, JsonRequestBehavior.AllowGet);
                    }


                if (ModelState.IsValid)
                {
                    PeriodosDAL d = new PeriodosDAL();

                    if (sm.S_merc_periodo_fdsFrom == "0")
                    { sm.S_merc_periodo_fdsFrom = null; }

                    if (sm.S_merc_periodo_fdsTo == "0")
                    { sm.S_merc_periodo_fdsTo = null; }


                    if (sm.MinimoNoites_id == 0)
                    {
                        sm.MinimoNoites_id = null;
                        sm.S_merc_periodo_minimo = null;
                        sm.S_merc_periodo_mandatorio = null;
                    }

                    if (sm.Promocao_id == 0)
                    { sm.Promocao_id = null; }


                    if (sm.Responsavel_id == 0)
                    { sm.Responsavel_id = sm.S_id; }


                    if (sm.Pacote_nome == null)
                    { sm.Pacote_nome = null; }
                    else
                    {
                        sm.Pacote_nome = sm.Pacote_nome.Trim();
                        FileTarifasDAL ftd = new FileTarifasDAL();
                        string NomePacAntigo = d.ObterPorIdPeriodo(sm.S_merc_periodo_id).Pacote_nome;
                        ftd.AtualizaTodosNomePac(NomePacAntigo, sm.Pacote_nome.Trim(), Convert.ToDateTime(sm.S_merc_periodo_from), Convert.ToDateTime(sm.S_merc_periodo_to), sm.S_id);
                    }

                    d.Atualizar(sm);

                    return Json(new { success = true, message = "Cadastrado atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string messages = string.Join("<br /> ", ModelState.Values
                                         .SelectMany(x => x.Errors)
                                         .Select(x => x.ErrorMessage));

                    throw new Exception(messages);

                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Erro: " + ex.Message, cod = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirPeriodo(int idPeriodo)
        {
            try
            {
                PeriodosDAL p = new PeriodosDAL();

                S_Mercado_Periodo mp = p.ObterPorIdMercado(idPeriodo);
                p.Excluir(mp);

                return Json(new { success = true, message = "Período excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ObterPeriodo(int idPeriodo)
        {
            try
            {
                var smp = new S_Mercado_Periodo();
                smp.S_mercado_estacao_id = 0;

                ViewBag.EstacaoPeriodoSpp = new SMercadoEstacaoDAL().ListarTodos();
                ViewBag.MercadoSppPeriodo = new MercadoDAL().ListarTodos();
                ViewBag.BaseTarifariaSppPeriodo = new BaseTarifariaDAL().ListarTodos();
                ViewBag.ResponsavelSppPeriodo = new SupplierDAL().ListarTodos().OrderBy(s => s.S_nome);
                ViewBag.MoedaPeriodoSpp = new MoedaDAL().ListarTodos();
                ViewBag.QtdNoitesPeriodoSpp = new MinNoitesDAL().ListarTodos();
                ViewBag.PromocaoPeriodoSpp = new PromocaoDAL().ListarTodos();

                S_Mercado_Periodo mp = new PeriodosDAL().ObterPorIdMercado(idPeriodo);

                return PartialView("_periodoSupplier", mp);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_periodoSupplier", null);
            }
        }

        public ActionResult GetAddTarifasSpp(int Sid, int idMercado, int idBaseTarifaria, int idresponsavel)
        {
            try
            {
                PeriodosDAL p = new PeriodosDAL();
                var lst = p.ListarTodosFds_model(Sid, idMercado, idBaseTarifaria, idresponsavel == 0 ? Sid : idresponsavel);
                ViewData["suppliertarifasedit"] = lst;

                PreencheTelaTarifas();

                return PartialView("_tarifasPeriodo_spp");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_tarifasPeriodo_spp");
            }
        }

        public JsonResult ObterPeriodo_Edit(int idPeriodo)
        {
            try
            {
                MoedaDAL m = new MoedaDAL();
                PeriodosDAL p = new PeriodosDAL();

                S_Mercado_Periodo d = p.ObterPorIdMercado(idPeriodo);

                Moeda n = new Moeda();
                n = m.ObterPorId(Convert.ToInt32(d.Moeda_id));

                //Dictionary<string, string> dadosTarifa = new Dictionary<string, string>();

                var mercado = "Mercado: " + d.Mercado.Mercado_nome;
                var basetarifaria = "Base Tarifária: " + d.BaseTarifaria.BaseTarifaria_nome;
                var moeda = "Moeda: " + d.Moeda.Moeda_sigla;
                var responsavel = "Responsável: " + p.ObterPorResp(Convert.ToInt32(d.Responsavel_id), idPeriodo).S_nome;

                var MinNoiteQtdAddTarifa = "";

                if (d.MinimoNoites_id != null)
                {
                    MinNoiteQtdAddTarifa = "Mínimo Noites: " + Convert.ToString(d.MinimoNoites.MinimoNoites_qtd);

                    if (d.S_merc_periodo_minimo == true)
                        MinNoiteQtdAddTarifa = MinNoiteQtdAddTarifa + " Mínimo";
                    else
                        MinNoiteQtdAddTarifa = MinNoiteQtdAddTarifa + " Mandatório";
                }
                else
                {
                    MinNoiteQtdAddTarifa = "";
                }

                var promocao = "";

                if (d.Promocao_id != null)
                    promocao = "Promoção: " + Convert.ToString(d.Promocao.Promocao_nome);
                else
                    promocao = "";


                PreencheTelaTarifas();

                //Dictionary<string, string> dadosPeriodo = new Dictionary<string, string>();

                //dadosPeriodo.Add("IdMercado", DropMercados.SelectedValue);
                //dadosPeriodo.Add("IdBaseTarifaria", DropBaseTarifarias.SelectedValue);
                //dadosPeriodo.Add("IdResponsavel", ddlResponsavelFiltroTarifas.SelectedValue);

                //Session["IdsPeriodos"] = dadosPeriodo;
                //Session["TitulosTarifas"] = dadosTarifa;

                //PanelTarifasNovo.Visible = true;


                return Json(new
                {
                    success = true,
                    merc = mercado,
                    basetar = basetarifaria,
                    moed = moeda,
                    resp = responsavel,
                    minimNoite = MinNoiteQtdAddTarifa,
                    promo = promocao
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public void PreencheTelaTarifas()
        {
            ViewBag.TipoTarifaSpp = new SMercadoTipoTarifaDAL().ListarTodos();
            ViewBag.CategoriaTarifaSpp = new TarifCategoriaDAL().ListarTodos();
            ViewBag.TipoRoomTarifaSpp = new TipoRoomDAL().ListarTodos();
            ViewBag.StatusTarifa = new SMercadoTarifaStatusDAL().ListarTodos();
            ViewBag.MealSpp = new MealDAL().ListarTodos_SMeal();
        }

        public JsonResult ExcluirSupplier(int IdSupplier)
        {
            try
            {
                if (IdSupplier == 0)
                {
                    return Json(new { success = false, message = "Selecione um supplier para ser excluído." }, JsonRequestBehavior.AllowGet);                   
                }

                if (!ExcluiFotoPasta(IdSupplier))
                {
                    return Json(new { success = false, message = "Não foi possível deletar as fotos na pasta 'Galeria' por favor tente novamente." }, JsonRequestBehavior.AllowGet);                  
                }                

                var lstFTarifas = new FileTarifasDAL().ListarTodos_Spp(IdSupplier);
                if (lstFTarifas.Count > 0)
                {
                    return Json(new { success = false, message = "Esse supplier está vinculado em alguma Quote, não pode ser excluído." }, JsonRequestBehavior.AllowGet);                 
                }
                var lstFTransf = new FileTransfersDAL().ListarTodos_Spp(IdSupplier);
                if (lstFTransf.Count > 0)
                {
                    return Json(new { success = false, message = "Esse supplier está vinculado em alguma Quote, não pode ser excluído." }, JsonRequestBehavior.AllowGet);
                }
                var lstFServExt = new FileServExtraDAL().ListarTodos_Spp(IdSupplier);
                if (lstFTransf.Count > 0)
                {
                    return Json(new { success = false, message = "Esse supplier está vinculado em alguma Quote, não pode ser excluído." }, JsonRequestBehavior.AllowGet);
                }



                SupplierDAL d = new SupplierDAL();

                Supplier s = d.ObterPorId(IdSupplier);
                d.Excluir(s);

                return Json(new { success = true, message = "Supplier excluido com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool ExcluiFotoPasta(int idspp)
        {
            try
            {
                FotoDAL f = new FotoDAL();               

                foreach (S_Image p in f.ListarTodosLista(idspp))
                {
                    string path = "~/Galeria/" + idspp + "/" + p.Image_nome;
                    System.IO.File.Delete(Server.MapPath(path));
                }
                return true;
            }
            catch (Exception ex)
            {                
                return false;
            }
        }

        public string ObterResponsavel(int idspp)
        {
            try
            {
                SupplierDAL sd = new SupplierDAL();

                var lst = sd.ListarResponsaveis(idspp);

                string nomes = "";
                foreach (var item in lst)
                {
                    nomes = nomes + "<b>" + item + "</b><br />";
                }

                return nomes;
            }
            catch (Exception ex)
            {
                return "error";
            }

        }

        #endregion

        #region Tarifas

        //Tarifas
        public ActionResult ObterTarifasCadastradas(int Sid, int IdTarifaBase, int idPeriodo)
        {
            List<TarifasCadastradasPeriodoModel> lst = new List<TarifasCadastradasPeriodoModel>();
            try
            {
                TarifaDAL t = new TarifaDAL();
                SBasesDAL sbd = new SBasesDAL();
                STarifaBaseDAL stbd = new STarifaBaseDAL();

                if (sbd.ObterPorIdSupplier(Sid).Count() != 0)
                {
                    S_Tarifa_Base st = stbd.ObterPorId(IdTarifaBase);
                    if (st != null)
                    {
                        lst = t.ListarTodosOrdenadoComBase_model(idPeriodo, IdTarifaBase);
                    }
                    else
                    {
                        lst = t.ListarTodosOrdenadoComBase_model(idPeriodo, 0);
                    }
                }
                else
                {
                    lst = t.ListarTodosOrdenado_model(idPeriodo);
                }

                return PartialView("_lst_tarifasCadastradas", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_tarifasCadastradas", lst);
            }
        }

        public JsonResult txtStatusTarifa_func(int idperiodo, string txtcomissao, bool dividePac, string txtPorcentagemImposto, string txtPorcentagemTaxa, string ddlTipoCadastro, string txtStatusTarifa)
        {
            Utils u = new Utils();

            var retorno = "";

            if (txtStatusTarifa != "")
            {
                if (u.ValidaNumeroComVirgula(txtStatusTarifa))
                {
                    retorno = txtPorcentagemTaxa_func(txtcomissao, idperiodo, dividePac, txtPorcentagemImposto, txtPorcentagemTaxa, ddlTipoCadastro, txtStatusTarifa);

                    PeriodosDAL perd = new PeriodosDAL();

                    S_Mercado_Periodo smp = perd.ObterPorIdPeriodo(idperiodo);

                    var pars = retorno.Split('°');

                    //if (smp.S_Mercado_Estacao.S_mercado_estacao_pacote == null)
                    //{
                     var vlrcomiss =  txtComissao_func(txtcomissao, ddlTipoCadastro, pars[1], idperiodo, dividePac, txtPorcentagemImposto, txtPorcentagemTaxa, pars[0]);
                    //}


                    //pars[0]

                    return Json(new { success = true, message = "", total = vlrcomiss, tarifa = pars[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "Digitar somente números." }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false, message = "Erro" }, JsonRequestBehavior.AllowGet);
        }

        protected string txtPorcentagemTaxa_func(string txtcomissao, int idperiodo, bool dividePac, string txtPorcentagemImposto, string txtPorcentagemTaxa, string ddlTipoCadastro, string txtStatusTarifa)
        {
            Utils u = new Utils();

            var txtStatusTarifaTotal = "";

            if (!u.ValidaNumerosInteiros(txtPorcentagemImposto) || !u.ValidaNumerosInteiros(txtPorcentagemTaxa))
            {
                return "Digite somente números.";
            }

            if (ddlTipoCadastro.Equals("Valor"))
            {
                if (!txtStatusTarifa.Equals(""))
                {
                    if (!txtPorcentagemTaxa.Equals(""))
                    {
                        double valortarifa = Convert.ToDouble(txtStatusTarifa);
                        double valortotalComTaxa = (valortarifa * Convert.ToDouble(txtPorcentagemTaxa)) / 100;

                        double d = Convert.ToDouble(valortarifa + valortotalComTaxa);
                        txtStatusTarifaTotal = d.ToString("N2");

                        double valortarifa2 = Convert.ToDouble(txtStatusTarifaTotal);
                        double valortotalComImposto = (valortarifa2 * Convert.ToDouble(txtPorcentagemImposto)) / 100;

                        double d2 = Convert.ToDouble(valortarifa2 + valortotalComImposto);
                        txtStatusTarifaTotal = d2.ToString("N2");

                        PeriodosDAL perd = new PeriodosDAL();

                        S_Mercado_Periodo smp = perd.ObterPorIdPeriodo(idperiodo);

                        string div = ConfigurationManager.AppSettings["PkgPerNight"];


                        if (smp.S_Mercado_Estacao.S_mercado_estacao_pacote == true && div.Equals("Y") && dividePac == true)
                        {
                            if (smp.MinimoNoites.MinimoNoites_qtd == 0)
                            {
                                return "Pacote sem quantidade de noites cadastrada para divisão.";
                            }
                            else
                            {
                                txtComissao_func(txtcomissao, ddlTipoCadastro, txtStatusTarifa, idperiodo, dividePac, txtPorcentagemImposto, txtPorcentagemTaxa, txtStatusTarifaTotal);
                                txtStatusTarifaTotal = (Convert.ToDouble(txtStatusTarifaTotal) / smp.MinimoNoites.MinimoNoites_qtd).ToString("N2");
                                txtStatusTarifa = (Convert.ToDouble(txtStatusTarifa) / smp.MinimoNoites.MinimoNoites_qtd).ToString("N2");
                            }
                        }
                    }
                    else
                    {
                        txtStatusTarifa = "";
                    }
                }
                else
                {
                    return "Favor digitar o valor da tarifa antes das taxas.";
                }
            }

            return txtStatusTarifaTotal + "°" + txtStatusTarifa;
        }

        protected string txtComissao_func(string txtComissao, string ddlTipoCadastro, string txtStatusTarifa, int idperiodo, bool dividePac, string txtPorcentagemImposto, string txtPorcentagemTaxa, string txtStatusTarifaTotal)
        {

            Utils u = new Utils();

            if (!u.ValidaNumerosInteiros(txtComissao))
            {
                return "Digite somente números.";
            }


            if (ddlTipoCadastro.Equals("Valor"))
            {
                if (txtStatusTarifa != "")
                {

                    PeriodosDAL perd = new PeriodosDAL();

                    S_Mercado_Periodo smp = perd.ObterPorIdPeriodo(idperiodo);

                    if (smp.S_Mercado_Estacao.S_mercado_estacao_pacote == null)
                    {
                        txtPorcentagemTaxa_func(txtComissao, idperiodo, dividePac, txtPorcentagemImposto, txtPorcentagemTaxa, ddlTipoCadastro, txtStatusTarifa);
                    }

                    double valortarifa = Convert.ToDouble(txtStatusTarifa);
                    double valortotalComComissao = (valortarifa * Convert.ToDouble(txtComissao)) / 100;

                    double d = Convert.ToDouble(Convert.ToDouble(txtStatusTarifaTotal) - valortotalComComissao);
                    txtStatusTarifaTotal = d.ToString("N2");
                }
                else
                {
                    return "Favor digitar o valor da tarifa antes das taxas.";
                }
            }

            return txtStatusTarifaTotal;
        }

        public JsonResult SalvarTarifa(AddTarifaModel model)
        {
            try
            {
                //chkDividePacote.Checked = false;

                //Começa processo cadastro da tarifa
                S_Mercado_Tarifa mt = new S_Mercado_Tarifa();

                //Verifica se é valor ou status tarifa
                if (model.txtStatusTarifa != "" && model.ddlTipoCadastro.Equals("Valor"))
                {
                    mt.S_merc_tarif_tarifa = Convert.ToDecimal(model.txtStatusTarifa);
                    mt.S_merc_tarif_tarifa_total = Convert.ToDecimal(model.txtStatusTarifaTotal);
                    mt.S_merc_tarif_porc_taxa = Convert.ToDecimal(model.txtPorcentagemTaxa);
                    mt.S_merca_tarif_porc_imposto = Convert.ToDecimal(model.txtPorcentagemImposto);
                    mt.S_merca_tarif_porc_comissao = Convert.ToDecimal(model.txtComissao);

                }
                else
                {
                    mt.S_merc_tarif_status_id = model.statusid;
                }


                mt.Tipo_room_id = model.Tipo_room_id;
                mt.Tarif_categoria_id = model.Tarif_categoria_id;
                mt.S_mercado_tipo_tarifa_id = model.S_mercado_tipo_tarifa_id;
                mt.S_merc_periodo_id = model.idperiodo;


                if (model.RdoTarifa == 0)
                {
                    mt.S_merc_tarif_porPessoa = true;
                    mt.S_merc_tarif_porApartamento = null;
                }
                else
                {
                    mt.S_merc_tarif_porApartamento = true;
                    mt.S_merc_tarif_porPessoa = null;
                }

                MercadoTarifaDAL mtd = new MercadoTarifaDAL();
                PeriodosDAL perd = new PeriodosDAL();

                S_Mercado_Periodo smp = perd.ObterPorIdPeriodo(model.idperiodo);

                S_Mercado_Tarifa smtt = mtd.ObterPrimeiroPorPeriodo(model.idperiodo);

                if (model.RdoTarifa == 0)
                {
                    if (smtt != null)
                    {
                        if (smtt.S_merc_tarif_porPessoa != true)
                        {
                            return Json(new { success = false, message = "Esse período só cadastra tarifa por apartamento." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    if (smtt != null)
                    {
                        if (smtt.S_merc_tarif_porApartamento != true)
                        {
                            return Json(new { success = false, message = "Esse período só cadastra tarifa por pessoa." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                SBasesDAL sbd = new SBasesDAL();
                if (sbd.ObterPorIdSupplier(model.Sid).Count() != 0)
                {
                    //Salva tarifa
                    //txtPorcentagemTaxa_TextChanged(sender, e);
                    //txtComissao_TextChanged(sender, e);
                    //MostrarMsg("Cadastro efetuado com sucesso.");
                    //ViewState["ValorPkgPerNight"] = "";

                    mtd.Salvar(mt);
                    SalvarTarifaBase(mt.S_merc_tarif_id, model.IdTarifaBase);

                    return Json(new { success = true, message = "Cadastro efetuado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else if (!mtd.VerificaTarifaCadastrada(mt))
                {

                    //Salva tarifa
                    //txtPorcentagemTaxa_TextChanged(sender, e);
                    //txtComissao_TextChanged(sender, e);
                    //MostrarMsg("Cadastro efetuado com sucesso.");
                    //ViewState["ValorPkgPerNight"] = "";

                    mtd.Salvar(mt);
                    return Json(new { success = true, message = "Cadastro efetuado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "Tarifa já cadastrada, tente outra." }, JsonRequestBehavior.AllowGet);
                }

                //CarregarDadosTarifas(sender, e, "Nome");
                //chkDividePacote.Checked = true;
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizaTarifa(AddTarifaModel model)
        {
            try
            {

                S_Mercado_Tarifa mt = new S_Mercado_Tarifa();

                if (model.txtStatusTarifa != "0" && model.ddlTipoCadastro.Equals("Valor"))
                {
                    mt.S_merc_tarif_tarifa = Convert.ToDecimal(model.txtStatusTarifa);
                    mt.S_merc_tarif_tarifa_total = Convert.ToDecimal(model.txtStatusTarifaTotal);
                    mt.S_merc_tarif_porc_taxa = Convert.ToDecimal(model.txtPorcentagemTaxa);
                    mt.S_merca_tarif_porc_imposto = Convert.ToDecimal(model.txtPorcentagemImposto);
                    mt.S_merca_tarif_porc_comissao = Convert.ToDecimal(model.txtComissao);
                }
                else
                {
                    mt.S_merc_tarif_status_id = model.statusid;
                }


                mt.Tipo_room_id = model.Tipo_room_id;
                mt.Tarif_categoria_id = model.Tarif_categoria_id;
                mt.S_mercado_tipo_tarifa_id = model.S_mercado_tipo_tarifa_id;
                mt.S_merc_periodo_id = model.idperiodo;

                if (model.RdoTarifa == 0)
                {
                    mt.S_merc_tarif_porPessoa = true;
                    mt.S_merc_tarif_porApartamento = null;
                }
                else
                {
                    mt.S_merc_tarif_porApartamento = true;
                    mt.S_merc_tarif_porPessoa = null;
                }

                MercadoTarifaDAL mtd = new MercadoTarifaDAL();

                S_Mercado_Tarifa smtt = mtd.ObterPrimeiroPorPeriodo(model.idperiodo);

                if (model.RdoTarifa == 0)
                {
                    if (smtt.S_merc_tarif_porPessoa != true)
                    {
                        return Json(new { success = false, message = "Esse período só cadastra tarifa por apartamento." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (smtt.S_merc_tarif_porApartamento != true)
                    {
                        return Json(new { success = false, message = "Esse período só cadastra tarifa por pessoa." }, JsonRequestBehavior.AllowGet);
                    }
                }

                TarifaDAL s = new TarifaDAL();

                mt.S_merc_tarif_id = model.TarifaID;

                s.Atualizar(mt);

                return Json(new { success = true, message = "Tarifa atualizada com sucesso." }, JsonRequestBehavior.AllowGet);
                //MostrarMsg("Tarifa atualizada com sucesso.");
                //CarregarDadosTarifas(sender, e, "Nome");
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        protected string SalvarTarifaBase(int IdTarifa, int IdTarifaBase)
        {
            try
            {
                S_Tarifa_Base st = new S_Tarifa_Base();
                STarifaBaseDAL std = new STarifaBaseDAL();

                st.Base_id = IdTarifaBase;
                st.S_merc_tarif_id = IdTarifa;
                std.Salvar(st);

                return "ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public JsonResult GetTarifaSelecionada(int tarifaID)
        {
            try
            {
                TarifaDAL t = new TarifaDAL();
                S_Mercado_Tarifa mt = t.ObterPorId(tarifaID);
                AddTarifaModel m = new AddTarifaModel();

                if (mt.S_merc_tarif_tarifa != null)
                {
                    m.ddlTipoCadastro = "Valor";
                    //MudaTipoCadastro(sender, e);
                    m.txtStatusTarifa = Convert.ToString(mt.S_merc_tarif_tarifa);
                    m.txtStatusTarifaTotal = Convert.ToString(mt.S_merc_tarif_tarifa_total);

                    if (mt.S_merca_tarif_porc_imposto == null)
                    {
                        m.txtPorcentagemImposto = "0";
                    }
                    else
                    {
                        m.txtPorcentagemImposto = Convert.ToString(mt.S_merca_tarif_porc_imposto);
                    }

                    if (mt.S_merc_tarif_porc_taxa == null)
                    {
                        m.txtPorcentagemTaxa = "0";
                    }
                    else
                    {
                        m.txtPorcentagemTaxa = Convert.ToString(mt.S_merc_tarif_porc_taxa);
                    }

                    if (mt.S_merca_tarif_porc_comissao == null)
                    {
                        m.txtComissao = "0";
                    }
                    else
                    {
                        m.txtComissao = Convert.ToString(mt.S_merca_tarif_porc_comissao);
                    }
                }
                else
                {
                    m.S_merc_tarif_status_id = (int)mt.S_merc_tarif_status_id;
                    m.ddlTipoCadastro = "Status";

                    //DropStatusTarif.SelectedValue = Convert.ToString(mt.S_merc_tarif_status_id);
                    //ddlTipoCadastro.SelectedValue = "Status";
                    //MudaTipoCadastro(sender, e);
                    //chkValor_CheckedChanged(sender, e);
                }


                m.Tarif_categoria_id = mt.Tarif_categoria_id;
                m.Tipo_room_id = mt.Tipo_room_id;
                m.S_mercado_tipo_tarifa_id = mt.S_mercado_tipo_tarifa_id;

                m.S_merc_tarif_porApartamento = mt.S_merc_tarif_porApartamento == null ? false : true;
                m.S_merc_tarif_porPessoa = mt.S_merc_tarif_porPessoa == null ? false : true;


                //DropCategoria.SelectedValue = Convert.ToString(mt.Tarif_categoria_id);
                //DropTipoRoom.SelectedValue = Convert.ToString(mt.Tipo_room_id);
                //DropTipoTarifa.SelectedValue = Convert.ToString(mt.S_mercado_tipo_tarifa_id);

                if (mt.S_merc_tarif_porPessoa == true)
                {
                    m.RdoTarifa = 0;
                    //RdoTarifa.SelectedValue = "0";
                }
                else
                {
                    m.RdoTarifa = 1;
                    //RdoTarifa.SelectedValue = "1";
                }

                m.TarifaID = tarifaID;


                return Json(new { success = true, message = "", modelo = m }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult OpenOrdenarTarifas(int idperiodo)
        {
            List<OrdenarTarifasModel> lst = new List<OrdenarTarifasModel>();
            try
            {
                lst = new TarifaDAL().ListarTodosComCategoria_model(idperiodo);
                return PartialView("_ordernarTarifas", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_ordernarTarifas", lst);
            }
        }

        public JsonResult AtualizaOrdemTarifas(int idperiodo, string categs)
        {
            try
            {
                TarifaDAL td = new TarifaDAL();
                MercadoTarifaDAL mtDal = new MercadoTarifaDAL();

                string[] pars = categs.Split('~');

                for (int i = 0; i < pars.Length; i++)
                {
                    var smt = td.ListarTodosListaCategoria(idperiodo, Convert.ToInt32(pars[i].Split('°')[1]));

                    foreach (S_Mercado_Tarifa lista in smt)
                    {
                        S_Mercado_Tarifa sm = new S_Mercado_Tarifa();
                        sm.S_merc_tarif_ordem = Convert.ToInt32(pars[i].Split('°')[0]);
                        sm.S_merc_tarif_id = lista.S_merc_tarif_id;
                        td.AtualizarOrdem(sm);
                    }
                }


                //var smt = td.ListarTodosListaCategoria(idperiodo, Convert.ToInt32(item.Cells[0].Text));

                return Json(new { success = true, message = "Ordem alterada com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirTarifa(int Sid, int S_merc_tarif_id)
        {
            try
            {
                TarifaDAL t = new TarifaDAL();
                STarifaBaseDAL stbd = new STarifaBaseDAL();
                SBasesDAL sbd = new SBasesDAL();

                S_Mercado_Tarifa s = t.ObterPorId(S_merc_tarif_id);

                if (sbd.ObterPorIdSupplier(Sid).Count() != 0)
                {
                    S_Tarifa_Base st = stbd.ObterPorIdTarifa(S_merc_tarif_id);
                    stbd.Excluir(st);
                }

                t.Excluir(s);
                return Json(new { success = true, message = "Tarifa excluída com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PopulaGridBase(int Sid)
        {
            List<S_Bases> sba = new List<S_Bases>();
            try
            {
                SupplierDAL sp = new SupplierDAL();
                SBasesDAL bd = new SBasesDAL();

                sba = bd.ObterPorIdSupplier(Sid);

                //if(sba.Count > 0)
                //{
                //    ViewBag.IdTarifaBase = sba.First().Base_id;
                //}

                //GridBases.SelectedIndex = 0;

                //foreach (GridViewRow row in GridBases.Rows)
                //{
                //    ViewState["IdTarifaBase"] = row.Cells[0].Text;
                //    break;
                //}

                return PartialView("_lst_basesTarifas", sba);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_basesTarifas", sba);
            }
        }

        public JsonResult SelectCategoriaAdd(string txtAddCategoria)
        {
            TarifCategoriaDAL t = new TarifCategoriaDAL();
            Tarif_Categoria tc = t.ObterPorNome(txtAddCategoria);

            return Json(new { success = true, value = tc.Tarif_categoria_id, text = tc.Tarif_categoria_nome }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarCategoriaAddSpp(string txtAddCategoria)
        {
            try
            {
                Tarif_Categoria t = new Tarif_Categoria();
                TarifCategoriaDAL tc = new TarifCategoriaDAL();

                if (txtAddCategoria.Equals(""))
                {
                    return Json(new { success = false, message = "Nenhuma categoria foi escrita ou selecionada." }, JsonRequestBehavior.AllowGet);
                }

                if (!tc.VerificaExistente(txtAddCategoria))
                {
                    t.Tarif_categoria_nome = txtAddCategoria;
                    tc.Salvar(t);

                    //SelectCategoria(sender, e);
                    //txtAddCategoria.Text = "";
                }
                else
                {
                    return Json(new { success = false, message = "Categoria já existente, tente outra." }, JsonRequestBehavior.AllowGet);
                }

                var lst = tc.ListarTodos();

                string categs = "";
                foreach (var item in lst)
                {
                    categs = categs + string.Format("<option value=\"{0}\">{1}</option>", item.Tarif_categoria_id, item.Tarif_categoria_nome);
                }

                return Json(new { success = true, message = "Categoria cadastrada com sucesso.", cgs = categs, value = t.Tarif_categoria_id, text = t.Tarif_categoria_nome }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Meals


        //Meals
        public JsonResult txtPorcentagemTaxaMeal_TextChanged(string txtPorcentagemTaxaMeal, string txtPorcentagemImpostoMeal, string ddlTipoCadastroMeal, string txtValorStatusMeal)
        {
            try
            {
                Utils u = new Utils();
                var txtStatusMealTotal = "";

                if (!u.ValidaNumerosInteiros(txtPorcentagemTaxaMeal) || !u.ValidaNumerosInteiros(txtPorcentagemImpostoMeal))
                {
                    return Json(new { success = false, message = "Digite somente números." }, JsonRequestBehavior.AllowGet);
                }

                if (ddlTipoCadastroMeal.Equals("Valor"))
                {
                    if (!txtValorStatusMeal.Equals(""))
                    {
                        double valortarifa = Convert.ToDouble(txtValorStatusMeal);
                        double valortotalComTaxa = (valortarifa * Convert.ToDouble(txtPorcentagemTaxaMeal)) / 100;

                        double d = Convert.ToDouble(valortarifa + valortotalComTaxa);
                        txtStatusMealTotal = d.ToString("N2");

                        double valortarifa2 = Convert.ToDouble(txtStatusMealTotal);
                        double valortotalComImposto = (valortarifa2 * Convert.ToDouble(txtPorcentagemImpostoMeal)) / 100;

                        double d2 = Convert.ToDouble(valortarifa2 + valortotalComImposto);
                        txtStatusMealTotal = d2.ToString("N2");
                    }
                    else
                    {
                        return Json(new { success = false, message = "Favor digitar o valor da tarifa antes das taxas." }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { success = true, message = "", total = txtStatusMealTotal, tarifa = txtValorStatusMeal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CarregarMeal(int Sid, int IdTarifaBase, int idPeriodo)
        {
            List<MealsCadastradasPeriodoModel> lst = new List<MealsCadastradasPeriodoModel>();
            try
            {
                MealDAL m = new MealDAL();
                SBasesDAL sbd = new SBasesDAL();
                STarifaBaseDAL stbd = new STarifaBaseDAL();

                if (sbd.ObterPorIdSupplier(Sid).Count() != 0)
                {

                    S_Tarifa_Base st = stbd.ObterPorId(IdTarifaBase);

                    if (st != null)
                    {
                        lst = m.ListarTodos_model(idPeriodo, IdTarifaBase);
                    }
                    else
                    {
                        lst = m.ListarTodos_model(idPeriodo, 0);
                    }
                }
                else
                {
                    lst = m.ListarTodos_model(idPeriodo);
                }


                //foreach (GridViewRow item in GridMeal.Rows)
                //{
                //    Label lblAddFile = (Label)item.FindControl("lblAddFile");
                //    CheckBox chkAddFile = (CheckBox)item.FindControl("chkAddFile");

                //    if (lblAddFile.Text != "") chkAddFile.Checked = true; else chkAddFile.Checked = false;
                //}

                return PartialView("_lst_mealsCadastrados", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_mealsCadastrados", lst);
            }
        }

        public JsonResult chkAddFile_Checked(int IdMeal, bool check)
        {
            try
            {

                if (check == true)
                {
                    MealDAL md = new MealDAL();
                    S_Mercado_Meal smm = md.ObterPorId(IdMeal);
                    S_Mercado_Meal mnew = new S_Mercado_Meal();
                    mnew.AddFile = true;
                    mnew.S_merc_meal_id = smm.S_merc_meal_id;
                    md.AtualizarAddFile(mnew);
                }
                else
                {
                    MealDAL md = new MealDAL();
                    S_Mercado_Meal smm = md.ObterPorId(IdMeal);
                    S_Mercado_Meal mnew = new S_Mercado_Meal();
                    mnew.AddFile = null;
                    mnew.S_merc_meal_id = smm.S_merc_meal_id;
                    md.AtualizarAddFile(mnew);
                }


                return Json(new { success = true, message = "Alterado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult chkAddFileUmaVez_Checked(int IdMeal, bool check)
        {
            try
            {

                if (check == true)
                {
                    MealDAL md = new MealDAL();
                    S_Mercado_Meal smm = md.ObterPorId(IdMeal);
                    S_Mercado_Meal mnew = new S_Mercado_Meal();
                    mnew.MealUmaVez = true;
                    mnew.S_merc_meal_id = smm.S_merc_meal_id;
                    md.AtualizarAddFileUmaVez(mnew);
                }
                else
                {
                    MealDAL md = new MealDAL();
                    S_Mercado_Meal smm = md.ObterPorId(IdMeal);
                    S_Mercado_Meal mnew = new S_Mercado_Meal();
                    mnew.MealUmaVez = null;
                    mnew.S_merc_meal_id = smm.S_merc_meal_id;
                    md.AtualizarAddFileUmaVez(mnew);
                }


                return Json(new { success = true, message = "Alterado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetMealSelecionado(int mealID)
        {
            try
            {
                MealDAL t = new MealDAL();

                S_Mercado_Meal mt = t.ObterPorId(mealID);
                AddTarifaModel m = new AddTarifaModel();

                if (mt.S_merc_meal_tarifa != null)
                {
                    m.ddlTipoCadastro = "Valor";
                    //MudaTipoCadastroMeal(sender, e);

                    m.txtStatusTarifa = Convert.ToString(mt.S_merc_meal_tarifa);
                    m.txtStatusTarifaTotal = Convert.ToString(mt.S_merc_meal_tarifa_total);

                    if (mt.S_merca_meal_porc_imposto == null)
                    {
                        m.txtPorcentagemImposto = "0";
                    }
                    else
                    {
                        m.txtPorcentagemImposto = Convert.ToString(mt.S_merca_meal_porc_imposto);
                    }

                    if (mt.S_merc_meal_porc_taxa == null)
                    {
                        m.txtPorcentagemTaxa = "0";
                    }
                    else
                    {
                        m.txtPorcentagemTaxa = Convert.ToString(mt.S_merc_meal_porc_taxa);
                    }
                }
                else
                {
                    m.S_merc_tarif_status_id = Convert.ToInt32(mt.S_merc_tarif_status_id);
                    m.ddlTipoCadastro = "Status";
                }

                //m.S_merc_tarif_porApartamento = mt.S_merc_tarif_porApartamento == null ? false : true;
                //m.S_merc_tarif_porPessoa = mt.S_merc_tarif_porPessoa == null ? false : true;

                //DropMeal.SelectedValue = Convert.ToString(mt.S_meal_id);
                m.IDMeal = Convert.ToInt32(mt.S_meal_id);


                return Json(new { success = true, message = "", modelo = m }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarMeal(AddTarifaModel model)
        {
            try
            {
                S_Mercado_Meal sm = new S_Mercado_Meal();


                if (model.txtStatusTarifa != "" && model.txtStatusTarifa != "0")
                {
                    sm.S_merc_meal_tarifa = Convert.ToDecimal(model.txtStatusTarifa);
                    sm.S_merc_meal_tarifa_total = Convert.ToDecimal(model.txtStatusTarifaTotal);
                    sm.S_merca_meal_porc_imposto = Convert.ToDecimal(model.txtPorcentagemImposto);
                    sm.S_merc_meal_porc_taxa = Convert.ToDecimal(model.txtPorcentagemTaxa);
                }
                else
                {
                    sm.S_merc_tarif_status_id = model.S_merc_tarif_status_id;
                }

                sm.S_meal_id = model.IDMeal;
                sm.S_merc_periodo_id = model.idperiodo;


                MealDAL ml = new MealDAL();

                SBasesDAL sbd = new SBasesDAL();
                if (sbd.ObterPorIdSupplier(model.Sid).Count() != 0)
                {

                    ml.Salvar(sm);

                    SalvarMealBase(sm.S_merc_meal_id, model.IdTarifaBase);

                    return Json(new { success = true, message = "Cadastro efetuado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else if (!ml.VerificaMealCadastrado(sm))
                {
                    ml.Salvar(sm);
                    return Json(new { success = true, message = "Cadastro efetuado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "Meal já cadastrado, tente outro." }, JsonRequestBehavior.AllowGet);
                }


                //return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                //CarregarDadosMeal(sender, e);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        protected void SalvarMealBase(int IdMealTarifa, int IdTarifaBase)
        {
            try
            {
                S_Tarifa_Base st = new S_Tarifa_Base();
                STarifaBaseDAL std = new STarifaBaseDAL();

                st.Base_id = IdTarifaBase;
                st.S_merc_meal_id = IdMealTarifa;
                std.Salvar(st);

            }
            catch (Exception ex)
            {
            }
        }

        public JsonResult AtualizarMeal(AddTarifaModel model)
        {
            try
            {

                S_Mercado_Meal sm = new S_Mercado_Meal();

                sm.S_merc_meal_id = model.TarifaID;

                if (model.txtStatusTarifa != "")
                {
                    sm.S_merc_meal_tarifa = Convert.ToDecimal(model.txtStatusTarifa);
                    sm.S_merc_meal_tarifa_total = Convert.ToDecimal(model.txtStatusTarifaTotal);
                    sm.S_merca_meal_porc_imposto = Convert.ToDecimal(model.txtPorcentagemImposto);
                    sm.S_merc_meal_porc_taxa = Convert.ToDecimal(model.txtPorcentagemTaxa);
                }
                else
                {
                    sm.S_merc_tarif_status_id = model.IDMeal;
                }


                sm.S_meal_id = model.IDMeal;
                sm.S_merc_periodo_id = model.idperiodo;


                MealDAL ml = new MealDAL();

                ml.Atualizar(sm);

                return Json(new { success = true, message = "Meal atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirMeal(int Sid, int S_merc_meal_id)
        {
            try
            {
                MealDAL m = new MealDAL();
                STarifaBaseDAL stbd = new STarifaBaseDAL();
                SBasesDAL sbd = new SBasesDAL();

                S_Mercado_Meal mm = m.ObterPorId(S_merc_meal_id);

                if (sbd.ObterPorIdSupplier(Sid).Count() != 0)
                {
                    S_Tarifa_Base st = stbd.ObterPorIdMeal(S_merc_meal_id);
                    stbd.Excluir(st);
                }

                m.Excluir(mm);
                return Json(new { success = true, message = "Meal excluído com sucesso." }, JsonRequestBehavior.AllowGet);

                //MostrarMsg("Meal excluído com sucesso.");
                //CarregarDadosMeal(sender, e);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public void AtualizaMeal(int IdFileTarifa)
        {
            try
            {
                FileTarifasDAL ftd = new FileTarifasDAL();
                File_Tarifas ft = new File_Tarifas();

                ft.File_Tarifas_id = Convert.ToInt32(IdFileTarifa);
                ft.S_merc_tarif_vendaNet = 0;
                ft.S_merc_tarif_venda = 0;
                ft.S_merc_tarif_total = 0;
                ft.S_merc_tarif_valor = 0;
                ft.markup = 0;
                ft.markupNet = 0;
                ft.desconto = 0;
                ft.descontoNet = 0;

                ftd.Atualizar(ft);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
        }


        #endregion

        #region politica CHD

        //politica CHD
        public ActionResult OpenCHDTarifas(int idperiodo, int IdTarifaCHD)
        {

            try
            {

                string dropCHDs = "";
                for (int i = 0; i < 12; i++)
                {
                    dropCHDs = dropCHDs + string.Format("<option value=\"{0}\">{0}</option>", i);
                }

                string dropDeAteCHDs = "";
                for (int i = 0; i < 19; i++)
                {
                    dropDeAteCHDs = dropDeAteCHDs + string.Format("<option value=\"{0}\">{0}</option>", i);
                }

                ViewBag.dropCHDs = dropCHDs;
                ViewBag.dropDeAteCHDs = dropDeAteCHDs;


                SPoliticaChdDAL spd = new SPoliticaChdDAL();
                List<S_Politica_CHD> lstCHDs = new List<S_Politica_CHD>();
                if (IdTarifaCHD == 0)
                {
                    lstCHDs = spd.ListarTodos();
                }
                else
                {
                    lstCHDs = spd.ListarTodos(IdTarifaCHD);
                }
                ViewBag.ChdList = lstCHDs;


                return PartialView("_politicachdTarifas");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_politicachdTarifas");
            }
        }

        public JsonResult SalvarPoliticaChd(int IdPoliticaCHD, int ddlQtdChds, int ddlIdadeChdsDe, int ddlIdadeChdsAte, int ddlTipoValorPoliticaCHD, string txtValorPoliticaCHD)
        {
            try
            {
                SPoliticaChdDAL spd = new SPoliticaChdDAL();
                S_Politica_CHD ap = new S_Politica_CHD();


                if (IdPoliticaCHD != 0)
                {
                    ap.S_politicaCHD_qtd = Convert.ToInt32(ddlQtdChds);
                    ap.S_politicaCHD_idadeDe = Convert.ToInt32(ddlIdadeChdsDe);
                    ap.S_politicaCHD_idade = Convert.ToInt32(ddlIdadeChdsAte);
                    ap.S_politicaCHD_id = IdPoliticaCHD;

                    switch (Convert.ToInt32(ddlTipoValorPoliticaCHD))
                    {
                        case 0:
                            ap.S_politicaCHD_free = "Free";
                            break;
                        case 1:
                            ap.S_politicaCHD_valor = Convert.ToDecimal(txtValorPoliticaCHD);
                            break;
                        case 2:
                            ap.S_politicaCHD_percent = Convert.ToDecimal(txtValorPoliticaCHD);
                            break;
                    }

                    spd.Atualizar(ap);

                    return Json(new { success = true, message = "Política chd atualizada com sucesso." }, JsonRequestBehavior.AllowGet);

                    //LimpaModalPoliticaCHD();
                }
                else
                {
                    ap.S_politicaCHD_qtd = Convert.ToInt32(ddlQtdChds);
                    ap.S_politicaCHD_idadeDe = Convert.ToInt32(ddlIdadeChdsDe);
                    ap.S_politicaCHD_idade = Convert.ToInt32(ddlIdadeChdsAte);

                    switch (Convert.ToInt32(ddlTipoValorPoliticaCHD))
                    {
                        case 0:
                            ap.S_politicaCHD_free = "Free";
                            break;
                        case 1:
                            ap.S_politicaCHD_valor = Convert.ToDecimal(txtValorPoliticaCHD);
                            break;
                        case 2:
                            ap.S_politicaCHD_percent = Convert.ToDecimal(txtValorPoliticaCHD);
                            break;
                    }

                    spd.Salvar(ap);

                    return Json(new { success = true, message = "Política chd cadastrada com sucesso." }, JsonRequestBehavior.AllowGet);
                    //LimpaModalPoliticaCHD();
                }

                //PopularGridPoliticasCHDCadastro(0);
                //ModalPopupExtenderPoliticaChd.Show();
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPoliticaChdTarifa(int IdPoliticaCHD, int ddlQtdChds, int ddlIdadeChdsDe, int ddlIdadeChdsAte, int ddlTipoValorPoliticaCHD, string txtValorPoliticaCHD, int IdTarifaCHD)
        {
            try
            {
                SPoliticaCHDTarifasDAL spctd = new SPoliticaCHDTarifasDAL();
                SPoliticaChdDAL spd = new SPoliticaChdDAL();
                S_Politica_CHD spc = new S_Politica_CHD();

                MercadoTarifaDAL mtd = new MercadoTarifaDAL();

                decimal valPchd;

                if (txtValorPoliticaCHD.Equals("")) valPchd = 0; else valPchd = Convert.ToDecimal(txtValorPoliticaCHD);

                S_Politica_CHD spolitcachd = spd.VerificaExiste(Convert.ToInt32(ddlQtdChds), Convert.ToInt32(ddlIdadeChdsAte), Convert.ToInt32(ddlIdadeChdsDe), Convert.ToInt32(ddlTipoValorPoliticaCHD), valPchd, valPchd);

                if (spolitcachd != null)
                {
                    S_PoliticaCHDTarifas spct = new S_PoliticaCHDTarifas();

                    spct.S_merc_tarif_id = IdTarifaCHD;
                    spct.S_politicaCHD_id = spolitcachd.S_politicaCHD_id;

                    spctd.Salvar(spct);
                }
                else
                {
                    S_Politica_CHD ap = new S_Politica_CHD();

                    ap.S_politicaCHD_qtd = Convert.ToInt32(ddlQtdChds);
                    ap.S_politicaCHD_idadeDe = Convert.ToInt32(ddlIdadeChdsDe);
                    ap.S_politicaCHD_idade = Convert.ToInt32(ddlIdadeChdsAte);

                    switch (Convert.ToInt32(ddlTipoValorPoliticaCHD))
                    {
                        case 0:
                            ap.S_politicaCHD_free = "Free";
                            break;
                        case 1:
                            ap.S_politicaCHD_valor = Convert.ToDecimal(txtValorPoliticaCHD);
                            break;
                        case 2:
                            ap.S_politicaCHD_percent = Convert.ToDecimal(txtValorPoliticaCHD);
                            break;
                    }

                    spd.Salvar(ap);

                    S_PoliticaCHDTarifas spct = new S_PoliticaCHDTarifas();

                    spct.S_merc_tarif_id = IdTarifaCHD;
                    spct.S_politicaCHD_id = ap.S_politicaCHD_id;

                    spctd.Salvar(spct);
                }

                //CarregarTarifas(sender, e);

                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);

                //MostrarMsg("Registro cadastrado com sucesso.");

                //PopularGridPoliticasCHDCadastro(Convert.ToInt32(ViewState["IdTarifaCHDSelect"]));

                //ModalPopupExtenderPoliticaChd.Show();
            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPoliticaChdTodoPeriodo(string Ids, int idperiodo)
        {
            try
            {

                SPoliticaCHDTarifasDAL spctd = new SPoliticaCHDTarifasDAL();
                SPoliticaChdDAL spd = new SPoliticaChdDAL();
                S_Politica_CHD spc = new S_Politica_CHD();

                MercadoTarifaDAL mtd = new MercadoTarifaDAL();

                int entro = 0;

                string[] pars = Ids.Split('°');

                var lstCHDs = spd.ListarTodos();

                foreach (var item in lstCHDs)
                {
                    int pos = Array.IndexOf(pars, item.S_politicaCHD_id.ToString());

                    if (pos > -1)
                    {
                        entro = 1;
                        foreach (S_Mercado_Tarifa MercadoTarifa in mtd.ListarTodosPorPeriodo(idperiodo))
                        {
                            S_PoliticaCHDTarifas spct = new S_PoliticaCHDTarifas();
                            spct.S_merc_tarif_id = MercadoTarifa.S_merc_tarif_id;
                            spct.S_politicaCHD_id = Convert.ToInt32(pars[pos]);
                            spctd.Salvar(spct);
                        }
                    }
                }

                if (entro == 0)
                {
                    return Json(new { success = false, message = "Selecione ao menos uma políca CHD para adicionar." }, JsonRequestBehavior.AllowGet);
                }

                //CarregarTarifas(sender, e);

                return Json(new { success = true, message = "Tarifas atualizadas com sucesso." }, JsonRequestBehavior.AllowGet);

                //MostrarMsg("Tarifas atualizadas com sucesso.");
                //ModalPopupExtenderPoliticaChd.Show();
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPoliticaChdTodoHotel(string Ids, int idperiodo, int idsupplier)
        {
            try
            {
                SPoliticaCHDTarifasDAL spctd = new SPoliticaCHDTarifasDAL();
                SPoliticaChdDAL spd = new SPoliticaChdDAL();
                S_Politica_CHD spc = new S_Politica_CHD();

                MercadoTarifaDAL mtd = new MercadoTarifaDAL();
                PeriodosDAL pd = new PeriodosDAL();

                int entro = 0;

                string[] pars = Ids.Split('°');

                var lstCHDs = spd.ListarTodos();

                foreach (var item in lstCHDs)
                {
                    int pos = Array.IndexOf(pars, item.S_politicaCHD_id.ToString());
                    if (pos > -1)
                    {
                        foreach (S_Mercado_Periodo MercadoPeriodo in pd.ListarTodosPorSupplier(idsupplier))
                        {

                            foreach (S_Mercado_Tarifa MercadoTarifa in mtd.ListarTodosPorPeriodo(MercadoPeriodo.S_merc_periodo_id))
                            {
                                S_PoliticaCHDTarifas spct = new S_PoliticaCHDTarifas();
                                spct.S_merc_tarif_id = MercadoTarifa.S_merc_tarif_id;
                                spct.S_politicaCHD_id = Convert.ToInt32(pars[pos]);
                                spctd.Salvar(spct);
                            }
                        }
                    }
                }

                if (entro == 0)
                {
                    return Json(new { success = false, message = "Selecione ao menos uma políca CHD para adicionar." }, JsonRequestBehavior.AllowGet);
                }

                //CarregarTarifas(sender, e);

                return Json(new { success = false, message = "Tarifas atualizadas com sucesso." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPolitica_ID(int IdCHD)
        {
            try
            {
                var chd = new SPoliticaChdDAL().ObterPorId(IdCHD);

                SPoliticaCHDModel sp = new SPoliticaCHDModel();
                sp.S_politicaCHD_id = chd.S_politicaCHD_id;
                sp.S_politicaCHD_qtd = chd.S_politicaCHD_qtd;
                sp.S_politicaCHD_idadeDe = chd.S_politicaCHD_idadeDe;
                sp.S_politicaCHD_idade = chd.S_politicaCHD_idade;


                if (chd.S_politicaCHD_free != null)
                    sp.S_politicaCHD_free = "0";
                else if (chd.S_politicaCHD_valor != null)
                    sp.S_politicaCHD_free = "1";
                else if (chd.S_politicaCHD_percent != null)
                    sp.S_politicaCHD_free = "2";


                sp.S_politicaCHD_valor = chd.S_politicaCHD_valor;
                sp.S_politicaCHD_percent = chd.S_politicaCHD_percent;



                return Json(new { success = true, message = "", modelo = sp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region politica TC

        //politica TC
        public ActionResult OpenTcTarifas(int idperiodo, int IdTarifaTC)
        {

            try
            {

                SPoliticaTCDAL spd = new SPoliticaTCDAL();
                List<S_Politica_TC> lstTCs = new List<S_Politica_TC>();
                if (IdTarifaTC == 0)
                {
                    lstTCs = spd.ListarTodos();
                }
                else
                {
                    lstTCs = spd.ListarTodos(IdTarifaTC);
                }
                ViewBag.TcList = lstTCs;


                return PartialView("_politicatcTarifas");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_politicatcTarifas");
            }
        }

        public JsonResult GetPoliticaTC_ID(int IdTC)
        {
            try
            {
                var tc = new SPoliticaTCDAL().ObterPorId(IdTC);

                SPoliticaTcModel sp = new SPoliticaTcModel();
                sp.S_politicaTc_id = tc.S_politicaTC_id;
                sp.S_politicaTc_qtd = tc.S_politicaTC_qtd;


                if (tc.S_politicaTC_free != null)
                    sp.S_politicaTC_free = "0";
                else if (tc.S_politicaTC_valor != null)
                    sp.S_politicaTC_free = "1";
                else if (tc.S_politicaTC_percent != null)
                    sp.S_politicaTC_free = "2";


                sp.S_politicaTc_valor = tc.S_politicaTC_valor;
                sp.S_politicaTc_percent = tc.S_politicaTC_percent;



                return Json(new { success = true, message = "", modelo = sp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPoliticaTc(int IdPoliticaTC, int ddlTcAdd, int ddlTipoValorPoliticaTC, string txtValorPoliticaTC)
        {
            try
            {
                SPoliticaTCDAL spd = new SPoliticaTCDAL();
                S_Politica_TC ap = new S_Politica_TC();


                if (IdPoliticaTC != 0)
                {
                    ap.S_politicaTC_qtd = Convert.ToInt32(ddlTcAdd);
                    ap.S_politicaTC_id = IdPoliticaTC;

                    switch (Convert.ToInt32(ddlTipoValorPoliticaTC))
                    {
                        case 0:
                            ap.S_politicaTC_free = "Free";
                            break;
                        case 1:
                            ap.S_politicaTC_valor = Convert.ToDecimal(txtValorPoliticaTC);
                            break;
                        case 2:
                            ap.S_politicaTC_percent = Convert.ToDecimal(txtValorPoliticaTC);
                            break;
                    }

                    spd.Atualizar(ap);

                    return Json(new { success = true, message = "Política chd atualizada com sucesso." }, JsonRequestBehavior.AllowGet);

                    //LimpaModalPoliticaCHD();
                }
                else
                {
                    ap.S_politicaTC_qtd = Convert.ToInt32(ddlTcAdd);

                    switch (Convert.ToInt32(ddlTipoValorPoliticaTC))
                    {
                        case 0:
                            ap.S_politicaTC_free = "Free";
                            break;
                        case 1:
                            ap.S_politicaTC_valor = Convert.ToDecimal(txtValorPoliticaTC);
                            break;
                        case 2:
                            ap.S_politicaTC_percent = Convert.ToDecimal(txtValorPoliticaTC);
                            break;
                    }

                    spd.Salvar(ap);

                    return Json(new { success = true, message = "Política chd cadastrada com sucesso." }, JsonRequestBehavior.AllowGet);
                    //LimpaModalPoliticaCHD();
                }

                //PopularGridPoliticasCHDCadastro(0);
                //ModalPopupExtenderPoliticaChd.Show();
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPoliticaTcTarifa(int IdPoliticaTC, int ddlQtdTcs, int ddlTipoValorPoliticaTC, string txtValorPoliticaTC, int IdTarifaTC)
        {
            try
            {

                SPoliticaTCTarifasDAL spctd = new SPoliticaTCTarifasDAL();
                SPoliticaTCDAL spd = new SPoliticaTCDAL();
                S_Politica_TC spc = new S_Politica_TC();

                MercadoTarifaDAL mtd = new MercadoTarifaDAL();

                decimal valPchd;

                if (txtValorPoliticaTC.Equals("")) valPchd = 0; else valPchd = Convert.ToDecimal(txtValorPoliticaTC);

                S_Politica_TC spolitcachd = spd.VerificaExiste(Convert.ToInt32(ddlQtdTcs), Convert.ToInt32(ddlTipoValorPoliticaTC), valPchd, valPchd);

                if (spolitcachd != null)
                {
                    S_PoliticaTCTarifas spct = new S_PoliticaTCTarifas();

                    spct.S_merc_tarif_id = IdTarifaTC;
                    spct.S_politicaTC_id = spolitcachd.S_politicaTC_id;

                    spctd.Salvar(spct);
                }
                else
                {
                    S_Politica_TC ap = new S_Politica_TC();

                    ap.S_politicaTC_qtd = Convert.ToInt32(ddlQtdTcs);


                    switch (Convert.ToInt32(ddlTipoValorPoliticaTC))
                    {
                        case 0:
                            ap.S_politicaTC_free = "Free";
                            break;
                        case 1:
                            ap.S_politicaTC_valor = Convert.ToDecimal(txtValorPoliticaTC);
                            break;
                        case 2:
                            ap.S_politicaTC_percent = Convert.ToDecimal(txtValorPoliticaTC);
                            break;
                    }

                    spd.Salvar(ap);

                    S_PoliticaTCTarifas spct = new S_PoliticaTCTarifas();

                    spct.S_merc_tarif_id = IdTarifaTC;
                    spct.S_politicaTC_id = ap.S_politicaTC_id;

                    spctd.Salvar(spct);
                }

                //CarregarTarifas(sender, e);

                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);

                //MostrarMsg("Registro cadastrado com sucesso.");

                //PopularGridPoliticasCHDCadastro(Convert.ToInt32(ViewState["IdTarifaCHDSelect"]));

                //ModalPopupExtenderPoliticaChd.Show();
            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPoliticaTcTodoPeriodo(string Ids, int idperiodo)
        {
            try
            {

                SPoliticaTCTarifasDAL spctd = new SPoliticaTCTarifasDAL();
                SPoliticaTCDAL spd = new SPoliticaTCDAL();
                S_Politica_TC spc = new S_Politica_TC();

                MercadoTarifaDAL mtd = new MercadoTarifaDAL();

                int entro = 0;

                string[] pars = Ids.Split('°');

                var lstCHDs = spd.ListarTodos();

                foreach (var item in lstCHDs)
                {
                    int pos = Array.IndexOf(pars, item.S_politicaTC_id.ToString());

                    if (pos > -1)
                    {
                        entro = 1;
                        foreach (S_Mercado_Tarifa MercadoTarifa in mtd.ListarTodosPorPeriodo(idperiodo))
                        {
                            S_PoliticaTCTarifas spct = new S_PoliticaTCTarifas();
                            spct.S_merc_tarif_id = MercadoTarifa.S_merc_tarif_id;
                            spct.S_politicaTC_id = Convert.ToInt32(pars[pos]);
                            spctd.Salvar(spct);
                        }
                    }
                }

                if (entro == 0)
                {
                    return Json(new { success = false, message = "Selecione ao menos uma política TC para adicionar." }, JsonRequestBehavior.AllowGet);
                }

                //CarregarTarifas(sender, e);

                return Json(new { success = true, message = "Tarifas atualizadas com sucesso." }, JsonRequestBehavior.AllowGet);

                //MostrarMsg("Tarifas atualizadas com sucesso.");
                //ModalPopupExtenderPoliticaChd.Show();
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPoliticaTcTodoHotel(string Ids, int idperiodo, int idsupplier)
        {
            try
            {
                SPoliticaTCTarifasDAL spctd = new SPoliticaTCTarifasDAL();
                SPoliticaTCDAL spd = new SPoliticaTCDAL();
                S_Politica_TC spc = new S_Politica_TC();

                MercadoTarifaDAL mtd = new MercadoTarifaDAL();
                PeriodosDAL pd = new PeriodosDAL();

                int entro = 0;

                string[] pars = Ids.Split('°');

                var lstCHDs = spd.ListarTodos();

                foreach (var item in lstCHDs)
                {
                    int pos = Array.IndexOf(pars, item.S_politicaTC_id.ToString());
                    if (pos > -1)
                    {
                        foreach (S_Mercado_Periodo MercadoPeriodo in pd.ListarTodosPorSupplier(idsupplier))
                        {

                            foreach (S_Mercado_Tarifa MercadoTarifa in mtd.ListarTodosPorPeriodo(MercadoPeriodo.S_merc_periodo_id))
                            {
                                S_PoliticaTCTarifas spct = new S_PoliticaTCTarifas();
                                spct.S_merc_tarif_id = MercadoTarifa.S_merc_tarif_id;
                                spct.S_politicaTC_id = Convert.ToInt32(pars[pos]);
                                spctd.Salvar(spct);
                            }
                        }
                    }
                }

                if (entro == 0)
                {
                    return Json(new { success = false, message = "Selecione ao menos uma política de TC para adicionar." }, JsonRequestBehavior.AllowGet);
                }

                //CarregarTarifas(sender, e);

                return Json(new { success = false, message = "Tarifas atualizadas com sucesso." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Blackout

        public ActionResult ObterPeriodoBlack_Empty(int SupplierID, int MercadoID, int BaseTarifariaID)
        {
            var smp = new S_Mercado_Periodo_Black();
            smp.S_merc_periodo_black_id = 0;

            ViewBag.MercadoBlack = new MercadoDAL().ListarTodos();
            ViewBag.BaseTarifariaBlack = new BaseTarifariaDAL().ListarTodos();

            ViewBag.LstBalcks = new PeriodoBlackDAL().ListarTodos_model(SupplierID, MercadoID, BaseTarifariaID);

            return PartialView("_blackout_add", smp);
        }

        public JsonResult SalvarPeriodoBlack(S_Mercado_Periodo_Black sm)
        {
            try
            {

                Utils u = new Utils();

                if (sm.S_merc_periodo_black_obs != null)
                    if (!u.ValidaLetrasNumeros(sm.S_merc_periodo_black_obs))
                    {
                        return Json(new { success = false, message = "No campo observação, possui caracteres inválidos." }, JsonRequestBehavior.AllowGet);
                    }


                S_Mercado_Periodo_Black smb = new S_Mercado_Periodo_Black();


                smb.S_merc_periodo_black_from = Convert.ToDateTime(sm.S_merc_periodo_black_from);
                smb.S_merc_periodo_black_to = Convert.ToDateTime(sm.S_merc_periodo_black_to);
                smb.S_merc_periodo_black_obs = sm.S_merc_periodo_black_obs;
                smb.S_id = Convert.ToInt32(sm.S_id);
                smb.Mercado_id = Convert.ToInt32(sm.Mercado_id);
                smb.BaseTarifaria_id = Convert.ToInt32(sm.BaseTarifaria_id);

                PeriodoBlackDAL d = new PeriodoBlackDAL();
                d.Salvar(smb);

                //LimpaCampos(this.Controls);
                //lblMsgAddPeriodo.Text = "Cadastro efetuado com sucesso.";
                return Json(new { success = true, message = "Cadastro efetuado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirPeriodoBlack(int IDBlack)
        {
            try
            {
                PeriodoBlackDAL pDal = new PeriodoBlackDAL();

                var pb = pDal.ObterPorId(IDBlack);
                pDal.Excluir(pb);


                return Json(new { success = true, message = "Periodo excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Copiar Tarifas

        public ActionResult ObterCopiarTarifas_Empty(int SupplierID)
        {
            ViewBag.EstacaoCopTarifas = new SMercadoEstacaoDAL().ListarTodos();

            PeriodosDAL p = new PeriodosDAL();
            ViewBag.PeriodosDatas = p.ListarTodos_model(SupplierID);

            return PartialView("_copiarTarifas");
        }

        public JsonResult ObterFDS(int DropPeriodos)
        {
            if (DropPeriodos != 0)
            {
                PeriodosDAL pd = new PeriodosDAL();

                S_Mercado_Periodo p = pd.ObterPorIdPeriodo(DropPeriodos);

                return Json("FDS: " + p.S_merc_periodo_fdsFrom + "-" + p.S_merc_periodo_fdsTo, JsonRequestBehavior.AllowGet);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult CopiarTarifas(bool chkPercent, string txtPercent, int DropPeriodos, int DropEstacao, int Sid, bool chkProvisorio, DateTime from, DateTime to)
        {
            try
            {
                TarifaDAL t = new TarifaDAL();
                MealDAL m = new MealDAL();
                SPoliticaCHDTarifasDAL sctd = new SPoliticaCHDTarifasDAL();
                MercadoTarifaDAL mtd = new MercadoTarifaDAL();
                STarifaBaseDAL starBaseDAL = new STarifaBaseDAL();

                if (chkPercent && txtPercent.Equals(""))
                {
                    return Json(new { success = false, message = "Escolha um valor para porcentagem." }, JsonRequestBehavior.AllowGet);
                }

                int IdPeriodo = SalvarPeriodoCopia(DropEstacao, DropPeriodos, chkProvisorio, Sid, from, to);

                if (IdPeriodo == 0)
                {
                    return Json(new { success = false, message = "Houve um erro, tente novamente com outra data." }, JsonRequestBehavior.AllowGet);
                }

                foreach (S_Mercado_Tarifa p in t.ListarTodosLista(Convert.ToInt32(DropPeriodos)))
                {
                    S_Mercado_Tarifa s = new S_Mercado_Tarifa();

                    if (p.S_merc_tarif_tarifa != null)
                    {

                        if (chkPercent)
                        {
                            double percent = Convert.ToDouble(p.S_merc_tarif_tarifa_total) * Convert.ToDouble(txtPercent) / 100;
                            s.S_merc_tarif_tarifa_total = Convert.ToDecimal(percent) + Convert.ToDecimal(p.S_merc_tarif_tarifa_total);
                        }
                        else
                        {
                            s.S_merc_tarif_tarifa_total = Convert.ToDecimal(p.S_merc_tarif_tarifa_total);
                        }

                        s.S_merc_tarif_tarifa = Convert.ToDecimal(p.S_merc_tarif_tarifa);

                        s.S_merc_tarif_porc_taxa = Convert.ToDecimal(p.S_merc_tarif_porc_taxa);
                        s.S_merca_tarif_porc_imposto = Convert.ToDecimal(p.S_merca_tarif_porc_imposto);
                        s.S_merca_tarif_porc_comissao = Convert.ToDecimal(p.S_merca_tarif_porc_comissao);
                        s.S_merc_tarif_ordem = p.S_merc_tarif_ordem;
                        s.S_merc_tarif_porApartamento = p.S_merc_tarif_porApartamento;
                        s.S_merc_tarif_porPessoa = p.S_merc_tarif_porPessoa;

                    }
                    else
                    {
                        s.S_merc_tarif_status_id = Convert.ToInt32(p.S_merc_tarif_status_id);
                    }

                    s.Tipo_room_id = Convert.ToInt32(p.Tipo_room_id);
                    s.Tarif_categoria_id = Convert.ToInt32(p.Tarif_categoria_id);
                    s.S_mercado_tipo_tarifa_id = Convert.ToInt32(p.S_mercado_tipo_tarifa_id);
                    s.S_merc_periodo_id = IdPeriodo;

                                       
                    mtd.Salvar(s);

                    var stb = starBaseDAL.ObterPorIdTarifa(p.S_merc_tarif_id);
                    if(stb != null)
                    {
                        S_Tarifa_Base sttbb = new S_Tarifa_Base();
                        sttbb.Base_id = stb.Base_id;
                        sttbb.S_merc_tarif_id = s.S_merc_tarif_id;
                        starBaseDAL.Salvar(sttbb);
                    }

                    foreach (S_PoliticaCHDTarifas sc in sctd.ListarTodosLista(p.S_merc_tarif_id))
                    {
                        S_PoliticaCHDTarifas spct = new S_PoliticaCHDTarifas();

                        spct.S_merc_tarif_id = s.S_merc_tarif_id;
                        spct.S_politicaCHD_id = sc.S_politicaCHD_id;

                        sctd.Salvar(spct);
                    }

                }

                foreach (S_Mercado_Meal mm in m.ListarTodosLista(Convert.ToInt32(DropPeriodos)))
                {
                    S_Mercado_Meal s = new S_Mercado_Meal();

                    if (mm.S_merc_meal_tarifa_total != null)
                    {
                        if (chkPercent)
                        {
                            double percent = Convert.ToDouble(mm.S_merc_meal_tarifa_total) * Convert.ToDouble(txtPercent) / 100;
                            s.S_merc_meal_tarifa_total = Convert.ToDecimal(percent) + Convert.ToDecimal(mm.S_merc_meal_tarifa_total);
                        }
                        else
                        {
                            s.S_merc_meal_tarifa_total = Convert.ToDecimal(mm.S_merc_meal_tarifa_total);
                        }
                    }
                    else
                    {
                        s.S_merc_tarif_status_id = mm.S_merc_tarif_status_id;
                    }

                    s.S_meal_id = mm.S_meal_id;
                    s.S_merc_periodo_id = IdPeriodo;
                    s.S_merc_meal_tarifa = mm.S_merc_meal_tarifa;

                    s.S_merca_meal_porc_imposto = mm.S_merca_meal_porc_imposto;
                    s.S_merc_meal_porc_taxa = mm.S_merc_meal_porc_taxa;

                    m.Salvar(s);


                    var stb = starBaseDAL.ObterPorIdMeal(mm.S_merc_meal_id);
                    if (stb != null)
                    {
                        S_Tarifa_Base sttbb = new S_Tarifa_Base();
                        sttbb.Base_id = stb.Base_id;
                        sttbb.S_merc_meal_id = s.S_merc_meal_id;
                        starBaseDAL.Salvar(sttbb);
                    }

                }

                return Json(new { success = true, message = "Tarifas copiadas com sucesso." }, JsonRequestBehavior.AllowGet);


                //from.Text = "";
                //to.Text = "";
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public int SalvarPeriodoCopia(int DropEstacao, int DropPeriodos, bool chkProvisorio, int Sid, DateTime from, DateTime to)
        {
            PeriodosDAL p = new PeriodosDAL();
            S_Mercado_Periodo a = p.ObterPorIdMercado(Convert.ToInt32(DropPeriodos));
            S_Mercado_Periodo mp = new S_Mercado_Periodo();
            DescritivoPacoteDAL dpd = new DescritivoPacoteDAL();

            try
            {
                if (p.VerificaExiste(Convert.ToDateTime(from), Convert.ToDateTime(to), Sid))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "call me", "alert('Já existia cadastro com essas datas(From, To) neste hotel.')", true);
                    //return 0;
                }

                mp.S_merc_periodo_from = Convert.ToDateTime(from);
                mp.S_merc_periodo_to = Convert.ToDateTime(to);
                mp.S_id = a.S_id;
                mp.S_mercado_estacao_id = Convert.ToInt32(DropEstacao);
                mp.BaseTarifaria_id = a.BaseTarifaria_id;
                mp.Mercado_id = a.Mercado_id;
                mp.Moeda_id = a.Moeda_id;
                mp.Responsavel_id = a.Responsavel_id;
                mp.S_merc_periodo_fdsFrom = a.S_merc_periodo_fdsFrom;
                mp.S_merc_periodo_fdsTo = a.S_merc_periodo_fdsTo;
                mp.MinimoNoites_id = a.MinimoNoites_id;
                mp.S_merc_periodo_minimo = a.S_merc_periodo_minimo;
                mp.S_merc_periodo_mandatorio = a.S_merc_periodo_mandatorio;
                mp.Promocao_id = a.Promocao_id;
                mp.S_merc_remarks = a.S_merc_remarks;
                mp.Provisorio = chkProvisorio;
                mp.Pacote_nome = a.Pacote_nome;

                p.Salvar(mp);

                Descritivo_Pacote dp = dpd.ObterPorIdPeriodo(a.S_merc_periodo_id);

                if (dp != null)
                {
                    Descritivo_Pacote descp = new Descritivo_Pacote();

                    descp.Descritivo_descr0 = dp.Descritivo_descr0;
                    descp.Descritivo_descr1 = dp.Descritivo_descr1;
                    descp.Descritivo_descr2 = dp.Descritivo_descr2;
                    descp.Descritivo_descr3 = dp.Descritivo_descr3;
                    descp.Descritivo_descr4 = dp.Descritivo_descr4;
                    descp.Descritivo_descr5 = dp.Descritivo_descr5;
                    descp.Descritivo_descr6 = dp.Descritivo_descr6;
                    descp.Descritivo_descr7 = dp.Descritivo_descr7;
                    descp.Descritivo_descr8 = dp.Descritivo_descr8;
                    descp.Descritivo_descr9 = dp.Descritivo_descr9;
                    descp.S_merc_periodo_id = mp.S_merc_periodo_id;

                    dpd.Salvar(descp);
                }

                return Convert.ToInt32(mp.S_merc_periodo_id);

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion

        #region contato

        public ActionResult ObterContato_Empty(int SupplierID)
        {
            ContatoDAL c = new ContatoDAL();
            ViewBag.lstContatos = c.ListarTodos(SupplierID);
            return PartialView("_contatoSpp");
        }

        public JsonResult SalvarContato(S_Contato model)
        {
            try
            {
                S_Contato c = new S_Contato();

                c.SCONT_nome = model.SCONT_nome;
                c.SCONT_telefone = model.SCONT_telefone;
                c.SCONT_position = model.SCONT_position;
                c.SCONT_email = model.SCONT_email;
                c.S_id = model.S_id;

                ContatoDAL s = new ContatoDAL();

                s.Salvar(c);

                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);

                //MostrarMsg("Registro cadastrado com sucesso.");
                //LimpaCampos(this.Controls);
                //CarregarDados();
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirContato(int idContato)
        {
            try
            {

                ContatoDAL s = new ContatoDAL();
                S_Contato c = s.ObterPorId(idContato);
                s.Excluir(c);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region cond pag

        public ActionResult ObterCondPag_Empty(int SupplierID)
        {
            CondPagDAL cpd = new CondPagDAL();

            ViewBag.lstCondPag = cpd.ListarTodos(SupplierID);

            return PartialView("_condPagamento");
        }

        public JsonResult SalvarCond(S_Cond_Pagamento model)
        {
            try
            {
                S_Cond_Pagamento scp = new S_Cond_Pagamento();

                scp.CondPag_fontecambio = model.CondPag_fontecambio;
                scp.CondPag_tipocambio = model.CondPag_tipocambio;
                scp.CondPag_datatroca = model.CondPag_datatroca;
                scp.CondPag_prazofaturamento = model.CondPag_prazofaturamento;
                scp.CondPag_banco = model.CondPag_banco;
                scp.CondPag_agencia = model.CondPag_agencia;
                scp.CondPag_conta = model.CondPag_conta;
                scp.S_id = model.S_id;

                CondPagDAL cp = new CondPagDAL();

                cp.Salvar(scp);

                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirCondPag(int idCondPag)
        {
            try
            {
                CondPagDAL cpd = new CondPagDAL();

                S_Cond_Pagamento s = cpd.ObterPorId(idCondPag);
                cpd.Excluir(s);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Categorias Obs

        public ActionResult ObterCategoriasObs_Empty(int SupplierID)
        {
            TarifCategoriaDAL td = new TarifCategoriaDAL();

            ViewBag.lstCategoriasObs = td.ListarTodos_model(SupplierID);

            return PartialView("_categoriasObs");
        }

        public JsonResult GridDesc_Select(int myid, int Sid)
        {
            CategDescricaoDAL cdd = new CategDescricaoDAL();
            CategDescricao cd = cdd.ObterPorSupCateg(Sid, myid);

            if (cd != null)
            {
                return Json(new { success = true, message = "", txtRemarkDescCateg = cd.CategDesc, IdCategDescr = cd.CategDesc_id }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, message = "", txtRemarkDescCateg = "", IdCategDescr = "" }, JsonRequestBehavior.AllowGet);
            }

            //ViewState["IdCateg"] = myid;
            //lblMsgCadDesc.Text = "";
        }

        public JsonResult SalvarDescCateg(int Sid, int IdCateg, string txtRemarkDescCateg)
        {
            try
            {

                CategDescricaoDAL cdd = new CategDescricaoDAL();
                CategDescricao cd = new CategDescricao();

                CategDescricao cd2 = cdd.ObterPorSupCateg(Sid, IdCateg);

                if (cd2 == null)
                {
                    cd.CategDesc = txtRemarkDescCateg;
                    cd.S_id = Sid;
                    cd.Tarif_categoria_id = IdCateg;

                    cdd.Salvar(cd);
                    return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    cd.CategDesc = txtRemarkDescCateg;
                    cd.S_id = Sid;
                    cd.Tarif_categoria_id = IdCateg;
                    cd.CategDesc_id = cd2.CategDesc_id;

                    cdd.Atualizar(cd);
                    return Json(new { success = true, message = "Registro atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Descr Pacote

        public ActionResult AbrirPopUpAddDescrPacote(int idPeriodo)
        {
            Descritivo_Pacote dp = new Descritivo_Pacote();
            try
            {
                DescritivoPacoteDAL dpd = new DescritivoPacoteDAL();
                PeriodosDAL pd = new PeriodosDAL();
                S_Mercado_Periodo p = pd.ObterPorIdPeriodo(idPeriodo);

                int qtdNoites = 0;

                if (p.MinimoNoites != null)
                    qtdNoites = p.MinimoNoites.MinimoNoites_qtd;


                ViewBag.MinimoNoites = qtdNoites;

                if (dpd.VerificaPeriodo(idPeriodo))
                {
                    dp = dpd.ObterPorIdPeriodo(idPeriodo);
                }

                return PartialView("_descricaoPacote", dp);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_descricaoPacote", dp);
            }

        }

        public class AddDescricaoPeriodo
        {
            public string descr { get; set; }
            public int pos { get; set; }
            public int idPeriodo { get; set; }

            public bool breakfast { get; set; }
            public bool lunch { get; set; }
            public bool boxlunch { get; set; }
            public bool dinner { get; set; }
        }

        public JsonResult SalvarDescrPacote(AddDescricaoPeriodo model)
        {
            try
            {

                DescritivoPacoteDAL dpd = new DescritivoPacoteDAL();
                DescritivoOptionsDAL dOpDal = new DescritivoOptionsDAL();

                Descritivo_Pacote descrPacote = dpd.ObterPorIdPeriodo(model.idPeriodo);

                var conte = HttpUtility.UrlDecode(model.descr, System.Text.Encoding.Default);

                if (descrPacote == null && model.pos == 0)
                {
                    Descritivo_Pacote dp = new Descritivo_Pacote();
                    dp.Descritivo_descr0 = conte;
                    dp.S_merc_periodo_id = model.idPeriodo;
                    dpd.Salvar(dp);
                }
                else
                {
                    descrPacote.Descritivo_descr0 = conte;
                    dpd.Atualizar(descrPacote);
                }

                DescritivoOptions_Pacote descrOptions0 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 0);
                SalvarDescritivo_Options(descrOptions0, model, descrPacote.Descritivo_id, 0);

                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizarDescrPacote(AddDescricaoPeriodo model)
        {
            try
            {

                DescritivoPacoteDAL dpd = new DescritivoPacoteDAL();
                DescritivoOptionsDAL dOpDal = new DescritivoOptionsDAL();


                Descritivo_Pacote descrPacote = dpd.ObterPorIdPeriodo(model.idPeriodo);

                
                

                var conte = HttpUtility.UrlDecode(model.descr, System.Text.Encoding.Default);

                if (descrPacote != null)
                {

                    switch (model.pos)
                    {
                        case 0:

                            DescritivoOptions_Pacote descrOptions0 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 0);
                            SalvarDescritivo_Options(descrOptions0, model, descrPacote.Descritivo_id, 0);

                            descrPacote.Descritivo_descr0 = conte;
                            break;
                        case 1:

                            DescritivoOptions_Pacote descrOptions1 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 1);
                            SalvarDescritivo_Options(descrOptions1, model, descrPacote.Descritivo_id, 1);

                            descrPacote.Descritivo_descr1 = conte;
                            break;
                        case 2:
                            DescritivoOptions_Pacote descrOptions2 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 2);
                            SalvarDescritivo_Options(descrOptions2, model, descrPacote.Descritivo_id, 2);

                            descrPacote.Descritivo_descr2 = conte;
                            break;
                        case 3:
                            DescritivoOptions_Pacote descrOptions3 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 3);
                            SalvarDescritivo_Options(descrOptions3, model, descrPacote.Descritivo_id, 3);

                            descrPacote.Descritivo_descr3 = conte;
                            break;
                        case 4:
                            DescritivoOptions_Pacote descrOptions4 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 4);
                            SalvarDescritivo_Options(descrOptions4, model, descrPacote.Descritivo_id, 4);

                            descrPacote.Descritivo_descr4 = conte;
                            break;
                        case 5:
                            DescritivoOptions_Pacote descrOptions5 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 5);
                            SalvarDescritivo_Options(descrOptions5, model, descrPacote.Descritivo_id, 5);
                            descrPacote.Descritivo_descr5 = conte;
                            break;
                        case 6:
                            DescritivoOptions_Pacote descrOptions6 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 6);
                            SalvarDescritivo_Options(descrOptions6, model, descrPacote.Descritivo_id, 6);
                            descrPacote.Descritivo_descr6 = conte;
                            break;
                        case 7:
                            DescritivoOptions_Pacote descrOptions7 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 7);
                            SalvarDescritivo_Options(descrOptions7, model, descrPacote.Descritivo_id, 7);
                            descrPacote.Descritivo_descr7 = conte;
                            break;
                        case 8:
                            DescritivoOptions_Pacote descrOptions8 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 8);
                            SalvarDescritivo_Options(descrOptions8, model, descrPacote.Descritivo_id, 8);
                            descrPacote.Descritivo_descr8 = conte;
                            break;
                        case 9:
                            DescritivoOptions_Pacote descrOptions9 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 9);
                            SalvarDescritivo_Options(descrOptions9, model, descrPacote.Descritivo_id, 9);
                            descrPacote.Descritivo_descr9 = conte;
                            break;
                        case 10:
                            DescritivoOptions_Pacote descrOptions10 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 10);
                            SalvarDescritivo_Options(descrOptions10, model, descrPacote.Descritivo_id, 10);
                            descrPacote.Descritivo_descr10 = conte;
                            break;
                        case 11:
                            DescritivoOptions_Pacote descrOptions11 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 11);
                            SalvarDescritivo_Options(descrOptions11, model, descrPacote.Descritivo_id, 11);
                            descrPacote.Descritivo_descr11 = conte;
                            break;
                        case 12:
                            DescritivoOptions_Pacote descrOptions12 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 12);
                            SalvarDescritivo_Options(descrOptions12, model, descrPacote.Descritivo_id, 12);
                            descrPacote.Descritivo_descr12 = conte;
                            break;
                        case 13:
                            DescritivoOptions_Pacote descrOptions13 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 13);
                            SalvarDescritivo_Options(descrOptions13, model, descrPacote.Descritivo_id, 13);
                            descrPacote.Descritivo_descr13 = conte;
                            break;
                        case 14:
                            DescritivoOptions_Pacote descrOptions14 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 14);
                            SalvarDescritivo_Options(descrOptions14, model, descrPacote.Descritivo_id, 14);
                            descrPacote.Descritivo_descr14 = conte;
                            break;
                        case 15:
                            DescritivoOptions_Pacote descrOptions15 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 15);
                            SalvarDescritivo_Options(descrOptions15, model, descrPacote.Descritivo_id, 15);
                            descrPacote.Descritivo_descr15 = conte;
                            break;
                        case 16:
                            DescritivoOptions_Pacote descrOptions16 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 16);
                            SalvarDescritivo_Options(descrOptions16, model, descrPacote.Descritivo_id, 16);
                            descrPacote.Descritivo_descr16 = conte;
                            break;
                        case 17:
                            DescritivoOptions_Pacote descrOptions17 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 17);
                            SalvarDescritivo_Options(descrOptions17, model, descrPacote.Descritivo_id, 17);
                            descrPacote.Descritivo_descr17 = conte;
                            break;
                        case 18:
                            DescritivoOptions_Pacote descrOptions18 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 18);
                            SalvarDescritivo_Options(descrOptions18, model, descrPacote.Descritivo_id, 18);
                            descrPacote.Descritivo_descr18 = conte;
                            break;
                        case 19:
                            DescritivoOptions_Pacote descrOptions19 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 19);
                            SalvarDescritivo_Options(descrOptions19, model, descrPacote.Descritivo_id, 19);
                            descrPacote.Descritivo_descr19 = conte;
                            break;
                        case 20:
                            DescritivoOptions_Pacote descrOptions20 = dOpDal.ObterPorDecritivo(descrPacote.Descritivo_id, 20);
                            SalvarDescritivo_Options(descrOptions20, model, descrPacote.Descritivo_id, 20);
                            descrPacote.Descritivo_descr20 = conte;
                            break;
                    }

                    dpd.Atualizar(descrPacote);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        protected bool SalvarDescritivo_Options(DescritivoOptions_Pacote descrOptions, AddDescricaoPeriodo model, int Descritivo_id, int number)
        {
            try
            {
                DescritivoOptionsDAL dOpDal = new DescritivoOptionsDAL();

                if (descrOptions == null)
                {
                    descrOptions = new DescritivoOptions_Pacote();
                    descrOptions.DescritivoOptions_number = number;
                    descrOptions.DescritivoOptions_breakfast = model.breakfast ? "Breakfast" : null;
                    descrOptions.DescritivoOptions_boxlunch = model.boxlunch ? "Box Lunch" : null;
                    descrOptions.DescritivoOptions_dinner = model.dinner ? "Dinner" : null;
                    descrOptions.DescritivoOptions_lunch = model.lunch ? "Lunch" : null;
                    descrOptions.Descritivo_id = Descritivo_id;
                    dOpDal.Salvar(descrOptions);
                }
                else
                {
                    descrOptions.DescritivoOptions_breakfast = model.breakfast ? "Breakfast" : null;
                    descrOptions.DescritivoOptions_boxlunch = model.boxlunch ? "Box Lunch" : null;
                    descrOptions.DescritivoOptions_dinner = model.dinner ? "Dinner" : null;
                    descrOptions.DescritivoOptions_lunch = model.lunch ? "Lunch" : null;
                    descrOptions.Descritivo_id = Descritivo_id;
                    dOpDal.Atualizar(descrOptions);
                }


                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region provisorio

        public JsonResult TarifaProvisoria(int idPeriodo)
        {
            try
            {
                PeriodosDAL pd = new PeriodosDAL();

                S_Mercado_Periodo smp = pd.ObterPorIdPeriodo(idPeriodo);

                if (smp.Provisorio == null)
                    smp.Provisorio = true;
                else
                    smp.Provisorio = !smp.Provisorio;

                pd.Atualizar(smp);

                return Json(new { success = true, message = "Alterado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Descricoes

        public ActionResult Buscadescricao(int Sid)
        {
            SupplierDAL sd = new SupplierDAL();
            Model con = new Model();
            S_Descricao sdesc;
            try
            {
                sdesc = con.S_Descricao.Where(ds => ds.S_id == Sid).SingleOrDefault();
                if (sdesc != null)
                {
                    return PartialView("_descricoesSpp", sdesc);
                }
                else
                {
                    sdesc = new S_Descricao();
                    sdesc.Desc_id = 0;
                    return PartialView("_descricoesSpp", sdesc);
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_descricoesSpp", sdesc = new S_Descricao());
            }
        }

        public JsonResult CadastrarDescricao(S_Descricao model)
        {
            try
            {
                Utils util = new Utils();
                S_Descricao d = new S_Descricao();

                if (model.Desc_nRooms != null)
                {
                    if (!util.ValidaNumerosInteiros(model.Desc_nRooms.ToString()))
                    {
                        return Json(new { success = false, message = "Número de rooms digitar somente valor inteiro." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        d.Desc_nRooms = model.Desc_nRooms;
                    }
                }
                else
                {
                    d.Desc_nRooms = null;
                }

                var pequena = HttpUtility.UrlDecode(model.Desc_short, System.Text.Encoding.Default);
                var longa = HttpUtility.UrlDecode(model.Desc_long, System.Text.Encoding.Default);

                d.Desc_short = pequena;
                d.Desc_long = longa;
                d.Desc_remarks = model.Desc_remarks;
                d.Desc_cliente = model.Desc_cliente;
                d.Desc_voucher = model.Desc_voucher;
                d.Desc_HoraCheckIn = model.Desc_HoraCheckIn;
                d.Desc_HoraCheckOut = model.Desc_HoraCheckOut;
                d.Desc_LnkAdvisor = model.Desc_LnkAdvisor;
                d.S_id = model.S_id;

                DescricaoDAL s = new DescricaoDAL();

                if (model.Desc_id != 0)
                {
                    d.Desc_id = model.Desc_id;
                    s.Atualizar(d);
                }
                else
                {
                    s.Salvar(d);
                }

                return Json(new { success = true, message = "Descrição cadastrada com sucesso.", descrID = d.Desc_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Amenities

        public ActionResult PopulaAmenities(int Sid)
        {
            try
            {
                AmenitiesDAL amDal = new AmenitiesDAL();

                ViewBag.AmenitiesActivities = amDal.ListarPorGrupo(2);
                ViewBag.AmenitiesChildrenFacilities = amDal.ListarPorGrupo(6);
                ViewBag.AmenitiesGeneral = amDal.ListarPorGrupo(3);
                ViewBag.AmenitiesGeneralInfor = amDal.ListarPorGrupo(1);

                ViewBag.AmenitiesInternet = amDal.ListarPorGrupo(4);
                ViewBag.AmenitiesServices = amDal.ListarPorGrupo(5);
                ViewBag.AmenitiesSuite = amDal.ListarPorGrupo(7);

                ViewBag.lstIdsSpp = amDal.ListarPorSupplier_SAmenities(Sid);

                return PartialView("_amenities");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_amenities");
            }
        }

        public JsonResult SalvaAmenities(int Sid, int IDamenities)
        {
            try
            {
                AmenitiesDAL aDAL = new AmenitiesDAL();
                var aSpp = aDAL.ObterPorId_Spp(Sid, IDamenities);

                if (aSpp != null)
                {
                    aDAL.ExcluirSppAmenities(aSpp);
                }
                else
                {
                    aSpp = new S_SupplierAmenities();
                    aSpp.S_SupplierAmenities_Supplier_ID = Sid;
                    aSpp.S_SupplierAmenities_Amenities_ID = IDamenities;

                    aDAL.SalvarAppAmenities(aSpp);
                }

                return Json(new { success = true, message = "Alterado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Bases

        public JsonResult SalvarBases(int type, int index, string txtDe, string txtAte, int IdSupplier, int ddlMercadoFiltro_Base, int ddlBaseTarifariaFiltro_Base, int ddlMoedaFiltro_Base, int ddlTipoTransporte, int ddlTipoBase_Base)
        {
            try
            {

                if (type == 0)
                {
                    S_Bases b = new S_Bases();
                    SBasesDAL d = new SBasesDAL();

                    b.Base_index = index;
                    b.Base_de = Convert.ToInt32(txtDe);
                    b.Base_ate = Convert.ToInt32(txtAte);
                    b.S_id = IdSupplier;
                    b.Mercado_id = Convert.ToInt32(ddlMercadoFiltro_Base);
                    b.BaseTarifaria_id = Convert.ToInt32(ddlBaseTarifariaFiltro_Base);
                    b.Moeda_id = Convert.ToInt32(ddlMoedaFiltro_Base);
                    b.IdTipoTrans = Convert.ToInt32(ddlTipoTransporte);
                    b.TipoBase_id = Convert.ToInt32(ddlTipoBase_Base);


                    d.Salvar(b);
                }
                else
                {
                    SBasesDAL d = new SBasesDAL();

                    for (int i = Convert.ToInt32(txtDe); i < Convert.ToInt32(txtAte) + 1; i++)
                    {
                        S_Bases b = new S_Bases();                       

                        b.Base_de = Convert.ToInt32(i);
                        b.Base_ate = Convert.ToInt32(i);

                        b.Base_index = index;
                        b.S_id = IdSupplier;
                        b.Mercado_id = Convert.ToInt32(ddlMercadoFiltro_Base);
                        b.BaseTarifaria_id = Convert.ToInt32(ddlBaseTarifariaFiltro_Base);
                        b.Moeda_id = Convert.ToInt32(ddlMoedaFiltro_Base);
                        b.IdTipoTrans = Convert.ToInt32(ddlTipoTransporte);
                        b.TipoBase_id = Convert.ToInt32(ddlTipoBase_Base);

                        d.Salvar(b);
                    }
                }


                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);

                //MostrarMsg("Registro cadastrado com sucesso.");
                //txtDe.Text = "";
                //txtAte.Text = "";
                //PopulaGridBases(index);
                //PopulaGridResumoBases(Convert.ToInt32(Session["Sid"]));
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult PopulaGridResumoBases(int IdSupplier)
        {
            List<S_Bases> lst = new List<S_Bases>();
            try
            {
                SBasesDAL sbd = new SBasesDAL();
                lst = sbd.ObterPorIdSupplier(IdSupplier);

                return PartialView("_lst_bases", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_bases", lst);
            }
        }

        public ActionResult PopulaGridBasesIndex(int IdSupplier, int index, int ddlMercadoFiltro_Base, int ddlBaseTarifariaFiltro_Base, int ddlMoedaFiltro_Base, int ddlTipoTransporte, int ddlTipoBase_Base)
        {
            List<S_Bases> lst = new List<S_Bases>();
            try
            {
                SBasesDAL b = new SBasesDAL();
                lst = b.ListarTodosPorSupplier(IdSupplier, index, ddlMercadoFiltro_Base, ddlBaseTarifariaFiltro_Base, ddlMoedaFiltro_Base, ddlTipoTransporte, ddlTipoBase_Base);

                return PartialView("_lst_basesIndex", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_basesIndex", lst);
            }
        }

        public JsonResult ExcluirBases(int IdSupplier, int index, int ddlMercadoFiltro_Base, int ddlBaseTarifariaFiltro_Base, int ddlMoedaFiltro_Base, int ddlTipoTransporte, int ddlTipoBase_Base)
        {
            try
            {
                SBasesDAL b = new SBasesDAL();

                foreach (S_Bases sb in b.ListarTodosPorSupplier(IdSupplier, index, Convert.ToInt32(ddlMercadoFiltro_Base), Convert.ToInt32(ddlBaseTarifariaFiltro_Base), Convert.ToInt32(ddlMoedaFiltro_Base), Convert.ToInt32(ddlTipoTransporte), Convert.ToInt32(ddlTipoBase_Base)))
                {
                    b.Excluir(sb);
                }
               
                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Fotos

        [HttpPost]
        public ActionResult Upload(PictureSupplier picture)
        {
            CriarPastaSupplier(picture.idSupplier);

            FotoDAL fDal = new FotoDAL();

            foreach (var file in picture.Files)
            {
                if (file.ContentLength > 0)
                {
                    string fotoString = Convert.ToString(Guid.NewGuid().ToString("n") + ".jpg");
                    file.SaveAs(Server.MapPath("~/Galeria/") + picture.idSupplier + "/" + fotoString);

                    SalvarFoto(fotoString, picture.idSupplier);
                }
            }

            FotoDAL f = new FotoDAL();
            f.ImagemPrincipal(picture.idSupplier);

            ViewBag.message = "Fotos enviadas com sucesso.";

            var sp = new SupplierDAL().ObterPorId(picture.idSupplier);
            ViewBag.retornofoto = sp.S_nome;
            ViewBag.retornofotocidade = sp.Cidade.CID_nome;

            StartSupplier();
            return View("~/Views/Supplier/Suppliers.cshtml");
            //return View("Suppliers");            
        }

        protected string CriarPastaSupplier(int idSupplier)
        {
            string path = ("~/Galeria/" + idSupplier);
            try
            {
                if (!Directory.Exists(Server.MapPath(path)))
                { Directory.CreateDirectory(Server.MapPath(path)); }

                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public bool SalvarFoto(string ImgNome, int idSupplier)
        {
            S_Image s = new S_Image();

            try
            {
                s.Image_capa = "N";
                s.Image_nome = ImgNome;
                s.Image_hupload = Convert.ToString(DateTime.Now);
                s.S_id = idSupplier;

                FotoDAL f = new FotoDAL();

                f.Salvar(s);
                return true;
            }
            catch (Exception ex)
            {                
                return false;
            }
        }

        public ActionResult CarregarFotos(int idSupplier)
        {
            List<S_Image> lst = new List<S_Image>();
            try
            {
                FotoDAL f = new FotoDAL();

                lst = f.ListarTodosLista(idSupplier);

                return PartialView("_fotos", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_fotos", lst);
            }
        }

        public JsonResult ExcluirFoto(int Image_id, int idSupplier)
        {
            FotoDAL f = new FotoDAL();

            try
            {               

                S_Image si = f.ObterPorId(Convert.ToInt32(Image_id));

                if (!ExcluiFotoPasta(si.Image_nome, idSupplier))
                {
                    throw new Exception("Erro: Não foi possível deletar a foto da pasta galeria");
                }

                f.Excluir(si);

                return Json(new { success = true, message = "Foto excluída com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }            
        }

        public bool ExcluiFotoPasta(string foto, int idSupplier)
        {
            try
            {
                string path = "~/Galeria/" + idSupplier + "/" + foto;
                System.IO.File.Delete(Server.MapPath(path));
                return true;
            }
            catch (Exception ex)
            {                
                return false;
            }
        }

        public JsonResult CapaFoto(int idSupplier, int idImg)
        {
            try
            {
                FotoDAL f = new FotoDAL();

                f.AddImagemPrincipal(idSupplier, idImg);

                return Json(new { success = true, message = "Foto principal atualizada com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }
}