﻿
using BLL.Utils;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DAL.DataSet;
using DAL.Entity;
using DAL.Models;
using DAL.Persistencia;
using Site.Models;
using Site.Reports;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Site.Controllers
{

    [Authorize]
    public class ServicosController : Controller
    {
        // GET: Servicos
        public ActionResult Index()
        {
            StartServico();
            return View("Servicos");
        }

        protected void StartServico()
        {

            ViewBag.TipoServicoFiltro = new ServicoTipoDAL().ListarTodos();
            ViewBag.CategoriaFiltro = new ServicoTipoCategDAL().ListarTodos();
            ViewBag.StatusFiltro = new S_StatusDAL().ListarTodos();

            PaisDAL pdal = new PaisDAL();
            var pais = pdal.ListarTodos();
            ViewBag.PaisCadastro = pais;

            var paisfiltro = pdal.ListarTodosComServicos();
            ViewBag.PaisCadastroFiltro = paisfiltro;


            ViewBag.CidadeCadastro = new CidadeDAL().ListarTodos(pais.First().PAIS_id);

            ViewBag.CidadeFiltro = new CidadeDAL().ListarTodosServico(paisfiltro.First().PAIS_id);

            ViewBag.MoedaReport = new MoedaDAL().ListarTodos();
            ViewBag.Mercadoreport = new MercadoDAL().ListarTodos();
            ViewBag.BaseTarifariaReport = new BaseTarifariaDAL().ListarTodos();

            ViewBag.TipoBases = new TipoBaseDAL().ListarTodos().OrderByDescending(s => s.TipoBase_id);

            var sv = new S_Servicos();
            sv.Servicos_Id = 0;
            ViewData["servico"] = sv;

        }

        #region AutoCompletes

        public ActionResult ObterServicos_Filtros(string term, int idCidade, int idTipoServico, int idTipoCategServico, int idStatus)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new ServicoDAL().ListarServicos_Filtro(term, idCidade, idTipoServico, idTipoCategServico, idStatus);

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterSuppliers(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new SupplierDAL().ListarTodos(term).Select(s => s.S_nome).OrderBy(s => s).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        #endregion

        #region Servicos

        public string refreshctegoriasfiltro(int idTipoServico)
        {
            try
            {
                ServicoTipoCategDAL stcDal = new ServicoTipoCategDAL();

                var t = stcDal.ListarTodos(idTipoServico);

                string drop = "";
                drop = drop + "<option value=\"0\">Selecione...</option>";
                foreach (var item in t)
                {
                    drop = drop + string.Format("<option value=\"{0}\">{1}</option>", item.Tipo_categ_id, item.Tipo_categ_nome);
                }

                return drop;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ObterCidadesServicoDrop(int idPais)
        {
            try
            {
                SupplierDAL sDal = new SupplierDAL();
                CidadeDAL cDal = new CidadeDAL();
                var cid = cDal.ListarTodos(idPais);

                string drop = "";
                drop = drop + "<option value=\"0\">Selecione...</option>";
                foreach (var item in cid)
                {
                    drop = drop + string.Format("<option value=\"{0}\">{1}</option>", item.CID_id, item.CID_nome);
                }

                return drop;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ObterCidadesServicoDropFiltro(int idPais)
        {
            try
            {
                SupplierDAL sDal = new SupplierDAL();
                CidadeDAL cDal = new CidadeDAL();
                var cid = cDal.ListarTodosServico(idPais);

                string drop = "";
                drop = drop + "<option value=\"0\">Selecione...</option>";
                foreach (var item in cid)
                {
                    drop = drop + string.Format("<option value=\"{0}\">{1}</option>", item.CID_id, item.CID_nome);
                }

                return drop;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public ActionResult ObterServico(string servico)
        {
            S_Servicos spp = new S_Servicos();
            try
            {
                ServicoDAL s = new ServicoDAL();
                CidadeDAL cid = new CidadeDAL();

                string[] pars = servico.Split('°');

                string nomeS = pars[0].Remove(pars[0].Length - 1);
                string nomeCid = pars[1].Substring(1);

                int IdCidade = cid.ObterPorNome(nomeCid).CID_id;

                spp = s.ObterPorNome(nomeS, IdCidade);

                StartServico();
                return PartialView("_servico_add", spp);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_servico_add", spp);
            }
        }

        public JsonResult SalvarServico(S_Servicos serv)
        {
            ServicoDAL d = new ServicoDAL();

            if (serv.Servicos_Id == 0)
            {
                if (d.VerificaExiste(serv.Servicos_Nome, 0))
                {
                    return Json(new { success = false, message = "Já existe serviço cadastrado com esse nome nessa cidade, tente outro." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (d.VerificaExiste(serv.Servicos_Nome, serv.Servicos_Id))
                {
                    return Json(new { success = false, message = "Já existe serviço cadastrado com esse nome nessa cidade, tente outro." }, JsonRequestBehavior.AllowGet);
                }
            }

            if (serv.Cid_Id == 0)
            {
                return Json(new { success = false, message = "Já existe serviço cadastrado com esse nome nessa cidade, tente outro." }, JsonRequestBehavior.AllowGet);
            }

            if (serv.Tipo_Id == 0)
            {
                return Json(new { success = false, message = "Favor selecione um tipo de serviço." }, JsonRequestBehavior.AllowGet);
            }

            try
            {

                if (serv.Servicos_Id == 0)
                {
                    #region comment                   
                    //S_Servicos s = new S_Servicos();

                    //s.Servicos_Nome = serv.Servicos_Nome;
                    //s.STATUS_id = serv.STATUS_id;
                    //s.Tipo_Id = serv.Tipo_Id;
                    //s.Cid_Id = serv.Cid_Id;
                    //s.Pais_Id = serv.Pais_Id;

                    //s.Recommended = serv.Recommended;
                    //s.SIB = serv.SIB;

                    //if (serv.Tipo_Id == 2)
                    //    s.Servicos_transfer = "S";
                    //else
                    //    s.Servicos_transfer = null;


                    //if (serv.Tipo_categ_id == 0)
                    //    s.Tipo_categ_id = null;
                    //else
                    //    s.Tipo_categ_id = serv.Tipo_categ_id;

                    //s.Servicos_descr = serv.Servicos_descr;
                    //s.Servicos_descrCurt = serv.Servicos_descrCurt;
                    //s.Servicos_descrRemarks = serv.Servicos_descrRemarks;
                    //s.Servicos_descrVoucher = serv.Servicos_descrVoucher;
                    //s.Servicos_descrCliente = serv.Servicos_descrCliente;                    
                    #endregion

                    serv.Servicos_descr = HttpUtility.UrlDecode(serv.Servicos_descr, System.Text.Encoding.Default);
                    serv.Servicos_descrCurt = HttpUtility.UrlDecode(serv.Servicos_descrCurt, System.Text.Encoding.Default);

                    if (serv.Tipo_Id == 2)
                        serv.Servicos_transfer = "S";

                    d.Salvar(serv);

                    return Json(new { success = true, message = "Serviço cadastrado com sucesso.", cod = serv.Servicos_Id }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var nomeantigo = d.ObterPorId(serv.Servicos_Id).Servicos_Nome;

                    #region comment     
                    //S_Servicos s = new S_Servicos();

                    //s.Servicos_Id = serv.Servicos_Id;
                    //s.Servicos_Nome = serv.Servicos_Nome;
                    //s.STATUS_id = serv.STATUS_id;
                    //s.Tipo_Id = serv.Tipo_Id;
                    //s.Cid_Id = serv.Cid_Id;
                    //s.Pais_Id = serv.Pais_Id;
                    //s.Recommended = serv.Recommended;
                    //s.SIB = serv.SIB;


                    //if (serv.Tipo_Id == 2)                    
                    //    s.Servicos_transfer = "S";                    
                    //else                    
                    //    s.Servicos_transfer = null;

                    //if (serv.Tipo_categ_id == 0)
                    //    s.Tipo_categ_id = null;
                    //else
                    //    s.Tipo_categ_id = serv.Tipo_categ_id;

                    //s.Servicos_descr = serv.Servicos_descr;
                    //s.Servicos_descrCurt = serv.Servicos_descrCurt;
                    //s.Servicos_descrRemarks = serv.Servicos_descrRemarks;
                    //s.Servicos_descrVoucher = serv.Servicos_descrVoucher;
                    //s.Servicos_descrCliente = serv.Servicos_descrCliente;
                    #endregion

                    if (serv.Tipo_Id == 2)
                        serv.Servicos_transfer = "S";

                    serv.Servicos_descr = HttpUtility.UrlDecode(serv.Servicos_descr, System.Text.Encoding.Default);
                    serv.Servicos_descrCurt = HttpUtility.UrlDecode(serv.Servicos_descrCurt, System.Text.Encoding.Default);

                    serv.Servicos_descrRemarks = HttpUtility.UrlDecode(serv.Servicos_descrRemarks, System.Text.Encoding.Default);
                    serv.Servicos_descrCliente = HttpUtility.UrlDecode(serv.Servicos_descrCliente, System.Text.Encoding.Default);
                    serv.Servicos_descrVoucher = HttpUtility.UrlDecode(serv.Servicos_descrVoucher, System.Text.Encoding.Default);


                    d.Atualizar(serv);

                    FileTransfersDAL ftd = new FileTransfersDAL();
                    QuoteTransDAL qtrd = new QuoteTransDAL();

                    List<File_Transfers> filesTrf = ftd.ListarTodos(nomeantigo);
                    foreach (File_Transfers item in filesTrf)
                    {
                        File_Transfers ft = new File_Transfers();
                        ft.Transf_nome = serv.Servicos_Nome;
                        ft.Trf_CID_id = serv.Cid_Id;
                        ft.File_Transf_id = item.File_Transf_id;

                        ftd.AtualizarNomeCidade(ft);
                    }


                    foreach (var item in qtrd.ListarTodos(nomeantigo))
                    {
                        item.Transf_nome = serv.Servicos_Nome;
                        item.Trf_CID_id = serv.Cid_Id;
                        qtrd.AtualizarNomeCidade(item);
                    }


                    return Json(new { success = true, message = "Serviço atualizado com sucesso.", cod = serv.Servicos_Id }, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirServico(int idServico)
        {
            try
            {
                if (!ExcluiFotoPasta(idServico))
                {
                    return Json(new { success = false, message = "Não foi possível deletar as fotos na pasta 'Galeria/Servicos' por favor tente novamente." }, JsonRequestBehavior.AllowGet);
                }

                ServicoDAL s = new ServicoDAL();

                S_Servicos si = s.ObterPorId(idServico);

                List<File_Transfers> filesTrf = new FileTransfersDAL().ListarTodos(si.Servicos_Nome);
                var filesQtr = new QuoteTransDAL().ListarTodos(si.Servicos_Nome);

                if (filesTrf.Count > 0)
                {
                    return Json(new { success = false, message = "Esse serviço não pode ser excluído." }, JsonRequestBehavior.AllowGet);
                }
                if (filesQtr.Count > 0)
                {
                    return Json(new { success = false, message = "Esse serviço não pode ser excluído." }, JsonRequestBehavior.AllowGet);
                }

                s.Excluir(si);

                return Json(new { success = true, message = "Serviço excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool ExcluiFotoPasta(int idServico)
        {
            try
            {
                FotoServicoDAL f = new FotoServicoDAL();

                foreach (Servicos_Imagem p in f.ListarTodosLista(idServico))
                {
                    string path = "~/Galeria/Servicos/" + idServico + "/" + p.Serv_img_nome;
                    System.IO.File.Delete(Server.MapPath(path));
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Report

        protected Decimal arrendodar(Decimal valor)
        {
            decimal valorArr = 0;
            try
            {
                string[] pars = Convert.ToDecimal(valor).ToString().Split(',');
                double valorArredondado = 0;
                ArredondaDAL arrd = new ArredondaDAL();
                if (pars.Count() > 1)
                {
                    Arredonda arr;

                    if (pars[1].Length == 1)
                    {
                        string val = pars[1] + 0;
                        arr = arrd.ObterArr(Convert.ToInt32(val.Substring(0, 2)));
                    }
                    else
                    {
                        arr = arrd.ObterArr(Convert.ToInt32(pars[1].Substring(0, 2)));
                    }

                    if (arr.Arredonda_recebe < 1)
                    {
                        valorArr = Convert.ToDecimal(pars[0]) + Convert.ToDecimal(arr.Arredonda_recebe);
                        valorArredondado = Convert.ToDouble(valorArr);
                    }
                    else
                    {
                        valorArr = Convert.ToDecimal(pars[0]) + Convert.ToDecimal(arr.Arredonda_recebe);
                        valorArredondado = Convert.ToDouble(valorArr);
                    }
                }

                return valorArr;

            }
            catch (Exception ex)
            {
            }

            return valorArr;
        }

        protected string NuloZero(string valor)
        {
            if (valor == null)
            {
                return "0";
            }
            else
            {
                return valor;
            }
        }

        public class ModeloCidade
        {

            private string idCidade;
            public string IdCidade
            {
                get { return idCidade; }
                set { idCidade = value; }
            }

            private string sigla;
            public string Sigla
            {
                get { return sigla; }
                set { sigla = value; }
            }


        }
        public class ModeloServico
        {

            private string idServico;
            public string IdServico
            {
                get { return idServico; }
                set { idServico = value; }
            }

            private string idSupplier;
            public string IdSupplier
            {
                get { return idSupplier; }
                set { idSupplier = value; }
            }


        }
        string pathSaveReport(string nome)
        {
            return Server.MapPath("~/ReportsGerados/" + nome);
        }
        string urlOpenReport(string nome)
        {
            string urltoda = HttpContext.Request.Url.AbsoluteUri;
            string host = urltoda.Replace("Servicos", "°");
            return host.Split('°')[0] + "ReportsGerados/" + nome;
        }

        public ActionResult btnGerarRelatorioServicoTarifario(DateTime txtfromReportTarifario,
                                                              DateTime txttoReportTarifario,
                                                              string txtMarkUpRelatorioTarifario,
                                                              int ddlPaisReportTarifario,
                                                              int ddlCidadeRerpotTarifario,
                                                              int ddlBaseTarifariaReportTarifario,
                                                              int ddlMercadoReportTarifario,
                                                              int ddlCambioReportTarifario,
                                                              int ddlFormatoReportServicos)
        {


            DSServicos ds = new DSServicos();

            DataTable dt = ds.Tables["Servicos"];

            var str = ConfigurationManager.ConnectionStrings["Conexao"].ToString();
            SqlConnection Con = new SqlConnection(str);
            string sql;
            string sql2;
            string sql3;
            string sql4;
            string sql5;
            string sql6;
            string sql7;
            string sql8;

            bool cabecalho = true;
            bool sib = true;
            bool naoSib = true;
            bool adiciona = false;

            var fr = string.Format("{0:dd/MM/yyyy}", txtfromReportTarifario);
            var to = string.Format("{0:dd/MM/yyyy}", txttoReportTarifario);

            DateTime dataFrom = DateTime.ParseExact(fr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dataTo = DateTime.ParseExact(to, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            decimal markUp = 0;
            if (!txtMarkUpRelatorioTarifario.Equals(""))
            {
                markUp = Convert.ToDecimal(txtMarkUpRelatorioTarifario);
            }

            decimal cambio = 1;
            decimal total;
            string totalString;
            Utils utils = new Utils();

            Con.Open();

            sql = "SELECT DISTINCT(S_Servicos.Cid_Id), Cidade.CID_nome, Cidade.CID_uf ";
            sql = sql + "FROM Monta_Servico_Valores  ";
            sql = sql + "INNER JOIN Monta_Servico ON Monta_Servico_Valores.IdMServico = Monta_Servico.IdMServico ";
            sql = sql + "INNER JOIN S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id ";
            sql = sql + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico ";
            sql = sql + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";
            sql = sql + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";
            sql = sql + "LEFT OUTER JOIN S_Mercado_Tarifa_Status ON S_Mercado_Tarifa_Status.S_mercado_tarifa_status_id = Monta_Servico_Valores.S_mercado_tarifa_status_id ";
            sql = sql + "WHERE S_Servicos.PAIS_id = " + Convert.ToInt32(ddlPaisReportTarifario);
            if (Convert.ToInt32(ddlCidadeRerpotTarifario) != 0)
            {
                sql = sql + " AND S_Servicos.Cid_Id = " + Convert.ToInt32(ddlCidadeRerpotTarifario);
            }
            sql = sql + " ORDER BY 2 ";

            SqlCommand cmd = new SqlCommand(sql, Con);
            SqlDataReader reader = cmd.ExecuteReader();

            DataRow drw = dt.NewRow();

            List<ModeloCidade> listaCidades = new List<ModeloCidade>();
            ModeloCidade modeloCidade = new ModeloCidade();

            string cidade;

            List<ModeloServico> listaServicos;
            ModeloServico servicos;

            List<string> listaBases;

            string d;
            string idSupplier = "";

            while (reader.Read())
            {
                modeloCidade = new ModeloCidade();
                modeloCidade.IdCidade = reader["Cid_Id"].ToString();
                modeloCidade.Sigla = reader["CID_uf"].ToString();

                listaCidades.Add(modeloCidade);
            }


            foreach (ModeloCidade item in listaCidades)
            {

                cabecalho = true;
                sib = true;
                naoSib = true;


                sql2 = "SELECT DISTINCT S_Servicos.Servicos_Nome, S_Servicos.Servicos_Id, Supplier.S_nome, Cidade.CID_nome, 'SIB' = case when S_Servicos.SIB IS NULL then (0) ELSE S_Servicos.SIB end, S_Servicos_Tipo_Categ.Ordenacao ";
                sql2 = sql2 + "FROM  Supplier ";
                sql2 = sql2 + "INNER JOIN S_Bases ON Supplier.S_id = S_Bases.S_id ";
                sql2 = sql2 + "INNER JOIN Monta_Servico ON Supplier.S_id = Monta_Servico.S_id ";
                sql2 = sql2 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico_Temporada ";
                sql2 = sql2 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";

                sql2 = sql2 + "INNER JOIN S_Servicos_Tipo_Categ ON S_Servicos.Tipo_categ_id = S_Servicos_Tipo_Categ.Tipo_categ_id ";

                sql2 = sql2 + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";

                sql2 = sql2 + "WHERE S_Servicos.Cid_Id = " + item.IdCidade;
                sql2 = sql2 + " AND MServico_DataFrom <= '" + dataFrom.ToString("yyyyMMdd") + "' and MServico_DataTo >= '" + dataTo.ToString("yyyyMMdd") + "'";

                if (Convert.ToInt32(ddlBaseTarifariaReportTarifario) != 0)
                {
                    sql2 = sql2 + "AND BaseTarifaria_id = " + Convert.ToInt32(ddlBaseTarifariaReportTarifario);
                }
                if (Convert.ToInt32(ddlMercadoReportTarifario) != 0)
                {
                    sql2 = sql2 + "AND Mercado_id = " + Convert.ToInt32(ddlMercadoReportTarifario);
                }

                if (!item.Sigla.Equals("RJ"))
                {
                    sql2 = sql2 + " AND Supplier.Tarifario = 'True' ";
                }

                ////COMEÇA OR

                sql2 = sql2 + " OR S_Servicos.Cid_Id = " + item.IdCidade;
                sql2 = sql2 + " AND MServico_DataFrom <= '" + dataTo.ToString("yyyyMMdd") + "' and MServico_DataTo >= '" + dataFrom.ToString("yyyyMMdd") + "'";

                if (Convert.ToInt32(ddlBaseTarifariaReportTarifario) != 0)
                {
                    sql2 = sql2 + "AND BaseTarifaria_id = " + Convert.ToInt32(ddlBaseTarifariaReportTarifario);
                }
                if (Convert.ToInt32(ddlMercadoReportTarifario) != 0)
                {
                    sql2 = sql2 + "AND Mercado_id = " + Convert.ToInt32(ddlMercadoReportTarifario);
                }

                if (!item.Sigla.Equals("RJ"))
                {
                    sql2 = sql2 + " AND Supplier.Tarifario = 'True' ";
                }

                sql2 = sql2 + "UNION ALL ";

                sql2 = sql2 + "SELECT DISTINCT S_Servicos.Servicos_Nome, S_Servicos.Servicos_Id, Supplier.S_nome, Cidade.CID_nome, 'SIB' = case when S_Servicos.SIB IS NULL then (0) ELSE S_Servicos.SIB end, S_Servicos_Tipo_Categ.Ordenacao ";
                sql2 = sql2 + "FROM Monta_Servico_Valores ";
                sql2 = sql2 + "INNER JOIN Monta_Servico ON Monta_Servico_Valores.IdMServico = Monta_Servico.IdMServico ";
                sql2 = sql2 + "INNER JOIN S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id ";
                sql2 = sql2 + "INNER JOIN Supplier ON Supplier.S_id = S_Bases.S_id ";
                sql2 = sql2 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico ";
                sql2 = sql2 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";

                sql2 = sql2 + "INNER JOIN S_Servicos_Tipo_Categ ON S_Servicos.Tipo_categ_id = S_Servicos_Tipo_Categ.Tipo_categ_id ";

                sql2 = sql2 + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";
                sql2 = sql2 + "LEFT OUTER JOIN S_Mercado_Tarifa_Status ON S_Mercado_Tarifa_Status.S_mercado_tarifa_status_id = Monta_Servico_Valores.S_mercado_tarifa_status_id ";
                sql2 = sql2 + "WHERE S_Servicos.Cid_Id = " + item.IdCidade;
                sql2 = sql2 + " AND MServico_DataFrom <= '" + dataFrom.ToString("yyyyMMdd") + "' and MServico_DataTo >= '" + dataTo.ToString("yyyyMMdd") + "'";

                if (Convert.ToInt32(ddlBaseTarifariaReportTarifario) != 0)
                {
                    sql2 = sql2 + "AND BaseTarifaria_id = " + Convert.ToInt32(ddlBaseTarifariaReportTarifario);
                }
                if (Convert.ToInt32(ddlMercadoReportTarifario) != 0)
                {
                    sql2 = sql2 + "AND Mercado_id = " + Convert.ToInt32(ddlMercadoReportTarifario);
                }

                if (!item.Sigla.Equals("RJ"))
                {
                    sql2 = sql2 + " AND Supplier.Tarifario = 'True' ";
                }

                sql2 = sql2 + " AND SubServico is not null ";


                ////////COMEÇA OR

                sql2 = sql2 + " OR S_Servicos.Cid_Id = " + item.IdCidade;
                sql2 = sql2 + " AND MServico_DataFrom <= '" + dataTo.ToString("yyyyMMdd") + "' and MServico_DataTo >= '" + dataFrom.ToString("yyyyMMdd") + "'";

                if (Convert.ToInt32(ddlBaseTarifariaReportTarifario) != 0)
                {
                    sql2 = sql2 + "AND BaseTarifaria_id = " + Convert.ToInt32(ddlBaseTarifariaReportTarifario);
                }
                if (Convert.ToInt32(ddlMercadoReportTarifario) != 0)
                {
                    sql2 = sql2 + "AND Mercado_id = " + Convert.ToInt32(ddlMercadoReportTarifario);
                }

                if (!item.Sigla.Equals("RJ"))
                {
                    sql2 = sql2 + " AND Supplier.Tarifario = 'True' ";
                }

                sql2 = sql2 + " AND SubServico is not null ";

                sql2 = sql2 + "ORDER BY 'SIB', S_Servicos_Tipo_Categ.Ordenacao ";

                SqlCommand cmd2 = new SqlCommand(sql2, Con);
                SqlDataReader reader2 = cmd2.ExecuteReader();

                listaServicos = new List<ModeloServico>();
                cidade = "";

                bool preenche = false;
                while (reader2.Read())
                {
                    cidade = reader2["CID_nome"].ToString();

                    servicos = new ModeloServico();
                    servicos.IdServico = reader2["Servicos_Id"].ToString();
                    servicos.IdSupplier = "";//reader2["S_id"].ToString();
                    listaServicos.Add(servicos);

                    preenche = true;
                }

                if (preenche)
                {
                    drw = dt.NewRow();
                    drw["CServico"] = cidade;
                    dt.Rows.Add(drw);
                }



                foreach (ModeloServico item2 in listaServicos)
                {



                    sql6 = "SELECT DISTINCT(Servico_Completo.SubServico) ";
                    sql6 = sql6 + "FROM Monta_Servico_Valores  ";
                    sql6 = sql6 + "INNER JOIN Monta_Servico ON Monta_Servico_Valores.IdMServico = Monta_Servico.IdMServico ";
                    sql6 = sql6 + "INNER JOIN S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id ";
                    sql6 = sql6 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico ";
                    sql6 = sql6 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";
                    sql6 = sql6 + "WHERE S_Servicos.Servicos_Id = " + item2.IdServico;

                    SqlCommand cmd6 = new SqlCommand(sql6, Con);
                    SqlDataReader reader6 = cmd6.ExecuteReader();

                    bool subServico = false;
                    string txtSubServico = "";

                    while (reader6.Read())
                    {

                        txtSubServico = reader6["SubServico"].ToString();
                        if (!txtSubServico.Equals(""))
                        {
                            subServico = true;
                        }
                    }

                    if (!subServico)
                    {

                        sql3 = "SELECT DISTINCT(CAST(Base_de AS VARCHAR(5)) + '/' + CAST(Base_ate AS VARCHAR(5))) as Base, Base_ate, S_Servicos.Servicos_Nome, S_Servicos.SIB ";
                        sql3 = sql3 + "FROM S_Bases_Valor ";
                        sql3 = sql3 + "INNER JOIN Monta_Servico ON S_Bases_Valor.IdMServico = Monta_Servico.IdMServico ";
                        sql3 = sql3 + "INNER JOIN S_Bases ON S_Bases_Valor.Base_id = S_Bases.Base_id ";
                        sql3 = sql3 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico_Temporada ";
                        sql3 = sql3 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";
                        sql3 = sql3 + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";
                        sql3 = sql3 + "WHERE S_Servicos.Servicos_Id = " + item2.IdServico;
                        sql3 = sql3 + " ORDER BY Base_ate ";
                    }
                    else
                    {

                        sql8 = "SELECT TOP(1) Monta_Servico.S_id ";
                        sql8 = sql8 + "FROM Monta_Servico_Valores  ";
                        sql8 = sql8 + "INNER JOIN Monta_Servico ON Monta_Servico_Valores.IdMServico = Monta_Servico.IdMServico ";
                        sql8 = sql8 + "INNER JOIN S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id ";
                        sql8 = sql8 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico ";
                        sql8 = sql8 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";
                        sql8 = sql8 + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";
                        sql8 = sql8 + "LEFT OUTER JOIN S_Mercado_Tarifa_Status ON S_Mercado_Tarifa_Status.S_mercado_tarifa_status_id = Monta_Servico_Valores.S_mercado_tarifa_status_id ";
                        sql8 = sql8 + "WHERE S_Servicos.Cid_Id = " + item.IdCidade;
                        sql8 = sql8 + " AND S_Servicos.Servicos_Id = " + item2.IdServico; ;
                        sql8 = sql8 + " ORDER BY S_id ";

                        SqlCommand cmd8 = new SqlCommand(sql8, Con);
                        SqlDataReader reader8 = cmd8.ExecuteReader();

                        while (reader8.Read())
                        {
                            item2.IdSupplier = reader8["S_id"].ToString();
                        }


                        sql3 = "SELECT DISTINCT(CAST(Base_de AS VARCHAR(5)) + '/' + CAST(Base_ate AS VARCHAR(5))) as Base, Base_ate, S_Servicos.Servicos_Nome, S_Servicos.SIB ";
                        sql3 = sql3 + "FROM Monta_Servico_Valores  ";
                        sql3 = sql3 + "INNER JOIN Monta_Servico ON Monta_Servico_Valores.IdMServico = Monta_Servico.IdMServico ";
                        sql3 = sql3 + "INNER JOIN S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id ";
                        sql3 = sql3 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico ";
                        sql3 = sql3 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";
                        sql3 = sql3 + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";
                        sql3 = sql3 + "LEFT OUTER JOIN S_Mercado_Tarifa_Status ON S_Mercado_Tarifa_Status.S_mercado_tarifa_status_id = Monta_Servico_Valores.S_mercado_tarifa_status_id ";
                        sql3 = sql3 + "WHERE S_Servicos.Servicos_Id = " + item2.IdServico;
                        sql3 = sql3 + "AND Monta_Servico.S_id = " + item2.IdSupplier;
                        sql3 = sql3 + " ORDER BY Base_ate ";
                    }


                    SqlCommand cmd3 = new SqlCommand(sql3, Con);
                    SqlDataReader reader3 = cmd3.ExecuteReader();

                    listaBases = new List<string>();
                    string servico = "";
                    string sibString = "";
                    idSupplier = item2.IdSupplier;

                    drw = dt.NewRow();

                    while (reader3.Read())
                    {
                        servico = reader3["Servicos_Nome"].ToString();
                        sibString = reader3["SIB"].ToString();
                        listaBases.Add(reader3["Base"].ToString());
                    }



                    if (cabecalho)
                    {
                        for (int i = 0; i < listaBases.Count; i++)
                        {
                            d = "C" + Convert.ToString(i + 1);
                            drw[d] = listaBases[i];
                        }

                        if (naoSib && (sibString.ToString().Equals("") || sibString.ToString().Equals("False")))
                        {
                            drw["Servico"] = "P R I V A T E   S E R V I C E S";
                            naoSib = false;
                        }

                        dt.Rows.Add(drw);

                        drw = dt.NewRow();

                        cabecalho = false;

                    }


                    if (sib && sibString.Equals("True"))
                    {
                        drw["Servico"] = "R E G U L A R   S E R V I C E S";
                        dt.Rows.Add(drw);
                        drw = dt.NewRow();
                        sib = false;
                    }

                    sql5 = "SELECT ";
                    if (!subServico)
                    {
                        sql5 = sql5 + "TOP(1) Monta_Servico.IdMServico AS IdServico, Monta_Servico.MServico_DataFrom ";
                    }
                    else
                    {
                        sql5 = sql5 + "DISTINCT(Monta_Servico.IdMServico) AS IdServico, Monta_Servico.MServico_DataFrom ";
                    }
                    sql5 = sql5 + "FROM Monta_Servico_Valores  ";
                    sql5 = sql5 + "INNER JOIN Monta_Servico ON Monta_Servico_Valores.IdMServico = Monta_Servico.IdMServico ";
                    sql5 = sql5 + "INNER JOIN S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id ";
                    sql5 = sql5 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico ";
                    sql5 = sql5 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";
                    sql5 = sql5 + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";
                    sql5 = sql5 + "LEFT OUTER JOIN S_Mercado_Tarifa_Status ON S_Mercado_Tarifa_Status.S_mercado_tarifa_status_id = Monta_Servico_Valores.S_mercado_tarifa_status_id ";

                    sql5 = sql5 + "WHERE S_Servicos.Servicos_Id = " + item2.IdServico;
                    if (subServico)
                    {
                        sql5 = sql5 + "AND Monta_Servico.S_id = " + idSupplier;
                    }

                    //alteração 22/10/2014 não aparecia sub serviços pois o TOP(1) da query sempre pegava o primeiro e não respeitava a data...

                    sql5 = sql5 + " AND (Monta_Servico.MServico_DataFrom <= '" + dataFrom.ToString("yyyyMMdd") + "') AND (Monta_Servico.MServico_DataTo >= '" + dataTo.ToString("yyyyMMdd") + "') OR";
                    sql5 = sql5 + " S_Servicos.Servicos_Id = " + item2.IdServico;
                    if (subServico)
                    {
                        sql5 = sql5 + "AND Monta_Servico.S_id = " + idSupplier;
                    }
                    sql5 = sql5 + " AND (Monta_Servico.MServico_DataFrom <= '" + dataTo.ToString("yyyyMMdd") + "') AND (Monta_Servico.MServico_DataTo >= '" + dataFrom.ToString("yyyyMMdd") + "') ";

                    //fim 22/10/2014

                    sql5 = sql5 + " ORDER BY Monta_Servico.MServico_DataFrom, IdServico ";

                    SqlCommand cmd5 = new SqlCommand(sql5, Con);
                    SqlDataReader reader5 = cmd5.ExecuteReader();

                    while (reader5.Read())
                    {

                        if (!subServico)
                        {

                            sql7 = "SELECT Cidade.CID_nome, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, S_Servicos.Servicos_Nome, S_Bases.Base_de, S_Bases.Base_ate, S_Bases.Moeda_id, S_Bases_Valor.S_Bases_Valor_valor, '' as S_mercado_tarifa_status_nome, Moeda_id  ";
                            sql7 = sql7 + "FROM S_Bases_Valor ";
                            sql7 = sql7 + "INNER JOIN Monta_Servico ON S_Bases_Valor.IdMServico = Monta_Servico.IdMServico ";
                            sql7 = sql7 + "INNER JOIN S_Bases ON S_Bases_Valor.Base_id = S_Bases.Base_id ";
                            sql7 = sql7 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico_Temporada ";
                            sql7 = sql7 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";
                            sql7 = sql7 + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";
                            sql7 = sql7 + "WHERE S_Servicos.Servicos_Id = " + item2.IdServico;
                            sql7 = sql7 + "AND Servico_Completo.IdMServico = " + reader5["IdServico"];

                            sql7 = sql7 + " AND MServico_DataFrom <= '" + dataFrom.ToString("yyyyMMdd") + "' and MServico_DataTo >= '" + dataTo.ToString("yyyyMMdd") + "'";

                            //////COMEÇAR OR

                            sql7 = sql7 + " OR S_Servicos.Servicos_Id = " + item2.IdServico;
                            sql7 = sql7 + " AND Servico_Completo.IdMServico = " + reader5["IdServico"];
                            sql7 = sql7 + " AND MServico_DataFrom <= '" + dataTo.ToString("yyyyMMdd") + "' and MServico_DataTo >= '" + dataFrom.ToString("yyyyMMdd") + "'";

                            sql7 = sql7 + " ORDER BY Cidade.CID_nome, S_Servicos.Servicos_Nome, S_Bases.Base_ate, S_Bases.Base_de, Monta_Servico.MServico_DataFrom ";



                            SqlCommand cmd7 = new SqlCommand(sql7, Con);
                            SqlDataReader reader7 = cmd7.ExecuteReader();

                            //bool passou = false;
                            int coluna = 1;
                            while (reader7.Read())
                            {
                                cambio = 1;
                                if (Convert.ToInt32(reader7["Moeda_id"]) != Convert.ToInt32(ddlCambioReportTarifario) && ddlCambioReportTarifario != 0)
                                {
                                    cambio = utils.CalcularCambio(Convert.ToInt32(reader7["Moeda_id"]), Convert.ToInt32(ddlCambioReportTarifario));
                                }

                                adiciona = true;
                                drw["Servico"] = servico;

                                drw["D20"] = string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:MMM, dd yyyy}", reader7["MServico_DataFrom"]) + " - " + string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:MMM, dd yyyy}", reader7["MServico_DataTo"]);

                                d = "D" + coluna.ToString();

                                if (coluna < 19)
                                {
                                    total = arrendodar(Convert.ToDecimal(NuloZero(reader7["S_Bases_Valor_valor"].ToString())) * cambio + ((Convert.ToDecimal(NuloZero(reader7["S_Bases_Valor_valor"].ToString())) * cambio) / 100) * markUp);
                                    totalString = Convert.ToString(total.ToString("N2"));
                                    drw[d] = totalString;
                                }

                                coluna = coluna + 1;

                            }

                        }
                        else
                        {
                            sql4 = "SELECT Cidade.CID_nome, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, S_Servicos.Servicos_Nome, S_Bases.Base_de, S_Bases.Base_ate, S_Bases.Moeda_id, ";
                            sql4 = sql4 + "Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_mercado_tarifa_status_nome, Servico_Completo.SubServico, Servico_Completo.IdMServico_Temporada ";
                            sql4 = sql4 + "FROM Monta_Servico_Valores  ";
                            sql4 = sql4 + "INNER JOIN Monta_Servico ON Monta_Servico_Valores.IdMServico = Monta_Servico.IdMServico ";
                            sql4 = sql4 + "INNER JOIN S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id ";
                            sql4 = sql4 + "INNER JOIN Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico ";
                            sql4 = sql4 + "INNER JOIN S_Servicos ON Servico_Completo.Servicos_Id = S_Servicos.Servicos_Id ";
                            sql4 = sql4 + "INNER JOIN Cidade ON S_Servicos.Cid_Id = Cidade.CID_id ";
                            sql4 = sql4 + "LEFT OUTER JOIN S_Mercado_Tarifa_Status ON S_Mercado_Tarifa_Status.S_mercado_tarifa_status_id = Monta_Servico_Valores.S_mercado_tarifa_status_id ";
                            sql4 = sql4 + "WHERE S_Servicos.Servicos_Id = " + item2.IdServico;
                            sql4 = sql4 + "AND Monta_Servico.IdMServico = " + reader5["IdServico"];

                            sql4 = sql4 + " AND MServico_DataFrom <= '" + dataFrom.ToString("yyyyMMdd") + "' and MServico_DataTo >= '" + dataTo.ToString("yyyyMMdd") + "'";


                            /////COMEÇA O OR

                            sql4 = sql4 + " OR S_Servicos.Servicos_Id = " + item2.IdServico;
                            sql4 = sql4 + " AND Monta_Servico.IdMServico = " + reader5["IdServico"];

                            sql4 = sql4 + " AND MServico_DataFrom <= '" + dataTo.ToString("yyyyMMdd") + "' and MServico_DataTo >= '" + dataFrom.ToString("yyyyMMdd") + "'";


                            sql4 = sql4 + " ORDER BY Cidade.CID_nome, S_Bases.Base_ate, S_Servicos.Servicos_Nome, S_Bases.Base_de, Monta_Servico.MServico_DataFrom ";



                            SqlCommand cmd4 = new SqlCommand(sql4, Con);
                            SqlDataReader reader4 = cmd4.ExecuteReader();

                            //bool passou = false;
                            int coluna = 1;
                            while (reader4.Read())
                            {


                                if (reader4["MServico_Valores_Bulk_Total"].ToString().Equals(""))
                                {
                                    totalString = reader4["S_mercado_tarifa_status_nome"].ToString();
                                }
                                else
                                {
                                    cambio = 1;
                                    if (Convert.ToInt32(reader4["Moeda_id"]) != Convert.ToInt32(ddlCambioReportTarifario) && ddlCambioReportTarifario != 0)
                                    {
                                        cambio = utils.CalcularCambio(Convert.ToInt32(reader4["Moeda_id"]), Convert.ToInt32(ddlCambioReportTarifario));
                                    }


                                    total = arrendodar(Convert.ToDecimal(reader4["MServico_Valores_Bulk_Total"].ToString()) * cambio + ((Convert.ToDecimal(reader4["MServico_Valores_Bulk_Total"].ToString()) * cambio) / 100) * markUp);
                                    totalString = Convert.ToString(total.ToString("N2"));
                                }

                                adiciona = true;
                                drw["Servico"] = servico;
                                drw["D20"] = string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:MMM, dd yyyy}", reader4["MServico_DataFrom"]) + " - " + string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:MMM, dd yyyy}", reader4["MServico_DataTo"]);

                                d = "D" + coluna.ToString();
                                drw[d] = totalString;

                                coluna = coluna + 1;

                            }
                        }

                        if (adiciona)
                        {
                            dt.Rows.Add(drw);
                            drw = dt.NewRow();
                            adiciona = false;
                        }

                    }

                }
            }

            RptServicosTarifario rpt = new RptServicosTarifario();

            rpt.SetDataSource(dt);

            string path = pathSaveReport("tarifarioservicos");
            string url = urlOpenReport("tarifarioservicos");

            switch (ddlFormatoReportServicos)
            {
                case 1:
                    if (System.IO.File.Exists(path + ".pdf"))
                        System.IO.File.Delete(path + ".pdf");

                    rpt.ExportToDisk(ExportFormatType.PortableDocFormat, path + ".pdf");
                    url = url + ".pdf";

                    return Json(new { success = true, url }, JsonRequestBehavior.AllowGet);


                case 2:
                    if (System.IO.File.Exists(path + ".doc"))
                        System.IO.File.Delete(path + ".doc");

                    rpt.ExportToDisk(ExportFormatType.WordForWindows, path + ".doc");
                    url = url + ".doc";

                    return Json(new { success = true, url }, JsonRequestBehavior.AllowGet);

                case 3:
                    if (System.IO.File.Exists(path + ".xls"))
                        System.IO.File.Delete(path + ".xls");

                    rpt.ExportToDisk(ExportFormatType.Excel, path + ".xls");
                    url = url + ".xls";

                    return Json(new { success = true, url }, JsonRequestBehavior.AllowGet);

                default:
                    Stream streamP = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    return File(streamP, "application/pdf", "tarifarioservicos.pdf");
            }

        }

        #endregion

        #region Periodos Servicos

        public ActionResult PopularGridSuppliersServico(int IdServico)
        {
            List<PeriodoSimplesModel> lst = new List<PeriodoSimplesModel>();

            try
            {
                MontaServicoDAL msd = new MontaServicoDAL();

                lst = msd.ListarTodosCondicaoPorServico_model(IdServico);

                return PartialView("_lst_PeriodosSimples", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_PeriodosSimples", lst);
            }
        }

        public ActionResult PopulaGridServicoSozinho(int txtCodMServicoSozinho, int index, string txtSupplierMontaServico, int ddlMercadoFiltro_Base, int ddlBaseTarifariaFiltro_Base, int ddlMoedaFiltro_Base, int ddlTipoBase_Base)
        {
            List<S_Bases> lst = new List<S_Bases>();
            try
            {
                SBasesDAL b = new SBasesDAL();
                SupplierDAL s = new SupplierDAL();
                MontaServicoDAL ms = new MontaServicoDAL();

                Supplier sp = s.ObterSupplierPorNome(txtSupplierMontaServico);
                //ViewState["IdSupplier"] = sp.S_id;

                TempData["idSupplier"] = sp.S_id;

                if (b.ListarTodosPorSupplier(sp.S_id, index, Convert.ToInt32(ddlMercadoFiltro_Base), Convert.ToInt32(ddlBaseTarifariaFiltro_Base), Convert.ToInt32(ddlMoedaFiltro_Base), Convert.ToInt32(ddlTipoBase_Base)).Count() == 0)
                {
                    ViewBag.message = "Não existe bases cadastradas para este supplier.";
                    return PartialView("_lst_baseSupplierServicoSozinho", lst);
                }
                else
                {
                    //btnSalvarTarifas.Visible = true;
                    //rdoBasesIndex.SelectedValue = index.ToString();
                    //lblMsgCadMServicos.Text = "";
                }

                SMercadoTarifaStatusDAL smt = new SMercadoTarifaStatusDAL();
                ViewBag.tipoCadastroBSSS = smt.ListarTodos();

                lst = b.ListarTodosPorSupplier(sp.S_id, index, Convert.ToInt32(ddlMercadoFiltro_Base), Convert.ToInt32(ddlBaseTarifariaFiltro_Base), Convert.ToInt32(ddlMoedaFiltro_Base), Convert.ToInt32(ddlTipoBase_Base));

                ViewBag.listaValores = 0;

                if (txtCodMServicoSozinho != 0)
                {
                    //PopulaIntoGridBaseSupplierServicoSozinho(GridBaseSupplierServicoSozinho);
                    ViewBag.listaValores = 0;
                }



                return PartialView("_lst_baseSupplierServicoSozinho", lst);


            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_baseSupplierServicoSozinho", lst);
            }
        }

        public JsonResult CalculaTaxas(decimal valorBaseSozinha, decimal txtPorcentagemTaxa, decimal txtPorcentagemImposto, decimal txtComissao)
        {
            try
            {
                decimal taxa = 0;
                decimal imposto = 0;
                decimal comissao = 0;
                decimal total = 0;

                if (txtPorcentagemTaxa != 0)
                {
                    taxa = ((valorBaseSozinha * txtPorcentagemTaxa) / 100) + valorBaseSozinha;
                }

                if (txtPorcentagemImposto != 0)
                {
                    imposto = ((taxa * txtPorcentagemImposto) / 100) + taxa;
                }

                if (taxa != 0)
                    total = taxa;

                if (imposto != 0)
                    total = imposto;


                if (txtComissao != 0)
                {
                    comissao = (valorBaseSozinha * txtComissao) / 100;

                    if (total != 0)
                        comissao = total - comissao;
                    else
                        comissao = valorBaseSozinha - comissao;
                }


                if (total == 0)
                    total = valorBaseSozinha;

                if (comissao != 0)
                    total = comissao;

                total = Math.Round(total, 2);

                return Json(new { success = true, message = "", tot = total }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarTarifas(AddPeriodoServicoSozinhoModel model)
        {
            try
            {

                Utils u = new Utils();
                MontaServicoDAL msd = new MontaServicoDAL();

                Monta_Servico ms = new Monta_Servico();

                ms.MServico_Bulk_Porc_Taxa = Convert.ToDecimal(model.txtPorcentagemTaxa);
                ms.MServico_Bulk_Porc_Imposto = Convert.ToDecimal(model.txtPorcentagemImposto1);
                ms.MServico_Bulk_Porc_Comissao = Convert.ToDecimal(model.txtComissao);
                ms.MServico_DataFrom = Convert.ToDateTime(model.from);
                ms.MServico_DataTo = Convert.ToDateTime(model.to);
                ms.MServico_Obs = model.txtObs;
                ms.S_id = model.IdSupplier;
                ms.Tipo_Id = Convert.ToInt32(model.ddlTipoServico2);
                ms.Tipo_categ_id = Convert.ToInt32(model.ddlTipoServicoCateg2);

                msd.Salvar(ms);

                Servico_Completo sc = new Servico_Completo();
                sc.IdMServico = ms.IdMServico;
                sc.Servicos_Id = model.IdServico;
                sc.SubServico = false;
                sc.Base_Index = model.index;

                ServicoCompletoDAL scd = new ServicoCompletoDAL();
                scd.Salvar(sc);

                string[] pars = model.valoresGrid.Split('~');

                var l = System.Threading.Thread.CurrentThread.CurrentCulture;

                for (int i = 0; i < pars.Length; i++)
                {

                    Monta_Servico_Valores msv = new Monta_Servico_Valores();

                    string[] vrs = pars[i].Split('°');

                    string baseID = vrs[0];
                    var ddlStatusTarifa = vrs[2];

                    var txtValorServicoSozinhoVenda = "";
                    var txtValorServicoSozinho = "";
                    var lblValorServicoSozinhoTotal = "";

                    if (l.Name.Equals("en-US"))
                    {
                        txtValorServicoSozinhoVenda = vrs[5].Replace(',', '.');
                        txtValorServicoSozinho = vrs[3].Replace(',', '.');
                        lblValorServicoSozinhoTotal = vrs[4].Replace(',', '.');

                        //msv.MServico_Valores_Bulk = Convert.ToDecimal(bulks[0].Replace(',', '.'));
                        //msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(totalbulks[0].Replace(',', '.'));
                    }
                    else
                    {
                        txtValorServicoSozinhoVenda = vrs[5].Replace('.', ',');
                        txtValorServicoSozinho = vrs[3].Replace('.', ',');
                        lblValorServicoSozinhoTotal = vrs[4].Replace('.', ',');

                        //msv.MServico_Valores_Bulk = Convert.ToDecimal(bulks[0].Replace('.', ','));
                        //msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(totalbulks[0].Replace('.', ','));
                    }


                    //string Bulk = txtValorServicoSozinho.Text;

                    var tipocadastro = vrs[1];

                    msv.IdMServico = ms.IdMServico;

                    if (model.opStatusTarifa == 1)
                    {
                        if (txtValorServicoSozinho.Equals(""))
                        {
                            msv.MServico_Valores_Bulk_Venda = null;
                            msv.MServico_Valores_Bulk = null;
                            msv.MServico_Valores_Bulk_Total = null;
                            msv.S_mercado_tarifa_status_id = Convert.ToInt32(ddlStatusTarifa);
                        }
                        else
                        {
                            if (txtValorServicoSozinhoVenda.Equals(""))
                            {
                                msv.MServico_Valores_Bulk_Venda = null;
                            }
                            else
                            {
                                msv.MServico_Valores_Bulk_Venda = Convert.ToDecimal(txtValorServicoSozinhoVenda);
                            }
                            msv.MServico_Valores_Bulk = Convert.ToDecimal(txtValorServicoSozinho);

                            if (lblValorServicoSozinhoTotal.Equals(""))
                            { msv.MServico_Valores_Bulk_Total = null; }
                            else
                            { msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(lblValorServicoSozinhoTotal); }
                        }
                    }
                    else
                    {
                        msv.MServico_Valores_Bulk_Venda = null;
                        msv.MServico_Valores_Bulk = null;
                        msv.MServico_Valores_Bulk_Total = null;

                        int IdStatus = Convert.ToInt32(ConfigurationManager.AppSettings["StatusTarifaServico"]);

                        msv.S_mercado_tarifa_status_id = IdStatus;
                    }

                    msv.Base_id = Convert.ToInt32(baseID);

                    MontaServicoValoresDAL msvd = new MontaServicoValoresDAL();
                    msvd.Salvar(msv);
                }


                //MostrarMsg("Registro cadastrado com sucesso.");

                //VerificaServico();


                //PopulaGridServicoSozinho(Convert.ToInt32(rdoBasesIndex.SelectedValue));
                //PopulaGridMServicosSozinhoCadastrados();
                //popularDropPeriodosCopiarMServicoSozinho();

                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizarTarifas(AddPeriodoServicoSozinhoModel model)
        {
            try
            {

                Utils u = new Utils();
                MontaServicoDAL msd = new MontaServicoDAL();

                Monta_Servico ms = new Monta_Servico();

                ms.IdMServico = model.CodMServicoSozinho;
                ms.MServico_Bulk_Porc_Taxa = Convert.ToDecimal(model.txtPorcentagemTaxa);
                ms.MServico_Bulk_Porc_Imposto = Convert.ToDecimal(model.txtPorcentagemImposto1);
                ms.MServico_Bulk_Porc_Comissao = Convert.ToDecimal(model.txtComissao);
                ms.MServico_DataFrom = Convert.ToDateTime(model.from);
                ms.MServico_DataTo = Convert.ToDateTime(model.to);
                ms.MServico_Obs = model.txtObs;
                ms.S_id = model.IdSupplier;
                ms.Tipo_Id = Convert.ToInt32(model.ddlTipoServico2);
                ms.Tipo_categ_id = Convert.ToInt32(model.ddlTipoServicoCateg2);

                msd.Atualizar(ms);

                //Servico_Completo sc = new Servico_Completo();
                //sc.IdMServico = ms.IdMServico;
                //sc.Servicos_Id = model.IdServico;
                //sc.SubServico = false;
                //sc.Base_Index = model.index;

                //ServicoCompletoDAL scd = new ServicoCompletoDAL();
                //scd.Salvar(sc);

                string[] pars = model.valoresGrid.Split('~');

                var l = System.Threading.Thread.CurrentThread.CurrentCulture;

                for (int i = 0; i < pars.Length; i++)
                {

                    Monta_Servico_Valores msv = new Monta_Servico_Valores();

                    string[] vrs = pars[i].Split('°');

                    string baseID = vrs[0];
                    var ddlStatusTarifa = vrs[2];
                    var txtValorServicoSozinhoVenda = vrs[5].Replace('.', ',');
                    var txtValorServicoSozinho = vrs[3].Replace('.', ',');
                    var lblValorServicoSozinhoTotal = vrs[4].Replace('.', ',');
                    var tipocadastro = vrs[1];

                    msv.IdMServicoValores = Convert.ToInt32(vrs[6]);
                    msv.IdMServico = ms.IdMServico;

                    if (tipocadastro.Equals("1"))
                    {
                        msv.MServico_Valores_Bulk_Venda = null;
                        msv.MServico_Valores_Bulk = null;
                        msv.MServico_Valores_Bulk_Total = null;
                        msv.S_mercado_tarifa_status_id = Convert.ToInt32(ddlStatusTarifa);
                    }
                    else
                    {

                        if (txtValorServicoSozinhoVenda.Equals(""))
                        {
                            msv.MServico_Valores_Bulk_Venda = null;
                        }
                        else
                        {
                            if (l.Name.Equals("en-US"))
                            {
                                msv.MServico_Valores_Bulk_Venda = Convert.ToDecimal(vrs[5]);
                            }
                            else
                            {
                                msv.MServico_Valores_Bulk_Venda = Convert.ToDecimal(txtValorServicoSozinhoVenda);
                            }

                        }
                        if (l.Name.Equals("en-US"))
                        {
                            msv.MServico_Valores_Bulk = Convert.ToDecimal(vrs[3]);
                        }
                        else
                        {
                            msv.MServico_Valores_Bulk = Convert.ToDecimal(txtValorServicoSozinho);
                        }

                        if (lblValorServicoSozinhoTotal.Equals(""))
                        { msv.MServico_Valores_Bulk_Total = null; }
                        else
                        {
                            if (l.Name.Equals("en-US"))
                            {
                                msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(vrs[4]);
                            }
                            else
                            {
                                msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(lblValorServicoSozinhoTotal);
                            }
                        }
                    }

                    msv.Base_id = Convert.ToInt32(baseID);

                    MontaServicoValoresDAL msvd = new MontaServicoValoresDAL();
                    msvd.Atualizar(msv);
                }

                return Json(new { success = true, message = "Registro atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirTarifas(int myid)
        {
            try
            {
                MontaServicoDAL msd = new MontaServicoDAL();

                Monta_Servico ms = msd.ObterPorId(myid);

                msd.Excluir(ms);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PopulaGridMServicosSozinhoCadastrados(int IdSupplier, int IdServico)
        {
            List<PeriodosCadastradosSozinhoModel> lst = new List<PeriodosCadastradosSozinhoModel>();
            try
            {
                MontaServicoDAL ms = new MontaServicoDAL();

                lst = ms.ListarTodosCondicaoMServico_model(IdSupplier, IdServico);

                return PartialView("_lst_GridMServicosSozinhoCadastrados", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_GridMServicosSozinhoCadastrados", lst);
            }
        }

        public class EditarServSozinhoModel
        {
            public string txtCodMServicoSozinho { get; set; }
            public string ddlTipoServico2 { get; set; }
            public string ddlTipoServicoCateg2 { get; set; }
            public string from { get; set; }
            public string to { get; set; }
            public string txtObs { get; set; }
            public string txtPorcentagemTaxa { get; set; }
            public string txtPorcentagemImposto1 { get; set; }
            public string txtComissao { get; set; }
            public int ddlMercadoFiltro_Base { get; set; }
            public int ddlBaseTarifariaFiltro_Base { get; set; }
            public int ddlTipoBase_Base { get; set; }
            public int ddlMoedaFiltro_Base { get; set; }
            public int index { get; set; }
        }

        public JsonResult EditarMServico(string parametros, int idservico)
        {
            try
            {
                string[] param = parametros.Split(',');

                MontaServicoDAL msd = new MontaServicoDAL();
                MontaServicoValoresDAL msvd = new MontaServicoValoresDAL();

                Monta_Servico ms = msd.ObterPorId(Convert.ToInt32(param[0]));

                EditarServSozinhoModel model = new EditarServSozinhoModel();

                model.txtCodMServicoSozinho = ms.IdMServico.ToString();
                model.ddlTipoServico2 = ms.Tipo_Id.ToString();
                //popularDropTipoServicoCateg();
                model.ddlTipoServicoCateg2 = ms.Tipo_categ_id.ToString();
                model.from = string.Format("{0:yyyy-MM-dd}", ms.MServico_DataFrom);
                model.to = string.Format("{0:yyyy-MM-dd}", ms.MServico_DataTo);
                model.txtObs = ms.MServico_Obs;
                model.txtPorcentagemTaxa = ms.MServico_Bulk_Porc_Taxa.ToString();
                model.txtPorcentagemImposto1 = ms.MServico_Bulk_Porc_Imposto.ToString();
                model.txtComissao = ms.MServico_Bulk_Porc_Comissao.ToString();


                SBasesDAL bd = new SBasesDAL();

                S_Bases sb = bd.ObterPorIdSupplier(ms.S_id, Convert.ToInt32(param[1])).First();

                model.ddlMercadoFiltro_Base = Convert.ToInt32(param[1]);
                model.ddlBaseTarifariaFiltro_Base = Convert.ToInt32(param[2]);
                model.ddlTipoBase_Base = Convert.ToInt32(param[3]);
                model.ddlMoedaFiltro_Base = Convert.ToInt32(param[4]);

                Monta_Servico_Valores msv = msvd.ObterPorIdMServico(ms.IdMServico);


                model.index = Convert.ToInt32(msv.S_Bases.Base_index);

                var modeloDados1 = PopulaIntoGridBaseSupplierServicoSozinho(ms.IdMServico); //, model.index, ms.S_id, model.ddlMercadoFiltro_Base, model.ddlBaseTarifariaFiltro_Base, model.ddlMoedaFiltro_Base, model.ddlTipoBase_Base);

                return Json(new { success = true, message = "", modelo = model, modeloDados = modeloDados1 }, JsonRequestBehavior.AllowGet);

                //PopulaGridServicoSozinho(index);

            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string PopulaIntoGridBaseSupplierServicoSozinho(int IdMServico)
        {
            //List<DadosGridSozinhoModel> listaDados = new List<DadosGridSozinhoModel>();
            try
            {
                MontaServicoValoresDAL msv = new MontaServicoValoresDAL();

                List<Monta_Servico_Valores> listaValores = msv.ListaTodos(IdMServico);
                MontaServicoDAL ms = new MontaServicoDAL();
                SBasesDAL b = new SBasesDAL();

                //var lst = b.ListarTodosPorSupplier(idsupplier, index, Convert.ToInt32(ddlMercadoFiltro_Base), Convert.ToInt32(ddlBaseTarifariaFiltro_Base), Convert.ToInt32(ddlMoedaFiltro_Base), Convert.ToInt32(ddlTipoBase_Base));

                string str = "";
                foreach (Monta_Servico_Valores lista in listaValores)
                {
                    //DadosGridSozinhoModel dado = new DadosGridSozinhoModel();


                    //dado.Base_id = (int)lista.Base_id;

                    if (lista.MServico_Valores_Bulk == null)
                    {
                        str = str + string.Format("{0}°{1}°{2}°{3}~", (int)lista.Base_id, "Status", lista.IdMServicoValores, (int)lista.S_mercado_tarifa_status_id);

                        //dado.ddlTipoCadastro = "Status";
                        //dado.ddlStatusTarifa = (int)lista.S_mercado_tarifa_status_id;
                        //dado.lblIDValores = lista.IdMServicoValores;

                        //ddlTipoCadastro.SelectedValue = "1";                               
                        //ddlStatusTarifa.SelectedValue = lista.S_mercado_tarifa_status_id.ToString();
                        //ddlStatusTarifa.Visible = true;
                        //txtValorServicoSozinho.Visible = false;
                        //txtValorServicoSozinhoVenda.Visible = false;
                        //lblIDValores.Text = Convert.ToString(lista.IdMServicoValores);
                    }
                    else
                    {
                        var vlr = lista.MServico_Valores_Bulk == null ? "" : lista.MServico_Valores_Bulk.ToString();
                        var vlrTotal = lista.MServico_Valores_Bulk_Total == null ? "" : lista.MServico_Valores_Bulk_Total.ToString();
                        var vlrVenda = lista.MServico_Valores_Bulk_Venda == null ? "" : lista.MServico_Valores_Bulk_Venda.ToString();

                        str = str + string.Format("{0}°{1}°{2}°{3}°{4}°{5}~", (int)lista.Base_id, "Valor", lista.IdMServicoValores, vlr, vlrTotal, vlrVenda);

                        //dado.ddlTipoCadastro = "Valor";
                        //dado.txtValorServicoSozinhoVenda = lista.MServico_Valores_Bulk_Venda == null ? "" : lista.MServico_Valores_Bulk_Venda.ToString();
                        //dado.txtValorServicoSozinho = lista.MServico_Valores_Bulk == null ? "" :  lista.MServico_Valores_Bulk.ToString();
                        //dado.lblValorServicoSozinhoTotal = lista.MServico_Valores_Bulk_Total == null ? "" :  lista.MServico_Valores_Bulk_Total.ToString();
                        //dado.lblIDValores = lista.IdMServicoValores;

                        //txtValorServicoSozinhoVenda.Text = Convert.ToString(lista.MServico_Valores_Bulk_Venda);
                        //txtValorServicoSozinho.Text = Convert.ToString(lista.MServico_Valores_Bulk);
                        //lblValorServicoSozinhoTotal.Text = Convert.ToString(lista.MServico_Valores_Bulk_Total);
                        //lblIDValores.Text = Convert.ToString(lista.IdMServicoValores);
                        //ddlStatusTarifa.Visible = false;
                        //txtValorServicoSozinho.Visible = true;
                    }

                    //listaDados.Add(dado);
                }

                return str.Substring(0, str.Length - 1);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return "";
            }
        }

        public ActionResult GetTelaCopiarServicoSozinho(int IdSupplier, int IdServico)
        {
            try
            {
                MontaServicoDAL ms = new MontaServicoDAL();

                ViewBag.ddlPeriodosMServicoSozinho = ms.ListarTodosCondicaoMServico_model(IdSupplier, IdServico);

                return PartialView("_copiarServicoSozinho");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_copiarServicoSozinho");
            }
        }

        public JsonResult AdicionaMontaServicoSozinho(int ddlPeriodosMServicoSozinho, string txtPeriodoFromCopiarMServSozinho, string txtPeriodoToCopiarMServSozinho, string percent, int idservico)
        {
            try
            {

                MontaServicoDAL msd = new MontaServicoDAL();

                Monta_Servico ms = msd.ObterPorId(Convert.ToInt32(ddlPeriodosMServicoSozinho));

                Monta_Servico novo = new Monta_Servico();
                novo.MServico_Bulk_Porc_Taxa = ms.MServico_Bulk_Porc_Taxa;
                novo.MServico_Bulk_Porc_Imposto = ms.MServico_Bulk_Porc_Imposto;
                novo.MServico_Bulk_Porc_Comissao = ms.MServico_Bulk_Porc_Comissao;
                novo.MServico_PorPessoa = ms.MServico_PorPessoa;
                novo.MServico_DataFrom = Convert.ToDateTime(txtPeriodoFromCopiarMServSozinho);
                novo.MServico_DataTo = Convert.ToDateTime(txtPeriodoToCopiarMServSozinho);
                novo.MServico_Obs = ms.MServico_Obs;
                novo.S_id = ms.S_id;
                novo.Tipo_Id = ms.Tipo_Id;
                novo.Tipo_categ_id = ms.Tipo_categ_id;
                msd.Salvar(novo);

                AdicionaMontaServicoValoresSozinho(ms.IdMServico, novo.IdMServico, Convert.ToDecimal(ms.MServico_Bulk_Porc_Taxa), Convert.ToDecimal(ms.MServico_Bulk_Porc_Imposto), Convert.ToDecimal(ms.MServico_Bulk_Porc_Comissao), percent, idservico);

                return Json(new { success = true, message = "Copiado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        protected void AdicionaMontaServicoValoresSozinho(int IdMServicoAntigo, int IdMServicoNovo, decimal taxa, decimal imposto, decimal comissao, string porcentagem, int IdServico)
        {
            try
            {

                MontaServicoValoresDAL msvd = new MontaServicoValoresDAL();
                SBasesDAL sbaD = new SBasesDAL();

                List<Monta_Servico_Valores> listaMSV = msvd.ListaTodos(IdMServicoAntigo);

                var idbase = 0;
                S_Bases ba = new S_Bases();

                foreach (Monta_Servico_Valores item in listaMSV)
                {
                    Monta_Servico_Valores novo = new Monta_Servico_Valores();

                    if (item.MServico_Valores_Bulk == null)
                    {
                        novo.MServico_Valores_Bulk_Venda = item.MServico_Valores_Bulk_Venda;
                        novo.MServico_Valores_Bulk = item.MServico_Valores_Bulk;
                        novo.MServico_Valores_Bulk_Total = item.MServico_Valores_Bulk_Total;
                        novo.S_mercado_tarifa_status_id = item.S_mercado_tarifa_status_id;
                    }
                    else
                    {
                        novo.MServico_Valores_Bulk_Venda = item.MServico_Valores_Bulk_Venda;
                        novo.MServico_Valores_Bulk = item.MServico_Valores_Bulk;
                        novo.MServico_Valores_Bulk_Total = item.MServico_Valores_Bulk_Total;

                        if (porcentagem.Equals(""))
                        {
                            novo.MServico_Valores_Bulk_Venda = item.MServico_Valores_Bulk_Venda;
                            novo.MServico_Valores_Bulk = item.MServico_Valores_Bulk;
                            novo.MServico_Valores_Bulk_Total = item.MServico_Valores_Bulk_Total;
                        }
                        else
                        {
                            novo.MServico_Valores_Bulk_Venda = item.MServico_Valores_Bulk_Venda;

                            double percent = Convert.ToDouble(item.MServico_Valores_Bulk) * Convert.ToDouble(porcentagem) / 100;

                            novo.MServico_Valores_Bulk = Convert.ToDecimal(percent + Convert.ToDouble(item.MServico_Valores_Bulk));

                            double Bulk = Convert.ToDouble(novo.MServico_Valores_Bulk);

                            if (!taxa.Equals(""))
                            {

                                double valortarifa = Convert.ToDouble(Bulk);
                                double valortotalComTaxa = (valortarifa * Convert.ToDouble(taxa)) / 100;

                                double d = Convert.ToDouble(valortarifa + valortotalComTaxa);
                                Bulk = d;

                                double valortarifa2 = Convert.ToDouble(Bulk);
                                double valortotalComImposto = (valortarifa2 * Convert.ToDouble(imposto)) / 100;

                                double d2 = Convert.ToDouble(valortarifa2 + valortotalComImposto);

                                novo.MServico_Valores_Bulk_Total = Convert.ToDecimal(d2);

                                if (comissao != 0)
                                {
                                    double valortarifaCom = Convert.ToDouble(Bulk);
                                    double valortotalComComissao = (valortarifa * Convert.ToDouble(comissao)) / 100;

                                    double dCom = Convert.ToDouble(Convert.ToDouble(Bulk) - valortotalComComissao);

                                    novo.MServico_Valores_Bulk_Total = Convert.ToDecimal(dCom);
                                }

                            }
                        }
                    }

                    novo.IdMServico = IdMServicoNovo;
                    novo.Base_id = item.Base_id;

                    if (idbase == 0)
                    {
                        ba = sbaD.ObterPorId((int)item.Base_id);
                    }

                    msvd.Salvar(novo);
                }

                Servico_Completo sc = new Servico_Completo();
                sc.IdMServico = IdMServicoNovo;
                sc.Servicos_Id = IdServico;
                sc.SubServico = false;
                sc.Base_Index = ba.Base_index; //Convert.ToInt32(rdoBasesIndex.SelectedValue);

                ServicoCompletoDAL scd = new ServicoCompletoDAL();
                scd.Salvar(sc);



            }
            catch (Exception ex)
            {

            }
        }

        public ActionResult GetTelaCopiarServicoSub(int IdSupplier, int IdServico)
        {
            try
            {
                MontaServicoDAL ms = new MontaServicoDAL();

                ViewBag.ddlPeriodosMServicoSub = ms.ListarTodosSubServicoSub_model(IdSupplier, IdServico);

                return PartialView("_copiarServicoSub");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_copiarServicoSub");
            }
        }

        public JsonResult AdicionaMontaServicoSub(int ddlPeriodosMServicoSub, string txtPeriodoFromCopiarMServSub, string txtPeriodoToCopiarMServSub, int idservico)
        {
            try
            {

                MontaServicoDAL msd = new MontaServicoDAL();
                ServicoCompletoDAL scd = new ServicoCompletoDAL();

                Monta_Servico ms = msd.ObterPorId(Convert.ToInt32(ddlPeriodosMServicoSub));

                Monta_Servico mss = new Monta_Servico();

                mss.MServico_Bulk_Porc_Taxa = ms.MServico_Bulk_Porc_Taxa;
                mss.MServico_Bulk_Porc_Imposto = ms.MServico_Bulk_Porc_Imposto;
                mss.MServico_Bulk_Porc_Comissao = ms.MServico_Bulk_Porc_Comissao;
                mss.MServico_Bulk_Porc_ValorAdicional = ms.MServico_Bulk_Porc_ValorAdicional;
                mss.MServico_PorPessoa = ms.MServico_PorPessoa;
                mss.MServico_DataFrom = Convert.ToDateTime(txtPeriodoFromCopiarMServSub);
                mss.MServico_DataTo = Convert.ToDateTime(txtPeriodoToCopiarMServSub);
                mss.MServico_Obs = ms.MServico_Obs;
                mss.S_id = ms.S_id;
                mss.Tipo_Id = ms.Tipo_Id;
                mss.Tipo_categ_id = ms.Tipo_categ_id;
                mss.ItemSubServico_id = ms.ItemSubServico_id;
                mss.SubItem_id = ms.SubItem_id;

                msd.Salvar(mss);

                List<Servico_Completo> sc = scd.ListarTodosTemporada(Convert.ToInt32(ddlPeriodosMServicoSub));

                foreach (Servico_Completo item in sc)
                {

                    Servico_Completo scc = new Servico_Completo();

                    scc.IdMServico = item.IdMServico;
                    scc.IdMServico_Temporada = mss.IdMServico;
                    scc.Base_Index = item.Base_Index;
                    scc.Servicos_Id = item.Servicos_Id;
                    scc.ServicoCompleto_de = item.ServicoCompleto_de;
                    scc.ServicoCompleto_ate = item.ServicoCompleto_ate;
                    scc.SubServico = item.SubServico;

                    scd.Salvar(scc);
                }

                //PreencheTarifaCopiada(mss.IdMServico);

                return Json(new { success = true, message = "Copiado com sucesso.", idMserv = mss.IdMServico }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Periodos Subs

        public ActionResult PopularGridSuppliersSub(int IdServico)
        {
            List<PeriodoSimplesModel> lst = new List<PeriodoSimplesModel>();

            try
            {
                MontaServicoDAL msd = new MontaServicoDAL();

                lst = msd.ListarTodosCondicaoPorSubServico_model(IdServico);

                return PartialView("_lst_PeriodosSub", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_PeriodosSub", lst);
            }
        }

        public ActionResult PopulaGridBaseSub(string txtSupplier, int IdMServico, int index)
        {
            List<GridBasesSubModel> lst = new List<GridBasesSubModel>();
            try
            {
                SupplierDAL sp = new SupplierDAL();
                SBasesDAL bd = new SBasesDAL();
                CambioDAL camd = new CambioDAL();
                SBaseValorDAL sbval = new SBaseValorDAL();

                //IdMServico = 221;
                var indexretorno = 0;

                if (!txtSupplier.Equals(""))
                {
                    Supplier s = sp.FindAllNome(txtSupplier);

                    if (IdMServico == 0)
                    {
                        //Supplier s = sp.FindAllNome(txtSupplier);

                        //ViewState["IdSupplierSubServicoPrincipal"] = s.S_id;

                        lst = bd.ObterPorIdSupplierBaseIndex_model(s.S_id, index);


                        //lblMoedaBaseSubServ.Text = "Moeda: " + bd.ObterUmPorIdSupplierIndex(s.S_id, Convert.ToInt32(rdoBasesIndexSubServico.SelectedValue)).Moeda.Moeda_sigla;
                        //ViewState["IdMoedaBaseSubServico"] = bd.ObterUmPorIdSupplierIndex(s.S_id, Convert.ToInt32(rdoBasesIndexSubServico.SelectedValue)).Moeda.Moeda_id;

                        ViewBag.IdMoedaBaseSubServico = bd.ObterUmPorIdSupplierIndex(s.S_id, index).Moeda.Moeda_id;
                        ViewBag.lblMoedaBaseSubServ = bd.ObterUmPorIdSupplierIndex(s.S_id, index).Moeda.Moeda_sigla;

                        //PopulaGridTemporadasSubServicos(s.S_id, Convert.ToInt32(Session["IdServico"]));
                        //popularDropTemporadasCadastradasSubServ(s.S_id, Convert.ToInt32(Session["IdServico"]));
                        //PopulaGridSubServAdicionados(IdMServico);
                    }
                    else
                    {
                        //Supplier s = sp.FindAllNome(txtSupplier);

                        indexretorno = (int)sbval.ObterPorIdMServ(IdMServico).S_Bases.Base_index;

                        //ViewState["IdSupplierSubServicoPrincipal"] = s.S_id;
                        //popularDropTemporadasCadastradasSubServ(s.S_id, Convert.ToInt32(Session["IdServico"]));

                        lst = bd.ObterPorIdSupplierBase_model(s.S_id, IdMServico, index);

                        ViewBag.IdMoedaBaseSubServico = bd.ObterUmPorIdSupplierIndex(s.S_id, index).Moeda.Moeda_id;
                        ViewBag.lblMoedaBaseSubServ = bd.ObterUmPorIdSupplierIndex(s.S_id, index).Moeda.Moeda_sigla;

                        //lblMoedaBaseSubServ.Text = "Moeda: " + bd.ObterUmPorIdSupplierIndex(s.S_id, Convert.ToInt32(rdoBasesIndexSubServico.SelectedValue)).Moeda.Moeda_sigla;
                        //ViewState["IdMoedaBaseSubServico"] = bd.ObterUmPorIdSupplierIndex(s.S_id, Convert.ToInt32(rdoBasesIndexSubServico.SelectedValue)).Moeda.Moeda_id;

                        //DataBoundGridBase(IdMServico);
                        //PopulaGridSubServAdicionados(IdMServico);
                        //PopulaGridTemporadasSubServicos(s.S_id, Convert.ToInt32(Session["IdServico"]));
                    }

                    TempData["idSupplierSub"] = s.S_id;
                    ViewBag.IdSupplierSubServicoPrincipal = s.S_id;

                }
                else
                {
                    lst = bd.ObterPorIdSupplierBaseIndex_model(0, index);
                    TempData["idSupplierSub"] = 0;
                }


                return PartialView("_lst_GridBasesSub", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_GridBasesSub", lst);
            }
        }

        public ActionResult DataBoundGridBase(int idsupplier, int IdMServico, int index)
        {
            List<GridBasesSubModel> lst = new List<GridBasesSubModel>();
            try
            {
                SupplierDAL sp = new SupplierDAL();
                SBasesDAL bd = new SBasesDAL();
                CambioDAL camd = new CambioDAL();
                SBaseValorDAL sbvd = new SBaseValorDAL();

                lst = bd.ObterPorIdSupplierBase_model(idsupplier, IdMServico, index);
                List<GridBasesSubModel> lstnew = new List<GridBasesSubModel>();

                foreach (var item in lst)
                {
                    S_Bases_Valor sbv = sbvd.ObterPorIds(IdMServico, item.Base_id);

                    GridBasesSubModel it = lst.Where(a => a.Base_id == item.Base_id).SingleOrDefault();
                    it.lblValorBase = Convert.ToString(sbv.S_Bases_Valor_valor);

                    lstnew.Add(it);
                }

                TempData["idSupplierSub"] = idsupplier;
                ViewBag.IdMoedaBaseSubServico = bd.ObterUmPorIdSupplierIndex(idsupplier, index).Moeda.Moeda_id;
                ViewBag.lblMoedaBaseSubServ = bd.ObterUmPorIdSupplierIndex(idsupplier, index).Moeda.Moeda_sigla;
                ViewBag.IdSupplierSubServicoPrincipal = idsupplier;

                return PartialView("_lst_GridBasesSub", lstnew);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_GridBasesSub", lst);
            }
        }

        public ActionResult PopulaGridSubCadastrados(int IdSupplier, int IdServico)
        {
            List<GridCadastradosSubModel> lst = new List<GridCadastradosSubModel>();
            try
            {

                MontaServicoDAL ms = new MontaServicoDAL();

                lst = ms.ListarTodosSubServicoSub_model(IdSupplier, IdServico);

                //if (GridTemporadasSubServicos.Rows.Count > 0)
                //{
                //    btnCopiarTemporadaSubServ.Visible = true;
                //}
                //else
                //{
                //    btnCopiarTemporadaSubServ.Visible = false;
                //}

                return PartialView("_lst_GridSubCadastrados", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_GridSubCadastrados", lst);
            }
        }

        public JsonResult btnNovaTemporadaSubServico(int NovaTemporadaSubServico, string txtDataFromSubServico, string txtDataToSubServico, int IdSupplierSubServicoPrincipal)
        {
            try
            {
                if (NovaTemporadaSubServico == 0)
                {
                    Monta_Servico ms = new Monta_Servico();
                    MontaServicoDAL msd = new MontaServicoDAL();

                    ms.MServico_DataFrom = Convert.ToDateTime(txtDataFromSubServico);
                    ms.MServico_DataTo = Convert.ToDateTime(txtDataToSubServico);
                    ms.S_id = IdSupplierSubServicoPrincipal;

                    msd.Salvar(ms);

                    //btnNovaTemporadaSubServico.Visible = true;
                    //ViewState["NovaTemporadaSubServico"] = 1;
                    //ViewState["IdMontaSubServico"] = ms.IdMServico;

                    //btnCalculaServico.Visible = true;
                    //btnOpenAddSubServicos.Visible = true;
                    //imgRefresh.Visible = true;

                    return Json(new { success = true, message = "Temporada cadastrada com sucesso, adicione os Sub serviços.", NovaTemporadaSubServicoRetorno = 1, IdMontaSubServicoTemporada = ms.IdMServico }, JsonRequestBehavior.AllowGet);
                }



                //else
                //{
                //    ViewState["NovaTemporadaSubServico"] = 0;
                //    txtDataFromSubServico.Text = "";
                //    txtDataToSubServico.Text = "";
                //    //popularDropTipoServico3();
                //    PopulaGridBase(0);
                //    PopulaGridSubServAdicionados(0);

                //    btnCalculaServico.Visible = false;
                //    btnOpenAddSubServicos.Visible = false;
                //    imgRefresh.Visible = false;

                //    MostrarMsg("Preencha os campos para adicionar uma nova temporada.");

                //    return Json(new { success = false, message = "Temporada cadastrada com sucesso, adicione os Sub serviços.", NovaTemporadaSubServicoRetorno = 1, IdMontaSubServico = ms.IdMServico }, JsonRequestBehavior.AllowGet);
                //}


                return Json(new { success = false, message = "erro." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateGridSubs_empty()
        {
            try
            {
                return PartialView("_subservicosAdd");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_subservicosAdd");
            }
        }

        public ActionResult UpdateGridSubs(string spp, string dtFrom, string dtTo, string indexBase)
        {
            try
            {
                MontaServicoDAL ms = new MontaServicoDAL();
                SupplierDAL sd = new SupplierDAL();

                Supplier s = sd.ObterSupplierPorNome(spp);

                DateTime From = Convert.ToDateTime(dtFrom);
                DateTime To = Convert.ToDateTime(dtTo);

                ViewBag.SppPesqSubs = spp;
                ViewBag.IndexSelect = indexBase;

                var lst = ms.ListarTodosCondicaoMServicoSub(s.S_id, From, To, Convert.ToInt32(indexBase));
                ViewBag.GridMServicos = lst;

                return PartialView("_subservicosAdd");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_subservicosAdd");
            }
        }

        public JsonResult EditarMServico_Add(int IdServico, int idMSTemporada, int rdoBasesIndexSubServico, int idMServico)
        {
            try
            {
                ServicoCompletoDAL scd = new ServicoCompletoDAL();
                MontaServicoValoresDAL msd = new MontaServicoValoresDAL();
                Servico_Completo sc = new Servico_Completo();

                if (scd.VerificaMServicoTemp(idMServico, idMSTemporada))
                {
                    return Json(new { success = false, message = "Este Subserviço já foi adicionado a este serviço." }, JsonRequestBehavior.AllowGet);
                }

                sc.IdMServico = idMServico;
                sc.Servicos_Id = IdServico;
                sc.IdMServico_Temporada = idMSTemporada;
                sc.Base_Index = rdoBasesIndexSubServico;

                scd.Salvar(sc);

                return Json(new { success = true, message = "Subserviço adicionado com sucesso." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PopulaGridSubServAdicionados(int IdServico, int IdMServico)
        {
            List<Servico_Completo> lst = new List<Servico_Completo>();
            try
            {

                ServicoCompletoDAL scd = new ServicoCompletoDAL();

                lst = scd.ListarTodosSubServico(IdServico, IdMServico);

                //if (scd.ListarTodosSubServico(Convert.ToInt32(Session["IdServico"]), IdMServico) != null)
                //{
                //    btnCalculaServico.Visible = true;
                //}

                return PartialView("_lst_GridSubServAdicionados", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_GridSubServAdicionados", lst);
            }
        }

        public JsonResult EditarSubServico(int IdMServico)
        {
            try
            {
                MontaServicoDAL msd = new MontaServicoDAL();
                ServicoCompletoDAL scd = new ServicoCompletoDAL();

                Monta_Servico ms = msd.ObterPorId(IdMServico);

                //txtDataFromSubServico.Text = string.Format("{0:dd/MM/yyyy}", ms.MServico_DataFrom);
                //txtDataToSubServico.Text = string.Format("{0:dd/MM/yyyy}", ms.MServico_DataTo);

                //PopulaGridBase(ms.IdMServico);
                //PopulaGridSubServAdicionados(ms.IdMServico);

                //ViewState["IdMontaSubServico"] = ms.IdMServico;

                //btnCalculaServico.Visible = true;
                //btnOpenAddSubServicos.Visible = true;
                //imgRefresh.Visible = true;
                //btnNovaTemporadaSubServico.Visible = true;

                //ViewState["NovaTemporadaSubServico"] = 1;
                //lblMsgCadSubServico.Text = "";

                var fromdata = string.Format("{0:yyyy-MM-dd}", ms.MServico_DataFrom);
                var todata = string.Format("{0:yyyy-MM-dd}", ms.MServico_DataTo);

                return Json(new { success = true, message = "", fromdt = fromdata, todt = todata }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CalculaServico(string str, int idsupplier, int IdMServicoParam, int index, int IdServico, int IdMoedaBaseSubServico, int IdSupplierSubServicoPrincipal)
        {
            try
            {
                string popUpConver = "";
                bool novadatanex = false;
                int indexBase;
                List<S_Bases> listaBases = new List<S_Bases>();

                SBaseValorDAL sbd = new SBaseValorDAL();
                SBasesDAL sbdd = new SBasesDAL();
                SBaseValorDAL sbasedal = new SBaseValorDAL();
                MoedaDAL moedaDal = new MoedaDAL();
                MontaServicoDAL msdal = new MontaServicoDAL();

                List<GridBasesSubModel> lst = sbdd.ObterPorIdSupplierBase_model(idsupplier, IdMServicoParam, index);
                List<GridBasesSubModel> lstnew = new List<GridBasesSubModel>();
                foreach (var item in lst)
                {
                    S_Bases_Valor sbv = sbasedal.ObterPorIds(IdMServicoParam, item.Base_id);
                    GridBasesSubModel it = lst.Where(a => a.Base_id == item.Base_id).SingleOrDefault();
                    it.lblValorBase = Convert.ToString(sbv.S_Bases_Valor_valor);
                    lstnew.Add(it);
                }

                if (lstnew.Count == 0)
                {
                    lstnew = sbdd.ObterPorIdSupplierBaseIndex_model(idsupplier, index);
                }

                foreach (var it in lstnew)
                {
                    S_Bases_Valor sss = sbasedal.ObterPorIds(IdMServicoParam, it.Base_id);
                    if (sss != null)
                    {
                        sbasedal.Excluir(sss);
                    }
                }



                int qtdBases = lstnew.Count; //GridBases.Rows.Count;
                double total = 0;

                ServicoCompletoDAL scd = new ServicoCompletoDAL();

                var lstServComp = str.Split('~');

                Monta_Servico monNovo = msdal.ObterPorId(IdMServicoParam);
                Monta_Servico monNovaData = new Monta_Servico();

                for (int i = 0; i < lstServComp.Length; i++)
                {

                    var item = lstServComp[i].Split('°');

                    //TextBox txtDe = (TextBox)item.FindControl("txtDe");
                    //TextBox txtPara = (TextBox)item.FindControl("txtPara");
                    //Label lblIdSupplier = (Label)item.FindControl("lblIdSupplier");
                    //Label lblIdMServico = (Label)item.FindControl("lblIdMServico");

                    var txtDe = item[0];
                    var txtPara = item[1];
                    var IdServicoCompleto = Convert.ToInt32(item[2]);
                    var lblIdMServico = Convert.ToInt32(item[3]);
                    var lblIdSupplier = Convert.ToInt32(item[4]);

                    Monta_Servico msver = msdal.ObterPorId(lblIdMServico);

                    Servico_Completo scIndex = scd.ObterPorId(IdServicoCompleto);

                    if (monNovo.MServico_DataTo > msver.MServico_DataFrom)
                    {

                        if (scIndex != null)
                        {
                            monNovaData = msdal.ObterPeriodo_NewObs(msver.S_id, (int)msver.ItemSubServico_id,
                                                                    (int)msver.SubItem_id, (DateTime)monNovo.MServico_DataFrom,
                                                                    (DateTime)monNovo.MServico_DataTo, (int)scIndex.Base_Index, msver.MServico_Obs);

                            if (monNovaData == null)
                                novadatanex = true;
                        }

                    }

                    indexBase = Convert.ToInt32(scIndex.Base_Index);

                    if (scIndex != null)
                    {
                        Servico_Completo sc = new Servico_Completo();

                        sc.ServicoCompleto_id = IdServicoCompleto;
                        sc.ServicoCompleto_de = Convert.ToInt32(txtDe);
                        sc.ServicoCompleto_ate = Convert.ToInt32(txtPara);

                        if (monNovaData != null)
                            sc.IdMServico = monNovaData.IdMServico;

                        scd.Atualizar(sc);
                    }
                    else
                    {
                        if (scd.ListarTodosTemporada(IdMServicoParam) == null)
                        {
                            return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
                        }

                    }

                    foreach (var item2 in lstnew)
                    {

                        int DeBase = item2.Base_de;
                        int ParaBase = item2.Base_ate;

                        if ((Convert.ToInt32(txtDe) <= DeBase) && (Convert.ToInt32(txtPara) >= ParaBase) ||
                            (Convert.ToInt32(txtPara) >= DeBase) && (Convert.ToInt32(txtDe) <= ParaBase))
                        {

                            MontaServicoValoresDAL msv = new MontaServicoValoresDAL();
                            S_Bases_Valor sb = new S_Bases_Valor();

                            int IdSupplier = Convert.ToInt32(lblIdSupplier);

                            int IdMServico = 0;

                            if (monNovaData != null)
                                IdMServico = Convert.ToInt32(monNovaData.IdMServico);
                            else
                                IdMServico = Convert.ToInt32(lblIdMServico);

                            listaBases = sbdd.ObterPorIdSupplierDeAte(IdSupplier, Convert.ToInt32(txtDe), Convert.ToInt32(txtPara), indexBase);
                            Boolean FlagMesmoSub = false;

                            foreach (S_Bases bases in listaBases)
                            {
                                qtdBases = qtdBases - 1;


                                if ((bases.Base_de <= DeBase) && (bases.Base_ate >= ParaBase) ||
                                    (bases.Base_de <= ParaBase) && (bases.Base_ate >= DeBase))
                                {

                                    CambioDAL cambDal = new CambioDAL();

                                    Monta_Servico_Valores mmsv = msv.ObterPorIdBase(bases.Base_id, IdMServico);

                                    double valorSub;

                                    if (mmsv == null)
                                        continue;

                                    if (mmsv.S_Bases.Moeda_id == 1)
                                    {

                                    }

                                    if (mmsv.S_Bases.Moeda_id != IdMoedaBaseSubServico)
                                    {
                                        Cambio camb = cambDal.ObterPorIdMoedaPara(mmsv.S_Bases.Moeda_id, IdMoedaBaseSubServico);
                                        double valorSubN = Convert.ToDouble(msv.ObterPorIdBase(bases.Base_id, IdMServico).MServico_Valores_Bulk_Total);

                                        valorSub = valorSubN * Convert.ToDouble(camb.Cambio1);

                                        Moeda moedaDe = moedaDal.ObterPorId(mmsv.S_Bases.Moeda_id);
                                        Moeda moedaPara = moedaDal.ObterPorId(IdMoedaBaseSubServico);

                                        popUpConver = "de " + moedaDe.Moeda_sigla + " para " + moedaPara.Moeda_sigla;

                                        if (mmsv.S_Bases.TipoBase_id == 1)
                                        {
                                            double valorDiv = Convert.ToDouble(DeBase);
                                            valorSub = valorSub / valorDiv;
                                        }
                                    }
                                    else
                                    {
                                        if (mmsv.S_Bases.TipoBase_id == 1)
                                        {
                                            valorSub = Convert.ToDouble(msv.ObterPorIdBase(bases.Base_id, IdMServico).MServico_Valores_Bulk_Total);

                                            double valorDiv = Convert.ToDouble(DeBase);

                                            valorSub = valorSub / valorDiv;

                                        }
                                        else
                                        {
                                            valorSub = Convert.ToDouble(msv.ObterPorIdBase(bases.Base_id, IdMServico).MServico_Valores_Bulk_Total);
                                        }
                                    }

                                    if (FlagMesmoSub)
                                    {
                                        valorSub = 0;
                                    }

                                    S_Bases_Valor s = sbd.ObterPorIds(IdMServicoParam, item2.Base_id); //Convert.ToInt32(item2.Cells[3].Text));

                                    if (s != null)
                                    {
                                        total = (Convert.ToDouble(s.S_Bases_Valor_valor) + valorSub);

                                        sb.S_Bases_Valor_id = s.S_Bases_Valor_id;
                                        sb.S_Bases_Valor_valor = Convert.ToDecimal(total);
                                        sb.Base_id = item2.Base_id;
                                        sb.IdMServico = IdMServicoParam;

                                        sbd.Atualizar(sb);
                                    }
                                    else
                                    {
                                        sb.S_Bases_Valor_valor = Convert.ToDecimal(valorSub);
                                        sb.Base_id = item2.Base_id;
                                        sb.IdMServico = IdMServicoParam;

                                        sbd.Salvar(sb);
                                    }
                                    FlagMesmoSub = true;
                                }
                            }
                        }
                    }

                    List<S_Bases_Valor> listaBaseValor = sbd.ListarTodos(IdMServicoParam);
                    List<S_Bases> listaBaseTotalSupp = sbdd.ObterPorIdSupplierIndex(IdSupplierSubServicoPrincipal, index);

                    if (listaBaseValor.Count() < listaBaseTotalSupp.Count)
                    {
                        var t = listaBaseTotalSupp.Where(l => !listaBaseValor.Select(l1 => l1.Base_id).Contains(l.Base_id));

                        foreach (var tt in t)
                        {
                            S_Bases_Valor sb2 = new S_Bases_Valor();

                            sb2.Base_id = tt.Base_id;
                            sb2.S_Bases_Valor_valor = Convert.ToDecimal(0);
                            sb2.IdMServico = IdMServicoParam;

                            sbd.Salvar(sb2);
                        }
                    }
                }

                //PopulaGridBase(IdMontaSubServico);

                if (popUpConver != "")
                {
                    if (novadatanex)
                        popUpConver = popUpConver + "\n\r Um ou mais subserviços não foram calculados, pois não existe nessa nova data.";

                    return Json(new { success = true, message = "Solicitação realizada com sucesso. Houve conversão na(s) Categoria(s) " + popUpConver, IdMServico = IdMServicoParam }, JsonRequestBehavior.AllowGet);
                }
                else if (novadatanex)
                {
                    return Json(new { success = true, message = "Um ou mais subserviços não foram calculados, pois não existe nessa nova data.", IdMServico = IdMServicoParam }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = true, message = "Solicitação realizada com sucesso.", IdMServico = IdMServicoParam }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvaSubsAdicionados(string str, int idsupplier, int IdMServicoParam, int index, int IdServico, int IdMoedaBaseSubServico, int IdSupplierSubServicoPrincipal)
        {
            try
            {
                
                bool novadatanex = false;
                int indexBase;
                List<S_Bases> listaBases = new List<S_Bases>();

                SBaseValorDAL sbd = new SBaseValorDAL();
                SBasesDAL sbdd = new SBasesDAL();
                SBaseValorDAL sbasedal = new SBaseValorDAL();
                MoedaDAL moedaDal = new MoedaDAL();
                MontaServicoDAL msdal = new MontaServicoDAL();

                List<GridBasesSubModel> lst = sbdd.ObterPorIdSupplierBase_model(idsupplier, IdMServicoParam, index);
                List<GridBasesSubModel> lstnew = new List<GridBasesSubModel>();
                foreach (var item in lst)
                {
                    S_Bases_Valor sbv = sbasedal.ObterPorIds(IdMServicoParam, item.Base_id);
                    GridBasesSubModel it = lst.Where(a => a.Base_id == item.Base_id).SingleOrDefault();
                    it.lblValorBase = Convert.ToString(sbv.S_Bases_Valor_valor);
                    lstnew.Add(it);
                }

                if (lstnew.Count == 0)
                {
                    lstnew = sbdd.ObterPorIdSupplierBaseIndex_model(idsupplier, index);
                }

                foreach (var it in lstnew)
                {
                    S_Bases_Valor sss = sbasedal.ObterPorIds(IdMServicoParam, it.Base_id);
                    if (sss != null)
                    {
                        sbasedal.Excluir(sss);
                    }
                }



                var lstServComp = str.Split('~');

                Monta_Servico monNovo = msdal.ObterPorId(IdMServicoParam);
                Monta_Servico monNovaData = new Monta_Servico();
                ServicoCompletoDAL scd = new ServicoCompletoDAL();

                for (int i = 0; i < lstServComp.Length; i++)
                {

                    var item = lstServComp[i].Split('°');


                    var txtDe = item[0];
                    var txtPara = item[1];
                    var IdServicoCompleto = Convert.ToInt32(item[2]);
                    var lblIdMServico = Convert.ToInt32(item[3]);
                    var lblIdSupplier = Convert.ToInt32(item[4]);

                    Monta_Servico msver = msdal.ObterPorId(lblIdMServico);

                    Servico_Completo scIndex = scd.ObterPorId(IdServicoCompleto);

                    //if (monNovo.MServico_DataTo > msver.MServico_DataFrom)
                    //{

                    //    if (scIndex != null)
                    //    {
                    //        monNovaData = msdal.ObterPeriodo_NewObs(msver.S_id, (int)msver.ItemSubServico_id,
                    //                                                (int)msver.SubItem_id, (DateTime)monNovo.MServico_DataFrom,
                    //                                                (DateTime)monNovo.MServico_DataTo, (int)scIndex.Base_Index, msver.MServico_Obs);

                    //        if (monNovaData == null)
                    //            novadatanex = true;
                    //    }

                    //}

                    indexBase = Convert.ToInt32(scIndex.Base_Index);

                    if (scIndex != null)
                    {
                        Servico_Completo sc = new Servico_Completo();

                        sc.ServicoCompleto_id = IdServicoCompleto;
                        sc.ServicoCompleto_de = Convert.ToInt32(txtDe);
                        sc.ServicoCompleto_ate = Convert.ToInt32(txtPara);

                        //if (monNovaData != null)
                        //    sc.IdMServico = monNovaData.IdMServico;

                        scd.Atualizar(sc);
                    }
                    else
                    {
                        if (scd.ListarTodosTemporada(IdMServicoParam) == null)
                        {
                            return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
                        }

                    }

                }



                    return Json(new { success = true, message = "Solicitação realizada com sucesso.", IdMServico = IdMServicoParam }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirSubServico(int IdMServico)
        {
            try
            {
                ServicoCompletoDAL scd = new ServicoCompletoDAL();
                MontaServicoDAL msd = new MontaServicoDAL();
                SBaseValorDAL sbv = new SBaseValorDAL();


                foreach (Servico_Completo item in scd.ListarTodosTemporada(IdMServico))
                {
                    scd.Excluir(item);
                }

                foreach (S_Bases_Valor item in sbv.ListarTodos(IdMServico))
                {
                    sbv.Excluir(item);
                }

                Monta_Servico ms = msd.ObterPorId(IdMServico);

                msd.Excluir(ms);

                return Json(new { success = true, message = "Excluído com sucesso." }, JsonRequestBehavior.AllowGet);

                //PopulaGridTemporadasSubServicos(Convert.ToInt32(ViewState["IdSupplierSubServicoPrincipal"]), Convert.ToInt32(Session["IdServico"]));

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirServCompleto(int IdMServico, int IdMServico_Temporada, int Base_Index)
        {
            try
            {

                ServicoCompletoDAL scd = new ServicoCompletoDAL();
                Servico_Completo sc = scd.ObterPorIdMServicoTemp(IdMServico, IdMServico_Temporada, Base_Index);
                scd.Excluir(sc);

                //PopulaGridSubServAdicionados(IdMServico_Temporada);
                //CalculaServico();

                //MostrarMsg("Registro excluído com sucesso.");

                return Json(new { success = true, message = "Excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Servicos Por Supplier

        public ActionResult PopulaGridServPorSupp(string txtSupplier)
        {
            List<GridServPorSuppModel> lst = new List<GridServPorSuppModel>();
            try
            {
                ServicoCompletoDAL scd = new ServicoCompletoDAL();

                Supplier sp = new SupplierDAL().ObterSupplierPorNome(txtSupplier);

                lst = scd.ListarTodosServPorSupplier_model(sp.S_id);

                return PartialView("_lst_GridServPorSupp", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_GridServPorSupp", lst);
            }
        }

        public ActionResult btnReportFiltroSupplierServPorSupp(string txtSupplier)
        {


            string NomeEmpresa = ConfigurationManager.AppSettings["NomeEmpresa"];
            string EnderecoEmpresa = ConfigurationManager.AppSettings["EnderecoEmpresa"];
            string ContatoEmpresa = ConfigurationManager.AppSettings["ContatoEmpresa"];

            Supplier sp = new SupplierDAL().ObterSupplierPorNome(txtSupplier);

            DSFiltroSupplierServico ds = new DSFiltroSupplierServico();

            DataTable dt = ds.Tables["FILTROSUPPLIER"];

            var pars = ConfigurationManager.ConnectionStrings["Conexao"].ToString();
            SqlConnection Con = new SqlConnection(pars);
            string sql;

            Con.Open();

            sql = "SELECT S_Servicos.Servicos_Nome, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, S_Bases.Base_de, S_Bases.Base_ate, ";
            sql = sql + "Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_Mercado_Tarifa_Status.S_mercado_tarifa_status_nome, Monta_Servico.S_id ";
            sql = sql + "FROM S_Servicos INNER JOIN Servico_Completo ON S_Servicos.Servicos_Id = Servico_Completo.Servicos_Id INNER JOIN Monta_Servico ON ";
            sql = sql + "Servico_Completo.IdMServico = Monta_Servico.IdMServico INNER JOIN Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN ";
            sql = sql + "S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id LEFT OUTER JOIN S_Mercado_Tarifa_Status ON Monta_Servico_Valores.S_mercado_tarifa_status_id = S_Mercado_Tarifa_Status.S_mercado_tarifa_status_id ";
            sql = sql + "WHERE Monta_Servico.S_id = " + Convert.ToInt32(sp.S_id);

            SqlCommand cmd = new SqlCommand(sql, Con);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DataRow dr = dt.NewRow();

                dr["Servico_nome"] = reader["Servicos_Nome"].ToString();
                dr["DataFrom"] = Convert.ToDateTime(reader["MServico_DataFrom"]);
                dr["DataTo"] = Convert.ToDateTime(reader["MServico_DataTo"]);
                dr["Base_de"] = Convert.ToInt32(reader["Base_de"]);
                dr["Base_ate"] = Convert.ToInt32(reader["Base_ate"]);
                dr["BulkTotal"] = reader["MServico_Valores_Bulk_Total"];
                dr["StatusNome"] = reader["S_mercado_tarifa_status_nome"].ToString();

                dt.Rows.Add(dr);
            }

            RptFiltroSupplierServico rpt = new RptFiltroSupplierServico();

            CrystalDecisions.CrystalReports.Engine.TextObject ObjNomeEmpresa = rpt.ReportDefinition.Sections[1].ReportObjects["NomeEmpresa"] as TextObject;
            CrystalDecisions.CrystalReports.Engine.TextObject ObjEnderecoEmpresa = rpt.ReportDefinition.Sections[1].ReportObjects["EnderecoEmpresa"] as TextObject;
            CrystalDecisions.CrystalReports.Engine.TextObject ObjContatoEmpresa = rpt.ReportDefinition.Sections[1].ReportObjects["ContatoEmpresa"] as TextObject;
            ObjNomeEmpresa.Text = NomeEmpresa;
            ObjEnderecoEmpresa.Text = EnderecoEmpresa;
            ObjContatoEmpresa.Text = ContatoEmpresa;

            rpt.SetDataSource(dt);

            Con.Close();

            string path = pathSaveReport("Serviços por supplier");
            string url = urlOpenReport("Serviços por supplier");


            rpt.ExportToDisk(ExportFormatType.PortableDocFormat, path + ".pdf");
            url = url + ".pdf";

            return Json(new { success = true, url }, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Fotos

        [HttpPost]
        public ActionResult Upload(Picture picture)
        {
            CriarPastaServico(picture.idServico);

            FotoDAL fDal = new FotoDAL();

            foreach (var file in picture.Files)
            {
                if (file.ContentLength > 0)
                {
                    string fotoString = Convert.ToString(Guid.NewGuid().ToString("n") + ".jpg");
                    file.SaveAs(Server.MapPath("~/Galeria/Servicos/") + picture.idServico + "/" + fotoString);

                    SalvarFoto(fotoString, picture.idServico);
                }
            }

            FotoServicoDAL f = new FotoServicoDAL();
            f.ImagemPrincipal(picture.idServico);

            var serv = new ServicoDAL().ObterPorId(picture.idServico);
            ViewBag.retornofoto = serv.Servicos_Nome;
            ViewBag.retornofotocidade = serv.Cidade.CID_nome;

            ViewBag.message = "Fotos enviadas com sucesso.";

            StartServico();
            return View("~/Views/Servicos/Servicos.cshtml");
            //return View("Servicos");
        }

        protected string CriarPastaServico(int IdServico)
        {
            string path = ("~/Galeria/Servicos/" + IdServico);
            try
            {
                if (!Directory.Exists(Server.MapPath(path)))
                {
                    Directory.CreateDirectory(Server.MapPath(path));
                }

                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public bool SalvarFoto(string ImgNome, int idServico)
        {
            Servicos_Imagem s = new Servicos_Imagem();

            try
            {
                s.Serv_img_capa = "N";
                s.Serv_img_nome = ImgNome;
                s.Serv_img_hupload = Convert.ToString(DateTime.Now);
                s.Servicos_Id = idServico;

                FotoServicoDAL f = new FotoServicoDAL();

                f.Salvar(s);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public ActionResult CarregarFotos(int idServico)
        {
            List<Servicos_Imagem> lst = new List<Servicos_Imagem>();
            try
            {
                FotoServicoDAL f = new FotoServicoDAL();

                lst = f.ListarTodos_model(idServico);

                return PartialView("_fotos", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_fotos", lst);
            }
        }

        public JsonResult ExcluirFoto(int Serv_img_id, int IdServico)
        {
            FotoServicoDAL f = new FotoServicoDAL();
            try
            {

                Servicos_Imagem si = f.ObterPorId(Convert.ToInt32(Serv_img_id));

                if (!ExcluiFotoPasta(si.Serv_img_nome, IdServico))
                {
                    return Json(new { success = false, message = "Não foi possível deletar a foto da pasta galeria, tente novamente." }, JsonRequestBehavior.AllowGet);
                }

                f.Excluir(si);

                f.ImagemPrincipal(IdServico);

                return Json(new { success = true, message = "Foto excluída com sucesso." }, JsonRequestBehavior.AllowGet);

                //CarregarListFotos(sender, e);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool ExcluiFotoPasta(string foto, int IdServico)
        {
            try
            {
                string path = "~/Galeria/Servicos/" + IdServico + "/" + foto;
                System.IO.File.Delete(Server.MapPath(path));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public JsonResult CapaFoto(int IdServico, int idImg)
        {
            try
            {
                FotoServicoDAL f = new FotoServicoDAL();

                f.AddImagemPrincipal(IdServico, idImg);

                return Json(new { success = true, message = "Foto principal atualizada com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Holidays

        public ActionResult PopulaHolidays(int IdServico)
        {
            try
            {
                HolidaysDAL hd = new HolidaysDAL();

                Holidays h = hd.ObterPorIdServ(IdServico);

                if (h == null)
                {
                    Holidays hy = new Holidays();

                    hy.Servicos_Id = IdServico;
                    hy.Holidays_seg = false;
                    hy.Holidays_ter = false;
                    hy.Holidays_qua = false;
                    hy.Holidays_qui = false;
                    hy.Holidays_sex = false;
                    hy.Holidays_sab = false;
                    hy.Holidays_dom = false;

                    hd.Salvar(hy);

                    h = hy;

                    //PopulaGridHolidaysDates(hy.Holidays_id);
                    //PopulaGridHolidaysPeriodos(hy.Holidays_id);
                }

                return PartialView("_holidays", h);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_holidays", null);
            }
        }

        public JsonResult btnSalvarDate(string txtdataFooter, int IdHolidays)
        {
            try
            {
                HolidaysDatesDAL hdd = new HolidaysDatesDAL();

                Holidays_Dates hd = new Holidays_Dates();

                hd.Holidays_Dates_data = Convert.ToDateTime(txtdataFooter);
                hd.Holidays_id = IdHolidays;
                hdd.Salvar(hd);

                return Json(new { success = true, message = "Adicionado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult btnSalvarPeriodosHolidays(string txtdataFromFooter, string txtdataToFooter, int IdHolidays)
        {
            try
            {
                HolidaysPeriodosDAL hdd = new HolidaysPeriodosDAL();

                Holidays_Periodo hd = new Holidays_Periodo();

                hd.HoliDays_Periodo_from = Convert.ToDateTime(txtdataFromFooter);
                hd.HoliDays_Periodo_to = Convert.ToDateTime(txtdataToFooter);
                hd.Holidays_id = IdHolidays;

                hdd.Salvar(hd);

                return Json(new { success = true, message = "Adicionado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult btnSalvarHolidays(int IdHolidays, bool chkSeg, bool chkTer, bool chkQua, bool chkQui, bool chkSex, bool chkSab, bool chkDom)
        {
            try
            {
                Holidays h = new Holidays();

                h.Holidays_id = IdHolidays;
                h.Holidays_seg = chkSeg;
                h.Holidays_ter = chkTer;
                h.Holidays_qua = chkQua;
                h.Holidays_qui = chkQui;
                h.Holidays_sex = chkSex;
                h.Holidays_sab = chkSab;
                h.Holidays_dom = chkDom;

                HolidaysDAL hd = new HolidaysDAL();

                hd.Atualizar(h);

                return Json(new { success = true, message = "Registro atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult btnSalvarHolidaysRemarks(int IdHolidays, string remarks)
        {
            try
            {
                HolidaysDAL hd = new HolidaysDAL();

                Holidays h = new Holidays();

                h.Holidays_id = IdHolidays;
                h.Holidays_remarks = remarks;

                hd.AtualizarRemakrs(h);

                return Json(new { success = true, message = "Registro atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirHolidaysDates(int Holidays_Dates_id)
        {
            try
            {

                HolidaysDatesDAL hdd = new HolidaysDatesDAL();

                Holidays_Dates hd = hdd.ObterPorId(Holidays_Dates_id);

                hdd.Excluir(hd);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExluirHolidaysPeriodo(int HoliDays_Periodo_id)
        {
            try
            {

                HolidaysPeriodosDAL hdd = new HolidaysPeriodosDAL();

                Holidays_Periodo hd = hdd.ObterPorId(HoliDays_Periodo_id);

                hdd.Excluir(hd);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Info Servicos

        public class servicoInfo
        {
            public string lblNomeServico { get; set; }
            public string lblCidade { get; set; }
            public string lblPais { get; set; }
            public string lblInfoLong { get; set; }
            public string lblInfoShort { get; set; }
            public string FotoPrincipal { get; set; }
        }

        public ActionResult InfoServicos(int idServico)
        {
            FotoServicoDAL f = new FotoServicoDAL();

            ViewBag.servicoDataList = f.ListarTodos_model(idServico);

            ServicoDAL s = new ServicoDAL();

            S_Servicos ss = s.ObterPorId(idServico);

            servicoInfo md = new servicoInfo();

            md.lblNomeServico = ss.Servicos_Nome;
            md.lblCidade = ss.Cidade.CID_nome;
            md.lblPais = ss.Pais.PAIS_nome;
            md.lblInfoLong = ss.Servicos_descr;
            md.lblInfoShort = ss.Servicos_descrCurt;
         
            var foto = new FotoServicoDAL().ObterImgPrincipal(ss.Servicos_Id);

            if (foto != null)
            {
                md.FotoPrincipal = "../Galeria/Servicos/" + ss.Servicos_Id + "/" + f.ObterImgPrincipal(ss.Servicos_Id).Serv_img_nome;
            }
            else
            {
                md.FotoPrincipal = "../images/semimagem.gif";
            }

            ViewBag.infos = md;

            return View();
        }

        #endregion

    }
}