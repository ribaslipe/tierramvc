﻿using DAL.Entity;
using DAL.Models;
using DAL.Persistencia;
using EvoPdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Site.Controllers
{
    public class OfferController : Controller
    {
        // GET: Offer
        public ActionResult OfferEdit(int IdMoedaQuote, string qCode, int type, int ratear, int foto, int optQuote)
        {
            PreencheTela(IdMoedaQuote, qCode, type, ratear, foto, optQuote);
            return View();
        }

        protected void PreencheTela(int IdMoedaQuote, string qCode, int type, int ratear, int foto, int optQuote)
        {

            PreencheOfertaGeral(IdMoedaQuote, qCode, type, ratear, foto, optQuote);

            TBItineraryDAL tbdDal = new TBItineraryDAL();
            TBItinerary_HighlightsDAL tbDal = new TBItinerary_HighlightsDAL();
            CidadeDAL cdal = new CidadeDAL();

            Quotation _quotation = new QuotationDAL().ObterPorCode(qCode);
            Quotation_Grupo _qgrupo = new QuotationGrupoDAL().ObterPorIdQuotation(_quotation.Quotation_Id);

            FileTarifasDAL ftrDal = new FileTarifasDAL();
            FileTransfersDAL ftrsDal = new FileTransfersDAL();
            FileServExtraDAL fsextDal = new FileServExtraDAL();
            MealDAL mealdal = new MealDAL();

            List<TBItineraryDAL.CidadeHigh> cids = tbdDal.ListarTodosCidadesComHotels_(qCode, optQuote);
            foreach (var item in cids)
            {
                Cidade cid = cdal.ObterPorNome(item.Cidade);
                var lstvis = tbDal.ListarPorCidade_False(cid.CID_id, qCode, optQuote);
                if (lstvis.Count > 0)
                {
                    cids = cids.Where(s => s.Cidade != cid.CID_nome).ToList();
                }
            }
            foreach (var item in cids)
            {
                PreencheHightLightCity(item.Cidade, qCode, optQuote);
            }
            List<TBItinerary_Highlights> lstInto = tbDal.ListarTodos(qCode, optQuote);

            ViewBag.hightlights = cids;
            ViewBag.hightlightsInto = lstInto;

            ViewBag.lcidades = tbdDal.ListarTodosCidadesComHotels_String(qCode);

            ViewBag.qcode = qCode;
            ViewBag.optquote = optQuote;

            #region includes            

            int FileID = new FileCarrinhoDAL().RetornaFileObj(qCode).File_id;

            //FileTransfersDAL ftrDal = new FileTransfersDAL();
            Including_HighLightsDAL ihd = new Including_HighLightsDAL();

            List<Including_HighLights> lstInc = ihd.ListarPQuotation(qCode, optQuote);
            List<Including_HighLights> orderedList = new List<Including_HighLights>();

            var lstft = ftrDal.ListarTodosOpt(FileID, optQuote);
            int days = Convert.ToInt32(ftrsDal.MontaCarrinhoNewFile(FileID, optQuote)) + 1;

            if (lstInc.Count < 3)
            {
                ihd.ExcluirTodosCode(qCode, optQuote);

                ihd.InsertDefault_New(qCode, optQuote, days);
                orderedList = ihd.ListarPQuotation(qCode, optQuote);
            }
            else
            {
                orderedList = lstInc;
            }
            ViewBag.hightlightsIncluding = orderedList;
            #region comment            
            //if (lstInc.Count > 0)
            //{
            //    if (lstInc.Count(a => a.isNumDays == true) > 0)
            //    {
            //        orderedList.Add(lstInc.Where(a => a.isNumDays == true).Single());
            //    }
            //    else
            //    {
            //        orderedList.Add(new Including_HighLights()
            //        {
            //            ID = -1,
            //            Descricao = string.Format("{0} days / {1} nights", days, (days - 1))
            //        });
            //    }

            //    if (lstInc.Count(a => a.isNumTransfers == true) > 0)
            //    {
            //        orderedList.Add(lstInc.Where(a => a.isNumTransfers == true).Single());
            //    }
            //    else
            //    {
            //        orderedList.Add(new Including_HighLights()
            //        {
            //            ID = -2,
            //            Descricao = "Transfers in private with English-speaking guide(unless noted)"
            //        });
            //    }

            //    orderedList.AddRange(lstInc.Where(a => !(a.isNumDays == true) && !(a.isNumTransfers == true)).ToList());
            //}
            //else
            //{
            //    //ihd.InsertDefault(qCode, optQuote);
            //    //orderedList = ihd.ListarPQuotation(qCode, optQuote);
            //}
            #endregion

            #endregion

            #region not includes

            Discretion_HighLightsDAL dhd = new Discretion_HighLightsDAL();
            List<Discretion_HighLights> lstDis = dhd.ListarPQuotation(qCode, optQuote);

            if (lstDis.Count == 0)
            {
                dhd.InsertDefaults(qCode, optQuote);
                lstDis = dhd.ListarPQuotation(qCode, optQuote);
            }

            ViewBag.hightlightsDiscretion = lstDis;

            #endregion

            #region notes

            TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
            List<TBItinerary_Info> lstIti = tbinfod.ObterPorQuote(qCode, optQuote);
            if (lstIti.Count > 0 && lstIti.Where(s => s.Note != "" && s.Note != null).Count() > 0)
            {
                //ViewBag.EditorNotes = lstIti[0].Note;

                ViewBag.EditorNotes = lstIti.Where(s => s.Note != "" && s.Note != null).First().Note;
            }
            else
            {
                string str = "Thank you for choosing SouthAmerica.travel!<br><br>  You can view your itinerary by clicking on the \"full itinerary\" tab or by viewing the PDF version.  Our itineraries are completely customizable and can be changed to match your exact travel dates, hotel preferences, tour needs, etc.<br><br>";
                str = str + "Please check all entry requirements for each country you will visit based on your citizenship. You must have 6 months validity on your passport.<br><br>";
                str = str + "International and domestic flights are priced separately and your quote (per person) can be found under the \"flights\" tab.<br><br>";
                str = str + "I look forward to discussing the itinerary with you!";

                ViewBag.EditorNotes = str;
                SaveNote(qCode, optQuote, str);
            }

            #endregion

            #region images

            TBItinerary_HighlightsImages thi = new TBItinerary_HighlightsImagesDAL().ObterPorCode(qCode, optQuote);
            if (thi != null)
            {
                if (thi.FirstImage != null)
                    ViewBag.hgImg1 = "../../Galeria/TBItinerary/" + thi.FirstImage;
                else
                {
                    ViewBag.hgImg1 = "../../Galeria/noimage.jpg";
                }

                if (thi.LastImage != null)
                    ViewBag.hgImg2 = "../../Galeria/TBItinerary/" + thi.LastImage;
                else
                {
                    ViewBag.hgImg2 = "../../Galeria/noimage.jpg";
                }
            }
            else
            {
                var fileid = new FileCarrinhoDAL().ObterPorIdGrupo(_qgrupo.Quotation_Grupo_Id).File_id;
                var pars = insereFoto(fileid, optQuote, qCode);

                if (pars != "")
                {
                    ViewBag.hgImg1 = pars.Split('°')[0];
                    ViewBag.hgImg2 = pars.Split('°')[1];
                }
            }

            #endregion

            #region itinerary

            TBItinerary_SupplierDAL tbSpDal = new TBItinerary_SupplierDAL();

            List<itineraryModel> newitineraryEdit = new List<itineraryModel>();

            var itineraryEdit = tbdDal.ListarItinerary(qCode, optQuote);
            double dia = 1;
            DateTime dtnow = new DateTime();
            DateTime dtPrimeira = new DateTime();

            itineraryModel itemAnterior = null;

            itineraryModel itemNextN = null;
            itineraryModel itemNext = new itineraryModel();

            int countL = 0;

            foreach (var item in itineraryEdit)
            {

                if (item.titulo.Contains("Hotel Windsor Leme"))
                {

                }

                if (itemAnterior != null)
                {
                    itemAnterior = null;
                    countL++;
                    continue;
                }

                if (itemNextN != null && item.Flag.Equals("HOTEL"))
                {
                    itemNextN = null;
                    countL++;
                    continue;
                }

                itineraryModel it = new itineraryModel();
                it.cidade = item.cidade;
                it.dtfrom = (DateTime)item.dtfrom;
                it.dtTo = (DateTime)item.dtTo;
                it.Code = item.Code;
                it.idTabela = item.idTabela;
                it.optquote = item.optquote;
                it.Sid = item.Sid;
                it.tbItineraryID = item.tbItineraryID;
                it.NomePacote = item.NomePacote;

                if (item.NomePacote != null)
                {
                    it.DescPacote = retornaPacote(item.dtfrom, item.dtTo, item.Sid, item.NomePacote, item.idTabela, item.optquote, item.Code);
                }

                int tot = countL + 1;
                if (itineraryEdit.Count() != tot)
                    itemNext = itineraryEdit[tot];


                if (item.MealNome == itemNext.MealNome && item.Flag == itemNext.Flag && item.Sid == itemNext.Sid && item.dtfrom == itemNext.dtfrom && item.categoria == itemNext.categoria && item.titulo == itemNext.titulo)
                {
                    if (item.room == null)
                    {
                        if (itemNext.room != null)
                        {
                            it.titulo = string.Format("{0} {1} & {2} {3}", item.titulo, item.room, itemNext.room, item.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            if (item.Flag.Equals("MEAL"))
                            {
                                it.titulo = string.Format("{0} {1}", item.titulo, item.MealNome.Replace("INCL", ""));
                                itemAnterior = itemNext;
                            }
                            else
                            {
                                it.titulo = string.Format("{0} {1}", item.titulo, item.room);
                                itemAnterior = itemNext;
                            }
                        }
                    }
                    else
                    {

                        if (tot + 1 < itineraryEdit.Count)
                            itemNextN = itineraryEdit[tot + 1];

                        if (itemNextN != null && item.Flag == itemNextN.Flag && item.Sid == itemNextN.Sid && item.dtfrom == itemNextN.dtfrom && item.titulo == itemNextN.titulo)
                        {
                            it.titulo = string.Format("{0} - {1} - {2}", item.titulo, item.room, item.categoria + " & " + itemNext.categoria + " & " + itemNextN.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} - {1} & {2} - {3}", item.titulo, item.room, itemNext.room, item.categoria);
                            itemAnterior = itemNext;
                        }
                    }
                }
                else
                {
                    if (item.room == null)
                    {
                        if (item.Flag.Equals("MEAL"))
                        {
                            it.titulo = string.Format("{0} {1}", item.titulo, item.MealNome.Replace("INCL", ""));
                            itemAnterior = null;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} {1} {2}", item.titulo, item.room, item.categoria);
                            itemAnterior = null;
                        }
                    }
                    else
                    {

                        if (item.Flag == itemNext.Flag && item.Sid == itemNext.Sid && item.dtfrom == itemNext.dtfrom && item.titulo == itemNext.titulo)
                        {
                            it.titulo = string.Format("{0} - {1} & {2}", item.titulo, item.room + " " + item.categoria, itemNext.room + " " + itemNext.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} - {1} - {2}", item.titulo, item.room, item.categoria);
                            itemAnterior = null;
                        }
                    }
                }



                if (item.Flag.Equals("HOTEL") || item.Flag.Equals("MEAL"))
                {
                    var ft = ftrDal.ObterPorId(item.idTabela);


                    it.nights = (item.dtTo - item.dtfrom).TotalDays.ToString();


                    var mealS = mealdal.ObterPorNome(ft.S_meal_nome);
                    string nomeNight = "";

                    if (mealS != null)
                    {
                        if (mealS.S_meal_breakfast == true)
                        {
                            nomeNight = " Days";
                            it.nights = (Convert.ToInt32(it.nights) + 1).ToString();
                        }
                        else
                        {
                            nomeNight = " Nights";
                        }
                    }
                    else
                    {
                        nomeNight = " Nights";
                    }

                    if (Convert.ToInt32(it.nights) > 1)
                    {
                        it.nights = it.nights + nomeNight;
                    }
                    else
                    {
                        if (Convert.ToInt32(it.nights) == 1)
                        {
                            it.nights = it.nights + nomeNight;
                        }
                        else
                        {
                            it.nights = "";
                        }
                    }

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);

                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name;  //" <span style=\"color:#ff0000;\">Optional</span>";
                            it.optional = true;
                        }
                        else
                        {
                            it.titulo = tSp.S_Name;
                        }
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                        {
                            it.titulo = it.titulo;
                        }
                    }
                }
                else if (item.Flag.Equals("EXTRA"))
                {
                    var ft = fsextDal.ObterPorId(item.idTabela);

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = tSp.S_Name;

                        //it.titulo = tSp.S_Name;
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = it.titulo;

                        //it.titulo = it.titulo;
                    }

                }
                else
                {
                    var ft = ftrsDal.ObterPorId(item.idTabela);

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = tSp.S_Name;

                        //it.titulo = tSp.S_Name;
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = it.titulo;

                        //it.titulo = it.titulo;
                    }

                    //if (tSp != null)
                    //{
                    //    it.titulo = tSp.S_Name;
                    //}
                    //else
                    //{
                    //    it.titulo = it.titulo;
                    //}
                }

                it.Flag = item.Flag;
                it.ordem = item.ordem;
                it.categoria = item.categoria;
                it.room = item.room;

                it.urlIcon = retornaUrlIcone(item.Flag, item.Sid, item.titulo, item.idTabela);

                if (dtnow != item.dtfrom && dtnow.Year != 1)
                    dia = (item.dtfrom - dtPrimeira).TotalDays + 1;

                //dia = (item.dtfrom - dtnow).TotalDays;

                it.descricao = retornaDescricao(item.Flag, item.Sid, item.idTabela, qCode, optQuote, item.titulo, item.cidade);

                if (dtnow.Year == 1)
                    dtPrimeira = item.dtfrom;


                it.urlFoto = retornaUrlFoto(item.idTabela, item.Code, item.optquote, item.Flag, item.Sid);


                dtnow = it.dtfrom;
                it.dia = Convert.ToInt32(dia);

                countL++;

                newitineraryEdit.Add(it);
            }

            ViewData["itineraryEdit"] = newitineraryEdit;

            #endregion

            #region prices

            ViewBag.prices = montaPrices(qCode, optQuote, ratear);

            TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();
            var tbp = tbpd.ObterPorQuote(qCode, optQuote);

            if (tbp.Count > 0)
            {
                if (tbp.First().Preco_Link != null && tbp.First().Preco_Link != "." && tbp.First().Preco_Link != "")
                {
                    ViewBag.priceLink = tbp.First().Preco_Link;
                }
                if (tbp.First().Preco_Notes != null && tbp.First().Preco_Notes != "." && tbp.First().Preco_Notes != "")
                {
                    ViewBag.priceNotes = tbp.First().Preco_Notes;
                }
            }

            #endregion

            #region hoteis

            ViewData["hoteisEdit"] = HotelsPorCodigo(qCode, optQuote);

            #endregion

            #region flights

            ViewBag.flights = CarregaFlights(qCode, optQuote);

            #endregion

        }

        protected bool checkIfExists(string highlight, string qCode, int optQuote)
        {
            return new TBItinerary_HighlightsDAL().CheckExists(highlight, qCode, optQuote);
        }

        protected void PreencheOfertaGeral(int IdMoedaQuote, string qCode, int type, int ratear, int foto, int optQuote)
        {
            TBItineraryDAL tbid = new TBItineraryDAL();
            List<TBItinerary> listaTB = tbid.ListarTodosCodeOpt(qCode, optQuote);

            if (listaTB.Count() > 0)
                foreach (TBItinerary liTB in listaTB) tbid.Excluir(liTB);


            CidadeDAL cd = new CidadeDAL();
            SupplierDAL spd = new SupplierDAL();
            FileTarifasDAL ftd = new FileTarifasDAL();
            FileTransfersDAL fts = new FileTransfersDAL();
            FileServExtraDAL fsed = new FileServExtraDAL();
            FileCarrinhoDAL fcd = new FileCarrinhoDAL();
            TarifaDAL tard = new TarifaDAL();
            MontaServicoDAL msd = new MontaServicoDAL();
            MoedaDAL mod = new MoedaDAL();
            CambioDAL cambDal = new CambioDAL();
            SBaseValorDAL sbaseV = new SBaseValorDAL();
            ServicoDAL servDal = new ServicoDAL();
            RangeDal rd = new RangeDal();
            Moeda moedaQ = mod.ObterPorId(IdMoedaQuote);

            Quotation _quotation = new QuotationDAL().ObterPorCode(qCode);
            Quotation_Grupo _qgrupo = new QuotationGrupoDAL().ObterPorIdQuotation(_quotation.Quotation_Id);

            int IdFile = fcd.ObterPorIdGrupo(_qgrupo.Quotation_Grupo_Id).File_id;
            int IdQuote = fcd.RetornaQuote(IdFile);
            int OptQuote = optQuote;
            int i = 0;
            var qgp = new QuotationGrupoDAL().ObterPorIdQuotation(IdQuote);
            decimal soma = 0;
            int qtdBases = rd.QtdBases(IdQuote, OptQuote);
            string[] bases = new string[qtdBases];

            foreach (string item in rd.RetornaTodasBases(IdQuote, OptQuote))
            {
                bases[i] = item;
                i++;
            }

            i = 0;

            string[] basesT = bases[i].ToString().Split('/');
            string baseDe = basesT[0];
            string baseAte = basesT[1];
            //string ratear = ratear;



            foreach (Ranges item in rd.ObterPorTarifaDeAteOpt(IdQuote, baseDe, baseAte, OptQuote))
            {
                switch (item.Flag)
                {
                    case "hotel":
                        File_Tarifas ft = ftd.ObterPorId(Convert.ToInt32(item.FileTabelaId));
                        TBItinerary tbi = new TBItinerary();

                        tbi.OptQuote = OptQuote;
                        tbi.Code = qCode;
                        tbi.DateFrom = ft.Data_From;
                        tbi.DateTo = ft.Data_To;
                        tbi.SuppNome = ft.S_nome;
                        tbi.TC_Rateado = ratear.Equals("0") ? true : false;

                        if (Convert.ToBoolean(ft.Meal))
                        {
                            tbi.Flag = "MEAL";

                            if (ft.S_meal_status != null)
                                tbi.MealNome = ft.S_meal_nome + " " + ft.S_meal_status;
                            else
                                tbi.MealNome = ft.S_meal_nome;
                        }
                        else
                        {
                            tbi.Room = ft.Room;
                            tbi.Categoria = ft.Categoria;
                            tbi.Flag = "HOTEL";
                            tbi.NomePacote = ft.NomePacote;
                        }
                        tbi.S_id = ft.S_id;
                        tbi.idTabela = ft.File_Tarifas_id;
                        tbi.Quotation_Code = qCode;
                        tbi.Ordem = ft.Ordem;

                        Supplier sp = spd.ObterPorId(Convert.ToInt32(ft.S_id));
                        tbi.Cidade = sp.Cidade.CID_nome;

                        if (ft.Flights_id != null)
                        {
                            FileFlightsDAL ffd = new FileFlightsDAL();
                            File_Flights ff = ffd.ObterPorId(Convert.ToInt32(ft.Flights_id));
                            tbi.Flight_voo = ff.Flights_nome;
                            tbi.Flight_hora = ff.Flights_hora;
                            tbi.Flight_pickUp = ff.Flights_pickUp;
                        }

                        tbid.Salvar(tbi);

                        foreach (Ranges rangesHoteis in rd.ObterPorTarifaDeAteOptItensOrdem(IdQuote, OptQuote, ft.File_Tarifas_id, "hotel"))
                        {
                            tbid.Atualizar(RetornaRange(i), RetornaBase(i), Convert.ToDecimal(rangesHoteis.ValorTotal), rangesHoteis.Ranges_de + "/" + rangesHoteis.Ranges_ate, tbi.Itinerary_id);
                            i++;
                        }

                        i = 0;

                        break;

                    case "servico":
                        File_Transfers ftr = fts.ObterPorId(Convert.ToInt32(item.FileTabelaId));
                        TBItinerary tbii = new TBItinerary();

                        tbii.OptQuote = OptQuote;
                        tbii.Code = qCode;
                        tbii.ServNome = ftr.Transf_nome;
                        tbii.DateFrom = ftr.Data_From;
                        tbii.DateTo = ftr.Data_To;
                        tbii.S_id = ftr.S_id;
                        tbii.idTabela = ftr.File_Transf_id;
                        tbii.Quotation_Code = qCode;
                        tbii.Ordem = ftr.Ordem;
                        tbii.TC_Rateado = ratear.Equals("0") ? true : false;

                        if (ftr.Flights_id != null)
                        {
                            FileFlightsDAL ffd = new FileFlightsDAL();
                            File_Flights ff = ffd.ObterPorId(Convert.ToInt32(ftr.Flights_id));

                            tbii.Flight_voo = ff.Flights_nome;
                            tbii.Flight_hora = ff.Flights_hora;
                            tbii.Flight_pickUp = ff.Flights_pickUp;
                        }

                        if (ftr.SubServico != null)
                            tbii.Flag = "SubServ";
                        else if (ftr.Trf_Tours.Equals("TRF"))
                            tbii.Flag = "TRANSFER";
                        else
                            tbii.Flag = "TOUR";

                        S_Servicos ss = new S_Servicos();
                        Cidade cdd = new Cidade();

                        if (ftr.Trf_CID_id != null)
                        {
                            ss = servDal.ObterPorNome(ftr.Transf_nome.Trim(), Convert.ToInt32(ftr.Trf_CID_id));
                            cdd = cd.ObterPorId((int)ftr.Trf_CID_id);
                        }
                        else
                        {
                            ss = servDal.ObterPorNome(ftr.Transf_nome.Trim());
                        }


                        if (ss == null)
                            ss = servDal.ObterPorNome(ftr.Transf_nome.Trim());

                        if (cdd != null)
                            tbii.Cidade = cdd.CID_nome;
                        else
                            tbii.Cidade = ss.Cidade.CID_nome;

                        tbid.Salvar(tbii);

                        foreach (Ranges rangesHoteis in rd.ObterPorTarifaDeAteOptItensOrdem(IdQuote, OptQuote, ftr.File_Transf_id, "servico"))
                        {
                            tbid.Atualizar(RetornaRange(i), RetornaBase(i), Convert.ToDecimal(rangesHoteis.ValorTotal), rangesHoteis.Ranges_de + "/" + rangesHoteis.Ranges_ate, tbii.Itinerary_id);
                            i++;
                        }

                        i = 0;
                        break;
                    case "extra":
                        File_ServExtra ftsx = fsed.ObterPorId(Convert.ToInt32(item.FileTabelaId));
                        TBItinerary tbi3 = new TBItinerary();

                        tbi3.OptQuote = OptQuote;
                        tbi3.Code = qCode;
                        tbi3.SuppNome = ftsx.S_nome;
                        tbi3.ServExtrNome = ftsx.File_ServExtra_nome;
                        tbi3.DateFrom = ftsx.Data_From;
                        tbi3.DateTo = ftsx.Data_To;
                        tbi3.Flag = "EXTRA";
                        tbi3.S_id = ftsx.S_id;
                        tbi3.idTabela = ftsx.File_ServExtra_id;
                        tbi3.Quotation_Code = qCode;
                        tbi3.Ordem = ftsx.Ordem;
                        tbi3.TC_Rateado = ratear.Equals("0") ? true : false;

                        if (ftsx.Flights_id != null)
                        {
                            FileFlightsDAL ffd = new FileFlightsDAL();
                            File_Flights ff = ffd.ObterPorId(Convert.ToInt32(ftsx.Flights_id));

                            tbi3.Flight_voo = ff.Flights_nome;
                            tbi3.Flight_hora = ff.Flights_hora;
                            tbi3.Flight_pickUp = ff.Flights_pickUp;
                        }


                        if (ftsx.Ext_CID_id != null && ftsx.Ext_CID_id != 0)
                        {
                            Cidade c = cd.ObterPorId(Convert.ToInt32(ftsx.Ext_CID_id));
                            if (c != null)
                            {
                                tbi3.Cidade = c.CID_nome;
                            }
                            else
                            {
                                Supplier spp = spd.ObterPorId(Convert.ToInt32(ftsx.S_id));
                                tbi3.Cidade = spp.Cidade.CID_nome;
                            }
                        }
                        else
                        {
                            Supplier spp = spd.ObterPorId(Convert.ToInt32(ftsx.S_id));
                            tbi3.Cidade = spp.Cidade.CID_nome;
                        }

                        tbid.Salvar(tbi3);

                        foreach (Ranges rangesHoteis in rd.ObterPorTarifaDeAteOptItensOrdem(IdQuote, OptQuote, ftsx.File_ServExtra_id, "extra"))
                        {
                            tbid.Atualizar(RetornaRange(i), RetornaBase(i), Convert.ToDecimal(rangesHoteis.ValorTotal), rangesHoteis.Ranges_de + "/" + rangesHoteis.Ranges_ate, tbi3.Itinerary_id);
                            i++;
                        }
                        i = 0;
                        break;
                }
            }

        }

        protected string RetornaRange(int bases)
        {
            string range = "";
            switch (bases)
            {
                case 0:
                    range = "Range01";
                    break;
                case 1:
                    range = "Range02";
                    break;
                case 2:
                    range = "Range03";
                    break;
                case 3:
                    range = "Range04";
                    break;
                case 4:
                    range = "Range05";
                    break;
                case 5:
                    range = "Range06";
                    break;
                case 6:
                    range = "Range07";
                    break;
                case 7:
                    range = "Range08";
                    break;
                case 8:
                    range = "Range09";
                    break;
                case 9:
                    range = "Range10";
                    break;
                case 10:
                    range = "Range11";
                    break;
                case 11:
                    range = "Range12";
                    break;
            }
            return range;
        }

        protected string RetornaBase(int bases)
        {
            string range = "";
            switch (bases)
            {
                case 0:
                    range = "Base01";
                    break;
                case 1:
                    range = "Base02";
                    break;
                case 2:
                    range = "Base03";
                    break;
                case 3:
                    range = "Base04";
                    break;
                case 4:
                    range = "Base05";
                    break;
                case 5:
                    range = "Base06";
                    break;
                case 6:
                    range = "Base07";
                    break;
                case 7:
                    range = "Base08";
                    break;
                case 8:
                    range = "Base09";
                    break;
                case 9:
                    range = "Base10";
                    break;
                case 10:
                    range = "Base11";
                    break;
                case 11:
                    range = "Base12";
                    break;
            }
            return range;
        }

        public ActionResult Index(string code)
        {
            OfferHash offert = new OfferDal().ObterPorHash(code);

            CidadeDAL cdal = new CidadeDAL();
            QuotationDAL _QuotationDal = new QuotationDAL();
            TBItineraryDAL tbdDal = new TBItineraryDAL();
            TBItinerary_HighlightsDAL tbDal = new TBItinerary_HighlightsDAL();

            MealDAL mealdal = new MealDAL();

            #region titulo

            Quotation _quotation = new QuotationDAL().ObterPorCode(offert.OfferHash_quote);
            Quotation_Grupo _qgrupo = new QuotationGrupoDAL().ObterPorIdQuotation(_quotation.Quotation_Id);

            ViewBag.nomeCliente = _qgrupo.Cliente.Cliente_nome;

            if (_qgrupo.tourCode != null)
            {
                ViewBag.tourCode = _qgrupo.tourCode + " - " + offert.OfferHash_quote.Substring(5, offert.OfferHash_quote.Length - 5);
            }
            else
            {
                ViewBag.tourCode = offert.OfferHash_quote.Substring(5, offert.OfferHash_quote.Length - 5);
            }

            //ViewBag.tourCode = _qgrupo.tourCode;

            #endregion

            #region imagesHight

            TBItinerary_HighlightsImagesDAL thid = new TBItinerary_HighlightsImagesDAL();
            TBItinerary_HighlightsImages thi = thid.ObterPorCode(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            if (thi != null)
            {
                if (thi.FirstImage != null)
                    ViewBag.hgImg1 = "../../Galeria/TBItinerary/" + thi.FirstImage;
                else
                {
                    //ViewBag.hgImg1 = "../../Galeria/noimage.jpg";

                    //insereFoto(_qgrupo)
                }

                if (thi.LastImage != null)
                    ViewBag.hgImg2 = "../../Galeria/TBItinerary/" + thi.LastImage;
                else
                    ViewBag.hgImg2 = "../../Galeria/noimage.jpg";
            }
            else
            {
                var fileid = new FileCarrinhoDAL().ObterPorIdGrupo(_qgrupo.Quotation_Grupo_Id).File_id;
                var pars = insereFoto(fileid, (int)offert.OfferHash_optQuote, offert.OfferHash_quote);

                ViewBag.hgImg1 = pars.Split('°')[0];
                ViewBag.hgImg2 = pars.Split('°')[1];
            }

            #endregion

            #region Notes

            TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
            List<TBItinerary_Info> lstIti = tbinfod.ObterPorQuote(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            //ViewBag.EditorNotes = lstIti[0].Note;

            if (lstIti.Count > 0)
            {
                ViewBag.EditorNotes = lstIti.Where(s => s.Note != "" && s.Note != null).First().Note;
            }

            #endregion

            #region hightlight

            List<TBItineraryDAL.CidadeHigh> cids = tbdDal.ListarTodosCidadesComHotels_(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            List<TBItinerary_Highlights> lstInto = tbDal.ListarTodos(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            foreach (var item in cids)
            {
                Cidade cid = cdal.ObterPorNome(item.Cidade);
                var lstvis = tbDal.ListarPorCidade_False(cid.CID_id, offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
                if (lstvis.Count > 0)
                {
                    cids = cids.Where(s => s.Cidade != cid.CID_nome).ToList();
                }
            }

            ViewBag.hightlights = cids;
            ViewBag.hightlightsInto = lstInto;

            #endregion

            #region hightlight includes

            Including_HighLightsDAL InHighLightsDAL = new Including_HighLightsDAL();

            List<Including_HighLights> lstInc = InHighLightsDAL.ListarPQuotation(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            List<Including_HighLights> orderedList = new List<Including_HighLights>();

            FileTarifasDAL ftrDal = new FileTarifasDAL();
            FileTransfersDAL ftrsDal = new FileTransfersDAL();
            FileServExtraDAL fsextDal = new FileServExtraDAL();

            int FileID = new FileCarrinhoDAL().RetornaFileObj(offert.OfferHash_quote).File_id;

            int days = Convert.ToInt32(ftrsDal.MontaCarrinhoNewFile(FileID, (int)offert.OfferHash_optQuote)) + 1;

            if (lstInc.Count < 3)
            {
                InHighLightsDAL.ExcluirTodosCode(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

                InHighLightsDAL.InsertDefault_New(offert.OfferHash_quote, (int)offert.OfferHash_optQuote, days);
                orderedList = InHighLightsDAL.ListarPQuotation(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            }
            else
            {
                orderedList = lstInc;
            }
            ViewBag.hightlightsIncluding = orderedList;

            #endregion

            #region not includes

            Discretion_HighLightsDAL dhd = new Discretion_HighLightsDAL();
            List<Discretion_HighLights> lstDis = dhd.ListarPQuotation(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            //if (lstDis.Count == 0)
            //{
            //    dhd.InsertDefaults(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            //    lstDis = dhd.ListarPQuotation(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            //}

            ViewBag.hightlightsDiscretion = lstDis;

            #endregion

            #region Map

            getAllCitysForItinerary(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            #endregion

            #region itinerary

            TBItinerary_SupplierDAL tbSpDal = new TBItinerary_SupplierDAL();

            List<itineraryModel> newitineraryEdit = new List<itineraryModel>();

            var itineraryEdit = tbdDal.ListarItinerary(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            double dia = 1;
            DateTime dtnow = new DateTime();
            DateTime dtPrimeira = new DateTime();

            itineraryModel itemAnterior = null;

            itineraryModel itemNextN = null;
            itineraryModel itemNext = new itineraryModel();

            int countL = 0;

            foreach (var item in itineraryEdit)
            {

                if (itemAnterior != null)
                {
                    itemAnterior = null;
                    countL++;
                    continue;
                }

                if (itemNextN != null && item.Flag.Equals("HOTEL"))
                {
                    itemNextN = null;
                    countL++;
                    continue;
                }

                itineraryModel it = new itineraryModel();
                it.cidade = item.cidade;
                it.dtfrom = (DateTime)item.dtfrom;
                it.dtTo = (DateTime)item.dtTo;
                it.Code = item.Code;
                it.idTabela = item.idTabela;
                it.optquote = item.optquote;
                it.Sid = item.Sid;
                it.tbItineraryID = item.tbItineraryID;
                it.NomePacote = item.NomePacote;

                it.FlightVoo = item.FlightVoo;
                it.FlightHora = item.FlightHora;

                if (item.NomePacote != null)
                {
                    it.DescPacote = retornaPacote(item.dtfrom, item.dtTo, item.Sid, item.NomePacote, item.idTabela, item.optquote, item.Code);
                }

                int tot = countL + 1;
                if (itineraryEdit.Count() != tot)
                    itemNext = itineraryEdit[tot];


                //if (item.Flag == itemNext.Flag && item.Sid == itemNext.Sid && item.dtfrom == itemNext.dtfrom && item.categoria == itemNext.categoria && item.titulo == itemNext.titulo)
                //{
                //    it.titulo = string.Format("{0} {1} & {2} {3}", item.titulo, item.room, itemNext.room, item.categoria);
                //    itemAnterior = itemNext;
                //}
                //else
                //{
                //    it.titulo = string.Format("{0} {1} {2}", item.titulo, item.room, item.categoria);
                //    itemAnterior = null;
                //}

                if (item.MealNome == itemNext.MealNome && item.Flag == itemNext.Flag && item.Sid == itemNext.Sid && item.dtfrom == itemNext.dtfrom && item.categoria == itemNext.categoria && item.titulo == itemNext.titulo)
                {
                    if (item.room == null)
                    {
                        if (itemNext.room != null)
                        {
                            it.titulo = string.Format("{0} {1} & {2} {3}", item.titulo, item.room, itemNext.room, item.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            if (item.Flag.Equals("MEAL"))
                            {
                                it.titulo = string.Format("{0} {1}", item.titulo, item.MealNome.Replace("INCL", ""));
                                itemAnterior = itemNext;
                            }
                            else
                            {
                                it.titulo = string.Format("{0} {1}", item.titulo, item.room);
                                itemAnterior = itemNext;
                            }
                        }
                    }
                    else
                    {

                        if (tot + 1 < itineraryEdit.Count)
                            itemNextN = itineraryEdit[tot + 1];

                        if (itemNextN != null && item.Flag == itemNextN.Flag && item.Sid == itemNextN.Sid && item.dtfrom == itemNextN.dtfrom && item.titulo == itemNextN.titulo)
                        {
                            it.titulo = string.Format("{0} - {1} - {2}", item.titulo, item.room, item.categoria + " & " + itemNext.categoria + " & " + itemNextN.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} - {1} & {2} - {3}", item.titulo, item.room, itemNext.room, item.categoria);
                            itemAnterior = itemNext;
                        }
                    }
                }
                else
                {
                    if (item.room == null)
                    {
                        if (item.Flag.Equals("MEAL"))
                        {
                            it.titulo = string.Format("{0} {1}", item.titulo, item.MealNome.Replace("INCL", ""));
                            itemAnterior = null;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} {1} {2}", item.titulo, item.room, item.categoria);
                            itemAnterior = null;
                        }
                    }
                    else
                    {

                        if (item.Flag == itemNext.Flag && item.Sid == itemNext.Sid && item.dtfrom == itemNext.dtfrom && item.titulo == itemNext.titulo)
                        {
                            it.titulo = string.Format("{0} - {1} & {2}", item.titulo, item.room + " " + item.categoria, itemNext.room + " " + itemNext.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} - {1} - {2}", item.titulo, item.room, item.categoria);
                            itemAnterior = null;
                        }

                    }
                }

                if (item.Flag.Equals("HOTEL") || item.Flag.Equals("MEAL"))
                {
                    var ft = ftrDal.ObterPorId(item.idTabela);

                    it.nights = (item.dtTo - item.dtfrom).TotalDays.ToString();

                    var mealS = mealdal.ObterPorNome(ft.S_meal_nome);
                    string nomeNight = "";

                    if (mealS != null)
                    {
                        if (mealS.S_meal_breakfast == true)
                        {
                            nomeNight = " Days";
                            it.nights = (Convert.ToInt32(it.nights) + 1).ToString();
                        }
                        else
                        {
                            nomeNight = " Nights";
                        }
                    }
                    else
                    {
                        nomeNight = " Nights";
                    }

                    if (Convert.ToInt32(it.nights) > 1)
                    {
                        it.nights = it.nights + nomeNight;
                    }
                    else
                    {
                        if (Convert.ToInt32(it.nights) == 1)
                        {
                            it.nights = it.nights + nomeNight;
                        }
                        else
                        {
                            it.nights = "";
                        }
                    }

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = tSp.S_Name;
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = it.titulo;
                    }
                }
                else if (item.Flag.Equals("EXTRA"))
                {
                    var ft = fsextDal.ObterPorId(item.idTabela);

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = tSp.S_Name;
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = it.titulo;
                    }

                }
                else
                {
                    var ft = ftrsDal.ObterPorId(item.idTabela);

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = tSp.S_Name;
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = it.titulo;
                    }
                }

                //if (item.Flag.Equals("HOTEL"))
                //{
                //    it.nights = (item.dtTo - item.dtfrom).TotalDays.ToString();

                //    if (Convert.ToInt32(it.nights) > 1)
                //        it.nights = it.nights + " Nights";
                //    else
                //        it.nights = it.nights + " Night";


                //    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                //    if (tSp != null)
                //    {
                //        it.titulo = tSp.S_Name;
                //    }
                //    else
                //    {
                //        it.titulo = it.titulo;
                //    }
                //}
                //else
                //{
                //    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                //    if (tSp != null)
                //    {
                //        it.titulo = tSp.S_Name;
                //    }
                //    else
                //    {
                //        it.titulo = it.titulo;
                //    }
                //}


                it.Flag = item.Flag;
                it.ordem = item.ordem;
                it.categoria = item.categoria;
                it.room = item.room;



                it.urlIcon = retornaUrlIcone(item.Flag, item.Sid, item.titulo, item.idTabela);

                if (dtnow != item.dtfrom && dtnow.Year != 1)
                    dia = (item.dtfrom - dtPrimeira).TotalDays + 1;


                it.descricao = retornaDescricao(item.Flag, item.Sid, item.idTabela, offert.OfferHash_quote, (int)offert.OfferHash_optQuote, item.titulo, item.cidade);

                if (dtnow.Year == 1)
                    dtPrimeira = item.dtfrom;


                it.urlFoto = retornaUrlFoto(item.idTabela, item.Code, item.optquote, item.Flag, item.Sid);


                dtnow = it.dtfrom;
                it.dia = Convert.ToInt32(dia);

                countL++;

                newitineraryEdit.Add(it);
            }

            ViewData["itinerary"] = newitineraryEdit;

            #endregion

            #region hoteis

            ViewData["hoteisEdit"] = HotelsPorCodigo(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            #endregion

            #region flights

            ViewBag.flights = CarregaFlights(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            #endregion

            #region prices

            ViewBag.prices = montaPrices_link(offert.OfferHash_quote, (int)offert.OfferHash_optQuote, (int)offert.OfferHash_ratear);

            TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();
            var tbp = tbpd.ObterPorQuote(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            if (tbp.Count > 0)
            {
                if (tbp.First().Preco_Link != null && tbp.First().Preco_Link != "." && tbp.First().Preco_Link != "")
                {
                    ViewBag.priceLink = tbp.First().Preco_Link;
                }
                else
                {
                    ViewBag.priceLink = "";
                }

                if (tbp.First().Preco_Notes != null && tbp.First().Preco_Notes != "." && tbp.First().Preco_Notes != "")
                {
                    ViewBag.priceNotes = tbp.First().Preco_Notes;
                }
                else
                {
                    ViewBag.priceNotes = "";
                }
            }

            #endregion

            return View();
        }

        public void getAllCitysForItinerary(string qcode, int optquote)
        {
            try
            {
                PaisDAL pd = new PaisDAL();
                CidadeDAL cdal = new CidadeDAL();
                TBItineraryDAL tbdal = new TBItineraryDAL();
                SupplierDAL sdal = new SupplierDAL();

                string pars = "", parshoteis = "";
                int numDias = 0;

                //SAO PAULO Brazil @2~PUERTO IGUAZU Argentina @2~BUENOS AIRES Argentina @3                

                foreach (var item in tbdal.ObterPorQuoteOrder(qcode, optquote)) //tbdal.ObterPorQuote(Code).Select(f => f.Cidade).Distinct())
                {

                    if (item.Flag.Equals("HOTEL"))
                    {
                        numDias = Convert.ToInt32((Convert.ToDateTime(item.DateTo) - Convert.ToDateTime(item.DateFrom)).TotalDays);
                    }

                    if (!pars.Contains(item.Cidade) && item.Flag.Equals("HOTEL"))
                    {
                        Pais pais = pd.ObterPorCidade(item.Cidade);
                        pars = pars + item.Cidade + " " + pais.PAIS_nome + " @" + numDias + "~";

                        Cidade cdd = cdal.ObterPorNome(item.Cidade);

                        Supplier spp = sdal.ObterSupplierPorNome(item.SuppNome, cdd.CID_id);
                        Supplier spp1 = sdal.ObterSupplierPorNome(item.SuppNome);




                        if (spp != null)
                        {

                            string endereco = spp.S_endereco;

                            if (spp.Longitude != null)
                            {
                                var obteve = ObtemEndecoGoogle(spp.Latitude + "," + spp.Longitude);

                                if (obteve != "")
                                    endereco = obteve;
                            }

                            parshoteis = parshoteis + item.SuppNome + "@" + endereco + "~";
                        }
                        else if (spp1 != null)
                        {

                            string endereco = spp1.S_endereco;

                            if (spp1.Longitude != null)
                            {
                                var obteve = ObtemEndecoGoogle(spp1.Latitude + "," + spp1.Longitude);

                                if (obteve != "")
                                    endereco = obteve;

                            }


                            parshoteis = parshoteis + item.SuppNome + "@" + endereco + "~";
                        }
                    }
                }

                pars = pars.Remove(pars.Length - 1);
                parshoteis = parshoteis.Remove(parshoteis.Length - 1);

                ViewBag.hdetemp = pars;
                ViewBag.hdetempHoteis = parshoteis;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        public string ObtemEndecoGoogle(string coordinate)
        {

            //Console.WriteLine("enter coordinate:");
            //string coordinate = "32.797821,-96.781720"; //Console.ReadLine();
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("https://maps.googleapis.com/maps/api/geocode/xml?latlng=" + coordinate);

                XmlNodeList xNodelst = xDoc.GetElementsByTagName("result");
                XmlNode xNode = xNodelst.Item(0);
                string FullAddress = xNode.SelectSingleNode("formatted_address").InnerText;
                //string numero = xNode.SelectSingleNode("address_component[1]/long_name").InnerText;
                //string rua = xNode.SelectSingleNode("address_component[2]/long_name").InnerText;
                //string bairro = xNode.SelectSingleNode("address_component[3]/long_name").InnerText;
                //string Area = xNode.SelectSingleNode("address_component[4]/long_name").InnerText;
                //string County = xNode.SelectSingleNode("address_component[5]/long_name").InnerText;
                //string estado = xNode.SelectSingleNode("address_component[6]/long_name").InnerText;
                //string cep = xNode.SelectSingleNode("address_component[8]/long_name").InnerText;
                //string Country = xNode.SelectSingleNode("address_component[7]/long_name").InnerText;
                //Console.WriteLine("Full Address: " + FullAddress);               
                //Console.ReadLine();
                return FullAddress;

            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public JsonResult GeraLink(string qcode, int type, int price, int optquote, int ratear)
        {
            try
            {
                OfferDal oDal = new OfferDal();

                OfferHash offert = oDal.ObterPorCodes(qcode, type, price, optquote, ratear);

                if (offert != null)
                {
                    return Json(new { success = true, message = "", hash = offert.OfferHash_hash }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    OfferHash oh = new OfferHash();
                    oh.OfferHash_hash = Guid.NewGuid().ToString();
                    oh.OfferHash_optQuote = optquote;
                    oh.OfferHash_price = price;
                    oh.OfferHash_quote = qcode;
                    oh.OfferHash_type = type;
                    oh.OfferHash_ratear = ratear;
                    oDal.Salvar(oh);

                    return Json(new { success = true, message = "", hash = oh.OfferHash_hash }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult OfferPDF(string code)
        {
            OfferHash offert = new OfferDal().ObterPorHash(code);

            QuotationDAL _QuotationDal = new QuotationDAL();
            TBItineraryDAL tbdDal = new TBItineraryDAL();
            TBItinerary_HighlightsDAL tbDal = new TBItinerary_HighlightsDAL();
            MealDAL mealdal = new MealDAL();
            CidadeDAL cdal = new CidadeDAL();

            #region titulo

            Quotation _quotation = new QuotationDAL().ObterPorCode(offert.OfferHash_quote);
            Quotation_Grupo _qgrupo = new QuotationGrupoDAL().ObterPorIdQuotation(_quotation.Quotation_Id);

            ViewBag.nomeCliente = _qgrupo.Cliente.Cliente_nome;

            if (_qgrupo.tourCode != null)
            {
                ViewBag.tourCode = _qgrupo.tourCode + " - " + offert.OfferHash_quote.Substring(5, offert.OfferHash_quote.Length - 5);
            }
            else
            {
                //ViewBag.tourCode = offert.OfferHash_quote;
                ViewBag.tourCode = offert.OfferHash_quote.Substring(5, offert.OfferHash_quote.Length - 5);
            }

            #endregion

            #region imagesHight

            TBItinerary_HighlightsImagesDAL thid = new TBItinerary_HighlightsImagesDAL();
            TBItinerary_HighlightsImages thi = thid.ObterPorCode(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            if (thi != null)
            {
                if (thi.FirstImage != null)
                    ViewBag.hgImg1 = "http://54.232.209.130/TierraMVC/Galeria/TBItinerary/" + thi.FirstImage;
                else
                    ViewBag.hgImg1 = "http://54.232.209.130/TierraMVC/Galeria/noimage.jpg";


                if (thi.LastImage != null)
                    ViewBag.hgImg2 = "http://54.232.209.130/TierraMVC/Galeria/TBItinerary/" + thi.LastImage;
                else
                    ViewBag.hgImg2 = "http://54.232.209.130/TierraMVC/Galeria/noimage.jpg";
            }
            else
            {
                var fileid = new FileCarrinhoDAL().ObterPorIdGrupo(_qgrupo.Quotation_Grupo_Id).File_id;
                var pars = insereFotoPDF(fileid, (int)offert.OfferHash_optQuote);

                ViewBag.hgImg1 = pars.Split('°')[0];
                ViewBag.hgImg2 = pars.Split('°')[1];
            }

            #endregion

            #region Notes

            TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
            List<TBItinerary_Info> lstIti = tbinfod.ObterPorQuote(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            //ViewBag.EditorNotes = lstIti[0].Note;

            if (lstIti.Count > 0)
            {
                ViewBag.EditorNotes = lstIti.Where(s => s.Note != "" && s.Note != null).First().Note;
            }

            #endregion

            #region hightlight

            List<TBItineraryDAL.CidadeHigh> cids = tbdDal.ListarTodosCidadesComHotels_(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            foreach (var item in cids)
            {
                Cidade cid = cdal.ObterPorNome(item.Cidade);
                var lstvis = tbDal.ListarPorCidade_False(cid.CID_id, offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
                if (lstvis.Count > 0)
                {
                    cids = cids.Where(s => s.Cidade != cid.CID_nome).ToList();
                }
            }
            foreach (var item in cids)
            {
                PreencheHightLightCity(item.Cidade, offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            }
            List<TBItinerary_Highlights> lstInto = tbDal.ListarTodos(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);


            //List<TBItineraryDAL.CidadeHigh> cids = tbdDal.ListarTodosCidadesComHotels_(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            //List<TBItinerary_Highlights> lstInto = tbDal.ListarTodos(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            ViewBag.hightlights = cids;
            ViewBag.hightlightsInto = lstInto;

            #endregion

            #region hightlight includes

            Including_HighLightsDAL InHighLightsDAL = new Including_HighLightsDAL();

            List<Including_HighLights> lstInc = InHighLightsDAL.ListarPQuotation(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            List<Including_HighLights> orderedList = new List<Including_HighLights>();

            FileTarifasDAL ftrDal = new FileTarifasDAL();
            FileTransfersDAL ftrsDal = new FileTransfersDAL();
            FileServExtraDAL fsextDal = new FileServExtraDAL();

            int FileID = new FileCarrinhoDAL().RetornaFileObj(offert.OfferHash_quote).File_id;

            int days = Convert.ToInt32(ftrsDal.MontaCarrinhoNewFile(FileID, (int)offert.OfferHash_optQuote)) + 1;

            if (lstInc.Count < 3)
            {
                InHighLightsDAL.ExcluirTodosCode(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

                InHighLightsDAL.InsertDefault_New(offert.OfferHash_quote, (int)offert.OfferHash_optQuote, days);
                orderedList = InHighLightsDAL.ListarPQuotation(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            }
            else
            {
                orderedList = lstInc;
            }
            ViewBag.hightlightsIncluding = orderedList;

            #endregion

            #region not includes

            Discretion_HighLightsDAL dhd = new Discretion_HighLightsDAL();
            List<Discretion_HighLights> lstDis = dhd.ListarPQuotation(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            //if (lstDis.Count == 0)
            //{
            //    dhd.InsertDefaults(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            //    lstDis = dhd.ListarPQuotation(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            //}

            ViewBag.hightlightsDiscretion = lstDis;

            #endregion

            #region Map

            getAllCitysForItinerary(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            #endregion

            #region itinerary

            TBItinerary_SupplierDAL tbSpDal = new TBItinerary_SupplierDAL();

            List<itineraryModel> newitineraryEdit = new List<itineraryModel>();

            var itineraryEdit = tbdDal.ListarItinerary(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);
            double dia = 1;
            DateTime dtnow = new DateTime();
            DateTime dtPrimeira = new DateTime();

            itineraryModel itemAnterior = null;
            itineraryModel itemNextN = null;
            itineraryModel itemNext = new itineraryModel();

            int countL = 0;

            foreach (var item in itineraryEdit)
            {

                if (itemAnterior != null)
                {
                    itemAnterior = null;
                    countL++;
                    continue;
                }
                if (itemNextN != null && item.Flag.Equals("HOTEL"))
                {
                    itemNextN = null;
                    countL++;
                    continue;
                }

                itineraryModel it = new itineraryModel();
                it.cidade = item.cidade;
                it.dtfrom = (DateTime)item.dtfrom;
                it.dtTo = (DateTime)item.dtTo;
                it.Code = item.Code;
                it.idTabela = item.idTabela;
                it.optquote = item.optquote;
                it.Sid = item.Sid;
                it.tbItineraryID = item.tbItineraryID;
                it.NomePacote = item.NomePacote;

                it.FlightVoo = item.FlightVoo;
                it.FlightHora = item.FlightHora;


                if (item.NomePacote != null)
                {
                    it.DescPacote = retornaPacote(item.dtfrom, item.dtTo, item.Sid, item.NomePacote, item.idTabela, item.optquote, item.Code);
                }

                int tot = countL + 1;
                if (itineraryEdit.Count() != tot)
                    itemNext = itineraryEdit[tot];


                //if (item.Flag == itemNext.Flag && item.Sid == itemNext.Sid && item.dtfrom == itemNext.dtfrom && item.categoria == itemNext.categoria && item.titulo == itemNext.titulo)
                //{
                //    it.titulo = string.Format("{0} {1} & {2} {3}", item.titulo, item.room, itemNext.room, item.categoria);
                //    itemAnterior = itemNext;
                //}
                //else
                //{
                //    it.titulo = string.Format("{0} {1} {2}", item.titulo, item.room, item.categoria);
                //    itemAnterior = null;
                //}

                if (item.MealNome == itemNext.MealNome && item.Flag == itemNext.Flag && item.Sid == itemNext.Sid && item.dtfrom == itemNext.dtfrom && item.categoria == itemNext.categoria && item.titulo == itemNext.titulo)
                {
                    if (item.room == null)
                    {
                        if (itemNext.room != null)
                        {
                            it.titulo = string.Format("{0} {1} & {2} {3}", item.titulo, item.room, itemNext.room, item.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            if (item.Flag.Equals("MEAL"))
                            {
                                it.titulo = string.Format("{0} {1}", item.titulo, item.MealNome.Replace("INCL", ""));

                                itemAnterior = itemNext;
                            }
                            else
                            {
                                it.titulo = string.Format("{0} {1}", item.titulo, item.room);
                                itemAnterior = itemNext;
                            }
                        }
                    }
                    else
                    {

                        if (tot + 1 < itineraryEdit.Count)
                            itemNextN = itineraryEdit[tot + 1];

                        if (itemNextN != null && item.Flag == itemNextN.Flag && item.Sid == itemNextN.Sid && item.dtfrom == itemNextN.dtfrom && item.titulo == itemNextN.titulo)
                        {
                            it.titulo = string.Format("{0} - {1} - {2}", item.titulo, item.room, item.categoria + " & " + itemNext.categoria + " & " + itemNextN.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} - {1} & {2} - {3}", item.titulo, item.room, itemNext.room, item.categoria);
                            itemAnterior = itemNext;
                        }
                    }
                }
                else
                {
                    if (item.room == null)
                    {
                        if (item.Flag.Equals("MEAL"))
                        {
                            it.titulo = string.Format("{0} {1}", item.titulo, item.MealNome.Replace("INCL", ""));
                            itemAnterior = null;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} {1} {2}", item.titulo, item.room, item.categoria);
                            itemAnterior = null;
                        }
                    }
                    else
                    {

                        if (item.Flag == itemNext.Flag && item.Sid == itemNext.Sid && item.dtfrom == itemNext.dtfrom && item.titulo == itemNext.titulo)
                        {
                            it.titulo = string.Format("{0} - {1} & {2}", item.titulo, item.room + " " + item.categoria, itemNext.room + " " + itemNext.categoria);
                            itemAnterior = itemNext;
                        }
                        else
                        {
                            it.titulo = string.Format("{0} - {1} - {2}", item.titulo, item.room, item.categoria);
                            itemAnterior = null;
                        }

                    }
                }


                if (item.Flag.Equals("HOTEL") || item.Flag.Equals("MEAL"))
                {
                    var ft = ftrDal.ObterPorId(item.idTabela);

                    it.nights = (item.dtTo - item.dtfrom).TotalDays.ToString();

                    var mealS = mealdal.ObterPorNome(ft.S_meal_nome);
                    string nomeNight = "";

                    if (mealS != null)
                    {
                        if (mealS.S_meal_breakfast == true)
                        {
                            nomeNight = " Days";
                            it.nights = (Convert.ToInt32(it.nights) + 1).ToString();
                        }
                        else
                        {
                            nomeNight = " Nights";
                        }
                    }
                    else
                    {
                        nomeNight = " Nights";
                    }

                    if (Convert.ToInt32(it.nights) > 1)
                    {
                        it.nights = it.nights + nomeNight;
                    }
                    else
                    {
                        if (Convert.ToInt32(it.nights) == 1)
                        {
                            it.nights = it.nights + nomeNight;
                        }
                        else
                        {
                            it.nights = "";
                        }
                    }

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = tSp.S_Name;
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = it.titulo;
                    }
                }
                else if (item.Flag.Equals("EXTRA"))
                {
                    var ft = fsextDal.ObterPorId(item.idTabela);

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = tSp.S_Name;
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = it.titulo;
                    }

                }
                else
                {
                    var ft = ftrsDal.ObterPorId(item.idTabela);

                    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                    if (tSp != null)
                    {
                        tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                        if (ft.TOptional == true)
                        {
                            it.titulo = tSp.S_Name; /* + " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = tSp.S_Name;
                    }
                    else
                    {
                        if (ft.TOptional == true)
                        {
                            it.titulo = it.titulo; /*+ " Optional";  //" <span style=\"color:#ff0000;\">Optional</span>";*/
                            it.optional = true;
                        }
                        else
                            it.titulo = it.titulo;
                    }
                }


                //if (item.Flag.Equals("HOTEL"))
                //{
                //    it.nights = (item.dtTo - item.dtfrom).TotalDays.ToString();

                //    if (Convert.ToInt32(it.nights) > 1)
                //        it.nights = it.nights + " Nights";
                //    else
                //        it.nights = it.nights + " Night";


                //    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                //    if (tSp != null)
                //    {
                //        it.titulo = tSp.S_Name;
                //    }
                //    else
                //    {
                //        it.titulo = it.titulo;
                //    }
                //}
                //else
                //{
                //    var tSp = tbSpDal.ObterPorSupplierId(item.Sid, item.Code, item.idTabela, item.optquote);
                //    if (tSp != null)
                //    {
                //        it.titulo = tSp.S_Name;
                //    }
                //    else
                //    {
                //        it.titulo = it.titulo;
                //    }
                //}

                it.Flag = item.Flag;
                it.ordem = item.ordem;
                it.categoria = item.categoria;
                it.room = item.room;

                it.urlIcon = retornaUrlIcone(item.Flag, item.Sid, item.titulo, item.idTabela);

                if (dtnow != item.dtfrom && dtnow.Year != 1)
                    dia = (item.dtfrom - dtPrimeira).TotalDays + 1;


                it.descricao = retornaDescricao(item.Flag, item.Sid, item.idTabela, offert.OfferHash_quote, (int)offert.OfferHash_optQuote, item.titulo, item.cidade);

                if (dtnow.Year == 1)
                    dtPrimeira = item.dtfrom;


                it.urlFoto = retornaUrlFoto(item.idTabela, item.Code, item.optquote, item.Flag, item.Sid);


                dtnow = it.dtfrom;
                it.dia = Convert.ToInt32(dia);

                countL++;

                newitineraryEdit.Add(it);
            }

            ViewData["itinerary"] = newitineraryEdit;

            #endregion

            #region hoteis

            ViewData["hoteisEdit"] = HotelsPorCodigo(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            #endregion

            #region flights

            ViewBag.flights = CarregaFlights(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            #endregion

            #region prices

            ViewBag.prices = montaPrices_link(offert.OfferHash_quote, (int)offert.OfferHash_optQuote, (int)offert.OfferHash_ratear);

            TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();
            var tbp = tbpd.ObterPorQuote(offert.OfferHash_quote, (int)offert.OfferHash_optQuote);

            if (tbp.Count > 0)
            {
                if (tbp.First().Preco_Link != null && tbp.First().Preco_Link != "." && tbp.First().Preco_Link != "")
                {
                    ViewBag.priceLink = tbp.First().Preco_Link;
                }
                else
                {
                    ViewBag.priceLink = "";
                }

                if (tbp.First().Preco_Notes != null && tbp.First().Preco_Notes != "." && tbp.First().Preco_Notes != "")
                {
                    ViewBag.priceNotes = tbp.First().Preco_Notes;
                }
                else
                {
                    ViewBag.priceNotes = "";
                }
            }

            #endregion

            return View();
        }

        public void ExportPDF(string code)
        {

            // Create a HTML to PDF converter object with default settings
            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();


            // Set license key received after purchase to use the converter in licensed mode
            // Leave it not set to use the converter in demo mode
            htmlToPdfConverter.LicenseKey = "8nxsfWhtfW9ran1rc219bmxzbG9zZGRkZH1t";

            // Set HTML Viewer width in pixels which is the equivalent in converter of the browser window width
            htmlToPdfConverter.HtmlViewerWidth = 1024; // int.Parse(htmlViewerWidthTextBox.Text);


            htmlToPdfConverter.PdfDocumentOptions.AvoidImageBreak = true;


            // Set HTML viewer height in pixels to convert the top part of a HTML page 
            // Leave it not set to convert the entire HTML
            //if (htmlViewerHeightTextBox.Text.Length > 0)
            //htmlToPdfConverter.HtmlViewerHeight = 10; //int.Parse(htmlViewerHeightTextBox.Text);

            htmlToPdfConverter.ClipHtmlView = true;

            // Set PDF page size which can be a predefined size like A4 or a custom size in points 
            // Leave it not set to have a default A4 PDF page
            htmlToPdfConverter.PdfDocumentOptions.PdfPageSize = EvoPdf.PdfPageSize.A4; //SelectedPdfPageSize();


            // Set PDF page orientation to Portrait or Landscape
            // Leave it not set to have a default Portrait orientation for PDF page
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = EvoPdf.PdfPageOrientation.Portrait; //SelectedPdfPageOrientation();


            // Set the maximum time in seconds to wait for HTML page to be loaded 
            // Leave it not set for a default 60 seconds maximum wait time
            htmlToPdfConverter.NavigationTimeout = 30; //int.Parse(navigationTimeoutTextBox.Text);


            // Set an adddional delay in seconds to wait for JavaScript or AJAX calls after page load completed
            // Set this property to 0 if you don't need to wait for such asynchcronous operations to finish
            //if (conversionDelayTextBox.Text.Length > 0)
            htmlToPdfConverter.ConversionDelay = 5; //int.Parse(conversionDelayTextBox.Text);


            // The buffer to receive the generated PDF document
            byte[] outPdfBuffer = null;

            //htmlToPdfConverter.PdfDocumentOptions.TopMargin = 30;
            //htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 30;
            //htmlToPdfConverter.PdfDocumentOptions.RightMargin = 30;
            //htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 30;

            htmlToPdfConverter.PdfDocumentOptions.TopMargin = 0;
            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 0;
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = 0;
            htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 0;

            //string url = HttpContext.Current.Request.Url.AbsoluteUri + "&tipo=fulliti";

            string url = "";
            url = ConfigurationManager.AppSettings["URL_EVO"].ToString() + "code=" + code;
            //url = ConfigurationManager.AppSettings["URL_EVO_LOCAL"].ToString() + "code=" + code;

            // Convert the HTML page given by an URL to a PDF document in a memory buffer
            outPdfBuffer = htmlToPdfConverter.ConvertUrl(url);


            // Send the PDF as response to browser

            // Set response content type
            Response.AddHeader("Content-Type", "application/pdf");


            // Instruct the browser to open the PDF file as an attachment or inline
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=OFFER.pdf; size={1}",
                false ? "inline" : "attachment", outPdfBuffer.Length.ToString()));


            // Write the PDF document buffer to HTTP response
            Response.BinaryWrite(outPdfBuffer);


            // End the HTTP response and stop the current page processing
            Response.End();



            //// Create a HTML to PDF converter object with default settings
            //HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();

            //// Set HTML Viewer width in pixels which is the equivalent in converter of the browser window width
            //htmlToPdfConverter.HtmlViewerWidth = 1024;


            //// Set license key received after purchase to use the converter in licensed mode
            //// Leave it not set to use the converter in demo mode
            //htmlToPdfConverter.LicenseKey = "8nxsfWhtfW9ran1rc219bmxzbG9zZGRkZH1t";

            //htmlToPdfConverter.PdfDocumentOptions.AvoidImageBreak = true;

            //// Set PDF page size which can be a predefined size like A4 or a custom size in points 
            //// Leave it not set to have a default A4 PDF page
            //htmlToPdfConverter.PdfDocumentOptions.PdfPageSize = EvoPdf.PdfPageSize.A4;

            //// Set PDF page orientation to Portrait or Landscape
            //// Leave it not set to have a default Portrait orientation for PDF page
            //htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = EvoPdf.PdfPageOrientation.Portrait;

            //// Set the maximum time in seconds to wait for HTML page to be loaded 
            //// Leave it not set for a default 60 seconds maximum wait time
            //htmlToPdfConverter.NavigationTimeout = 30;

            //// Set an adddional delay in seconds to wait for JavaScript or AJAX calls after page load completed
            //// Set this property to 0 if you don't need to wait for such asynchcronous operations to finish
            //htmlToPdfConverter.ConversionDelay = 5;

            //string url = "";


            //url = ConfigurationManager.AppSettings["URL_EVO"].ToString() + "code=094406c3-29d9-4782-a3b1-c2644189b1d7";


            //// Convert the HTML page given by an URL to a PDF document in a memory buffer
            //byte[] outPdfBuffer = htmlToPdfConverter.ConvertUrl(url);

            //// Send the PDF file to browser
            //FileResult fileResult = new FileContentResult(outPdfBuffer, "application/pdf");
            //fileResult.FileDownloadName = "OFFER.pdf";

            //return fileResult;
        }

        public JsonResult VerificaPrice(string hash)
        {
            OfferHash offert = new OfferDal().ObterPorHash(hash);
            return Json(new { price = offert.OfferHash_price }, JsonRequestBehavior.AllowGet);
        }

        #region HightLightCity

        public JsonResult ResetHighlights(string qCode, int optQuote)
        {
            new TBItinerary_HighlightsImagesDAL().DeletarPorCode(qCode, optQuote);
            new TBItinerary_HighlightsDAL().DeletarPorCode(qCode, optQuote);
            new Including_HighLightsDAL().ExcluirTodosCode(qCode, optQuote);
            new Discretion_HighLightsDAL().ExcluirTodosCode(qCode, optQuote);

            TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
            List<TBItinerary_Info> lstIti = tbinfod.ObterPorQuote(qCode, optQuote);
            if (lstIti.Count > 0)
            {
                lstIti[0].Note = "";
                tbinfod.Atualizar(lstIti[0]);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        protected void PreencheHightLightCity(string cids, string qCode, int optQuote)
        {
            try
            {
                var tbihd = new TBItinerary_HighlightsDAL();
                Cidade cidade = new CidadeDAL().ObterPorNome(cids);

                var lstHighlights = tbihd.ListarPorCidade_(cidade.CID_id, qCode, optQuote);

                if (cidade.CID_nome.Equals("TRELEW"))
                {

                }

                if (lstHighlights.Count == 0)
                {
                    var iti = new TBItineraryDAL().ListarTodosSERVICOS_CidadeSemTransfer(qCode, cids, optQuote);

                    if (iti.Count == 0)
                    {
                        TBItinerary_Highlights tbh2 = new TBItinerary_Highlights();
                        tbh2.Cidade_ID = cidade.CID_id;
                        tbh2.Visible = true;
                        tbh2.optQuote = optQuote;
                        tbh2.Code = qCode;
                        tbh2.Highlight = "";
                        tbihd.Adicionar(tbh2);
                    }

                    foreach (var item in iti)
                    {
                        if (item.ServNome == null)
                        {
                            if (checkIfExists(item.ServExtrNome, qCode, optQuote))
                                break;
                            else
                                if (checkIfExists(item.ServNome, qCode, optQuote))
                                break;
                        }

                        TBItinerary_Highlights tbh = new TBItinerary_Highlights();

                        if (item.Flag.Equals("HOTEL"))
                        {
                            if (!item.NomePacote.Equals("OMNIBEES") && item.NomePacote != null)
                            {
                                tbh.Highlight = item.SuppNome;
                            }
                            else
                            {
                                tbh.Highlight = "";
                            }

                        }
                        else if (item.Flag.Equals("TRANSFER"))
                        {
                            tbh.Highlight = "";
                        }
                        else if (item.ServNome == null)
                        {
                            tbh.Highlight = item.ServExtrNome;
                        }
                        else
                        {
                            tbh.Highlight = item.ServNome;
                        }


                        tbh.Cidade_ID = cidade.CID_id;
                        tbh.Code = qCode;
                        tbh.optQuote = optQuote;
                        tbh.Visible = true;

                        tbihd.Adicionar(tbh);
                    }

                }
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
        }

        public JsonResult EditarHighlight(int idHighlight, string hightlight)
        {
            try
            {
                TBItinerary_HighlightsDAL tbihd = new TBItinerary_HighlightsDAL();

                var item = tbihd.Obter(idHighlight);
                item.Highlight = hightlight;

                tbihd.Editar(item);

                return Json(new { success = true, message = "Editado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeletarHighlight(int idHighlight)
        {
            try
            {
                TBItinerary_HighlightsDAL tbihd = new TBItinerary_HighlightsDAL();

                var item = tbihd.Obter(idHighlight);

                tbihd.Excluir(item);

                return Json(new { success = true, message = "Deletado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeletarHighlightCity(string cidade, string qCode, int optQuote)
        {
            try
            {
                TBItinerary_HighlightsDAL tbihd = new TBItinerary_HighlightsDAL();

                Cidade cid = new CidadeDAL().ObterPorNome(cidade);

                var lstvis = tbihd.ListarPorCidade(cid.CID_id, qCode, optQuote);
                foreach (var item in lstvis)
                {
                    item.Visible = false;
                    tbihd.EditarVisible(item);
                }


                return Json(new { success = true, message = "Deletado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateHightLightCity(string qCode, int optQuote)
        {
            try
            {

                TBItineraryDAL tbihd = new TBItineraryDAL();
                TBItinerary_HighlightsDAL hh = new TBItinerary_HighlightsDAL();
                CidadeDAL cd = new CidadeDAL();

                List<TBItineraryDAL.CidadeHigh> cids = tbihd.ListarTodosCidadesComHotels_(qCode, optQuote);
                List<TBItinerary_Highlights> lstInto = hh.ListarTodos(qCode, optQuote);

                foreach (var item in cids)
                {
                    Cidade cid = cd.ObterPorNome(item.Cidade);
                    var lstvis = hh.ListarPorCidade_False(cid.CID_id, qCode, optQuote);

                    if (lstvis.Count > 0)
                    {
                        cids = cids.Where(s => s.Cidade != cid.CID_nome).ToList();
                    }
                }

                ViewBag.hightlights = cids;
                ViewBag.hightlightsInto = lstInto;


                return PartialView("_hightlights_city");
            }
            catch (Exception ex)
            {
                return PartialView("_hightlights_city");
            }
        }

        public JsonResult AddHighlight(string cidade, string Highlight, string qcode, int optquote)
        {
            try
            {
                Cidade cid = new CidadeDAL().ObterPorNome(cidade);

                TBItinerary_HighlightsDAL tbhDal = new TBItinerary_HighlightsDAL();

                TBItinerary_Highlights th = new TBItinerary_Highlights();
                th.Cidade_ID = cid.CID_id;
                th.Code = qcode;
                th.Highlight = Highlight;
                th.optQuote = optquote;
                th.Visible = true;
                tbhDal.Adicionar(th);


                return Json(new { success = true, message = "Adicionado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Including

        public JsonResult EditarHighlightIncluding(int idHighlightIncluding, string hightlightIncluding)
        {
            try
            {
                Including_HighLightsDAL tbihd = new Including_HighLightsDAL();

                var item = tbihd.Obter(idHighlightIncluding);
                item.Descricao = hightlightIncluding;

                tbihd.Edit(item);

                return Json(new { success = true, message = "Editado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddHighlightIncluding(string HighlightIncluding, string qcode, int optquote)
        {
            try
            {
                Including_HighLightsDAL tbhDal = new Including_HighLightsDAL();

                Including_HighLights th = new Including_HighLights();
                th.Descricao = HighlightIncluding;
                th.optQuote = optquote;
                th.Quotation_Code = qcode;
                th.Visible = true;
                tbhDal.Salvar(th);


                return Json(new { success = true, message = "Adicionado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateHightLightIncluding(string qCode, int optQuote)
        {
            try
            {

                Including_HighLightsDAL ihd = new Including_HighLightsDAL();
                List<Including_HighLights> lstInc = ihd.ListarPQuotation(qCode, optQuote);
                ViewBag.hightlightsIncluding = lstInc;

                return PartialView("_hightlight_including");
            }
            catch (Exception ex)
            {
                return PartialView("_hightlight_including");
            }
        }

        public JsonResult DeletarHighlightIncluding(int idHighlightIncluding)
        {
            try
            {
                Including_HighLightsDAL tbihd = new Including_HighLightsDAL();

                var item = tbihd.Obter(idHighlightIncluding);

                tbihd.Excluir(item);

                return Json(new { success = true, message = "Deletado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Discretion

        public JsonResult EditarHighlightDiscretion(int idHighlightDiscretion, string hightlightDiscretion)
        {
            try
            {
                Discretion_HighLightsDAL tbihd = new Discretion_HighLightsDAL();

                var item = tbihd.Obter(idHighlightDiscretion);
                item.Descricao = hightlightDiscretion;

                tbihd.Editar(item.ID, hightlightDiscretion);

                return Json(new { success = true, message = "Editado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddHighlightDiscretion(string HighlightDiscretion, string qcode, int optquote)
        {
            try
            {
                Discretion_HighLightsDAL tbhDal = new Discretion_HighLightsDAL();

                Discretion_HighLights th = new Discretion_HighLights();
                th.Descricao = HighlightDiscretion;
                th.optQuote = optquote;
                th.Quotation_Code = qcode;
                th.Visible = true;
                tbhDal.Salvar(th);


                return Json(new { success = true, message = "Adicionado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateHightLightDiscretion(string qCode, int optQuote)
        {
            try
            {

                Discretion_HighLightsDAL ihd = new Discretion_HighLightsDAL();
                List<Discretion_HighLights> lstInc = ihd.ListarPQuotation(qCode, optQuote);
                ViewBag.hightlightsDiscretion = lstInc;

                return PartialView("_hightlight_discretion");
            }
            catch (Exception ex)
            {
                return PartialView("_hightlight_discretion");
            }
        }

        public JsonResult DeletarHighlightDiscretion(int idHighlightDiscretion)
        {
            try
            {
                Discretion_HighLightsDAL tbihd = new Discretion_HighLightsDAL();

                var item = tbihd.Obter(idHighlightDiscretion);

                tbihd.Excluir(item);

                return Json(new { success = true, message = "Deletado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region NotesHightLight

        protected void SaveNote(string qCode, int optQuote, string txtEditorNotes)
        {
            TBItineraryDAL tbid = new TBItineraryDAL();
            List<TBItinerary> lstIti = tbid.ObterPorQuote(qCode);
            TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();

            var lstantigos = tbinfod.ObterPorQuote(qCode, optQuote);
            foreach (var item in lstantigos)
            {
                if (lstIti.Where(s => s.idTabela == item.IdTabela).Count() == 0)
                {
                    tbinfod.Excluir(item);
                }
            }

            try
            {
                foreach (TBItinerary tbi in lstIti)
                {
                    TBItinerary_Info tbinfo = tbinfod.ObterPorQuotationIdTabela((int)tbi.idTabela, qCode, optQuote);

                    if (tbinfo == null)
                    {
                        tbinfo = new TBItinerary_Info();
                        tbinfo.Note = txtEditorNotes;
                        tbinfo.Quotation_Code = qCode;
                        tbinfo.IdTabela = (int)tbi.idTabela;
                        tbinfo.optQuote = optQuote;
                        tbinfod.Salvar(tbinfo);
                    }
                    else
                    {
                        tbinfo.Note = txtEditorNotes;
                        tbinfo.ID = tbinfo.ID;
                        tbinfod.Atualizar(tbinfo);
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public class SalvarNotesModel
        {
            public string txtEditorNotes { get; set; }
            public string qCode { get; set; }
            public int optQuote { get; set; }
        }

        public JsonResult SalvarNotesNew(SalvarNotesModel model)
        {
            try
            {
                TBItineraryDAL tbid = new TBItineraryDAL();
                List<TBItinerary> lstIti = tbid.ObterPorQuote(model.qCode, model.optQuote);
                TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();

                var conte = HttpUtility.UrlDecode(model.txtEditorNotes, System.Text.Encoding.Default);

                var lstantigos = tbinfod.ObterPorQuote(model.qCode, model.optQuote);
                foreach (var item in lstantigos)
                {
                    if (lstIti.Where(s => s.idTabela == item.IdTabela).Count() == 0)
                    {
                        tbinfod.Excluir(item);
                    }
                }

                foreach (TBItinerary tbi in lstIti)
                {
                    TBItinerary_Info tbinfo = tbinfod.ObterPorQuotationIdTabela((int)tbi.idTabela, model.qCode, model.optQuote);

                    if (tbinfo == null)
                    {
                        tbinfo = new TBItinerary_Info();
                        tbinfo.Note = conte;
                        tbinfo.Quotation_Code = model.qCode;
                        tbinfo.IdTabela = (int)tbi.idTabela;
                        tbinfo.optQuote = model.optQuote;
                        tbinfod.Salvar(tbinfo);
                    }
                    else
                    {
                        tbinfo.Note = conte;
                        tbinfo.ID = tbinfo.ID;
                        tbinfod.Atualizar(tbinfo);
                    }
                }

                //sectionPopUpSucesso.Style.Add("display", "block");

                return Json(new { success = true, message = "Atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region ImagesHightLight

        public string insereFoto(int fileid, int optQuote, string qcode)
        {
            try
            {
                FileTransfersDAL ftrDal = new FileTransfersDAL();
                S_Servicos lServ = new S_Servicos();
                var lstft = ftrDal.ListarTodosOpt(fileid, optQuote);

                string str = "";

                if (lstft.Where(a => a.Trf_Tours == "TRS").Count() > 0)
                {
                    lServ = new ServicoDAL().ObterPorNome(lstft.Where(a => a.Trf_Tours == "TRS").First().Transf_nome);
                    Servicos_Imagem siFirst = new FotoServicoDAL().ObterImgPrincipal(lServ.Servicos_Id);

                    if (siFirst != null)
                        str = string.Format("../../Galeria/Servicos/{0}/{1}", lServ.Servicos_Id, siFirst.Serv_img_nome);
                    else
                        str = "";


                    if (siFirst != null)
                        AddImage(qcode, optQuote, true, string.Format("~/Galeria/Servicos/{0}/{1}", siFirst.Servicos_Id, siFirst.Serv_img_nome));
                }

                if (lstft.Where(a => a.Trf_Tours == "TRS").Count() > 0)
                {
                    lServ = new ServicoDAL().ObterPorNome(lstft.Where(a => a.Trf_Tours == "TRS").Last().Transf_nome);
                    Servicos_Imagem siLast = new FotoServicoDAL().ObterImgPrincipal(lServ.Servicos_Id);

                    if (siLast != null)
                        str = str + "°" + string.Format("../../Galeria/Servicos/{0}/{1}", lServ.Servicos_Id, siLast.Serv_img_nome);
                    else
                        str = str + "°" + "";

                    if (siLast != null)
                        AddImage(qcode, optQuote, false, string.Format("~/Galeria/Servicos/{0}/{1}", lServ.Servicos_Id, siLast.Serv_img_nome));
                }

                return str;
            }
            catch
            {
                throw;
            }
        }

        public bool AddImage(string qcod, int optq, bool capa, string ulr)
        {

            TBItinerary_HighlightsImagesDAL thid = new TBItinerary_HighlightsImagesDAL();
            TBItinerary_HighlightsImages thi = thid.ObterPorCode(qcod, optq);

            if (thi == null)
                thi = new TBItinerary_HighlightsImages();


            thi.Code = qcod;
            thi.optQuote = optq;

            var guid = Guid.NewGuid().ToString();

            System.IO.File.Copy(Server.MapPath(ulr), Server.MapPath("../../Galeria/TBItinerary/" + guid + ".jpg"));


            if (capa)
                thi.FirstImage = guid + ".jpg";
            else
                thi.LastImage = guid + ".jpg";


            if (thi.Id == 0)
            {
                thid.Adicionar(thi);
            }
            else
            {
                thid.Atualizar(thi);
            }


            return true;
        }


        public string insereFotoPDF(int fileid, int optQuote)
        {
            try
            {
                FileTransfersDAL ftrDal = new FileTransfersDAL();
                S_Servicos lServ = new S_Servicos();
                var lstft = ftrDal.ListarTodosOpt(fileid, optQuote);

                string str = "";

                if (lstft.Where(a => a.Trf_Tours == "TRS").Count() > 0)
                {
                    lServ = new ServicoDAL().ObterPorNome(lstft.Where(a => a.Trf_Tours == "TRS").First().Transf_nome);
                    Servicos_Imagem siFirst = new FotoServicoDAL().ObterImgPrincipal(lServ.Servicos_Id);

                    if (siFirst != null)
                        str = string.Format("http://54.232.209.130/TierraTMVC/Galeria/Servicos/{0}/{1}", lServ.Servicos_Id, siFirst.Serv_img_nome);
                    else
                        str = "";


                    //if (siFirst != null)
                    //    AddImage(true, string.Format("~/Galeria/Servicos/{0}/{1}", firstServ.Servicos_Id, siFirst.Serv_img_nome));
                }

                if (lstft.Where(a => a.Trf_Tours == "TRS").Count() > 0)
                {
                    lServ = new ServicoDAL().ObterPorNome(lstft.Where(a => a.Trf_Tours == "TRS").Last().Transf_nome);
                    Servicos_Imagem siLast = new FotoServicoDAL().ObterImgPrincipal(lServ.Servicos_Id);

                    if (siLast != null)
                        str = str + "°" + string.Format("http://54.232.209.130/TierraTMVC/Galeria/Servicos/{0}/{1}", lServ.Servicos_Id, siLast.Serv_img_nome);
                    else
                        str = str + "°" + "";

                    //if (siLast != null)
                    //    AddImage(false, string.Format("~/Galeria/Servicos/{0}/{1}", lastServ.Servicos_Id, siLast.Serv_img_nome));
                }

                return str;
            }
            catch
            {
                throw;
            }
        }

        public JsonResult EnviaFotosHightLight(string qcod, int optq, int type)
        {
            string guid = "";

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];

                guid = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(pic.FileName);

                TBItinerary_HighlightsImagesDAL thid = new TBItinerary_HighlightsImagesDAL();
                TBItinerary_HighlightsImages thi = thid.ObterPorCode(qcod, optq);

                if (thi == null)
                {
                    thi = new TBItinerary_HighlightsImages();
                    thi.Code = qcod;
                    thi.optQuote = optq;
                }

                if (!Directory.Exists(Server.MapPath("~/Galeria/TBItinerary")))
                    Directory.CreateDirectory(Server.MapPath("~/Galeria/TBItinerary"));


                pic.SaveAs(Server.MapPath("~/Galeria/TBItinerary/" + guid));

                if (type == 1)
                    thi.FirstImage = guid;
                else
                    thi.LastImage = guid;


                if (thi.Id == 0)
                {
                    thid.Adicionar(thi);
                }
                else
                {
                    thid.Atualizar(thi);
                }

            }

            return Json(new { success = true, message = "Enviado com sucesso.", urlFoto = guid }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Itinerary

        protected string retornaUrlIcone(string flag, int idSpp, string lblSNome, int idTabela)
        {

            string url = "";
            string UrlEmpresa = ConfigurationManager.AppSettings["UrlEmpresa"].ToString();
            bool visible = true;
            Supplier sup = new SupplierDAL().ObterPorId(idSpp);


            FileTarifasDAL ftDal = new FileTarifasDAL();
            FileTransfersDAL ftrDal = new FileTransfersDAL();
            FileServExtraDAL fserDal = new FileServExtraDAL();
            ServicoDAL servd = new ServicoDAL();

            switch (flag)
            {
                case "HOTEL":

                    visible = true;
                    url = UrlEmpresa + "/images/Icones/hotel-icon-mini.png";

                    if (sup != null)
                    {

                        if (sup.S_Categoria_Escrita != null)
                        {
                            if (sup.Categ_id == 27)
                            {
                                url = UrlEmpresa + "/images/Icones/Amazon.png";
                            }
                            else if (sup.Categ_id == 28)
                            {
                                url = UrlEmpresa + "/images/Icones/Lodge.png";
                            }
                        }

                        //if (sup.S_Categoria_Escrita != null)
                        //{
                        //    if (sup.S_Categoria_Escrita.CategoriaEscrita_nome == "Lodge")
                        //    {
                        //        if (lblSNome.ToUpper().Contains("AMAZON"))
                        //            url = UrlEmpresa + "/images/Icones/Lodge.png";
                        //        if (lblSNome.ToUpper().Contains("PANTANAL"))
                        //            url = UrlEmpresa + "/images/Icones/Amazon.png";
                        //    }
                        //}
                    }

                    File_Tarifas ft = ftDal.ObterPorId(idTabela);

                    //if (ft.TBulk == true)
                    //    lblopcoes.Text = lblopcoes.Text + " BULK ";

                    //if (ft.TOptional == true)
                    //    lblopcoes.Text = lblopcoes.Text + " Optional ";

                    break;

                case "MEAL":

                    visible = true;
                    url = UrlEmpresa + "/images/Icones/breakfast.png";

                    File_Tarifas fta = ftDal.ObterPorId(idTabela);

                    //if (fta.TBulk == true)
                    //    lblopcoes.Text = lblopcoes.Text + " BULK ";

                    //if (fta.TOptional == true)
                    //    lblopcoes.Text = lblopcoes.Text + " Optional ";

                    break;

                case "TRANSFER":

                    visible = true;

                    url = UrlEmpresa + "/images/Icones/transfer-icon-mini2.png";

                    File_Transfers ftr = ftrDal.ObterPorId(idTabela);
                    S_Servicos serv = new S_Servicos();


                    if (ftr.Servicos_Id != null)
                        serv = servd.ObterPorId((int)ftr.Servicos_Id);

                    if (serv.Servicos_Nome == null)
                    {
                        serv = servd.ObterPorNome(ftr.Transf_nome, (int)ftr.Trf_CID_id);
                    }

                    S_Servicos_Tipo_Categ categServ = new ServicoTipoCategDAL().ObterPorId((int)serv.Tipo_categ_id);

                    if (categServ.Tipo_categ_nome.Equals("Flight"))
                    {
                        url = UrlEmpresa + "/images/Icones/iconFlightOffer.jpg";
                    }
                    else
                    {
                        if ((serv.SIB != null) && (serv.SIB == true))
                            url = UrlEmpresa + "/images/Icones/transfer-icon-mini-group.png";
                        else
                            url = UrlEmpresa + "/images/Icones/transfer-icon-mini-private.png";
                    }

                    //if (ftr.TBulk == true)
                    //    lblopcoes.Text = lblopcoes.Text + " BULK ";

                    //if (ftr.TOptional == true)
                    //    lblopcoes.Text = lblopcoes.Text + " Optional ";

                    break;

                case "TOUR":

                    visible = true;
                    url = UrlEmpresa + "/images/Icones/tour-mini.png";

                    File_Transfers ftrr = ftrDal.ObterPorId(idTabela);
                    S_Servicos ssserv = new S_Servicos();


                    if (ftrr.Servicos_Id != null)
                        ssserv = servd.ObterPorId((int)ftrr.Servicos_Id);


                    if (ssserv.Servicos_Nome == null)
                    {
                        ssserv = servd.ObterPorNome(ftrr.Transf_nome, (int)ftrr.Trf_CID_id);
                    }


                    if (ssserv.SIB != null)
                    {
                        if (ssserv.SIB == true)
                            url = UrlEmpresa + "/images/Icones/tour-mini-group.png";
                        else if (ssserv.SIB == false)
                            url = UrlEmpresa + "/images/Icones/tour-mini-private.png";

                        //imgIcon.Style.Add("height", "50px");
                    }

                    //if (ftrr.TBulk == true)
                    //    lblopcoes.Text = lblopcoes.Text + " BULK ";

                    //if (ftrr.TOptional == true)
                    //    lblopcoes.Text = lblopcoes.Text + " Optional ";

                    S_Servicos serv_tour = servd.ObterPorNome(ftrr.Transf_nome, (int)ftrr.Trf_CID_id);

                    if (serv_tour != null)
                        if (serv_tour.Tipo_categ_id == new ServicoTipoCategDAL().ObterPorNome("Entrance Ticket").Tipo_categ_id)
                            url = UrlEmpresa + "/images/Icones/ticket.png";

                    if (sup != null)
                    {
                        if (sup.S_Categoria_Escrita != null)
                            if (sup.S_Categoria_Escrita.CategoriaEscrita_nome == "Cruise")
                            {
                                url = UrlEmpresa + "/images/Icones/Cruise.png";
                                //imgIcon.Style.Add("height", "50px");
                            }
                    }

                    break;

                case "EXTRA":

                    File_ServExtra ftex = fserDal.ObterPorId(idTabela);

                    visible = true;

                    switch (ftex.TipoServico)
                    {
                        case "Transfer":
                            url = UrlEmpresa + "/images/Icones/transfer-icon.png";
                            break;
                        case "Extra":
                            url = UrlEmpresa + "/images/Icones/extraSpecial.png";
                            break;
                        case "Excursion":
                            url = UrlEmpresa + "/images/Icones/tour.png";
                            break;
                    }                                                                            


                    //if (ftex.TBulk == true)
                    //    lblopcoes.Text = lblopcoes.Text + " BULK ";

                    //if (ftex.TOptional == true)
                    //    lblopcoes.Text = lblopcoes.Text + " Optional ";


                    break;

                case "SubServ":

                    visible = true;
                    url = UrlEmpresa + "/images/Icones/transfer-icon-mini2.png";

                    File_Transfers ftsb = ftrDal.ObterPorId(idTabela);
                    S_Servicos sserv = new S_Servicos();


                    if (ftsb.Servicos_Id != null)
                    {
                        sserv = new ServicoDAL().ObterPorId((int)ftsb.Servicos_Id);
                    }


                    if (sserv.Servicos_Nome == null)
                    {
                        sserv = servd.ObterPorNome(ftsb.Transf_nome, (int)ftsb.Trf_CID_id);
                    }

                    //if (ftsb.Trf_Tours.ToUpper() == "TRF")
                    //{
                    //    url = UrlEmpresa + "/images/Icones/transfer-icon-mini2.png";
                    //}
                    //else 

                    if (sserv != null)
                    {
                        if (sserv.SIB == null || sserv.SIB == false)
                            if (sserv.Servicos_transfer != null && sserv.Servicos_transfer.ToLower() == "s")
                                url = UrlEmpresa + "/images/Icones/transfer-icon-mini-private.png";
                            else
                                url = UrlEmpresa + "/images/Icones/tour-mini-private.png";
                        else
                            if (sserv.Servicos_transfer != null && sserv.Servicos_transfer.ToLower() == "s")
                            url = UrlEmpresa + "/images/Icones/transfer-icon-mini-group.png";
                        else
                            url = UrlEmpresa + "/images/Icones/tour-mini-group.png";


                        if (sserv.Tipo_categ_id == new ServicoTipoCategDAL().ObterPorNome("Entrance Ticket").Tipo_categ_id)
                            url = UrlEmpresa + "/images/Icones/ticket.png";

                    }

                    //if (ftsb.TBulk == true)
                    //    lblopcoes.Text = lblopcoes.Text + " BULK ";

                    //if (ftsb.TOptional == true)
                    //    lblopcoes.Text = lblopcoes.Text + " Optional ";

                    break;
            }

            return url;
        }

        protected string retornaDescricao(string flag, int Sid, int idTabela, string qCode, int optQuote, string titulo, string cidade)
        {
            try
            {
                DescricaoDAL dd = new DescricaoDAL();
                TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
                ServicoDAL servd = new ServicoDAL();
                CidadeDAL cd = new CidadeDAL();

                TBItinerary_Info tbinfoGeral = tbinfod.ObterPorQuotationIdTabela(idTabela, qCode, optQuote);

                switch (flag)
                {
                    case "HOTEL":

                        S_Descricao sd = dd.ObterPorIdSupp(Sid);

                        if (sd != null)
                        {
                            string descr = sd.Desc_short + "</br>" + sd.Desc_long;

                            if (tbinfoGeral != null)
                            {
                                if (!string.IsNullOrEmpty(tbinfoGeral.Descricao))
                                    descr = tbinfoGeral.Descricao;
                            }

                            return descr;
                        }
                        else
                        {
                            return "No description";
                        }

                    case "TOUR":
                    case "TRANSFER":

                        S_Servicos sserv = servd.ObterPorNome(titulo, cd.ObterPorNome(cidade).CID_id);

                        if (sserv.Servicos_descr != null)
                        {
                            string descr2 = sserv.Servicos_descr;

                            if (tbinfoGeral != null)
                            {
                                if (!string.IsNullOrEmpty(tbinfoGeral.Descricao))
                                    descr2 = tbinfoGeral.Descricao;
                            }

                            return descr2;

                        }
                        else
                        {
                            return "No description";
                        }

                    case "EXTRA":

                        S_Descricao sd2 = dd.ObterPorIdSupp(Sid);

                        string descr3 = "";

                        if (sd2 != null)
                            descr3 = sd2.Desc_short + "</br>" + sd2.Desc_long;

                        if (tbinfoGeral != null)
                        {
                            if (!string.IsNullOrEmpty(tbinfoGeral.Descricao))
                            {
                                descr3 = tbinfoGeral.Descricao;
                            }
                            else
                            {
                                return "No description";
                            }

                            return descr3;
                        }
                        else
                        {
                            return "No description";
                        }

                    case "MEAL":
                        return "";

                    case "SubServ":

                        S_Servicos subserv = servd.ObterPorNome(titulo, cd.ObterPorNome(cidade).CID_id);

                        if (subserv.Servicos_descr != null)
                        {
                            string descr2 = subserv.Servicos_descr;

                            if (tbinfoGeral != null)
                            {
                                if (!string.IsNullOrEmpty(tbinfoGeral.Descricao))
                                    descr2 = tbinfoGeral.Descricao;
                            }

                            return descr2;

                        }
                        else
                        {
                            return "No description";
                        }


                }


                return "";
            }
            catch
            {
                throw;
            }
        }

        protected string retornaPacote(DateTime dtFrom, DateTime dtTo, int Sid, string nomePacote, int idTabela, int optquote, string qcode)
        {
            TBItinerary_DescritivoPacote tbidp = new TBItinerary_DescritivoPacoteDAL().ObterPacote(dtFrom, dtTo, Sid, nomePacote);
            DescritivoPacoteDAL dpd = new DescritivoPacoteDAL();

            DescritivoOptionsDAL doDal = new DescritivoOptionsDAL();

            TBItineraryCodePacoteDAL tbCodePacote = new TBItineraryCodePacoteDAL();
            var PacCode = tbCodePacote.obterpacote(idTabela, optquote, qcode);

            if (PacCode != null)
            {
                return PacCode.TBItinerary_codePacote;
            }

            string DescricaoPac = "";

            if ((tbidp == null) || (tbidp.Descritivo_id <= 0))
            {
                Descritivo_Pacote dpcote = dpd.ObterPacote(dtFrom, dtTo, Sid, nomePacote);
                double numDays = (dtTo - dtFrom).TotalDays + 1;
                int d = 0;

                if (dpcote != null)
                {

                    DescritivoOptions_Pacote dopt0 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 0);
                    if (dopt0 != null)
                    {
                        string cab0 = RetornaDescrOptions(dopt0, DescricaoPac, dtFrom);
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                    }
                    else
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                    }

                    DescricaoPac = DescricaoPac + dpcote.Descritivo_descr0;
                    DescricaoPac = DescricaoPac + "<br /><br />";
                    d++;

                    if (dpcote.Descritivo_descr1 != null)
                    {
                        if (dpcote.Descritivo_descr1.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt1 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 1);
                            if (dopt1 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt1, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(1).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(1).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }
                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(1).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr1;
                            DescricaoPac = DescricaoPac + "<br /><br />";
                            d++;
                        }
                    }


                    if (dpcote.Descritivo_descr2 != null)
                    {
                        if (dpcote.Descritivo_descr2.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt2 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 2);
                            if (dopt2 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt2, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(2).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(2).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }
                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(2).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr2;
                            DescricaoPac = DescricaoPac + "<br /><br />";
                            d++;
                        }
                    }


                    if (dpcote.Descritivo_descr3 != null)
                    {
                        if (dpcote.Descritivo_descr3.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt3 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 3);
                            if (dopt3 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt3, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(3).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(3).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }
                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(3).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr3;
                            DescricaoPac = DescricaoPac + "<br /><br />";
                            d++;
                        }
                    }

                    if (dpcote.Descritivo_descr4 != null)
                    {
                        if (dpcote.Descritivo_descr4.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt4 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 4);
                            if (dopt4 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt4, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(4).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(4).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }
                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(4).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr4;
                            DescricaoPac = DescricaoPac + "<br /><br />";
                            d++;
                        }
                    }

                    if (dpcote.Descritivo_descr5 != null)
                    {
                        if (dpcote.Descritivo_descr5.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt5 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 5);
                            if (dopt5 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt5, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(5).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(5).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }
                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(5).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr5;
                            DescricaoPac = DescricaoPac + "<br /><br />";
                            d++;
                        }
                    }

                    if (dpcote.Descritivo_descr6 != null)
                    {
                        if (dpcote.Descritivo_descr6.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt6 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 6);
                            if (dopt6 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt6, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(6).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(6).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }
                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(6).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr6;
                            DescricaoPac = DescricaoPac + "<br /><br />";
                            d++;
                        }
                    }

                    if (dpcote.Descritivo_descr7 != null)
                    {
                        if (dpcote.Descritivo_descr7.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt7 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 7);
                            if (dopt7 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt7, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(7).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(7).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }

                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(7).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr7;
                            DescricaoPac = DescricaoPac + "<br /><br />";
                            d++;
                        }
                    }

                    if (dpcote.Descritivo_descr8 != null)
                    {
                        if (dpcote.Descritivo_descr8.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt8 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 8);
                            if (dopt8 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt8, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(8).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(8).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }

                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(8).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr8;
                            DescricaoPac = DescricaoPac + "<br /><br />";
                            d++;
                        }
                    }


                    if (dpcote.Descritivo_descr9 != null)
                    {
                        if (dpcote.Descritivo_descr9.ToString() != "" && d != numDays)
                        {
                            DescritivoOptions_Pacote dopt9 = doDal.ObterPorDecritivo(dpcote.Descritivo_id, 9);
                            if (dopt9 != null)
                            {
                                string cab0 = RetornaDescrOptions(dopt9, DescricaoPac, dtFrom);
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(9).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";
                            }
                            else
                            {
                                DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(9).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            }

                            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(9).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                            DescricaoPac = DescricaoPac + dpcote.Descritivo_descr9;
                            d++;
                        }
                    }

                }
            }
            else
            {
                double numDays = (dtTo - dtFrom).TotalDays + 1;
                int d = 0;

                if (tbidp != null)
                {
                    DescricaoPac = DescricaoPac + "<b>" + dtFrom.ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                    DescricaoPac = DescricaoPac + tbidp.Descritivo_descr0;
                    DescricaoPac = DescricaoPac + "<br /><br />";
                    d++;

                    if (tbidp.Descritivo_descr1.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(1).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr1;
                        DescricaoPac = DescricaoPac + "<br /><br />";
                        d++;
                    }

                    if (tbidp.Descritivo_descr2.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(2).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr2;
                        DescricaoPac = DescricaoPac + "<br /><br />";
                        d++;
                    }

                    if (tbidp.Descritivo_descr3.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(3).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr3;
                        DescricaoPac = DescricaoPac + "<br /><br />";
                        d++;
                    }

                    if (tbidp.Descritivo_descr4.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(4).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr4;
                        DescricaoPac = DescricaoPac + "<br /><br />";
                        d++;
                    }
                    if (tbidp.Descritivo_descr5.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(5).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr5;
                        DescricaoPac = DescricaoPac + "<br /><br />";
                        d++;
                    }

                    if (tbidp.Descritivo_descr6.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(6).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr6;
                        DescricaoPac = DescricaoPac + "<br /><br />";
                        d++;
                    }

                    if (tbidp.Descritivo_descr7.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(7).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr7;
                        DescricaoPac = DescricaoPac + "<br /><br />";
                        d++;
                    }

                    if (tbidp.Descritivo_descr8.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(8).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr8;
                        DescricaoPac = DescricaoPac + "<br /><br />";
                        d++;
                    }

                    if (tbidp.Descritivo_descr9.ToString() != "" && d != numDays)
                    {
                        DescricaoPac = DescricaoPac + "<b>" + dtFrom.AddDays(9).ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>";
                        DescricaoPac = DescricaoPac + tbidp.Descritivo_descr9;
                        d++;
                    }

                }
            }


            return DescricaoPac;
        }

        protected string RetornaDescrOptions(DescritivoOptions_Pacote dopt0, string DescricaoPac, DateTime dtFrom)
        {
            string cab0 = "";
            cab0 += dopt0.DescritivoOptions_breakfast != null ? dopt0.DescritivoOptions_breakfast + "    " : "";
            cab0 += dopt0.DescritivoOptions_lunch != null ? dopt0.DescritivoOptions_lunch + "    " : "";
            cab0 += dopt0.DescritivoOptions_boxlunch != null ? dopt0.DescritivoOptions_boxlunch + "    " : "";
            cab0 += dopt0.DescritivoOptions_dinner != null ? dopt0.DescritivoOptions_dinner + "    " : "";

            //DescricaoPac = DescricaoPac + "<b>" + dtFrom.ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-US")) + "</b><br/>" + cab0 + "<br />";                       

            return cab0;
        }

        public JsonResult retornaTitulo(int IdTbItine, int Sid, string code, int idTabela, int optquote, string tituloCompleto)
        {
            try
            {
                string tit = "";

                TBItineraryDAL tbDal = new TBItineraryDAL();
                TBItinerary_SupplierDAL tbSpDal = new TBItinerary_SupplierDAL();

                var tSp = tbSpDal.ObterPorSupplierId(Sid, code, idTabela, optquote);

                var conte = HttpUtility.UrlDecode(tituloCompleto, System.Text.Encoding.Default);

                if (tSp != null)
                {
                    tSp.S_Name = tSp.S_Name.Replace("Optional", "");
                    tit = tSp.S_Name;
                    //tit = tituloCompleto;
                }
                else
                {
                    //tit = tituloCompleto.Trim().Split('-')[0].Replace("&amp;", "&");
                    tit = conte;
                }


                return Json(new { success = true, message = "", titulo = tit }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult saveTitulo(int IdTbItine, int Sid, string code, int idTabela, int optquote, string tituloCompleto, string dtFrom, string dtTo)
        {
            try
            {
                TBItinerary_SupplierDAL tbSpDal = new TBItinerary_SupplierDAL();
                TBItineraryDAL tbDal = new TBItineraryDAL();

                var tSp = tbSpDal.ObterPorSupplierId(Sid, code, idTabela, optquote);

                if (tSp == null)
                {
                    TBItinerary_Supplier ts = new TBItinerary_Supplier();
                    ts.S_Id = Sid;
                    var tit1 = tituloCompleto.Trim().Replace("&amp;", "&");
                    ts.S_Name = tit1;
                    ts.Code = code;
                    ts.optQuote = optquote;
                    ts.idTabela = idTabela;
                    ts.DateFrom = Convert.ToDateTime(dtFrom);
                    tbSpDal.Adicionar(ts);
                }
                else
                {
                    var ts = tbSpDal.ObterPorSupplierId(Sid, code, idTabela, optquote);
                    ts.DateFrom = Convert.ToDateTime(dtFrom);
                    ts.S_Name = tituloCompleto;
                    tbSpDal.Atualizar(ts);
                }

                var tbb = tbDal.ObterPorId(IdTbItine);
                string nights = "";

                bool optional = false;
                switch (tbb.Flag)
                {
                    case "HOTEL":
                    case "MEAL":

                        var ft = new FileTarifasDAL().ObterPorId((int)tbb.idTabela);
                        if (ft.TOptional == true)
                            optional = true;

                        break;

                    case "TOUR":
                    case "TRANSFER":

                        var ftr = new FileTransfersDAL().ObterPorId((int)tbb.idTabela);
                        if (ftr.TOptional == true)
                            optional = true;

                        break;

                    case "EXTRA":

                        var ftx = new FileServExtraDAL().ObterPorId((int)tbb.idTabela);
                        if (ftx.TOptional == true)
                            optional = true;

                        break;


                }

                if (tbb.Flag.Equals("HOTEL"))
                {
                    nights = " - " + ((DateTime)tbb.DateTo - (DateTime)tbb.DateFrom).TotalDays.ToString() + "Night(s)";
                }
                else
                {
                    nights = "";
                }

                string optionalstr = "";
                if (optional)
                {
                    optionalstr = " - <span style=\"color:#ff0000;\">Optional</span>";
                }

                return Json(new { success = true, message = "", titulo = tituloCompleto + optionalstr + nights }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public class saveDescricaoModel
        {
            public int IdTbItine { get; set; }
            public string code { get; set; }
            public int optquote { get; set; }
            public string texto { get; set; }
            public int idTabela { get; set; }
        }

        public JsonResult saveDescricao(saveDescricaoModel model)
        {
            try
            {
                TBItineraryDAL tbd = new TBItineraryDAL();

                TBItinerary tbi = tbd.ObterPorId(model.IdTbItine);
                TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
                TBItinerary_Info tbinfo = tbinfod.ObterPorQuotationIdTabela((int)tbi.idTabela, tbi.Quotation_Code, model.optquote);

                var conte = HttpUtility.UrlDecode(model.texto, System.Text.Encoding.Default);

                if (tbinfo == null)
                {
                    tbinfo = new TBItinerary_Info();
                    tbinfo.Descricao = conte;
                    tbinfo.Quotation_Code = model.code;
                    tbinfo.IdTabela = (int)tbi.idTabela;
                    tbinfo.optQuote = model.optquote;
                    tbinfod.Salvar(tbinfo);
                }
                else
                {
                    tbinfo.Descricao = conte;
                    tbinfo.ID = tbinfo.ID;
                    tbinfod.Atualizar(tbinfo);
                }


                return Json(new { success = true, message = "Atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string retornaUrlFoto(int idTabela, string code, int optquote, string flag, int Sid)
        {
            try
            {

                if (idTabela == 529932)
                {

                }

                DescricaoDAL dd = new DescricaoDAL();

                TBItineraryDAL tbdal = new TBItineraryDAL();
                TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
                FotoDAL fd = new FotoDAL();
                string UrlEmpresa = ConfigurationManager.AppSettings["UrlEmpresa"].ToString();
                TBItinerary_Info tbinfoGeral = tbinfod.ObterPorQuotationIdTabela(idTabela, code, optquote);
                S_Image si = fd.ObterPorSupplierCapa(Sid);
                string fotoResize = "";

                var t = tbdal.ObterPorParam(code, optquote, idTabela);

                switch (t.Flag)
                {
                    case "TRANSFER":
                    case "TOUR":
                    case "SubServ":
                        ServicoDAL servd = new ServicoDAL();
                        FotoServicoDAL fsd = new FotoServicoDAL();
                        CidadeDAL cdal = new CidadeDAL();

                        S_Servicos sserv = servd.ObterPorNome(t.ServNome, cdal.ObterPorNome(t.Cidade).CID_id);
                        Servicos_Imagem servImg = fsd.ObterImgPrincipal(sserv.Servicos_Id);

                        if (tbinfoGeral != null)
                        {
                            if (tbinfoGeral.ImagemPrincipal != null)
                            {
                                if (tbinfoGeral.ImagemPrincipal.Equals("deleted"))
                                {
                                    return "";
                                }
                                else
                                {
                                    return UrlEmpresa + "/Galeria/imgPrincipalQuoteNew/" + tbinfoGeral.ImagemPrincipal;
                                }
                            }
                        }

                        if (servImg != null)
                        {
                            return UrlEmpresa + "/Galeria/Servicos/" + sserv.Servicos_Id + "/" + servImg.Serv_img_nome;
                        }

                        //if (sserv.Tipo_categ_id == 20)
                        //{
                        //    return UrlEmpresa + "/images/Icones/arrival.png";
                        //}


                        //else
                        //{
                        //    if (tbinfoGeral != null)
                        //    {
                        //        if (!string.IsNullOrEmpty(tbinfoGeral.ImagemPrincipal))
                        //        {
                        //            if (tbinfoGeral.ImagemPrincipal != "deleted")
                        //            {
                        //                return UrlEmpresa + "/Galeria/imgPrincipalQuoteNew/" + tbinfoGeral.ImagemPrincipal;
                        //            }
                        //        }
                        //        else
                        //        {
                        //            return UrlEmpresa + "/Galeria/noimage.jpg";
                        //        }
                        //    }
                        //    else
                        //    {
                        //        return UrlEmpresa + "/Galeria/noimage.jpg";
                        //    }
                        //}

                        return "";

                    case "HOTEL":

                        S_Image si1 = fd.ObterPorSupplierCapa((int)t.S_id);

                        if (tbinfoGeral != null)
                        {
                            if (tbinfoGeral.ImagemPrincipal != null)
                            {
                                if (tbinfoGeral.ImagemPrincipal.Equals("deleted"))
                                {
                                    return "";
                                }
                                else
                                {
                                    return UrlEmpresa + "/Galeria/imgPrincipalQuoteNew/" + tbinfoGeral.ImagemPrincipal;
                                }
                            }

                            //if (tbinfoGeral.ImagemPrincipal != null)
                            //{
                            //    if (tbinfoGeral.ImagemPrincipal.Equals("deleted"))
                            //    {
                            //        return "";
                            //    }
                            //}
                        }

                        if (si1 != null)
                        {
                            return UrlEmpresa + "/Galeria/" + (int)t.S_id + "/" + si.Image_nome;
                        }
                        else
                        {
                            if (tbinfoGeral != null)
                            {
                                if (!string.IsNullOrEmpty(tbinfoGeral.ImagemPrincipal))
                                {
                                    if (tbinfoGeral.ImagemPrincipal != "deleted")
                                    {
                                        return UrlEmpresa + "/Galeria/imgPrincipalQuoteNew/" + tbinfoGeral.ImagemPrincipal;
                                    }
                                }
                                else
                                {
                                    return UrlEmpresa + "/Galeria/noimage.jpg";
                                }
                            }
                            else
                            {
                                return UrlEmpresa + "/Galeria/noimage.jpg";
                            }
                        }
                        return "";

                }

                if (si != null)
                {
                    fotoResize = "/Galeria/" + Sid + "/" + si.Image_nome;

                    if (tbinfoGeral != null)
                        if (!string.IsNullOrEmpty(tbinfoGeral.ImagemPrincipal))
                        {
                            if (tbinfoGeral.ImagemPrincipal != "deleted")
                            {
                                return UrlEmpresa + "/Galeria/imgPrincipalQuoteNew/" + tbinfoGeral.ImagemPrincipal;
                            }
                        }
                        else
                        {
                            return UrlEmpresa + fotoResize;
                        }
                    else
                        return UrlEmpresa + fotoResize;
                }
                else
                {

                    fotoResize = "/Galeria/noimage.jpg";

                    if (tbinfoGeral != null)
                        if (!string.IsNullOrEmpty(tbinfoGeral.ImagemPrincipal))
                        {
                            if (tbinfoGeral.ImagemPrincipal != "deleted")
                            {
                                return UrlEmpresa + "/Galeria/imgPrincipalQuoteNew/" + tbinfoGeral.ImagemPrincipal;
                            }
                        }
                        else
                        {
                            return UrlEmpresa + fotoResize;
                        }
                    else
                        return UrlEmpresa + fotoResize;
                }

                return fotoResize;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public JsonResult enviaFotoItinerary(int itiID, int tabID, string quotationCode, int optQuote)
        {

            string guid = "";

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];

                guid = Guid.NewGuid().ToString("n") + System.IO.Path.GetExtension(pic.FileName);

                if (!Directory.Exists(Server.MapPath("~/Galeria/imgPrincipalQuoteNew")))
                    Directory.CreateDirectory(Server.MapPath("~/Galeria/imgPrincipalQuoteNew"));


                pic.SaveAs(Server.MapPath("~/Galeria/imgPrincipalQuoteNew/" + guid));


                TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
                TBItinerary_Info tbinfo = tbinfod.ObterPorQuotationIdTabela(tabID, quotationCode, optQuote);

                if (tbinfo == null)
                {
                    tbinfo = new TBItinerary_Info();
                    tbinfo.ImagemPrincipal = guid;
                    tbinfo.Quotation_Code = quotationCode;
                    tbinfo.IdTabela = tabID;
                    tbinfo.optQuote = optQuote;
                    tbinfod.Salvar(tbinfo);
                }
                else
                {
                    tbinfo.ImagemPrincipal = guid;
                    tbinfo.ID = tbinfo.ID;
                    tbinfod.Atualizar(tbinfo);
                }

                return Json(new { success = true, message = "Enviado com sucesso.", urlFoto = guid }, JsonRequestBehavior.AllowGet);
            }


            return Json(new { success = false, message = "Houve um erro.", urlFoto = guid }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult deleteImage(int idTabela, string quotationCode, int optQuote)
        {
            TBItinerary_InfoDAL tbinfod = new TBItinerary_InfoDAL();
            TBItinerary_Info tbinfoGeral = tbinfod.ObterPorQuotationIdTabela(idTabela, quotationCode, optQuote);

            if (tbinfoGeral == null)
                tbinfoGeral = new TBItinerary_Info();

            tbinfoGeral.ImagemPrincipal = "deleted";

            if (tbinfoGeral.ID > 0)
                tbinfod.Atualizar(tbinfoGeral);
            else
                tbinfod.Salvar(tbinfoGeral);


            return Json(new { success = true, message = "Foto excluída com sucesso." }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveDescricaoPacote(saveDescricaoModel model)
        {
            try
            {
                TBItineraryCodePacoteDAL tbinfod = new TBItineraryCodePacoteDAL();
                TBItinerary_CodePacote tbinfo = tbinfod.obterpacote(model.idTabela, model.optquote, model.code);

                var conte = HttpUtility.UrlDecode(model.texto, System.Text.Encoding.Default);

                if (tbinfo == null)
                {
                    tbinfo = new TBItinerary_CodePacote();
                    tbinfo.TBItinerary_codePacote = conte;
                    tbinfo.Quotation_Code = model.code;
                    tbinfo.idTabela = model.idTabela;
                    tbinfo.optQuote = model.optquote;
                    tbinfod.Salvar(tbinfo);
                }
                else
                {
                    tbinfo.TBItinerary_codePacote = conte;
                    tbinfod.Atualizar(tbinfo);
                }


                return Json(new { success = true, message = "Atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public class gridranges
        {
            public string Room { get; set; }
            public string RoomSub { get; set; }
            public string Base { get; set; }
            public string Price { get; set; }
            public string OldPrice { get; set; }
            public string Preco_Notes { get; set; }
            public string qCode { get; set; }
            public int optQuote { get; set; }
            public bool visible { get; set; }
        }

        protected List<gridranges> montaPrices(string qCode, int optQuote, int ratear)
        {
            List<gridranges> lstGR = new List<gridranges>();
            try
            {

                RangeDal rd = new RangeDal();
                FileCarrinhoDAL fcd = new FileCarrinhoDAL();
                TBItineraryDAL tbid = new TBItineraryDAL();

                Quotation _quotation = new QuotationDAL().ObterPorCode(qCode);
                Quotation_Grupo _qgrupo = new QuotationGrupoDAL().ObterPorIdQuotation(_quotation.Quotation_Id);

                int IdFile = fcd.ObterPorIdGrupo(_qgrupo.Quotation_Grupo_Id).File_id;
                int IdQuote = fcd.RetornaQuote(IdFile);

                int qtdRooms = tbid.QtdRooms(qCode, optQuote);
                string[] rooms = new string[qtdRooms];
                int k = 0;

                foreach (string nome in tbid.ObterRoomsCode(qCode, optQuote))
                {
                    rooms[k] = nome;
                    k++;
                }
                k = 0;


                int qtdBases = rd.QtdBases(IdQuote, optQuote);
                string[] bases = new string[qtdBases];

                int i = 0;
                foreach (string item in rd.RetornaTodasBases(IdQuote, optQuote))
                {
                    bases[i] = item;
                    i++;
                }

                i = 0;

                string[] basesT = bases[i].ToString().Split('/');

                TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();

                for (int l = 0; l < qtdBases; l++)
                {
                    for (int ro = 0; ro < rooms.Length; ro++)
                    {
                        decimal somatorio = 0;

                        if (ratear == 0)
                        {
                            if (bases[l].ToString().Contains("TC"))
                                continue;
                            somatorio = SomaDasBases_Rateado(qCode, rooms, k, l, optQuote);
                        }
                        else
                            somatorio = SomaDasBases(qCode, rooms, k, l, optQuote);

                        gridranges gr = new gridranges();


                        var price = tbpd.ObterPorBaseAndRoom(qCode, bases[l], rooms[k], optQuote);
                        string sum = somatorio.ToString("N2");
                        gr.OldPrice = sum;

                        if (price != null)
                        {
                            sum = price.PrecoAlterado;
                            gr.visible = price.visible == false ? false : true;
                            //drw["Preco_Notes"] = price.Preco_Notes;
                            //drw["Preco_Link"] = price.Preco_Link == null ? "" : price.Preco_Link;
                        }
                        else
                        {
                            gr.visible = true;
                        }

                        switch (rooms[k])
                        {
                            case "Single":
                                gr.Room = "Total per person in Single (Travelers from ";
                                gr.Base = bases[l] + ")";
                                gr.Price = sum;
                                break;
                            case "Double":
                                gr.Room = "Total per person in Double (Travelers from ";
                                gr.Base = bases[l] + ")";
                                gr.Price = sum;
                                break;
                            case "Triple":
                                gr.Room = "Total per person in Triple (Travelers from ";
                                gr.Base = bases[l] + ")";
                                gr.Price = sum;
                                break;
                            case "Quadruple":
                                gr.Room = "Total per person in Quadruple (Travelers from ";
                                gr.Base = bases[l] + ")";
                                gr.Price = sum;
                                break;
                        }

                        gr.optQuote = optQuote;
                        gr.qCode = qCode;

                        lstGR.Add(gr);

                        if (rooms.Length > 1)
                            k++;
                    }
                    k = 0;
                }

                return lstGR;

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return lstGR;
            }
        }

        protected List<gridranges> montaPrices_link(string qCode, int optQuote, int ratear)
        {
            List<gridranges> lstGR = new List<gridranges>();
            try
            {

                RangeDal rd = new RangeDal();
                FileCarrinhoDAL fcd = new FileCarrinhoDAL();
                TBItineraryDAL tbid = new TBItineraryDAL();

                Quotation _quotation = new QuotationDAL().ObterPorCode(qCode);
                Quotation_Grupo _qgrupo = new QuotationGrupoDAL().ObterPorIdQuotation(_quotation.Quotation_Id);

                int IdFile = fcd.ObterPorIdGrupo(_qgrupo.Quotation_Grupo_Id).File_id;
                int IdQuote = fcd.RetornaQuote(IdFile);

                int qtdRooms = tbid.QtdRooms(qCode, optQuote);
                string[] rooms = new string[qtdRooms];
                int k = 0;

                foreach (string nome in tbid.ObterRoomsCode(qCode, optQuote))
                {
                    rooms[k] = nome;
                    k++;
                }
                k = 0;


                int qtdBases = rd.QtdBases(IdQuote, optQuote);
                string[] bases = new string[qtdBases];

                int i = 0;
                foreach (string item in rd.RetornaTodasBases(IdQuote, optQuote))
                {
                    bases[i] = item;
                    i++;
                }

                i = 0;

                string[] basesT = bases[i].ToString().Split('/');

                TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();

                for (int l = 0; l < qtdBases; l++)
                {
                    for (int ro = 0; ro < rooms.Length; ro++)
                    {
                        decimal somatorio = 0;

                        if (ratear == 0)
                        {
                            if (bases[l].ToString().Contains("TC"))
                                continue;
                            somatorio = SomaDasBases_Rateado(qCode, rooms, k, l, optQuote);
                        }
                        else
                            somatorio = SomaDasBases(qCode, rooms, k, l, optQuote);

                        gridranges gr = new gridranges();


                        var price = tbpd.ObterPorBaseAndRoom(qCode, bases[l], rooms[k], optQuote);
                        string sum = somatorio.ToString("N2");
                        gr.OldPrice = sum;

                        if (price != null)
                        {
                            sum = price.PrecoAlterado;
                            gr.visible = price.visible == false ? false : true;
                        }
                        else
                        {
                            gr.visible = true;
                        }

                        switch (rooms[k])
                        {
                            case "Single":
                                gr.Room = "Prices per person based on:";
                                gr.RoomSub = "Single Room";
                                gr.Base = bases[l] + " travelers";
                                gr.Price = sum;
                                break;
                            case "Double":
                                gr.Room = "Prices per person based on:";
                                gr.RoomSub = "Double Room";
                                gr.Base = bases[l] + " travelers";
                                gr.Price = sum;
                                break;
                            case "Triple":
                                gr.Room = "Prices per person based on:";
                                gr.RoomSub = "Triple Room";
                                gr.Base = bases[l] + " travelers";
                                gr.Price = sum;
                                break;
                            case "Quadruple":
                                gr.Room = "Prices per person based on:";
                                gr.RoomSub = "Quadruple Room";
                                gr.Base = bases[l] + " travelers";
                                gr.Price = sum;
                                break;
                        }

                        gr.optQuote = optQuote;
                        gr.qCode = qCode;

                        lstGR.Add(gr);

                        if (rooms.Length > 1)
                            k++;
                    }
                    k = 0;
                }

                return lstGR;

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return lstGR;
            }
        }

        protected decimal SomaDasBases(string Code, string[] rooms, int k, int bases, int optquote)
        {
            TBItineraryDAL tbid = new TBItineraryDAL();
            SqlCon con = new SqlCon();
            decimal retorno = 0;
            decimal somatorio = 0;

            try
            {
                string sql = "SELECT Itinerary_id, Code, idTabela, Quotation_Code, SuppNome, S_id, ServNome, ServExtrNome, DateFrom, DateTo, Cidade, Categoria, Room, Flag, Flight_voo, Flight_hora, Flight_pickUp, Ordem, ";
                sql = sql + "NomePacote, MealNome, DAY, Range01, Base01, Range02, Base02, Range03, Base03, Range04, Base04, Range05, Base05, Range06, Base06, Range07, Base07, Range08, Base08, Range09, ";
                sql = sql + "Base09, Range10, Base10, Range11, Base11, Range12, Base12, Range13, Base13, Range14, Base14, Range15, Base15, Range16, Base16, Range17, Base17 ";
                sql = sql + "FROM TBItinerary WHERE (Code = @p1) AND (OptQuote = @p2) ORDER BY DateFrom, Ordem ";
                con.AbrirConexao();
                con.Cmd = new SqlCommand(sql, con.Con);
                con.Cmd.Parameters.AddWithValue("@p1", Code);
                con.Cmd.Parameters.AddWithValue("@p2", optquote);
                con.Dr = con.Cmd.ExecuteReader();

                while (con.Dr.Read())
                {
                    if (con.Dr["Flag"].ToString().Equals("HOTEL") || con.Dr["Flag"].ToString().Equals("MEAL"))
                    {
                        File_Tarifas ft = new FileTarifasDAL().ObterPorId(Convert.ToInt32(con.Dr["idTabela"]));

                        if (ft.TOptional == true)
                            continue;

                        if (ft.TBulk == true)
                        {
                            if (rooms[k].Equals(ft.Room))
                            {
                                somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                                somatorio = Math.Round(somatorio, 2);
                            }
                        }
                        else
                        {
                            if (rooms[k].Equals(ft.Room))
                                somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);

                            if (ft.Meal == true && ft.S_meal_status == null)
                                somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                        }
                    }
                    else if (con.Dr["Flag"].ToString().Equals("TRANSFER") || con.Dr["Flag"].ToString().Equals("TOUR") || con.Dr["Flag"].ToString().Equals("SubServ"))
                    {
                        File_Transfers ftrans = new FileTransfersDAL().ObterPorId(Convert.ToInt32(con.Dr["idTabela"]));

                        if (ftrans.TOptional == true)
                            continue;

                        if (ftrans.TBulk == true)
                            somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                        else
                            somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                    }
                    else if (con.Dr["Flag"].ToString().Equals("EXTRA"))
                    {
                        File_ServExtra fservext = new FileServExtraDAL().ObterPorId(Convert.ToInt32(con.Dr["idTabela"]));

                        if (fservext.TOptional == true)
                            continue;

                        if (fservext.TBulk == true)
                            somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                        else
                            somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                    }

                    retorno = somatorio;
                }
                return retorno;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return 0;
            }
            finally
            {
                con.FecharConexao();
            }
        }

        protected decimal SomaDasBases_Rateado(string Code, string[] rooms, int k, int bases, int optquote)
        {
            TBItineraryDAL tbid = new TBItineraryDAL();
            SqlCon con = new SqlCon();
            decimal retorno = 0;
            decimal somatorio = 0;

            try
            {
                string sql = "SELECT Itinerary_id, Code, idTabela, Quotation_Code, SuppNome, S_id, ServNome, ServExtrNome, DateFrom, DateTo, Cidade, Categoria, Room, Flag, Flight_voo, Flight_hora, Flight_pickUp, Ordem, ";
                sql = sql + "NomePacote, MealNome, DAY, Range01, Base01, Range02, Base02, Range03, Base03, Range04, Base04, Range05, Base05, Range06, Base06, Range07, Base07, Range08, Base08, Range09, ";
                sql = sql + "Base09, Range10, Base10, Range11, Base11, Range12, Base12, Range13, Base13, Range14, Base14, Range15, Base15, Range16, Base16, Range17, Base17 ";
                sql = sql + "FROM TBItinerary WHERE (Code = @p1) AND (OptQuote = @p2) ORDER BY DateFrom, Ordem ";

                con.AbrirConexao();
                con.Cmd = new SqlCommand(sql, con.Con);
                con.Cmd.Parameters.AddWithValue("@p1", Code);
                con.Cmd.Parameters.AddWithValue("@p2", optquote);
                con.Dr = con.Cmd.ExecuteReader();

                while (con.Dr.Read())
                {
                    if (con.Dr["Flag"].ToString().Equals("HOTEL") || con.Dr["Flag"].ToString().Equals("MEAL"))
                    {
                        File_Tarifas ft = new FileTarifasDAL().ObterPorId(Convert.ToInt32(con.Dr["idTabela"]));

                        if (ft.TOptional == true)
                            continue;

                        if (ft.TBulk == true)
                        {
                            decimal varTC = 0;
                            if (con.Dr[RetornaBase(bases + 1)].ToString().Contains("TC"))
                            {
                                int Bdivisao = Convert.ToInt32(con.Dr[RetornaBase(bases)].ToString().Split('/').First());
                                decimal Vdivisao = Convert.ToDecimal(con.Dr[RetornaRange(bases + 1)]);

                                varTC = Vdivisao / Bdivisao;
                            }
                            if (rooms[k].Equals(ft.Room))
                            {
                                somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                                somatorio = Math.Round(somatorio, 2);
                            }
                            if (varTC != 0)
                                somatorio = somatorio + varTC;
                        }
                        else
                        {
                            decimal varTC = 0;

                            if (con.Dr[RetornaBase(bases + 1)].ToString().Contains("TC"))
                            {
                                int Bdivisao = Convert.ToInt32(con.Dr[RetornaBase(bases)].ToString().Split('/').First());
                                decimal Vdivisao = Convert.ToDecimal(con.Dr[RetornaRange(bases + 1)]);
                                varTC = Vdivisao / Bdivisao;
                            }
                            if (rooms[k].Equals(ft.Room))
                                somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);

                            if (ft.Meal == true && ft.S_meal_status == null)
                                somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);

                            if (varTC != 0)
                                somatorio = somatorio + varTC;
                        }
                    }
                    else if (con.Dr["Flag"].ToString().Equals("TRANSFER") || con.Dr["Flag"].ToString().Equals("TOUR") || con.Dr["Flag"].ToString().Equals("SubServ"))
                    {
                        File_Transfers ftrans = new FileTransfersDAL().ObterPorId(Convert.ToInt32(con.Dr["idTabela"]));

                        if (ftrans.TOptional == true)
                            continue;

                        decimal varTC = 0;
                        if (con.Dr[RetornaBase(bases + 1)].ToString().Contains("TC"))
                        {
                            int Bdivisao = Convert.ToInt32(con.Dr[RetornaBase(bases)].ToString().Split('/').First());
                            decimal Vdivisao = Convert.ToDecimal(con.Dr[RetornaRange(bases + 1)]);

                            varTC = Vdivisao / Bdivisao;
                        }

                        if (ftrans.TBulk == true)
                            somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                        else
                            somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);

                        if (varTC != 0)
                            somatorio = somatorio + varTC;
                    }
                    else if (con.Dr["Flag"].ToString().Equals("EXTRA"))
                    {
                        File_ServExtra fservext = new FileServExtraDAL().ObterPorId(Convert.ToInt32(con.Dr["idTabela"]));

                        if (fservext.TOptional == true)
                            continue;

                        decimal varTC = 0;
                        if (con.Dr[RetornaBase(bases + 1)].ToString().Contains("TC"))
                        {
                            int Bdivisao = Convert.ToInt32(con.Dr[RetornaBase(bases)].ToString().Split('/').First());
                            decimal Vdivisao = Convert.ToDecimal(con.Dr[RetornaRange(bases + 1)]);

                            varTC = Vdivisao / Bdivisao;
                        }

                        if (fservext.TBulk == true)
                            somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);
                        else
                            somatorio = somatorio + Convert.ToDecimal(con.Dr[RetornaRange(bases)]);

                        if (varTC != 0)
                            somatorio = somatorio + varTC;
                    }

                    retorno = somatorio;
                }

                return retorno;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return 0;
            }
            finally
            {
                con.FecharConexao();
            }
        }

        public JsonResult SaveVisiblePrice(string qCode, string bases, string room, int optquote, string price, bool type)
        {
            try
            {
                SavePriceNew_Visible(qCode, bases, room, optquote, price, type);
                return Json(new { success = true, message = "Visible alterado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SavePriceNew(string qCode, string bases, string room, int optquote, string price)
        {
            try
            {
                TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();
                string bs = bases.Substring(0, bases.IndexOf(')'));

                string roomLimpa = "";

                if (room.ToLower().Contains("single"))
                    roomLimpa = "Single";
                else if (room.ToLower().Contains("double"))
                    roomLimpa = "Double";
                else if (room.ToLower().Contains("triple"))
                    roomLimpa = "Triple";
                else if (room.ToLower().Contains("quadruple"))
                    roomLimpa = "Quadruple";

                TBItinerary_Prices tbp = tbpd.ObterPorBaseAndRoom(qCode, bs, roomLimpa, optquote);

                if (tbp == null)
                {
                    TBItinerary_Prices tbp2 = new TBItinerary_Prices();
                    tbp2.Base = bs;
                    tbp2.QuotationCode = qCode;
                    tbp2.TipoRoom = roomLimpa;
                    tbp2.optQuote = optquote;
                    tbp2.PrecoAlterado = price;
                    tbp2.Preco_Notes = ".";
                    tbpd.Salvar(tbp2);
                }
                else
                {
                    tbp.PrecoAlterado = price;
                    tbpd.Atualizar(tbp);
                }

                return Json(new { success = true, message = "Preço alterado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public void SavePriceNew_Visible(string qCode, string bases, string room, int optquote, string price, bool type)
        {
            try
            {
                TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();
                string bs = bases.Substring(0, bases.IndexOf(')'));

                string roomLimpa = "";

                if (room.ToLower().Contains("single"))
                    roomLimpa = "Single";
                else if (room.ToLower().Contains("double"))
                    roomLimpa = "Double";
                else if (room.ToLower().Contains("triple"))
                    roomLimpa = "Triple";
                else if (room.ToLower().Contains("quadruple"))
                    roomLimpa = "Quadruple";

                TBItinerary_Prices tbp = tbpd.ObterPorBaseAndRoom(qCode, bs, roomLimpa, optquote);

                if (!type)
                {
                    if (tbp == null)
                    {
                        TBItinerary_Prices tbp2 = new TBItinerary_Prices();
                        tbp2.Base = bs;
                        tbp2.QuotationCode = qCode;
                        tbp2.TipoRoom = roomLimpa;
                        tbp2.optQuote = optquote;
                        tbp2.PrecoAlterado = price;
                        tbp2.Preco_Notes = ".";
                        tbp2.visible = false;
                        tbpd.Salvar(tbp2);
                    }
                    else
                    {
                        tbp.visible = false;
                        tbp.PrecoAlterado = price;
                        tbpd.Atualizar(tbp);
                    }
                }
                else
                {
                    if (tbp == null)
                    {
                        TBItinerary_Prices tbp2 = new TBItinerary_Prices();
                        tbp2.Base = bs;
                        tbp2.QuotationCode = qCode;
                        tbp2.TipoRoom = roomLimpa;
                        tbp2.optQuote = optquote;
                        tbp2.PrecoAlterado = price;
                        tbp2.Preco_Notes = ".";
                        tbp2.visible = true;
                        tbpd.Salvar(tbp2);
                    }
                    else
                    {
                        tbp.visible = true;
                        tbp.PrecoAlterado = price;
                        tbpd.Atualizar(tbp);
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public class SaveNotesLink
        {
            public string qCode { get; set; }
            public int optquote { get; set; }
            public string notes { get; set; }
            public string Room { get; set; }
            public string price { get; set; }
            public string bases { get; set; }
            public string link { get; set; }
        }

        public JsonResult SaveNotesPrices(SaveNotesLink model)
        {
            try
            {
                TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();
                var tbp = tbpd.ObterPorQuote(model.qCode, model.optquote);

                var conte = HttpUtility.UrlDecode(model.notes, System.Text.Encoding.Default);


                if (tbp.Count > 0)
                {
                    foreach (var item in tbp)
                    {
                        item.Preco_Notes = conte;
                        tbpd.Atualizar(item);
                    }
                }
                else
                {
                    string roomLimpa = "";

                    if (model.Room.ToLower().Contains("single"))
                        roomLimpa = "Single";
                    else if (model.Room.ToLower().Contains("double"))
                        roomLimpa = "Double";
                    else if (model.Room.ToLower().Contains("triple"))
                        roomLimpa = "Triple";
                    else if (model.Room.ToLower().Contains("quadruple"))
                        roomLimpa = "Quadruple";

                    string bs = model.bases.Substring(0, model.bases.IndexOf(')'));

                    TBItineraryDAL tbdDal = new TBItineraryDAL();
                    var tb = tbdDal.ObterPorQuote(model.qCode, model.optquote);

                    TBItinerary_Prices tbp2 = new TBItinerary_Prices();
                    tbp2.Base = bs;
                    tbp2.QuotationCode = model.qCode;
                    tbp2.TipoRoom = roomLimpa;
                    tbp2.optQuote = model.optquote;
                    tbp2.PrecoAlterado = model.price;
                    tbp2.Preco_Notes = conte;
                    tbpd.Salvar(tbp2);
                }

                return Json(new { success = true, message = "Preço alterado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveLinkPrices(SaveNotesLink model)
        {
            try
            {
                TBItinerary_PricesDAL tbpd = new TBItinerary_PricesDAL();
                var tbp = tbpd.ObterPorQuote(model.qCode, model.optquote);

                //var conte = HttpUtility.UrlDecode(model.notes, System.Text.Encoding.Default);


                if (tbp.Count > 0)
                {
                    foreach (var item in tbp)
                    {
                        item.Preco_Link = model.link;
                        tbpd.Atualizar(item);
                    }
                }
                else
                {
                    string roomLimpa = "";

                    if (model.Room.ToLower().Contains("single"))
                        roomLimpa = "Single";
                    else if (model.Room.ToLower().Contains("double"))
                        roomLimpa = "Double";
                    else if (model.Room.ToLower().Contains("triple"))
                        roomLimpa = "Triple";
                    else if (model.Room.ToLower().Contains("quadruple"))
                        roomLimpa = "Quadruple";

                    string bs = model.bases.Substring(0, model.bases.IndexOf(')'));

                    TBItineraryDAL tbdDal = new TBItineraryDAL();
                    var tb = tbdDal.ObterPorQuote(model.qCode, model.optquote);

                    TBItinerary_Prices tbp2 = new TBItinerary_Prices();
                    tbp2.Base = bs;
                    tbp2.QuotationCode = model.qCode;
                    tbp2.TipoRoom = roomLimpa;
                    tbp2.optQuote = model.optquote;
                    tbp2.PrecoAlterado = model.price;
                    tbp2.Preco_Notes = ".";
                    tbp2.Preco_Link = model.link;
                    tbpd.Salvar(tbp2);
                }

                return Json(new { success = true, message = "Preço alterado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Hoteis

        protected List<HoteisOfferModel> HotelsPorCodigo(string qCode, int optQuote)
        {
            List<HoteisOfferModel> lst = new List<HoteisOfferModel>();

            try
            {

                FileCarrinhoDAL fcd = new FileCarrinhoDAL();
                RangeDal rd = new RangeDal();

                var _itineraryByUserQuote = new TBItineraryDAL().ListarTodosCodeOpt(qCode, optQuote).FirstOrDefault();
                string UrlEmpresa = ConfigurationManager.AppSettings["UrlEmpresa"].ToString();

                Quotation _quotation = new QuotationDAL().ObterPorCode(qCode);
                Quotation_Grupo _qgrupo = new QuotationGrupoDAL().ObterPorIdQuotation(_quotation.Quotation_Id);

                int IdQuote = fcd.ObterPorIdGrupo(_qgrupo.Quotation_Grupo_Id).File_id;
                int lastSupplierID = 0;

                List<int> lastSupplierIDs = new List<int>();

                foreach (File_Tarifas ft in new FileTarifasDAL().ListarTodosOpt(IdQuote, optQuote))
                {
                    var rr = rd.ObterPorTarifa(ft.File_Tarifas_id, _quotation.Quotation_Id);
                    if (rr.Count == 0)
                        continue;

                    HoteisOfferModel ht = new HoteisOfferModel();

                    S_Categoria Categoria = new CategoriaDAL().ObterPorSupp(Convert.ToInt32(ft.S_id));

                    Supplier s = new SupplierDAL().ObterPorId(Convert.ToInt32(ft.S_id));

                   
                    if (lastSupplierID == s.S_id)
                        continue;


                    if (lastSupplierIDs.Contains(s.S_id))
                        continue;

                    lastSupplierID = s.S_id;
                    lastSupplierIDs.Add(s.S_id);

                    string imgStars = "";

                    if (s.ClassificacaoLodge_id != null)
                    {
                        switch (s.ClassificacaoLodge_id)
                        {
                            case 1:
                                imgStars = UrlEmpresa + "/imgCategorias/3star.gif";
                                break;
                            case 2:
                                imgStars = UrlEmpresa + "/imgCategorias/3star.gif";
                                break;
                            case 3:
                                imgStars = UrlEmpresa + "/imgCategorias/3star.gif";
                                break;
                            case 4:
                                imgStars = UrlEmpresa + "/imgCategorias/4star.gif";
                                break;
                            case 5:
                                imgStars = UrlEmpresa + "/imgCategorias/5star.gif";
                                break;
                        }
                    }
                    else
                    {
                        if (Categoria != null)
                            imgStars = UrlEmpresa + "/imgCategorias/" + Categoria.Categ_imgnome;
                        else
                            imgStars = UrlEmpresa + "/imgCategorias/semcategoria.gif";
                    }

                    

                    Cidade Cidade = new CidadeDAL().ObterPorNomeSupplier(s.S_nome);
                    Pais pais = new PaisDAL().ObterPorId(Convert.ToInt32(s.PAIS_id));
                    string fotoResize = "";

                    FotoDAL fd = new FotoDAL();
                    S_Image si = fd.ObterPorSupplierCapa(s.S_id);


                    if (si != null)
                        fotoResize = "/Galeria/" + s.S_id + "/" + si.Image_nome;
                    else
                        fotoResize = "/Galeria/semimagem.jpg";

                    S_Descricao descricao = new DescricaoDAL().ObterPorIdSupp(Convert.ToInt32(ft.S_id));

                    string descr = "";
                    if (descricao != null) descr = descricao.Desc_long; else descr = "No description";


                    ht.imgStars = imgStars;
                    ht.idSupplier = (int)ft.S_id;
                    ht.nomeHotel = ft.S_nome;
                    ht.cidade = Cidade.CID_nome + " - " + pais.PAIS_nome;
                    ht.descricaoHotel = descr;
                    ht.imgHotel = UrlEmpresa + fotoResize;

                    ht.lstAmenities = Amenities((int)ft.S_id);
                    ht.lstAmenitiesItens = AmenitiesItens((int)ft.S_id);

                    lst.Add(ht);
                }

                return lst;

                //if (_qgrupo.tourCode != null)
                //    lblTourCode.Text = _qgrupo.tourCode.ToUpper() + " - " + _itineraryByUserQuote.Quotation_Code.ToUpper();
                //else
                //    lblTourCode.Text = _itineraryByUserQuote.Quotation_Code.ToUpper();
                //lblNomeCliente.Text = _qgrupo.Cliente.Cliente_nome.ToUpper();
            }
            catch (Exception ex)
            {
                return lst;
            }
        }

        public List<S_GroupAmenities> Amenities(int idsupplier)
        {
            try
            {
                GroupAmenitiesDAL gamDal = new GroupAmenitiesDAL();

                return gamDal.ListarTodos_Spp(idsupplier);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<S_Amenities> AmenitiesItens(int idsupplier)
        {
            try
            {
                GroupAmenitiesDAL gamDal = new GroupAmenitiesDAL();

                return gamDal.ListarTodos_Spp_Itens(idsupplier);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #endregion

        #region Flights

        private string CarregaFlights(string qCode, int optQuote)
        {
            TBItinerary_FlightsInformationDAL tfid = new TBItinerary_FlightsInformationDAL();
            var tfi = tfid.ObterPorQuote(qCode, optQuote);

            if (tfi != null)
                return tfi.FlightInformation;
            else
                return "";
        }

        public class FlightText
        {
            public string txtLinkFlight { get; set; }
            public string qCode { get; set; }
            public int optQuote { get; set; }
        }

        public JsonResult SalvarFlight(FlightText model)
        {
            try
            {
                TBItinerary_FlightsInformationDAL tfid = new TBItinerary_FlightsInformationDAL();
                var tfi = tfid.ObterPorQuote(model.qCode, model.optQuote);


                var conte = HttpUtility.UrlDecode(model.txtLinkFlight, System.Text.Encoding.Default);

                if (tfi == null)
                {
                    tfi = new TBItinerary_FlightsInformation();
                    tfi.QuotationCode = model.qCode;
                    tfi.optQuote = model.optQuote;
                }
                tfi.FlightInformation = conte;

                if (tfi.Id == 0)
                    tfid.Adicionar(tfi);
                else
                    tfid.Atualizar(tfi);


                return Json(new { success = true, message = "Cadastro realizado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }
}