﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Entity;
using DAL.Persistencia;
using System.Configuration;
using BLL.Utils;
using System.Data;
using Site.Reports;
using DAL.DataSet;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Web.Security;
using Site.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using System.Collections.Specialized;
using System.Globalization;


namespace Site.Controllers
{
    [Authorize]
    public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult Index()
        {

            var c = new Cliente();
            c.Cliente_id = 0;
            ViewData["cliente"] = c;
            StartCliente();

            var mcli = new Cliente_Markup();
            mcli.Markup_id = 0;
            ViewData["markupcliente"] = mcli;

            return View("Clientes");
        }    

        public Usuarios ObterUser()
        {
            try
            {
                FormsIdentity id = (FormsIdentity)System.Web.HttpContext.Current.User.Identity;
                FormsAuthenticationTicket ticket = id.Ticket;
                return new UsuarioDAL().ObterPorEmail(ticket.Name);
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return null;
            }
        }

        #region Autocompletes

        public ActionResult ObterQuotations(string term, int user_id)
        {
            try
            {
                List<string> quots = new List<string>();

                //if (term.Equals("*"))
                //    quots = new QuotationDAL().ListarTodosString(term, user_id);
                //else
                quots = new QuotationDAL().ListarTodosString(term, user_id).Take(100).ToList();

                return Json(quots, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterQuotations_copia(string term)
        {
            try
            {
                List<string> quots = new List<string>();
                quots = new QuotationDAL().ListarTodosString(term).Take(100).ToList();

                return Json(quots, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterClientes(string term)
        {
            try
            {
                List<string> cls = new ClienteDAL().ListarTodos(term).Select(s => s.Cliente_nome).Distinct().Take(100).ToList();
                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterCidades(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                if (term.Equals("*"))
                    cls = new CidadeDAL().ListarTodos().OrderBy(s => s.CID_nome).Select(s => s.CID_nome).Distinct().ToList();
                else
                    cls = new CidadeDAL().ListarTodos(term).Select(s => s.CID_nome).Distinct().Take(100).ToList();


                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterTransfers(string term, string cidade)
        {
            try
            {
                ServicoDAL ftd = new ServicoDAL();
                List<string> cls = new List<string>();

                if (!cidade.Equals(""))
                    cls = ftd.ListarTodosTrf(term, new CidadeDAL().ObterPorNome(cidade).CID_id).Select(s => s.Servicos_Nome).Distinct().ToList();
                else
                    cls = ftd.ListarTodosTrf(term).Select(s => s.Servicos_Nome).Distinct().Take(200).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterTours(string term, string cidade)
        {
            try
            {
                ServicoDAL ftd = new ServicoDAL();
                List<string> cls = new List<string>();

                if (!cidade.Equals(""))
                    cls = ftd.ListarTodosTor(term, new CidadeDAL().ObterPorNome(cidade).CID_id).Select(s => s.Servicos_Nome).Distinct().ToList();
                else
                    cls = ftd.ListarTodosTor(term).Select(s => s.Servicos_Nome).Distinct().Take(200).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterSuppliers(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new SupplierDAL().ListarTodos(term).Select(s => s.S_nome).OrderBy(s => s).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterHoteis(string term, string cidade)
        {
            try
            {
                SupplierDAL ftd = new SupplierDAL();
                List<string> lst = new List<string>();

                if (!cidade.Equals(""))
                {
                    lst = ftd.FindAllTipo(term, 1, new CidadeDAL().ObterPorNome(cidade).CID_id).OrderBy(s => s.S_nome).Select(s => s.S_nome).ToList();
                }
                else
                {
                    lst = ftd.FindAllTipo(term, 1).OrderBy(s => s.S_nome).Select(s => s.S_nome).ToList();
                }

                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterPaxsGroups(string term, int user_id)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new QuotationGrupoDAL().ListarPaxsGroup(user_id, 0, term).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterSuppliers_Filtros(string term, int idStatus, int idTipoSupplier, int TipoPesquisa, int idCidade)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new SupplierDAL().ListarSupplier_Filtros(term, idStatus, idTipoSupplier, TipoPesquisa, idCidade);

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterCategorias(string term)
        {
            try
            {
                TarifCategoriaDAL t = new TarifCategoriaDAL();
                List<Tarif_Categoria> tc = t.ListarTodos(term);

                List<string> lista = new List<string>();

                foreach (Tarif_Categoria p in tc)
                {
                    lista.Add(p.Tarif_categoria_nome);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        #endregion

        #region Clientes

        protected void StartCliente()
        {
            ViewBag.CliRepresentante = new ClienteDAL().ListarTodos();
            ViewBag.CliCentroCusto = new CentroCustoDAL().ListarTodosC_C();
            ViewBag.CliMercado = new MercadoDAL().ListarTodos();
            ViewBag.CliBaseTarifaria = new BaseTarifariaDAL().ListarTodos();
            var pais = new PaisDAL().ListarTodos();
            ViewBag.CliPais = pais;
            ViewBag.CliCidade = new CidadeDAL().ListarTodos(pais.First().PAIS_id);
        }

        protected List<Cliente> ObterClientes(int page = 0)
        {

            List<Cliente> alunos = new List<Cliente>();
            try
            {
                ClienteDAL ud = new ClienteDAL();
                List<Cliente> lista = ud.ListarTodos();

                const int PageSize = 20;

                var count = lista.Count;
                var data = lista.Skip(page * PageSize).Take(PageSize).ToList();

                decimal tot = ((decimal)count / (decimal)PageSize);

                if (tot > 1)
                {
                    string[] pars = Convert.ToDecimal(tot).ToString().Split(',');
                    if (pars.Count() > 1)
                    {
                        if (Convert.ToInt32(pars[1].First()) >= 1)
                        {
                            tot = tot + 1;
                            string[] pg = tot.ToString().Split(',');
                            tot = Convert.ToInt32(pg[0]);
                        }
                    }
                }

                ViewBag.Pages = Convert.ToInt32(tot);

                return lista;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return alunos;
            }

        }

        public ActionResult PagClientes(int page = 0)
        {
            ViewData["alunos"] = ObterClientes(page);
            return PartialView("_lst_clientes", ViewData["clientes"]);
        }

        public ActionResult ObterCliente(string cliente)
        {
            Cliente cli = new Cliente();
            try
            {
                cli = new ClienteDAL().ObterPorNome_(cliente);

                ViewBag.CliRepresentante = new ClienteDAL().ListarTodos();
                ViewBag.CliCentroCusto = new CentroCustoDAL().ListarTodosC_C();
                ViewBag.CliMercado = new MercadoDAL().ListarTodos();
                ViewBag.CliBaseTarifaria = new BaseTarifariaDAL().ListarTodos();
                var pais = new PaisDAL().ListarTodos();
                ViewBag.CliPais = pais;
                ViewBag.CliCidade = new CidadeDAL().ListarTodos((int)cli.PAIS_id);

                return PartialView("_cliente_add", cli);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cliente_add", cli);
            }
        }

        public ActionResult ObterMarkupsCliente(string cliente)
        {
            var cli = new ClienteDAL().ObterPorNome_(cliente);
            ViewBag.MarkupCliente = new ClienteMarkupDAL().ListarTodos(cli.Cliente_id);
            return PartialView("_lst_markupCliente");
        }

        public JsonResult ObterCidadesDrop(int idPais)
        {
            try
            {
                SupplierDAL sDal = new SupplierDAL();
                CidadeDAL cDal = new CidadeDAL();


                var cid = cDal.ListarTodos(idPais);

                string drop = "<select id=\"ddlCidadeCliente\" class=\"form-control\" name=\"CID_id\" >";
                drop = drop + "<option value=\"0\">Selecione...</option>";

                foreach (var item in cid)
                {
                    drop = drop + string.Format("<option value=\"{0}\">{1}</option>", item.CID_id, item.CID_nome);
                }
                drop = drop + "</select>";

                return Json(drop, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarCliente(Cliente cli)
        {
            try
            {
                ClienteDAL s = new ClienteDAL();

                if (s.ObterPorNome(cli.Cliente_nome) != null)
                {
                    return Json(new { success = false, message = "Já existe cliente cadastrado com esse nome, tente outro." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.CentroCusto_id == 0)
                {
                    return Json(new { success = false, message = "Selecione um centro de custo." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.BaseTarifaria_id == 0)
                {
                    return Json(new { success = false, message = "Selecione uma base tarifária." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.Mercado_id == 0)
                {
                    return Json(new { success = false, message = "Selecione um mercado." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.PAIS_id == 0)
                {
                    return Json(new { success = false, message = "Selecione um país." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.CID_id == 0)
                {
                    return Json(new { success = false, message = "Selecione uma cidade." }, JsonRequestBehavior.AllowGet);
                }

                //if (ModelState.IsValid)
                //{
                    var u = ObterUser();
                    cli.US_id = u.US_id;

                    if (cli.Representante_id == 0)
                    {
                        cli.Representante_id = null;
                    }

                    s.Salvar(cli);
                    //ViewBag.message = "Aluno atualizado com sucesso.";
                    //return PartialView("_cliente_add", c);
                    return Json(new { success = true, message = "Cliente cadastrado com sucesso.", cod = cli.Cliente_id }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    string messages = string.Join("<br /> ", ModelState.Values
                //                         .SelectMany(x => x.Errors)
                //                         .Select(x => x.ErrorMessage));

                //    throw new Exception(messages);
                //}

            }
            catch (Exception ex)
            {
                //ViewBag.message = "Erro: " + ex.Message;
                //return PartialView("_cliente_add", c);
                return Json(new { success = false, message = "Erro: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizarCliente(Cliente cli)
        {
            try
            {
                if (cli.CentroCusto_id == 0)
                {
                    return Json(new { success = false, message = "Selecione um centro de custo." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.BaseTarifaria_id == 0)
                {
                    return Json(new { success = false, message = "Selecione uma base tarifária." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.Mercado_id == 0)
                {
                    return Json(new { success = false, message = "Selecione um mercado." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.PAIS_id == 0)
                {
                    return Json(new { success = false, message = "Selecione um país." }, JsonRequestBehavior.AllowGet);
                }
                if (cli.CID_id == 0)
                {
                    return Json(new { success = false, message = "Selecione uma cidade." }, JsonRequestBehavior.AllowGet);
                }

                ClienteDAL s = new ClienteDAL();

                if (ModelState.IsValid)
                {
                    var u = ObterUser();
                    cli.US_id = u.US_id;

                    if (cli.Representante_id == 0)
                    {
                        cli.Representante_id = null;
                    }


                    s.Atualizar(cli);
                    return Json(new { success = true, message = "Cliente atualizado com sucesso.", cod = cli.Cliente_id }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string messages = string.Join("<br /> ", ModelState.Values
                                         .SelectMany(x => x.Errors)
                                         .Select(x => x.ErrorMessage));

                    throw new Exception(messages);
                }

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Erro: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObterMarkupCliente_id(int id)
        {
            var cli = new ClienteMarkupDAL().ObterPorId(id);
            return PartialView("_markupCliente_add", cli);
        }

        public ActionResult ObterMarkupCliente_Empty()
        {
            var cli = new Cliente_Markup();
            cli.Markup_id = 0;
            return PartialView("_markupCliente_add", cli);
        }

        public ActionResult DeletarMarkupCliente(int id, string cliente)
        {
            ClienteMarkupDAL cDAL = new ClienteMarkupDAL();
            var cm = cDAL.ObterPorId(id);
            cDAL.Excluir(cm);
            return ObterMarkupsCliente(cliente);
        }

        public JsonResult AtualizarClienteMarkup(Cliente_Markup cli)
        {
            try
            {
                ClienteMarkupDAL s = new ClienteMarkupDAL();

                if (ModelState.IsValid)
                {
                    s.Atualizar(cli);
                    return Json(new { success = true, message = "Markup atualizado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string messages = string.Join("<br /> ", ModelState.Values
                                         .SelectMany(x => x.Errors)
                                         .Select(x => x.ErrorMessage));

                    throw new Exception(messages);
                }

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Erro: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarClienteMarkup(Cliente_Markup cli)
        {
            try
            {
                ClienteMarkupDAL s = new ClienteMarkupDAL();

                if (ModelState.IsValid)
                {
                    s.Salvar(cli);

                    return Json(new { success = true, message = "Markup cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string messages = string.Join("<br /> ", ModelState.Values
                                         .SelectMany(x => x.Errors)
                                         .Select(x => x.ErrorMessage));

                    throw new Exception(messages);
                }

            }
            catch (Exception ex)
            {

                return Json(new { success = false, message = "Erro: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }
}