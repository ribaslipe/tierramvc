﻿using BLL.Utils;
using DAL.Entity;
using DAL.Models;
using DAL.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Site.Controllers
{
    public class SubServicosController : Controller
    {
        // GET: SubServicos
        public ActionResult Index()
        {
            ViewBag.UserLog = ObterUser().US_nome;
            StartSubServicos();
            return View("SubServicos");
        }

        public Usuarios ObterUser()
        {
            try
            {
                FormsIdentity id = (FormsIdentity)System.Web.HttpContext.Current.User.Identity;
                FormsAuthenticationTicket ticket = id.Ticket;
                return new UsuarioDAL().ObterPorEmail(ticket.Name);
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return null;
            }
        }

        #region AutoCompletes

        public ActionResult ObterSuppliers(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new SupplierDAL().ListarTodos(term).Select(s => s.S_nome).OrderBy(s => s).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        #endregion

        #region SubServiços

        protected void StartSubServicos()
        {            
            ViewBag.Items = new ItemSubServicoDAL().ListarTodos();            
            ViewBag.SubItems = new SubItemDAL().ListarTodos();

            ViewBag.MercadoSppTarifas = new MercadoDAL().ListarTodos();
            ViewBag.BaseTarifariaSppTarifas = new BaseTarifariaDAL().ListarTodos();        
            ViewBag.TipoBases = new TipoBaseDAL().ListarTodos();
            ViewBag.MoedaSupplier = new MoedaDAL().ListarTodos();
        }

        public ActionResult PopulaGridMServicos(string supplier)
        {
            List<GridSubServicosModel> lst = new List<GridSubServicosModel>();
            try
            {
                SupplierDAL d = new SupplierDAL();               
                var spp = d.ObterSupplierPorNome(supplier);
                lst = new MontaServicoDAL().ListarTodosCondicaoMSCustos(spp.S_id);               
                return PartialView("_lst_subservicos", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_subservicos", lst);
            }
        }

        public ActionResult FiltroMServico(string supplier, string txtFiltroMServicosDataFrom, string txtFiltroMServicosDataTo, string ddlFiltroMServicoItemSubServico, string ddlFiltroMServicoSubItem, string rdoBasesIndexSubServico)
        {
            List<GridSubServicosModel> lst = new List<GridSubServicosModel>();
            try
            {
                SupplierDAL d = new SupplierDAL();             
                var spp = d.ObterSupplierPorNome(supplier);
               
                MontaServicoDAL ms = new MontaServicoDAL();
                lst = ms.ListarTodosCondicaoMSCustos(spp.S_id, Convert.ToDateTime(txtFiltroMServicosDataFrom), 
                                                               Convert.ToDateTime(txtFiltroMServicosDataTo), 
                                                               Convert.ToInt32(ddlFiltroMServicoItemSubServico), 
                                                               Convert.ToInt32(ddlFiltroMServicoSubItem), 
                                                               Convert.ToInt32(rdoBasesIndexSubServico));
               

                return PartialView("_lst_subservicos", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_subservicos", lst);
            }
        }

        public class retornoSubServico
        {
            public int codMServico { get; set; }
            public string dtFrom { get; set; }
            public string dtTo { get; set; }
            public string obs { get; set; }
            public string porcentagem { get; set; }
            public string imposto { get; set; }
            public string comissao { get; set; }
            public int itemsubservico { get; set; }
            public int subitem { get; set; }
            public int mercado { get; set; }
            public int basetarifaria { get; set; }
            public int tipobase { get; set; }
            public int moeda { get; set; }
            public int indexBase { get; set; }
            public int Sid { get; set; }
        }

        public JsonResult EditarMServico(int myid, int index)
        {
            retornoSubServico model = new retornoSubServico();
            try
            {                                
                MontaServicoDAL msd = new MontaServicoDAL();
                MontaServicoValoresDAL msvd = new MontaServicoValoresDAL();

                Monta_Servico ms = msd.ObterPorId(myid);

                model.codMServico = ms.IdMServico;                
                
                model.dtFrom = string.Format("{0:yyyy-MM-dd}", ms.MServico_DataFrom);
                model.dtTo = string.Format("{0:yyyy-MM-dd}", ms.MServico_DataTo);
                model.obs = ms.MServico_Obs;
                model.porcentagem = ms.MServico_Bulk_Porc_Taxa.ToString();
                model.imposto = ms.MServico_Bulk_Porc_Imposto.ToString();
                model.comissao = ms.MServico_Bulk_Porc_Comissao.ToString();
                model.itemsubservico = (int)ms.ItemSubServico_id;
                model.subitem = (int)ms.SubItem_id;

                SBasesDAL bd = new SBasesDAL();

                S_Bases sb = bd.ObterPorIdSupplierIndex(ms.S_id, index).First();

                model.mercado = sb.Mercado_id;
                model.basetarifaria = sb.BaseTarifaria_id;
                model.tipobase = (int)sb.TipoBase_id;
                model.moeda = sb.Moeda_id;

                Monta_Servico_Valores msv = msvd.ObterPorIdMServico(ms.IdMServico);

                model.indexBase = Convert.ToInt32(msv.S_Bases.Base_index);
                model.Sid = ms.S_id;

                //PopulaDataListBases(index);

                ////btnSalvarTarifas.Text = "Novo";
                ////btnAtualizarTarifas.Visible = true;
                ////lblMsgCadMServicos.Text = "";


                return Json(new { success = true, modelo = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return Json(new { success = false, modelo = model }, JsonRequestBehavior.AllowGet);
            }
        }
       

        public ActionResult PopulaDataListBases(int index, int S_id, int ddlMercadoFiltro_Base, int ddlBaseTarifariaFiltro_Base, int ddlMoedaFiltro_Base, int ddlTipoBase_Base, int txtCodMServico, string supplier)
        {
            List<S_Bases> LSTsba = new List<S_Bases>();
            List<ValoresBasesIntoModel> lst = new List<ValoresBasesIntoModel>();
            List<ValoresBasesIntoModel> lstCpreco = new List<ValoresBasesIntoModel>();
            try
            {
                if(S_id == 0)
                {
                    SupplierDAL d = new SupplierDAL();
                    var spp = d.ObterSupplierPorNome(supplier);
                    S_id = spp.S_id;
                }

                LSTsba = new SBasesDAL().ListarTodosPorSupplier(S_id, index, Convert.ToInt32(ddlMercadoFiltro_Base), Convert.ToInt32(ddlBaseTarifariaFiltro_Base), Convert.ToInt32(ddlMoedaFiltro_Base), Convert.ToInt32(ddlTipoBase_Base));


                foreach (var item in LSTsba)
                {
                    lst.Add(new ValoresBasesIntoModel { idbase = Convert.ToInt32(item.Base_id), De = item.Base_de.ToString(), Ate = item.Base_ate.ToString() });
                }

                if(txtCodMServico != 0)
                {
                    List<Monta_Servico_Valores> listaValores = new MontaServicoValoresDAL().ListaTodos(Convert.ToInt32(txtCodMServico));

                    foreach (var item in lst)
                    {
                        var bb = listaValores.Where(s => s.Base_id == item.idbase).SingleOrDefault();
                        if(bb != null)
                        {
                            lstCpreco.Add(new ValoresBasesIntoModel { idbase = Convert.ToInt32(bb.Base_id), De = item.De.ToString(), Ate = item.Ate.ToString(), MServico_Valores_Bulk = bb.MServico_Valores_Bulk.ToString().Replace(",","."), MServico_Valores_Bulk_Total = bb.MServico_Valores_Bulk_Total.ToString().Replace(",", "."), IdMServicoValores = bb.IdMServicoValores.ToString().Replace(",", ".") });
                        }
                    }


                    return PartialView("_lst_basesprecos", lstCpreco);
                }

                return PartialView("_lst_basesprecos", lst);
            }
            catch (Exception)
            {

                return PartialView("_lst_basesprecos", lst);
            }
        }

        public JsonResult ExcluirMServico(int IdMServico)
        {
            try
            {
                //            if ((base.ObterPerfilLogado() == (int)PerfilUsuario.Financeiro) ||
                //(base.ObterPerfilLogado() == (int)PerfilUsuario.Operacional))
                //                return;

                int myid = Convert.ToInt32(IdMServico);

                MontaServicoDAL msd = new MontaServicoDAL();

                Monta_Servico ms = msd.ObterPorId(myid);

                msd.Excluir(ms);

                return Json(new { success = true, message = "Excluído com sucesso." }, JsonRequestBehavior.AllowGet);
                //PopulaGridMServicos();
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //public JsonResult PopulaIntoBases(int txtCodMServico)
        //{
        //    List<ValoresBasesInto> lst = new List<ValoresBasesInto>();        
        //    List<Monta_Servico_Valores> listaValores = new MontaServicoValoresDAL().ListaTodos(Convert.ToInt32(txtCodMServico));
        //    foreach (var item in listaValores)
        //    {
        //        lst.Add(new ValoresBasesInto { idbase = Convert.ToInt32(item.Base_id), MServico_Valores_Bulk = item.MServico_Valores_Bulk.ToString(), MServico_Valores_Bulk_Total = item.MServico_Valores_Bulk_Total.ToString(), IdMServicoValores = item.IdMServicoValores.ToString() });
        //    }

        //    var qtd = lst.Count;

        //    return Json(new { success = false, modelo = lst, qtdd = qtd }, JsonRequestBehavior.AllowGet);

        //}

        #endregion

        #region CRUD

        public JsonResult SalvarTarifas(string supplier, int ddlItemSubServico, int ddlSubItemSubServico, string txtPorcentagemTaxa, string txtPorcentagemImposto1, string txtComissao, string from, string to, string txtObs, string vr1, string vr2)
        {
            try
            {

                var spp = new SupplierDAL().ObterSupplierPorNome(supplier);               

                Utils u = new Utils();
                MontaServicoDAL msd = new MontaServicoDAL();

                if (msd.VerificaMS(Convert.ToInt32(spp.S_id),
                    Convert.ToInt32(ddlItemSubServico),
                    Convert.ToInt32(ddlSubItemSubServico)))
                {
                    return Json(new { success = false, message = "Já existem tarifas cadastradas com esses parâmetros(Transporte, Servico e Serviço Categoria)." }, JsonRequestBehavior.AllowGet);                    
                }               
          

                Monta_Servico ms = new Monta_Servico();

                ms.MServico_Bulk_Porc_Taxa = Convert.ToDecimal(txtPorcentagemTaxa);
                ms.MServico_Bulk_Porc_Imposto = Convert.ToDecimal(txtPorcentagemImposto1);
                ms.MServico_Bulk_Porc_Comissao = Convert.ToDecimal(txtComissao);
                ms.MServico_DataFrom = Convert.ToDateTime(from);
                ms.MServico_DataTo = Convert.ToDateTime(to);
                ms.MServico_Obs = txtObs;
                ms.S_id = Convert.ToInt32(spp.S_id);

                ms.ItemSubServico_id = Convert.ToInt32(ddlItemSubServico);
                ms.SubItem_id = Convert.ToInt32(ddlSubItemSubServico);

                msd.Salvar(ms);

                MontaServicoValoresDAL msvd = new MontaServicoValoresDAL();
                string[] pars1 = vr1.Split('~');
                string[] pars2 = vr2.Split('~');

                var l = System.Threading.Thread.CurrentThread.CurrentCulture;

                for (int i = 0; i < pars1.Length; i++)
                {
                    var bulks = pars1[i].Split('°');
                    var totalbulks = pars2[i].Split('°');

                    Monta_Servico_Valores msv = new Monta_Servico_Valores();
                    msv.IdMServico = ms.IdMServico;

                    if (l.Name.Equals("en-US"))
                    {
                        msv.MServico_Valores_Bulk = Convert.ToDecimal(bulks[0].Replace(',', '.'));
                        msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(totalbulks[0].Replace(',', '.'));
                    }
                    else
                    {
                        msv.MServico_Valores_Bulk = Convert.ToDecimal(bulks[0].Replace('.', ','));
                        msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(totalbulks[0].Replace('.', ','));
                    }

                    if(bulks[1].Equals(totalbulks[1]))
                    {
                        msv.Base_id = Convert.ToInt32(bulks[1]);                           
                        msvd.Salvar(msv);
                    }
                }
                                        

                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizarTarifas(string txtCodMServico, string supplier, int ddlItemSubServico, int ddlSubItemSubServico, string txtPorcentagemTaxa, string txtPorcentagemImposto1, string txtComissao, string from, string to, string txtObs, string vr1, string vr2, string vr3)
        {
            try
            {

                var spp = new SupplierDAL().ObterSupplierPorNome(supplier);

                Utils u = new Utils();
                MontaServicoDAL msd = new MontaServicoDAL();                
                Monta_Servico ms = new Monta_Servico();

                ms.IdMServico = Convert.ToInt32(txtCodMServico);
                ms.MServico_Bulk_Porc_Taxa = Convert.ToDecimal(txtPorcentagemTaxa);
                ms.MServico_Bulk_Porc_Imposto = Convert.ToDecimal(txtPorcentagemImposto1);
                ms.MServico_Bulk_Porc_Comissao = Convert.ToDecimal(txtComissao);
                ms.MServico_DataFrom = Convert.ToDateTime(from);
                ms.MServico_DataTo = Convert.ToDateTime(to);
                ms.MServico_Obs = txtObs;
                ms.S_id = Convert.ToInt32(spp.S_id);

                ms.ItemSubServico_id = Convert.ToInt32(ddlItemSubServico);
                ms.SubItem_id = Convert.ToInt32(ddlSubItemSubServico);

                msd.Atualizar(ms);

                MontaServicoValoresDAL msvd = new MontaServicoValoresDAL();
                string[] pars1 = vr1.Split('~');
                string[] pars2 = vr2.Split('~');
                string[] pars3 = vr3.Split('~');

                var l = System.Threading.Thread.CurrentThread.CurrentCulture;

                for (int i = 0; i < pars1.Length; i++)
                {
                    var bulks = pars1[i].Split('°');
                    var totalbulks = pars2[i].Split('°');

                    Monta_Servico_Valores msv = new Monta_Servico_Valores();
                    msv.IdMServicoValores = Convert.ToInt32(pars3[i]);
                    msv.IdMServico = ms.IdMServico;

                    if (l.Name.Equals("en-US"))
                    {
                        msv.MServico_Valores_Bulk = Convert.ToDecimal(bulks[0].Replace(',', '.'));
                        msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(totalbulks[0].Replace(',', '.'));
                    }
                    else
                    {
                        msv.MServico_Valores_Bulk = Convert.ToDecimal(bulks[0].Replace('.', ','));
                        msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(totalbulks[0].Replace('.', ','));
                    }

                    //msv.MServico_Valores_Bulk = Convert.ToDecimal(bulks[0].Replace('.', ','));
                    //msv.MServico_Valores_Bulk_Total = Convert.ToDecimal(totalbulks[0].Replace('.', ','));

                    if (bulks[1].Equals(totalbulks[1]))
                    {
                        msv.Base_id = Convert.ToInt32(bulks[1]);
                        msvd.Atualizar(msv);
                    }
                }


                return Json(new { success = true, message = "Registro cadastrado com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

    }
}