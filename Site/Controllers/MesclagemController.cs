﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using BLL.Utils;
using DAL.Entity;
using DAL.Persistencia;
using Newtonsoft.Json;
using Site.Models;
using DAL.Models;

namespace Site.Controllers
{
    [Authorize]
    public class MesclagemController : Controller
    {
        // GET: Mesclagem
        public ActionResult Index()
        {
            StartSupplier();
            return View();
        }

        public string ObterCidadesSupplierDrop(int plataforma, string idPais)
        {
            FileStream fs = null;
            StreamReader fr = null;
            Dictionary<string, string> cidadesDictionary = new Dictionary<string, string>();
            try
            {

                int provedor = Convert.ToInt32(plataforma);

                switch (provedor)
                {
                    case (int)EnumServicos.Cts:
                        fs = new FileStream(Server.MapPath("~/WebServ/cidadescts.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.Omnibees:
                        fs = new FileStream(Server.MapPath("~/WebServ/cidadesomni.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.RoomsXml:
                        fs = new FileStream(Server.MapPath("~/WebServ/cidadesroomsxml.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.HotelsPro:
                        fs = new FileStream(Server.MapPath("~/WebServ/hotelspro/" + idPais + ".json"), FileMode.Open, FileAccess.Read);
                        break;
                }


                fr = new StreamReader(fs);
                String s = null;
                if (plataforma == (int)EnumServicos.HotelsPro)
                {
                    s = fr.ReadToEnd();
                    var destinations = JsonConvert.DeserializeObject<List<HotelsProDestinationsModel>>(s);
                    foreach (var destination in destinations)
                    {
                        cidadesDictionary.Add(destination.Code, destination.Name);
                    }
                }
                else
                {
                    s = fr.ReadLine();
                    while (s != null)
                    {
                        string[] strsplit = s.Split('~');

                        if (Convert.ToInt32(strsplit[2]) != Convert.ToInt32(idPais))
                        {
                            s = fr.ReadLine();
                            continue;
                        }

                        if (!cidadesDictionary.ContainsKey(strsplit[0]))
                        {
                            cidadesDictionary.Add(strsplit[0], strsplit[1]);
                        }
                        s = fr.ReadLine();
                    }
                    fr.Close();

                }
            }
            catch (FileNotFoundException e)
            {
                Response.Write("cannot read sample");
            }

            var items = from pair in cidadesDictionary
                        orderby pair.Value ascending
                        select pair;

            string drop = "";
            drop = drop + "<option value=\"0\">Selecione...</option>";
            foreach (var item in items)
            {
                drop = drop + $"<option value=\"{item.Key}\">{item.Value}</option>";
            }

            return drop;
        }

        public string ObterPaisesSupplierDrop(int plataforma)
        {
            FileStream fs = null;
            StreamReader fr = null;
            Dictionary<string, string> paisesDictionary = new Dictionary<string, string>();
            try
            {

                int provedor = Convert.ToInt32(plataforma);

                switch (provedor)
                {
                    case (int)EnumServicos.Cts:
                        fs = new FileStream(Server.MapPath("~/WebServ/paisescts.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.Omnibees:
                        fs = new FileStream(Server.MapPath("~/WebServ/paisesomni.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.RoomsXml:
                        fs = new FileStream(Server.MapPath("~/WebServ/paisesroomsxml.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.HotelsPro:
                        fs = new FileStream(Server.MapPath("~/WebServ/hotelspro/paises.json"), FileMode.Open, FileAccess.Read);

                        break;
                }

                fr = new StreamReader(fs);
                String s = null;

                if (provedor == (int)EnumServicos.HotelsPro)
                {
                    s = fr.ReadToEnd();
                    var countries = JsonConvert.DeserializeObject<List<HotelsProCountryModel>>(s);
                    foreach (var pais in countries)
                    {
                        paisesDictionary.Add(pais.Code, pais.Name);
                    }
                }
                else
                {
                    s = fr.ReadLine();
                    while (s != null)
                    {
                        string[] strsplit = s.Split('~');

                        paisesDictionary.Add(strsplit[0], strsplit[1]);

                        s = fr.ReadLine();
                    }
                    fr.Close();
                }


            }
            catch (FileNotFoundException e)
            {
                Response.Write("cannot read sample");
            }

            var items = from pair in paisesDictionary
                        orderby pair.Value ascending
                        select pair;

            string drop = "";
            drop = drop + "<option value=\"0\">Selecione...</option>";
            foreach (var item in items)
            {
                drop = drop + $"<option value=\"{item.Key}\">{item.Value}</option>";
            }

            return drop;
        }

        public void StartSupplier()
        {

            SupplierDAL sppDal = new SupplierDAL();
            var lstSpp = sppDal.ListarTodos();

            var spp = new Supplier();
            spp.S_id = 0;
            ViewData["supplier"] = spp;
            ViewBag.TipoSupplier = new TipoSuplsDAL().ListarTodos();
            ViewBag.CategoriasTierra = new TarifCategoriaDAL().ListarTodos();
            var pais = new PaisDAL().ListarTodos();
            ViewBag.PaisSupplier = pais;
            ViewBag.CidadeSupplier = new CidadeDAL().ListarTodos().OrderBy(s => s.CID_nome);
            ViewBag.CidadeSupplierFiltro = new CidadeDAL().ListarTodos();
            ViewBag.HoteisIntegrados = new SupplierXMLDAL().ObterTodos().OrderBy(s => s.S_nome).ToList();

            ViewData["HoteisIntegrados"] = new SupplierXMLDAL().ObterTodos().OrderBy(s => s.S_nome).ToList();

            PlataformaDAL pdal = new PlataformaDAL();
            ViewBag.Plataforma = pdal.ListarTodos();
            ViewBag.HoteisTierra = new SupplierDAL().ListarTodos().OrderBy(s => s.S_nome).ToList();
            ViewBag.CategoriasIntegradas = new Tarif_Categoria_XMLDAL().ObterTodos();
        }

        public string ObterHoteisPlataforma(int plataforma, string cidade)
        {
            FileStream fs = null;
            StreamReader fr = null;
            Dictionary<string, string> hotelDictionary = new Dictionary<string, string>();
            try
            {

                int provedor = Convert.ToInt32(plataforma);

                switch (provedor)
                {
                    case (int)EnumServicos.Cts:
                        fs = new FileStream(Server.MapPath("~/WebServ/hoteiscts.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.Omnibees:
                        fs = new FileStream(Server.MapPath("~/WebServ/hoteisomni.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.RoomsXml:
                        fs = new FileStream(Server.MapPath("~/WebServ/hoteisroomsxml.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.HotelsPro:
                        fs = new FileStream(Server.MapPath("~/WebServ/hotelspro/hotelsresumed.json"), FileMode.Open, FileAccess.Read);
                        break;
                }

                fr = new StreamReader(fs);
                String s = null;
                if (plataforma == (int)EnumServicos.HotelsPro)
                {
                    s = fr.ReadToEnd();
                    var hoteis = JsonConvert.DeserializeObject<List<HotelsProHotelsModel>>(s);

                    var filtrado = hoteis.Where(x => x.Destination == cidade).ToList();

                    foreach (var hotel in filtrado)
                    {
                        hotelDictionary.Add(hotel.Code, hotel.Name);
                    }
                }
                else
                {
                    s = fr.ReadLine();
                    while (s != null)
                    {
                        string[] strsplit = s.Split('~');

                        if (Convert.ToInt32(strsplit[2]) != Convert.ToInt32(cidade))
                        {
                            s = fr.ReadLine();
                            continue;
                        }

                        if (!hotelDictionary.ContainsKey(strsplit[0]))
                        {
                            hotelDictionary.Add(strsplit[0], strsplit[1]);
                        }
                        s = fr.ReadLine();
                    }
                }
                fr.Close();

            }
            catch (FileNotFoundException e)
            {
                Response.Write("cannot read sample");
            }

            var items = from pair in hotelDictionary
                        orderby pair.Value ascending
                        select pair;

            string drop = "";
            drop = drop + "<option value=\"0\">Selecione...</option>";
            foreach (var item in items)
            {
                drop = drop + $"<option value=\"{item.Key}\">{item.Value}</option>";
            }

            return drop;


        }

        public string ObterCategoriasPlataforma(int plataforma, int cidade)
        {
            FileStream fs = null;
            StreamReader fr = null;
            Dictionary<string, string> categoriasDictionary = new Dictionary<string, string>();
            try
            {

                int provedor = Convert.ToInt32(plataforma);
                SupplierXMLDAL sxmlDal = new SupplierXMLDAL();

                switch (provedor)
                {
                    case (int)EnumServicos.Cts:
                        fs = new FileStream(Server.MapPath("~/WebServ/categoriascts.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.Omnibees:
                        fs = new FileStream(Server.MapPath("~/WebServ/categoriasomni.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.RoomsXml:
                        fs = new FileStream(Server.MapPath("~/WebServ/categoriasroomsxml.txt"), FileMode.Open, FileAccess.Read);
                        break;
                    case (int)EnumServicos.HotelsPro:
                        fs = new FileStream(Server.MapPath("~/WebServ/hotelspro/hoteisCategorias.json"), FileMode.Open, FileAccess.Read);
                        break;
                }

                var sxmldal = new SupplierXMLDAL();
                fr = new StreamReader(fs);
                String s = null;
                if (plataforma == (int)EnumServicos.HotelsPro)
                {
                    s = fr.ReadToEnd();
                    var hcat = JsonConvert.DeserializeObject<List<HotelsProHotelsCategories>>(s);

                    foreach (var hotel in hcat)
                    {

                        foreach (var cat in hotel.Categorias)
                        {
                            if (!categoriasDictionary.ContainsKey(cat.Code + "°" + hotel.CodeHotel))
                                categoriasDictionary.Add(cat.Code + "°" + hotel.CodeHotel, hotel.NomeHotel + " - " + cat.Name);
                        }
                    }
                }
                else
                {
                    try
                    {
                        s = fr.ReadLine();
                        while (s != null)
                        {
                            string[] strsplit = s.Split('~');

                            Supplier_XML supp = sxmldal.ObterPorSppPorCidade(strsplit[2], cidade);

                            if (supp == null)
                            {
                                s = fr.ReadLine();
                                continue;
                            }

                            //count++;
                            if (!categoriasDictionary.ContainsKey(strsplit[0]))
                            {
                                try
                                {
                                    categoriasDictionary.Add(strsplit[0] + "°" + supp.Id, supp.Supplier.S_nome + "°" + strsplit[1]);
                                }
                                catch (ArgumentException ex)
                                {

                                }

                            }
                            s = fr.ReadLine();

                        }
                    }
                    catch (Exception e)
                    {

                        throw;
                    }
                }
                fr.Close();

            }
            catch (FileNotFoundException ex)
            {
                Response.Write("cannot read sample");
            }


            var items = from pair in categoriasDictionary
                        orderby pair.Value ascending
                        select pair;

            string drop = "";
            drop = drop + "<option value=\"0\">Selecione...</option>";
            foreach (var item in items)
            {
                drop = drop + $"<option value=\"{item.Key}\">{item.Value}</option>";
            }

            return drop;


        }
        
        public JsonResult SalvarSupplier(int idPlataforma, string idHotelPlataforma, int idHotelTierra)
        {
            try
            {
                SupplierXMLDAL lDal = new SupplierXMLDAL();
                var resposta = new SimpleReturnMessage();
                //verifica se mesclagem já existe
                Supplier_XML retorno = lDal.ObterPorSppPL(idHotelPlataforma, idPlataforma);

                if (retorno != null)
                {
                    return Json(new { Sucesso = false, Mensagem = "Esse hotel já foi cadastrado." }, JsonRequestBehavior.AllowGet);                    
                }

                Supplier_XML l = new Supplier_XML();
                l.Id_Plataforma = idPlataforma;
                l.SupplerId_Plataforma = idHotelPlataforma;
                l.SupplierId_Prosoftware = idHotelTierra;
                lDal.Salvar(l);

                return Json(new { Sucesso = true, Mensagem = "Mesclagem efetuada com sucesso." }, JsonRequestBehavior.AllowGet);                
            }
            catch (Exception ex)
            {
                return Json(new { Sucesso = true, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);                
            }


        }

        [HttpPost]
        public JsonResult SalvarCategoria(int idPlataforma, int idCategoria, string categoriaPlataforma)
        {
            try
            {
                var lDal = new Tarif_Categoria_XMLDAL();

                string[] pars = categoriaPlataforma.Split('°');

                var s = new SupplierXMLDAL().Obter(Convert.ToInt32(pars[1]));

                Tarif_Categoria_XML lret = lDal.ObterPorPlataformaCategoria(idPlataforma, s.SupplierId_Prosoftware, Convert.ToInt32(pars[0]));

                if (lret != null)
                {
                    return Json(new SimpleReturnMessage()
                    {
                        Sucesso = false,
                        Mensagem = "Categoria já cadastrada."
                    });
                }

                var l = new Tarif_Categoria_XML();
                l.Plataforma_Id = Convert.ToInt32(idPlataforma);
                l.Plataforma_CatId = Convert.ToInt32(pars[0]);
                l.Id_TarifCategoria = idCategoria;
                l.Descricao = categoriaPlataforma;
                l.S_XMLId = Convert.ToInt32(pars[1]);
                lDal.Salvar(l);

                return Json(new SimpleReturnMessage()
                {
                    Sucesso = true,
                    Mensagem = "Categoria mesclada."
                });

            }
            catch (Exception ex)
            {
                return Json(new SimpleReturnMessage()
                {
                    Sucesso = false,
                    Mensagem = "Categoria já cadastrada."
                });
            }
        }

        [HttpPost]
        public JsonResult RemoverMesclagemHotel(int id)
        {
            try
            {
                var dal = new SupplierXMLDAL();
                var supplierxml = dal.Obter(id);

                dal.Excluir(supplierxml);

                return Json(new SimpleReturnMessage()
                {
                    Sucesso = true,
                    Mensagem = "Removido com sucesso"
                });
            }
            catch (Exception ex)
            {
                return Json(new SimpleReturnMessage()
                {
                    Sucesso = false,
                    Mensagem = ex.Message
                });
            }
        }

        [HttpPost]
        public JsonResult RemoverMesclagemCategoria(int id)
        {
            try
            {
                var dal = new Tarif_Categoria_XMLDAL();
                var cateogiraxml = dal.Obter(id);

                dal.Excluir(cateogiraxml);

                return Json(new SimpleReturnMessage()
                {
                    Sucesso = true,
                    Mensagem = "Removido com sucesso"
                });
            }
            catch (Exception ex)
            {
                return Json(new SimpleReturnMessage()
                {
                    Sucesso = false,
                    Mensagem = ex.Message
                });
            }
        }

        public ActionResult AtualizaGrid()
        {
            List<HoteisOmniModel> lst = new List<HoteisOmniModel>();
            try
            {
                lst = new SupplierXMLDAL().ObterTodos().OrderBy(s => s.S_nome).ToList();
                return PartialView("_lst_cadastrosMesclagem", lst);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_cadastrosMesclagem", lst);
            }
        }

    }
}
