﻿using DAL.Entity;
using DAL.Persistencia;
using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Site.Controllers
{
    [Authorize]
    public class CadastrosController : Controller
    {
        // GET: Cadastros
        public ActionResult Index()
        {
            StartCadastros();
            return View();
        }

        protected void StartCadastros()
        {
            ViewData["pais"] = new Pais();
            ViewData["cidade"] = new Cidade();
            ViewData["mercado"] = new Mercado();
            ViewData["basetarifaria"] = new BaseTarifaria();
            ViewData["moeda"] = new Moeda();
            ViewData["rede"] = new S_Rede();
            ViewData["temporada"] = new S_Mercado_Estacao();
            ViewData["meal"] = new S_Meal();
            ViewData["categApto"] = new Tarif_Categoria();
            ViewData["categHotel"] = new S_Categoria_Escrita();
            ViewData["tipoServico"] = new S_Servicos_Tipo();
            ViewData["categoria"] = new S_Servicos_Tipo_Categ();
            ViewData["subtipo"] = new SubItem();
            ViewData["subitem"] = new ItemSubServico();
            ViewData["classificacao"] = new S_Categoria();
            ViewData["amenitie"] = new S_Amenities();


            var pa = new PaisDAL().ListarTodos();
            ViewBag.paises = pa;
            ViewBag.CidPais = pa;

            ViewBag.cidades = new CidadeDAL().ListarTodos();
            ViewBag.mercados = new MercadoDAL().ListarTodos();
            ViewBag.basetarifarialst = new BaseTarifariaDAL().ListarTodos();
            ViewBag.moedas = new MoedaDAL().ListarTodos();
            ViewBag.redes = new RedeDAL().ListarTodos();
            ViewBag.temporadas = new MercadoEstacaoDAL().ListarTodos();
            ViewBag.meals = new MealDAL().ListarTodos_SMeal();
            ViewBag.categAptos = new TarifCategoriaDAL().ListarTodos();
            ViewBag.categhoteis = new CategoriaEscritaDAL().ListarTodos("");
            ViewBag.tiposervicos = new ServicoTipoDAL().ListarTodos();
            ViewBag.categorias = new ServicoTipoCategDAL().ListarTodos();
            ViewBag.TipoServCategoria = new ServicoTipoDAL().ListarTodos();
            ViewBag.subtipos = new SubItemDAL().ListarTodos();
            ViewBag.subitens = new ItemSubServicoDAL().ListarTodos();
            ViewBag.classificacoes = new CategoriaDAL().ListarTodos();

            ViewBag.amenities = new AmenitiesDAL().ListarTodos();
            ViewBag.ddlGroupAmenities = new GroupAmenitiesDAL().ListarTodos();
        }

        #region Autocompletes       

        public ActionResult ObterAmenities(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new AmenitiesDAL().ListarTodos(term).Select(s => s.S_Amenities_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterClassificacao(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new CategoriaDAL().ListarTodos(term).Select(s => s.Categ_classificacao).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObtersubItem(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new ItemSubServicoDAL().ListarTodos(term).Select(s => s.ItemSubServico_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterSubTipos(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new SubItemDAL().ListarTodos(term).Select(s => s.SubItem_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterCategoria(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new ServicoTipoCategDAL().ListarTodos(term).Select(s => s.Tipo_categ_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterTipoServico(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new ServicoTipoDAL().ListarTodos(term).Select(s => s.Tipo_Nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterCategHotel(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new CategoriaEscritaDAL().ListarTodos(term).Select(s => s.CategoriaEscrita_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterTarifCateg(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new TarifCategoriaDAL().ListarTodos(term).Select(s => s.Tarif_categoria_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterMeal(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new MealDAL().ListarTodos_SMeal(term).Select(s => s.S_meal_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterTemporada(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new MercadoEstacaoDAL().ListarTodos(term).Select(s => s.S_mercado_estacao_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterRede(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new RedeDAL().ListarTodos(term).Select(s => s.Rede_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterMoeda(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new MoedaDAL().ListarTodos(term).Select(s => s.Moeda_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterBaseTarifaria(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new BaseTarifariaDAL().ListarTodos(term).Select(s => s.BaseTarifaria_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterMercados(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new MercadoDAL().ListarTodos(term).Select(s => s.Mercado_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterPaises(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new PaisDAL().ListarTodos(term).Select(s => s.PAIS_nome).Distinct().ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterCidades(string term)
        {
            try
            {
                List<string> cls = new List<string>();

                cls = new CidadeDAL().ListarTodos(term).Select(s => s.CID_nome).Distinct().Take(100).ToList();

                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public ActionResult ObterCategorias(string term)
        {
            try
            {
                TarifCategoriaDAL t = new TarifCategoriaDAL();
                List<Tarif_Categoria> tc = t.ListarTodos(term);

                List<string> lista = new List<string>();

                foreach (Tarif_Categoria p in tc)
                {
                    lista.Add(p.Tarif_categoria_nome);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return null;
            }
        }

        public JsonResult ExcluirPais(int idpais)
        {
            try
            {
                PaisDAL p = new PaisDAL();

                Pais d = p.ObterPorId(idpais);
                p.Excluir(d);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Pais

        public ActionResult GetPais(string pais)
        {
            Pais p = new Pais();
            try
            {
                PaisDAL pdal = new PaisDAL();

                var pa = pdal.ObterPorNome_(pais);

                p = pdal.ObterPorId(pa.PAIS_id);
                ViewBag.paises = pdal.ListarTodos();

                return PartialView("_cad_pais", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_pais", p);
            }
        }

        public JsonResult SalvarPais(Pais pais)
        {
            try
            {
                PaisDAL p = new PaisDAL();

                Pais d = new Pais();

                if (pais.PAIS_id == 0)
                {
                    if (p.ObterPorNome(pais.PAIS_nome))
                    {
                        return Json(new { success = false, message = "Já existe país cadastrado com esse nome, tente outro." }, JsonRequestBehavior.AllowGet);
                    }

                    d.PAIS_nome = pais.PAIS_nome;
                    d.PAIS_uf = pais.PAIS_uf;

                    if (pais.PAIS_mercosul == false)
                        d.PAIS_mercosul = null;
                    else
                        d.PAIS_mercosul = true;

                    p.Salvar(d);
                }
                else
                {

                    d.PAIS_id = pais.PAIS_id;
                    d.PAIS_nome = pais.PAIS_nome;
                    d.PAIS_uf = pais.PAIS_uf;

                    if (pais.PAIS_mercosul == false)
                        d.PAIS_mercosul = null;
                    else
                        d.PAIS_mercosul = true;

                    p.Atualizar(d);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = d.PAIS_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Cidade

        public ActionResult GetCidade(string cidade)
        {
            Cidade p = new Cidade();
            try
            {
                CidadeDAL pdal = new CidadeDAL();

                var pa = pdal.ObterPorNome(cidade);

                p = pdal.ObterPorId(pa.CID_id);
                ViewBag.cidades = pdal.ListarTodos();


                ViewBag.CidPais = new PaisDAL().ListarTodos();
                ViewBag.cidades = pdal.ListarTodos();

                return PartialView("_cad_cidade", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_cidade", p);
            }
        }

        public JsonResult SalvarCidade(Cidade cid)
        {
            try
            {
                CidadeDAL p = new CidadeDAL();

                Cidade c = new Cidade();

                if (cid.CID_id == 0)
                {
                    if (p.VerificaNome(cid.CID_nome))
                    {
                        return Json(new { success = false, message = "Já existe cidade cadastrado com esse nome, tente outro." }, JsonRequestBehavior.AllowGet);
                    }

                    c.CID_nome = cid.CID_nome;
                    c.CID_cod_Iata = cid.CID_cod_Iata;
                    c.CID_uf = cid.CID_uf;
                    c.PAIS_id = cid.PAIS_id;

                    c.ObsTarifario = cid.ObsTarifario;

                    p.Salvar(c);

                }
                else
                {

                    c.CID_id = cid.CID_id;
                    c.CID_nome = cid.CID_nome;
                    c.CID_cod_Iata = cid.CID_cod_Iata;
                    c.CID_uf = cid.CID_uf;
                    c.PAIS_id = cid.PAIS_id;

                    c.ObsTarifario = cid.ObsTarifario;

                    p.Atualizar(c);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = c.CID_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirCidade(int idcidade)
        {
            try
            {
                CidadeDAL d = new CidadeDAL();
                SupplierDAL sd = new SupplierDAL();

                if (sd.VerificaAgregado(idcidade))
                {
                    return Json(new { success = false, message = "Esta cidade está agregada há outros cadastros, não pode ser excluída." }, JsonRequestBehavior.AllowGet);
                }

                Cidade c = d.ObterPorId(idcidade);
                d.Excluir(c);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Mercado

        public ActionResult GetMercado(string mercado)
        {
            Mercado p = new Mercado();
            try
            {
                MercadoDAL pdal = new MercadoDAL();

                p = pdal.ObterPorNome_model(mercado);

                ViewBag.mercados = new MercadoDAL().ListarTodos();

                return PartialView("_cad_mercado", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_mercado", p);
            }
        }

        public JsonResult SalvarMercado(Mercado mer)
        {
            try
            {
                MercadoDAL p = new MercadoDAL();

                Mercado m = new Mercado();


                if (mer.Mercado_id == 0)
                {
                    if (p.ObterPorNome(mer.Mercado_nome))
                    {
                        return Json(new { success = false, message = "Já existe mercado cadastrado com esse nome, tente outro." }, JsonRequestBehavior.AllowGet);
                    }

                    m.Mercado_nome = mer.Mercado_nome;
                    p.Salvar(m);

                }
                else
                {

                    m.Mercado_id = mer.Mercado_id;
                    m.Mercado_nome = mer.Mercado_nome;
                    p.Atualizar(m);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = m.Mercado_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirMercado(int idmercado)
        {
            try
            {
                MercadoDAL m = new MercadoDAL();
                Mercado d = m.ObterPorId(idmercado);

                m.Excluir(d);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Base Tarifaria

        public ActionResult GetBaseTarifaria(string basetarifaria)
        {
            BaseTarifaria p = new BaseTarifaria();
            try
            {
                BaseTarifariaDAL pdal = new BaseTarifariaDAL();

                p = pdal.ObterPorNome_model(basetarifaria);

                ViewBag.basetarifarialst = new BaseTarifariaDAL().ListarTodos();

                return PartialView("_cad_basetarifaria", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_basetarifaria", p);
            }
        }

        public JsonResult SalvarBaseTarifaria(BaseTarifaria ba)
        {
            try
            {
                BaseTarifariaDAL p = new BaseTarifariaDAL();

                BaseTarifaria b = new BaseTarifaria();


                if (ba.BaseTarifaria_id == 0)
                {
                    if (p.ObterPorNome(ba.BaseTarifaria_nome))
                    {
                        return Json(new { success = false, message = "Já existe essa Base Tarifária cadastrada com esse nome, tente outro." }, JsonRequestBehavior.AllowGet);
                    }

                    b.BaseTarifaria_nome = ba.BaseTarifaria_nome;
                    p.Salvar(b);

                }
                else
                {
                    b.BaseTarifaria_id = ba.BaseTarifaria_id;
                    b.BaseTarifaria_nome = ba.BaseTarifaria_nome;

                    p.Atualizar(b);

                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = b.BaseTarifaria_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirBaseTarifaria(int idbasetarifaria)
        {
            try
            {
                BaseTarifariaDAL b = new BaseTarifariaDAL();
                BaseTarifaria d = b.ObterPorId(idbasetarifaria);
                b.Excluir(d);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Moeda

        public ActionResult GetMoeda(string moeda)
        {
            Moeda p = new Moeda();
            try
            {
                MoedaDAL pdal = new MoedaDAL();

                p = pdal.ObterPorNome_model(moeda);

                ViewBag.moedas = new MoedaDAL().ListarTodos();

                return PartialView("_cad_moeda", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_moeda", p);
            }
        }

        public JsonResult SalvarMoeda(Moeda ba)
        {
            try
            {
                MoedaDAL p = new MoedaDAL();

                Moeda m = new Moeda();


                if (ba.Moeda_id == 0)
                {
                    if (p.ObterPorNome(ba.Moeda_nome))
                    {
                        return Json(new { success = false, message = "Já existe moeda cadastrada com esse nome, tente outra." }, JsonRequestBehavior.AllowGet);
                    }

                    m.Moeda_nome = ba.Moeda_nome;
                    m.Moeda_sigla = ba.Moeda_sigla;

                    p.Salvar(m);

                }
                else
                {
                    m.Moeda_id = ba.Moeda_id;
                    m.Moeda_nome = ba.Moeda_nome;
                    m.Moeda_sigla = ba.Moeda_sigla;

                    p.Atualizar(m);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = m.Moeda_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirMoeda(int idmoeda)
        {
            try
            {
                MoedaDAL b = new MoedaDAL();
                Moeda d = b.ObterPorId(idmoeda);
                b.Excluir(d);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Redes

        public ActionResult GetRedes(string rede)
        {
            S_Rede p = new S_Rede();
            try
            {
                RedeDAL pdal = new RedeDAL();

                p = pdal.ObterPorNome_model(rede);

                ViewBag.redes = new RedeDAL().ListarTodos();

                return PartialView("_cad_rede", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_rede", p);
            }
        }

        public JsonResult SalvarRede(S_Rede re)
        {
            try
            {
                RedeDAL p = new RedeDAL();

                S_Rede m = new S_Rede();

                if (re.Rede_id == 0)
                {
                    if (p.ObterPorNome(re.Rede_nome))
                    {
                        return Json(new { success = false, message = "Já existe Rede cadastrada com esse nome, tente outra." }, JsonRequestBehavior.AllowGet);
                    }

                    m.Rede_nome = re.Rede_nome;
                    m.Rede_endereco = re.Rede_endereco;
                    m.Rede_cnpj = re.Rede_cnpj;
                    m.Rede_telefone = re.Rede_telefone;
                    m.Rede_email = re.Rede_email;

                    p.Salvar(m);
                }
                else
                {
                    m.Rede_id = re.Rede_id;
                    m.Rede_nome = re.Rede_nome;
                    m.Rede_endereco = re.Rede_endereco;
                    m.Rede_cnpj = re.Rede_cnpj;
                    m.Rede_telefone = re.Rede_telefone;
                    m.Rede_email = re.Rede_email;

                    p.Atualizar(m);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = m.Rede_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirRede(int idRede)
        {
            try
            {
                RedeDAL p = new RedeDAL();
                S_Rede d = p.ObterPorId(idRede);
                p.Excluir(d);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Temporada

        public ActionResult GetTemporada(string temporada)
        {
            S_Mercado_Estacao p = new S_Mercado_Estacao();
            try
            {
                MercadoEstacaoDAL pdal = new MercadoEstacaoDAL();

                p = pdal.ObterPorNome(temporada);

                if (p.S_mercado_estacao_pacote == null)
                    p.S_mercado_estacao_pacote = false;

                ViewBag.temporadas = pdal.ListarTodos();

                return PartialView("_cad_temporada", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_temporada", p);
            }
        }

        public JsonResult SalvarTemporada(S_Mercado_Estacao est)
        {
            try
            {
                MercadoEstacaoDAL p = new MercadoEstacaoDAL();

                S_Mercado_Estacao m = new S_Mercado_Estacao();

                if (est.S_mercado_estacao_id == 0)
                {

                    if (p.VerificaExiste(est.S_mercado_estacao_nome))
                    {
                        return Json(new { success = false, message = "Já existe estação/pacote cadastrado com esse nome, tente outro." }, JsonRequestBehavior.AllowGet);
                    }

                    m.S_mercado_estacao_nome = est.S_mercado_estacao_nome;

                    if (est.S_mercado_estacao_pacote == false)
                        m.S_mercado_estacao_pacote = null;
                    else
                        m.S_mercado_estacao_pacote = true;


                    if (est.S_mercado_estacao_seasonal == true)
                        m.S_mercado_estacao_seasonal = true;
                    else
                        m.S_mercado_estacao_seasonal = null;


                    p.Salvar(m);

                }
                else
                {


                    m.S_mercado_estacao_id = est.S_mercado_estacao_id;
                    m.S_mercado_estacao_nome = est.S_mercado_estacao_nome;

                    if (est.S_mercado_estacao_pacote == false)
                        m.S_mercado_estacao_pacote = null;
                    else
                        m.S_mercado_estacao_pacote = true;


                    if (est.S_mercado_estacao_seasonal == true)
                        m.S_mercado_estacao_seasonal = true;
                    else
                        m.S_mercado_estacao_seasonal = null;

                    p.Atualizar(m);

                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = m.S_mercado_estacao_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirTemporada(int idTemporada)
        {
            try
            {
                MercadoEstacaoDAL p = new MercadoEstacaoDAL();
                S_Mercado_Estacao d = p.ObterPorId(idTemporada);
                p.Excluir(d);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Meal

        public ActionResult GetMeal(string meal)
        {
            S_Meal p = new S_Meal();
            try
            {
                MealDAL pdal = new MealDAL();

                p = pdal.ObterPorNome(meal);

                ViewBag.meals = new MealDAL().ListarTodos_SMeal();

                return PartialView("_cad_meal", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_meal", p);
            }
        }

        public JsonResult SalvarMeal(S_Meal me)
        {
            try
            {
                BreakFastDAL bfd = new BreakFastDAL();

                S_Meal m = new S_Meal();

                if (me.S_meal_id == 0)
                {
                    m.S_meal_nome = me.S_meal_nome;
                    bfd.Salvar(m);
                }
                else
                {
                    m.S_meal_id = me.S_meal_id;
                    m.S_meal_nome = me.S_meal_nome;
                    bfd.Atualizar(m);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = m.S_meal_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirMeal(int idmeal)
        {
            try
            {
                BreakFastDAL p = new BreakFastDAL();
                S_Meal d = p.ObterPorId(idmeal);
                p.Excluir(d);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Categ Apto

        public ActionResult GetTarifCateg(string tarifCateg)
        {
            Tarif_Categoria p = new Tarif_Categoria();
            try
            {
                TarifCategoriaDAL pdal = new TarifCategoriaDAL();

                p = pdal.ObterPorNome(tarifCateg);

                ViewBag.categAptos = new TarifCategoriaDAL().ListarTodos();

                return PartialView("_cad_categApto", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_categApto", p);
            }
        }

        public JsonResult SalvarTarifCateg(Tarif_Categoria me)
        {
            try
            {
                TarifCategoriaDAL tcd = new TarifCategoriaDAL();

                Tarif_Categoria tc = new Tarif_Categoria();

                if (me.Tarif_categoria_id == 0)
                {
                    tc.Tarif_categoria_nome = me.Tarif_categoria_nome;
                    tcd.Salvar(tc);
                }
                else
                {
                    tc.Tarif_categoria_id = me.Tarif_categoria_id;
                    tc.Tarif_categoria_nome = me.Tarif_categoria_nome;
                    tcd.Atualizar(tc);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = tc.Tarif_categoria_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirTarifCateg(int idTarifCateg)
        {
            try
            {
                TarifCategoriaDAL tcd = new TarifCategoriaDAL();
                Tarif_Categoria tc = tcd.ObterPorId(idTarifCateg);
                tcd.Excluir(tc);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Categ Hotel

        public ActionResult GetCategHotel(string categHotel)
        {
            S_Categoria_Escrita p = new S_Categoria_Escrita();
            try
            {
                CategoriaEscritaDAL pdal = new CategoriaEscritaDAL();

                p = pdal.ObterPorNome(categHotel);

                ViewBag.categhoteis = new CategoriaEscritaDAL().ListarTodos("");

                return PartialView("_cad_categHotel", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_categHotel", p);
            }
        }

        public JsonResult SalvarCategHotel(S_Categoria_Escrita me)
        {
            try
            {
                CategoriaEscritaDAL ced = new CategoriaEscritaDAL();

                S_Categoria_Escrita sce = new S_Categoria_Escrita();

                if (me.CategoriaEscrita_id == 0)
                {
                    sce.CategoriaEscrita_nome = me.CategoriaEscrita_nome;
                    ced.Salvar(sce);
                }
                else
                {
                    sce.CategoriaEscrita_id = me.CategoriaEscrita_id;
                    sce.CategoriaEscrita_nome = me.CategoriaEscrita_nome;
                    ced.Atualizar(sce);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = sce.CategoriaEscrita_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirCategHotel(int idCategHotel)
        {
            try
            {
                CategoriaEscritaDAL tcd = new CategoriaEscritaDAL();

                if (tcd.VerificaAgregado(idCategHotel))
                {
                    return Json(new { success = false, message = "Esta categoria está agregada há outros cadastros, não pode ser excluída." }, JsonRequestBehavior.AllowGet);
                }

                S_Categoria_Escrita tc = tcd.ObterPorId(idCategHotel);
                tcd.Excluir(tc);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Tipo Servico

        public ActionResult GetTipoServ(string tipoServ)
        {
            S_Servicos_Tipo p = new S_Servicos_Tipo();
            try
            {
                ServicoTipoDAL pdal = new ServicoTipoDAL();

                p = pdal.ObterPorNome(tipoServ);

                ViewBag.tiposervicos = new ServicoTipoDAL().ListarTodos();

                return PartialView("_cad_tipoServico", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_tipoServico", p);
            }
        }

        public JsonResult SalvarTipoServ(S_Servicos_Tipo me)
        {
            try
            {
                ServicoTipoDAL std = new ServicoTipoDAL();

                S_Servicos_Tipo tc = new S_Servicos_Tipo();

                if (me.Tipo_Id == 0)
                {
                    tc.Tipo_Nome = me.Tipo_Nome;
                    std.Salvar(tc);
                }
                else
                {
                    tc.Tipo_Id = me.Tipo_Id;
                    tc.Tipo_Nome = me.Tipo_Nome;
                    std.Atualizar(tc);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = tc.Tipo_Id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirTipoServ(int idTipoServ)
        {
            try
            {
                ServicoTipoDAL tcd = new ServicoTipoDAL();
                S_Servicos_Tipo tc = tcd.ObterPorId(idTipoServ);
                tcd.Excluir(tc);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Categoria

        public ActionResult GetCategoria(string categoria)
        {
            S_Servicos_Tipo_Categ p = new S_Servicos_Tipo_Categ();
            try
            {
                ServicoTipoCategDAL pdal = new ServicoTipoCategDAL();

                p = pdal.ObterPorNome(categoria);

                ViewBag.categorias = new ServicoTipoCategDAL().ListarTodos();
                ViewBag.TipoServCategoria = new ServicoTipoDAL().ListarTodos();

                return PartialView("_cad_categoria", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_categoria", p);
            }
        }

        public JsonResult SalvarCategoria(S_Servicos_Tipo_Categ me)
        {
            try
            {
                ServicoTipoCategDAL std = new ServicoTipoCategDAL();

                S_Servicos_Tipo_Categ tc = new S_Servicos_Tipo_Categ();

                if (me.Tipo_categ_id == 0)
                {
                    tc.Tipo_categ_nome = me.Tipo_categ_nome;
                    tc.Tipo_Id = me.Tipo_Id;
                    std.Salvar(tc);
                }
                else
                {
                    tc.Tipo_categ_id = me.Tipo_categ_id;
                    tc.Tipo_categ_nome = me.Tipo_categ_nome;
                    tc.Tipo_Id = me.Tipo_Id;

                    std.Atualizar(tc);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = tc.Tipo_Id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirCategoria(int idCategoria)
        {
            try
            {
                ServicoTipoCategDAL tcd = new ServicoTipoCategDAL();
                S_Servicos_Tipo_Categ tc = tcd.ObterPorId(idCategoria);
                tcd.Excluir(tc);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Sub Tipo

        public ActionResult GetSubTipo(string subTipo)
        {
            SubItem p = new SubItem();
            try
            {
                SubItemDAL pdal = new SubItemDAL();

                p = pdal.ObterPorNome(subTipo);
                ViewBag.subtipos = new SubItemDAL().ListarTodos();

                return PartialView("_cad_subTipo", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_subTipo", p);
            }
        }

        public JsonResult SalvarSubTipo(SubItem me)
        {
            try
            {
                SubItemDAL std = new SubItemDAL();

                SubItem tc = new SubItem();

                if (me.SubItem_id == 0)
                {
                    if (std.VerificaNome(me.SubItem_nome))
                    {
                        return Json(new { success = false, message = "Já possui esse sub item cadastrado tente outro." }, JsonRequestBehavior.AllowGet);
                    }

                    tc.SubItem_nome = me.SubItem_nome;
                    std.Salvar(tc);
                }
                else
                {
                    tc.SubItem_id = me.SubItem_id;
                    tc.SubItem_nome = me.SubItem_nome;
                    std.Atualizar(tc);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = tc.SubItem_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirSubTipo(int idSubTipo)
        {
            try
            {
                SubItemDAL tcd = new SubItemDAL();
                SubItem tc = tcd.ObterPorId(idSubTipo);
                tcd.Excluir(tc);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Sub Item

        public ActionResult GetsubItem(string subItem)
        {
            ItemSubServico p = new ItemSubServico();
            try
            {
                ItemSubServicoDAL pdal = new ItemSubServicoDAL();

                p = pdal.ObterPorNome(subItem);
                ViewBag.subitens = new ItemSubServicoDAL().ListarTodos();

                return PartialView("_cad_subItem", p);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_cad_subItem", p);
            }
        }

        public JsonResult SalvarsubItem(ItemSubServico me)
        {
            try
            {
                ItemSubServicoDAL std = new ItemSubServicoDAL();

                ItemSubServico tc = new ItemSubServico();

                if (me.ItemSubServico_id == 0)
                {
                    tc.ItemSubServico_nome = me.ItemSubServico_nome;
                    std.Salvar(tc);
                }
                else
                {
                    tc.ItemSubServico_id = me.ItemSubServico_id;
                    tc.ItemSubServico_nome = me.ItemSubServico_nome;
                    std.Atualizar(tc);
                }

                return Json(new { success = true, message = "Registro cadastrado com sucesso.", cod = tc.ItemSubServico_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExcluirsubItem(int idsubItem)
        {
            try
            {
                ItemSubServicoDAL tcd = new ItemSubServicoDAL();
                ItemSubServico tc = tcd.ObterPorId(idsubItem);
                tcd.Excluir(tc);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Classificacao

        [HttpPost]
        public ActionResult UploadClassificacao(PictureClassificacao picture)
        {
            S_Categoria sc = new S_Categoria();
            CategoriaDAL s = new CategoriaDAL();

            if (picture.FilesClassificacao.FirstOrDefault() == null)
            {
                sc.Categ_classificacao = picture.classificacao;
                sc.Categ_imgnome = "semfoto.gif";

                s.Salvar(sc);
            }
            else
            {
                foreach (var file in picture.FilesClassificacao)
                {
                    ViewBag.message = ValidarFotoCategoria(file);
                    if (ViewBag.message.Equals(""))
                    {

                        string foto = Convert.ToString(Guid.NewGuid().ToString("n") + ".jpg");
                        file.SaveAs(Server.MapPath("~/ImgCategorias/") + foto);
                        sc.Categ_classificacao = picture.classificacao;
                        sc.Categ_imgnome = foto;
                        s.Salvar(sc);
                    }
                    else
                    {
                        ViewBag.tab = "classificacao";
                        StartCadastros();
                        return View("Index");
                    }
                }
            }

            ViewBag.tab = "classificacao";
            ViewBag.message = "Classificação cadastrada com sucesso.";
            StartCadastros();
            return View("Index");
        }

        public string ValidarFotoCategoria(HttpPostedFileBase FileUploadCategoria)
        {
            try
            {
                if (FileUploadCategoria.ContentLength > 0) //HasFile -> Contem um Arquivo?
                {
                    if (FileUploadCategoria.FileName.EndsWith(".jpg") || FileUploadCategoria.FileName.EndsWith(".gif"))
                    {
                        if (FileUploadCategoria.ContentLength <= (1024 * 1024))
                        {
                            return "";
                        }
                        else
                        {
                            throw new Exception("Erro. Selecione apenas imagens de até 1MB");
                        }
                    }
                    else
                    {
                        throw new Exception("Erro. Selecione apenas imagens jpg ou gif.");
                    }
                }
                else
                {
                    throw new Exception("Erro. Informe um arquivo para Upload.");
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public JsonResult ExcluirClassificacao(int idClassificacao)
        {
            try
            {
                CategoriaDAL tcd = new CategoriaDAL();
                if (tcd.VerificaAgregado(idClassificacao))
                {
                    return Json(new { success = false, message = "Esta classificação está agregada há outros cadastros, não pode ser excluída." }, JsonRequestBehavior.AllowGet);
                }

                S_Categoria tc = tcd.ObterPorId(idClassificacao);
                tcd.Excluir(tc);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult refreshgridclassificacao()
        {
            try
            {
                ViewBag.classificacoes = new CategoriaDAL().ListarTodos();
                return PartialView("_lst_classificacao");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_classificacao");
            }
        }

        #endregion

        #region Classificacao

        [HttpPost]
        public ActionResult UploadAmenities(PictureAmenities picture)
        {
            S_Categoria sc = new S_Categoria();
            CategoriaDAL s = new CategoriaDAL();


            string extensao = "";

            foreach (var file in picture.FilesAmenities)
            {
                if (file.FileName.EndsWith(".jpg"))
                    extensao = ".jpg";
                else if (file.FileName.EndsWith(".png"))
                    extensao = ".png";
                else if (file.FileName.EndsWith(".gif"))
                    extensao = ".gif";

                ViewBag.messageAmenities = ValidarFotoAmenitie(file);
                if (ViewBag.messageAmenities.Equals(""))
                {
                    S_Amenities Amenity = new S_Amenities();
                    Amenity.S_Amenities_nome = picture.S_Amenities_nome;
                    Amenity.S_Amenities_imgNome = Guid.NewGuid().ToString("n") + extensao;
                    Amenity.S_GroupAmenities_ID = picture.S_GroupAmenities_ID;
                    new AmenitiesDAL().Salvar(Amenity);

                    file.SaveAs(Server.MapPath("~/iconsAmenities/") + Amenity.S_Amenities_imgNome);
                }
                else
                {
                    ViewBag.tab = "amenitie";
                    StartCadastros();
                    return View("Index");
                }
            }


            ViewBag.tab = "amenitie";
            ViewBag.messageAmenities = "Amenitie cadastrada com sucesso.";
            StartCadastros();
            return View("Index");
        }

        public string ValidarFotoAmenitie(HttpPostedFileBase FileUploadAmenitie)
        {
            try
            {
                if (FileUploadAmenitie.ContentLength > 0) //HasFile -> Contem um Arquivo?
                {
                    if (FileUploadAmenitie.FileName.EndsWith(".jpg") ||
                        FileUploadAmenitie.FileName.EndsWith(".gif") ||
                        FileUploadAmenitie.FileName.EndsWith(".png"))
                    {
                        if (FileUploadAmenitie.ContentLength <= (1024 * 1024))
                        {
                            return "";
                        }
                        else
                        {
                            throw new Exception("Erro. Selecione apenas imagens de até 1MB");
                        }
                    }
                    else
                    {
                        throw new Exception("Erro. Selecione apenas imagens jpg ou gif.");
                    }
                }
                else
                {
                    throw new Exception("Erro. Informe um arquivo para Upload.");
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public JsonResult ExcluirAmenitie(int idAmenitie)
        {
            try
            {                
                AmenitiesDAL amDAL = new AmenitiesDAL();
                S_Amenities sa = amDAL.ObterPorID(idAmenitie);
                amDAL.Excluir(sa);

                return Json(new { success = true, message = "Registro excluído com sucesso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult refreshgridamenities()
        {
            try
            {
                ViewBag.amenities = new AmenitiesDAL().ListarTodos();
                return PartialView("_lst_amenities");
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return PartialView("_lst_amenities");
            }
        }

        #endregion

    }
}