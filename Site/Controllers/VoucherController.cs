﻿using DAL.Entity;
using DAL.Persistencia;
using EvoPdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Site.Controllers
{
    public class VoucherController : Controller
    {

        #region Parameters
        GrupoQtdAdultNomeDAL gqanomes = new GrupoQtdAdultNomeDAL();

        GrupoQtdAdultDAL gqa = new GrupoQtdAdultDAL();
        GrupoQtdChdDAL gqc = new GrupoQtdChdDAL();
        GrupoQtdInfDAL gqi = new GrupoQtdInfDAL();
        QuoteTarifasDAL qtarifasd = new QuoteTarifasDAL();
        QuoteTransDAL qtransd = new QuoteTransDAL();
        QuotationGrupoDAL qgd = new QuotationGrupoDAL();
        FileCarrinhoDAL fcd = new FileCarrinhoDAL();
        FileFlightsDAL ffd = new FileFlightsDAL();
        SupplierDAL sd = new SupplierDAL();
        CidadeDAL cd = new CidadeDAL();
        DescricaoDAL descdal = new DescricaoDAL();
        PaisDAL pd = new PaisDAL();
        VoucherDAL vd = new VoucherDAL();
        Voucher_RecommendationsDAL vrd = new Voucher_RecommendationsDAL();
        Voucher_IncludedServicesDAL visd = new Voucher_IncludedServicesDAL();
        Voucher_AtYourDiscretionDAL vayd = new Voucher_AtYourDiscretionDAL();
        Voucher_TripDAL vtd = new Voucher_TripDAL();
        ServicoDAL sdal = new ServicoDAL();

        File_Carrinho fc = new File_Carrinho();
        Quotation_Grupo qg = new Quotation_Grupo();
        File_Flights ff = new File_Flights();
        Supplier supplier = new Supplier();
        S_Servicos serv = new S_Servicos();
        Cidade c = new Cidade();
        DAL.Entity.Voucher v = new DAL.Entity.Voucher();

        List<Voucher_Trip> lvt = new List<Voucher_Trip>();
        List<Quotation_Grupo_Qtd_Adult_Nome> lstqgqan = new List<Quotation_Grupo_Qtd_Adult_Nome>();
        #endregion

        // GET: Voucher
        public ActionResult Index(int quotationGroupId, int optQuote, int pubpage, int grpNomes, string allnames)
        {
            CarregaDados(quotationGroupId, optQuote, pubpage, grpNomes, allnames);


            return View();
        }

        private void CarregaDados(int quotationGroupId, int optQuote, int pubpage, int grpNomes, string allnames)
        {
            qg = qgd.ObterPorId(quotationGroupId);
            fc = fcd.ObterPorIdGrupo(qg.Quotation_Grupo_Id);
            v = vd.ObterPorQuotationGrupoId(qg.Quotation_Grupo_Id, optQuote);

            //hdfQuotationGrupoId.Value = qg.Quotation_Grupo_Id.ToString();

            if (v == null)
            {
                v = new DAL.Entity.Voucher();
            }

            lvt = vtd.ListarPorVoucher(v.Id);


            v.TourName = (string.IsNullOrEmpty(v.TourName)) ? qg.Pax_Group_Name : v.TourName;


            if (string.IsNullOrEmpty(v.TourCode))
            {
                if (qg.tourCode != null)
                    v.TourCode = qg.tourCode.ToString() + " - " + qg.Quotation_Id.ToString();
                else
                    v.TourCode = qg.Quotation_Id.ToString();
            }
            else
            {
                v.TourCode = v.TourCode;
            }

            //var grpn = Convert.ToInt32(grpNomes);

            //buscar nomes novo            
            if (allnames.Equals("OnlyVoucher"))
            {
                var gruposNomes = gqa.ObterPorIdGrupoLST(qg.Quotation_Grupo_Id);
                int qtdAdls = 0;
                string nomesadls = "";

                foreach (var item in gruposNomes)
                {
                    var NomesAdls = gqanomes.ListarPorAdultos(item.Qtd_Adult_id);
                    foreach (var item2 in NomesAdls)
                    {
                        if (item2.voucher == true)
                        {
                            nomesadls = nomesadls + item2.Nome + " " + item2.Sobrenome + ", ";
                            qtdAdls++;
                        }
                    }
                }

                v.NbrTravellers = Convert.ToByte(qtdAdls);
                v.Travellers = nomesadls;
            }
            else
            {
                var gruposNomes = gqa.ObterPorIdGrupoLST(qg.Quotation_Grupo_Id);
                int qtdAdls = 0;
                string nomesadls = "";

                foreach (var item in gruposNomes)
                {
                    var NomesAdls = gqanomes.ListarPorAdultos(item.Qtd_Adult_id);
                    foreach (var item2 in NomesAdls)
                    {
                        nomesadls = nomesadls + item2.Nome + " " + item2.Sobrenome + ", ";
                        qtdAdls++;
                    }
                }

                v.NbrTravellers = Convert.ToByte(qtdAdls);
                v.Travellers = nomesadls;
            }



            v.NbrChildren = Convert.ToByte((v.Id > 0) ? v.NbrChildren.ToString() : gqc.ObterPorIdGrupo(qg.Quotation_Grupo_Id).Qtd.ToString());
            v.NbrInfants = Convert.ToByte((v.Id > 0) ? v.NbrInfants.ToString() : gqi.ObterPorIdGrupo(qg.Quotation_Grupo_Id).Qtd.ToString());

            //var ggr = gqa.ObterPorIdGrupoGrp(qg.Quotation_Grupo_Id, Convert.ToInt32(grpNomes));

            //if (ggr != null)
            //{
            //    v.NbrTravellers = Convert.ToByte((v.Id > 0) ? v.NbrTravellers : ggr.Qtd);
            //}
            //else
            //{
            //    v.NbrTravellers = Convert.ToByte((v.Id > 0) ? v.NbrTravellers : gqa.ObterPorId(qg.Quotation_Grupo_Id).Qtd);
            //}

            //v.NbrChildren = Convert.ToByte((v.Id > 0) ? v.NbrChildren.ToString() : gqc.ObterPorIdGrupo(qg.Quotation_Grupo_Id).Qtd.ToString());
            //v.NbrInfants = Convert.ToByte((v.Id > 0) ? v.NbrInfants.ToString() : gqi.ObterPorIdGrupo(qg.Quotation_Grupo_Id).Qtd.ToString());

            //if (string.IsNullOrEmpty(v.Travellers))
            //{
            //    if (ggr != null)
            //        lstqgqan = ggr.Quotation_Grupo_Qtd_Adult_Nome.ToList();
            //    else
            //        lstqgqan = gqa.ObterPorId(qg.Quotation_Grupo_Id).Quotation_Grupo_Qtd_Adult_Nome.ToList();


            //    string listaNomes = "";
            //    foreach (Quotation_Grupo_Qtd_Adult_Nome nome in lstqgqan)
            //    {
            //        if (!nome.Nome.Equals(""))
            //            listaNomes = listaNomes + nome.Nome + " " + nome.Sobrenome + ", ";
            //    }

            //    v.Travellers = listaNomes;
            //}

            var data = string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:ddd, dd MMM}", qg.dataIN) + " - " +
                string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:ddd, dd MMM}", qg.dateOut);


            if (v.TripEdited)
            {
                lvt = new List<Voucher_Trip>();
                lvt = vtd.ListarPorVoucher(v.Id);

                if (lvt.Count == 0)
                    MontaNaoEditado();

                ViewBag.rptTrip = lvt;
            }
            else
            {
                MontaNaoEditado();
            }

            v.DataInOut = data; //(string.IsNullOrEmpty(v.DataInOut)) ? data : v.DataInOut;
            v.Note = string.IsNullOrEmpty(v.Note) ? "We recommend to re-confirm the mentioned times with the local guides" : v.Note;
            v.SecurityCheck = (string.IsNullOrEmpty(v.SecurityCheck)) ? "Please make a copy of your passport, tickets and credit cards." : v.SecurityCheck;
            v.EmergencyNumbers = (string.IsNullOrEmpty(v.EmergencyNumbers)) ? "" : v.EmergencyNumbers.Replace("<br/>", "\n");

            string titleemergency = "Note: * Emergency? Urgent Question? Call the 24/7 number in the affected country (below) \n";
            titleemergency = titleemergency + "* Your Travel Consultant and our offices are not available outside of office hours \n";
            titleemergency = titleemergency + "* Alternatively, you can reach the Emergency lines by calling any office number + Extension 911 (Example: +1 (800) 747-4540 ext 911*)";

            //"Reach ANY emergency number via ANY international number at Extension 911 (example: +1 (800) 747-4540 ext. 911)"

            v.TitleEmergencyNumbers = string.IsNullOrEmpty(v.TitleEmergencyNumbers) ? titleemergency : v.TitleEmergencyNumbers;


            v.QuotationGrupoId = quotationGroupId;
            v.optQuote = optQuote;

            List<Voucher_Recommendations> lvr = new List<Voucher_Recommendations>();

            if (!v.ImportantRecommendationsEdited)
            {
                var paises = VerificaPaises();

                //if (ecuador)
                //{
                lvr = vrd.AddDefault(v.Id, paises.ecuador, paises.argentina, paises.brasil, paises.bolivia);
                //}
                //else
                //{
                //    lvr = vrd.AddDefault(v.Id);
                //}
            }
            else
            {
                lvr = vrd.ObterPorVoucher(v.Id);
            }


            List<Voucher_IncludedServices> lvis = new List<Voucher_IncludedServices>();

            if (!v.IncludedServicesEdited)
            {
                lvis = visd.AddDefault(v.Id);
            }
            else
            {
                lvis = visd.ObterPorVoucher(v.Id);
            }


            List<Voucher_AtYourDiscretion> lydis = new List<Voucher_AtYourDiscretion>();

            if (!v.AtYourDiscretionEdited)
            {
                lydis = vayd.AddDefault(v.Id);
            }
            else
            {
                lydis = vayd.ObterPorVoucher(v.Id);
            }

            ViewBag.grpnumber = grpNomes;
            ViewBag.allnames = allnames;

            ViewBag.surname = qg.surname;

            ViewBag.AtYourDiscretion = lydis;
            ViewBag.IncludedServices = lvis;
            ViewBag.ImportantRecommendations = lvr.Where(s => s.Recommendation != null).ToList();
            ViewBag.voucher = v;
        }

        private void MontaNaoEditado()
        {
            //try
            //{
            List<string> lstPaises = new List<string>();
            string numEmergenciaPaises = "";
            lvt = new List<Voucher_Trip>();

            Action<Pais> checkPaisesEmergencia = delegate (Pais p)
            {
                if (p != null)
                    if (p.PAIS_descricao != null)
                    {
                        bool existe = false;

                        foreach (string pais in lstPaises)
                            if (p.PAIS_nome == pais)
                                existe = true;

                        if (!existe)
                        {
                            lstPaises.Add(p.PAIS_nome);
                            numEmergenciaPaises += p.PAIS_nome;
                            numEmergenciaPaises += "<br/>";
                            numEmergenciaPaises += p.PAIS_descricao;
                            numEmergenciaPaises += "<br/>";
                            numEmergenciaPaises += "<br/>";
                        }
                    }
            };

            Func<Supplier, string> addSupplierBasicInfo = delegate (Supplier s)
            {
                string sbi = s.S_nome;
                sbi += "<br/>";
                sbi += s.S_endereco;
                sbi += "<br/>";
                sbi += s.S_telefone;
                sbi += "<br/>";
                sbi += s.S_email;

                if ((s.Cidade != null) && (s.Cidade.CID_nome != null))
                    sbi += "<br/>" + s.Cidade.CID_nome + " - " + pd.ObterPorId((int)s.PAIS_id).PAIS_nome;

                return sbi;
            };

            Func<File_Flights, string> addFlightsInformation = delegate (File_Flights ff)
            {
                string flightInfo = "";


                if (ff.Flights_pickUp != null)
                {
                    if (!ff.Flights_pickUp.Trim().Equals(string.Empty))
                    {
                        if (flightInfo.Equals(string.Empty))
                        {
                            flightInfo += "Pick Up Time: " + ff.Flights_pickUp;
                        }
                        else
                        {
                            flightInfo += " / Pick Up Time: " + ff.Flights_pickUp;
                        }
                    }
                }

                if (ff.Flights_nome != null)
                {
                    if (!ff.Flights_nome.Trim().Equals(string.Empty))
                    {
                        flightInfo += "<br />Flight: " + ff.Flights_nome;
                    }
                }

                if (ff.Flights_hora != null)
                {
                    if (!ff.Flights_hora.Trim().Equals(string.Empty))
                    {
                        if (flightInfo.Equals(string.Empty))
                        {
                            if ((ff.Flights_DataFrom != null) && ((bool)ff.Flights_DataFrom))
                            {
                                flightInfo += "Arrives: " + ff.Flights_hora;
                            }
                            else
                            {
                                flightInfo += "Departs: " + ff.Flights_hora;
                            }
                        }
                        else if ((ff.Flights_DataFrom != null) && ((bool)ff.Flights_DataFrom))
                        {
                            flightInfo += " <br /> Arrives: " + ff.Flights_hora;
                        }
                        else
                        {
                            flightInfo += "<br /> Departs: " + ff.Flights_hora;
                        }
                    }
                }


                if (ff.Flights_remarks != null)
                {
                    if (!ff.Flights_remarks.Trim().Equals(string.Empty))
                    {
                        if (flightInfo.Equals(string.Empty))
                        {
                            flightInfo += " " + ff.Flights_remarks;
                        }
                        else
                        {
                            flightInfo += "<br /> " + ff.Flights_remarks;
                        }
                    }
                }

                return flightInfo;
            };

            string Servico = "";
            int id = 1;
            List<dynamic> lstOrdenacao = new List<dynamic>();

            CidadeDAL cDal = new CidadeDAL();

            foreach (Quote_Tarifas p in qtarifasd.ListarTodos(fc.File_id))
            {

                if (p.Status.Equals("CA"))
                    continue;

                if (p.Meal != null)
                    if (p.S_meal_nome.Equals("Buffet Breakfast"))
                        continue;

                Servico = "";
                Voucher_Trip vt = new Voucher_Trip();
                vt.Id = id;
                id += 1;
                vt.VoucherId = v.Id;

                ff = ffd.ObterPorId(Convert.ToInt32(p.Flights_id));

                supplier = sd.ObterPorId(Convert.ToInt32(p.S_id));

                vt.Destination = supplier.Cidade.CID_nome + " - " + supplier.Cidade.Pais.PAIS_nome;

                if (supplier.S_nome.Equals("Explora Sacred Valley"))
                {

                }

                vt.DateRange =
                    string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:ddd, dd MMM}", p.Data_From) +
                    "<br/>to<br/>" +
                    string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:ddd, dd MMM}", p.Data_To);

                checkPaisesEmergencia(supplier.Cidade.Pais);

                var s = "";

                if (ffd.ObterPorId(Convert.ToInt32(p.Flights_id)) != null)
                {

                    string sServ = "";

                    if (ff != null)
                    {
                        sServ = addFlightsInformation(ff);
                    }


                    if (sServ.Equals(string.Empty))
                    {
                        if (p.Meal != null)
                        {
                            if (!p.S_meal_nome.Equals("Buffet Breakfast"))
                            {
                                s = p.Categoria + " - " + p.Room + " - " + p.S_meal_nome + "<br/>" + supplier.S_telefone + "<br/>" + supplier.S_endereco;
                            }
                        }
                        else
                        {
                            s = p.Categoria + " - " + p.Room + " - Breakfast Included." + "<br/>" + supplier.S_telefone + "<br/>" + supplier.S_endereco;
                        }
                    }
                    else
                    {
                        if (p.Meal != null)
                        {
                            if (!p.S_meal_nome.Equals("Buffet Breakfast"))
                            {
                                s = p.Categoria + " - " + p.Room + " - " + p.S_meal_nome + "<br/>" + supplier.S_telefone + "<br/>" + supplier.S_endereco + "<br/>" + sServ + "<br/>";
                            }
                        }
                        else
                        {
                            s = p.Categoria + " - " + p.Room + " - Breakfast Included." + "<br/>" + supplier.S_telefone + "<br/>" + supplier.S_endereco + "<br/>" + sServ + "<br/>";
                        }
                                
                    }

                    if (!String.IsNullOrEmpty(ff.Flights_ConfirmationNbr))
                    {
                        if (s.ToString().Equals(string.Empty))
                        {
                            s = "Confirmation Nbr:  " + ff.Flights_ConfirmationNbr;
                        }
                        else
                        {
                            s = s + "<br/>" + "Confirmation Nbr:  " + ff.Flights_ConfirmationNbr;
                        }
                    }
                    else if (s.ToString().Equals(string.Empty))
                    {
                        s = "Confirmation Nbr:  " + ff.Flights_ConfirmationNbr;
                    }
                    else
                    {
                        s = s + " " + "<br/>" + "Confirmation Nbr:  " + (((qg.tourCode == null) || (qg.tourCode == "")) ? qg.Quotation_Id.ToString() : qg.tourCode.ToString() + " - " + qg.Quotation_Id.ToString());
                    }
                }
                else
                {
                    if (p.Meal != null)
                    {
                        if (!p.S_meal_nome.Equals("Buffet Breakfast"))
                        {
                            s = s + p.Categoria + " - " + p.Room + " - " + p.S_meal_nome + "<br/>" + "Telephone: " + supplier.S_telefone + "<br/>" + "Address: " + supplier.S_endereco;
                            s = s + "<br/>" + "Confirmation Nbr: " + (qg.tourCode == null ? qg.Quotation_Id.ToString() : qg.tourCode.ToString() + " - " + qg.Quotation_Id.ToString());
                        }
                    }
                    else
                    {
                        s = s + p.Categoria + " - " + p.Room + " - Breakfast Included." + "<br/>" + "Telephone: " + supplier.S_telefone + "<br/>" + "Address: " + supplier.S_endereco;
                        s = s + "<br/>" + "Confirmation Nbr: " + (qg.tourCode == null ? qg.Quotation_Id.ToString() : qg.tourCode.ToString() + " - " + qg.Quotation_Id.ToString());
                    }
                           
                }

                var desc = descdal.ObterPorIdSupp((int)p.S_id);
                string s_desc = null;

                if (desc != null)
                {
                    s_desc = descdal.ObterPorIdSupp((int)p.S_id).Desc_voucher;
                }

                if ((s_desc != null) && (!(s_desc.Equals(""))))
                {
                    s = p.S_nome + "<br/>" + s_desc + "<br/>" + s;
                }
                else
                {
                    s = p.S_nome + "<br/>" + s;
                }

                Servico = s;

                //if ((supplier.S_Descricao != null) && (supplier.S_Descricao.Count() > 0))
                //{
                //    Servico += "<br/><br/>" + supplier.S_Descricao.First().Desc_voucher;
                //}

                vt.Description = Servico;
                lstOrdenacao.Add(new { Data = Convert.ToDateTime(p.Data_From), DataTo = Convert.ToDateTime(p.Data_To), VoucherTrip = vt, Ordenacao = p.Ordem });
            }


            foreach (Quote_Transf p in qtransd.ListarTodos(fc.File_id).Where(c => c.Range_id != 0))
            {

                if (p.Status.Equals("CA"))
                    continue;

                Servico = "";
                Voucher_Trip vt = new Voucher_Trip();
                vt.VoucherId = v.Id;
                vt.Id = id;
                id += 1;
                ff = ffd.ObterPorId(Convert.ToInt32(p.Flights_id));

                S_Servicos sserv = sdal.ObterPorNome(p.Transf_nome, (int)p.Trf_CID_id);
                supplier = sd.ObterPorId(Convert.ToInt32(p.S_id));

                Cidade cd = new Cidade();
                if (p.Trf_CID_id != null)
                {
                    cd = cDal.ObterPorId((int)p.Trf_CID_id);
                    vt.Destination = cd.CID_nome + " - " + cd.Pais.PAIS_nome;
                }
                else
                {
                    vt.Destination = supplier.Cidade.CID_nome + " - " + supplier.Cidade.Pais.PAIS_nome;
                }

                vt.DateRange = string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:ddd, dd MMM}", p.Data_From);


                if (supplier.Cidade.Pais.PAIS_id == 43)
                {

                }

                if (cd.CID_id != 0)
                {
                    //checkPaisesEmergencia(supplier.Cidade.Pais);
                    checkPaisesEmergencia(cd.Pais);
                }
                else
                {
                    checkPaisesEmergencia(supplier.Cidade.Pais);
                }


                if ((sserv.SIB != null) && ((bool)sserv.SIB))
                {
                    if ((p.Trf_Tours != null) && (!p.Trf_Tours.Equals("TRS")))
                    {
                        Servico = "Group Transfer";
                    }
                    else
                    {
                        Servico = "Group Tour";
                    }
                }
                else
                {
                    if ((p.Trf_Tours != null) && (!p.Trf_Tours.Equals("TRS")))
                    {
                        Servico = "Private Transfer";
                    }
                    else
                    {
                        Servico = "Private Tour";
                    }
                }

                string s = "";
                if (ffd.ObterPorId(Convert.ToInt32(p.Flights_id)) != null)
                {
                    string sServ = "";

                    if (ff != null)
                    {
                        sServ = addFlightsInformation(ff);
                    }


                    if (sServ.Equals(string.Empty))
                    {
                        s = (((supplier.S_telefone == null) || (supplier.S_telefone.Equals(string.Empty))) ? "" : " - " + supplier.S_telefone) + "<br/>";
                    }
                    else
                    {
                        s = (((supplier.S_telefone == null) || (supplier.S_telefone.Equals(string.Empty))) ? "" : " - " + supplier.S_telefone) + "<br/>" + sServ + "<br/>";
                    }

                    if ((p.Trf_Tours != null) && (!p.Trf_Tours.Equals("TRS")))
                    {
                        if (!String.IsNullOrEmpty(ff.Flights_ConfirmationNbr))
                        {
                            if (s.ToString().Equals(string.Empty))
                            {
                                s = "Flight Confirmation Nbr (PNR):  " + ff.Flights_ConfirmationNbr;
                            }
                            else
                            {
                                s = s + " " + "Flight Confirmation Nbr (PNR):  " + ff.Flights_ConfirmationNbr;
                            }
                        }
                        else if (s.ToString().Equals(string.Empty))
                        {
                            s = "Flight Confirmation Nbr (PNR):  " + ff.Flights_ConfirmationNbr;
                        }
                        else
                        {
                            s = s + " " + "Flight Confirmation Nbr (PNR):  " + (((qg.tourCode == null) || (qg.tourCode == "")) ? qg.Quotation_Id.ToString() : qg.tourCode.ToString() + " - " + qg.Quotation_Id.ToString());
                        }
                    }
                }
                else
                {
                    s = (supplier.S_telefone == "" ? "" : supplier.S_telefone);

                    if ((p.Trf_Tours != null) && (!p.Trf_Tours.Equals("TRS")))
                    {
                        s = s + "<br/>" + "Flight Confirmation Nbr (PNR): " + (((qg.tourCode == null) || (qg.tourCode == "")) ? qg.Quotation_Id.ToString() : qg.tourCode.ToString() + " - " + qg.Quotation_Id.ToString());
                    }
                }

                if (p.Trf_CID_id != null)
                {
                    serv = sdal.ObterPorNome(p.Transf_nome, Convert.ToInt32(p.Trf_CID_id));
                }
                else
                {
                    serv = sdal.ObterPorNome(p.Transf_nome, sd.ObterPorId(Convert.ToInt32(p.S_id)).Cidade.CID_id);
                }

                string descrVoucher = serv.Servicos_descrVoucher;

                if (descrVoucher != null)
                {
                    if (!descrVoucher.Equals(""))
                    {
                        s = p.Transf_nome + "\r\n" + descrVoucher + "\r\n" + s;
                    }
                    else
                    {
                        //s = p.Transf_nome + "\r\n" + s;
                        s = p.Transf_nome + s;
                    }
                }
                else
                {
                    //s = p.Transf_nome + "\r\n" + s;

                    s = p.Transf_nome + s;
                }

                Servico += "<br />" + s;

                if (p.SubServico == true)
                {
                    //List<SubServicosFile> lsf = new SubServicosFileDAL().ListarTodosQTrsf(p.Quote_Transf_id);
                    //foreach (var sf in lsf)
                    //{
                    //    Supplier s = sd.ObterSupplierPorNome(sf.NomeSupplier);
                    //}
                }
                else
                {
                    if ((supplier.S_Descricao != null) && (supplier.S_Descricao.Count() > 0))
                    {
                        Servico += "<br/><br/>" + supplier.S_Descricao.First().Desc_voucher;
                    }
                }

                vt.Description = Servico;
                lstOrdenacao.Add(new { Data = Convert.ToDateTime(p.Data_From), DataTo = Convert.ToDateTime(p.Data_To), VoucherTrip = vt, Ordenacao = p.Ordem });
            }


            QuoteServExtDAL fse = new QuoteServExtDAL();
            foreach (Quote_ServExtra fser in fse.ListarTodos(fc.File_id))
            {

                if (fser.Status.Equals("CA"))
                    continue;


                Servico = "";
                Voucher_Trip vt = new Voucher_Trip();
                vt.VoucherId = v.Id;
                vt.Id = id;
                id += 1;
                ff = ffd.ObterPorId(Convert.ToInt32(fser.Flights_id));
                supplier = sd.ObterPorId(Convert.ToInt32(fser.S_id));
                vt.Destination = supplier.Cidade.CID_nome + " - " + supplier.Cidade.Pais.PAIS_nome;
                checkPaisesEmergencia(supplier.Cidade.Pais);

                vt.DateRange = string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:ddd, dd MMM}", fser.Data_From);

                if (ffd.ObterPorId(Convert.ToInt32(fser.Flights_id)) != null)
                {
                    string sServ = "";

                    if (ff != null)
                    {
                        sServ = addFlightsInformation(ff);
                    }


                    if (!sServ.Equals(string.Empty))
                    {
                        Servico += fser.File_ServExtra_nome + " / " + supplier.S_telefone + "<br/>" + sServ + "<br/>";
                    }
                }
                else
                {
                    Servico = fser.File_ServExtra_nome + " / " + supplier.S_telefone;
                }


                if ((supplier.S_Descricao != null) && (supplier.S_Descricao.Count() > 0))
                {
                    Servico += "<br/><br/>" + supplier.S_Descricao.First().Desc_voucher;
                }


                vt.Description = Servico;
                lstOrdenacao.Add(new { Data = Convert.ToDateTime(fser.Data_From), DataTo = Convert.ToDateTime(fser.Data_To), VoucherTrip = vt, Ordenacao = fser.Ordem });
            }

            if (string.IsNullOrEmpty(v.EmergencyNumbers))
                v.EmergencyNumbers = numEmergenciaPaises.Replace("<br/>", "\n");

            lstOrdenacao = lstOrdenacao.OrderBy(a => a.Data).ThenBy(a => a.Ordenacao).ThenBy(a => a.DataTo).ToList();

            foreach (var item in lstOrdenacao)
            {
                lvt.Add(item.VoucherTrip);
            }

            ViewBag.rptTrip = lvt;
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}
        }

        public ActionResult Voucher(int quotationGroupId, int optQuote, int grpNomes, string allnames)
        {

            ViewBag.lblDataVoucher = string.Format(new System.Globalization.CultureInfo("en-GB"), "{0:ddd, dd MMM}", DateTime.Now);
            CarregaDados(quotationGroupId, optQuote, 1, grpNomes, allnames);

            return View();
        }

        #region funcoes

        public class paisesverif
        {
            public bool ecuador { get; set; }
            public bool argentina { get; set; }
            public bool brasil { get; set; }
            public bool bolivia { get; set; }
        }

        public paisesverif VerificaPaises()
        {
            bool ecuador = false;
            bool argentina = false;
            bool brasil = false;
            bool bolivia = false;

            paisesverif pv = new paisesverif();

            foreach (var item in qtarifasd.ListarTodos(fc.File_id))
            {
                var spp = sd.ObterPorId((int)item.S_id);

                if (spp.PAIS_id == 60)
                    ecuador = true;

                if (spp.PAIS_id == 11)
                    argentina = true;

                if (spp.PAIS_id == 29)
                    brasil = true;

                if (spp.PAIS_id == 28)
                    bolivia = true;

            }

            foreach (var item in qtransd.ListarTodos(fc.File_id).Where(c => c.Range_id != 0))
            {
                if (item.Trf_CID_id != null)
                {
                    var cdd = cd.ObterPorId((int)item.Trf_CID_id);
                    if (cdd.PAIS_id == 60)
                        ecuador = true;

                    if (cdd.PAIS_id == 11)
                        argentina = true;

                    if (cdd.PAIS_id == 29)
                        brasil = true;

                    if (cdd.PAIS_id == 28)
                        bolivia = true;

                }
            }

            QuoteServExtDAL fse = new QuoteServExtDAL();
            foreach (var item in fse.ListarTodos(fc.File_id))
            {
                var spp = sd.ObterPorId((int)item.S_id);
                if (spp.PAIS_id == 60)
                    ecuador = true;

                if (spp.PAIS_id == 11)
                    argentina = true;

                if (spp.PAIS_id == 29)
                    brasil = true;

                if (spp.PAIS_id == 28)
                    bolivia = true;
            }

            pv.argentina = argentina;
            pv.bolivia = bolivia;
            pv.brasil = brasil;
            pv.ecuador = ecuador;

            return pv;

        }

        public class salvarvoucherModel
        {
            public int QuotationGrupoId { get; set; }
            public int optQuote { get; set; }
            public string DataPrograma { get; set; }
            public string TourName { get; set; }
            public string TourCode { get; set; }
            public string QtdTravellers { get; set; }
            public string Children { get; set; }
            public string Infants { get; set; }
            public string TravellersName { get; set; }
            public string Notes { get; set; }
            public string security { get; set; }
            public string EmergencyNumbers { get; set; }

            public string TitleEmergencyNumbers { get; set; }

            public string importantrecommendations { get; set; }
            public string includedservices { get; set; }
            public string atyourdiscretion { get; set; }

            public string vouchertrip { get; set; }
        }

        public JsonResult salvarvoucher(salvarvoucherModel model)
        {
            try
            {
                var securitytxt = HttpUtility.UrlDecode(model.security, System.Text.Encoding.Default);

                string lsttrip = "";
                if (model.vouchertrip != null)
                {
                    model.vouchertrip = model.vouchertrip.Replace("+", "=");
                    lsttrip = HttpUtility.UrlDecode(model.vouchertrip, System.Text.Encoding.Default);
                    lsttrip = lsttrip.Replace("=", "+");
                }

                qg = qgd.ObterPorId(model.QuotationGrupoId);
                fc = fcd.ObterPorIdGrupo(qg.Quotation_Grupo_Id);
                v = vd.ObterPorQuotationGrupoId(qg.Quotation_Grupo_Id, model.optQuote);

                if (v == null) v = new DAL.Entity.Voucher();

                v.QuotationGrupoId = model.QuotationGrupoId;
                v.optQuote = model.optQuote;
                v.DataInOut = model.DataPrograma;
                v.TourName = model.TourName;
                v.TourCode = model.TourCode;
                v.NbrTravellers = Convert.ToByte(model.QtdTravellers);
                v.NbrChildren = Convert.ToByte(model.Children);
                v.NbrInfants = Convert.ToByte(model.Infants);
                v.Travellers = model.TravellersName;
                v.Note = model.Notes;

                v.TitleEmergencyNumbers = model.TitleEmergencyNumbers;

                v.SecurityCheck = securitytxt;

                if (model.EmergencyNumbers != null)
                    v.EmergencyNumbers = model.EmergencyNumbers.Replace("\n", "<br/>");

                v.ImportantRecommendationsEdited = true;
                v.IncludedServicesEdited = true;
                v.AtYourDiscretionEdited = true;
                v.TripEdited = true;

                if (v.Id > 0)
                    vd.AtualizaVoucher(v);
                else
                    vd.AddVoucher(v);


                var recommends = model.importantrecommendations.Split('°');
                var includeds = model.includedservices.Split('°');
                var atyourdisctretion = model.atyourdiscretion.Split('°');

                var vouchertrip = lsttrip.Split('^');

                if (v.Id != 0)
                {
                    foreach (var item in vrd.ObterPorVoucher(v.Id)) vrd.Excluir(item);
                    foreach (var item in visd.ObterPorVoucher(v.Id)) visd.Excluir(item);
                    foreach (var item in vayd.ObterPorVoucher(v.Id)) vayd.Excluir(item);

                    foreach (var item in vtd.ListarPorVoucher(v.Id)) vtd.Deletar(item);
                }

                for (int i = 0; i < recommends.Length; i++)
                {
                    Voucher_Recommendations vr = new Voucher_Recommendations();
                    vr.Recommendation = recommends[i].Replace("&amp;", "&");
                    vr.VoucherId = v.Id;

                    if (vr.Recommendation != "")
                        vrd.Adicionar(vr);
                }

                for (int i = 0; i < includeds.Length; i++)
                {
                    Voucher_IncludedServices vis = new Voucher_IncludedServices();
                    vis.Service = includeds[i].Replace("&amp;", "&");
                    vis.VoucherId = v.Id;

                    if (vis.Service != "")
                        visd.Adicionar(vis);
                }

                for (int i = 0; i < atyourdisctretion.Length; i++)
                {

                    Voucher_AtYourDiscretion vay = new Voucher_AtYourDiscretion();
                    vay.Description = atyourdisctretion[i].Replace("&amp;", "&");
                    vay.VoucherId = v.Id;

                    if (vay.Description != "")
                        vayd.Adicionar(vay);
                }

                for (int i = 0; i < vouchertrip.Length; i++)
                {
                    var filho = vouchertrip[i].Split('~');

                    Voucher_Trip vt = new Voucher_Trip();
                    vt.DateRange = filho[0];
                    vt.Destination = filho[1];
                    vt.Description = filho[2];
                    vt.VoucherId = v.Id;

                    vtd.Adicionar(vt);
                }


                return Json(new { success = true, message = "Voucher Saved" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult trataTextoTrip(string texto)
        {
            var t = texto.Replace("\n", "<br/>");

            return Json(new { success = true, message = "ok", text = t }, JsonRequestBehavior.AllowGet);
        }

        public void ExportPDF(string quotationGroupId, int optQuote, int grpNomes, string allnames)
        {

            // Create a HTML to PDF converter object with default settings
            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();


            // Set license key received after purchase to use the converter in licensed mode
            // Leave it not set to use the converter in demo mode
            htmlToPdfConverter.LicenseKey = "8nxsfWhtfW9ran1rc219bmxzbG9zZGRkZH1t";

            // Set HTML Viewer width in pixels which is the equivalent in converter of the browser window width
            htmlToPdfConverter.HtmlViewerWidth = 1024; // int.Parse(htmlViewerWidthTextBox.Text);


            htmlToPdfConverter.PdfDocumentOptions.AvoidImageBreak = true;


            // Set the HTML content clipping option to force the HTML content width to be exactly HtmlViewerWidth pixels
            // If this option is false then the actual HTML content width can be larger than HtmlViewerWidth pixels in case the HTML page 
            // cannot be entirely displayed in the given viewer width
            // By default this option is false and the HTML content is not clipped
            htmlToPdfConverter.ClipHtmlView = true;

            // Set HTML viewer height in pixels to convert the top part of a HTML page 
            // Leave it not set to convert the entire HTML
            //if (htmlViewerHeightTextBox.Text.Length > 0)
            //htmlToPdfConverter.HtmlViewerHeight = 10; //int.Parse(htmlViewerHeightTextBox.Text);


            // Set PDF page size which can be a predefined size like A4 or a custom size in points 
            // Leave it not set to have a default A4 PDF page
            htmlToPdfConverter.PdfDocumentOptions.PdfPageSize = EvoPdf.PdfPageSize.A4; //SelectedPdfPageSize();


            // Set PDF page orientation to Portrait or Landscape
            // Leave it not set to have a default Portrait orientation for PDF page
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = EvoPdf.PdfPageOrientation.Portrait; //SelectedPdfPageOrientation();


            // Set the maximum time in seconds to wait for HTML page to be loaded 
            // Leave it not set for a default 60 seconds maximum wait time
            htmlToPdfConverter.NavigationTimeout = 30; //int.Parse(navigationTimeoutTextBox.Text);


            // Set an adddional delay in seconds to wait for JavaScript or AJAX calls after page load completed
            // Set this property to 0 if you don't need to wait for such asynchcronous operations to finish
            //if (conversionDelayTextBox.Text.Length > 0)
            htmlToPdfConverter.ConversionDelay = 5; //int.Parse(conversionDelayTextBox.Text);


            // The buffer to receive the generated PDF document
            byte[] outPdfBuffer = null;

            htmlToPdfConverter.PdfDocumentOptions.TopMargin = 0;
            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 0;
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = 0;
            htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 0;

            //htmlToPdfConverter.PdfDocumentOptions.TopMargin = 30;
            //htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 30;
            //htmlToPdfConverter.PdfDocumentOptions.RightMargin = 30;
            //htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 30;


            //string url = HttpContext.Current.Request.Url.AbsoluteUri + "&tipo=fulliti";

            string url = "";
            url = ConfigurationManager.AppSettings["URL_EVO_Voucher"].ToString() + "quotationGroupId=" + quotationGroupId + "&optQuote=" + optQuote + "&grpNomes=" + grpNomes + "&exportpdf=1&allnames=" + allnames;

            //url = ConfigurationManager.AppSettings["URL_EVO_LOCAL_Voucher"].ToString() + "quotationGroupId=" + quotationGroupId + "&optQuote=" + optQuote + "&grpNomes=" + grpNomes + "&exportpdf=1&allnames=" + allnames;

            // Convert the HTML page given by an URL to a PDF document in a memory buffer
            outPdfBuffer = htmlToPdfConverter.ConvertUrl(url);


            // Send the PDF as response to browser

            // Set response content type
            Response.AddHeader("Content-Type", "application/pdf");


            // Instruct the browser to open the PDF file as an attachment or inline
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=OFFER.pdf; size={1}",
                false ? "inline" : "attachment", outPdfBuffer.Length.ToString()));


            // Write the PDF document buffer to HTTP response
            Response.BinaryWrite(outPdfBuffer);


            // End the HTTP response and stop the current page processing
            Response.End();
        }

        public JsonResult resetVoucher(int QuotationGrupoId, int optQuote)
        {
            try
            {
                qg = qgd.ObterPorId(QuotationGrupoId);
                var voucher = vd.ObterPorQuotationGrupoId(qg.Quotation_Grupo_Id, optQuote);

                if (voucher != null)
                    vd.DeleteVoucher(voucher.Id);


                return Json(new { success = true, message = "Voucher reset successfully." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


    }
}