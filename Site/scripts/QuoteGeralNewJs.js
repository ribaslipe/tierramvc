﻿var initialized = 0;
var map;
var PlanCoordinates = new Array();
var Processed = 0;
var FirstMarker;

window.onload = initializemap;


function makeSafe() {
    document.getElementById('txtDescricao').value = window.escape(document.getElementById('txtDescricao').value);
};

function makeDangerous() {
    document.getElementById('txtDescricao').value = window.unescape(document.getElementById('txtDescricao').value);
}

function initializemap() {
    if (initialized == 0) {
        var myLatLng = new google.maps.LatLng(-10.629851, -55.712187);
        var myOptions = {
            zoom: 5,
            center: myLatLng,
            mapTypeId: 'hybrid',
            styles: [
                {
                    featureType: "road",
                    stylers: [
                         { visibility: "off" }
                    ]
                }
            ]
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var v_arr = $('#hdetemp').val().split("~");
        var v_hots = $('#hdetempHoteis').val().split("~");

        var i;

        for (i = 0; i < v_arr.length; ++i) {

            if (i > 0 && $.trim(v_arr[i].split("@")[0].toUpperCase()) == $.trim(v_arr[0].split("@")[0].toUpperCase())) {
                showSleepsPerCity(v_arr[i].split("@")[0], v_arr[0].split("@")[1] + '%2B' + v_arr[i].split("@")[1], 1, v_arr.length, i, v_hots[i]);
            }
            else {

                showSleepsPerCity(v_arr[i].split("@")[0], v_arr[i].split("@")[1], 0, v_arr.length, i, v_hots[i]);

            }
        }
    }

    initialized = 1;
}

function showSleepsPerCity(city, duration, roundtrip, points, index, hotls) {
    var geocoder = new google.maps.Geocoder();
    var htl = hotls.split("@");
    var descricao = "<table border='0' cellpadding='0' cellspacing='0'><tr><td valign='top' style='padding-left: 5px; padding-right:10px; text-align:left; font-family:Tahoma; font-size:9pt'><b>" + htl[0] + "</b><br/>" + htl[1] + "</td></tr></table>";

    geocoder.geocode({ 'address': city }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.setCenter(results[0].geometry.location);
            PlanCoordinates[index] = results[0].geometry.location;
            Processed++;

            if (roundtrip == 0) {
                var markerImage = new google.maps.MarkerImage('/NewTierra/Pages/ashx/tourmapa.ashx?ID=' + duration + '&TYPE=0',
                    new google.maps.Size(30, 30),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(12, 15));

                var marker = new google.maps.Marker({
                    icon: markerImage,
                    map: map,
                    position: results[0].geometry.location,
                    title: 'Hotel'
                });

                var infowindow = new google.maps.InfoWindow({
                    content: descricao
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });

                if (Processed == 1) {
                    FirstMarker = marker;
                }
            }
            else {

                FirstMarker.setMap(null);

                var markerImage = new google.maps.MarkerImage('/NewTierra/Pages/ashx/tourmapa.ashx?ID=' + duration + '&TYPE=1',
                    new google.maps.Size(50, 50),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(22, 25));

                var marker = new google.maps.Marker({
                    icon: markerImage,
                    map: map,
                    position: results[0].geometry.location
                });

            }

            if (Processed == points) {
                var flightPath = new google.maps.Polyline({
                    path: PlanCoordinates,
                    strokeColor: "#4cc3f1",
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });

                flightPath.setMap(map);
            }

        }
    });
}

function mapacenter() {
    google.maps.event.trigger(map, 'resize');
    map.setCenter(map.getCenter());
};

function resizeMap() {
    x = map.getZoom();
    c = map.getCenter();
    google.maps.event.trigger(map, 'resize');
    map.setZoom(x);
    map.setCenter(c);
};

function AbrirPopUp(url, title, w, h) {
    debugger;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left + '');
}

jQuery(function ($) {
    $.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
     'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
     'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

$(document).ready(function () {
    makeDangerous();
    $.datepicker.formatDate('yyyy-mm-dd');

    $("#txtDataFrom").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        onClose: function (selectedDate) {
            $("#txtDataTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#txtDataTo").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        onClose: function (selectedDate) {
            $("#txtDataFrom").datepicker("option", "maxDate", selectedDate);
        }
    });


    $("#btnSalvarDescHotel").click(function () {
        makeSafe();
        $("#form1").submit();
    });

    function convertImagesToBase64() {
        contentDocument = tinymce.get('fullContent').getDoc();
        var regularImages = contentDocument.querySelectorAll("img");
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        [].forEach.call(regularImages, function (imgElement) {
            // preparing canvas for drawing
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            canvas.width = imgElement.width;
            canvas.height = imgElement.height;

            ctx.drawImage(imgElement, 0, 0);
            // by default toDataURL() produces png image, but you can also export to jpeg
            // checkout function's documentation for more details
            var dataURL = canvas.toDataURL();
            imgElement.setAttribute('src', dataURL);
        })
        canvas.remove();
    }
})