﻿var initialized = 0;
var map;
var PlanCoordinates = new Array();
var Processed = 0;
var FirstMarker;

function initializemap() {
    if (initialized == 0) {
        var myLatLng = new google.maps.LatLng(-10.629851, -55.712187);
        var myOptions = {
            zoom: 5,
            center: myLatLng,
            mapTypeId: 'hybrid',
            styles: [
                {
                    featureType: "road",
                    stylers: [
                         { visibility: "off" }
                    ]
                }
            ]
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var v_arr = $('#hdetemp').val().split("~");
        var v_hots = $('#hdetempHoteis').val().split("~");

        var i;

        for (i = 0; i < v_arr.length; ++i) {

            if (i > 0 && $.trim(v_arr[i].split("@")[0].toUpperCase()) == $.trim(v_arr[0].split("@")[0].toUpperCase())) {
                showSleepsPerCity(v_arr[i].split("@")[0], v_arr[0].split("@")[1] + '%2B' + v_arr[i].split("@")[1], 1, v_arr.length, i, v_hots[i]);
            }
            else {
                showSleepsPerCity(v_arr[i].split("@")[0], v_arr[i].split("@")[1], 0, v_arr.length, i, v_hots[i]);
            }
        }
    }

    initialized = 1;
}

function showSleepsPerCity(city, duration, roundtrip, points, index, hotls) {
    var geocoder = new google.maps.Geocoder();
    var htl = hotls.split("@");
    var descricao = "<table border='0' cellpadding='0' cellspacing='0'><tr><td valign='top' style='padding-left: 5px; padding-right:10px; text-align:left; font-family:Tahoma; font-size:9pt'><b>" + htl[0] + "</b><br/>" + htl[1] + "</td></tr></table>";

    geocoder.geocode({ 'address': city }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.setCenter(results[0].geometry.location);
            PlanCoordinates[index] = results[0].geometry.location;
            Processed++;

            if (roundtrip == 0) {
                var markerImage = new google.maps.MarkerImage('/NewTierra/Pages/ashx/tourmapa.ashx?ID=' + duration + '&TYPE=0',
                    new google.maps.Size(30, 30),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(12, 15));

                var marker = new google.maps.Marker({
                    icon: markerImage,
                    map: map,
                    position: results[0].geometry.location,
                    title: 'Hotel'
                });

                var infowindow = new google.maps.InfoWindow({
                    content: descricao
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });

                if (Processed == 1) {
                    FirstMarker = marker;
                }
            }
            else {

                FirstMarker.setMap(null);

                var markerImage = new google.maps.MarkerImage('/NewTierra/Pages/ashx/tourmapa.ashx?ID=' + duration + '&TYPE=1',
                    new google.maps.Size(50, 50),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(22, 25));

                var marker = new google.maps.Marker({
                    icon: markerImage,
                    map: map,
                    position: results[0].geometry.location
                });

            }

            if (Processed == points) {
                var flightPath = new google.maps.Polyline({
                    path: PlanCoordinates,
                    strokeColor: "#4cc3f1",
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });

                flightPath.setMap(map);
            }

        }
    });
}

function mapacenter() {
    google.maps.event.trigger(map, 'resize');
    map.setCenter(map.getCenter());
};

function resizeMap() {
    x = map.getZoom();
    c = map.getCenter();
    google.maps.event.trigger(map, 'resize');
    map.setZoom(x);
    map.setCenter(c);
};

function tabActiveChanged(tabId) {
    $(".nav-tabs li").removeClass("active");
    $("#" + tabId).addClass("active");
}

$(function () {
    $("button")
        .button()
        .click(function (event) {
            event.preventDefault();
        });
});

$(document).ready(function () {
    initializemap();

    $('#tabhighlights').click(function () {
        $('.customapp').height(0);
    });

    $('#tourmap_tab').click(function () {
        $('.customapp').height(585);
    });
    $('#fullit').click(function () {
        $('.customapp').height(0);
    });
    $('#hotels').click(function () {
        $('.customapp').height(0);
    });
    $('#Aprices').click(function () {
        $('.customapp').height(0);
    });

    jScript();

    //$('.btnSaveDescription').click(function () {
    //    var desc = $(this).parent().find('.editorDescricao').summernote('code');
    //    $(this).parent().find('#txtDescricao')[0].value = desc;
    //});
});



function jScript() {
    $('.summernote').summernote({
        minHeight: 150,
        width: 550
    });

    if ($('#hdfEditorPriceNotes').val() != '') {
        $('#editorPriceNotes').summernote('code', $('#hdfEditorPriceNotes').val());
    }

    $('#btnSavePriceNotes').click(function () {
        $('#hdfEditorPriceNotes').val($('#editorPriceNotes').summernote('code'));
    });

    $('#btnSaveNotes').click(function () {
        $('#hdfEditorPriceNotes').val($('#editorPriceNotes').summernote('code'));
    });

    $('.editorDescricao').summernote({
        minHeight: 150,
        width: 550
    });

    $('.dataList .dlQuoteInto').each(function () {
        $(this).children('.divDescrGeral').each(function () {
            $(this).find(".editorDescricao").summernote('code', $(this).find('#txtDescricao').val());
        });
    });

    //$('.btnSaveDescription').click(function () {
    //    var desc = $(this).parent().find('.editorDescricao').summernote('code');
    //    //alert(desc);
    //    $(this).parent().find('#txtDescricao')[0].value = desc;
    //});
}