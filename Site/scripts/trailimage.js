/**
 * This libary is used to generate an image popup and follows your mouse around, when you hover over a thumbnail. It is used on the viewprofile page.
 */

var offsetfrommouse = [15, 15];
var displayduration = 0;
var currentimageheight = 270;
if (document.getElementById || document.all) {
    document.write('<div id="trailimageid">');
    document.write("</div>")
}

function gettrailobj() {
    if (document.getElementById) {
        return document.getElementById("trailimageid").style
    }
    else {
        if (document.all) {
            return document.all.trailimagid.style
        }
    }
}

function gettrailobjnostyle() {
    if (document.getElementById) {
        return document.getElementById("trailimageid")
    }
    else {
        if (document.all) {
            return document.all.trailimagid
        }
    }
}

function truebody() {
    return (!window.opera && document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body
}

function showtrailb(c, a, b) {
    if (a > 0) {
        currentimageheight = a
    }
    document.onmousemove = followmouse;
    newHTML = '<div style="padding: 5px; background-color: #FFF; border: 1px solid #888;z-index: 1100;" >';
    newHTML = newHTML + '<div align="center" style="padding: 8px 2px 2px 2px;z-index: 1100;">' + b;
    newHTML = newHTML + '<br><img src="' + c + '" border="0"></div>';
    newHTML = newHTML + "</div>";
    gettrailobjnostyle().innerHTML = newHTML;
    gettrailobj().display = "inline"
}

function showtrail(b, a) {
    if (a > 0) {
        currentimageheight = a
    }
    document.onmousemove = followmouse;
    newHTML = '<div style="padding: 5px; background-color: #FFF; border: 1px solid #888;z-index: 1100;">';
    newHTML = newHTML + '<div align="center" style="padding: 4px 2px 2px 2px;z-index: 1100;">';
    newHTML = newHTML + '<img src="' + b + '" border="0"></div>';
    newHTML = newHTML + "</div>";
    gettrailobjnostyle().innerHTML = newHTML;
    gettrailobj().display = "inline"
}

function showtrailBatch(c, b, a) {
    document.onmousemove = followmouseBatch;
    cameraHTML = "";
    newHTML = '<div style="padding: 5px; background-color: #FFF; border: 1px solid #888;z-index: 1100;" id="trailInnerDiv">';
    newHTML = newHTML + '<div align="center" style="padding: 8px 2px 2px 2px;z-index: 1100;">';
    newHTML = newHTML + '<img src="' + c + '" border="0"></div>';
    newHTML = newHTML + "</div>";
    gettrailobjnostyle().innerHTML = newHTML;
    gettrailobj().display = "inline";
    gettrailobj().position = "absolute";
    currentimageheight = $("trailInnerDiv").offsetHeight
}

function hidetrail() {
    gettrailobj().innerHTML = " ";
    gettrailobj().display = "none";
    document.onmousemove = "";
    gettrailobj().left = "-10000px"
}

function followmouse(f) {
    var b = offsetfrommouse[0];
    var a = offsetfrommouse[1];
    var d = document.all ? truebody().scrollLeft + truebody().clientWidth : pageXOffset + window.innerWidth - 15;
    var c = document.all ? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight);
    if (typeof f != "undefined") {
        if (d - f.pageX < gettrailobjnostyle().offsetWidth) {
            b = f.pageX - b - gettrailobjnostyle().offsetWidth
        }
        else {
            b += f.pageX
        } if (c - f.pageY < (currentimageheight + 110)) {
            a += f.pageY - Math.max(0, (110 + currentimageheight + f.pageY - c - truebody().scrollTop))
        }
        else {
            a += f.pageY
        }
    }
    else {
        if (typeof window.event != "undefined") {
            if (d - event.clientX < gettrailobjnostyle().offsetWidth) {
                b = event.clientX + truebody().scrollLeft - b - gettrailobjnostyle().offsetWidth
            }
            else {
                b += truebody().scrollLeft + event.clientX
            } if (c - event.clientY < (currentimageheight + 110)) {
                a += event.clientY + truebody().scrollTop - Math.max(0, (110 + currentimageheight + event.clientY - c))
            }
            else {
                a += truebody().scrollTop + event.clientY
            }
        }
    } if (a < 0) {
        a = a * -1
    }
    gettrailobj().left = b + "px";
    gettrailobj().top = a + "px"
}

function followmouseBatch(j) {
    var c = offsetfrommouse[0];
    var b = offsetfrommouse[1];
    var h = document.all ? truebody().scrollLeft + truebody().clientWidth : pageXOffset + window.innerWidth - 15;
    var f = document.all ? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight);
    var g = $("trailInnerDiv");
    var d = g.offsetHeight;
    var a = g.offsetWidth;
    scrollPos = Position.realOffset(truebody());
    if (typeof j != "undefined") {
        if (h - j.pageX < gettrailobjnostyle().offsetWidth) {
            c = j.pageX - c - gettrailobjnostyle().offsetWidth
        }
        else {
            c += j.pageX
        } if ((j.pageY - scrollPos[1]) + d > f) {
            b = -b + (j.pageY - d)
        }
        else {
            b += j.pageY
        }
    }
    else {
        if (typeof window.event != "undefined") {
            if (event.clientX + a > h) {
                c = -c + ((event.clientX + scrollPos[0]) - a)
            }
            else {
                c += (event.clientX + scrollPos[0])
            } if (event.clientY + d > f) {
                b = -b + ((event.clientY + scrollPos[1]) - d)
            }
            else {
                b += (event.clientY + scrollPos[1])
            }
        }
    } if (b < 0) {
        b = b * -1
    }
    gettrailobj().left = c + "px";
    gettrailobj().top = b + "px"
}