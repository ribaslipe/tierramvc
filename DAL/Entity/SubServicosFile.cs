namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SubServicosFile")]
    public partial class SubServicosFile
    {
        [Key]
        public int SubServicosFile_id { get; set; }

        [StringLength(100)]
        public string NomeFantasia { get; set; }

        [StringLength(100)]
        public string NomeSupplier { get; set; }

        [StringLength(100)]
        public string NomeSubItem { get; set; }

        [StringLength(50)]
        public string ServDe { get; set; }

        [StringLength(50)]
        public string ServAte { get; set; }

        public decimal? Valor { get; set; }

        [StringLength(50)]
        public string Moeda { get; set; }

        public int? Moeda_id { get; set; }

        public int? Ranges_id { get; set; }

        public int? Quote_Transf_id { get; set; }

        public bool? FTC { get; set; }

        public decimal? Valor_Conferir { get; set; }

        public decimal? Cambio { get; set; }

        public bool? Cancelado { get; set; }
    }
}
