namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TBItinerary_CodePacote")]
    public partial class TBItinerary_CodePacote
    {
        [Key]
        public int TBItinerary_id { get; set; }

        public string TBItinerary_codePacote { get; set; }

        public int idTabela { get; set; }

        [StringLength(50)]
        public string Quotation_Code { get; set; }

        public int optQuote { get; set; }
    }
}
