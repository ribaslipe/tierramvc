namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_InformationHotel
    {
        [Key]
        public int S_InformationHotel_id { get; set; }

        public int? S_Amenities_id { get; set; }

        public int? S_ServicosHotel_id { get; set; }

        public int? S_id { get; set; }

        public virtual S_ServicosHotel S_ServicosHotel { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
