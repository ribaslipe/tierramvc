namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Categoria_Escrita
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Categoria_Escrita()
        {
            Supplier = new HashSet<Supplier>();
        }

        [Key]
        public int CategoriaEscrita_id { get; set; }

        [StringLength(50)]
        public string CategoriaEscrita_nome { get; set; }

        //public virtual S_Categoria_Escrita S_Categoria_Escrita1 { get; set; }

        //public virtual S_Categoria_Escrita S_Categoria_Escrita2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Supplier> Supplier { get; set; }
    }
}
