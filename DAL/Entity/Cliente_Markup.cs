namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cliente_Markup
    {
        [Key]
        public int Markup_id { get; set; }

        public DateTime? Markup_dataInicial { get; set; }

        public DateTime? Markup_dataFinal { get; set; }

        public decimal? Markup_hotel { get; set; }

        public decimal? Markup_descontoHotel { get; set; }

        public decimal? Markup_servico { get; set; }

        public decimal? Markup_descontoServico { get; set; }

        public int? TipoFile_id { get; set; }

        public int? Cliente_id { get; set; }

        public decimal? Markup_seasonal { get; set; }

        public decimal? Markup_meals { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual Tipo_File Tipo_File { get; set; }
    }
}
