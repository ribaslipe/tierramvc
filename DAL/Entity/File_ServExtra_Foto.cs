namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class File_ServExtra_Foto
    {
        [Key]
        public int fsef_id { get; set; }

        public int fsef_File_ServExtra_ID { get; set; }

        [Required]
        [StringLength(100)]
        public string fsef_nome { get; set; }

        public virtual File_ServExtra File_ServExtra { get; set; }
    }
}
