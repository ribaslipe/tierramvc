namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Meal
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Meal()
        {
            S_Mercado_Meal = new HashSet<S_Mercado_Meal>();
        }

        [Key]
        public int S_meal_id { get; set; }

        [StringLength(50)]
        public string S_meal_nome { get; set; }

        public bool? S_meal_breakfast { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Meal> S_Mercado_Meal { get; set; }
    }
}
