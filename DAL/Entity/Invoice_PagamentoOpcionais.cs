namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Invoice_PagamentoOpcionais
    {
        [Key]
        public int Invoice_PagamentoOpcionais_id { get; set; }

        public decimal? Valor { get; set; }

        public DateTime? data { get; set; }

        public decimal? cambio { get; set; }

        public int? F_Conta_id { get; set; }

        public int? Moeda_id { get; set; }

        public string obs { get; set; }

        public int? Invoice_Opcional_id { get; set; }

        public int? idPagManual { get; set; }

        public virtual F_Conta F_Conta { get; set; }

        public virtual Invoice_Opcional Invoice_Opcional { get; set; }

        public virtual Moeda Moeda { get; set; }
    }
}
