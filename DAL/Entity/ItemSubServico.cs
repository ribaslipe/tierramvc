namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ItemSubServico")]
    public partial class ItemSubServico
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ItemSubServico()
        {
            Monta_Servico = new HashSet<Monta_Servico>();
        }

        [Key]
        public int ItemSubServico_id { get; set; }

        [StringLength(50)]
        public string ItemSubServico_nome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Monta_Servico> Monta_Servico { get; set; }
    }
}
