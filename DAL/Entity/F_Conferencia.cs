namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class F_Conferencia
    {
        public int id { get; set; }

        public decimal? USD { get; set; }

        public decimal? Reais { get; set; }

        public decimal? Saldo_Conferir { get; set; }

        [StringLength(50)]
        public string tabela_referencia { get; set; }

        public int? id_servico { get; set; }

        public bool? status { get; set; }

        public int? id_fatura { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? datahora { get; set; }

        public int? id_login { get; set; }

        [StringLength(50)]
        public string nome_file { get; set; }

        [StringLength(50)]
        public string tipo_conferencia { get; set; }
    }
}
