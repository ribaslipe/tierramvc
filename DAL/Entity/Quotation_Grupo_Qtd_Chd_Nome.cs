namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Quotation_Grupo_Qtd_Chd_Nome
    {
        [Key]
        public int Qtd_Chd_Nome_id { get; set; }

        public int? Qtd_Chd_id { get; set; }

        [StringLength(100)]
        public string Nome { get; set; }

        [StringLength(100)]
        public string Sobrenome { get; set; }

        public int? Idade { get; set; }

        public virtual Quotation_Grupo_Qtd_Chd Quotation_Grupo_Qtd_Chd { get; set; }
    }
}
