namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Promocao")]
    public partial class Promocao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Promocao()
        {
            S_Mercado_Periodo = new HashSet<S_Mercado_Periodo>();
        }

        [Key]
        public int Promocao_id { get; set; }

        [StringLength(20)]
        public string Promocao_nome { get; set; }

        [StringLength(2000)]
        public string Promocao_obs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo> S_Mercado_Periodo { get; set; }
    }
}
