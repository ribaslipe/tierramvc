namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Arredonda")]
    public partial class Arredonda
    {
        [Key]
        public int Arredonda_id { get; set; }

        public int? Arredonda_ate { get; set; }

        public int? Arredonda_de { get; set; }

        public int? Arredonda_para { get; set; }

        public decimal? Arredonda_recebe { get; set; }
    }
}
