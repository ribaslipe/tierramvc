namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TBItinerary_Supplier
    {
        public int Id { get; set; }

        public int S_Id { get; set; }

        [Required]
        [StringLength(500)]
        public string S_Name { get; set; }

        [Required]
        [StringLength(20)]
        public string Code { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateFrom { get; set; }

        public int? idTabela { get; set; }

        public int optQuote { get; set; }
    }
}
