namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FaturaSupplier")]
    public partial class FaturaSupplier
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FaturaSupplier()
        {
            Fatura_Supplier_Pagamento = new HashSet<Fatura_Supplier_Pagamento>();
        }

        [Key]
        public int Fatura_id { get; set; }

        [StringLength(20)]
        public string Fatura_numero { get; set; }

        public decimal? Fatura_multa { get; set; }

        public decimal? Fatura_VlrTotServ { get; set; }

        public DateTime? Fatura_Emissao { get; set; }

        public DateTime? Fatura_Vencimento { get; set; }

        public DateTime? Fatura_Pagamento { get; set; }

        public decimal? Fatura_Descontos { get; set; }

        public string Fatura_Descontos_Obs { get; set; }

        public decimal? Fatura_MoraDia { get; set; }

        public decimal? Fatura_Comissoes { get; set; }

        public string Fatura_Comissoes_Obs { get; set; }

        public decimal? Fatura_MoraDiaR { get; set; }

        public decimal? Fatura_Despesas { get; set; }

        public string Fatura_Despesas_Obs { get; set; }

        public decimal? Fatura_Cambio { get; set; }

        public decimal? Fatura_Deposito { get; set; }

        public string Fatura_Deposito_Obs { get; set; }

        public decimal? Fatura_VlrTotPagar { get; set; }

        public decimal? Fatura_Impostos { get; set; }

        public string Fatura_Impostos_Obs { get; set; }

        public int? Moeda_id { get; set; }

        public int? CentroCusto_id { get; set; }

        public int? ClassificacaoContabil_id { get; set; }

        public int? TipoFatura_id { get; set; }

        public string Fatura_Obs_Geral { get; set; }

        public int? Supplier_id { get; set; }

        [StringLength(50)]
        public string Fatura_Numero_Banco { get; set; }

        [StringLength(50)]
        public string Fatura_Agencia { get; set; }

        [StringLength(50)]
        public string Fatura_Conta { get; set; }

        [StringLength(1000)]
        public string ObsGeral { get; set; }

        public bool? Fatura_Status { get; set; }

        public decimal? Fatura_Valor_Conferir { get; set; }

        public int? id_Fatura_Supplier_Pagamento_Grupo { get; set; }

        [StringLength(200)]
        public string nome_destinatario { get; set; }

        public string obs_conferencia { get; set; }

        public virtual C_CentroCusto C_CentroCusto { get; set; }

        public virtual C_ClassificacaoContabil C_ClassificacaoContabil { get; set; }

        public virtual C_TipoFatura C_TipoFatura { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Fatura_Supplier_Pagamento> Fatura_Supplier_Pagamento { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
