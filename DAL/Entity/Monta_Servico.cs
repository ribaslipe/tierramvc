namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Monta_Servico
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Monta_Servico()
        {
            Monta_Servico_Valores = new HashSet<Monta_Servico_Valores>();
            S_Bases_Valor = new HashSet<S_Bases_Valor>();
            Servico_Completo = new HashSet<Servico_Completo>();
        }

        [Key]
        public int IdMServico { get; set; }

        public decimal? MServico_Bulk_Porc_Taxa { get; set; }

        public decimal? MServico_Bulk_Porc_Imposto { get; set; }

        public decimal? MServico_Bulk_Porc_Comissao { get; set; }

        public decimal? MServico_Bulk_Porc_ValorAdicional { get; set; }

        public decimal? MServico_PorPessoa { get; set; }

        public DateTime? MServico_DataFrom { get; set; }

        public DateTime? MServico_DataTo { get; set; }

        [StringLength(100)]
        public string MServico_Obs { get; set; }

        public int S_id { get; set; }

        public int? Tipo_Id { get; set; }

        public int? Tipo_categ_id { get; set; }

        public int? ItemSubServico_id { get; set; }

        public int? SubItem_id { get; set; }

        public virtual ItemSubServico ItemSubServico { get; set; }

        public virtual S_Servicos_Tipo S_Servicos_Tipo { get; set; }

        public virtual S_Servicos_Tipo_Categ S_Servicos_Tipo_Categ { get; set; }

        public virtual SubItem SubItem { get; set; }

        public virtual Supplier Supplier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Monta_Servico_Valores> Monta_Servico_Valores { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Bases_Valor> S_Bases_Valor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Servico_Completo> Servico_Completo { get; set; }
    }
}
