namespace DAL.Entity
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=Conexao")
        {
        }

        public virtual DbSet<C01repplicationtest> C01repplicationtest { get; set; }
        public virtual DbSet<Arredonda> Arredonda { get; set; }
        public virtual DbSet<BaseTarifaria> BaseTarifaria { get; set; }
        public virtual DbSet<C_CentroCusto> C_CentroCusto { get; set; }
        public virtual DbSet<C_ClassificacaoContabil> C_ClassificacaoContabil { get; set; }
        public virtual DbSet<C_TipoFatura> C_TipoFatura { get; set; }
        public virtual DbSet<Cambio> Cambio { get; set; }
        public virtual DbSet<Cambio_File> Cambio_File { get; set; }
        public virtual DbSet<Cambio_Tipo> Cambio_Tipo { get; set; }
        public virtual DbSet<CategDescricao> CategDescricao { get; set; }
        public virtual DbSet<categoriasPlat> categoriasPlat { get; set; }
        public virtual DbSet<Centro_Custo> Centro_Custo { get; set; }
        public virtual DbSet<Cidade> Cidade { get; set; }
        public virtual DbSet<Cidade_XML> Cidade_XML { get; set; }
        public virtual DbSet<Cli_Contato> Cli_Contato { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Cliente_Markup> Cliente_Markup { get; set; }
        public virtual DbSet<Deposito> Deposito { get; set; }
        public virtual DbSet<Descritivo_Pacote> Descritivo_Pacote { get; set; }
        public virtual DbSet<Discretion_HighLights> Discretion_HighLights { get; set; }
        public virtual DbSet<Email> Email { get; set; }
        public virtual DbSet<F_Agencia> F_Agencia { get; set; }
        public virtual DbSet<F_Banco> F_Banco { get; set; }
        public virtual DbSet<F_Conferencia> F_Conferencia { get; set; }
        public virtual DbSet<F_Conta> F_Conta { get; set; }
        public virtual DbSet<Fatura_Supplier_Pagamento> Fatura_Supplier_Pagamento { get; set; }
        public virtual DbSet<FaturaSupplier> FaturaSupplier { get; set; }
        public virtual DbSet<File_Carrinho> File_Carrinho { get; set; }
        public virtual DbSet<File_Flights> File_Flights { get; set; }
        public virtual DbSet<File_Liguagem> File_Liguagem { get; set; }
        public virtual DbSet<File_ServExtra> File_ServExtra { get; set; }
        public virtual DbSet<File_ServExtra_Foto> File_ServExtra_Foto { get; set; }
        public virtual DbSet<File_ServExtraDescritivo> File_ServExtraDescritivo { get; set; }
        public virtual DbSet<File_Tarifas> File_Tarifas { get; set; }
        public virtual DbSet<File_Transfers> File_Transfers { get; set; }
        public virtual DbSet<Holidays> Holidays { get; set; }
        public virtual DbSet<Holidays_Dates> Holidays_Dates { get; set; }
        public virtual DbSet<Holidays_Periodo> Holidays_Periodo { get; set; }
        public virtual DbSet<Including_HighLights> Including_HighLights { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<Invoice_Opcional> Invoice_Opcional { get; set; }
        public virtual DbSet<Invoice_Pagamento> Invoice_Pagamento { get; set; }
        public virtual DbSet<Invoice_PagamentoOpcionais> Invoice_PagamentoOpcionais { get; set; }
        public virtual DbSet<Invoice_Troca> Invoice_Troca { get; set; }
        public virtual DbSet<ItemSubServico> ItemSubServico { get; set; }
        public virtual DbSet<Linguagem> Linguagem { get; set; }
        public virtual DbSet<LogErro> LogErro { get; set; }
        public virtual DbSet<MarkupFile> MarkupFile { get; set; }
        public virtual DbSet<MarkupGeral> MarkupGeral { get; set; }
        public virtual DbSet<MarkupPais> MarkupPais { get; set; }
        public virtual DbSet<MarkupPaisFile> MarkupPaisFile { get; set; }
        public virtual DbSet<Mercado> Mercado { get; set; }
        public virtual DbSet<MinimoNoites> MinimoNoites { get; set; }
        public virtual DbSet<Moeda> Moeda { get; set; }
        public virtual DbSet<Moeda_XML> Moeda_XML { get; set; }
        public virtual DbSet<Monta_Servico> Monta_Servico { get; set; }
        public virtual DbSet<Monta_Servico_Valores> Monta_Servico_Valores { get; set; }
        public virtual DbSet<OfferHash> OfferHash { get; set; }
        public virtual DbSet<Ordem_Pagamento> Ordem_Pagamento { get; set; }
        public virtual DbSet<Ordem_Pagamento_Itens> Ordem_Pagamento_Itens { get; set; }
        public virtual DbSet<Pacote> Pacote { get; set; }
        public virtual DbSet<Pacote_Descricao> Pacote_Descricao { get; set; }
        public virtual DbSet<Pacote_Foto> Pacote_Foto { get; set; }
        public virtual DbSet<Pagamento_Manual> Pagamento_Manual { get; set; }
        public virtual DbSet<Pais> Pais { get; set; }
        public virtual DbSet<Pais_XML> Pais_XML { get; set; }
        public virtual DbSet<Plataforma_XML> Plataforma_XML { get; set; }
        public virtual DbSet<PlataformaReserva_XML> PlataformaReserva_XML { get; set; }
        public virtual DbSet<Promocao> Promocao { get; set; }
        public virtual DbSet<Quotation> Quotation { get; set; }
        public virtual DbSet<Quotation_Grupo> Quotation_Grupo { get; set; }
        public virtual DbSet<Quotation_Grupo_Qtd_Adult> Quotation_Grupo_Qtd_Adult { get; set; }
        public virtual DbSet<Quotation_Grupo_Qtd_Adult_Nome> Quotation_Grupo_Qtd_Adult_Nome { get; set; }
        public virtual DbSet<Quotation_Grupo_Qtd_Baby> Quotation_Grupo_Qtd_Baby { get; set; }
        public virtual DbSet<Quotation_Grupo_Qtd_Baby_Nome> Quotation_Grupo_Qtd_Baby_Nome { get; set; }
        public virtual DbSet<Quotation_Grupo_Qtd_Chd> Quotation_Grupo_Qtd_Chd { get; set; }
        public virtual DbSet<Quotation_Grupo_Qtd_Chd_Nome> Quotation_Grupo_Qtd_Chd_Nome { get; set; }
        public virtual DbSet<Quotation_Status> Quotation_Status { get; set; }
        public virtual DbSet<Quotation_TourMap> Quotation_TourMap { get; set; }
        public virtual DbSet<Quote_ServExtra> Quote_ServExtra { get; set; }
        public virtual DbSet<Quote_Tarifas> Quote_Tarifas { get; set; }
        public virtual DbSet<Quote_Transf> Quote_Transf { get; set; }
        public virtual DbSet<Ranges> Ranges { get; set; }
        public virtual DbSet<Remark_File> Remark_File { get; set; }
        public virtual DbSet<RoomingList> RoomingList { get; set; }
        public virtual DbSet<S_Amenities> S_Amenities { get; set; }
        public virtual DbSet<S_Bases> S_Bases { get; set; }
        public virtual DbSet<S_Bases_Valor> S_Bases_Valor { get; set; }
        public virtual DbSet<S_Categoria> S_Categoria { get; set; }
        public virtual DbSet<S_Categoria_Escrita> S_Categoria_Escrita { get; set; }
        public virtual DbSet<S_Cond_Pagamento> S_Cond_Pagamento { get; set; }
        public virtual DbSet<S_Contato> S_Contato { get; set; }
        public virtual DbSet<S_Descricao> S_Descricao { get; set; }
        public virtual DbSet<S_GroupAmenities> S_GroupAmenities { get; set; }
        public virtual DbSet<S_Image> S_Image { get; set; }
        public virtual DbSet<S_InformationHotel> S_InformationHotel { get; set; }
        public virtual DbSet<S_Meal> S_Meal { get; set; }
        public virtual DbSet<S_Mercado_Estacao> S_Mercado_Estacao { get; set; }
        public virtual DbSet<S_Mercado_Meal> S_Mercado_Meal { get; set; }
        public virtual DbSet<S_Mercado_Periodo> S_Mercado_Periodo { get; set; }
        public virtual DbSet<S_Mercado_Periodo_Black> S_Mercado_Periodo_Black { get; set; }
        public virtual DbSet<S_Mercado_Periodo_Fds> S_Mercado_Periodo_Fds { get; set; }
        public virtual DbSet<S_Mercado_Tarifa> S_Mercado_Tarifa { get; set; }
        public virtual DbSet<S_Mercado_Tarifa_Status> S_Mercado_Tarifa_Status { get; set; }
        public virtual DbSet<S_Mercado_Tipo_Tarifa> S_Mercado_Tipo_Tarifa { get; set; }
        public virtual DbSet<S_Politica_CHD> S_Politica_CHD { get; set; }
        public virtual DbSet<S_Politica_TC> S_Politica_TC { get; set; }
        public virtual DbSet<S_PoliticaCHDTarifas> S_PoliticaCHDTarifas { get; set; }
        public virtual DbSet<S_PoliticaTCTarifas> S_PoliticaTCTarifas { get; set; }
        public virtual DbSet<S_Rede> S_Rede { get; set; }
        public virtual DbSet<S_Servicos> S_Servicos { get; set; }
        public virtual DbSet<S_Servicos_Tipo> S_Servicos_Tipo { get; set; }
        public virtual DbSet<S_Servicos_Tipo_Categ> S_Servicos_Tipo_Categ { get; set; }
        public virtual DbSet<S_ServicosHotel> S_ServicosHotel { get; set; }
        public virtual DbSet<S_Status> S_Status { get; set; }
        public virtual DbSet<S_SupplierAmenities> S_SupplierAmenities { get; set; }
        public virtual DbSet<S_Tarifa_Base> S_Tarifa_Base { get; set; }
        public virtual DbSet<Servico_Completo> Servico_Completo { get; set; }
        public virtual DbSet<Servico_Completo_File> Servico_Completo_File { get; set; }
        public virtual DbSet<Servicos_Imagem> Servicos_Imagem { get; set; }
        public virtual DbSet<SubItem> SubItem { get; set; }
        public virtual DbSet<SubServicosFile> SubServicosFile { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }
        public virtual DbSet<Supplier_XML> Supplier_XML { get; set; }
        public virtual DbSet<Tarif_Categoria> Tarif_Categoria { get; set; }
        public virtual DbSet<Tarif_Categoria_XML> Tarif_Categoria_XML { get; set; }
        public virtual DbSet<TBItinerary> TBItinerary { get; set; }
        public virtual DbSet<TBItinerary_DescritivoPacote> TBItinerary_DescritivoPacote { get; set; }
        public virtual DbSet<TBItinerary_FlightsInformation> TBItinerary_FlightsInformation { get; set; }
        public virtual DbSet<TBItinerary_Highlights> TBItinerary_Highlights { get; set; }
        public virtual DbSet<TBItinerary_HighlightsImages> TBItinerary_HighlightsImages { get; set; }
        public virtual DbSet<TBItinerary_Info> TBItinerary_Info { get; set; }
        public virtual DbSet<TBItinerary_Prices> TBItinerary_Prices { get; set; }
        public virtual DbSet<TBItinerary_Summary> TBItinerary_Summary { get; set; }
        public virtual DbSet<TBItinerary_Supplier> TBItinerary_Supplier { get; set; }
        public virtual DbSet<Tipo_Base> Tipo_Base { get; set; }
        public virtual DbSet<Tipo_File> Tipo_File { get; set; }
        public virtual DbSet<Tipo_Room> Tipo_Room { get; set; }
        public virtual DbSet<Tipo_Transporte> Tipo_Transporte { get; set; }
        public virtual DbSet<TipoSupl> TipoSupl { get; set; }
        public virtual DbSet<Transf_Bancaria> Transf_Bancaria { get; set; }
        public virtual DbSet<Usuario_Perfil> Usuario_Perfil { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<Usuarios_DiasSemana> Usuarios_DiasSemana { get; set; }
        public virtual DbSet<Usuarios_Horarios> Usuarios_Horarios { get; set; }
        public virtual DbSet<Voucher> Voucher { get; set; }
        public virtual DbSet<Voucher_AtYourDiscretion> Voucher_AtYourDiscretion { get; set; }
        public virtual DbSet<Voucher_IncludedServices> Voucher_IncludedServices { get; set; }
        public virtual DbSet<Voucher_Recommendations> Voucher_Recommendations { get; set; }
        public virtual DbSet<Voucher_Trip> Voucher_Trip { get; set; }
        public virtual DbSet<TBItinerary_CodePacote> TBItinerary_CodePacote { get; set; }
        public virtual DbSet<DescritivoOptions_Pacote> DescritivoOptions_Pacote { get; set; }
        public virtual DbSet<ClassificacaoLodge> ClassificacaoLodge { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Arredonda>()
                .Property(e => e.Arredonda_recebe)
                .HasPrecision(10, 2);

            modelBuilder.Entity<BaseTarifaria>()
                .HasMany(e => e.S_Bases)
                .WithRequired(e => e.BaseTarifaria)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<C_CentroCusto>()
                .Property(e => e.CentroCusto_nome)
                .IsFixedLength();

            modelBuilder.Entity<C_CentroCusto>()
                .HasMany(e => e.Pagamento_Manual)
                .WithOptional(e => e.C_CentroCusto)
                .HasForeignKey(e => e.id_centro_custo);

            modelBuilder.Entity<C_ClassificacaoContabil>()
                .Property(e => e.ClassificacaoContabil_nome)
                .IsFixedLength();

            modelBuilder.Entity<C_ClassificacaoContabil>()
                .HasMany(e => e.Pagamento_Manual)
                .WithOptional(e => e.C_ClassificacaoContabil)
                .HasForeignKey(e => e.id_classificacao_contabil);

            modelBuilder.Entity<C_ClassificacaoContabil>()
                .HasMany(e => e.Transf_Bancaria)
                .WithOptional(e => e.C_ClassificacaoContabil)
                .HasForeignKey(e => e.ClassContab_id);

            modelBuilder.Entity<C_TipoFatura>()
                .Property(e => e.TipoFatura_nome)
                .IsFixedLength();

            modelBuilder.Entity<Cambio>()
                .Property(e => e.Cambio1)
                .HasPrecision(10, 6);

            modelBuilder.Entity<Cambio_File>()
                .Property(e => e.CambioFile)
                .HasPrecision(10, 6);

            modelBuilder.Entity<Cidade>()
                .HasMany(e => e.categoriasPlat)
                .WithOptional(e => e.Cidade)
                .HasForeignKey(e => e.CIDSistema_id);

            modelBuilder.Entity<Cidade>()
                .HasMany(e => e.S_Servicos)
                .WithRequired(e => e.Cidade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.Quotation_Grupo)
                .WithRequired(e => e.Cliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente_Markup>()
                .Property(e => e.Markup_hotel)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Cliente_Markup>()
                .Property(e => e.Markup_descontoHotel)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Cliente_Markup>()
                .Property(e => e.Markup_servico)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Cliente_Markup>()
                .Property(e => e.Markup_descontoServico)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Cliente_Markup>()
                .Property(e => e.Markup_seasonal)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Cliente_Markup>()
                .Property(e => e.Markup_meals)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Deposito>()
                .HasMany(e => e.Invoice_Pagamento)
                .WithOptional(e => e.Deposito)
                .HasForeignKey(e => e.Deposito_id)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Deposito>()
                .HasMany(e => e.Pagamento_Manual)
                .WithOptional(e => e.Deposito)
                .HasForeignKey(e => e.Deposito_id);

            modelBuilder.Entity<Discretion_HighLights>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<F_Agencia>()
                .HasMany(e => e.F_Conta)
                .WithOptional(e => e.F_Agencia)
                .HasForeignKey(e => e.id_Agencia);

            modelBuilder.Entity<F_Banco>()
                .HasMany(e => e.F_Agencia)
                .WithOptional(e => e.F_Banco)
                .HasForeignKey(e => e.id_Banco);

            modelBuilder.Entity<F_Conta>()
                .HasMany(e => e.Deposito)
                .WithOptional(e => e.F_Conta)
                .HasForeignKey(e => e.F_Conta_id);

            modelBuilder.Entity<F_Conta>()
                .HasMany(e => e.Fatura_Supplier_Pagamento)
                .WithOptional(e => e.F_Conta)
                .HasForeignKey(e => e.id_Conta);

            modelBuilder.Entity<F_Conta>()
                .HasMany(e => e.Invoice_Pagamento)
                .WithOptional(e => e.F_Conta)
                .HasForeignKey(e => e.F_Conta_id);

            modelBuilder.Entity<F_Conta>()
                .HasMany(e => e.Invoice_PagamentoOpcionais)
                .WithOptional(e => e.F_Conta)
                .HasForeignKey(e => e.F_Conta_id);

            modelBuilder.Entity<F_Conta>()
                .HasMany(e => e.Pagamento_Manual)
                .WithOptional(e => e.F_Conta)
                .HasForeignKey(e => e.id_conta);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_multa)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_VlrTotServ)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_Descontos)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_MoraDia)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_Comissoes)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_MoraDiaR)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_Despesas)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_Deposito)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_VlrTotPagar)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_Impostos)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .Property(e => e.Fatura_Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<FaturaSupplier>()
                .HasMany(e => e.Fatura_Supplier_Pagamento)
                .WithOptional(e => e.FaturaSupplier)
                .HasForeignKey(e => e.id_FaturaSupplier);

            modelBuilder.Entity<File_Carrinho>()
                .HasMany(e => e.File_Tarifas)
                .WithOptional(e => e.File_Carrinho)
                .WillCascadeOnDelete();

            modelBuilder.Entity<File_Carrinho>()
                .HasMany(e => e.File_Transfers)
                .WithOptional(e => e.File_Carrinho)
                .WillCascadeOnDelete();

            modelBuilder.Entity<File_Carrinho>()
                .HasMany(e => e.Ordem_Pagamento)
                .WithRequired(e => e.File_Carrinho)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<File_Liguagem>()
                .HasMany(e => e.Quotation_Grupo)
                .WithRequired(e => e.File_Liguagem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.S_merc_tarif_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.markupNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.descontoNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.File_ServExtra_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.File_ServExtra_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.File_ServExtra_venda)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.File_ServExtra_total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .Property(e => e.Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_ServExtra>()
                .HasMany(e => e.File_ServExtra_Foto)
                .WithRequired(e => e.File_ServExtra)
                .HasForeignKey(e => e.fsef_File_ServExtra_ID);

            modelBuilder.Entity<File_ServExtra>()
                .HasMany(e => e.File_ServExtraDescritivo)
                .WithRequired(e => e.File_ServExtra)
                .HasForeignKey(e => e.FileServExtra_ID);

            modelBuilder.Entity<File_ServExtra_Foto>()
                .Property(e => e.fsef_nome)
                .IsUnicode(false);

            modelBuilder.Entity<File_ServExtraDescritivo>()
                .Property(e => e.FileServExtra_Descritivo)
                .IsUnicode(false);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.markupNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.descontoNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.S_merc_tarif_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.S_merc_tarif_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.S_merc_tarif_venda)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.S_merc_tarif_total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Tarifas>()
                .Property(e => e.Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.S_merc_tarif_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.markupNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.descontoNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.Transf_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.Transf_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.Transf_venda)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.Transf_total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .Property(e => e.Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<File_Transfers>()
                .HasMany(e => e.Servico_Completo_File)
                .WithOptional(e => e.File_Transfers)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Holidays>()
                .HasMany(e => e.Holidays_Dates)
                .WithOptional(e => e.Holidays)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Holidays>()
                .HasMany(e => e.Holidays_Periodo)
                .WithOptional(e => e.Holidays)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Including_HighLights>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<Invoice_Opcional>()
                .Property(e => e.Grand_Total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Invoice_PagamentoOpcionais>()
                .Property(e => e.Valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Invoice_PagamentoOpcionais>()
                .Property(e => e.cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Invoice_Troca>()
                .Property(e => e.Vr_Troca_USD)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Invoice_Troca>()
                .Property(e => e.Vr_Troca_Real)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Invoice_Troca>()
                .Property(e => e.Tx_Bancaria_USD)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Invoice_Troca>()
                .Property(e => e.Invoice_troca_cambio)
                .HasPrecision(10, 6);

            modelBuilder.Entity<Invoice_Troca>()
                .Property(e => e.Taxa_Bancaria)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupFile>()
                .Property(e => e.MarkupFile_hotel)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupFile>()
                .Property(e => e.MarkupFile_descontoHotel)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupFile>()
                .Property(e => e.MarkupFile_servico)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupFile>()
                .Property(e => e.MarkupFile_descontoServico)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupFile>()
                .Property(e => e.MarkupFile_seasonal)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupFile>()
                .Property(e => e.MarkupFile_meals)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupGeral>()
                .Property(e => e.MarkupGeral_hotel)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupGeral>()
                .Property(e => e.MarkupGeral_descontoHotel)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupGeral>()
                .Property(e => e.MarkupGeral_servico)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupGeral>()
                .Property(e => e.MarkupGeral_descontoServico)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupGeral>()
                .Property(e => e.MarkupGeral_seasonal)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupGeral>()
                .Property(e => e.MarkupGeral_meals)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupPais>()
                .Property(e => e.Markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupPais>()
                .Property(e => e.MarkupServ)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupPais>()
                .Property(e => e.Desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupPais>()
                .Property(e => e.MarkupLodge)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupPaisFile>()
                .Property(e => e.Markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupPaisFile>()
                .Property(e => e.MarkupServ)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupPaisFile>()
                .Property(e => e.Desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<MarkupPaisFile>()
                .Property(e => e.MarkupLodge)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Mercado>()
                .HasMany(e => e.S_Bases)
                .WithRequired(e => e.Mercado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Mercado>()
                .HasMany(e => e.S_Mercado_Periodo_Black)
                .WithRequired(e => e.Mercado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Mercado>()
                .HasMany(e => e.S_Mercado_Periodo)
                .WithRequired(e => e.Mercado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Moeda>()
                .HasMany(e => e.Cambio)
                .WithOptional(e => e.Moeda)
                .HasForeignKey(e => e.MoedaID_De);

            modelBuilder.Entity<Moeda>()
                .HasMany(e => e.Cambio1)
                .WithOptional(e => e.Moeda1)
                .HasForeignKey(e => e.MoedaID_Para);

            modelBuilder.Entity<Moeda>()
                .HasMany(e => e.F_Conta)
                .WithOptional(e => e.Moeda)
                .HasForeignKey(e => e.id_Moeda);

            modelBuilder.Entity<Moeda>()
                .HasMany(e => e.Pagamento_Manual)
                .WithOptional(e => e.Moeda)
                .HasForeignKey(e => e.id_moeda);

            modelBuilder.Entity<Moeda>()
                .HasMany(e => e.S_Bases)
                .WithRequired(e => e.Moeda)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Moeda_XML>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<Monta_Servico>()
                .Property(e => e.MServico_Bulk_Porc_Taxa)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Monta_Servico>()
                .Property(e => e.MServico_Bulk_Porc_Imposto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Monta_Servico>()
                .Property(e => e.MServico_Bulk_Porc_Comissao)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Monta_Servico>()
                .Property(e => e.MServico_Bulk_Porc_ValorAdicional)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Monta_Servico>()
                .Property(e => e.MServico_PorPessoa)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Monta_Servico>()
                .HasMany(e => e.Monta_Servico_Valores)
                .WithOptional(e => e.Monta_Servico)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Monta_Servico>()
                .HasMany(e => e.S_Bases_Valor)
                .WithOptional(e => e.Monta_Servico)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Monta_Servico>()
                .HasMany(e => e.Servico_Completo)
                .WithOptional(e => e.Monta_Servico)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Monta_Servico_Valores>()
                .Property(e => e.MServico_Valores_Bulk_Venda)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Monta_Servico_Valores>()
                .Property(e => e.MServico_Valores_Bulk)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Monta_Servico_Valores>()
                .Property(e => e.MServico_Valores_Bulk_Total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ordem_Pagamento>()
                .HasMany(e => e.Ordem_Pagamento_Itens)
                .WithRequired(e => e.Ordem_Pagamento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ordem_Pagamento_Itens>()
                .Property(e => e.Flag)
                .IsUnicode(false);

            modelBuilder.Entity<Pacote>()
                .Property(e => e.Pacote_ValorTotal)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Pais>()
                .HasMany(e => e.S_Servicos)
                .WithRequired(e => e.Pais)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Plataforma_XML>()
                .HasMany(e => e.Cidade_XML)
                .WithOptional(e => e.Plataforma_XML)
                .HasForeignKey(e => e.id_Plataforma);

            modelBuilder.Entity<Plataforma_XML>()
                .HasMany(e => e.Moeda_XML)
                .WithRequired(e => e.Plataforma_XML)
                .HasForeignKey(e => e.id_Plataforma)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Plataforma_XML>()
                .HasMany(e => e.Pais_XML)
                .WithOptional(e => e.Plataforma_XML)
                .HasForeignKey(e => e.id_Plataforma);

            modelBuilder.Entity<Plataforma_XML>()
                .HasMany(e => e.Supplier_XML)
                .WithRequired(e => e.Plataforma_XML)
                .HasForeignKey(e => e.Id_Plataforma)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Plataforma_XML>()
                .HasMany(e => e.Tarif_Categoria_XML)
                .WithRequired(e => e.Plataforma_XML)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Quotation_Grupo>()
                .HasMany(e => e.Quotation_Grupo_Qtd_Baby)
                .WithOptional(e => e.Quotation_Grupo)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Quotation_Grupo>()
                .HasMany(e => e.Quotation_TourMap)
                .WithRequired(e => e.Quotation_Grupo)
                .HasForeignKey(e => e.tourMap_QuotationGroup_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Quotation_Grupo>()
                .HasMany(e => e.Remark_File)
                .WithOptional(e => e.Quotation_Grupo)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Quotation_Grupo>()
                .HasMany(e => e.RoomingList)
                .WithOptional(e => e.Quotation_Grupo)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Quotation_Grupo_Qtd_Adult>()
                .HasMany(e => e.Quotation_Grupo_Qtd_Adult_Nome)
                .WithRequired(e => e.Quotation_Grupo_Qtd_Adult)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Quotation_Grupo_Qtd_Baby>()
                .HasMany(e => e.Quotation_Grupo_Qtd_Baby_Nome)
                .WithRequired(e => e.Quotation_Grupo_Qtd_Baby)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Quotation_TourMap>()
                .Property(e => e.tourMap_Path)
                .IsUnicode(false);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.markupNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.descontoNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.S_merc_tarif_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.File_ServExtra_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.File_ServExtra_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.File_ServExtra_venda)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.File_ServExtra_total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_ServExtra>()
                .Property(e => e.Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.markupNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.descontoNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.S_merc_tarif_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.S_merc_tarif_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.S_merc_tarif_venda)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.S_merc_tarif_total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Tarifas>()
                .Property(e => e.Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.markupNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.descontoNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.S_merc_tarif_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.Transf_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.Transf_vendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.Transf_venda)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.Transf_total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Quote_Transf>()
                .Property(e => e.Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ranges>()
                .Property(e => e.markup)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ranges>()
                .Property(e => e.desconto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ranges>()
                .Property(e => e.markupNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ranges>()
                .Property(e => e.descontoNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ranges>()
                .Property(e => e.VendaNet)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ranges>()
                .Property(e => e.Valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ranges>()
                .Property(e => e.Venda)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Ranges>()
                .Property(e => e.ValorTotal)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Amenities>()
                .HasMany(e => e.S_SupplierAmenities)
                .WithOptional(e => e.S_Amenities)
                .HasForeignKey(e => e.S_SupplierAmenities_Amenities_ID);

            modelBuilder.Entity<S_Bases>()
                .HasMany(e => e.Monta_Servico_Valores)
                .WithOptional(e => e.S_Bases)
                .WillCascadeOnDelete();

            modelBuilder.Entity<S_Bases>()
                .HasMany(e => e.S_Bases_Valor)
                .WithOptional(e => e.S_Bases)
                .WillCascadeOnDelete();

            modelBuilder.Entity<S_Bases>()
                .HasMany(e => e.S_Tarifa_Base)
                .WithOptional(e => e.S_Bases)
                .WillCascadeOnDelete();

            modelBuilder.Entity<S_Bases_Valor>()
                .Property(e => e.S_Bases_Valor_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Categoria>()
                .Property(e => e.Categ_imgnome)
                .IsUnicode(false);

            //modelBuilder.Entity<S_Categoria_Escrita>()
            //    .HasOptional(e => e.S_Categoria_Escrita1)
            //    .WithRequired(e => e.S_Categoria_Escrita2);

            modelBuilder.Entity<S_GroupAmenities>()
                .Property(e => e.S_GroupAmenities_Name)
                .IsFixedLength();

            modelBuilder.Entity<S_Image>()
                .Property(e => e.Image_nome)
                .IsUnicode(false);

            modelBuilder.Entity<S_Meal>()
                .HasMany(e => e.S_Mercado_Meal)
                .WithRequired(e => e.S_Meal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<S_Mercado_Estacao>()
                .HasMany(e => e.S_Mercado_Periodo)
                .WithRequired(e => e.S_Mercado_Estacao)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<S_Mercado_Meal>()
                .Property(e => e.S_merc_meal_tarifa)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Meal>()
                .Property(e => e.S_merc_meal_tarifa_total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Meal>()
                .Property(e => e.S_merc_meal_porc_taxa)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Meal>()
                .Property(e => e.S_merca_meal_porc_imposto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Periodo_Fds>()
                .Property(e => e.S_mercado_per_fds_from)
                .IsFixedLength();

            modelBuilder.Entity<S_Mercado_Periodo_Fds>()
                .Property(e => e.S_mercado_per_fds_to)
                .IsFixedLength();

            modelBuilder.Entity<S_Mercado_Tarifa>()
                .Property(e => e.S_merc_tarif_tarifa)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Tarifa>()
                .Property(e => e.S_merc_tarif_tarifa_total)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Tarifa>()
                .Property(e => e.S_merc_tarif_porc_taxa)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Tarifa>()
                .Property(e => e.S_merca_tarif_porc_imposto)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Tarifa>()
                .Property(e => e.S_merca_tarif_porc_comissao)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Mercado_Tarifa>()
                .HasMany(e => e.S_PoliticaCHDTarifas)
                .WithOptional(e => e.S_Mercado_Tarifa)
                .WillCascadeOnDelete();

            modelBuilder.Entity<S_Mercado_Tarifa_Status>()
                .HasMany(e => e.S_Mercado_Meal)
                .WithOptional(e => e.S_Mercado_Tarifa_Status)
                .HasForeignKey(e => e.S_merc_tarif_status_id);

            modelBuilder.Entity<S_Mercado_Tarifa_Status>()
                .HasMany(e => e.S_Mercado_Tarifa)
                .WithOptional(e => e.S_Mercado_Tarifa_Status)
                .HasForeignKey(e => e.S_merc_tarif_status_id);

            modelBuilder.Entity<S_Mercado_Tipo_Tarifa>()
                .HasMany(e => e.Quotation_Grupo)
                .WithRequired(e => e.S_Mercado_Tipo_Tarifa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<S_Mercado_Tipo_Tarifa>()
                .HasMany(e => e.S_Mercado_Tarifa)
                .WithRequired(e => e.S_Mercado_Tipo_Tarifa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<S_Politica_CHD>()
                .Property(e => e.S_politicaCHD_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Politica_CHD>()
                .Property(e => e.S_politicaCHD_percent)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Politica_CHD>()
                .HasMany(e => e.S_PoliticaCHDTarifas)
                .WithOptional(e => e.S_Politica_CHD)
                .WillCascadeOnDelete();

            modelBuilder.Entity<S_Politica_TC>()
                .Property(e => e.S_politicaTC_valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Politica_TC>()
                .Property(e => e.S_politicaTC_percent)
                .HasPrecision(10, 2);

            modelBuilder.Entity<S_Servicos>()
                .HasMany(e => e.Holidays)
                .WithOptional(e => e.S_Servicos)
                .WillCascadeOnDelete();

            modelBuilder.Entity<S_Servicos>()
                .HasMany(e => e.Servico_Completo)
                .WithOptional(e => e.S_Servicos)
                .WillCascadeOnDelete();

            modelBuilder.Entity<S_Servicos_Tipo>()
                .Property(e => e.Tipo_Nome)
                .IsUnicode(false);

            modelBuilder.Entity<S_Servicos_Tipo>()
                .HasMany(e => e.S_Servicos_Tipo_Categ)
                .WithOptional(e => e.S_Servicos_Tipo)
                .WillCascadeOnDelete();

            modelBuilder.Entity<S_Status>()
                .HasMany(e => e.S_Servicos)
                .WithRequired(e => e.S_Status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Servico_Completo_File>()
                .Property(e => e.Valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Servico_Completo_File>()
                .Property(e => e.Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Servico_Completo_File>()
                .Property(e => e.Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Servicos_Imagem>()
                .Property(e => e.Serv_img_nome)
                .IsUnicode(false);

            modelBuilder.Entity<SubServicosFile>()
                .Property(e => e.Valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<SubServicosFile>()
                .Property(e => e.Valor_Conferir)
                .HasPrecision(10, 2);

            modelBuilder.Entity<SubServicosFile>()
                .Property(e => e.Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.FaturaSupplier)
                .WithOptional(e => e.Supplier)
                .HasForeignKey(e => e.Supplier_id);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.Monta_Servico)
                .WithRequired(e => e.Supplier)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.Ordem_Pagamento)
                .WithRequired(e => e.Supplier)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.S_Bases)
                .WithRequired(e => e.Supplier)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.S_Contato)
                .WithOptional(e => e.Supplier)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.S_Mercado_Periodo)
                .WithRequired(e => e.Supplier)
                .HasForeignKey(e => e.S_id);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.S_Mercado_Periodo1)
                .WithOptional(e => e.Supplier1)
                .HasForeignKey(e => e.Responsavel_id);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.Supplier_XML)
                .WithRequired(e => e.Supplier)
                .HasForeignKey(e => e.SupplierId_Prosoftware)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Supplier_XML>()
                .HasMany(e => e.Tarif_Categoria_XML)
                .WithOptional(e => e.Supplier_XML)
                .HasForeignKey(e => e.S_XMLId);

            modelBuilder.Entity<Tarif_Categoria>()
                .HasMany(e => e.S_Mercado_Tarifa)
                .WithRequired(e => e.Tarif_Categoria)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tarif_Categoria_XML>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range01)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range02)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range03)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range04)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range05)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range06)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range07)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range08)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range09)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range10)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range11)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range12)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range13)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range14)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range15)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range16)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary>()
                .Property(e => e.Range17)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TBItinerary_FlightsInformation>()
                .Property(e => e.FlightInformation)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Highlights>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Highlights>()
                .Property(e => e.Highlight)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_HighlightsImages>()
                .Property(e => e.FirstImage)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_HighlightsImages>()
                .Property(e => e.LastImage)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_HighlightsImages>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Info>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Info>()
                .Property(e => e.PrecoAlterado)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Info>()
                .Property(e => e.Descricao)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Info>()
                .Property(e => e.ImagemPrincipal)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Info>()
                .Property(e => e.LinkFlights)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Info>()
                .Property(e => e.Preco_Notes)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Prices>()
                .Property(e => e.QuotationCode)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Prices>()
                .Property(e => e.PrecoAlterado)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Prices>()
                .Property(e => e.Preco_Notes)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Prices>()
                .Property(e => e.Base)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Prices>()
                .Property(e => e.Preco_Link)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Supplier>()
                .Property(e => e.S_Name)
                .IsUnicode(false);

            modelBuilder.Entity<TBItinerary_Supplier>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Tipo_Room>()
                .HasMany(e => e.S_Mercado_Tarifa)
                .WithRequired(e => e.Tipo_Room)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Transf_Bancaria>()
                .Property(e => e.TrfContas_Valor)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Transf_Bancaria>()
                .Property(e => e.TrfContas_Cambio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Fatura_Supplier_Pagamento)
                .WithOptional(e => e.Usuarios)
                .HasForeignKey(e => e.id_Login);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Pagamento_Manual)
                .WithOptional(e => e.Usuarios)
                .HasForeignKey(e => e.id_login);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Quotation_Grupo)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios_DiasSemana>()
                .HasMany(e => e.Usuarios_Horarios)
                .WithOptional(e => e.Usuarios_DiasSemana)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Voucher>()
                .HasMany(e => e.Voucher_AtYourDiscretion)
                .WithRequired(e => e.Voucher)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Voucher>()
                .HasMany(e => e.Voucher_IncludedServices)
                .WithRequired(e => e.Voucher)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Voucher>()
                .HasMany(e => e.Voucher_Recommendations)
                .WithRequired(e => e.Voucher)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Voucher>()
                .HasMany(e => e.Voucher_Trip)
                .WithRequired(e => e.Voucher)
                .WillCascadeOnDelete(false);
        }
    }
}
