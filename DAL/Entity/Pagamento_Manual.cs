namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Pagamento_Manual
    {
        public int id { get; set; }

        public int? id_conta { get; set; }

        public int? id_classificacao_contabil { get; set; }

        public int? id_centro_custo { get; set; }

        public int? id_moeda { get; set; }

        public decimal? valor_pago { get; set; }

        public string historico { get; set; }

        [StringLength(50)]
        public string tipo_lancamento { get; set; }

        public DateTime? data { get; set; }

        public int? id_login { get; set; }

        public decimal? saldo_parcial { get; set; }

        public DateTime? data_lancamento { get; set; }

        [StringLength(50)]
        public string InvoiceTroca_guide { get; set; }

        public int? Deposito_id { get; set; }

        public int? TrfContas_id { get; set; }

        public virtual C_CentroCusto C_CentroCusto { get; set; }

        public virtual C_ClassificacaoContabil C_ClassificacaoContabil { get; set; }

        public virtual Deposito Deposito { get; set; }

        public virtual F_Conta F_Conta { get; set; }

        public virtual Moeda Moeda { get; set; }

        public virtual Transf_Bancaria Transf_Bancaria { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
