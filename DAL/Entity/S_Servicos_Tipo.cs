namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Servicos_Tipo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Servicos_Tipo()
        {
            Monta_Servico = new HashSet<Monta_Servico>();
            S_Servicos = new HashSet<S_Servicos>();
            S_Servicos_Tipo_Categ = new HashSet<S_Servicos_Tipo_Categ>();
        }

        [Key]
        public int Tipo_Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Tipo_Nome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Monta_Servico> Monta_Servico { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Servicos> S_Servicos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Servicos_Tipo_Categ> S_Servicos_Tipo_Categ { get; set; }
    }
}
