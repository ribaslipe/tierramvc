namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Servico_Completo_File
    {
        [Key]
        public int ServicoCompletoFile_id { get; set; }

        public int? IdMServico { get; set; }

        public int? File_Transf_id { get; set; }

        public int? Base_Index { get; set; }

        public int? Servicos_Id { get; set; }

        public decimal? Valor { get; set; }

        public int? ServicoCompleto_de { get; set; }

        public int? ServicoCompleto_ate { get; set; }

        public int? SupplierID { get; set; }

        public bool? FTC { get; set; }

        public decimal? Valor_Conferir { get; set; }

        public decimal? Cambio { get; set; }

        public bool? Cancelado { get; set; }

        public int? Quote_Transf_id { get; set; }

        public virtual File_Transfers File_Transfers { get; set; }
    }
}
