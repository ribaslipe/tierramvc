namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Politica_TC
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Politica_TC()
        {
            S_PoliticaTCTarifas = new HashSet<S_PoliticaTCTarifas>();
        }

        [Key]
        public int S_politicaTC_id { get; set; }

        public int? S_politicaTC_qtd { get; set; }

        public int? S_politicaTC_idadeDe { get; set; }

        public int? S_politicaTC_idade { get; set; }

        [StringLength(20)]
        public string S_politicaTC_free { get; set; }

        public decimal? S_politicaTC_valor { get; set; }

        public decimal? S_politicaTC_percent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_PoliticaTCTarifas> S_PoliticaTCTarifas { get; set; }
    }
}
