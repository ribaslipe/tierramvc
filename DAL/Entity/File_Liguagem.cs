namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class File_Liguagem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public File_Liguagem()
        {
            Quotation_Grupo = new HashSet<Quotation_Grupo>();
        }

        [Key]
        public int FLinguagem_id { get; set; }

        [StringLength(50)]
        public string FLinguagem_nome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_Grupo> Quotation_Grupo { get; set; }
    }
}
