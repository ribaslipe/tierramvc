namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Descritivo_Pacote
    {
        [Key]
        public int Descritivo_id { get; set; }

        public string Descritivo_descr0 { get; set; }

        public string Descritivo_descr1 { get; set; }

        public string Descritivo_descr2 { get; set; }

        public string Descritivo_descr3 { get; set; }

        public string Descritivo_descr4 { get; set; }

        public string Descritivo_descr5 { get; set; }

        public string Descritivo_descr6 { get; set; }

        public string Descritivo_descr7 { get; set; }

        public string Descritivo_descr8 { get; set; }

        public string Descritivo_descr9 { get; set; }

        public int? S_merc_periodo_id { get; set; }

        public string Descritivo_descr10 { get; set; }

        public string Descritivo_descr11 { get; set; }

        public string Descritivo_descr12 { get; set; }

        public string Descritivo_descr13 { get; set; }

        public string Descritivo_descr14 { get; set; }

        public string Descritivo_descr15 { get; set; }

        public string Descritivo_descr16 { get; set; }

        public string Descritivo_descr17 { get; set; }

        public string Descritivo_descr18 { get; set; }

        public string Descritivo_descr19 { get; set; }

        public string Descritivo_descr20 { get; set; }
    }
}
