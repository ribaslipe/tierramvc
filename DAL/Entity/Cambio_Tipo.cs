namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cambio_Tipo
    {
        public int id { get; set; }

        [StringLength(50)]
        public string nome { get; set; }
    }
}
