namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Discretion_HighLights
    {
        public int ID { get; set; }

        [Required]
        public string Descricao { get; set; }

        [Required]
        [StringLength(50)]
        public string Quotation_Code { get; set; }

        public bool? Visible { get; set; }

        public int optQuote { get; set; }
    }
}
