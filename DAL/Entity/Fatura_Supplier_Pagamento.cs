namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Fatura_Supplier_Pagamento
    {
        public int id { get; set; }

        public int? id_Conta { get; set; }

        public int? id_Login { get; set; }

        public int? id_FaturaSupplier { get; set; }

        public DateTime? data { get; set; }

        public int? grupo { get; set; }

        [StringLength(50)]
        public string tipo_pagamento { get; set; }

        public decimal? saldo_parcial { get; set; }

        public decimal? valor_pago { get; set; }

        public DateTime? data_lancamento { get; set; }

        public string historico { get; set; }

        [StringLength(50)]
        public string numero_cheque { get; set; }

        public virtual F_Conta F_Conta { get; set; }

        public virtual FaturaSupplier FaturaSupplier { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
