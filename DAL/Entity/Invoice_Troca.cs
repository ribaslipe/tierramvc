namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Invoice_Troca
    {
        [Key]
        public int Invoice_troca_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Invoice_troca_dtEntrada { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Invoice_troca_dtTroca { get; set; }

        public decimal? Vr_Troca_USD { get; set; }

        public decimal? Vr_Troca_Real { get; set; }

        public decimal? Tx_Bancaria_USD { get; set; }

        public decimal? Invoice_troca_cambio { get; set; }

        public int? Creditada_tab_F_conta_id { get; set; }

        public int? Debitada_inv_F_conta_id { get; set; }

        [StringLength(50)]
        public string Code_baixa_extrato { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Invoice_troca_dtRecebimento { get; set; }

        [StringLength(50)]
        public string Invoice_troca_tipo { get; set; }

        public int? Cliente_id { get; set; }

        [StringLength(50)]
        public string File_Code { get; set; }

        public int? Invoice_Pagamento_id { get; set; }

        public int? CentroCusto_id { get; set; }

        public int? ClassificacaoContabil_id { get; set; }

        [StringLength(100)]
        public string Troca_Guide { get; set; }

        public bool? Trocado { get; set; }

        public decimal? Taxa_Bancaria { get; set; }

        public virtual C_CentroCusto C_CentroCusto { get; set; }

        public virtual C_ClassificacaoContabil C_ClassificacaoContabil { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual Invoice_Pagamento Invoice_Pagamento { get; set; }
    }
}
