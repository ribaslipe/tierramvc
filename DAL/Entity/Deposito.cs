namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Deposito")]
    public partial class Deposito
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Deposito()
        {
            Invoice_Pagamento = new HashSet<Invoice_Pagamento>();
            Pagamento_Manual = new HashSet<Pagamento_Manual>();
        }

        public int id { get; set; }

        public decimal? valor { get; set; }

        public decimal? cambio { get; set; }

        public int? moeda_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? data { get; set; }

        public int? Cliente_id { get; set; }

        public string Historico { get; set; }

        public int? F_Conta_id { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual F_Conta F_Conta { get; set; }

        public virtual Moeda Moeda { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_Pagamento> Invoice_Pagamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pagamento_Manual> Pagamento_Manual { get; set; }
    }
}
