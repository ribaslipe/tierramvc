namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Pacote_Foto
    {
        [Key]
        public int Pacote_Foto_id { get; set; }

        [StringLength(50)]
        public string Pacote_Foto_nome { get; set; }

        [StringLength(50)]
        public string Pacote_Foto_capa { get; set; }

        [StringLength(1000)]
        public string Pacote_Foto_memo { get; set; }

        [StringLength(50)]
        public string Pacote_Foto_hupload { get; set; }

        [StringLength(1000)]
        public string Pacote_Foto_descr { get; set; }

        [StringLength(20)]
        public string Pacote_Code { get; set; }
    }
}
