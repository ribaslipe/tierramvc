namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Tarifa_Base
    {
        [Key]
        public int S_Tarifa_Base_id { get; set; }

        public int? Base_id { get; set; }

        public int? S_merc_tarif_id { get; set; }

        public int? S_merc_meal_id { get; set; }

        public virtual S_Bases S_Bases { get; set; }
    }
}
