namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Bases
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Bases()
        {
            Monta_Servico_Valores = new HashSet<Monta_Servico_Valores>();
            S_Bases_Valor = new HashSet<S_Bases_Valor>();
            S_Tarifa_Base = new HashSet<S_Tarifa_Base>();
        }

        [Key]
        public int Base_id { get; set; }

        public int? Base_de { get; set; }

        public int? Base_ate { get; set; }

        public int? Base_index { get; set; }

        public int S_id { get; set; }

        public int Mercado_id { get; set; }

        public int BaseTarifaria_id { get; set; }

        public int Moeda_id { get; set; }

        public int? IdTipoTrans { get; set; }

        public int? TipoBase_id { get; set; }

        public virtual BaseTarifaria BaseTarifaria { get; set; }

        public virtual Mercado Mercado { get; set; }

        public virtual Moeda Moeda { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Monta_Servico_Valores> Monta_Servico_Valores { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual Tipo_Base Tipo_Base { get; set; }

        public virtual Tipo_Transporte Tipo_Transporte { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Bases_Valor> S_Bases_Valor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Tarifa_Base> S_Tarifa_Base { get; set; }
    }
}
