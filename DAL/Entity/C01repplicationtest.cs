namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("01repplicationtest")]
    public partial class C01repplicationtest
    {
        public int id { get; set; }

        public DateTime test_date { get; set; }

        [StringLength(20)]
        public string server_name { get; set; }
    }
}
