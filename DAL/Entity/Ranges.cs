namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Ranges
    {
        [Key]
        public int Ranges_id { get; set; }

        [StringLength(50)]
        public string Ranges_de { get; set; }

        [StringLength(50)]
        public string Ranges_ate { get; set; }

        public int? Quotation_Id { get; set; }

        [StringLength(50)]
        public string Flag { get; set; }

        public int? FileTabelaId { get; set; }

        public decimal? markup { get; set; }

        public decimal? desconto { get; set; }

        public decimal? markupNet { get; set; }

        public decimal? descontoNet { get; set; }

        public decimal? VendaNet { get; set; }

        public decimal? Valor { get; set; }

        public decimal? Venda { get; set; }

        public decimal? ValorTotal { get; set; }

        public int? Ordem { get; set; }

        public int? RangeID { get; set; }

        public int? DAY { get; set; }

        public int? QtdTC { get; set; }

        public int? OptQuote { get; set; }

        [StringLength(50)]
        public string OptQuoteNome { get; set; }

        public virtual Quotation Quotation { get; set; }
    }
}
