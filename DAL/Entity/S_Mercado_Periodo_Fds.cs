namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Mercado_Periodo_Fds
    {
        [Key]
        public int S_mercado_per_fds_id { get; set; }

        [StringLength(10)]
        public string S_mercado_per_fds_from { get; set; }

        [StringLength(10)]
        public string S_mercado_per_fds_to { get; set; }

        public int S_merc_periodo_id { get; set; }

        public virtual S_Mercado_Periodo S_Mercado_Periodo { get; set; }
    }
}
