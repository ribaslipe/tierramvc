namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TBItinerary_FlightsInformation
    {
        public int Id { get; set; }

        [Required]
        public string FlightInformation { get; set; }

        [Required]
        [StringLength(12)]
        public string QuotationCode { get; set; }

        public int optQuote { get; set; }
    }
}
