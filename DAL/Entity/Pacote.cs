namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Pacote")]
    public partial class Pacote
    {
        [Key]
        public int Pacote_id { get; set; }

        [StringLength(20)]
        public string Pacote_Code { get; set; }

        [StringLength(200)]
        public string Pacote_NomePacote { get; set; }

        [StringLength(50)]
        public string Pacote_Quote { get; set; }

        [StringLength(200)]
        public string SuppNome { get; set; }

        [StringLength(200)]
        public string Categoria { get; set; }

        [StringLength(200)]
        public string Room { get; set; }

        public int? S_id { get; set; }

        [StringLength(200)]
        public string ServNome { get; set; }

        [StringLength(200)]
        public string ServExtraNome { get; set; }

        [StringLength(200)]
        public string MealNome { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DataFrom { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DataTo { get; set; }

        [StringLength(200)]
        public string Cidade { get; set; }

        [StringLength(20)]
        public string Flag { get; set; }

        public int? Ordem { get; set; }

        public decimal? Pacote_ValorTotal { get; set; }

        public int? NumNoites { get; set; }
    }
}
