namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MarkupGeral")]
    public partial class MarkupGeral
    {
        [Key]
        public int MarkupGeral_id { get; set; }

        public DateTime? MarkupGeral_dataInicial { get; set; }

        public DateTime? MarkupGeral_dataFinal { get; set; }

        public decimal? MarkupGeral_hotel { get; set; }

        public decimal? MarkupGeral_descontoHotel { get; set; }

        public decimal? MarkupGeral_servico { get; set; }

        public decimal? MarkupGeral_descontoServico { get; set; }

        public decimal? MarkupGeral_seasonal { get; set; }

        public decimal? MarkupGeral_meals { get; set; }
    }
}
