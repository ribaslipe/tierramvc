namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Supplier")]
    public partial class Supplier
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Supplier()
        {
            CategDescricao = new HashSet<CategDescricao>();
            FaturaSupplier = new HashSet<FaturaSupplier>();
            Monta_Servico = new HashSet<Monta_Servico>();
            Ordem_Pagamento = new HashSet<Ordem_Pagamento>();
            S_Bases = new HashSet<S_Bases>();
            S_Cond_Pagamento = new HashSet<S_Cond_Pagamento>();
            S_Contato = new HashSet<S_Contato>();
            S_Descricao = new HashSet<S_Descricao>();
            S_Image = new HashSet<S_Image>();
            S_InformationHotel = new HashSet<S_InformationHotel>();
            S_Mercado_Periodo = new HashSet<S_Mercado_Periodo>();
            S_Mercado_Periodo1 = new HashSet<S_Mercado_Periodo>();
            S_Mercado_Periodo_Black = new HashSet<S_Mercado_Periodo_Black>();
            Supplier_XML = new HashSet<Supplier_XML>();
        }

        [Key]
        public int S_id { get; set; }

        [StringLength(50)]
        public string S_nome { get; set; }

        [StringLength(50)]
        public string S_razaosocial { get; set; }

        [StringLength(50)]
        public string S_cnpj { get; set; }

        [StringLength(250)]
        public string S_endereco { get; set; }

        [StringLength(15)]
        public string S_cep { get; set; }

        [StringLength(20)]
        public string S_fax { get; set; }

        [StringLength(50)]
        public string S_teleplantao { get; set; }

        [StringLength(50)]
        public string S_telefone { get; set; }

        [StringLength(500)]
        public string S_http { get; set; }

        [StringLength(50)]
        public string S_email { get; set; }

        [StringLength(50)]
        public string S_faturar { get; set; }

        [StringLength(50)]
        public string S_reservas { get; set; }

        public int? S_qtdpessoas { get; set; }

        [StringLength(15)]
        public string S_pessoaOUapt { get; set; }

        public int? Responsavel_id { get; set; }

        public int? STATUS_id { get; set; }

        public int? TIPOSUPL_id { get; set; }

        public int? CID_id { get; set; }

        public int? PAIS_id { get; set; }

        public int? Categ_id { get; set; }

        public int? Rede_id { get; set; }

        public int? CategoriaEscrita_id { get; set; }

        public bool? Tarifario { get; set; }

        public bool? Recomended { get; set; }

        public bool? B2B { get; set; }

        public int? Moeda_id { get; set; }

        public int? ClassificacaoLodge_id { get; set; }

        [StringLength(50)]
        public string Latitude { get; set; }

        [StringLength(50)]
        public string Longitude { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CategDescricao> CategDescricao { get; set; }

        public virtual Cidade Cidade { get; set; }

        public virtual ClassificacaoLodge ClassificacaoLodge { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FaturaSupplier> FaturaSupplier { get; set; }

        public virtual Moeda Moeda { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Monta_Servico> Monta_Servico { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ordem_Pagamento> Ordem_Pagamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Bases> S_Bases { get; set; }

        public virtual S_Categoria_Escrita S_Categoria_Escrita { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Cond_Pagamento> S_Cond_Pagamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Contato> S_Contato { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Descricao> S_Descricao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Image> S_Image { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_InformationHotel> S_InformationHotel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo> S_Mercado_Periodo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo> S_Mercado_Periodo1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo_Black> S_Mercado_Periodo_Black { get; set; }

        public virtual TipoSupl TipoSupl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Supplier_XML> Supplier_XML { get; set; }
    }
}
