namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Monta_Servico_Valores
    {
        [Key]
        public int IdMServicoValores { get; set; }

        public decimal? MServico_Valores_Bulk_Venda { get; set; }

        public decimal? MServico_Valores_Bulk { get; set; }

        public decimal? MServico_Valores_Bulk_Total { get; set; }

        public int? IdMServico { get; set; }

        public int? Base_id { get; set; }

        public int? S_mercado_tarifa_status_id { get; set; }

        public virtual Monta_Servico Monta_Servico { get; set; }

        public virtual S_Bases S_Bases { get; set; }

        public virtual S_Mercado_Tarifa_Status S_Mercado_Tarifa_Status { get; set; }
    }
}
