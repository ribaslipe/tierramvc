namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TBItinerary_Info
    {
        public int ID { get; set; }

        public string Note { get; set; }

        [StringLength(50)]
        public string PrecoAlterado { get; set; }

        public string Descricao { get; set; }

        public string ImagemPrincipal { get; set; }

        public string LinkFlights { get; set; }

        public int IdTabela { get; set; }

        [Required]
        [StringLength(50)]
        public string Quotation_Code { get; set; }

        public string Preco_Notes { get; set; }

        public int optQuote { get; set; }
    }
}
