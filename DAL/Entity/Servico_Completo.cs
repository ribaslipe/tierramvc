namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Servico_Completo
    {
        [Key]
        public int ServicoCompleto_id { get; set; }

        public int? IdMServico { get; set; }

        public int? IdMServico_Temporada { get; set; }

        public int? Base_Index { get; set; }

        public int? Servicos_Id { get; set; }

        public int? ServicoCompleto_de { get; set; }

        public int? ServicoCompleto_ate { get; set; }

        public bool? SubServico { get; set; }

        public virtual Monta_Servico Monta_Servico { get; set; }

        public virtual S_Servicos S_Servicos { get; set; }
    }
}
