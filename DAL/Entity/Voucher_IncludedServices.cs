namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Voucher_IncludedServices
    {
        public int Id { get; set; }

        public int VoucherId { get; set; }

        [Required]
        public string Service { get; set; }

        public virtual Voucher Voucher { get; set; }
    }
}
