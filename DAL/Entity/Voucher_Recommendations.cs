namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Voucher_Recommendations
    {
        public int Id { get; set; }

        public int VoucherId { get; set; }

        [Required]
        public string Recommendation { get; set; }

        public virtual Voucher Voucher { get; set; }
    }
}
