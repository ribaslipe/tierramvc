namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cambio")]
    public partial class Cambio
    {
        [Key]
        public int Cambio_id { get; set; }

        public int? MoedaID_De { get; set; }

        public int? MoedaID_Para { get; set; }

        [Column("Cambio")]
        public decimal? Cambio1 { get; set; }

        [Column(TypeName = "date")]
        public DateTime? data { get; set; }

        public int? tipoCambio { get; set; }

        public virtual Moeda Moeda { get; set; }

        public virtual Moeda Moeda1 { get; set; }
    }
}
