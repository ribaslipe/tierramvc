namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Servicos_Imagem
    {
        [Key]
        public int Serv_img_id { get; set; }

        [Required]
        [StringLength(50)]
        public string Serv_img_nome { get; set; }

        [StringLength(50)]
        public string Serv_img_capa { get; set; }

        [StringLength(1000)]
        public string Serv_img_memo { get; set; }

        [StringLength(50)]
        public string Serv_img_hupload { get; set; }

        [StringLength(1000)]
        public string Serv_img_descr { get; set; }

        public int Servicos_Id { get; set; }

        public virtual S_Servicos S_Servicos { get; set; }
    }
}
