namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class File_ServExtraDescritivo
    {
        [Key]
        public int FileServExtra_Descritivo_ID { get; set; }

        [Required]
        public string FileServExtra_Descritivo { get; set; }

        public int FileServExtra_ID { get; set; }

        public virtual File_ServExtra File_ServExtra { get; set; }
    }
}
