namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Transf_Bancaria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Transf_Bancaria()
        {
            Pagamento_Manual = new HashSet<Pagamento_Manual>();
        }

        [Key]
        public int TrfContas_id { get; set; }

        public int? F_Conta_Cred_id { get; set; }

        public int? F_Conta_Deb_id { get; set; }

        public int? CentroCusto_id { get; set; }

        public int? ClassContab_id { get; set; }

        public DateTime? TrfContas_Data { get; set; }

        public decimal? TrfContas_Valor { get; set; }

        public decimal? TrfContas_Cambio { get; set; }

        public string TrfContas_Historico { get; set; }

        public virtual C_CentroCusto C_CentroCusto { get; set; }

        public virtual C_ClassificacaoContabil C_ClassificacaoContabil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pagamento_Manual> Pagamento_Manual { get; set; }
    }
}
