namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Descricao
    {
        [Key]
        public int Desc_id { get; set; }

        public string Desc_short { get; set; }

        public string Desc_long { get; set; }

        public string Desc_remarks { get; set; }

        public string Desc_cliente { get; set; }

        public string Desc_voucher { get; set; }

        public int? Desc_nRooms { get; set; }

        [StringLength(50)]
        public string Desc_HoraCheckIn { get; set; }

        [StringLength(50)]
        public string Desc_HoraCheckOut { get; set; }

        public string Desc_LnkAdvisor { get; set; }

        public int S_id { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
