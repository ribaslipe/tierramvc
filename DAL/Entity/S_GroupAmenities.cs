namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_GroupAmenities
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_GroupAmenities()
        {
            S_Amenities = new HashSet<S_Amenities>();
        }

        [Key]
        public int S_GroupAmenities_ID { get; set; }

        [Required]
        [StringLength(30)]
        public string S_GroupAmenities_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Amenities> S_Amenities { get; set; }
    }
}
