namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DescritivoOptions_Pacote
    {
        [Key]
        public int DescritivoOptions_id { get; set; }

        public int? DescritivoOptions_number { get; set; }

        [StringLength(50)]
        public string DescritivoOptions_breakfast { get; set; }

        [StringLength(50)]
        public string DescritivoOptions_lunch { get; set; }

        [StringLength(50)]
        public string DescritivoOptions_boxlunch { get; set; }

        [StringLength(50)]
        public string DescritivoOptions_dinner { get; set; }

        public int? Descritivo_id { get; set; }

        public virtual Descritivo_Pacote Pais { get; set; }

    }
}
