namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Mercado_Estacao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Mercado_Estacao()
        {
            S_Mercado_Periodo = new HashSet<S_Mercado_Periodo>();
        }

        [Key]
        public int S_mercado_estacao_id { get; set; }

        [StringLength(50)]
        public string S_mercado_estacao_nome { get; set; }

        public bool? S_mercado_estacao_pacote { get; set; }

        public bool? S_mercado_estacao_seasonal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo> S_Mercado_Periodo { get; set; }
    }
}
