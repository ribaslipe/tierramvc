namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Quotation_Grupo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Quotation_Grupo()
        {
            Cambio_File = new HashSet<Cambio_File>();
            Invoice = new HashSet<Invoice>();
            Invoice_Opcional = new HashSet<Invoice_Opcional>();
            MarkupFile = new HashSet<MarkupFile>();
            Quotation_Grupo_Qtd_Adult = new HashSet<Quotation_Grupo_Qtd_Adult>();
            Quotation_Grupo_Qtd_Baby = new HashSet<Quotation_Grupo_Qtd_Baby>();
            Quotation_Grupo_Qtd_Chd = new HashSet<Quotation_Grupo_Qtd_Chd>();
            Quotation_TourMap = new HashSet<Quotation_TourMap>();
            Remark_File = new HashSet<Remark_File>();
            RoomingList = new HashSet<RoomingList>();
        }

        [Key]
        public int Quotation_Grupo_Id { get; set; }

        public int Quotation_Id { get; set; }

        public int Cliente_id { get; set; }

        public int S_mercado_tipo_tarifa_id { get; set; }

        public int US_id { get; set; }

        public int FLinguagem_id { get; set; }

        public int? Moeda_id { get; set; }

        public int? Quotation_status_id { get; set; }

        public int Quotation_Grupo_NGrupo { get; set; }

        [Required]
        [StringLength(100)]
        public string Pax_Group_Name { get; set; }

        [Column(TypeName = "date")]
        public DateTime dataQuote { get; set; }

        [Column(TypeName = "date")]
        public DateTime dataIN { get; set; }

        [Column(TypeName = "date")]
        public DateTime dateOut { get; set; }

        [StringLength(50)]
        public string tourCode { get; set; }

        public bool? cancelAll { get; set; }

        [StringLength(200)]
        public string surname { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cambio_File> Cambio_File { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual File_Liguagem File_Liguagem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoice { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_Opcional> Invoice_Opcional { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MarkupFile> MarkupFile { get; set; }

        public virtual Moeda Moeda { get; set; }

        public virtual Quotation Quotation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_Grupo_Qtd_Adult> Quotation_Grupo_Qtd_Adult { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_Grupo_Qtd_Baby> Quotation_Grupo_Qtd_Baby { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_Grupo_Qtd_Chd> Quotation_Grupo_Qtd_Chd { get; set; }

        public virtual Quotation_Status Quotation_Status { get; set; }

        public virtual S_Mercado_Tipo_Tarifa S_Mercado_Tipo_Tarifa { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_TourMap> Quotation_TourMap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Remark_File> Remark_File { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RoomingList> RoomingList { get; set; }
    }
}
