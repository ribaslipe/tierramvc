namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class F_Conta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public F_Conta()
        {
            Deposito = new HashSet<Deposito>();
            Fatura_Supplier_Pagamento = new HashSet<Fatura_Supplier_Pagamento>();
            Invoice_Pagamento = new HashSet<Invoice_Pagamento>();
            Invoice_PagamentoOpcionais = new HashSet<Invoice_PagamentoOpcionais>();
            Pagamento_Manual = new HashSet<Pagamento_Manual>();
        }

        public int id { get; set; }

        [StringLength(50)]
        public string Numero { get; set; }

        [StringLength(50)]
        public string Nome { get; set; }

        public int? id_Agencia { get; set; }

        public int? id_Moeda { get; set; }

        public decimal? saldo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Deposito> Deposito { get; set; }

        public virtual F_Agencia F_Agencia { get; set; }

        public virtual Moeda Moeda { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Fatura_Supplier_Pagamento> Fatura_Supplier_Pagamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_Pagamento> Invoice_Pagamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_PagamentoOpcionais> Invoice_PagamentoOpcionais { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pagamento_Manual> Pagamento_Manual { get; set; }
    }
}
