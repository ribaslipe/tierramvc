namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Holidays
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Holidays()
        {
            Holidays_Dates = new HashSet<Holidays_Dates>();
            Holidays_Periodo = new HashSet<Holidays_Periodo>();
        }

        [Key]
        public int Holidays_id { get; set; }

        public int? Servicos_Id { get; set; }

        public bool? Holidays_seg { get; set; }

        public bool? Holidays_ter { get; set; }

        public bool? Holidays_qua { get; set; }

        public bool? Holidays_qui { get; set; }

        public bool? Holidays_sex { get; set; }

        public bool? Holidays_sab { get; set; }

        public bool? Holidays_dom { get; set; }

        [StringLength(200)]
        public string Holidays_remarks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Holidays_Dates> Holidays_Dates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Holidays_Periodo> Holidays_Periodo { get; set; }

        public virtual S_Servicos S_Servicos { get; set; }
    }
}
