namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Moeda_XML
    {
        public int id { get; set; }

        public int Moeda_id_Plataforma { get; set; }

        public int Moeda_id_Prosoftware { get; set; }

        public int id_Plataforma { get; set; }

        [Required]
        [StringLength(50)]
        public string Descricao { get; set; }

        public virtual Plataforma_XML Plataforma_XML { get; set; }
    }
}
