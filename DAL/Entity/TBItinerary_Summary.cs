namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TBItinerary_Summary
    {
        [Key]
        public int TBItinerary_summary_id { get; set; }

        [StringLength(50)]
        public string Quotation_Code { get; set; }

        public int? OptQuote { get; set; }

        public int? TBItinerary_summary_day { get; set; }

        public DateTime? TBItinerary_summary_date { get; set; }

        [StringLength(500)]
        public string TBItinerary_summary_service { get; set; }

        [StringLength(500)]
        public string TBItinerary_summary_hotel { get; set; }

        [StringLength(500)]
        public string TBItinerary_summary_meal { get; set; }

        [StringLength(50)]
        public string Flag { get; set; }

        [StringLength(50)]
        public string Categoria { get; set; }

        [StringLength(50)]
        public string Room { get; set; }

        [StringLength(50)]
        public string Cidade { get; set; }
    }
}
