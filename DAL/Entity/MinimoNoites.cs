namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MinimoNoites
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MinimoNoites()
        {
            S_Mercado_Periodo = new HashSet<S_Mercado_Periodo>();
        }

        [Key]
        public int MinimoNoites_id { get; set; }

        public int MinimoNoites_qtd { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo> S_Mercado_Periodo { get; set; }
    }
}
