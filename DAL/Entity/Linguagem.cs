namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Linguagem")]
    public partial class Linguagem
    {
        [Key]
        public int Linguagem_id { get; set; }

        [StringLength(50)]
        public string Linguagem_nome { get; set; }
    }
}
