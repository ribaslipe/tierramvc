namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Ordem_Pagamento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ordem_Pagamento()
        {
            Ordem_Pagamento_Itens = new HashSet<Ordem_Pagamento_Itens>();
        }

        [Key]
        public int OrdemPagamento_ID { get; set; }

        public int S_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime Data { get; set; }

        public int File_id { get; set; }

        public virtual File_Carrinho File_Carrinho { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ordem_Pagamento_Itens> Ordem_Pagamento_Itens { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
