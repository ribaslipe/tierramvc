namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Remark_File
    {
        [Key]
        public int remarkfile_id { get; set; }

        public string remarkfile_text { get; set; }

        public int? Quotation_Grupo_Id { get; set; }

        public virtual Quotation_Grupo Quotation_Grupo { get; set; }
    }
}
