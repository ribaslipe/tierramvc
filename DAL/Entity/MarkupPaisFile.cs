namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MarkupPaisFile")]
    public partial class MarkupPaisFile
    {
        [Key]
        public int MarkupFilePais_id { get; set; }

        public int? Pais_id { get; set; }

        public decimal? Markup { get; set; }

        public decimal? MarkupServ { get; set; }

        public decimal? Desconto { get; set; }

        public decimal? MarkupLodge { get; set; }

        public int? Quotation_Grupo_Id { get; set; }
    }
}
