namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Mercado_Tarifa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Mercado_Tarifa()
        {
            S_PoliticaCHDTarifas = new HashSet<S_PoliticaCHDTarifas>();
        }

        [Key]
        public int S_merc_tarif_id { get; set; }

        public decimal? S_merc_tarif_tarifa { get; set; }

        public int? S_merc_tarif_status_id { get; set; }

        public int Tipo_room_id { get; set; }

        public int Tarif_categoria_id { get; set; }

        public int S_mercado_tipo_tarifa_id { get; set; }

        public int S_merc_periodo_id { get; set; }

        public int? S_politicaCHD_id { get; set; }

        public decimal? S_merc_tarif_tarifa_total { get; set; }

        public decimal? S_merc_tarif_porc_taxa { get; set; }

        public decimal? S_merca_tarif_porc_imposto { get; set; }

        public decimal? S_merca_tarif_porc_comissao { get; set; }

        public bool? S_merc_tarif_porPessoa { get; set; }

        public bool? S_merc_tarif_porApartamento { get; set; }

        public int? S_merc_tarif_ordem { get; set; }

        public int? S_politicaTC_id { get; set; }

        public virtual S_Mercado_Periodo S_Mercado_Periodo { get; set; }

        public virtual S_Mercado_Tarifa_Status S_Mercado_Tarifa_Status { get; set; }

        public virtual S_Mercado_Tipo_Tarifa S_Mercado_Tipo_Tarifa { get; set; }

        public virtual Tarif_Categoria Tarif_Categoria { get; set; }

        public virtual Tipo_Room Tipo_Room { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_PoliticaCHDTarifas> S_PoliticaCHDTarifas { get; set; }
    }
}
