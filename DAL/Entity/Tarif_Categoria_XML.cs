namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tarif_Categoria_XML
    {
        public int Id { get; set; }

        public int? S_XMLId { get; set; }

        public int? Id_TipoRoom { get; set; }

        public int? Id_TarifCategoria { get; set; }

        public int Plataforma_Id { get; set; }

        public int Plataforma_CatId { get; set; }

        [Required]
        [StringLength(100)]
        public string Descricao { get; set; }

        public int? MaxOccupancy { get; set; }

        public int? MinOccupancy { get; set; }

        [StringLength(100)]
        public string AgeQualifyCode { get; set; }

        public string Description { get; set; }

        public virtual Plataforma_XML Plataforma_XML { get; set; }

        public virtual Supplier_XML Supplier_XML { get; set; }
    }
}
