namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cliente")]
    public partial class Cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cliente()
        {
            Cli_Contato = new HashSet<Cli_Contato>();
            Cliente_Markup = new HashSet<Cliente_Markup>();
            Deposito = new HashSet<Deposito>();
            Invoice_Troca = new HashSet<Invoice_Troca>();
            Quotation_Grupo = new HashSet<Quotation_Grupo>();
        }

        [Key]
        public int Cliente_id { get; set; }

        [StringLength(50)]
        public string Cliente_nome { get; set; }

        [StringLength(200)]
        public string Cliente_endereco { get; set; }

        [StringLength(50)]
        public string Cliente_email { get; set; }

        [StringLength(100)]
        public string Cliente_http { get; set; }

        [StringLength(12)]
        public string Cliente_cep { get; set; }

        [StringLength(20)]
        public string Cliente_telefone { get; set; }

        [StringLength(20)]
        public string Cliente_fax { get; set; }

        public DateTime? Cliente_since { get; set; }

        public int? Representante_id { get; set; }

        public int? Mercado_id { get; set; }

        public int? BaseTarifaria_id { get; set; }

        public int? CentroCusto_id { get; set; }

        public int? PAIS_id { get; set; }

        public int? CID_id { get; set; }

        public string Cliente_senha { get; set; }

        public int? US_id { get; set; }

        public virtual BaseTarifaria BaseTarifaria { get; set; }

        public virtual Centro_Custo Centro_Custo { get; set; }

        public virtual Cidade Cidade { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cli_Contato> Cli_Contato { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cliente_Markup> Cliente_Markup { get; set; }

        public virtual Mercado Mercado { get; set; }

        public virtual Pais Pais { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Deposito> Deposito { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_Troca> Invoice_Troca { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_Grupo> Quotation_Grupo { get; set; }
    }
}
