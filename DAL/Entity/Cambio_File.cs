namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cambio_File
    {
        [Key]
        public int CambioFile_id { get; set; }

        public decimal? CambioFile { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CambioFile_dataFrom { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CambioFile_dataTo { get; set; }

        public int? Quotation_Grupo_Id { get; set; }

        public virtual Quotation_Grupo Quotation_Grupo { get; set; }
    }
}
