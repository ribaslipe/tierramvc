namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BaseTarifaria")]
    public partial class BaseTarifaria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BaseTarifaria()
        {
            Cliente = new HashSet<Cliente>();
            S_Bases = new HashSet<S_Bases>();
            S_Mercado_Periodo = new HashSet<S_Mercado_Periodo>();
            S_Mercado_Periodo_Black = new HashSet<S_Mercado_Periodo_Black>();
        }

        [Key]
        public int BaseTarifaria_id { get; set; }

        [StringLength(50)]
        public string BaseTarifaria_nome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cliente> Cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Bases> S_Bases { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo> S_Mercado_Periodo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo_Black> S_Mercado_Periodo_Black { get; set; }
    }
}
