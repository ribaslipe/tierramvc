namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Holidays_Dates
    {
        [Key]
        public int Holidays_Dates_id { get; set; }

        public int? Holidays_id { get; set; }

        public DateTime? Holidays_Dates_data { get; set; }

        public virtual Holidays Holidays { get; set; }
    }
}
