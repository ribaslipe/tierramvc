namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Mercado_Tarifa_Status
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Mercado_Tarifa_Status()
        {
            Monta_Servico_Valores = new HashSet<Monta_Servico_Valores>();
            S_Mercado_Meal = new HashSet<S_Mercado_Meal>();
            S_Mercado_Tarifa = new HashSet<S_Mercado_Tarifa>();
        }

        [Key]
        public int S_mercado_tarifa_status_id { get; set; }

        [StringLength(50)]
        public string S_mercado_tarifa_status_nome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Monta_Servico_Valores> Monta_Servico_Valores { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Meal> S_Mercado_Meal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Tarifa> S_Mercado_Tarifa { get; set; }
    }
}
