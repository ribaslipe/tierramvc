namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TBItinerary_Prices
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string QuotationCode { get; set; }

        [Required]
        [StringLength(50)]
        public string PrecoAlterado { get; set; }

        [Required]
        public string Preco_Notes { get; set; }

        [Required]
        [StringLength(20)]
        public string Base { get; set; }

        [StringLength(50)]
        public string TipoRoom { get; set; }

        public int optQuote { get; set; }

        public string Preco_Link { get; set; }

        public bool? visible { get; set; }
    }
}
