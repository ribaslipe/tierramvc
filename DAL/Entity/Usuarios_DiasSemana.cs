namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios_DiasSemana
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuarios_DiasSemana()
        {
            Usuarios_Horarios = new HashSet<Usuarios_Horarios>();
        }

        [Key]
        public int DiasSemana_id { get; set; }

        public bool? DiasSemana_seg { get; set; }

        public bool? DiasSemana_ter { get; set; }

        public bool? DiasSemana_qua { get; set; }

        public bool? DiasSemana_qui { get; set; }

        public bool? DiasSemana_sex { get; set; }

        public bool? DiasSemana_sab { get; set; }

        public bool? DiasSemana_dom { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuarios_Horarios> Usuarios_Horarios { get; set; }
    }
}
