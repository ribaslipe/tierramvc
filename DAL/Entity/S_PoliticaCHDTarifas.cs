namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_PoliticaCHDTarifas
    {
        [Key]
        public int S_PoliticaCHDTarifas_id { get; set; }

        public int? S_politicaCHD_id { get; set; }

        public int? S_merc_tarif_id { get; set; }

        public virtual S_Mercado_Tarifa S_Mercado_Tarifa { get; set; }

        public virtual S_Politica_CHD S_Politica_CHD { get; set; }
    }
}
