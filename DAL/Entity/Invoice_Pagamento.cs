namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Invoice_Pagamento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Invoice_Pagamento()
        {
            Invoice_Troca = new HashSet<Invoice_Troca>();
        }

        [Key]
        public int Invoice_Pagamento_id { get; set; }

        public decimal? Valor { get; set; }

        [Column(TypeName = "date")]
        public DateTime? data { get; set; }

        public decimal? cambio { get; set; }

        public int? F_Conta_id { get; set; }

        public int? Moeda_id { get; set; }

        public string obs { get; set; }

        public int? Invoice_id { get; set; }

        public int? idPagManual { get; set; }

        public int? Deposito_id { get; set; }

        public virtual Deposito Deposito { get; set; }

        public virtual F_Conta F_Conta { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual Moeda Moeda { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_Troca> Invoice_Troca { get; set; }
    }
}
