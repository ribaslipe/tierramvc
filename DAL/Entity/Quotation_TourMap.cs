namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Quotation_TourMap
    {
        [Key]
        public int tourMap_Id { get; set; }

        public int tourMap_QuotationGroup_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string tourMap_Path { get; set; }

        public virtual Quotation_Grupo Quotation_Grupo { get; set; }
    }
}
