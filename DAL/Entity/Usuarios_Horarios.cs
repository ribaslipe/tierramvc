namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios_Horarios
    {
        [Key]
        public int Horarios_id { get; set; }

        [StringLength(20)]
        public string Horarios_diaInicio { get; set; }

        [StringLength(20)]
        public string Horarios_diaFim { get; set; }

        public int? DiasSemana_id { get; set; }

        public DateTime? Horarios_feriasInicio { get; set; }

        public DateTime? Horarios_feriasFim { get; set; }

        public int US_id { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        public virtual Usuarios_DiasSemana Usuarios_DiasSemana { get; set; }
    }
}
