namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Politica_CHD
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Politica_CHD()
        {
            S_PoliticaCHDTarifas = new HashSet<S_PoliticaCHDTarifas>();
        }

        [Key]
        public int S_politicaCHD_id { get; set; }

        public int? S_politicaCHD_qtd { get; set; }

        public int? S_politicaCHD_idadeDe { get; set; }

        public int? S_politicaCHD_idade { get; set; }

        [StringLength(20)]
        public string S_politicaCHD_free { get; set; }

        public decimal? S_politicaCHD_valor { get; set; }

        public decimal? S_politicaCHD_percent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_PoliticaCHDTarifas> S_PoliticaCHDTarifas { get; set; }
    }
}
