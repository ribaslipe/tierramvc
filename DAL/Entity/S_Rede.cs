namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Rede
    {
        [Key]
        public int Rede_id { get; set; }

        [StringLength(50)]
        public string Rede_nome { get; set; }

        [StringLength(50)]
        public string Rede_endereco { get; set; }

        [StringLength(50)]
        public string Rede_cnpj { get; set; }

        [StringLength(50)]
        public string Rede_telefone { get; set; }

        [StringLength(50)]
        public string Rede_email { get; set; }
    }
}
