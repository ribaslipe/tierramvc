namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Quote_Tarifas
    {
        [Key]
        public int Quote_Tarifas_id { get; set; }

        public int? File_id { get; set; }

        public int? Flights_id { get; set; }

        public int? Paxs { get; set; }

        public int? Paying_Pax { get; set; }

        public int? Supp_Paying_Pax { get; set; }

        public DateTime? Data_From { get; set; }

        public DateTime? Data_To { get; set; }

        public decimal? markup { get; set; }

        public decimal? desconto { get; set; }

        public decimal? markupNet { get; set; }

        public decimal? descontoNet { get; set; }

        public decimal? S_merc_tarif_vendaNet { get; set; }

        public int? NumNoites { get; set; }

        public int? Qtd_Tarifas { get; set; }

        public decimal? S_merc_tarif_valor { get; set; }

        public decimal? S_merc_tarif_venda { get; set; }

        public decimal? S_merc_tarif_total { get; set; }

        [StringLength(200)]
        public string S_nome { get; set; }

        [StringLength(50)]
        public string S_email { get; set; }

        [StringLength(200)]
        public string S_meal_nome { get; set; }

        [StringLength(50)]
        public string S_meal_status { get; set; }

        public bool? Meal { get; set; }

        public int? S_meal_id { get; set; }

        public int? S_id { get; set; }

        [StringLength(500)]
        public string Categoria { get; set; }

        public int? Categoria_id { get; set; }

        [StringLength(50)]
        public string Room { get; set; }

        public int? Room_id { get; set; }

        [StringLength(50)]
        public string Moeda { get; set; }

        public int? Moeda_id { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string Flag { get; set; }

        public bool? FTC { get; set; }

        public decimal? Valor_Conferir { get; set; }

        public decimal? Cambio { get; set; }

        public bool? Cancelado { get; set; }

        public int? Ordem { get; set; }

        [StringLength(100)]
        public string NomePacote { get; set; }

        public bool? UpdVenda { get; set; }

        public int? Range_id { get; set; }

        public bool? GeraFile { get; set; }

        [StringLength(20)]
        public string FdsFrom { get; set; }

        [StringLength(20)]
        public string FdsTo { get; set; }

        public bool? TBulk { get; set; }

        public bool? TC { get; set; }

        public bool? FileWeb { get; set; }

        public int? Plataforma_id { get; set; }

        public bool? Provisorio { get; set; }
    }
}
