namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Ordem_Pagamento_Itens
    {
        [Key]
        public int OrdemPagamento_Itens_ID { get; set; }

        public int OrdemPagamento_ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime Data_From { get; set; }

        [Column(TypeName = "date")]
        public DateTime Data_To { get; set; }

        public int FileTabela_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string Flag { get; set; }

        public virtual Ordem_Pagamento Ordem_Pagamento { get; set; }
    }
}
