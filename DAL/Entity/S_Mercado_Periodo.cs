namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Mercado_Periodo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Mercado_Periodo()
        {
            S_Mercado_Meal = new HashSet<S_Mercado_Meal>();
            S_Mercado_Periodo_Fds = new HashSet<S_Mercado_Periodo_Fds>();
            S_Mercado_Tarifa = new HashSet<S_Mercado_Tarifa>();
        }

        [Key]
        public int S_merc_periodo_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? S_merc_periodo_from { get; set; }

        [Column(TypeName = "date")]
        public DateTime? S_merc_periodo_to { get; set; }

        [StringLength(100)]
        public string Pacote_nome { get; set; }

        [StringLength(10)]
        public string S_merc_periodo_fdsFrom { get; set; }

        [StringLength(10)]
        public string S_merc_periodo_fdsTo { get; set; }

        [StringLength(100)]
        public string S_merc_remarks { get; set; }

        public int S_id { get; set; }

        public int S_mercado_estacao_id { get; set; }

        public int Mercado_id { get; set; }

        public int? Moeda_id { get; set; }

        public int? MinimoNoites_id { get; set; }

        public bool? S_merc_periodo_minimo { get; set; }

        public bool? S_merc_periodo_mandatorio { get; set; }

        public int? BaseTarifaria_id { get; set; }

        public int? Promocao_id { get; set; }

        public int? Responsavel_id { get; set; }

        public bool? Provisorio { get; set; }

        public virtual BaseTarifaria BaseTarifaria { get; set; }

        public virtual Mercado Mercado { get; set; }

        public virtual MinimoNoites MinimoNoites { get; set; }

        public virtual Moeda Moeda { get; set; }

        public virtual Promocao Promocao { get; set; }

        public virtual S_Mercado_Estacao S_Mercado_Estacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Meal> S_Mercado_Meal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Periodo_Fds> S_Mercado_Periodo_Fds { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual Supplier Supplier1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Tarifa> S_Mercado_Tarifa { get; set; }
    }
}
