namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Cond_Pagamento
    {
        [Key]
        public int CondPag_id { get; set; }

        [StringLength(100)]
        public string CondPag_fontecambio { get; set; }

        [StringLength(50)]
        public string CondPag_tipocambio { get; set; }

        [StringLength(50)]
        public string CondPag_datatroca { get; set; }

        public int? CondPag_prazofaturamento { get; set; }

        [StringLength(50)]
        public string CondPag_banco { get; set; }

        [StringLength(50)]
        public string CondPag_agencia { get; set; }

        [StringLength(50)]
        public string CondPag_conta { get; set; }

        public int? S_id { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
