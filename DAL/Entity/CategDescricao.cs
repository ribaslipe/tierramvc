namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CategDescricao")]
    public partial class CategDescricao
    {
        [Key]
        public int CategDesc_id { get; set; }

        public int? S_id { get; set; }

        public int? Tarif_categoria_id { get; set; }

        public string CategDesc { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual Tarif_Categoria Tarif_Categoria { get; set; }
    }
}
