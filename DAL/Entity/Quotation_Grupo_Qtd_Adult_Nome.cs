namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Quotation_Grupo_Qtd_Adult_Nome
    {
        [Key]
        public int Qtd_Adult_Nome_id { get; set; }

        public int Qtd_Adult_id { get; set; }

        [StringLength(10)]
        public string Tratamento { get; set; }

        [StringLength(100)]
        public string Nome { get; set; }

        [StringLength(100)]
        public string Sobrenome { get; set; }

        public int? Idade { get; set; }

        public bool? Principal_Indicador { get; set; }

        public bool? voucher { get; set; }
        
        public virtual Quotation_Grupo_Qtd_Adult Quotation_Grupo_Qtd_Adult { get; set; }
    }
}
