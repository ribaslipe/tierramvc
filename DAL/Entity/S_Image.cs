namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Image
    {
        [Key]
        public int Image_id { get; set; }

        [Required]
        [StringLength(50)]
        public string Image_nome { get; set; }

        [StringLength(50)]
        public string Image_capa { get; set; }

        [StringLength(1000)]
        public string Image_memo { get; set; }

        [StringLength(50)]
        public string Image_hupload { get; set; }

        [StringLength(1000)]
        public string Image_descr { get; set; }

        public int S_id { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
