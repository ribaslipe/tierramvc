namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Holidays_Periodo
    {
        [Key]
        public int HoliDays_Periodo_id { get; set; }

        public int? Holidays_id { get; set; }

        public DateTime? HoliDays_Periodo_from { get; set; }

        public DateTime? HoliDays_Periodo_to { get; set; }

        public virtual Holidays Holidays { get; set; }
    }
}
