namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LogErro")]
    public partial class LogErro
    {
        [Key]
        public int LogErro_id { get; set; }

        [StringLength(500)]
        public string LogErro_pagina { get; set; }

        [StringLength(50)]
        public string LogErro_linha { get; set; }

        [StringLength(50)]
        public string LogErro_coluna { get; set; }

        [StringLength(500)]
        public string LogErro_erro { get; set; }

        public DateTime? LogErro_dataInicial { get; set; }

        public DateTime? LogErro_dataFinal { get; set; }
    }
}
