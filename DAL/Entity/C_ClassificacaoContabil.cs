namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C_ClassificacaoContabil
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public C_ClassificacaoContabil()
        {
            FaturaSupplier = new HashSet<FaturaSupplier>();
            Invoice_Troca = new HashSet<Invoice_Troca>();
            Pagamento_Manual = new HashSet<Pagamento_Manual>();
            Transf_Bancaria = new HashSet<Transf_Bancaria>();
        }

        [Key]
        public int ClassificacaoContabil_id { get; set; }

        [StringLength(50)]
        public string ClassificacaoContabil_nome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FaturaSupplier> FaturaSupplier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_Troca> Invoice_Troca { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pagamento_Manual> Pagamento_Manual { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Transf_Bancaria> Transf_Bancaria { get; set; }
    }
}
