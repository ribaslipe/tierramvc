namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PlataformaReserva_XML
    {
        [Key]
        public int PlataformaReserva_id { get; set; }

        public int? Plataforma_id { get; set; }

        [StringLength(100)]
        public string ReservaID { get; set; }

        [StringLength(100)]
        public string ReservaINDEX { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public string DescricaoReserva { get; set; }

        public string DescricaoCancel { get; set; }

        public string DescricaoPag { get; set; }

        public bool? BreackFast { get; set; }

        public int? Quote_Tarifas_id { get; set; }

        public int? File_Tarifas_id { get; set; }

        public bool? CancelAll { get; set; }

        public int? grp_number { get; set; }

        public string nomesreserva { get; set; }

    }
}
