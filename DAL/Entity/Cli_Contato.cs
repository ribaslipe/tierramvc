namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cli_Contato
    {
        [Key]
        public int Contato_id { get; set; }

        [StringLength(50)]
        public string Contato_nome { get; set; }

        [StringLength(50)]
        public string Contato_position { get; set; }

        [StringLength(20)]
        public string Contato_telefone { get; set; }

        [StringLength(20)]
        public string Contato_fax { get; set; }

        [StringLength(50)]
        public string Contato_email { get; set; }

        public DateTime? Contato_dataNiver { get; set; }

        public bool? Contato_mailing { get; set; }

        public int Cliente_id { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
