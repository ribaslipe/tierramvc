namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("categoriasPlat")]
    public partial class categoriasPlat
    {
        [Key]
        public int categoriasPlat_id { get; set; }

        [StringLength(500)]
        public string categoriasPlat_code { get; set; }

        [StringLength(500)]
        public string categoriasPlat_value { get; set; }

        public int? CIDSistema_id { get; set; }

        public int? Plataforma_id { get; set; }

        public virtual Cidade Cidade { get; set; }

        public virtual Plataforma_XML Plataforma_XML { get; set; }
    }
}
