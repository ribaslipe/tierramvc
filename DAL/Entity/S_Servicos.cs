namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Servicos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Servicos()
        {
            Holidays = new HashSet<Holidays>();
            Servico_Completo = new HashSet<Servico_Completo>();
            Servicos_Imagem = new HashSet<Servicos_Imagem>();
        }

        [Key]
        public int Servicos_Id { get; set; }

        [Required]
        [StringLength(500)]
        public string Servicos_Nome { get; set; }

        public string Servicos_descr { get; set; }

        public string Servicos_descrCurt { get; set; }

        public string Servicos_descrRemarks { get; set; }

        public string Servicos_descrCliente { get; set; }

        public string Servicos_descrVoucher { get; set; }

        [StringLength(20)]
        public string Servicos_transfer { get; set; }

        public int STATUS_id { get; set; }

        public int? Tipo_Id { get; set; }

        public int? Tipo_categ_id { get; set; }

        public int Cid_Id { get; set; }

        public int Pais_Id { get; set; }

        public bool? SIB { get; set; }

        public bool? Recommended { get; set; }

        public bool? Tarifario { get; set; }

        public virtual Cidade Cidade { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Holidays> Holidays { get; set; }

        public virtual Pais Pais { get; set; }

        public virtual S_Servicos_Tipo S_Servicos_Tipo { get; set; }

        public virtual S_Servicos_Tipo_Categ S_Servicos_Tipo_Categ { get; set; }

        public virtual S_Status S_Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Servico_Completo> Servico_Completo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Servicos_Imagem> Servicos_Imagem { get; set; }
    }
}
