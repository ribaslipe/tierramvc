namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Pacote_Descricao
    {
        [Key]
        public int Pacote_descr_id { get; set; }

        public string Pacote_descr { get; set; }

        [StringLength(20)]
        public string Pacote_Code { get; set; }
    }
}
