namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Servicos_Tipo_Categ
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Servicos_Tipo_Categ()
        {
            Monta_Servico = new HashSet<Monta_Servico>();
            S_Servicos = new HashSet<S_Servicos>();
        }

        [Key]
        public int Tipo_categ_id { get; set; }

        [StringLength(100)]
        public string Tipo_categ_nome { get; set; }

        public int? Tipo_Id { get; set; }

        public int? Ordenacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Monta_Servico> Monta_Servico { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Servicos> S_Servicos { get; set; }

        public virtual S_Servicos_Tipo S_Servicos_Tipo { get; set; }
    }
}
