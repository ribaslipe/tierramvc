namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuarios()
        {
            Fatura_Supplier_Pagamento = new HashSet<Fatura_Supplier_Pagamento>();
            Pagamento_Manual = new HashSet<Pagamento_Manual>();
            Quotation_Grupo = new HashSet<Quotation_Grupo>();
            Usuarios_Horarios = new HashSet<Usuarios_Horarios>();
        }

        [Key]
        public int US_id { get; set; }

        [StringLength(50)]
        public string US_nome { get; set; }

        [StringLength(50)]
        public string US_senha { get; set; }

        [StringLength(50)]
        public string US_email { get; set; }

        public bool? US_status { get; set; }

        public int? Perfil_id { get; set; }

        public int? Cliente_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Fatura_Supplier_Pagamento> Fatura_Supplier_Pagamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pagamento_Manual> Pagamento_Manual { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_Grupo> Quotation_Grupo { get; set; }

        public virtual Usuario_Perfil Usuario_Perfil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuarios_Horarios> Usuarios_Horarios { get; set; }
    }
}
