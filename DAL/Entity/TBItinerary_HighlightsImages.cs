namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TBItinerary_HighlightsImages
    {
        public int Id { get; set; }

        [StringLength(200)]
        public string FirstImage { get; set; }

        [StringLength(200)]
        public string LastImage { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        public int optQuote { get; set; }

        
    }
}
