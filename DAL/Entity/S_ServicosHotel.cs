namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_ServicosHotel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_ServicosHotel()
        {
            S_InformationHotel = new HashSet<S_InformationHotel>();
        }

        [Key]
        public int S_ServicosHotel_id { get; set; }

        [StringLength(50)]
        public string S_ServicosHotel_nome { get; set; }

        [StringLength(100)]
        public string S_ServicosHotel_imgNome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_InformationHotel> S_InformationHotel { get; set; }
    }
}
