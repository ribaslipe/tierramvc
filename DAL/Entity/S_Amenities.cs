namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Amenities
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Amenities()
        {
            S_SupplierAmenities = new HashSet<S_SupplierAmenities>();
        }

        [Key]
        public int S_Amenities_id { get; set; }

        [StringLength(50)]
        public string S_Amenities_nome { get; set; }

        [StringLength(100)]
        public string S_Amenities_imgNome { get; set; }

        public int? S_GroupAmenities_ID { get; set; }

        public virtual S_GroupAmenities S_GroupAmenities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_SupplierAmenities> S_SupplierAmenities { get; set; }
    }
}
