namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Invoice_Opcional
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Invoice_Opcional()
        {
            Invoice_PagamentoOpcionais = new HashSet<Invoice_PagamentoOpcionais>();
        }

        [Key]
        public int Invoice_Opcional_id { get; set; }

        public int? Quotation_Grupo_Id { get; set; }

        public DateTime? DataEmissao { get; set; }

        public decimal? Grand_Total { get; set; }

        [StringLength(500)]
        public string CaminhoPDF { get; set; }

        [StringLength(100)]
        public string NomeTo { get; set; }

        [StringLength(100)]
        public string Pax_Group_Name { get; set; }

        [StringLength(50)]
        public string Numero_File { get; set; }

        [StringLength(10)]
        public string Currency { get; set; }

        [Column(TypeName = "date")]
        public DateTime? vencimento { get; set; }

        [StringLength(50)]
        public string FlagTabela { get; set; }

        public int? idTabela { get; set; }

        public virtual Quotation_Grupo Quotation_Grupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_PagamentoOpcionais> Invoice_PagamentoOpcionais { get; set; }
    }
}
