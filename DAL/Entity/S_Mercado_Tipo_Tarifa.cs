namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Mercado_Tipo_Tarifa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public S_Mercado_Tipo_Tarifa()
        {
            Quotation_Grupo = new HashSet<Quotation_Grupo>();
            S_Mercado_Tarifa = new HashSet<S_Mercado_Tarifa>();
        }

        [Key]
        public int S_mercado_tipo_tarifa_id { get; set; }

        [StringLength(50)]
        public string S_mercado_tipo_tarifa_nome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_Grupo> Quotation_Grupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Tarifa> S_Mercado_Tarifa { get; set; }
    }
}
