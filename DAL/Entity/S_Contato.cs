namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Contato
    {
        [Key]
        public int SCONT_id { get; set; }

        [StringLength(50)]
        public string SCONT_nome { get; set; }

        [StringLength(50)]
        public string SCONT_telefone { get; set; }

        [StringLength(50)]
        public string SCONT_position { get; set; }

        [StringLength(50)]
        public string SCONT_email { get; set; }

        public int? S_id { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
