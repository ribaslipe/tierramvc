namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Invoice")]
    public partial class Invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Invoice()
        {
            Invoice_Pagamento = new HashSet<Invoice_Pagamento>();
        }

        [Key]
        public int Invoice_id { get; set; }

        public int? Quotation_Grupo_Id { get; set; }

        public DateTime? DataEmissao { get; set; }

        public decimal? Grand_Total { get; set; }

        public string CaminhoPDF { get; set; }

        [StringLength(50)]
        public string NomeTo { get; set; }

        [StringLength(100)]
        public string Pax_Group_Name { get; set; }

        public int? Numero_File { get; set; }

        [StringLength(10)]
        public string Currency { get; set; }

        [Column(TypeName = "date")]
        public DateTime? vencimento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice_Pagamento> Invoice_Pagamento { get; set; }

        public virtual Quotation_Grupo Quotation_Grupo { get; set; }
    }
}
