namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Quotation_Grupo_Qtd_Adult
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Quotation_Grupo_Qtd_Adult()
        {
            Quotation_Grupo_Qtd_Adult_Nome = new HashSet<Quotation_Grupo_Qtd_Adult_Nome>();
        }

        [Key]
        public int Qtd_Adult_id { get; set; }

        public int Quotation_Grupo_Id { get; set; }

        public int Qtd { get; set; }

        public int? grp_number { get; set; }

        public virtual Quotation_Grupo Quotation_Grupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quotation_Grupo_Qtd_Adult_Nome> Quotation_Grupo_Qtd_Adult_Nome { get; set; }
    }
}
