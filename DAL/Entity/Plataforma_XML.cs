namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Plataforma_XML
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Plataforma_XML()
        {
            categoriasPlat = new HashSet<categoriasPlat>();
            Cidade_XML = new HashSet<Cidade_XML>();
            Moeda_XML = new HashSet<Moeda_XML>();
            Pais_XML = new HashSet<Pais_XML>();
            Supplier_XML = new HashSet<Supplier_XML>();
            Tarif_Categoria_XML = new HashSet<Tarif_Categoria_XML>();
        }

        [Key]
        public int Plataforma_id { get; set; }

        [StringLength(50)]
        public string Plataforma_nome { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<categoriasPlat> categoriasPlat { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cidade_XML> Cidade_XML { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Moeda_XML> Moeda_XML { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pais_XML> Pais_XML { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Supplier_XML> Supplier_XML { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tarif_Categoria_XML> Tarif_Categoria_XML { get; set; }
    }
}
