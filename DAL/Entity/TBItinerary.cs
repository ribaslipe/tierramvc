namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TBItinerary")]
    public partial class TBItinerary
    {
        [Key]
        public int Itinerary_id { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        public int? idTabela { get; set; }

        [StringLength(50)]
        public string Quotation_Code { get; set; }

        [StringLength(200)]
        public string SuppNome { get; set; }

        public int? S_id { get; set; }

        [StringLength(200)]
        public string ServNome { get; set; }

        [StringLength(200)]
        public string ServExtrNome { get; set; }

        [StringLength(200)]
        public string MealNome { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        [StringLength(200)]
        public string Cidade { get; set; }

        [StringLength(200)]
        public string Categoria { get; set; }

        [StringLength(200)]
        public string Room { get; set; }

        [StringLength(20)]
        public string Flag { get; set; }

        [StringLength(50)]
        public string Flight_voo { get; set; }

        [StringLength(50)]
        public string Flight_hora { get; set; }

        [StringLength(50)]
        public string Flight_pickUp { get; set; }

        public int? DAY { get; set; }

        public int? Ordem { get; set; }

        [StringLength(100)]
        public string NomePacote { get; set; }

        [StringLength(50)]
        public string tourmap { get; set; }

        public decimal? Range01 { get; set; }

        [StringLength(50)]
        public string Base01 { get; set; }

        public decimal? Range02 { get; set; }

        [StringLength(50)]
        public string Base02 { get; set; }

        public decimal? Range03 { get; set; }

        [StringLength(50)]
        public string Base03 { get; set; }

        public decimal? Range04 { get; set; }

        [StringLength(50)]
        public string Base04 { get; set; }

        public decimal? Range05 { get; set; }

        [StringLength(50)]
        public string Base05 { get; set; }

        public decimal? Range06 { get; set; }

        [StringLength(50)]
        public string Base06 { get; set; }

        public decimal? Range07 { get; set; }

        [StringLength(50)]
        public string Base07 { get; set; }

        public decimal? Range08 { get; set; }

        [StringLength(50)]
        public string Base08 { get; set; }

        public decimal? Range09 { get; set; }

        [StringLength(50)]
        public string Base09 { get; set; }

        public decimal? Range10 { get; set; }

        [StringLength(50)]
        public string Base10 { get; set; }

        public decimal? Range11 { get; set; }

        [StringLength(50)]
        public string Base11 { get; set; }

        public decimal? Range12 { get; set; }

        [StringLength(50)]
        public string Base12 { get; set; }

        public decimal? Range13 { get; set; }

        [StringLength(50)]
        public string Base13 { get; set; }

        public decimal? Range14 { get; set; }

        [StringLength(50)]
        public string Base14 { get; set; }

        public decimal? Range15 { get; set; }

        [StringLength(50)]
        public string Base15 { get; set; }

        public decimal? Range16 { get; set; }

        [StringLength(50)]
        public string Base16 { get; set; }

        public decimal? Range17 { get; set; }

        [StringLength(50)]
        public string Base17 { get; set; }

        public bool? TC_Rateado { get; set; }

        public int? OptQuote { get; set; }
    }
}
