namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MarkupPais
    {
        [Key]
        public int MarkupPais_id { get; set; }

        public int? Pais_id { get; set; }

        public decimal? Markup { get; set; }

        public decimal? MarkupServ { get; set; }

        public decimal? Desconto { get; set; }

        public decimal? MarkupLodge { get; set; }

        public virtual Pais Pais { get; set; }
    }
}
