namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_SupplierAmenities
    {
        [Key]
        public int S_SupplierAmenities_ID { get; set; }

        public int? S_SupplierAmenities_Supplier_ID { get; set; }

        public int? S_SupplierAmenities_Amenities_ID { get; set; }

        public virtual S_Amenities S_Amenities { get; set; }
    }
}
