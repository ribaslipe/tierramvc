namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_PoliticaTCTarifas
    {
        [Key]
        public int S_PoliticaTCTarifas_id { get; set; }

        public int? S_politicaTC_id { get; set; }

        public int? S_merc_tarif_id { get; set; }

        public virtual S_Politica_TC S_Politica_TC { get; set; }
    }
}
