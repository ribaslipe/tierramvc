namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Mercado_Meal
    {
        [Key]
        public int S_merc_meal_id { get; set; }

        public int S_meal_id { get; set; }

        public decimal? S_merc_meal_tarifa { get; set; }

        public int? S_merc_tarif_status_id { get; set; }

        public int S_merc_periodo_id { get; set; }

        public decimal? S_merc_meal_tarifa_total { get; set; }

        public decimal? S_merc_meal_porc_taxa { get; set; }

        public decimal? S_merca_meal_porc_imposto { get; set; }

        public bool? AddFile { get; set; }

        public bool? MealUmaVez { get; set; }

        public virtual S_Meal S_Meal { get; set; }

        public virtual S_Mercado_Periodo S_Mercado_Periodo { get; set; }

        public virtual S_Mercado_Tarifa_Status S_Mercado_Tarifa_Status { get; set; }
    }
}
