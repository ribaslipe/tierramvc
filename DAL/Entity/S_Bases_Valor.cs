namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Bases_Valor
    {
        [Key]
        public int S_Bases_Valor_id { get; set; }

        public decimal? S_Bases_Valor_valor { get; set; }

        public int? Base_id { get; set; }

        public int? IdMServico { get; set; }

        public virtual Monta_Servico Monta_Servico { get; set; }

        public virtual S_Bases S_Bases { get; set; }
    }
}
