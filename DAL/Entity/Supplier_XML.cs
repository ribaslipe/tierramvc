namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Supplier_XML
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Supplier_XML()
        {
            Tarif_Categoria_XML = new HashSet<Tarif_Categoria_XML>();
        }

        public int Id { get; set; }

        public int Id_Plataforma { get; set; }

        public int SupplierId_Prosoftware { get; set; }

        [StringLength(50)]
        public string SupplerId_Plataforma { get; set; }

        public virtual Plataforma_XML Plataforma_XML { get; set; }

        public virtual Supplier Supplier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tarif_Categoria_XML> Tarif_Categoria_XML { get; set; }
    }
}
