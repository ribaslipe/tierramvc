namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MarkupFile")]
    public partial class MarkupFile
    {
        [Key]
        public int MarkupFile_id { get; set; }

        public DateTime? MarkupFile_dataInicial { get; set; }

        public DateTime? MarkupFile_dataFinal { get; set; }

        public decimal? MarkupFile_hotel { get; set; }

        public decimal? MarkupFile_descontoHotel { get; set; }

        public decimal? MarkupFile_servico { get; set; }

        public decimal? MarkupFile_descontoServico { get; set; }

        public decimal? MarkupFile_seasonal { get; set; }

        public decimal? MarkupFile_meals { get; set; }

        public int? Quotation_Grupo_Id { get; set; }

        public virtual Quotation_Grupo Quotation_Grupo { get; set; }
    }
}
