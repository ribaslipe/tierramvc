namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TBItinerary_Highlights
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Code { get; set; }
        
        [StringLength(100)]
        public string Highlight { get; set; }

        public int? Cidade_ID { get; set; }

        public bool? Visible { get; set; }

        public int optQuote { get; set; }
    }
}
