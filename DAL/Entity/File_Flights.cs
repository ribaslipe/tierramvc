namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class File_Flights
    {
        [Key]
        public int Flights_id { get; set; }

        public int? Cidade_id { get; set; }

        [StringLength(50)]
        public string Cidade_iata { get; set; }

        public bool? Flights_DataFrom { get; set; }

        public bool? Flights_DataTo { get; set; }

        [StringLength(100)]
        public string Flights_nome { get; set; }

        [StringLength(20)]
        public string Flights_hora { get; set; }

        [StringLength(50)]
        public string Flights_pickUp { get; set; }

        [StringLength(50)]
        public string Flights_dropOff { get; set; }

        [StringLength(500)]
        public string Flights_remarks { get; set; }

        [StringLength(50)]
        public string Flights_ConfirmationNbr { get; set; }

        public bool? Flights_OS { get; set; }
    }
}
