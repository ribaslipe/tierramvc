namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OfferHash")]
    public partial class OfferHash
    {
        [Key]
        public int OfferHash_id { get; set; }

        [StringLength(50)]
        public string OfferHash_hash { get; set; }

        [StringLength(50)]
        public string OfferHash_quote { get; set; }

        public int? OfferHash_type { get; set; }

        public int? OfferHash_price { get; set; }

        public int? OfferHash_optQuote { get; set; }

        public int? OfferHash_ratear { get; set; }
    }
}
