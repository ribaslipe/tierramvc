namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tipo_Room
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tipo_Room()
        {
            S_Mercado_Tarifa = new HashSet<S_Mercado_Tarifa>();
        }

        [Key]
        public int Tipo_room_id { get; set; }

        [StringLength(50)]
        public string Tipo_room_nome { get; set; }

        public int? Tipo_room_number { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<S_Mercado_Tarifa> S_Mercado_Tarifa { get; set; }
    }
}
