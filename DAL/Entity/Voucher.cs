namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Voucher")]
    public partial class Voucher
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Voucher()
        {
            Voucher_AtYourDiscretion = new HashSet<Voucher_AtYourDiscretion>();
            Voucher_IncludedServices = new HashSet<Voucher_IncludedServices>();
            Voucher_Recommendations = new HashSet<Voucher_Recommendations>();
            Voucher_Trip = new HashSet<Voucher_Trip>();
        }

        public int Id { get; set; }

        public int QuotationGrupoId { get; set; }

        [Required]
        [StringLength(200)]
        public string TourName { get; set; }

        [Required]
        [StringLength(200)]
        public string TourCode { get; set; }

        public byte NbrTravellers { get; set; }

        public byte NbrChildren { get; set; }

        public byte NbrInfants { get; set; }

        [Required]        
        public string Travellers { get; set; }

        [Required]
        [StringLength(50)]
        public string DataInOut { get; set; }

        [Required]
        public string EmergencyNumbers { get; set; }

        [Required]
        public string SecurityCheck { get; set; }

        public bool ImportantRecommendationsEdited { get; set; }

        public bool IncludedServicesEdited { get; set; }

        public bool AtYourDiscretionEdited { get; set; }

        public bool TripEdited { get; set; }

        public int optQuote { get; set; }

        public string Note { get; set; }

        public bool? allNames { get; set; }

        public string TitleEmergencyNumbers { get; set; }        

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Voucher_AtYourDiscretion> Voucher_AtYourDiscretion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Voucher_IncludedServices> Voucher_IncludedServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Voucher_Recommendations> Voucher_Recommendations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Voucher_Trip> Voucher_Trip { get; set; }
    }
}
