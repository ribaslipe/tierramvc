namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Quotation_Grupo_Qtd_Baby_Nome
    {
        [Key]
        public int Qtd_Baby_Nome_id { get; set; }

        public int Qtd_Baby_id { get; set; }

        [StringLength(100)]
        public string Nome { get; set; }

        [StringLength(100)]
        public string Sobrenome { get; set; }

        public virtual Quotation_Grupo_Qtd_Baby Quotation_Grupo_Qtd_Baby { get; set; }
    }
}
