namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Categoria
    {
        [Key]
        public int Categ_id { get; set; }

        [StringLength(50)]
        public string Categ_classificacao { get; set; }

        public int? Categ_ordem { get; set; }

        [StringLength(500)]
        public string Categ_imgmemo { get; set; }

        [StringLength(50)]
        public string Categ_imgnome { get; set; }
    }
}
