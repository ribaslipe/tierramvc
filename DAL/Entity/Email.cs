namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Email")]
    public partial class Email
    {
        [Key]
        public int Email_id { get; set; }

        public string Email_Obs { get; set; }

        public int? TIPOSUPL_id { get; set; }

        public virtual TipoSupl TipoSupl { get; set; }
    }
}
