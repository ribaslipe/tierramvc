namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cidade_XML
    {
        public int id { get; set; }

        public int? Cid_id_Plataforma { get; set; }

        public int? Cid_id_Prosoftware { get; set; }

        public int? id_Plataforma { get; set; }

        public virtual Plataforma_XML Plataforma_XML { get; set; }
    }
}
