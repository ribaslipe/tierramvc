namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class S_Mercado_Periodo_Black
    {
        [Key]
        public int S_merc_periodo_black_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? S_merc_periodo_black_from { get; set; }

        [Column(TypeName = "date")]
        public DateTime? S_merc_periodo_black_to { get; set; }

        [StringLength(1000)]
        public string S_merc_periodo_black_obs { get; set; }

        public int S_id { get; set; }

        public int Mercado_id { get; set; }

        public int? BaseTarifaria_id { get; set; }

        public virtual BaseTarifaria BaseTarifaria { get; set; }

        public virtual Mercado Mercado { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
