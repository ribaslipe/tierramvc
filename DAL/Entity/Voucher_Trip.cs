namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Voucher_Trip
    {
        public int Id { get; set; }

        public int VoucherId { get; set; }

        [Required]
        [StringLength(100)]
        public string DateRange { get; set; }

        [Required]
        public string Destination { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual Voucher Voucher { get; set; }
    }
}
