namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Quote_ServExtra
    {
        [Key]
        public int Quote_ServExtra_id { get; set; }

        public int? File_id { get; set; }

        public int? Flights_id { get; set; }

        public int? Paxs { get; set; }

        public int? Paying_Pax { get; set; }

        public int? Supp_Paying_Pax { get; set; }

        public DateTime? Data_From { get; set; }

        public DateTime? Data_To { get; set; }

        public decimal? markup { get; set; }

        public decimal? desconto { get; set; }

        public decimal? markupNet { get; set; }

        public decimal? descontoNet { get; set; }

        public decimal? S_merc_tarif_vendaNet { get; set; }

        public int? Qtd_ServExtr { get; set; }

        public int? NumPaxs_ServExtr { get; set; }

        public decimal? File_ServExtra_valor { get; set; }

        public decimal? File_ServExtra_vendaNet { get; set; }

        public decimal? File_ServExtra_venda { get; set; }

        public decimal? File_ServExtra_total { get; set; }

        [StringLength(200)]
        public string S_nome { get; set; }

        public int? S_id { get; set; }

        [StringLength(100)]
        public string File_ServExtra_nome { get; set; }

        [StringLength(100)]
        public string TipoServico { get; set; }

        public int? TipoServico_id { get; set; }

        [StringLength(50)]
        public string Moeda { get; set; }

        public int? Moeda_id { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string Flag { get; set; }

        public bool? FTC { get; set; }

        public decimal? Valor_Conferir { get; set; }

        public decimal? Cambio { get; set; }

        public bool? Cancelado { get; set; }

        public int? Ordem { get; set; }

        public bool? UpdVenda { get; set; }

        public int? Range_id { get; set; }

        public bool? TBulk { get; set; }

        public bool? TC { get; set; }

        public bool? FileWeb { get; set; }
    }
}
