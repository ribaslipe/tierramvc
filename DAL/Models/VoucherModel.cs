﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class VoucherModel
    {
        public string Data { get; set; }
        public string Cidade { get; set; }
        public string Servico { get; set; }
        public int Pax { get; set; }
        public string Flight { get; set; }
        public string Supplier { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Usuario { get; set; }
        public string Linguagem { get; set; }
        public Byte[] Logo { get; set; }
        public DateTime DataFrom { get; set; }
        public string dtFrom { get; set; }
        public string dtTo { get; set; }
        public bool Meal { get; set; }
        public int Flight_id { get; set; }
        public int S_id { get; set; }
        public string Descricao { get; set; }
    }
}
