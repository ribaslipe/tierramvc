﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class TarifasHotelModel
    {
        public string S_mercado_tarifa_status_nome { get; set; }
        public string Tarif_categoria_nome { get; set; }
        public string Tipo_room_nome { get; set; }
        public int S_merc_tarif_id { get; set; }
        public string Moeda_sigla { get; set; }
        public int Moeda_id { get; set; }
        public int Tarif_categoria_id { get; set; }
        public int Tipo_room_id { get; set; }
        public int Tipo_room_number { get; set; }
        public bool S_merc_tarif_porApartamento { get; set; }
        public string EmailSupplier { get; set; }
        public int S_id { get; set; }
        public string IdHotelXml { get; set; }
        public int CategoriaIdXml { get; set; }
        public string S_mercado_tipo_tarifa_nome { get; set; }

        public string Refeicao { get; set; }

        public DateTime dtfrom { get; set; }
        public DateTime dtTo { get; set; }

        public decimal S_merc_tarif_tarifa_total { get; set; }
        public decimal S_merc_tarif_tarifa_total2 { get; set; }
        public decimal S_merc_tarif_tarifa_total3 { get; set; }
        public decimal S_merc_tarif_tarifa_total4 { get; set; }

        public string TituloTarifa { get; set; }

        public string PoliticaCancel { get; set; }

        public string disponibilidade { get; set; }

        public int qtdPax { get; set; }

        public string QuoteId_RoomsXML { get; set; }
        public string QuoteId_HotelsPro { get; set; }

        public string PoliticaCancelamento { get; set; }
        public string PoliticaCancelamento2 { get; set; }
        public string PoliticaCancelamento3 { get; set; }
        public string PoliticaCancelamento4 { get; set; }

        public string RatePlanName { get; set; }

        public long? RatePlanId { get; set; }
        public string RatePlanName2 { get; set; }
        public string RatePlanName3 { get; set; }
        public string RatePlanName4 { get; set; }

        public bool block { get; set; }
        public int? totaldayspck { get; set; }
        public string DisponibilidadeRooms { get; set; }

        public DateTime dtBlockPack { get; set; }

        public bool? NR { get; set; }
        public bool? checkHotelsPro { get; set; }

        public string QuoteId { get; set; }

        public string conditionRoomsXml { get; set; }
        public string disponibilidadeRoomsXML { get; set; }


        public string S_merc_periodo_fdsFrom { get; set; }
        public string S_merc_periodo_fdsTo { get; set; }
        public int Responsavel_id { get; set; }
        public string ResponsavelNome { get; set; }
        public string S_mercado_estacao_nome { get; set; }
        public string Pacote_nome { get; set; }

        public string vr { get; set; }

        public List<MealsHotelModel> lstMeal { get; set; }
    }
}
