﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class ProcurarServicoModel
    {
        public DateTime DataIn { get; set; }
        public DateTime DataOut { get; set; }
        public string Cidade { get; set; }
        public string Nome { get; set; }
        public int NumPax { get; set; }

        public int S_id { get; set; }
        public int ServId { get; set; }

        public bool recommended { get; set; }

        public int moedaCapaid { get; set; }
    }
}
