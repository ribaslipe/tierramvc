﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class OrdemPagServicosModel
    {
        public int ID { get; set; }
        public string TipoQuote { get; set; }
        public DateTime DataFrom { get; set; }
        public DateTime DataTo { get; set; }
        public string Service_Name { get; set; }
        public string Transf_Nome { get; set; }
    }
}
