﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class ReceitaOperacionalModel
    {

        public DateTime? dtFrom { get; set; }
        public DateTime? dtTo { get; set; }
        public string nome { get; set; }
        public string servNome { get; set; }
        public decimal valorConferidoDolar { get; set; }
        public decimal ValorTotal { get; set; }
        public decimal cambioFile { get; set; }
        public decimal cambioConf { get; set; }
        public decimal valorNet { get; set; }
        public int payPax { get; set; }
        public string payPaxSub { get; set; }
        public int numNoites { get; set; }


    }
}
