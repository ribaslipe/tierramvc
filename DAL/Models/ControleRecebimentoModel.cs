﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL.Models
{
    public class ControleRecebimentoModel
    {
        public DateTime dtINFILE { get; set; }
        public string Cliente { get; set; }
        public string File { get; set; }
        public DateTime dtPagamento { get; set; }
        public decimal TOTAL { get; set; }
        public string NomeConta { get; set; }
        public decimal vrPago { get; set; }
        public int idConta { get; set; }

        public decimal vrInvoice { get; set; }
        public string currency { get; set; }
        public decimal cambio { get; set; }

        public decimal? IOF { get; set; }

        public int idCentroCusto { get; set; }

    }
}
