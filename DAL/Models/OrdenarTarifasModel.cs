﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class OrdenarTarifasModel
    {
        public int Tarif_categoria_id { get; set; }
        public string Tarif_categoria_nome { get; set; }
        public int S_merc_tarif_ordem { get; set; }
        public int CategoriaID { get; set; }
    }
}
