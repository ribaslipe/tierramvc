﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class ResultadoHoteisModel
    {
        public int S_id { get; set; }
        public string S_nome { get; set; }
        public string S_endereco { get; set; }
        public string S_telefone { get; set; }
        public string Moeda_sigla { get; set; }
        public string Pacote_nome { get; set; }
        public string S_merc_tarif_tarifa_total { get; set; }
        public string Tipo_room_nome { get; set; }
        public string Tarif_categoria_nome { get; set; }
        public int Tarif_categoria_id { get; set; }
        public string S_mercado_estacao_nome { get; set; }
        public string S_merc_periodo_from { get; set; }
        public string S_merc_periodo_to { get; set; }
        public string S_merc_periodo_fdsFrom { get; set; }
        public string S_merc_periodo_fdsTo { get; set; }
        public bool S_merc_tarif_porApartamento { get; set; }
        public int Tipo_room_number { get; set; }
        public string Responsavel { get; set; }
        public int Responsavel_id { get; set; }
        public string S_mercado_tarifa_status_nome { get; set; }

        public List<TarifasHotelModel> lstTarifas { get; set; }
        public List<MealsHotelModel> lstMeals { get; set; }
    }
}
