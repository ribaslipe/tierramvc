﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class ServicosBasesModel
    {

        public string Servicos_Nome { get; set; }
        public int? Base_de { get; set; }
        public int? Base_ate { get; set; }
        public decimal MServico_Valores_Bulk { get; set; }
        public decimal MServico_Valores_Bulk_Total { get; set; }
        public decimal MServico_Valores_Bulk_Venda { get; set; }
        public string Moeda_sigla { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
    }
}
