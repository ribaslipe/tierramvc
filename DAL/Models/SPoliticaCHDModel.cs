﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class SPoliticaCHDModel
    {
        public int S_politicaCHD_id { get; set; }
        public int? S_politicaCHD_qtd { get; set; }
        public int? S_politicaCHD_idadeDe { get; set; }
        public int? S_politicaCHD_idade { get; set; }
        public string S_politicaCHD_free { get; set; }
        public decimal? S_politicaCHD_valor { get; set; }
        public decimal? S_politicaCHD_percent { get; set; }
    }
}
