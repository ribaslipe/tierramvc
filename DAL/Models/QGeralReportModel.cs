﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class QGeralReportModel
    {

        public int idCentroCusto { get; set; }
        public string Cliente { get; set; }
        public int idCliente { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public int NumPaxs { get; set; }
        public int NumFile { get; set; }
        public string NumQuote { get; set; }
        public int idQuoteGrupo { get; set; }                
        public decimal TotalInvoice { get; set; }
        public int idPais { get; set; }

    }
}
