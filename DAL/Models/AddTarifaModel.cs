﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AddTarifaModel
    {
        public int idperiodo { get; set; }
        public string ddlTipoCadastro { get; set; }
        public bool dividePac { get; set; }
        public string txtStatusTarifa { get; set; }
        public string txtStatusTarifaTotal { get; set; }
        public int statusid { get; set; }
        public string txtPorcentagemTaxa { get; set; }
        public string txtPorcentagemImposto { get; set; }
        public string txtComissao { get; set; }
        public int Tipo_room_id { get; set; }
        public int Tarif_categoria_id { get; set; }
        public int S_mercado_tipo_tarifa_id { get; set; }
        public int RdoTarifa { get; set; }
        public int Sid { get; set; }
        public int S_merc_tarif_status_id { get; set; }
        public int TarifaID { get; set; }
        public bool? S_merc_tarif_porApartamento { get; set; }
        public bool? S_merc_tarif_porPessoa { get; set; }

        public int IDMeal { get; set; }
        public int IdTarifaBase { get; set; }

    }
}
