﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class QuotationGrupoModel
    {

        public int Quotation_Grupo_Id { get; set; }

        public string quotationCode { get; set; }

        public int Quotation_Id { get; set; }

        public int Cliente_id { get; set; }
        public string Cliente_nome { get; set; }
        public Cliente Cliente { get; set; }

        public int S_mercado_tipo_tarifa_id { get; set; }

        public int US_id { get; set; }
        public Usuarios Usuarios { get; set; }

        public int FLinguagem_id { get; set; }

        public int? Moeda_id { get; set; }

        public int? Quotation_status_id { get; set; }

        public int Quotation_Grupo_NGrupo { get; set; }

        public string Pax_Group_Name { get; set; }

        public DateTime dataQuote { get; set; }

        public DateTime? dataIN { get; set; }

        public DateTime? dateOut { get; set; }

        public string tourCode { get; set; }

        public decimal CambioFile { get; set; }    

        public decimal markupFile { get; set; }
        
        public Moeda Moeda { get; set; }


        public Quotation_Status Quotation_Status { get; set; }

        public int S_merc_tarif_id { get; set; }
        public S_Mercado_Tipo_Tarifa S_Mercado_Tipo_Tarifa { get; set; }


        public File_Liguagem File_Liguagem { get; set; }

        public List<Remark_File> Remark_File { get; set; }
        public string remarks { get; set; }

        public List<Quotation_Grupo_Qtd_Adult> Quotation_Grupo_Qtd_Adult { get; set; }
        public int QtdAdult { get; set; }

        public List<Quotation_Grupo_Qtd_Chd> Quotation_Grupo_Qtd_Chd { get; set; }
        public int QtdChd { get; set; }

        public List<Quotation_Grupo_Qtd_Baby> Quotation_Grupo_Qtd_Baby { get; set; }
        public int QtdBaby { get; set; }


        public List<MarkupFile> lstMarkupFile { get; set; }

        public bool cancelall { get; set; }

        public int? grpselecionadonomes { get; set; }

        public string surname { get; set; }
    }

}
