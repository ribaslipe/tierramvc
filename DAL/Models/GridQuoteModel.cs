﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class GridQuoteModel
    {
        public int idRange { get; set; }

        public int IdRetorna { get; set; }
        public DateTime? dtFrom { get; set; }
        public DateTime? dtTo { get; set; }
        public string Nome { get; set; }
        public string NomeTr { get; set; }
        public string Room { get; set; }
        public string Categoria { get; set; }
        public string Flag { get; set; }
        public string MealNome { get; set; }
        public string MealStatus { get; set; }
        public string Status { get; set; }
        public int? DAY { get; set; }
        public bool? Bulk { get; set; }
        public bool? Optional { get; set; }
        public bool SubServico { get; set; }
        public int? Paying_Pax { get; set; }
        public int? Ordem { get; set; }
        public string NomePacote { get; set; }

        public string NomeSupp { get; set; }

        public int OptQuote { get; set; }

        public string DE { get; set; }
        public string ATE { get; set; }

        public decimal VENDA { get; set; }
        public decimal VALOR { get; set; }
        public decimal VRNET { get; set; }
        public decimal VRTOTAL { get; set; }

        public int Flight_id { get; set; }

        public int idCidade { get; set; }

        public bool TC { get; set; }

        public int S_id { get; set; }
        public int Plataforma_id { get; set; }
        public string Plataforma { get; set; }

        public int Responsavel_id { get; set; }

        public string remarkservico { get; set; }

        public bool? Provisorio { get; set; }

        public List<string> lstValores { get; set; }
    }

}
