﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class InvoicesPagosClienteModel
    {

        public decimal? Valor { get; set; }
        public DateTime dtRecebida { get; set; }
        public int idBancoRecebido { get; set; }
        public string NomeBancoReceb { get; set; }

    }
}
