﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class FilePrintQuoteModel
    {

        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public string ServNome { get; set; }
        public string Room { get; set; }
        public string Meal { get; set; }
        public string Bulk { get; set; }
        public string Optional { get; set; }
        public decimal VrNet { get; set; }
        public decimal VrMarkup { get; set; }
        public decimal VrDesconto { get; set; }
        public int NrNoites { get; set; }
        public decimal VrVenda { get; set; }
        public decimal VrTotalNet { get; set; }
        public decimal VrTotalVenda { get; set; }
        public string Base { get; set; }
        public int IdRetorna { get; set; }
        public string Flag { get; set; }
        public int ordem { get; set; }
        public int idCidade { get; set; }
        public int idServico { get; set; }

    }
}
