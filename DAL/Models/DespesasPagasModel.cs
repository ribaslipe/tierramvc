﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class DespesasPagasModel
    {

        public string CentroCusto { get; set; }
        public string ClassContab { get; set; }
        public decimal VrReal { get; set; }
        public decimal VrUSD { get; set; }
        public DateTime dtPagamento { get; set; }
        public string Fornecedor { get; set; }
        public string FaturaNumero { get; set; }
        public string tipoFatura { get; set; }
        public string contaNome { get; set; }
        public decimal vrPago { get; set; }

        public int idFatura { get; set; }

    }
}
