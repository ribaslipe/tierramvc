﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class GridSubServAdicionadosModel
    {
        public string ItemSubServico_nome { get; set; }
        public string SubItem_nome { get; set; }
        public string S_nome { get; set; }
        public int S_id { get; set; }
        public int IdMServico { get; set; }
        public string ServicoCompleto_de { get; set; }
        public string ServicoCompleto_ate { get; set; }
        public int ServicoCompleto_id { get; set; }
        public int Base_Index { get; set; }
    }
}
