﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class TarifasSupplierModel
    {
        public int S_merc_periodo_id { get; set; }
        public DateTime S_merc_periodo_from { get; set; }
        public DateTime S_merc_periodo_to { get; set; }
        public string Pacote_nome { get; set; }
        public string S_mercado_estacao_nome { get; set; }
        public int S_id { get; set; }
        public string S_merc_periodo_fdsFrom { get; set; }
        public string S_merc_periodo_fdsTo { get; set; }

        public bool Provisorio { get; set; }
    }
}
