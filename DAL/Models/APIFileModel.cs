﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DAL.Models
{
    public class ItemFile
    {
        [XmlElement("QCode")]
        public string QCode { get; set; }

        [XmlElement("PaxGName")]
        public string PaxGName { get; set; }

        [XmlElement("dtFrom")]
        public DateTime dtFrom { get; set; }

        [XmlElement("dtTo")]
        public DateTime dtTo { get; set; }

        [XmlElement("Ordem")]
        public int Ordem { get; set; }

        [XmlElement("CodIATA")]
        public string CodIATA { get; set; }

        [XmlElement("Nome")]
        public string Nome { get; set; }

        [XmlElement("status")]
        public string status { get; set; }

    }
}
