﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class MealsHotelModel
    {
        public string S_meal_nome { get; set; }
        public int S_id { get; set; }
        public int S_merc_meal_id { get; set; }
        public int Moeda_id { get; set; }
        public decimal S_merc_meal_tarifa_total { get; set; }
        public string S_mercado_tarifa_status_nome { get; set; }
        public DateTime S_merc_periodo_from { get; set; }
        public DateTime S_merc_periodo_to { get; set; }
        public string Moeda_nome { get; set; }
        public decimal S_merc_meal_tarifa { get; set; }
        public string S_mercado_estacao_nome { get; set; }
        public string Moeda_sigla { get; set; }                              

    }
}
