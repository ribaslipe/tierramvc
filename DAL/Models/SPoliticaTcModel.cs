﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class SPoliticaTcModel
    {
        public int S_politicaTc_id { get; set; }
        public int? S_politicaTc_qtd { get; set; }
        public string S_politicaTC_free { get; set; }
        public decimal? S_politicaTc_valor { get; set; }
        public decimal? S_politicaTc_percent { get; set; }
    }
}
