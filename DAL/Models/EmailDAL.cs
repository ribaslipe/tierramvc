﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class EmailDAL
    {
        private Model Con;               

        public EmailDAL()
        {
            Con = new Model();
        }

        public Email ObterPorTipoSupp(int TipoSupp)
        {
            try
            {
                return Con.Email.Where(e => e.TIPOSUPL_id == TipoSupp).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
