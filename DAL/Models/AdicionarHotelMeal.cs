﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AdicionarHotelMeal
    {
        public string qCode { get; set; }
        public int IdQuotGrupo { get; set; }
        public DateTime DataPeriodoInHotel { get; set; }
        public DateTime DataPeriodoToHotel { get; set; }
        public string cidade { get; set; }
        public int moedaid { get; set; }
        public int moedaMealid { get; set; }
        public int OptQuote { get; set; }
        public string BaseDe { get; set; }
        public string BaseAte { get; set; }
        public int MealId { get; set; }
        public string S_meal_nome { get; set; }
        public string lblFDS { get; set; }
        public string lblNomePacote { get; set; }
        public string lblEstacao { get; set; }
        public string lblStatusMeal { get; set; }
        public int NumPax { get; set; }
        public int S_id { get; set; }
        public int ResponsavelId { get; set; }
        public string NomeSupplier { get; set; }
        public string S_mercado_tarifa_status_nome { get; set; }
        public decimal S_merc_meal_tarifa_total { get; set; }

        public int numnoites { get; set; }
    }
}
