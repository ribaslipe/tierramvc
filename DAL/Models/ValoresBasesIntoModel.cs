﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class ValoresBasesIntoModel
    {
        public string MServico_Valores_Bulk { get; set; }
        public string MServico_Valores_Bulk_Total { get; set; }
        public string IdMServicoValores { get; set; }
        public int idbase { get; set; }

        public string De { get; set; }
        public string Ate { get; set; }
    }
}
