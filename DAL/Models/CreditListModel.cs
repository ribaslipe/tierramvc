﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class CreditListModel
    {

        public DateTime dtRecebimento { get; set; }
        public string Cliente { get; set; }
        public int idCliente { get; set; }
        public string NomeConta { get; set; }
        public string File { get; set; }
        public int InvoiceNbr { get; set; }
        public decimal VrPag { get; set; }
        public string InvHistorico { get; set; }
        public string PaxGroupName { get; set; }
        public int idMoeda { get; set; }
    }
}
