﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class FaturasPagasClienteModel
    {

        public decimal VrReal { get; set; }
        public int idConferencia { get; set; }
        public string TabelaRef { get; set; }
        public int idSeguimento { get; set; }
        public decimal VrUSD { get; set; }


    }
}
