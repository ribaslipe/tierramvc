﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
   
    public class Hotel
    {
        public string @id { get; set; }
    }

    public class HotelAvailability
    {
        public Hotel Hotel { get; set; }
        public object Result { get; set; }
    }

    public class AvailabilitySearchResult
    {
        public string Currency { get; set; }
        public string TestMode { get; set; }
        public IList<HotelAvailability> HotelAvailability { get; set; }
    }

}
