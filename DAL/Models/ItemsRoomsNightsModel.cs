﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class ItemsRoomsNightsModel
    {
        
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public int NumPaxs { get; set; }
        public int NumFile { get; set; }
        public string NumQuote { get; set; }
        public int idQuoteGrupo { get; set; }
        public string PaxGroupName { get; set; }
        public bool status { get; set; }
        public decimal TotalFileNet { get; set; }
        public decimal TotalFileVenda { get; set; }
        public decimal TotalInvoice { get; set; }       

    }
}
