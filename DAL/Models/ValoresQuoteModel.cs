﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class ValoresQuoteModel
    {
        
        public int Ranges_id { get; set; }
        public string Ranges_de { get; set; }        
        public string Ranges_ate { get; set; }    
        public int? Quotation_Id { get; set; }        
        public string Flag { get; set; }
        public int? FileTabelaId { get; set; }
        public decimal? markup { get; set; }
        public decimal? desconto { get; set; }
        public decimal? markupNet { get; set; }
        public decimal? descontoNet { get; set; }
        public decimal? VendaNet { get; set; }
        public decimal? Valor { get; set; }
        public decimal? Venda { get; set; }
        public decimal? ValorTotal { get; set; }
        public int? Ordem { get; set; }
        public int? RangeID { get; set; }
        public int? DAY { get; set; }
        public int? QtdTC { get; set; }
        public int? OptQuote { get; set; }
        public string OptQuoteNome { get; set; }

        public decimal PayPax { get; set; }
        public decimal SppPayPax { get; set; }
        public decimal Paxs { get; set; }

        public int NumNoites { get; set; }
        public string NrNoitesDesc { get; set; }

        public string BasesSelec { get; set; }

        public string qCode { get; set; }

        public bool SubServico { get; set; }

        public int hdFile { get; set; }
    }
}
