﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class itineraryModel
    {
        public int dia { get; set; }
        public string cidade { get; set; }
        public DateTime dtfrom { get; set; }
        public DateTime dtTo { get; set; }
        public string titulo { get; set; }
        public string urlIcon { get; set; }
        public string urlFoto { get; set; }
        public string descricao { get; set; }
        public string Flag { get; set; }
        public int ordem { get; set; }
        public int Sid { get; set; }
        public int idTabela { get; set; }
        public string categoria { get; set; }
        public string room { get; set; }
        public int tbItineraryID { get; set; }
        public string nights { get; set; }
        public string Code { get; set; }
        public int optquote { get; set; }
        public string NomePacote { get; set; }
        public string DescPacote { get; set; }

        public string FlightVoo { get; set; }
        public string FlightHora { get; set; }

        public string MealNome { get; set; }

        public bool optional { get; set; }
    }
}
