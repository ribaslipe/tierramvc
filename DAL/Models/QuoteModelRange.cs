﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class QuoteModelRange
    {

        public decimal markup { get; set; }
        public decimal valor { get; set; }
        public decimal desconto { get; set; }
        public decimal markupNet { get; set; }
        public decimal descontoNet { get; set; }
        public decimal vendaNet { get; set; }

    }
}
