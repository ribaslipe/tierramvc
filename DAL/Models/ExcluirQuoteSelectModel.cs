﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class ExcluirQuoteSelectModel
    {
        public string tipotarifa { get; set; }
        public string qCode { get; set; }
        public int grAtivo { get; set; }
        public int IdQuotGrupo { get; set; }
        public int OptQuote { get; set; }
        public string idRetornaArr { get; set; }
    }
}
