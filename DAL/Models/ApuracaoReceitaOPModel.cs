﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL.Models
{
    public class ApuracaoReceitaOPModel
    {
        public int idCentroCusto { get; set; }
        public string Cliente { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public int NumPaxs { get; set; }
        public int NumFile { get; set; }
        public string NumQuote { get; set; }
        public int idQuoteGrupo { get; set; }
        public string PaxGroupName { get; set; }
        public string StatusFile { get; set; }
        public decimal TotalFileNet { get; set; }
        public decimal TotalFileVenda { get; set; }
        public decimal TotalInvoice { get; set; }
        public string Operador { get; set; }        
        public string StatusMeal { get; set; }
        public bool VerificaTC { get; set; }
        public decimal cambioFile { get; set; }

    }
}
