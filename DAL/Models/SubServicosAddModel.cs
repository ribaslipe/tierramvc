﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class SubServicosAddModel
    {

        //mon.IdMServico,
        //sse.Servicos_Nome,
        //sbv.S_Bases_Valor_valor,
        //sba.Base_de,
        //sba.Base_ate,
        //sco.IdMServico_Temporada,
        //spp.S_id,
        //spp.S_nome,
        //mod.Moeda_id,
        //mod.Moeda_sigla,
        //mon.MServico_DataFrom,
        //mon.MServico_DataTo

        public int IdMServico_MON { get; set; }

        public int IdMServico_SC { get; set; }

        public int IdMServico_Temp { get; set; }

        public string Servicos_Nome { get; set; }
        public decimal S_Bases_Valor_valor { get; set; }
        public int Base_de { get; set; }
        public int Base_ate { get; set; }
        public int IdMServico_Temporada { get; set; }
        public int S_id { get; set; }
        public string S_nome { get; set; }
        public int Moeda_id { get; set; }
        public string Moeda_sigla { get; set; }
        public DateTime MServico_DataFrom { get; set; }
        public DateTime MServico_DataTo { get; set; }

        public decimal VR_sub { get; set; }

        public string NomeFantasia { get; set; }
        public string NomeSubItem { get; set; }

        public int TipoBase { get; set; }
    }
}
