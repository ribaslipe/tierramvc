﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class GridCadastradosSubModel
    {

        public int IdMServico { get; set; }
        public string MServico_Obs { get; set; }
        public DateTime MServico_DataFrom { get; set; }
        public DateTime MServico_DataTo { get; set; }
        public string S_nome { get; set; }
        public string S_telefone { get; set; }
        public string S_email { get; set; }
    }
}
