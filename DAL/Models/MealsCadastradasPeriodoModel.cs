﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class MealsCadastradasPeriodoModel
    {
        public int S_merc_meal_id { get; set; }
        public decimal S_merc_meal_tarifa_total { get; set; }
        public string S_meal_nome { get; set; }
        public decimal S_merc_meal_tarifa { get; set; }
        public string S_mercado_tarifa_status_nome { get; set; }
        public bool AddFile { get; set; }
        public bool MealUmaVez { get; set; }
    }
}
