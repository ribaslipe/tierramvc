﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class ProcuraHoteisModel
    {
        public DateTime DataIn { get; set; }
        public DateTime DataOut { get; set; }
        public string Cidade { get; set; }
        public string Nome { get; set; }
        public int NumPax { get; set; }
        public int NumPaxLodge { get; set; }
        public int moedacapa { get; set; }

        public string AutoHotel { get; set; }

        public int? idEstacao { get; set; }

        public int S_id { get; set; }
        public int IdQuoteGrupo { get; set; }

        public int grpselecionadoreserva { get; set; }

        public bool somentesistema { get; set; }

        public bool somenteomni { get; set; }
        public bool somenteroomsxml { get; set; }
        public bool somentehotelspro { get; set; }
        public bool somentects { get; set; }
    }
}
