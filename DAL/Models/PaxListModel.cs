﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class PaxListModel
    {

        public int idCliente { get; set; }
        public string Cliente { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public DateTime dtToFile { get; set; }
        public int NumPaxs { get; set; }
        public int NumFile { get; set; }
        public string NumQuote { get; set; }
        public int idQuoteGrupo { get; set; }
        public string PaxGroupName { get; set; }
        public string StatusFile { get; set; }
        public int idStatusFile { get; set; }
        public decimal TotalFileNet { get; set; }
        public decimal TotalFileVenda { get; set; }
        public decimal TotalInvoice { get; set; }
        public string Operador { get; set; }        
        public decimal Diferenca { get; set; }
        public string StatusMeal { get; set; }
        public bool VerificaTC { get; set; }
        public int idUsuario { get; set; }
        public int idVendedor { get; set; }

        public int idCentroCustoCliente { get; set; }
        public int idMoeda { get; set; }
        public string MoedaNome { get; set; }
        public decimal CambioFile { get; set; }

        public int idPais { get; set; }
        public int idCidade { get; set; }
        public string Pais { get; set; }
        public string Cidade { get; set; }

        public decimal TotalIof { get; set; }

        public string TipoTarifaGroup { get; set; }

    }
}
