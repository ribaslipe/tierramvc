﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class FileOrdenadoModel
    {

        public int IdRetorna { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public string Nome { get; set; }
        public string NomeTr { get; set; }
        public string Room { get; set; }
        public string Categoria { get; set; }
        public string MealNome { get; set; }
        public string MealStatus { get; set; }
        public string Status { get; set; }
        public bool Bulk { get; set; }
        public string Flag { get; set; }
        public bool SubServico { get; set; }
        public int Paying_Pax { get; set; }
        public int Ordem { get; set; }

        //IdRetorna = fts.Quote_Tarifas_id,
        //dtFrom = fts.Data_From,
        //dtTo = fts.Data_To,
        //Nome = fts.S_nome,
        //NomeTr = fts.S_nome,
        //Room = fts.Room,
        //Categoria = fts.Categoria,
        //MealNome = fts.S_meal_nome,
        //MealStatus = fts.S_meal_status,
        //Status = fts.Status,
        //Bulk = fts.TBulk,
        //Flag = fts.Flag,
        //SubServico = false,
        //Paying_Pax = fts.Paying_Pax,
        //Ordem = fts.Ordem

    }
}
