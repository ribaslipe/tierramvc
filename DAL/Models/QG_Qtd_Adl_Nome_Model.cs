﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class QG_Qtd_Adl_Nome_Model
    {        
        public int Qtd_Adult_Nome_id { get; set; }
        public int Qtd_Adult_id { get; set; }        
        public string Tratamento { get; set; }        
        public string Nome { get; set; }        
        public string Sobrenome { get; set; }
        public int? Idade { get; set; }
        public bool? Principal_Indicador { get; set; }
        public int Quotation_Grupo_Id { get; set; }

        public int? grp { get; set; }
    }
}
