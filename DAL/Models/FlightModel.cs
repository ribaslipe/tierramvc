﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class FlightModel
    {

        public int idFlight { get; set; }
        public bool txtConfirmationNumber { get; set; }
        public string txtConfirmationNumberTXT { get; set; }
        public int idCidade { get; set; }
        public string NomeCidade { get; set; }
        public string InOutFlights { get; set; }

        public string txtFlight { get; set; }
        public string txtHoraFlight { get; set; }
        public string txtPickUpFlight { get; set; }
        public string txtDropOffFlight { get; set; }
        public string txtRemarksFlight { get; set; }
        public bool chkVooFlightOS { get; set; }

        public int ServicoID { get; set; }
        public string Flag { get; set; }

        public string type { get; set; }
    }
}
