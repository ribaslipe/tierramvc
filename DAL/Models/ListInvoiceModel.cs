﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class ListInvoiceModel
    {

        public int File_id { get; set; }
        public string File { get; set; }
        public string Cliente { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public decimal VrFile { get; set; }
        public int InvNumber { get; set; }
        public decimal VrInv { get; set; }
        public DateTime dtEmissao { get; set; }

        public decimal? IOF { get; set; }

    }
}
