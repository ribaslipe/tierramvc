﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class PeriodosCadastradosSozinhoModel
    {
        public DateTime MServico_DataFrom { get; set; }
        public DateTime MServico_DataTo { get; set; }
        public string Tipo_categ_nome { get; set; }
        public string MServico_Obs { get; set; }
        public int IdMServico { get; set; }
        public string Mercado_nome { get; set; }
        public int Mercado_id { get; set; }
        public int BaseTarifaria_id { get; set; }
        public int TipoBase_id { get; set; }
        public int Moeda_id { get; set; }
    }
}
