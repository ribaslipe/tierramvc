﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class LstToursModel
    {
        public string Servicos_Nome { get; set; }
        public int Servicos_Id { get; set; }
        public string S_nome { get; set; }
        public bool? Recomended { get; set; }
        public DateTime MServico_DataFrom { get; set; }
        public DateTime MServico_DataTo { get; set; }
        public string Moeda_sigla { get; set; }
        public int Moeda_id { get; set; }
        public decimal? MServico_Valores_Bulk_Total { get; set; }
        public int? Base_de { get; set; }
        public int? Base_ate { get; set; }
        public int S_id { get; set; }

        public int? bseIndex { get; set; }
        public string obs { get; set; }

        public string Tipo_Nome { get; set; }
        public string Tipo_categ_nome { get; set; }

        public int TipoBaseId { get; set; }
        public string TipoBase { get; set; }

        //subs
        public int IdMServico { get; set; }
        public decimal? S_Bases_Valor_valor { get; set; }
        public int IdMServico_Temporada { get; set; }
        public bool SubServico { get; set; }


        public int servCompetoID { get; set; }
    }
}
