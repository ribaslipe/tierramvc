﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class GridBasesSubModel
    {
        public int Base_de { get; set; }
        public int Base_ate { get; set; }
        public string lblValorBase { get; set; }
        public int Base_id { get; set; }
    }
}
