﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class ServicosOSModel
    {
        public string Cidade { get; set; }
        public DateTime DataFrom { get; set; }
        public string NomeServico { get; set; }
        public int IDservico { get; set; }
    }
}
