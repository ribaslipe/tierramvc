﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class RptClientesModel
    {
        public string nome { get; set; }
        public string endereco { get; set; }
        public string email { get; set; }
        public string pais { get; set; }
        public string cidade { get; set; }
        public string site { get; set; }
        public string telefone { get; set; }
        public string fax { get; set; }
        public string cep { get; set; }

        public string contatoNome { get; set; }
        public string contatoEmail { get; set; }
        public string contatoTel { get; set; }
        public string contatoFax { get; set; }

    }
}
