﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AddBaseModel
    {
        public string qCode { get; set; }
        public int OptQuote { get; set; }
        public string BaseDe { get; set; }
        public string BaseAte { get; set; }
        public int IdQuotGrupo { get; set; }
        public int idmoedacapa { get; set; }
    }
}
