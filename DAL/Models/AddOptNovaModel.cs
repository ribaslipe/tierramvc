﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AddOptNovaModel
    {
        public string qCodeCopia { get; set; }
        public string qCode { get; set; }
        public int IdQuotGrupo { get; set; }
        public int ddlOptSelectFileCopia { get; set; }

        public bool copiatudo { get; set; }
    }
}
