﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class InvoiceTrocaModal
    {
        public int idInvoice { get; set; }
        public int idInvoicePagamento { get; set; }
        public string ClienteNome { get; set; }
        public int idCliente { get; set; }
        public DateTime? dtBaixa { get; set; }
        public decimal ValorBaixa { get; set; }
        public string NomeBanco { get; set; }
        public string nrNumero { get; set; }
        public int idBanco { get; set; }
        public string File { get; set; }

        public decimal txBancaria { get; set; }
        public decimal VrTrocaUSD { get; set; }
        public decimal VrTrocaREAL { get; set; }
        public decimal CambioInvTroca { get; set; }
        public DateTime? dtTroca { get; set; }
        public int idBcoRecebimento { get; set; }
        public int idBcoTroca { get; set; }

        public decimal GrandTotalInv { get; set; }
        public bool Trocado { get; set; }
    }
}
