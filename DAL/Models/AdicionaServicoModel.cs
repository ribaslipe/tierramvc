﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AdicionaServicoModel
    {
        public string qCode { get; set; }
        public int IdQuotGrupo { get; set; }
        public DateTime DataFrom { get; set; }
        public DateTime DataOut { get; set; }
        public string nomeServico { get; set; }
        public int NumPax { get; set; }
        public string cidade { get; set; }
        public string SupplierNome { get; set; }
        public int moedaid { get; set; }
        public int S_id { get; set; }
        public int OptQuote { get; set; }
        public int? IdMServicoTemporada { get; set; }
        public string BaseDe { get; set; }
        public string BaseAte { get; set; }

        public string TipoSrv { get; set; }

        public int TipoExtraID { get; set; }
        public string TipoExtra { get; set; }

        public string NomeExtra { get; set; }

        public int moedaidSeguimento { get; set; }
        public string VrFile { get; set; }
    }
}
