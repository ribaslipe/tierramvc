﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class TotalFileModel
    {
        public decimal tarifa { get; set; }
        public decimal total { get; set; }
        public string room { get; set; }
        public string flag { get; set; }
        public bool tbulk { get; set; }
        public int Paying_Pax { get; set; }
    }
}
