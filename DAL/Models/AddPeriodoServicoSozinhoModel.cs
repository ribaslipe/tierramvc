﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AddPeriodoServicoSozinhoModel
    {
        public int IdSupplier { get; set; }
        public int IdServico { get; set; }
        public int index { get; set; }
        public int ddlTipoServico2 { get; set; }
        public int ddlTipoServicoCateg2 { get; set; }
        public string txtPorcentagemTaxa { get; set; }
        public string txtPorcentagemImposto1 { get; set; }
        public string txtComissao { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public string txtObs { get; set; }
        public string valoresGrid { get; set; }
        public int opStatusTarifa { get; set; }
        public int CodMServicoSozinho { get; set; }
    }
}
