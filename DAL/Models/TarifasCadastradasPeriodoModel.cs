﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class TarifasCadastradasPeriodoModel
    {       
        public decimal S_merc_tarif_tarifa_total { get; set; }
        public int S_merc_tarif_id { get; set; }
        public string Tipo_room_nome { get; set; }
        public string Tarif_categoria_nome { get; set; }
        public string S_mercado_tipo_tarifa_nome { get; set; }
        public string S_mercado_tarifa_status_nome { get; set; }
        public decimal S_merc_tarif_tarifa { get; set; }

    }
}
