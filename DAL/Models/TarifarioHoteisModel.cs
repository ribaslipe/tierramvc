﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Models
{
    public class TarifarioHoteisModel
    {
        public int Sid { get; set; }
        public string SNome { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public string StatusTarifa { get; set; }
        public decimal? VTotal { get; set; }
        public bool PorPessoa { get; set; }
        public bool PorApartamento { get; set; }
        public string CidNome { get; set; }
        public string RoomNome { get; set; }
        public int RoomNumber { get; set; }
        public string Categoria { get; set; }
        public int idCategoria { get; set; }
        public string TipoTarifa { get; set; }
        public int idPeriodo { get; set; }
        public string Classificacao { get; set; }
        public int BaseDe { get; set; }
        public int BaseAte { get; set; }
        public int minNoites { get; set; }
        public string PacoteNome { get; set; }
        public int idMoeda { get; set; }
        public bool MandatorioBool { get; set; }
        public bool MinimoBool { get; set; }
        public string FdsFrom { get; set; }
        public string FdsTo { get; set; }
        public string SMercadoEstacao { get; set; }
        public S_Mercado_Estacao S_Mercado_Estacao { get; set; }

        public int? OrdemCateg { get; set; }

        public bool Seasonal { get; set; }

    }
}
