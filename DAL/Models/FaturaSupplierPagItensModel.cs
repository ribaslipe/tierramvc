﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class FaturaSupplierPagItensModel
    {

        public int FaturaSppPagID { get; set; }
        public DateTime dataPag { get; set; }
        
        public DateTime? dataPag2 { get; set; }
        public string File { get; set; }
        public int FaturaSppID { get; set; }
        public int FConfID { get; set; }
        public string FConfFlag { get; set; }
        public int FConf_idTabela { get; set; }
        public decimal vlrPago { get; set; }
        public decimal vlrPagoConf { get; set; }
        public string TipoPagamento { get; set; }
        public string nrCheque { get; set; }
        public string nrConta { get; set; }
        public string NomeConta { get; set; }
        public decimal FaturaVrConferir { get; set; }
        public string TabelaRef { get; set; }
        public string tipoConferencia { get; set; }
    }
}
