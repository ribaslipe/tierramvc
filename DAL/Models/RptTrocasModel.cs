﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class RptTrocasModel
    {
        public DateTime dtTroca { get; set; }
        public decimal VrUSD { get; set; }
        public decimal VrBaixa { get; set; }
        public decimal Cambio { get; set; }
        public decimal VrREAL { get; set; }
        public string File { get; set; }
        public DateTime dtINFile { get; set; }
        public DateTime dtPagamento { get; set; }
        public string Cliente { get; set; }
        public int idCliente { get; set; }
        public int idContaRecebimento { get; set; }
        public int idContaCreditada { get; set; }
        public string NomeBanco { get; set; }
        public bool? Trocado { get; set; }
        public decimal TxBco { get; set; }
        public string guide { get; set; }
    }
}
