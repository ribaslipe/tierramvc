﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class InfosOmniModel
    {
        public string NomeHotelDescXML { get; set; }
        public string PlataformaDescXML { get; set; }
        public string EstatusDescXML { get; set; }
        public string DescricaoReservaDescXML { get; set; }
        public string DescricaoCancelDescXML { get; set; }
        public string DescricaoPagDescXML { get; set; }
        public string BreackFastDescXML { get; set; }
        public string status { get; set; }

        public string ReservaCod { get; set; }
        public string APInome { get; set; }
        public string NomesPaxs { get; set; }
    }
}
