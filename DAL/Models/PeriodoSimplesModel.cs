﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class PeriodoSimplesModel
    {
        public string S_nome { get; set; }
        public DateTime MServico_DataFrom { get; set; }
        public DateTime MServico_DataTo { get; set; }
    }
}
