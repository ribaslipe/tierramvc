﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class RoomNightsModel
    {

        public string Hotel { get; set; }
        public int idHotel { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public string nrQuote { get; set; }
        public string RoomNights { get; set; }
        public decimal vrTotal { get; set; }
        public decimal vrMedio { get; set; }
        public int idRoom { get; set; }
        public int qtdPax { get; set; }
        public bool status { get; set; }
        public bool meal { get; set; }
        public bool Bulk { get; set; }

    }
}
