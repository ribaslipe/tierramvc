﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AdicionarHotelModel
    {
        public string qCode { get; set; }
        public int IdQuotGrupo { get; set; }
        public DateTime DataPeriodoInHotel { get; set; }
        public DateTime DataPeriodoToHotel { get; set; }
        public int moedaid { get; set; }
        public int OptQuote { get; set; }
        public string NomeSupplier { get; set; }
        public string lblFDS { get; set; }
        public string lblNomePacote { get; set; }
        public string lblEstacao { get; set; }
        public int ResponsavelId { get; set; }
        public int lblIdTarifa { get; set; }
        public string QtdRoom { get; set; }
        public decimal lblValorTotal { get; set; }
        public string lblRoom { get; set; }
        public string lblMoeda { get; set; }
        public string lblCategoria { get; set; }
        public int lblCategoriaId { get; set; }
        public int lblRoomId { get; set; }
        public int lblSupplierId { get; set; }
        public string lblEmailSupplier { get; set; }
        public int lblMoedaId { get; set; }
        public string hdfPlataforma { get; set; }
        public string BaseDe { get; set; }
        public string BaseAte { get; set; }

        public bool Omni { get; set; }
        public string moeda_OMNI { get; set; }
        public int lblCategoriaId_OMNI { get; set; }
        public int IdHotelXml_OMNI { get; set; }
        public bool reservaquote { get; set; }

        public bool Cts { get; set; }
        public string moeda_Cts { get; set; }
        public int lblCategoriaId_Cts { get; set; }
        public int IdHotelXml_Cts { get; set; }


        public bool RoomsXML { get; set; }
        public string moeda_RoomXML { get; set; }
        public int lblCategoriaId_RoomXML { get; set; }
        public int IdHotelXml_RoomXML { get; set; }
        public string QuoteID_RoomXML { get; set; }
        public string lblValorTotalstring { get; set; }


        public bool HotelsPro { get; set; }
        public string QuoteID_HotelsPro { get; set; }

        public string QuoteId { get; set; }
        public long? RatePlanId { get; set; }

        public int grpselecionadoreserva { get; set; }

        public string Breakfast { get; set; }

        public string politicacancelamento { get; set; }
    }
}
