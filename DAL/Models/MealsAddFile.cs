﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class MealsAddFile
    {
        public int S_merc_periodo_id { get; set; }
        public DateTime? S_merc_periodo_from { get; set; }
        public DateTime? S_merc_periodo_to { get; set; }
        public string Pacote_nome { get; set; }
        public decimal? S_merc_meal_tarifa { get; set; }
        public int? S_merc_tarif_status_id { get; set; }
        public decimal? S_merc_meal_tarifa_total { get; set; }
        public int S_meal_id { get; set; }

        public int S_merc_meal_id { get; set; }
    }
}
