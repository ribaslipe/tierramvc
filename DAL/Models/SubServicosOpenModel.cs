﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class SubServicosOpenModel
    {
        public int idRange { get; set; }
        public int idRetorna { get; set; }
        public string Type { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public string Base { get; set; }
    }
}
