﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class FileInfosModel
    {

        public int IdTabela { get; set; }
        public decimal TarifaNet { get; set; }
        public string MealStatus { get; set; }
        public int NumPaxItem { get; set; }
        public int numNoites { get; set; }
        public string Flag { get; set; }
        public string Fornecedor { get; set; }
        public int idFornecedor { get; set; }
        public string FornecedorSub { get; set; }
        public string NomeItem { get; set; }
        public string TipoServ { get; set; }
        public decimal VlrVenda { get; set; }
        public decimal VlrConferido { get; set; }
        public decimal cambioConf { get; set; }
        public bool SubServico { get; set; }
        public string room_meal { get; set; }
        public bool bulk { get; set; }
    }
}
