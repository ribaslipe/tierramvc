﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class ConciliacaoVRModel
    {
        public string tabela { get; set; }
        public decimal valorparcial { get; set; }
        public int id { get; set; }
        public decimal valorpago { get; set; }
    }
}
