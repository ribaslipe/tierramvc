﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class RangesAddTc
    {
        public string Ranges_de { get; set; }
        public string Ranges_ate { get; set; }
        public int Ordem { get; set; }
        public int Quotation_Id { get; set; }
    }
}
