﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class CotacaoETModel
    {
        public string Nome { get; set; }
        public string descricao { get; set; }
        public double days { get; set; }
        public int unidade { get; set; }
        public decimal Valor { get; set; }
        public decimal VlrTotal { get; set; }

        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }

        public string flagTabela { get; set; }
        public int idTabela { get; set; }
    }
}
