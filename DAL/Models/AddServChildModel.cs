﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class AddServChildModel
    {

        public string ServNome { get; set; }
        public string SupplierNome { get; set; }
        public int SuppId { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public string MoedaSigla { get; set; }
        public int MoedaId { get; set; }
        public decimal ValorTotal { get; set; }
        public int BaseDe { get; set; }
        public int BaseAte { get; set; }
        public bool TipoServ { get; set; }
        public int IdTemporada { get; set; }
        public string Trf_Tour { get; set; }

    }
}
