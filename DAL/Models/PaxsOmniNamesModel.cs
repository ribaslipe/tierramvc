﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class PaxsOmniNamesModel
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public bool Primary { get; set; }
    }
}
