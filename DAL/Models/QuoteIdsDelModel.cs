﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class QuoteIdsDelModel
    {

        public int idRange { get; set; }
        public int idTabela { get; set; }
        public string flag { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public int Ordem { get; set; }

    }
}
