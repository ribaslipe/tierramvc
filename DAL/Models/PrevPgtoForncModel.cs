﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class PrevPgtoForncModel
    {
        
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }        
        public string Fornecedor { get; set; }
        public decimal vrReal { get; set; }
        public decimal vrDolar { get; set; }
        public DateTime dtPag { get; set; }
        public string nrCheque { get; set; }
        public string nrBanco { get; set; }
        public int IdTabela { get; set; }
        public decimal TarifaNet { get; set; }
        public decimal VlrVenda { get; set; }
        public int idFornecedor { get; set; }
        public string MealStatus { get; set; }
        public string TipoServ { get; set; }
        public int NumPaxItem { get; set; }
        public int numNoites { get; set; }
        public string Flag { get; set; }
        public string NomeItem { get; set; }
        public bool SubServico { get; set; }
        public string status { get; set; }

    }
}
