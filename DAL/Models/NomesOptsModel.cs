﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class NomesOptsModel
    {
        public int OptQuote { get; set; }
        public string OptQuoteNome { get; set; }
    }
}
