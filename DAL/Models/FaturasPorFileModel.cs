﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class FaturasPorFileModel
    {

        public string supplier { get; set; }
        public string numeroFatura { get; set; }
        public decimal cambio { get; set; }
        public decimal valorFatura { get; set; }
        public decimal valorPago { get; set; }
        public DateTime dtVencimento { get; set; }
        public string Code { get; set; }
        public decimal valorConf { get; set; }
        public int FatID { get; set; }
    }
}
