﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class DepositoListModel
    {

        public int idCliente { get; set; }
        public string Cliente { get; set; }
        public DateTime dtDeposito { get; set; }
        public string Historico { get; set; }
        public decimal VrDeposito { get; set; }

    }
}
