﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class DadosGridSozinhoModel
    {
        public string ddlTipoCadastro { get; set; }
        public int ddlStatusTarifa { get; set; }
        public string txtValorServicoSozinho { get; set; }
        public string txtValorServicoSozinhoVenda { get; set; }
        public string lblValorServicoSozinhoTotal { get; set; }
        public int lblIDValores { get; set; }
        public int Base_id { get; set; }
    }
}
