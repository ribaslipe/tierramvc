﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class HoteisOfferModel
    {
        public string imgStars { get; set; }
        public int idSupplier { get; set; }
        public string nomeHotel { get; set; }
        public string cidade { get; set; }
        public string descricaoHotel { get; set; }
        public string imgHotel { get; set; }

        public List<S_GroupAmenities> lstAmenities { get; set; }
        public List<S_Amenities> lstAmenitiesItens { get; set; }

    }
}
