﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class TarifasFileModel
    {
        public decimal Valor { get; set; }

        public decimal Tarifa { get; set; }
        public decimal TarifaBulk { get; set; }

        public decimal TarifaCusto { get; set; }
        public string MealStatus { get; set; }
        public int NumPaxItem { get; set; }
        public int numNoites { get; set; }
        public string Flag { get; set; }
        public bool sub { get; set; }
        public int idTabela { get; set; }
        public bool bulk { get; set; }
        public string status { get; set; }

        public string NomeSupp { get; set; }
        public int idSupp { get; set; }
        public bool VerificaTC { get; set; }


    }
}
