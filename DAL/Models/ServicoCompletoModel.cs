﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class ServicoCompletoModel
    {        
        public int Base_Index { get; set; }
        public int IdMServico { get; set; }
        public int IdMServico_Temporada { get; set; }
        public int Servicos_Id { get; set; }
        public int? ServicoCompleto_ate { get; set; }
        public int? ServicoCompleto_de { get; set; }
        public int ServicoCompleto_id { get; set; }
        public bool? SubServico { get; set; }
        public string ItemSubServico_nome { get; set; }
        public int S_id { get; set; }
        public int IdMServicoMS { get; set; }
        public string SubItem_nome { get; set; }
        public string S_nome { get; set; }
    }
}
