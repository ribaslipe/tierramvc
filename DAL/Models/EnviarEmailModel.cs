﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class EnviarEmailModel
    {

        public int IdQuotGrupo { get; set; }
        public int idSpp { get; set; }
        public string emailEnvio { get; set; }
        public string conteudo { get; set; }
        public int userCapa { get; set; }
    }
}
