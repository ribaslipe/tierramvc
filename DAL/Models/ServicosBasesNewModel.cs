﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class ServicosBasesNewModel
    {

        public int BaseDe { get; set; }
        public int BaseAte { get; set; }
        public string SNome { get; set; }
        public int SID { get; set; }

    }


    public class ServicosNomesNewModel
    {
        public string NomeServ { get; set; }
        public int IDServ { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public int ordem { get; set; }
        public int idMoeda { get; set; }

        public decimal? Valor { get; set; }
    }


    public class ServicosNomesBasesNewModel
    {
        public string NomeServ { get; set; }
        public int IDServ { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public int ordem { get; set; }
        public decimal Valor { get; set; }
        public string statusValor { get; set; }
        public int BaseDe { get; set; }
        public int BaseAte { get; set; }
        public int idMoeda { get; set; }
    }

}
