﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class RptTrf_EfetuadasModel
    {

        public int CCreditada { get; set; }

        public int CDebita { get; set; }

        public string CCreditadaNome { get; set; }

        public string CDebitaNome { get; set; }

        public string  C_custo { get; set; }

        public string C_contab { get; set; }

        public DateTime dtTransacao { get; set; }

        public decimal Valor { get; set; }

        public decimal cambio { get; set; }

        public string historico { get; set; }

        public int IDTabela { get; set; }
    }
}
