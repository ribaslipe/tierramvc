﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class PrePagamentoModel
    {

        public int idInvoice { get; set; }
        public DateTime dtRec { get; set; }
        public decimal valRec { get; set; }
        public int qgid { get; set; }
        public int fileid { get; set; }
        public string quotCode { get; set; }


    }
}
