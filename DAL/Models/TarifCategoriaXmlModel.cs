﻿namespace DAL.Models
{
    public class TarifCategoriaXmlModel
    {
        public int Id { get; set; }
        public string CategoriaNome { get; set; }
        public string HotelNome { get; set; }
        public string Plataforma { get; set; }
        public int IdTierra { get; set; }
        public int IdPlataforma { get; set; }
    }
}