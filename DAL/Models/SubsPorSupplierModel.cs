﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class SubsPorSupplierModel
    {
        public DateTime MServico_DataFrom { get; set; }
        public DateTime MServico_DataTo { get; set; }
        public string MServico_Obs { get; set; }
        public string ItemSubServico { get; set; }
        public string SubItem { get; set; }
        public int IdMServico { get; set; }
        public string Moeda_sigla { get; set; }
        public int Base_index { get; set; }
    }
}
