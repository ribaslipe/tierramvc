﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class DatasCopiarTarifaModel
    {

        public int idPeriodo { get; set; }
        public DateTime? dtFrom { get; set; }
        public string fdsFrom { get; set; }
        public string fdsTo { get; set; }
        public string Estacao { get; set; }
        public string concatena { get; set; }

    }
}
