﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Models
{
    public class ApuracaoNFModel
    {

        public string Cliente { get; set; }
        public string File { get; set; }
        public decimal ReceitaUSD { get; set; }
        public decimal CambioTroca { get; set; }
        public decimal CambioPrevisto { get; set; }
        public DateTime dtRecebido { get; set; }
        public DateTime dtTroca { get; set; }
        public string BcoRecebimento { get; set; }
        public string AReceber { get; set; }
        public decimal PgtoFornecedor { get; set; }
        public decimal PgtoPrevisto { get; set; }
        public decimal LucroOperacional { get; set; }
        public decimal Porcentagem { get; set; }

    }
}
