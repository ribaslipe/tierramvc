﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class FileFlightsDAL
    {

        private Model Con;

        public FileFlightsDAL()
        {
            Con = new Model();
        }

        public void Salvar(File_Flights c)
        {
            try
            {
                Con.File_Flights.Add(c);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(File_Flights c)
        {
            try
            {
                Con.File_Flights.Remove(c);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public File_Flights ObterPorId(int IdFlight)
        {
            try
            {
                return Con.File_Flights.Where(c => c.Flights_id == IdFlight).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(File_Flights novo)
        {
            File_Flights antigo = ObterPorId(novo.Flights_id);

            antigo.Cidade_id        = novo.Cidade_id;
            antigo.Cidade_iata      = novo.Cidade_iata;
            antigo.Flights_DataFrom = novo.Flights_DataFrom;
            antigo.Flights_DataTo   = novo.Flights_DataTo;
            antigo.Flights_nome     = novo.Flights_nome;
            antigo.Flights_hora     = novo.Flights_hora;
            antigo.Flights_pickUp   = novo.Flights_pickUp;
            antigo.Flights_dropOff  = novo.Flights_dropOff;
			antigo.Flights_ConfirmationNbr = novo.Flights_ConfirmationNbr;
            antigo.Flights_remarks  = novo.Flights_remarks;
            antigo.Flights_OS       = novo.Flights_OS;

            Con.SaveChanges();
        }

        public File_Flights Clone(File_Flights antigo, File_Flights novo)
        {
            try
            {
                novo.Cidade_id = antigo.Cidade_id;
                novo.Cidade_iata = antigo.Cidade_iata;
                novo.Flights_DataFrom = antigo.Flights_DataFrom;
                novo.Flights_DataTo = antigo.Flights_DataTo;
                novo.Flights_nome = antigo.Flights_nome;
                novo.Flights_hora = antigo.Flights_hora;
                novo.Flights_pickUp = antigo.Flights_pickUp;
                novo.Flights_dropOff = antigo.Flights_dropOff;
                novo.Flights_remarks = antigo.Flights_remarks;
                novo.Flights_ConfirmationNbr = antigo.Flights_ConfirmationNbr;
                return novo;
            }
            catch 
            {                
                throw;
            }
        }

    }
}
