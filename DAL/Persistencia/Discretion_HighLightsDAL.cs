﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class Discretion_HighLightsDAL
    {
        private Model Con;

        public Discretion_HighLightsDAL()
        {
            Con = new Model();
        }

        public Discretion_HighLights ObterPorDescQuot(string desc, string quotationcode, int optQuote)
        {
            try
            {
                return Con.Discretion_HighLights.Where(m => m.Descricao == desc && m.Quotation_Code == quotationcode && m.optQuote == optQuote).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Discretion_HighLights> ObterPorCode(string quotationcode, int optQuote)
        {
            try
            {
                return Con.Discretion_HighLights.Where(m => m.Quotation_Code == quotationcode && m.optQuote == optQuote).ToList();
            }
            catch
            {
                throw;
            }
        }

        public Discretion_HighLights Obter(int ID)
        {
            try
            {
                return Con.Discretion_HighLights.Where(m => m.ID == ID).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Discretion_HighLights> ListarPQuotation(string quotationcode, int optQuote)
        {
            try
            {
                return Con.Discretion_HighLights.Where(m => m.Quotation_Code == quotationcode || m.Quotation_Code.Equals("DEFAULT")).Where(x => x.optQuote == optQuote && x.Visible == true).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void InsertDefaults(string quotationCode, int optQuote)
        {
            try
            {
                List<String> listValues = new List<string>();
                listValues.Add("Spending money & tips");
                listValues.Add("Flights & Airport Taxes (quoted separately, except where noted)");
                listValues.Add("Drinks & Meals except where listed");
                listValues.Add("Extra Services, Optional Activities or Changes to your itinerary beyond our control");
                listValues.Add("Passports, Visas & Reciprocity Fees");
                listValues.Add("Baggage, Trip Cancellation & Medical Insurance (Ask your agent for a quote!)");
                               
                foreach (string item in listValues)
                {
                    Discretion_HighLights d = new Discretion_HighLights();
                    d.Visible = true;
                    d.Quotation_Code = quotationCode;
                    d.optQuote = optQuote;
                    d.Descricao = item;
                    Salvar(d);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Salvar(Discretion_HighLights d)
        {
            try
            {
                Con.Discretion_HighLights.Add(d);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Discretion_HighLights d)
        {
            try
            {
                Con.Discretion_HighLights.Remove(d);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        public void Editar(int _id, string _descricao)
        {
            try
            {
                var tbid_old = Con.Discretion_HighLights.Where(a => a.ID == _id).SingleOrDefault();
                tbid_old.Descricao = _descricao;
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        public void ExcluirTodosCode(string code, int optQuote)
        {
            var lst = Con.Discretion_HighLights.Where(a => a.Quotation_Code == code && a.optQuote == optQuote).ToList();

            foreach (var item in lst)
            {
                Con.Discretion_HighLights.Remove(item);
            }

            Con.SaveChanges();
        }
    }
}
