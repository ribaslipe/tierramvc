﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class DescritivoOptionsDAL
    {

        private Model Con;

        public DescritivoOptionsDAL()
        {
            Con = new Model();
        }

        public DescritivoOptions_Pacote ObterPorDecritivo(int DescritivoID)
        {
            try
            {
                return Con.DescritivoOptions_Pacote.Where(s => s.Descritivo_id == DescritivoID).SingleOrDefault();
            }
            catch 
            {
                throw;
            }
        }

        public DescritivoOptions_Pacote ObterPorDecritivo(int DescritivoID, int number)
        {
            try
            {
                return Con.DescritivoOptions_Pacote.Where(s => s.Descritivo_id == DescritivoID && s.DescritivoOptions_number == number).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(DescritivoOptions_Pacote d)
        {
            try
            {
                Con.DescritivoOptions_Pacote.Add(d);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(DescritivoOptions_Pacote novo)
        {
            try
            {
                var antigo = ObterPorDecritivo((int)novo.Descritivo_id);

                antigo.DescritivoOptions_breakfast = novo.DescritivoOptions_breakfast;
                antigo.DescritivoOptions_boxlunch = novo.DescritivoOptions_boxlunch;
                antigo.DescritivoOptions_dinner = novo.DescritivoOptions_dinner;
                antigo.DescritivoOptions_lunch = novo.DescritivoOptions_lunch;                

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
}
