﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class Usuario_DiaSemanaDAL
    {
        Model Con;

        public Usuario_DiaSemanaDAL()
        {
            Con = new Model();
        }

        public Usuarios_DiasSemana ObterPorID(int ID)
        {
            try
            {
                return Con.Usuarios_DiasSemana.Where(a => a.DiasSemana_id == ID).SingleOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Salvar(Usuarios_DiasSemana DiasSemana)
        {
            try
            {
                Con.Usuarios_DiasSemana.Add(DiasSemana);
                Con.SaveChanges();
                return DiasSemana.DiasSemana_id;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public void Atualizar(Usuarios_DiasSemana diasSemana)
        {
            try
            {
                Usuarios_DiasSemana antigo = ObterPorID(diasSemana.DiasSemana_id);



            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public Usuarios_DiasSemana Obter(bool seg, bool ter, bool qua, bool qui, bool sex, bool sab, bool dom)
        {
            try
            {
                return Con.Usuarios_DiasSemana.Where(a => a.DiasSemana_seg == seg &&
                                                            a.DiasSemana_ter == ter &&
                                                            a.DiasSemana_qua == qua &&
                                                            a.DiasSemana_qui == qui &&
                                                            a.DiasSemana_sex == sex &&
                                                            a.DiasSemana_sab == sab &&
                                                            a.DiasSemana_dom == dom).FirstOrDefault();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

    }
}
