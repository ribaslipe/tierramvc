﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class BreakFastDAL
    {

        private Model Con;

        public BreakFastDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Meal m)
        {
            try
            {
                Con.S_Meal.Add(m);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Meal ObterPorId(int IdMeal)
        {
            try
            {
                return Con.S_Meal.Where(m => m.S_meal_id == IdMeal).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Meal> ListarTodos(string nome)
        {
            try
            {
                if (nome.Equals(""))
                {
                    return Con.S_Meal.OrderBy(p => p.S_meal_nome).ToList();
                }
                else
                {
                    return Con.S_Meal.Where(m => m.S_meal_nome.Contains(nome)).OrderBy(p => p.S_meal_nome).ToList();
                }
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Meal> ListarTodos()
        {
            try
            {
                return Con.S_Meal.ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(S_Meal novo)
        {
            try
            {
                S_Meal antigo = ObterPorId(novo.S_meal_id);

                antigo.S_meal_nome = novo.S_meal_nome;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(S_Meal m)
        {
            try
            {
                Con.S_Meal.Remove(m);
                Con.SaveChanges();
            }
            catch
            {                
                throw;
            }
        }

        public bool VerificaNome(string Nome)
        {
            try
            {
                return Con.S_Meal.Where(s => s.S_meal_nome.Equals(Nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

    }
}
