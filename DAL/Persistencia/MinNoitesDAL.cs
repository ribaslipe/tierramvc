﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class MinNoitesDAL
    {
        private Model Con;

        public MinNoitesDAL()
        {
            Con = new Model();
        }

        public List<MinimoNoites> ListarTodos()
        {
            try
            {
                return Con.MinimoNoites.ToList();
            }
            catch 
            {
                throw;
            }
        }
    }
}
