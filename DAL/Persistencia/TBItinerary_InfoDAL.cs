﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class TBItinerary_InfoDAL
    {
        private Model Con;

        public TBItinerary_InfoDAL()
        {
            Con = new Model();
        }

        public void Atualizar(TBItinerary_Info t)
        {
            try
            {
                TBItinerary_Info antigo = ObterPorId(t.ID);

                antigo.Note = t.Note;
                antigo.PrecoAlterado = t.PrecoAlterado;
                antigo.Descricao = t.Descricao;
                antigo.ImagemPrincipal = t.ImagemPrincipal;
                antigo.IdTabela = t.IdTabela;
                antigo.Quotation_Code = t.Quotation_Code;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public TBItinerary_Info ObterPorId(int IdTbi)
        {
            try
            {
                return Con.TBItinerary_Info.Where(m => m.ID == IdTbi).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }
        public TBItinerary_Info ObterPorQuotationIdTabela(int IdTabela, string quotationcode, int optQuote)
        {
            try
            {
                return Con.TBItinerary_Info.Where(m => m.IdTabela == IdTabela && m.Quotation_Code == quotationcode && m.optQuote == optQuote).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }
        public List<TBItinerary_Info> ObterPorQuote(string quotationcode, int optQuote)
        {
            try
            {
                return Con.TBItinerary_Info.Where(m => m.Quotation_Code == quotationcode && m.optQuote == optQuote).ToList();
            }
            catch
            {
                throw;
            }
        }
        public void Salvar(TBItinerary_Info t)
        {
            try
            {
                Con.TBItinerary_Info.Add(t);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(TBItinerary_Info t)
        {
            try
            {
                Con.TBItinerary_Info.Remove(t);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
