﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class FileTransfersDAL
    {

        private Model Con;

        public FileTransfersDAL()
        {
            Con = new Model();
        }

        public List<File_Transfers> ListarTodos_Spp(int IdSupplier)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Transfers> ListarTodosNew(int IdFile)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.File_id == IdFile && f.Range_id == null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Transfers> ListarTodos(int IdFile)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.File_id == IdFile &&
                                                     f.Range_id == null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Transfers> ListarTodosRangeZero(int IdFile)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.File_id == IdFile &&
                                                     f.Range_id != null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Transfers> ListarTodosOpt(int IdFile, int OptQuote)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.File_id == IdFile &&
                                                     f.OptQuote == OptQuote).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Transfers> ListarTodosOptional(int IdFile, int OptQuote)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.File_id == IdFile &&
                                                     f.OptQuote == OptQuote &&
                                                     f.TOptional == true).ToList();
            }
            catch
            {
                throw;
            }
        }


        public List<File_Transfers> ListarTodosPorOrdem(int IdFile, int Ordem)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.File_id == IdFile &&
                                                     f.Ordem >= Ordem).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Transfers> ListarTodos(int IdFile, int IdSupplier)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.File_id == IdFile &&
                                                   f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Transfers> ListarTodos(string trfNome)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.Transf_nome.Equals(trfNome)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public File_Transfers ListarPor_Cidade(string trfNome, int IdCId)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.Transf_nome.Equals(trfNome) &&
                                                     f.Trf_CID_id == IdCId).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSuppliers(int IdFile)
        {
            try
            {
                return (from flt in Con.File_Transfers
                        where flt.File_id == IdFile
                        select new
                        {
                            flt.S_nome
                        }).ToList().Distinct();
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(File_Transfers f)
        {
            try
            {
                Con.File_Transfers.Add(f);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public File_Transfers ObterPorId(int IdFileTransf)
        {
            try
            {
                return Con.File_Transfers.Where(f => f.File_Transf_id == IdFileTransf).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(File_Transfers f)
        {
            try
            {
                Con.File_Transfers.Remove(f);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarSupplier(File_Transfers novo)
        {
            try
            {

                File_Transfers ftrnew = ObterPorId(novo.File_Transf_id);

                ftrnew.S_nome = novo.S_nome;
                ftrnew.S_id = novo.S_id;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Qtd_Transf = novo.Qtd_Transf;
                antigo.markup = novo.markup;
                antigo.markupNet = novo.markupNet;
                antigo.descontoNet = novo.descontoNet;
                antigo.desconto = novo.desconto;
                antigo.Transf_valor = novo.Transf_valor;
                antigo.Transf_venda = novo.Transf_venda;
                antigo.Transf_vendaNet = novo.Transf_vendaNet;
                antigo.Transf_total = novo.Transf_total;
                antigo.UpdVenda = novo.UpdVenda;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarVenda(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.markup = novo.markup;
                antigo.Transf_venda = novo.Transf_venda;
                antigo.Transf_total = novo.Transf_total;
                antigo.UpdVenda = true;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarVendaTotal(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Transf_venda = novo.Transf_venda;
                //antigo.Transf_vendaNet = novo.Transf_vendaNet;
                antigo.Transf_total = novo.Transf_total;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarStatus(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Status = novo.Status;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarBulk(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.TBulk = novo.TBulk;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOptional(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.TOptional = novo.TOptional;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarFlight(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Flights_id = novo.Flights_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarPaxs(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Qtd_Transf = novo.Qtd_Transf;
                antigo.Paxs = novo.Paxs;
                antigo.Paying_Pax = novo.Paying_Pax;
                antigo.Supp_Paying_Pax = novo.Supp_Paying_Pax;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarValorTotal(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Transf_valor = novo.Transf_valor;
                antigo.Transf_venda = novo.Transf_venda;
                antigo.Transf_total = novo.Transf_total;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOrdem(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Ordem = novo.Ordem;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNome(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Transf_nome = novo.Transf_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNomeCidade(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Transf_nome = novo.Transf_nome;
                antigo.Trf_CID_id = novo.Trf_CID_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarDatas(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Data_From = novo.Data_From;
                antigo.Data_To = novo.Data_To;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem(int IdFile)
        {
            try
            {
                var item = Con.File_Transfers.Where(f => f != null && f.File_id == IdFile).Max(f => f.Ordem);

                if (item != null)
                {
                    return Convert.ToInt32(item);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem_(int IdFile)
        {
            try
            {
                var item = Con.File_Transfers.Where(f => f != null && f.File_id == IdFile).OrderByDescending(s => s.Ordem).FirstOrDefault();

                if (item != null)
                {
                    return Convert.ToInt32(item.Ordem);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }

        public void AtualizarRange(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Range_id = novo.Range_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

       

        public void AtualizarNew(File_Transfers novo)
        {
            try
            {
                File_Transfers antigo = ObterPorId(novo.File_Transf_id);

                antigo.Qtd_Transf = novo.Qtd_Transf;
                antigo.Paying_Pax = novo.Paying_Pax;
                antigo.Supp_Paying_Pax = novo.Supp_Paying_Pax;
                antigo.Paxs = novo.Paxs;               

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
      
        public double MontaCarrinhoNewFile(int Code, int OptQuote)
        {
            try
            {
                var q = ((from fcr in Con.File_Carrinho                          
                          join fts in Con.File_Tarifas on fcr.File_id equals fts.File_id
                          join rgn in Con.Ranges on fts.File_Tarifas_id equals rgn.FileTabelaId
                          where fcr.File_id == Code &&
                                rgn.Flag.Equals("hotel") &&
                                rgn.OptQuote == OptQuote
                          select new
                          {
                              dtFrom = (DateTime)fts.Data_From,
                              dtTo = (DateTime)fts.Data_To
                          }).Distinct().ToList())
                        .Union
                        ((from fcr in Con.File_Carrinho                         
                          join fts in Con.File_Transfers on fcr.File_id equals fts.File_id
                          join rgn in Con.Ranges on fts.File_Transf_id equals rgn.FileTabelaId
                          where fcr.File_id == Code &&
                                rgn.Flag.Equals("servico") &&
                                rgn.OptQuote == OptQuote
                          select new
                          {
                              dtFrom = (DateTime)fts.Data_From,
                              dtTo = (DateTime)fts.Data_To
                          }).Distinct().ToList())
                          .Union
                          ((from fcr in Con.File_Carrinho                            
                            join fse in Con.File_ServExtra on fcr.File_id equals fse.File_id
                            join rgn in Con.Ranges on fse.File_ServExtra_id equals rgn.FileTabelaId
                            where fcr.File_id == Code &&
                                  rgn.Flag.Equals("extra") &&
                                  rgn.OptQuote == OptQuote
                            select new
                            {
                                dtFrom = (DateTime)fse.Data_From,
                                dtTo = (DateTime)fse.Data_To
                            }).Distinct().ToList()).OrderBy(a => a.dtFrom).ThenBy(a => a.dtTo).ToList();

                DateTime dt = (DateTime)q.First().dtFrom;
                DateTime d2 = (DateTime)q.OrderByDescending(a => a.dtFrom).ThenByDescending(b => b.dtTo).First().dtTo;

                return (d2 - dt).TotalDays;
            }
            catch
            {
                throw;
            }
        }

    }
}
