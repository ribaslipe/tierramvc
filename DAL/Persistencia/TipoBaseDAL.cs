﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class TipoBaseDAL
    {

        private Model Con;

        public TipoBaseDAL()
        {
            Con = new Model();
        }

        public List<Tipo_Base> ListarTodos()
        {
            try
            {
                return Con.Tipo_Base.OrderBy(t => t.TipoBase_nome).ToList();
            }
            catch 
            {
                throw;
            }
        }

    }
}
