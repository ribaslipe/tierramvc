﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;

namespace DAL.Persistencia
{
    public class CategoriaDAL
    {
        private Model Con;                 

        public CategoriaDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Categoria c)
        {
            try
            {

                //Rotinas prontas para o banco de dados
                Con.S_Categoria.Add(c); //insert into S_Categoria values...
                Con.SaveChanges(); //Executar a operação

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Categoria c)
        {
            try
            {
                Con.S_Categoria.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        public List<S_Categoria> ListarTodos()
        {
            try
            {
                //SQL -> select * from S_Categoria
                return Con.S_Categoria.OrderBy(s => s.Categ_classificacao).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Categoria> ListarTodos(string nome)
        {
            try
            {
                if (nome.Equals(""))
                {
                    return Con.S_Categoria.ToList();
                }
                else
                {
                    return Con.S_Categoria.Where(c => c.Categ_classificacao.Contains(nome)).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public S_Categoria ListarTodos(Int32 SupplierID)
        {
            try
            {
                var query = (from c in Con.Supplier
                            where c.S_id == SupplierID
                            select c.Categ_id).Single();

                return Con.S_Categoria.Where(sc => sc.Categ_id == query).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_Categoria ObterPorSupp(int IdSupplier)
        {
            try
            {
                return (from spp in Con.Supplier
                        join ctg in Con.S_Categoria on spp.Categ_id equals ctg.Categ_id
                        where spp.S_id == IdSupplier
                        select ctg).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Categoria ObterPorId(int IdCategoria)
        {
            try
            {
                //select * from TbCliente where IdCliente = ?
                return Con.S_Categoria.Where(c => c.Categ_id == IdCategoria).Single();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(S_Categoria registroNovo)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                S_Categoria registroAntigo = ObterPorId(registroNovo.Categ_id);

                //Atualizando os dados
                registroAntigo.Categ_classificacao = registroNovo.Categ_classificacao;
                registroAntigo.Categ_ordem = registroNovo.Categ_ordem;
                registroAntigo.Categ_imgmemo = registroNovo.Categ_imgmemo;
                registroAntigo.Categ_imgnome = registroNovo.Categ_imgnome;

                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaAgregado(int IdCategoria)
        {
            try
            {
                return Con.Supplier.Any(c => c.Categ_id == IdCategoria);
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExistente(string nome)
        {
            try
            {

                return Con.S_Categoria.Where(t => t.Categ_classificacao.Equals(nome)).Count() != 0;

            }
            catch
            {
                throw;
            }
        }

    }
}
