﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class CambioDAL
    {

        private Model Con;

        public CambioDAL()
        {
            Con = new Model();
        }

        //public Cambio ObterPorIdMoedaPara_OMNI(int IdMoedaDe, int IdMoedaPara)
        //{
        //    try
        //    {
        //        return Con.Cambio.Where(m => m.MoedaID_Para == IdMoedaPara 
        //                                  && m.MoedaID_De == IdMoedaDe
        //                                  && m.Cambio_Tipo.nome.Equals("Cambio OMNI")).OrderByDescending(m => m.data).FirstOrDefault();
               
        //        //return Con.Cambio.Where(m => m.MoedaID_Para == IdMoedaPara && m.MoedaID_De == IdMoedaDe).OrderByDescending(m => m.data).FirstOrDefault();
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public Cambio ObterPorIdMoedaPara(int IdMoedaDe, int IdMoedaPara)
        {
            try
            {
                return Con.Cambio.Where(m => m.MoedaID_Para == IdMoedaPara && m.MoedaID_De == IdMoedaDe).OrderByDescending(m => m.data).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }


        public Cambio ObterPorId(int id)
        {
            try
            {
                return Con.Cambio.Where(m => m.Cambio_id == id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Cambio ObterPorIdMoedaParaFirst(int IdMoedaDe, int IdMoedaPara)
        {
            try
            {
                return Con.Cambio.Where(m => m.MoedaID_Para == IdMoedaPara && m.MoedaID_De == IdMoedaDe).OrderByDescending(m => m.data).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Cambio ObterPorIdMoedaParaFirst(int IdMoedaDe, int IdMoedaPara, string type)
        {
            try
            {
                return (from cbo in Con.Cambio
                        join cbt in Con.Cambio_Tipo on cbo.tipoCambio equals cbt.id
                        where cbt.nome.Equals(type)
                        select cbo).OrderByDescending(m => m.data).FirstOrDefault();

            }
            catch
            {
                throw;
            }
        }





        public List<Cambio> pesquisarTodos()
        {
            try
            {
                return Con.Cambio.OrderByDescending(c => c.data).ToList();
            }
            catch
            {
                throw;
            }
        }


        public void incluir(Cambio c)
        {

            try
            {
                Con.Cambio.Add(c);
                Con.SaveChanges();
            }
            catch (Exception)
            {
                
                throw;
            }

        }

        public void excluir(Cambio c)
        {

            try
            {
                Con.Cambio.Remove(c);
                Con.SaveChanges();

            }
            catch (Exception)
            {
                
                throw;
            }

        }


        public void alterar(Cambio novo)
        {


            try
            {

                Cambio velho = ObterPorId(novo.Cambio_id);

                velho.data = novo.data;
                velho.MoedaID_De = novo.MoedaID_De;
                velho.MoedaID_Para = novo.MoedaID_Para;
                velho.tipoCambio = novo.tipoCambio;
                velho.Cambio1 = novo.Cambio1;

                Con.SaveChanges();

            }
            catch (Exception)
            {
                
                throw;
            }

        }

        public Cambio Obter_CambioTroca(DateTime data)
        {
            try
            {

                return (from cmb in Con.Cambio
                        join ctp in Con.Cambio_Tipo on cmb.tipoCambio equals ctp.id
                        where cmb.MoedaID_De == 1 &&
                              cmb.MoedaID_Para == 2 &&
                              cmb.data >= data &&
                              ctp.nome.Equals("Cambio TROCA")
                              ||
                              cmb.MoedaID_De == 1 &&
                              cmb.MoedaID_Para == 2 &&
                              cmb.data <= data &&
                              ctp.nome.Equals("Cambio TROCA")
                        select cmb).OrderByDescending(s => s.data).FirstOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public Cambio Obter_CambioTroca_(DateTime data)
        {
            try
            {
                #region query
                //SELECT        TOP (1) Cambio.MoedaID_De, Cambio.MoedaID_Para, Cambio.Cambio, Cambio.data, Cambio.tipoCambio, Cambio_Tipo.nome
                //FROM            Cambio INNER JOIN
                //Cambio_Tipo ON Cambio.tipoCambio = Cambio_Tipo.id
                //WHERE        (Cambio.MoedaID_De = 1) AND (Cambio.MoedaID_Para = 2) AND (Cambio.data >= '20160212') AND (Cambio_Tipo.nome = 'Cambio TROCA')
                //ORDER BY Cambio.data
                #endregion

                return (from cmb in Con.Cambio
                        join ctp in Con.Cambio_Tipo on cmb.tipoCambio equals ctp.id
                        where cmb.MoedaID_De == 1 &&
                              cmb.MoedaID_Para == 2 &&
                              cmb.data >= data &&
                              ctp.nome.Equals("Cambio TROCA")                             
                        select cmb).OrderBy(s => s.data).FirstOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public Cambio Obter_CambioTroca_DESC(DateTime data)
        {
            try
            {
                #region query
                //SELECT        TOP (1) Cambio.MoedaID_De, Cambio.MoedaID_Para, Cambio.Cambio, Cambio.data, Cambio.tipoCambio, Cambio_Tipo.nome
                //FROM            Cambio INNER JOIN
                //Cambio_Tipo ON Cambio.tipoCambio = Cambio_Tipo.id
                //WHERE        (Cambio.MoedaID_De = 1) AND (Cambio.MoedaID_Para = 2) AND (Cambio.data >= '20160212') AND (Cambio_Tipo.nome = 'Cambio TROCA')
                //ORDER BY Cambio.data
                #endregion

                return (from cmb in Con.Cambio
                        join ctp in Con.Cambio_Tipo on cmb.tipoCambio equals ctp.id
                        where cmb.MoedaID_De == 1 &&
                              cmb.MoedaID_Para == 2 &&
                              cmb.data <= data &&
                              ctp.nome.Equals("Cambio TROCA")
                        select cmb).OrderByDescending(s => s.data).FirstOrDefault();

            }
            catch
            {
                throw;
            }
        }

    }
}
