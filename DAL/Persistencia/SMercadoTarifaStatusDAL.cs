﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class SMercadoTarifaStatusDAL
    {

        private Model Con;

        public SMercadoTarifaStatusDAL()
        {
            Con = new Model();
        }


        public List<S_Mercado_Tarifa_Status> ListarTodos()
        {
            try
            {
                return Con.S_Mercado_Tarifa_Status.OrderBy(s => s.S_mercado_tarifa_status_nome).ToList();
            }
            catch 
            {                
                throw;
            }
        }
    }
}
