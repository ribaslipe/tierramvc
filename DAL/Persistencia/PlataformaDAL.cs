﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class PlataformaDAL
    {

        private Model Con;

        public PlataformaDAL()
        {
            Con = new Model();
        }


        public List<Plataforma_XML> ListarTodos()
        {
            try
            {
                return Con.Plataforma_XML.OrderBy(s => s.Plataforma_nome).ToList();
            }
            catch
            {                
                throw;
            }
        }

        public Plataforma_XML ObterPorId(int Id)
        {
            try
            {
                return Con.Plataforma_XML.Where(s => s.Plataforma_id == Id).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Salvar(Plataforma_XML p)
        {
            try
            {
                Con.Plataforma_XML.Add(p);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(Plataforma_XML p)
        {
            try
            {
                Plataforma_XML a = ObterPorId(p.Plataforma_id);

                a.Plataforma_nome = p.Plataforma_nome;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(Plataforma_XML p)
        {
            try
            {
                Con.Plataforma_XML.Remove(p);
                Con.SaveChanges();
            }
            catch
            {                
                throw;
            }
        }

    }
}
