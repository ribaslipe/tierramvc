﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;


namespace DAL.Persistencia
{
    public class BaseTarifariaDAL
    {

        private Model Con;

        public BaseTarifariaDAL()
        {
            Con = new Model();
        }

        public void Salvar(BaseTarifaria m)
        {
            try
            {
                Con.BaseTarifaria.Add(m);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public BaseTarifaria ObterPorId(int IdBase)
        {
            try
            {
                return Con.BaseTarifaria.Where(p => p.BaseTarifaria_id == IdBase).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(BaseTarifaria p)
        {
            try
            {
                Con.BaseTarifaria.Remove(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(BaseTarifaria novo)
        {
            try
            {
                BaseTarifaria antigo = ObterPorId(novo.BaseTarifaria_id);

                antigo.BaseTarifaria_nome = novo.BaseTarifaria_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<BaseTarifaria> ListarTodos()
        {
            try
            {
                return Con.BaseTarifaria.OrderBy(p => p.BaseTarifaria_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<BaseTarifaria> ListarTodos(string nome)
        {
            try
            {
                if (nome.Equals(""))
                {
                    return Con.BaseTarifaria.OrderBy(p => p.BaseTarifaria_nome).OrderBy(p => p.BaseTarifaria_nome).ToList();
                }
                else
                {
                    return Con.BaseTarifaria.Where(p => p.BaseTarifaria_nome.Contains(nome)).OrderBy(p => p.BaseTarifaria_nome).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public bool ObterPorNome(string nome)
        {
            try
            {
                return Con.BaseTarifaria.Where(s => s.BaseTarifaria_nome.Equals(nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public BaseTarifaria ObterPorNome_model(string nome)
        {
            try
            {
                return Con.BaseTarifaria.Where(s => s.BaseTarifaria_nome.Equals(nome)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
