﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class RedeDAL
    {

        private Model Con;

        public RedeDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Rede p)
        {
            try
            {
                Con.S_Rede.Add(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public S_Rede ObterPorId(int IdRede)
        {
            try
            {
                return Con.S_Rede.Where(p => p.Rede_id == IdRede).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Rede p)
        {
            try
            {
                Con.S_Rede.Remove(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(S_Rede novo)
        {
            try
            {
                S_Rede antigo = ObterPorId(novo.Rede_id);

                antigo.Rede_nome = novo.Rede_nome;
                antigo.Rede_endereco = novo.Rede_endereco;
                antigo.Rede_cnpj = novo.Rede_cnpj;
                antigo.Rede_telefone = novo.Rede_telefone;
                antigo.Rede_email = novo.Rede_email;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Rede> ListarTodos()
        {
            try
            {
                return Con.S_Rede.OrderBy(p => p.Rede_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Rede> ListarTodos(string nome)
        {
            try
            {
                if (nome.Equals(""))
                {
                    return Con.S_Rede.OrderBy(p => p.Rede_nome).OrderBy(p => p.Rede_nome).ToList();
                }
                else
                {
                    return Con.S_Rede.Where(p => p.Rede_nome.Contains(nome)).OrderBy(p => p.Rede_nome).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public bool ObterPorNome(string nome)
        {
            try
            {
                return Con.S_Rede.Where(s => s.Rede_nome.Equals(nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public S_Rede ObterPorNome_model(string nome)
        {
            try
            {
                return Con.S_Rede.Where(s => s.Rede_nome.Equals(nome)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
