﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class MercadoDAL
    {

        private Model Con;

        public MercadoDAL()
        {
            Con = new Model();
        }

        public void Salvar(Mercado m)
        {
            try
            {
                Con.Mercado.Add(m);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Mercado ObterPorId(int IdMercado)
        {
            try
            {
                return Con.Mercado.Where(p => p.Mercado_id == IdMercado).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Mercado p)
        {
            try
            {
                Con.Mercado.Remove(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Mercado novo)
        {
            try
            {
                Mercado antigo = ObterPorId(novo.Mercado_id);

                antigo.Mercado_nome = novo.Mercado_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Mercado> ListarTodos()
        {
            try
            {
                return Con.Mercado.OrderBy(p => p.Mercado_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Mercado> ListarTodos(string nome)
        {
            try
            {
                if (nome.Equals(""))
                {
                    return Con.Mercado.OrderBy(p => p.Mercado_nome).OrderBy(p => p.Mercado_nome).ToList();
                }
                else
                {
                    return Con.Mercado.Where(m => m.Mercado_nome.Contains(nome)).OrderBy(p => p.Mercado_nome).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public bool ObterPorNome(string nome)
        {
            try
            {
                return Con.Mercado.Where(s => s.Mercado_nome.Equals(nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public Mercado ObterPorNome_model(string nome)
        {
            try
            {
                return Con.Mercado.Where(s => s.Mercado_nome.Equals(nome)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
