﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class CondPagDAL
    {
        private Model Con;

        public CondPagDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Cond_Pagamento s)
        {
            try
            {
                Con.S_Cond_Pagamento.Add(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Cond_Pagamento ObterPorId(int IdCond)
        {
            try
            {
                return Con.S_Cond_Pagamento.Where(s => s.CondPag_id == IdCond).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Cond_Pagamento> ListarTodos(int IdSupplier)
        {
            try
            {
                return Con.S_Cond_Pagamento.Where(s => s.S_id == IdSupplier).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(S_Cond_Pagamento novo)
        {
            try
            {
                S_Cond_Pagamento antigo = ObterPorId(novo.CondPag_id);

                antigo.CondPag_fontecambio = novo.CondPag_fontecambio;
                antigo.CondPag_tipocambio = novo.CondPag_tipocambio;
                antigo.CondPag_datatroca = novo.CondPag_datatroca;
                antigo.CondPag_prazofaturamento = novo.CondPag_prazofaturamento;
                antigo.CondPag_banco = novo.CondPag_banco;
                antigo.CondPag_agencia = novo.CondPag_agencia;
                antigo.CondPag_conta = novo.CondPag_conta;
                antigo.S_id = novo.S_id;

                Con.SaveChanges();

            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(S_Cond_Pagamento s)
        {
            try
            {
                Con.S_Cond_Pagamento.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Cond_Pagamento ObterPorSupp(int idSupp)
        {
            try
            {
                return Con.S_Cond_Pagamento.Where(s => s.S_id == idSupp).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
