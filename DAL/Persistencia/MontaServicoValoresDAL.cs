﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class MontaServicoValoresDAL
    {

        private Model Con;

        public MontaServicoValoresDAL()
        {
            Con = new Model();
        }

        public void Salvar(Monta_Servico_Valores m)
        {
            try
            {
                Con.Monta_Servico_Valores.Add(m);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public List<Monta_Servico_Valores> ListaTodos(int IdMServico)
        {
            try
            {
                return Con.Monta_Servico_Valores.Where(m => m.IdMServico == IdMServico).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public Monta_Servico_Valores ObterPorId(int IdMServicoValores)
        {
            try
            {
                return Con.Monta_Servico_Valores.Where(m => m.IdMServicoValores == IdMServicoValores).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(Monta_Servico_Valores novo)
        {
            try
            {
                Monta_Servico_Valores antigo = ObterPorId(novo.IdMServicoValores);

                antigo.MServico_Valores_Bulk_Venda = novo.MServico_Valores_Bulk_Venda;
                antigo.MServico_Valores_Bulk = novo.MServico_Valores_Bulk;
                antigo.MServico_Valores_Bulk_Total = novo.MServico_Valores_Bulk_Total;
                antigo.S_mercado_tarifa_status_id = novo.S_mercado_tarifa_status_id;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Monta_Servico_Valores ObterPorIdMServico(int IdMServico)
        {
            try
            {
                return Con.Monta_Servico_Valores.Where(m => m.IdMServico == IdMServico).First();
            }
            catch 
            {                
                throw;
            }
        }

        public Monta_Servico_Valores ObterPorIdBase(int IdBase, int IdMServico)
        {
            try
            {
                return Con.Monta_Servico_Valores.Where(m => m.Base_id == IdBase &&
                                                            m.IdMServico == IdMServico).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }      

    }
}
