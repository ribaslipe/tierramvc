﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class TipoRoomDAL
    {

        private Model Con;

        public TipoRoomDAL()
        {
            Con = new Model();
        }

        public List<Tipo_Room> ListarTodosSupplier(int IdSupl)
        {
            try
            {
                return (from sm in Con.S_Mercado_Tarifa
                        join sp in Con.S_Mercado_Periodo on sm.S_merc_periodo_id equals sp.S_merc_periodo_id
                        join tr in Con.Tipo_Room on sm.Tipo_room_id equals tr.Tipo_room_id
                        where sp.S_id == IdSupl
                        select tr).OrderBy(t => t.Tipo_room_id).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Tipo_Room> ListarTodos()
        {
            try
            {
                return Con.Tipo_Room.ToList();
            }
            catch
            {
                throw;
            }
        }

        public Tipo_Room ObterPorId(int IdTipoRoom)
        {
            try
            {
                return Con.Tipo_Room.SingleOrDefault(t => t.Tipo_room_id == IdTipoRoom);
            }
            catch
            {
                throw;
            }
        }

        public Tipo_Room ObterPorNome(string nome)
        {
            try
            {
                return Con.Tipo_Room.SingleOrDefault(c => c.Tipo_room_nome.Equals(nome));
            }
            catch
            {
                throw;
            }
        }

        public Tipo_Room ObterPorRoomNumber(int number)
        {
            try
            {
                return Con.Tipo_Room.FirstOrDefault(x => x.Tipo_room_number == number);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
