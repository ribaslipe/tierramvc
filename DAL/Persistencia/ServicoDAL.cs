﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class ServicoDAL
    {

        private Model Con;

        public ServicoDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Servicos s)
        {
            try
            {
                Con.S_Servicos.Add(s);
                Con.SaveChanges();
            }
            catch 
            {
                throw;
            }
        }

        public void Atualizar(S_Servicos novo)
        {
            S_Servicos antigo = ObterPorId(novo.Servicos_Id);

            antigo.Servicos_Nome = novo.Servicos_Nome;
            antigo.Servicos_descr = novo.Servicos_descr;
            antigo.Servicos_descrCurt = novo.Servicos_descrCurt;
            antigo.Servicos_descrRemarks = novo.Servicos_descrRemarks;
            antigo.Servicos_descrVoucher = novo.Servicos_descrVoucher;
            antigo.Servicos_descrCliente = novo.Servicos_descrCliente;
            antigo.Servicos_transfer = novo.Servicos_transfer;
            antigo.STATUS_id = novo.STATUS_id;
            antigo.Tipo_Id = novo.Tipo_Id;
            antigo.Tipo_categ_id = novo.Tipo_categ_id;
            antigo.Cid_Id = novo.Cid_Id;
            antigo.Pais_Id = novo.Pais_Id;
            antigo.SIB = novo.SIB;
            antigo.Recommended = novo.Recommended;

            Con.SaveChanges();
        }

        public void Atualizar_NoDescr(S_Servicos novo)
        {
            S_Servicos antigo = ObterPorId(novo.Servicos_Id);

            antigo.Servicos_Nome = novo.Servicos_Nome;           
            antigo.Servicos_transfer = novo.Servicos_transfer;
            antigo.STATUS_id = novo.STATUS_id;
            antigo.Tipo_Id = novo.Tipo_Id;
            antigo.Tipo_categ_id = novo.Tipo_categ_id;
            antigo.Cid_Id = novo.Cid_Id;
            antigo.Pais_Id = novo.Pais_Id;
            antigo.SIB = novo.SIB;
            antigo.Recommended = novo.Recommended;

            Con.SaveChanges();
        }

        public void Excluir(S_Servicos s)
        {
            try
            {
                Con.S_Servicos.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {
                throw;
            }
        }

        public List<S_Servicos> ObterPorNomeLista(string nome)
        {
            try
            {
                return Con.S_Servicos.Where(s => s.Servicos_Nome.Equals(nome)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public S_Servicos ObterPorNome(string nome)
        {
            try
            {
                var query = (from s in Con.S_Servicos
                             join a in Con.S_Status on s.STATUS_id equals a.STATUS_id
                             join t in Con.S_Servicos_Tipo on s.Tipo_Id equals t.Tipo_Id
                             join c in Con.Cidade on s.Cid_Id equals c.CID_id
                             join p in Con.Pais on s.Pais_Id equals p.PAIS_id
                             where s.Servicos_Nome.Equals(nome)
                             select s).SingleOrDefault();

                return query;
            }
            catch
            {
                throw;
            }
        }

        public S_Servicos ObterPorNome(string nome, int IdCidade)
        {
            try
            {
                var query = (from s in Con.S_Servicos
                             join a in Con.S_Status on s.STATUS_id equals a.STATUS_id
                             join t in Con.S_Servicos_Tipo on s.Tipo_Id equals t.Tipo_Id
                             join c in Con.Cidade on s.Cid_Id equals c.CID_id
                             join p in Con.Pais on s.Pais_Id equals p.PAIS_id
                             where s.Servicos_Nome.Equals(nome) &&
                                   c.CID_id == IdCidade
                             select s).SingleOrDefault();

                return query;
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExiste(string nome, int IdCidade, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return Con.S_Servicos.Where(s => s.Servicos_Nome.Equals(nome) && s.Cid_Id == IdCidade && s.Servicos_Id != IdServico).Count() != 0;
                }
                else
                {
                    return Con.S_Servicos.Where(s => s.Servicos_Nome.Equals(nome) && s.Cid_Id == IdCidade).Count() != 0;
                }
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaExiste(string nome, int IdServico)
        {
            try
            {
                if (IdServico != 0)
                {
                    return Con.S_Servicos.Where(s => s.Servicos_Nome.Equals(nome) && s.Servicos_Id != IdServico).Count() != 0;
                }
                else
                {
                    return Con.S_Servicos.Where(s => s.Servicos_Nome.Equals(nome)).Count() != 0;
                }
            }
            catch
            {
                throw;
            }
        }

        public S_Servicos ObterPorId(int IdServico)
        {
            try
            {
                var query = (from s in Con.S_Servicos
                             join a in Con.S_Status on s.STATUS_id equals a.STATUS_id
                             join t in Con.S_Servicos_Tipo on s.Tipo_Id equals t.Tipo_Id
                             join c in Con.Cidade on s.Cid_Id equals c.CID_id
                             join p in Con.Pais on s.Pais_Id equals p.PAIS_id
                             where s.Servicos_Id == IdServico
                             select s).SingleOrDefault();

                return query;
            }
            catch 
            {
                throw;
            }
        }

        public List<S_Servicos> ListarTodosTrf(string nome)
        {
            try
            {
                return Con.S_Servicos.Where(s => s.Servicos_Nome.Contains(nome) &&
                                                 s.Servicos_transfer.Equals("S") &&
                                                 s.STATUS_id == 1).OrderBy(s => s.Servicos_Nome).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Servicos> ListarTodosTrf(string nome, int IdCidade)
        {
            try
            {
                return Con.S_Servicos.Where(s => s.Servicos_Nome.Contains(nome) &&
                                                 s.Cid_Id == IdCidade &&
                                                 s.Servicos_transfer.Equals("S") &&
                                                 s.STATUS_id == 1).OrderBy(s => s.Servicos_Nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Servicos> ListarTodosTor(string nome)
        {
            try
            {
                return Con.S_Servicos.Where(s => s.Servicos_Nome.Contains(nome) &&
                                                 s.Servicos_transfer == null &&
                                                 s.STATUS_id == 1).OrderBy(s => s.Servicos_Nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Servicos> ListarTodosTor(string nome, int IdCidade)
        {
            try
            {
                return Con.S_Servicos.Where(s => s.Servicos_Nome.Contains(nome) &&
                                                 s.Cid_Id == IdCidade &&
                                                 s.Servicos_transfer == null &&
                                                 s.STATUS_id == 1).OrderBy(s => s.Servicos_Nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<string> ListarServicos_Filtro(string prefixText, int idCidade, int idTipoServico, int idTipoCategServico, int idStatus)
        {
            try
            {

                List<S_Servicos> s = new List<S_Servicos>();

                if (idTipoServico > 0 && idStatus > 0 && idTipoCategServico > 0 && idCidade > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.Where(p => p.Tipo_Id == idTipoServico
                                            && p.STATUS_id == idStatus
                                            && p.Tipo_categ_id == idTipoCategServico
                                            && p.Cid_Id == idCidade).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText)
                                            && p.Tipo_Id == idTipoServico
                                            && p.STATUS_id == idStatus
                                            && p.Tipo_categ_id == idTipoCategServico
                                            && p.Cid_Id == idCidade).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                }
                else if (idCidade > 0 && idTipoServico > 0 && idTipoCategServico > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.Where(p => p.Tipo_Id == idTipoServico && p.Cid_Id == idCidade && p.Tipo_categ_id == idTipoCategServico).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText) && p.Tipo_Id == idTipoServico && p.Cid_Id == idCidade && p.Tipo_categ_id == idTipoCategServico).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                }
                else if (idTipoServico > 0 && idStatus > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.Where(p => p.Tipo_Id == idTipoServico && p.STATUS_id == idStatus).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText) && p.Tipo_Id == idTipoServico && p.STATUS_id == idStatus).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                }
                else if (idTipoServico > 0 && idCidade > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.Where(p => p.Tipo_Id == idTipoServico && p.Cid_Id == idCidade).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText) && p.Tipo_Id == idTipoServico && p.Cid_Id == idCidade).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                }
                else if (idStatus > 0 && idCidade > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.Where(p => p.STATUS_id == idStatus && p.Cid_Id == idCidade).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText) && p.STATUS_id == idStatus && p.Cid_Id == idCidade).OrderBy(p => p.Servicos_Nome).ToList();
                    }
                }
                else if (idTipoServico > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.Where(p => p.Tipo_Id == idTipoServico).OrderBy(z => z.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText) && p.Tipo_Id == idTipoServico).OrderBy(z => z.Servicos_Nome).ToList();
                    }
                }
                else if (idCidade > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.Where(p => p.Cid_Id == idCidade).OrderBy(z => z.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText) && p.Cid_Id == idCidade).OrderBy(z => z.Servicos_Nome).ToList();
                    }
                }
                else if (idStatus > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.Where(p => p.STATUS_id == idStatus).OrderBy(z => z.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText) && p.STATUS_id == idStatus).OrderBy(z => z.Servicos_Nome).ToList();
                    }
                }
                else
                {
                    if (prefixText.Equals("*"))
                    {
                        s = Con.S_Servicos.OrderBy(z => z.Servicos_Nome).ToList();
                    }
                    else
                    {
                        s = Con.S_Servicos.Where(p => p.Servicos_Nome.Contains(prefixText)).OrderBy(z => z.Servicos_Nome).ToList();
                    }
                }



                List<string> lista = new List<string>();

                if (s.Count == 0)
                {
                    lista.Add("Não existe serviço cadastrado com esse nome.");

                    return lista;
                }

                foreach (S_Servicos p in s)
                {
                    lista.Add(p.Servicos_Nome + " ° " + p.Cidade.CID_nome);
                }

                return lista;

            }
            catch 
            {
                throw;
            }
        }


    }
}
