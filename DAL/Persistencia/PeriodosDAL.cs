﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;
using DAL.Models;

namespace DAL.Persistencia
{
    public class PeriodosDAL
    {
        private Model Con;

        public PeriodosDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Mercado_Periodo c)
        {
            try
            {
                //Provedor de Conexão do EntityFramework
                //NewTierraEntities Con = new NewTierraEntities();

                //Rotinas prontas para o banco de dados
                Con.S_Mercado_Periodo.Add(c); //insert into S_Mercado_Periodo values...
                Con.SaveChanges(); //Executar a operação

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Mercado_Periodo c)
        {
            try
            {
                Con.S_Mercado_Periodo.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        public int GetPeriodsCount()
        {
            return Con.S_Mercado_Periodo.Count();
        }

        public List<S_Mercado_Periodo> ListarTodos()
        {
            try
            {
                //SQL -> select * from S_Mercado_Periodo_Black
                return Con.S_Mercado_Periodo.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Periodo> ListarTodosPorSupplier(int SupplierID)
        {
            try
            {
                return Con.S_Mercado_Periodo.Where(s => s.S_id == SupplierID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Periodo> ListarTodosPorSupplierReport(int SupplierID)
        {
            try
            {
                return (from smp in Con.S_Mercado_Periodo
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        where smp.S_id == SupplierID
                        select smp).Distinct().OrderBy(s => s.S_merc_periodo_from).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(Int32 SupplierID)
        {
            try
            {
                //SQL -> select * from S_Mercado_Periodo
                //return Con.S_Mercado_Periodo.ToList();

                var query = from p in Con.S_Mercado_Periodo
                            join c in Con.S_Mercado_Estacao on p.S_mercado_estacao_id equals c.S_mercado_estacao_id
                            join m in Con.Mercado on p.Mercado_id equals m.Mercado_id
                            where p.S_id == SupplierID
                            orderby p.S_merc_periodo_from descending
                            select new
                            {
                                p.S_merc_periodo_id,
                                p.S_merc_periodo_from,
                                p.S_merc_periodo_to,
                                p.S_id,
                                c.S_mercado_estacao_nome,
                                m.Mercado_nome
                            };

                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Periodo> ListarTodos_model(int SupplierID)
        {
            try
            {
                return Con.S_Mercado_Periodo.Where(s => s.S_id == SupplierID).OrderBy(s => s.S_merc_periodo_from).ToList();                                           
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(Int32 SupplierID, Int32 MercadoID, Int32 BaseTarifariaID, Int32 ResponsavelID)
        {
            try
            {

                var query = from p in Con.S_Mercado_Periodo
                            join c in Con.S_Mercado_Estacao on p.S_mercado_estacao_id equals c.S_mercado_estacao_id
                            join mpf in Con.S_Mercado_Periodo_Fds on p.S_merc_periodo_id equals mpf.S_merc_periodo_id into joinDeptEmp
                            from m in joinDeptEmp.DefaultIfEmpty()
                            where p.S_id == SupplierID
                            && p.Mercado_id == MercadoID
                            && p.BaseTarifaria_id == BaseTarifariaID
                            && p.Responsavel_id == ResponsavelID
                            orderby p.S_merc_periodo_from descending
                            select new
                            {
                                p.S_merc_periodo_id,
                                p.S_merc_periodo_from,
                                p.S_merc_periodo_to,
                                p.Pacote_nome,
                                c.S_mercado_estacao_nome,
                                m.S_mercado_per_fds_from,
                                m.S_mercado_per_fds_to,
                                p.S_id
                            };

                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosFds(Int32 SupplierID, Int32 MercadoID, Int32 BaseTarifariaID, Int32 ResponsavelID)
        {
            try
            {

                var query = from p in Con.S_Mercado_Periodo
                            join c in Con.S_Mercado_Estacao on p.S_mercado_estacao_id equals c.S_mercado_estacao_id
                            where p.S_id == SupplierID
                            && p.Mercado_id == MercadoID
                            && p.BaseTarifaria_id == BaseTarifariaID
                            && p.Responsavel_id == ResponsavelID
                            orderby p.S_merc_periodo_from descending
                            select new
                            {
                                p.S_merc_periodo_id,
                                p.S_merc_periodo_from,
                                p.S_merc_periodo_to,
                                p.Pacote_nome,
                                c.S_mercado_estacao_nome,
                                p.S_id,
                                p.S_merc_periodo_fdsFrom,
                                p.S_merc_periodo_fdsTo
                            };

                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TarifasSupplierModel> ListarTodosFds_model(Int32 SupplierID, Int32 MercadoID, Int32 BaseTarifariaID, Int32 ResponsavelID)
        {
            try
            {
                return (from p in Con.S_Mercado_Periodo
                            join c in Con.S_Mercado_Estacao on p.S_mercado_estacao_id equals c.S_mercado_estacao_id
                            where p.S_id == SupplierID
                            && p.Mercado_id == MercadoID
                            && p.BaseTarifaria_id == BaseTarifariaID
                            && p.Responsavel_id == ResponsavelID
                            orderby p.S_merc_periodo_from descending
                            select new TarifasSupplierModel
                            {
                                S_merc_periodo_id = p.S_merc_periodo_id,
                                S_merc_periodo_from = (DateTime)p.S_merc_periodo_from,
                                S_merc_periodo_to = (DateTime)p.S_merc_periodo_to,
                                Pacote_nome = p.Pacote_nome,
                                S_mercado_estacao_nome = c.S_mercado_estacao_nome,
                                S_id = p.S_id,
                                S_merc_periodo_fdsFrom = p.S_merc_periodo_fdsFrom,
                                S_merc_periodo_fdsTo = p.S_merc_periodo_fdsTo,
                                Provisorio = p.Provisorio != true ? false : true
                            }).ToList();

               
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosOrdenado(Int32 SupplierID, Int32 MercadoID, Int32 BaseTarifariaID, Int32 ResponsavelID, string Ordem)
        {
            try
            {

                var query = from p in Con.S_Mercado_Periodo
                            join c in Con.S_Mercado_Estacao on p.S_mercado_estacao_id equals c.S_mercado_estacao_id
                            join mpf in Con.S_Mercado_Periodo_Fds on p.S_merc_periodo_id equals mpf.S_merc_periodo_id into joinDeptEmp
                            from m in joinDeptEmp.DefaultIfEmpty()
                            where p.S_id == SupplierID && p.Mercado_id == MercadoID && p.BaseTarifaria_id == BaseTarifariaID
                            && p.Responsavel_id == ResponsavelID
                            orderby p.S_merc_periodo_from descending
                            select new
                            {
                                p.S_merc_periodo_id,
                                p.S_merc_periodo_from,
                                p.S_merc_periodo_to,
                                c.S_mercado_estacao_nome,
                                m.S_mercado_per_fds_from,
                                m.S_mercado_per_fds_to,
                                p.S_id
                            };

                var query2 = from p in Con.S_Mercado_Periodo
                             join c in Con.S_Mercado_Estacao on p.S_mercado_estacao_id equals c.S_mercado_estacao_id
                             join mpf in Con.S_Mercado_Periodo_Fds on p.S_merc_periodo_id equals mpf.S_merc_periodo_id into joinDeptEmp
                             from m in joinDeptEmp.DefaultIfEmpty()
                             where p.S_id == SupplierID && p.Mercado_id == MercadoID && p.BaseTarifaria_id == BaseTarifariaID
                             orderby p.S_merc_periodo_from ascending
                             select new
                             {
                                 p.S_merc_periodo_id,
                                 p.S_merc_periodo_from,
                                 p.S_merc_periodo_to,
                                 c.S_mercado_estacao_nome,
                                 m.S_mercado_per_fds_from,
                                 m.S_mercado_per_fds_to,
                                 p.S_id
                             };

                if (Ordem.Equals("ASC"))
                { return query2.ToList(); }
                else
                { return query.ToList(); }

            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Periodo ObterPorIdPeriodo(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Periodo.Where(c => c.S_merc_periodo_id == IdPeriodo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Periodo ObterPorId(int IdMercadoPeriodo)
        {
            try
            {
                return Con.S_Mercado_Periodo.Where(c => c.S_mercado_estacao_id == IdMercadoPeriodo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Supplier ObterPorResp(int IdResponsavel, int IdPeriodo)
        {
            try
            {

                return (from m in Con.S_Mercado_Periodo
                        join s in Con.Supplier on m.Responsavel_id equals s.S_id
                        where m.S_merc_periodo_id == IdPeriodo
                        select s).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Periodo ObterPorIdMercado(int IdMercadoPeriodo)
        {
            try
            {
                //select * from TbCliente where IdCliente = ?
                return Con.S_Mercado_Periodo.Where(c => c.S_merc_periodo_id == IdMercadoPeriodo).Single();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        //public Int32 VerificaTarifa(string from, string to, int Sid)
        //{
        //    try
        //    {
        //        //select * from TbProduto where Nome like ?
        //        //int cons1 = Con.S_Mercado_Periodo.Where(p => p.S_merc_periodo_from.Equals(from) || p.S_merc_periodo_to.Equals(to) || p.S_id.Equals(Sid)).Count();

        //        Int32 cons1 = Con.S_Mercado_Periodo.Where(p => p.S_id.Equals(Sid)).Count();
        //        Int32 cons2 = Con.S_Mercado_Periodo.Where(p => p.S_merc_periodo_from.Equals(from)).Count();


        //        if (cons1 > 0)
        //        {

        //        }
        //         return cons1;

        //        //Expressões LAMBDA
        //        /*
        //         * StartsWith   -> Começa com
        //         * EndsWith     -> Termina com
        //         * Contains     -> Contendo
        //         * Equals       -> Exatamente igual
        //         */
        //    }
        //    catch
        //    {
        //        throw; //Lançar a exceção adiante...
        //    }
        //}

        public void Atualizar(S_Mercado_Periodo registroNovo)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                S_Mercado_Periodo registroAntigo = ObterPorIdMercado(registroNovo.S_merc_periodo_id);

                //Atualizando os dados
                registroAntigo.S_merc_periodo_from = registroNovo.S_merc_periodo_from;
                registroAntigo.S_merc_periodo_to = registroNovo.S_merc_periodo_to;
                registroAntigo.Pacote_nome = registroNovo.Pacote_nome;
                registroAntigo.S_id = registroNovo.S_id;
                registroAntigo.S_mercado_estacao_id = registroNovo.S_mercado_estacao_id;
                registroAntigo.Mercado_id = registroNovo.Mercado_id;
                registroAntigo.Moeda_id = registroNovo.Moeda_id;
                registroAntigo.MinimoNoites_id = registroNovo.MinimoNoites_id;
                registroAntigo.Promocao_id = registroNovo.Promocao_id;
                registroAntigo.Responsavel_id = registroNovo.Responsavel_id;
                registroAntigo.S_merc_periodo_minimo = registroNovo.S_merc_periodo_minimo;
                registroAntigo.S_merc_periodo_mandatorio = registroNovo.S_merc_periodo_mandatorio;
                registroAntigo.S_merc_periodo_fdsFrom = registroNovo.S_merc_periodo_fdsFrom;
                registroAntigo.S_merc_periodo_fdsTo = registroNovo.S_merc_periodo_fdsTo;
                registroAntigo.S_merc_remarks = registroNovo.S_merc_remarks;

                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaTarifa(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Tarifa.Any(s => s.S_merc_periodo_id == IdPeriodo);
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExiste(DateTime from, DateTime to, int IdSupp)
        {
            try
            {
                return Con.S_Mercado_Periodo.Where(s => s.S_merc_periodo_from == from && s.S_merc_periodo_to == to && s.S_id == IdSupp).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public object ListarFDS(DateTime DataFrom, DateTime DataOut, int IdSupplier)
        {
            try
            {

                return (from smp in Con.S_Mercado_Periodo
                        where smp.S_merc_periodo_from <= DataFrom &&
                              smp.S_merc_periodo_to >= DataOut &&
                              smp.S_id == IdSupplier
                        select new
                        {
                            Texto = smp.S_merc_periodo_fdsFrom + "/" + smp.S_merc_periodo_fdsTo,
                            smp.S_merc_periodo_id
                        }).Distinct().ToList();


                //return Con.S_Mercado_Periodo.Where(s => s.S_merc_periodo_from <= DataFrom &&
                //                                        s.S_merc_periodo_to >= DataOut &&
                //                                        s.S_id == IdSupplier).ToList();

            }
            catch
            {
                throw;
            }
        }

        public bool VerificaProvisorio(int IdSupp)
        {
            try
            {
                return Con.S_Mercado_Periodo.Where(s => s.S_id == IdSupp && s.Provisorio == true).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarProvisorio(S_Mercado_Periodo registroNovo)
        {
            try
            {
                S_Mercado_Periodo registroAntigo = ObterPorIdMercado(registroNovo.S_merc_periodo_id);
                registroAntigo.Provisorio = registroNovo.Provisorio;
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizaProvisorios(int IdSupp)
        {
            try
            {
                foreach (S_Mercado_Periodo item in ListarTodosPorSupplier(IdSupp))
                {
                    S_Mercado_Periodo smt = new S_Mercado_Periodo();
                    smt.Provisorio = null;
                    smt.S_merc_periodo_id = item.S_merc_periodo_id;

                    AtualizarProvisorio(smt);
                }
            }
            catch
            {
                throw;
            }
        }

    }
}
