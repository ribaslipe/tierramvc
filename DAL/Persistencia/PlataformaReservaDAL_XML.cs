﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Persistencia;

namespace DAL.Persistencia
{
    public class PlataformaReservaDAL_XML
    {

        private Model Con;

        public PlataformaReservaDAL_XML()
        {
            Con = new Model();
        }


        public void Salvar(PlataformaReserva_XML p)
        {
            try
            {
                Con.PlataformaReserva_XML.Add(p);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public PlataformaReserva_XML ObterPorId(int id)
        {
            try
            {
                return Con.PlataformaReserva_XML.Where(s => s.PlataformaReserva_id == id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public PlataformaReserva_XML ObterPorQTarifa(int idQTarifas)
        {
            try
            {
                return Con.PlataformaReserva_XML.Where(s => s.Quote_Tarifas_id == idQTarifas).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public PlataformaReserva_XML ObterPorFTarifa(int idFTarifas)
        {
            try
            {
                return Con.PlataformaReserva_XML.Where(s => s.File_Tarifas_id == idFTarifas).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public PlataformaReserva_XML ObterPorReserva(string reserva)
        {
            try
            {
                return Con.PlataformaReserva_XML.Where(s => s.ReservaID == reserva).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(PlataformaReserva_XML novo)
        {
            try
            {
                PlataformaReserva_XML antigo = ObterPorQTarifa((int)novo.Quote_Tarifas_id);
                antigo.Status = novo.Status;
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void AtualizarStatusFtr(PlataformaReserva_XML novo)
        {
            try
            {
                PlataformaReserva_XML antigo = ObterPorId(novo.PlataformaReserva_id);
                antigo.Status = novo.Status;
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }


        public void Atualizar_QtrID(PlataformaReserva_XML novo)
        {
            try
            {
                PlataformaReserva_XML antigo = ObterPorId(novo.PlataformaReserva_id);
                antigo.Quote_Tarifas_id = antigo.Quote_Tarifas_id;
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<int> ObterTodasReservasQtr_UmMes()
        {
            try
            {
                //SELECT Quote_Tarifas.Quote_Tarifas_id, Quote_Tarifas.Data_From, Quote_Tarifas.Data_To, Quote_Tarifas.S_nome, PlataformaReserva_XML.PlataformaReserva_id
                //FROM            Quote_Tarifas INNER JOIN
                //PlataformaReserva_XML ON Quote_Tarifas.Quote_Tarifas_id = PlataformaReserva_XML.Quote_Tarifas_id
                //WHERE(Quote_Tarifas.Plataforma_id = 1) AND(Quote_Tarifas.Data_From > '19000101') AND(Quote_Tarifas.Data_From <= '20170611')


                //SELECT Quote_Tarifas.Quote_Tarifas_id, Quote_Tarifas.Data_From, Quote_Tarifas.Data_To, Quote_Tarifas.S_nome, 
                //PlataformaReserva_XML.PlataformaReserva_id, Quotation_Grupo.cancelAll
                //FROM            Quote_Tarifas INNER JOIN
                //PlataformaReserva_XML ON Quote_Tarifas.Quote_Tarifas_id = PlataformaReserva_XML.Quote_Tarifas_id INNER JOIN
                //File_Carrinho ON Quote_Tarifas.File_id = File_Carrinho.File_id INNER JOIN
                //Quotation_Grupo ON File_Carrinho.Quotation_Grupo_Id = Quotation_Grupo.Quotation_Grupo_Id
                //WHERE(Quote_Tarifas.Plataforma_id = 1) AND(Quote_Tarifas.Data_From > '19000101') AND(Quote_Tarifas.Data_From <= '20170611')

                DateTime dtAll = DateTime.Now.AddMonths(1);

                return (from qtr in Con.Quote_Tarifas
                        join pla in Con.PlataformaReserva_XML on qtr.Quote_Tarifas_id equals pla.Quote_Tarifas_id
                        join fle in Con.File_Carrinho on qtr.File_id equals fle.File_id
                        join quo in Con.Quotation_Grupo on fle.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                        where qtr.Plataforma_id == 1 &&
                              qtr.Data_From <= dtAll &&
                              quo.cancelAll == true &&
                              qtr.Status.Equals("OK")
                        select pla.PlataformaReserva_id).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<int> ObterTodasReservasFtr_UmMes(int Plataforma_id)
        {
            try
            {

                DateTime dtAll = DateTime.Now.AddMonths(1);

                return (from qtr in Con.File_Tarifas
                        join pla in Con.PlataformaReserva_XML on qtr.File_Tarifas_id equals pla.File_Tarifas_id
                        join fle in Con.File_Carrinho on qtr.File_id equals fle.File_id
                        join quo in Con.Quotation_Grupo on fle.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                        where qtr.Plataforma_id == Plataforma_id &&
                              qtr.Data_From <= dtAll  &&
                              quo.cancelAll == true &&
                              qtr.Status.Equals("OK")
                        select pla.PlataformaReserva_id).ToList();

            }
            catch
            {
                throw;
            }
        }

    }
}
