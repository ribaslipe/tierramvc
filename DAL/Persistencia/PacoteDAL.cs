﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class PacoteDAL
    {

        private Model Con;

        public PacoteDAL()
        {
            Con = new Model();
        }

        public void Salvar(Pacote p)
        {
            try
            {
                Con.Pacote.Add(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Pacote ObterPorId(int Id)
        {
            try
            {
                return Con.Pacote.Where(p => p.Pacote_id == Id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Pacote> ListarTodos(string code, string nome)
        {
            try
            {
                if (nome == "")
                {
                    return Con.Pacote.Where(p => p.Pacote_Code.Equals(code)).ToList();
                }
                else
                {
                    return Con.Pacote.Where(p => p.Pacote_NomePacote.Equals(nome)).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSearch(string code)
        {
            try
            {

                return (from pac in Con.Pacote
                        where pac.Pacote_Code.Contains(code)
                              ||
                              pac.Pacote_NomePacote.Contains(code)
                        orderby pac.DataFrom descending
                        select new
                        {
                            pac.Pacote_Code,
                            pac.Pacote_NomePacote,
                            pac.DataFrom,
                            pac.DataTo
                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSearch(DateTime dtIn, DateTime dtOut)
        {
            try
            {

                return (from pac in Con.Pacote
                        where pac.DataFrom <= dtIn &&
                              pac.DataTo >= dtOut
                              ||
                              pac.DataFrom <= dtOut &&
                              pac.DataTo >= dtIn
                        orderby pac.DataFrom descending
                        select new
                        {
                            pac.Pacote_Code,
                            pac.Pacote_NomePacote,
                            pac.DataFrom,
                            pac.DataTo
                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Pacote> ListarTodosHoteis(string code)
        {
            try
            {
                return Con.Pacote.Where(p => p.Pacote_Code.Equals(code) &&
                                             p.Flag.Equals("HOTEL") ||
                                             p.Pacote_Code.Equals(code) &&
                                             p.Flag.Equals("MEAL")).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Pacote> ListarTodosServicos(string code)
        {
            try
            {
                return Con.Pacote.Where(p => p.Pacote_Code.Equals(code) &&
                                             p.Flag.Equals("SubServ") ||
                                             p.Pacote_Code.Equals(code) &&
                                             p.Flag.Equals("TRANSFER") ||
                                             p.Pacote_Code.Equals(code) &&
                                             p.Flag.Equals("TOUR")).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Pacote> ListarTodosExtras(string code)
        {
            try
            {
                return Con.Pacote.Where(p => p.Pacote_Code.Equals(code) &&
                                             p.Flag.Equals("EXTRA")).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Pacote> ListarTodosFlag(string code, string flag)
        {
            try
            {
                return Con.Pacote.Where(p => p.Pacote_Code.Equals(code) &&
                                             p.Flag.Equals(flag)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos()
        {
            try
            {
                return (from pac in Con.Pacote
                        orderby pac.DataFrom descending
                        select new
                        {
                            pac.Pacote_Code,
                            pac.Pacote_NomePacote,
                            pac.DataFrom,
                            pac.DataTo
                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Pacote novo)
        {
            try
            {
                Pacote antigo = ObterPorId(novo.Pacote_id);

                antigo.DataFrom = novo.DataFrom;
                antigo.DataTo = novo.DataTo;
                antigo.Pacote_NomePacote = novo.Pacote_NomePacote;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void AtualizarValor(Pacote novo)
        {
            try
            {
                Pacote antigo = ObterPorId(novo.Pacote_id);

                antigo.Pacote_ValorTotal = novo.Pacote_ValorTotal;                

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public Pacote RetornaLast()
        {
            try
            {
                return Con.Pacote.OrderByDescending(p => p.Pacote_id).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Pacote p)
        {
            try
            {
                Con.Pacote.Remove(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
}
