﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class GroupAmenitiesDAL
    {
        private Model Con;

        public GroupAmenitiesDAL()
        {
            Con = new Model();
        }

        public List<S_GroupAmenities> ListarTodos()
        {
            try
            {
                return Con.S_GroupAmenities.OrderBy(a => a.S_GroupAmenities_Name).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public S_GroupAmenities ObterPorID(int IdGrupo)
        {
            try
            {
                return Con.S_GroupAmenities.Where(s => s.S_GroupAmenities_ID == IdGrupo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<S_GroupAmenities> ListarTodos_Spp(int idsupplier)
        {
            try
            {
                //SELECT DISTINCT S_GroupAmenities.S_GroupAmenities_ID, S_GroupAmenities.S_GroupAmenities_Name
                //FROM            S_GroupAmenities INNER JOIN
                //S_Amenities ON S_GroupAmenities.S_GroupAmenities_ID = S_Amenities.S_GroupAmenities_ID INNER JOIN
                //S_SupplierAmenities ON S_Amenities.S_Amenities_id = S_SupplierAmenities.S_SupplierAmenities_Amenities_ID
                //WHERE(S_SupplierAmenities.S_SupplierAmenities_Supplier_ID = 401718)
                //ORDER BY S_GroupAmenities.S_GroupAmenities_Name

                return (from sgr in Con.S_GroupAmenities
                        join sam in Con.S_Amenities on sgr.S_GroupAmenities_ID equals sam.S_GroupAmenities_ID
                        join ssu in Con.S_SupplierAmenities on sam.S_Amenities_id equals ssu.S_SupplierAmenities_Amenities_ID
                        where ssu.S_SupplierAmenities_Supplier_ID == idsupplier
                        select sgr).Distinct().OrderBy(s => s.S_GroupAmenities_Name).ToList();


            }
            catch 
            {
                throw;
            }
        }

        public List<S_Amenities> ListarTodos_Spp_Itens(int idsupplier)
        {
            try
            {
                //SELECT S_Amenities.S_Amenities_nome, S_GroupAmenities.S_GroupAmenities_ID
                //FROM            S_GroupAmenities INNER JOIN
                //S_Amenities ON S_GroupAmenities.S_GroupAmenities_ID = S_Amenities.S_GroupAmenities_ID INNER JOIN
                //S_SupplierAmenities ON S_Amenities.S_Amenities_id = S_SupplierAmenities.S_SupplierAmenities_Amenities_ID
                //WHERE(S_SupplierAmenities.S_SupplierAmenities_Supplier_ID = 404135)
                //ORDER BY S_GroupAmenities.S_GroupAmenities_Name, S_Amenities.S_Amenities_nome


                return (from sgr in Con.S_GroupAmenities
                        join sam in Con.S_Amenities on sgr.S_GroupAmenities_ID equals sam.S_GroupAmenities_ID
                        join ssu in Con.S_SupplierAmenities on sam.S_Amenities_id equals ssu.S_SupplierAmenities_Amenities_ID
                        where ssu.S_SupplierAmenities_Supplier_ID == idsupplier
                        select sam).Distinct().OrderBy(s => s.S_Amenities_nome).ToList();


            }
            catch
            {
                throw;
            }
        }

    }
}
