﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class ContatoDAL
    {

        private Model Con = new Model();

        //Gravar registro de contatos na tabela
        public void Salvar(S_Contato c)
        {
            try
            {
                //Rotina prontas para banco
                Con.S_Contato.Add(c);//insere S_Contato
                Con.SaveChanges();

            }
            catch
            {

                throw;
            }
        }

        //Método para Excluir o S_Contato na base
        public void Excluir(S_Contato c)
        {
            try
            {
                Con.S_Contato.Remove(c); //Prepara a deleção da S_Contato
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        //Metodo para listar todos os S_Contato cadastradas
        public List<S_Contato> ListarTodos()
        {
            try
            {
                //SQL -> select * from Cidades
                return Con.S_Contato.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Metodo para listar todos os S_Contato por supplier
        public List<S_Contato> ListarTodos(Int32 SupplierID)
        {
            try
            {
                return Con.S_Contato.Where(c => c.S_id == SupplierID).ToList();          
            }
            catch
            {
                throw;
            }
        }

        //Método para obter 1 cidade pelo Id
        public S_Contato ObterPorId(int IdContato)
        {
            try
            {
                //select * from Cidade where IdCidade = ?
                return Con.S_Contato.Where(c => c.SCONT_id == IdContato).Single();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        //Método para atualizar os dados da Cidade
        public void Atualizar(S_Contato registroNovo)
        {
            try
            {

                //Buscar na base de dados registro antigo
                S_Contato registroAntigo = ObterPorId(registroNovo.SCONT_id);

                //Atualizando os dados
                registroAntigo.SCONT_nome = registroNovo.SCONT_nome;
                registroAntigo.SCONT_telefone = registroNovo.SCONT_telefone;
                registroAntigo.SCONT_position = registroNovo.SCONT_position;
                registroAntigo.SCONT_email = registroNovo.SCONT_email;
                registroAntigo.S_id = registroNovo.S_id;

                //executa o commit
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }

        }
    }
}
