﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class MarkupPaisDAL
    {

        private Model Con;

        public MarkupPaisDAL()
        {
            Con = new Model();
        }

        public MarkupPais ObterPorPais(int IdPais)
        {
            try
            {
                return Con.MarkupPais.Where(m => m.Pais_id == IdPais).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
