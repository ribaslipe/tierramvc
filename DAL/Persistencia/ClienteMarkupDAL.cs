﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class ClienteMarkupDAL
    {

        private Model Con;

        public ClienteMarkupDAL()
        {
            Con = new Model();
        }

        public void Salvar(Cliente_Markup c)
        {
            try
            {
                Con.Cliente_Markup.Add(c);
                Con.SaveChanges();
            }
            catch
            {                
                throw;
            }
        }

        public Cliente_Markup ObterPorId(int IdMarkup)
        {
            try
            {
                return Con.Cliente_Markup.Where(c => c.Markup_id == IdMarkup).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public Cliente_Markup ObterPorIdCliente(int IdCliente)
        {
            try
            {
                var cli = Con.Cliente_Markup.Where(c => c.Cliente_id == IdCliente).SingleOrDefault();

                return cli;
            }
            catch 
            {                
                throw;
            }
        }       

        public List<Cliente_Markup> ObterPorIdCliente(int IdCliente, DateTime dtFrom, DateTime dtTo)
        {
            try
            {

                return Con.Cliente_Markup.Where(c => c.Cliente_id == IdCliente &&
                                                     c.Markup_dataInicial <= dtFrom &&
                                                     c.Markup_dataFinal >= dtTo
                                                     ||
                                                     c.Cliente_id == IdCliente &&
                                                     c.Markup_dataInicial <= dtTo &&
                                                     c.Markup_dataFinal >= dtFrom).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool ObterPorCliente(int IdCliente)
        {
            try
            {
                return Con.Cliente_Markup.Where(c => c.Cliente_id == IdCliente).Count() != 0;
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(Cliente_Markup c)
        {
            try
            {
                Con.Cliente_Markup.Remove(c);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public List<Cliente_Markup> ListarTodos(int IdCliente)
        {
            try
            {
               return Con.Cliente_Markup.Where(c => c.Cliente_id == IdCliente).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(Cliente_Markup novo)
        {
            try
            {
                Cliente_Markup antigo = ObterPorId(novo.Markup_id);

                antigo.Markup_dataInicial = novo.Markup_dataInicial;
                antigo.Markup_dataFinal = novo.Markup_dataFinal;
                antigo.Markup_hotel = novo.Markup_hotel;
                antigo.Markup_descontoHotel = novo.Markup_descontoHotel;
                antigo.Markup_servico = novo.Markup_servico;
                antigo.Markup_descontoServico = novo.Markup_descontoServico;
                antigo.TipoFile_id = novo.TipoFile_id;
                antigo.Cliente_id = novo.Cliente_id;

                antigo.Markup_seasonal = novo.Markup_seasonal;
                antigo.Markup_meals = novo.Markup_meals;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
