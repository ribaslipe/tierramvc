﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class MarkupPaisFileDAL
    {

        private Model Con;

        public MarkupPaisFileDAL()
        {
            Con = new Model();
        }

        public void Salvar(MarkupPaisFile m)
        {
            try
            {
                Con.MarkupPaisFile.Add(m);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public MarkupPaisFile ObterPorId(int IdMarkupFile)
        {
            try
            {
                return Con.MarkupPaisFile.Where(m => m.MarkupFilePais_id == IdMarkupFile).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public MarkupPaisFile ObterPorQGrupo(int IdQuotGrupo)
        {
            try
            {
                return Con.MarkupPaisFile.Where(m => m.Quotation_Grupo_Id == IdQuotGrupo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public MarkupPaisFile ObterPorQGrupo(int IdQuotGrupo, DateTime dtIn)
        {
            try
            {
                return Con.MarkupPaisFile.Where(m => m.Quotation_Grupo_Id == IdQuotGrupo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public MarkupPaisFile ObterPorQGrupoFirst(int IdQuotGrupo)
        {
            try
            {
                return Con.MarkupPaisFile.Where(m => m.Quotation_Grupo_Id == IdQuotGrupo).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<MarkupPaisFile> LisarPorQuot(int IdQuotGrupo)
        {
            try
            {
                return Con.MarkupPaisFile.Where(m => m.Quotation_Grupo_Id == IdQuotGrupo).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(MarkupPaisFile novo)
        {
            try
            {
                MarkupPaisFile antigo = ObterPorId(novo.MarkupFilePais_id);

                antigo.Markup = novo.Markup;
                antigo.MarkupServ = novo.MarkupServ;
                antigo.Desconto = novo.Desconto;
                antigo.MarkupLodge = novo.MarkupLodge;                

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(MarkupPaisFile m)
        {
            try
            {
                Con.MarkupPaisFile.Remove(m);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
}
