﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class TBItinerary_FlightsInformationDAL
    {
        private Model Con;

        public TBItinerary_FlightsInformationDAL()
        {
            Con = new Model();
        }

        public int Adicionar(TBItinerary_FlightsInformation tfi)
        {
            try
            {
                Con.TBItinerary_FlightsInformation.Add(tfi);
                Con.SaveChanges();
                return tfi.Id;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public void Atualizar(TBItinerary_FlightsInformation tfi)
        {
            var old = Con.TBItinerary_FlightsInformation.Where(a => a.Id == tfi.Id).Single();
            old.FlightInformation = tfi.FlightInformation;
            Con.SaveChanges();
        }

        public TBItinerary_FlightsInformation ObterPorQuote(string quote, int opt)
        {
            return Con.TBItinerary_FlightsInformation.Where(a => a.QuotationCode == quote && a.optQuote == opt).FirstOrDefault();
        }
    }
}
