﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class MercadoEstacaoDAL
    {

        private Model Con;

        public MercadoEstacaoDAL()
        {
            Con = new Model();
        }

        public bool VerificaPacote(int IdEstacao)
        {
            try
            {
                return Con.S_Mercado_Estacao.Where(e => e.S_mercado_estacao_id == IdEstacao
                                                   && e.S_mercado_estacao_pacote == true).Count() != 0;
            }
            catch 
            {
                throw;
            }
        }

        public bool VerificaPacotePorPeriodo(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Periodo.Where(e => e.S_Mercado_Estacao.S_mercado_estacao_pacote == true &&
                                                        e.S_merc_periodo_id == IdPeriodo).Count() != 0;

            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExiste(string nome)
        {
            try
            {
                return Con.S_Mercado_Estacao.Where(s => s.S_mercado_estacao_nome.Equals(nome)).Count() != 0;
            }
            catch 
            {                
                throw;
            }
        }

        public void Salvar(S_Mercado_Estacao s)
        {
            try
            {
                Con.S_Mercado_Estacao.Add(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Mercado_Estacao ObterPorId(int IdEstacao)
        {
            try
            {
                return Con.S_Mercado_Estacao.Where(s => s.S_mercado_estacao_id == IdEstacao).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(S_Mercado_Estacao novo)
        {
            try
            {
                S_Mercado_Estacao antigo = ObterPorId(novo.S_mercado_estacao_id);

                antigo.S_mercado_estacao_nome = novo.S_mercado_estacao_nome;
                antigo.S_mercado_estacao_pacote = novo.S_mercado_estacao_pacote;
                antigo.S_mercado_estacao_seasonal = novo.S_mercado_estacao_seasonal;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Mercado_Estacao> ListarTodos()
        {
            try
            {
                return Con.S_Mercado_Estacao.OrderBy(p => p.S_mercado_estacao_nome).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Mercado_Estacao> ListarTodos(string nome)
        {
            try
            {
                return Con.S_Mercado_Estacao.Where(s => s.S_mercado_estacao_nome.Contains(nome)).OrderBy(p => p.S_mercado_estacao_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Mercado_Estacao s)
        {
            try
            {
                Con.S_Mercado_Estacao.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaNome(string Nome)
        {
            try
            {
                return Con.S_Mercado_Estacao.Where(s => s.S_mercado_estacao_nome.Equals(Nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Estacao> ObterPorSupplier(int IdSupplier)
        {
            try
            {
                return (from sme in Con.S_Mercado_Estacao
                        join smp in Con.S_Mercado_Periodo on sme.S_mercado_estacao_id equals smp.S_mercado_estacao_id
                        where smp.S_id == IdSupplier
                        select sme).Distinct().ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Mercado_Estacao ObterPorNome(string temporada)
        {
            try
            {
                return Con.S_Mercado_Estacao.Where(s => s.S_mercado_estacao_nome.Equals(temporada)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
