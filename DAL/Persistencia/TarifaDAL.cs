﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;
using DAL.Persistencia;
using DAL.Models;

namespace DAL.Persistencia
{
    public class TarifaDAL
    {
        private Model Con;

        public TarifaDAL()
        {
            Con = new Model();
        }

        public object ListarTarifasParaCopiar(Int32 supplierID, Int32 mercadoID, Int32 mercadoEstacaoID, Int32 periodoID)
        {
            try
            {
                var query = from mt in Con.S_Mercado_Tarifa
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into joinDeptEmp
                            from m in joinDeptEmp.DefaultIfEmpty()
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mtt in Con.S_Mercado_Tipo_Tarifa on mt.S_mercado_tipo_tarifa_id equals mtt.S_mercado_tipo_tarifa_id
                            join mp in Con.S_Mercado_Periodo on mt.S_merc_periodo_id equals mp.S_merc_periodo_id
                            where mp.S_id == supplierID && mp.Mercado_id == mercadoID && mp.S_mercado_estacao_id == mercadoEstacaoID
                                  && mp.S_merc_periodo_id == periodoID
                            select new
                            {
                                mt.S_merc_tarif_tarifa,
                                tr.Tipo_room_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                m.S_mercado_tarifa_status_nome,
                                //mts.S_mercado_tarifa_status_nome,
                                tc.Tarif_categoria_nome,
                                //S_Mercado_Estacao.S_mercado_estacao_nome,
                                mt.S_merc_tarif_id
                            };
                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaPeriodoExistente(DateTime dataFrom, DateTime dataTo, int Sid, int mercadoID)
        {
            try
            {
                var Periodo = from mp in Con.S_Mercado_Periodo
                              where mp.S_id == Sid &&
                                    mp.S_merc_periodo_from == dataFrom &&
                                    mp.S_merc_periodo_to == dataTo &&
                                    mp.Mercado_id == mercadoID
                              select new
                              {
                                  mp.S_merc_periodo_id
                              };

                if (Periodo.Count() == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

                //return Periodo.ToList();

            }
            catch
            {

                throw;
            }

        }

        public object ListarTodos(Int32 PeriodoID)
        {
            try
            {
                var query = from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            //orderby mtt.S_mercado_tipo_tarifa_nome, tc.Tarif_categoria_nome, tr.Tipo_room_id, mt.S_merc_tarif_tarifa_total
                            //orderby tc.Tarif_categoria_nome ascending
                            select new
                            {
                                mt.S_merc_tarif_tarifa_total,
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            };

                return query.ToList();

            }
            catch
            {

                throw;
            }
        }

        public object ListarTodosComCategoria(int IdPeriodo)
        {
            try
            {
                return (from smt in Con.S_Mercado_Tarifa
                        join trg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals trg.Tarif_categoria_id
                        where smt.S_merc_periodo_id == IdPeriodo
                        select new
                        {
                            smt.Tarif_categoria_id,
                            trg.Tarif_categoria_nome,
                            smt.S_merc_tarif_ordem
                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<OrdenarTarifasModel> ListarTodosComCategoria_model(int IdPeriodo)
        {
            try
            {
                return (from smt in Con.S_Mercado_Tarifa
                        join trg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals trg.Tarif_categoria_id
                        where smt.S_merc_periodo_id == IdPeriodo
                        select new OrdenarTarifasModel
                        {
                            Tarif_categoria_id = smt.Tarif_categoria_id,
                            Tarif_categoria_nome = trg.Tarif_categoria_nome,
                            S_merc_tarif_ordem = smt.S_merc_tarif_ordem == null ? 0 : (int)smt.S_merc_tarif_ordem,
                            CategoriaID = smt.Tarif_categoria_id
                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Tarifa> ListarTodosListaCategoria(int IdPeriodo, int IdCategoria)
        {
            try
            {
                return Con.S_Mercado_Tarifa.Where(s => s.S_merc_periodo_id == IdPeriodo &&
                                                       s.Tarif_categoria_id == IdCategoria).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(int PeriodoID, int IdBase)
        {
            try
            {
                var query = from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tb in Con.S_Tarifa_Base on mt.S_merc_tarif_id equals tb.S_merc_tarif_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID &&
                                  tb.Base_id == IdBase
                            //orderby tc.Tarif_categoria_nome ascending
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            select new
                            {
                                mt.S_merc_tarif_tarifa_total,
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            };

                return query.ToList();

            }
            catch
            {

                throw;
            }
        }

        public object ListarTodosOrdenado(Int32 PeriodoID)
        {
            try
            {

                var query = from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            select new
                            {
                                mt.S_merc_tarif_tarifa_total,
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            };


                return query.ToList();


            }
            catch
            {

                throw;
            }
        }

        public List<TarifasCadastradasPeriodoModel> ListarTodosOrdenado_model(Int32 PeriodoID)
        {
            try
            {

                var query = from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            select new TarifasCadastradasPeriodoModel
                            {
                                S_merc_tarif_tarifa_total = mt.S_merc_tarif_tarifa_total == null ? 0 : (decimal)mt.S_merc_tarif_tarifa_total,
                                S_merc_tarif_id = mt.S_merc_tarif_id,
                                Tipo_room_nome = tr.Tipo_room_nome,
                                Tarif_categoria_nome = tc.Tarif_categoria_nome,
                                S_mercado_tipo_tarifa_nome = mtt.S_mercado_tipo_tarifa_nome,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                S_merc_tarif_tarifa = mt.S_merc_tarif_tarifa == null ? 0 : (decimal)mt.S_merc_tarif_tarifa
                            };


                return query.ToList();


            }
            catch
            {

                throw;
            }
        }

        public object ListarTodosOrdenadoComBase(Int32 PeriodoID, int IdBase)
        {
            try
            {

                var query = from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tb in Con.S_Tarifa_Base on mt.S_merc_tarif_id equals tb.S_merc_tarif_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID &&
                                  tb.Base_id == IdBase
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            select new
                            {
                                mt.S_merc_tarif_tarifa_total,
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            };


                return query.ToList();

            }
            catch
            {

                throw;
            }
        }

        public List<TarifasCadastradasPeriodoModel> ListarTodosOrdenadoComBase_model(Int32 PeriodoID, int IdBase)
        {
            try
            {

                var query = from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tb in Con.S_Tarifa_Base on mt.S_merc_tarif_id equals tb.S_merc_tarif_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID &&
                                  tb.Base_id == IdBase
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            select new TarifasCadastradasPeriodoModel
                            {
                                S_merc_tarif_tarifa_total = mt.S_merc_tarif_tarifa_total == null ? 0 : (decimal)mt.S_merc_tarif_tarifa_total,
                                S_merc_tarif_id = mt.S_merc_tarif_id,
                                Tipo_room_nome = tr.Tipo_room_nome,
                                Tarif_categoria_nome = tc.Tarif_categoria_nome,
                                S_mercado_tipo_tarifa_nome = mtt.S_mercado_tipo_tarifa_nome,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                S_merc_tarif_tarifa = mt.S_merc_tarif_tarifa == null ? 0 : (decimal)mt.S_merc_tarif_tarifa
                            };


                return query.ToList();

            }
            catch
            {

                throw;
            }
        }

        public object ListarTodos(Int32 PeriodoID, string opcao)
        {
            try
            {

                if (opcao.Equals("normal"))
                {
                    return (from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            //orderby mtt.S_mercado_tipo_tarifa_nome, tc.Tarif_categoria_nome, tr.Tipo_room_id, mt.S_merc_tarif_tarifa_total
                            //orderby tc.Tarif_categoria_nome ascending
                            select new
                            {
                                mt.S_merc_tarif_tarifa_total,
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            }).ToList();
                }
                else if (opcao.Equals("apartamento"))
                {

                    return (from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            //orderby mtt.S_mercado_tipo_tarifa_nome, tc.Tarif_categoria_nome, tr.Tipo_room_id, mt.S_merc_tarif_tarifa_total
                            //orderby tc.Tarif_categoria_nome ascending
                            select new
                            {
                                S_merc_tarif_tarifa_total = tr.Tipo_room_nome.Equals("Single") ? mt.S_merc_tarif_tarifa_total :
                                                            tr.Tipo_room_nome.Equals("Double") ? mt.S_merc_tarif_tarifa_total * 2 :
                                                            tr.Tipo_room_nome.Equals("Triple") ? mt.S_merc_tarif_tarifa_total * 3 :
                                                            tr.Tipo_room_nome.Equals("Quadruple") ? mt.S_merc_tarif_tarifa_total * 4 :
                                                            tr.Tipo_room_nome.Equals("Quintuple") ? mt.S_merc_tarif_tarifa_total * 5 :
                                                            tr.Tipo_room_nome.Equals("Sextuple") ? mt.S_merc_tarif_tarifa_total * 6 :
                                                            tr.Tipo_room_nome.Equals("Twin") ? mt.S_merc_tarif_tarifa_total * 2 :
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            }).ToList();
                }
                else
                {

                    return (from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            //orderby mtt.S_mercado_tipo_tarifa_nome, tc.Tarif_categoria_nome, tr.Tipo_room_id, mt.S_merc_tarif_tarifa_total
                            //orderby tc.Tarif_categoria_nome ascending
                            select new
                            {
                                S_merc_tarif_tarifa_total = tr.Tipo_room_nome.Equals("Single") ? mt.S_merc_tarif_tarifa_total :
                                                            tr.Tipo_room_nome.Equals("Double") ? mt.S_merc_tarif_tarifa_total / 2 :
                                                            tr.Tipo_room_nome.Equals("Triple") ? mt.S_merc_tarif_tarifa_total / 3 :
                                                            tr.Tipo_room_nome.Equals("Quadruple") ? mt.S_merc_tarif_tarifa_total / 4 :
                                                            tr.Tipo_room_nome.Equals("Quintuple") ? mt.S_merc_tarif_tarifa_total / 5 :
                                                            tr.Tipo_room_nome.Equals("Sextuple") ? mt.S_merc_tarif_tarifa_total / 6 :
                                                            tr.Tipo_room_nome.Equals("Twin") ? mt.S_merc_tarif_tarifa_total / 2 :
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            }).ToList();
                }

                //return query.ToList();
            }
            catch
            {

                throw;
            }
        }

        public object ListarTodos(Int32 PeriodoID, string opcao, int IdBase)
        {
            try
            {

                if (opcao.Equals("normal"))
                {
                    return (from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tb in Con.S_Tarifa_Base on mt.S_merc_tarif_id equals tb.S_merc_tarif_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID &&
                                  tb.Base_id == IdBase
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            //orderby mtt.S_mercado_tipo_tarifa_nome, tc.Tarif_categoria_nome, tr.Tipo_room_id, mt.S_merc_tarif_tarifa_total
                            //orderby tc.Tarif_categoria_nome ascending
                            select new
                            {
                                mt.S_merc_tarif_tarifa_total,
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            }).ToList();
                }
                else
                {
                    return (from mtt in Con.S_Mercado_Tipo_Tarifa
                            join mt in Con.S_Mercado_Tarifa on mtt.S_mercado_tipo_tarifa_id equals mt.S_mercado_tipo_tarifa_id
                            join tr in Con.Tipo_Room on mt.Tipo_room_id equals tr.Tipo_room_id
                            join tb in Con.S_Tarifa_Base on mt.S_merc_tarif_id equals tb.S_merc_tarif_id
                            join tc in Con.Tarif_Categoria on mt.Tarif_categoria_id equals tc.Tarif_categoria_id
                            join mts in Con.S_Mercado_Tarifa_Status on mt.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mt.S_merc_periodo_id == PeriodoID &&
                                  tb.Base_id == IdBase
                            orderby mtt.S_mercado_tipo_tarifa_nome, mt.S_merc_tarif_ordem, tr.Tipo_room_id
                            //orderby mtt.S_mercado_tipo_tarifa_nome, tc.Tarif_categoria_nome, tr.Tipo_room_id, mt.S_merc_tarif_tarifa_total
                            //orderby tc.Tarif_categoria_nome ascending
                            select new
                            {
                                S_merc_tarif_tarifa_total = tr.Tipo_room_nome.Equals("Single") ? mt.S_merc_tarif_tarifa_total :
                                                            tr.Tipo_room_nome.Equals("Double") ? mt.S_merc_tarif_tarifa_total / 2 :
                                                            tr.Tipo_room_nome.Equals("Triple") ? mt.S_merc_tarif_tarifa_total / 3 :
                                                            tr.Tipo_room_nome.Equals("Quadruple") ? mt.S_merc_tarif_tarifa_total / 4 :
                                                            tr.Tipo_room_nome.Equals("Quintuple") ? mt.S_merc_tarif_tarifa_total / 5 :
                                                            tr.Tipo_room_nome.Equals("Sextuple") ? mt.S_merc_tarif_tarifa_total / 6 :
                                                            tr.Tipo_room_nome.Equals("Twin") ? mt.S_merc_tarif_tarifa_total / 2 :
                                mt.S_merc_tarif_id,
                                tr.Tipo_room_nome,
                                tc.Tarif_categoria_nome,
                                mtt.S_mercado_tipo_tarifa_nome,
                                lf.S_mercado_tarifa_status_nome,
                                mt.S_merc_tarif_tarifa
                            }).ToList();
                }
                //return query.ToList();
            }
            catch
            {

                throw;
            }
        }

        public object ListarTodos(DateTime DataIn, DateTime DataOut, int IdSupplier)
        {
            try
            {

                return (from smp in Con.S_Mercado_Periodo
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                        join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                        join smm in Con.S_Mercado_Meal on smp.S_merc_periodo_id equals smm.S_merc_periodo_id
                        join sme in Con.S_Meal on smm.S_meal_id equals sme.S_meal_id
                        join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                        join stt in Con.S_Mercado_Tipo_Tarifa on smt.S_mercado_tipo_tarifa_id equals stt.S_mercado_tipo_tarifa_id
                        join sts in Con.S_Mercado_Tarifa_Status on smm.S_merc_tarif_status_id equals sts.S_mercado_tarifa_status_id into joinDeptEmp
                        from sts in joinDeptEmp.DefaultIfEmpty()
                        where smp.S_merc_periodo_from <= DataIn && smp.S_merc_periodo_to >= DataOut && (smp.S_id == IdSupplier) ||
                              smp.S_merc_periodo_from <= DataOut && smp.S_merc_periodo_to >= DataIn && (smp.S_id == IdSupplier)
                        select new
                        {
                            //smp.S_merc_periodo_from,
                            //smp.S_merc_periodo_to,
                            smt.S_merc_tarif_tarifa_total,
                            tpr.Tipo_room_nome,
                            tcg.Tarif_categoria_nome,
                            mod.Moeda_sigla,
                            //sts.S_mercado_tarifa_status_nome,
                            //smm.S_merc_meal_tarifa,
                            //sme.S_meal_nome,
                            //smm.S_merc_tarif_status_id,
                            //stt.S_mercado_tipo_tarifa_nome,
                            smt.S_merc_tarif_id
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosQuartos(DateTime DataIn, DateTime DataOut, int IdSupplier)
        {
            try
            {
                return (from smp in Con.S_Mercado_Periodo
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                        where smp.S_merc_periodo_from <= DataIn && smp.S_merc_periodo_to >= DataOut && (smp.S_id == IdSupplier) ||
                              smp.S_merc_periodo_from <= DataOut && smp.S_merc_periodo_to >= DataIn && (smp.S_id == IdSupplier)
                        select new
                        {
                            tcg.Tarif_categoria_nome

                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosRoom(DateTime DataIn, DateTime DataOut, int IdSupplier)
        {
            try
            {

                return (from smt in Con.S_Mercado_Tarifa
                        where smt.S_Mercado_Periodo.S_merc_periodo_from <= DataIn &&
                          smt.S_Mercado_Periodo.S_merc_periodo_to >= DataOut &&
                          smt.S_Mercado_Periodo.S_id == IdSupplier ||
                          smt.S_Mercado_Periodo.S_merc_periodo_from <= DataOut &&
                          smt.S_Mercado_Periodo.S_merc_periodo_to >= DataIn &&
                          smt.S_Mercado_Periodo.S_id == IdSupplier
                        group smt.Tipo_Room by new
                        {
                            smt.Tipo_Room.Tipo_room_nome,
                            Tipo_room_id = (int?)smt.Tipo_Room.Tipo_room_id
                        } into g
                        select new
                        {
                            g.Key.Tipo_room_nome,
                            Tipo_room_id = (int?)g.Key.Tipo_room_id
                        }).ToList();

                //return (from smp in Con.S_Mercado_Periodo
                //        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                //        join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                //        where smp.S_merc_periodo_from <= DataIn && smp.S_merc_periodo_to >= DataOut && (smp.S_id == IdSupplier) ||
                //              smp.S_merc_periodo_from <= DataOut && smp.S_merc_periodo_to >= DataIn && (smp.S_id == IdSupplier)
                //        orderby (tpr.Tipo_room_id)
                //        select new
                //        {
                //            tpr.Tipo_room_nome

                //        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosTarifas(DateTime DataIn, DateTime DataOut, int IdSupplier)
        {
            try
            {

                return (from smt in Con.S_Mercado_Tarifa
                        where
                          smt.S_Mercado_Periodo.S_merc_periodo_from <= DataIn &&
                          smt.S_Mercado_Periodo.S_merc_periodo_to >= DataOut &&
                          smt.S_Mercado_Periodo.S_id == IdSupplier ||
                          smt.S_Mercado_Periodo.S_merc_periodo_from <= DataOut &&
                          smt.S_Mercado_Periodo.S_merc_periodo_to >= DataIn &&
                          smt.S_Mercado_Periodo.S_id == IdSupplier
                        group new { smt, smt.Tipo_Room, smt.Tarif_Categoria } by new
                        {
                            smt.S_merc_tarif_tarifa_total,
                            smt.Tipo_Room.Tipo_room_nome,
                            Tipo_room_id = (int?)smt.Tipo_Room.Tipo_room_id,
                            smt.Tarif_Categoria.Tarif_categoria_nome
                        } into g
                        orderby
                          g.Key.Tarif_categoria_nome,
                          g.Key.S_merc_tarif_tarifa_total,
                          g.Key.Tipo_room_id
                        select new
                        {
                            g.Key.Tarif_categoria_nome,
                            g.Key.Tipo_room_nome,
                            S_merc_tarif_tarifa_total = (decimal?)g.Key.S_merc_tarif_tarifa_total,
                            Tipo_room_id = (int?)g.Key.Tipo_room_id
                        }).ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosHoteisTarifas(DateTime DataIn, DateTime DataOut, int IdSupplier, int IdCategoria)
        {
            try
            {

                return (from smp in Con.S_Mercado_Periodo
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                        join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                        join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                        where tcg.Tarif_categoria_id == IdCategoria &&
                              smp.S_merc_periodo_from <= DataIn &&
                              smp.S_merc_periodo_to >= DataOut &&
                              (smp.S_id == IdSupplier) ||
                              tcg.Tarif_categoria_id == IdCategoria &&
                              smp.S_merc_periodo_from <= DataOut &&
                              smp.S_merc_periodo_to >= DataIn &&
                              (smp.S_id == IdSupplier)
                        select new
                        {
                            tcg.Tarif_categoria_nome,
                            tpr.Tipo_room_nome,
                            smt.S_merc_tarif_tarifa_total,
                            smt.S_merc_tarif_id,
                            mod.Moeda_sigla,
                            mod.Moeda_id,
                            tcg.Tarif_categoria_id,
                            tpr.Tipo_room_id,
                            EmailSupplier = smp.Supplier.S_email,
                            S_id = smp.S_id
                        }).ToList();

            }
            catch
            {
                throw;
            }
        }


        public object ListarTodosHoteisTarifas(DateTime DataIn, DateTime DataOut, int IdSupplier, int IdCategoria, string Fds, int Pacote, string Estacao, string nomePacote)
        {
            try
            {

                if (Fds.Equals("") && Pacote == 0)
                {
                    return (from smp in Con.S_Mercado_Periodo
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into joinDeptEmp
                            from sts in joinDeptEmp.DefaultIfEmpty()
                            where tcg.Tarif_categoria_id == IdCategoria &&
                                  smp.S_merc_periodo_from == DataIn &&
                                  smp.S_merc_periodo_to == DataOut &&
                                  smp.S_Mercado_Estacao.S_mercado_estacao_nome.Equals(Estacao) &&
                                  smp.S_id == IdSupplier
                            orderby tpr.Tipo_room_number
                            select new
                            {
                                sts.S_mercado_tarifa_status_nome,
                                tcg.Tarif_categoria_nome,
                                tpr.Tipo_room_nome,
                                smt.S_merc_tarif_tarifa_total,
                                smt.S_merc_tarif_id,
                                mod.Moeda_sigla,
                                mod.Moeda_id,
                                tcg.Tarif_categoria_id,
                                tpr.Tipo_room_id,
                                tpr.Tipo_room_number,
                                smt.S_merc_tarif_porApartamento,
                                EmailSupplier = smp.Supplier.S_email,
                                S_id = smp.S_id
                            }).ToList();
                }
                else if (Fds.Equals("") && Pacote == 1)
                {
                    return (from smp in Con.S_Mercado_Periodo
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into joinDeptEmp
                            from sts in joinDeptEmp.DefaultIfEmpty()
                            where tcg.Tarif_categoria_id == IdCategoria &&
                                  smp.S_merc_periodo_from == DataIn &&
                                  smp.S_merc_periodo_to == DataOut &&
                                  smp.S_Mercado_Estacao.S_mercado_estacao_nome.Equals(Estacao) &&
                                  smp.S_id == IdSupplier &&
                                  smp.Pacote_nome.Equals(nomePacote)
                            orderby tpr.Tipo_room_number
                            select new
                            {
                                sts.S_mercado_tarifa_status_nome,
                                tcg.Tarif_categoria_nome,
                                tpr.Tipo_room_nome,
                                smt.S_merc_tarif_tarifa_total,
                                smt.S_merc_tarif_id,
                                mod.Moeda_sigla,
                                mod.Moeda_id,
                                tcg.Tarif_categoria_id,
                                tpr.Tipo_room_id,
                                tpr.Tipo_room_number,
                                smt.S_merc_tarif_porApartamento,
                                EmailSupplier = smp.Supplier.S_email,
                                S_id = smp.S_id
                            }).ToList();
                }
                else if (!Fds.Equals("") && Pacote == 1)
                {
                    return (from smp in Con.S_Mercado_Periodo
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into joinDeptEmp
                            from sts in joinDeptEmp.DefaultIfEmpty()
                            where tcg.Tarif_categoria_id == IdCategoria &&
                                  smp.S_merc_periodo_from <= DataIn &&
                                  smp.S_merc_periodo_to >= DataOut &&
                                  smp.S_id == IdSupplier &&
                                    smp.S_merc_periodo_fdsFrom.Equals(Fds) &&
                                    smp.S_Mercado_Estacao.S_mercado_estacao_nome.Equals(Estacao) &&
                                    smp.Pacote_nome.Equals(nomePacote)
                            orderby tpr.Tipo_room_number
                            select new
                            {
                                sts.S_mercado_tarifa_status_nome,
                                tcg.Tarif_categoria_nome,
                                tpr.Tipo_room_nome,
                                tpr.Tipo_room_number,
                                smt.S_merc_tarif_tarifa_total,
                                smt.S_merc_tarif_id,
                                mod.Moeda_sigla,
                                mod.Moeda_id,
                                tcg.Tarif_categoria_id,
                                tpr.Tipo_room_id,
                                smt.S_merc_tarif_porApartamento,
                                EmailSupplier = smp.Supplier.S_email,
                                S_id = smp.S_id
                            }).ToList();
                }
                else
                {
                    return (from smp in Con.S_Mercado_Periodo
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into joinDeptEmp
                            from sts in joinDeptEmp.DefaultIfEmpty()
                            where tcg.Tarif_categoria_id == IdCategoria &&
                                  smp.S_merc_periodo_from <= DataIn &&
                                  smp.S_merc_periodo_to >= DataOut &&
                                  (smp.S_id == IdSupplier) &&
                                    smp.S_merc_periodo_fdsFrom.Equals(Fds)
                            orderby tpr.Tipo_room_number
                            select new
                            {
                                sts.S_mercado_tarifa_status_nome,
                                tcg.Tarif_categoria_nome,
                                tpr.Tipo_room_nome,
                                tpr.Tipo_room_number,
                                smt.S_merc_tarif_tarifa_total,
                                smt.S_merc_tarif_id,
                                mod.Moeda_sigla,
                                mod.Moeda_id,
                                tcg.Tarif_categoria_id,
                                tpr.Tipo_room_id,
                                smt.S_merc_tarif_porApartamento,
                                EmailSupplier = smp.Supplier.S_email,
                                S_id = smp.S_id
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<TarifasHotelModel> ListarTodosHoteisTarifas(DateTime DataIn, DateTime DataOut, int IdSupplier, int IdCategoria, string Fds, int Pacote, string Estacao, string nomePacote, int IdResponsavel)
        {
            try
            {

                if (Fds.Equals("") && Pacote == 0)
                {
                    return (from smp in Con.S_Mercado_Periodo
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into joinDeptEmp
                            from sts in joinDeptEmp.DefaultIfEmpty()
                            where tcg.Tarif_categoria_id == IdCategoria &&
                                  smp.S_merc_periodo_from == DataIn &&
                                  smp.S_merc_periodo_to == DataOut &&
                                  smp.S_Mercado_Estacao.S_mercado_estacao_nome.Equals(Estacao) &&
                                  smp.S_id == IdSupplier &&
                                  smp.Responsavel_id == IdResponsavel
                            orderby tpr.Tipo_room_number
                            select new TarifasHotelModel
                            {
                                S_mercado_tarifa_status_nome = sts.S_mercado_tarifa_status_nome,
                                Tarif_categoria_nome = tcg.Tarif_categoria_nome,
                                Tipo_room_nome = tpr.Tipo_room_nome,
                                S_merc_tarif_tarifa_total = smt.S_merc_tarif_tarifa_total == null ? 0 : (decimal)smt.S_merc_tarif_tarifa_total,
                                S_merc_tarif_id = smt.S_merc_tarif_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                Tarif_categoria_id = tcg.Tarif_categoria_id,
                                Tipo_room_id = tpr.Tipo_room_id,
                                Tipo_room_number = (int)tpr.Tipo_room_number,
                                S_merc_tarif_porApartamento = smt.S_merc_tarif_porApartamento == true ? true : false,
                                EmailSupplier = smp.Supplier.S_email,
                                S_id = smp.S_id,
                                S_mercado_tipo_tarifa_nome = smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome,
                                TituloTarifa = "Sistema",
                                S_merc_periodo_fdsFrom = smp.S_merc_periodo_fdsFrom,
                                S_merc_periodo_fdsTo = smp.S_merc_periodo_fdsTo,
                                Pacote_nome = smp.Pacote_nome,
                                S_mercado_estacao_nome = Estacao,
                                Responsavel_id = smp.Responsavel_id == null ? 0 : (int)smp.Responsavel_id,
                                dtfrom = (DateTime)smp.S_merc_periodo_from,
                                dtTo = (DateTime)smp.S_merc_periodo_to
                            }).ToList();
                }
                else if (Fds.Equals("") && Pacote == 1)
                {
                    return (from smp in Con.S_Mercado_Periodo
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into joinDeptEmp
                            from sts in joinDeptEmp.DefaultIfEmpty()
                            where tcg.Tarif_categoria_id == IdCategoria &&
                                  smp.S_merc_periodo_from == DataIn &&
                                  smp.S_merc_periodo_to == DataOut &&
                                  smp.S_Mercado_Estacao.S_mercado_estacao_nome.Equals(Estacao) &&
                                  smp.S_id == IdSupplier &&
                                  smp.Pacote_nome.Equals(nomePacote) &&
                                  smp.Responsavel_id == IdResponsavel
                            orderby tpr.Tipo_room_number
                            select new TarifasHotelModel
                            {

                                S_mercado_tarifa_status_nome = sts.S_mercado_tarifa_status_nome,
                                Tarif_categoria_nome = tcg.Tarif_categoria_nome,
                                Tipo_room_nome = tpr.Tipo_room_nome,
                                S_merc_tarif_tarifa_total = smt.S_merc_tarif_tarifa_total == null ? 0 : (decimal)smt.S_merc_tarif_tarifa_total,
                                S_merc_tarif_id = smt.S_merc_tarif_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                Tarif_categoria_id = tcg.Tarif_categoria_id,
                                Tipo_room_id = tpr.Tipo_room_id,
                                Tipo_room_number = (int)tpr.Tipo_room_number,
                                S_merc_tarif_porApartamento = smt.S_merc_tarif_porApartamento == true ? true : false,
                                EmailSupplier = smp.Supplier.S_email,
                                S_id = smp.S_id,
                                S_mercado_tipo_tarifa_nome = smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome,
                                TituloTarifa = "Sistema",
                                S_merc_periodo_fdsFrom = smp.S_merc_periodo_fdsFrom,
                                S_merc_periodo_fdsTo = smp.S_merc_periodo_fdsTo,
                                Pacote_nome = smp.Pacote_nome,
                                S_mercado_estacao_nome = Estacao,
                                Responsavel_id = smp.Responsavel_id == null ? 0 : (int)smp.Responsavel_id,
                                dtfrom = (DateTime)smp.S_merc_periodo_from,
                                dtTo = (DateTime)smp.S_merc_periodo_to
                            }).ToList();
                }
                else if (!Fds.Equals("") && Pacote == 1)
                {
                    return (from smp in Con.S_Mercado_Periodo
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into joinDeptEmp
                            from sts in joinDeptEmp.DefaultIfEmpty()
                            where tcg.Tarif_categoria_id == IdCategoria &&
                                  smp.S_merc_periodo_from <= DataIn &&
                                  smp.S_merc_periodo_to >= DataOut &&
                                  smp.S_id == IdSupplier &&
                                    smp.S_merc_periodo_fdsFrom.Equals(Fds) &&
                                    smp.S_Mercado_Estacao.S_mercado_estacao_nome.Equals(Estacao) &&
                                    smp.Pacote_nome.Equals(nomePacote) &&
                                    smp.Responsavel_id == IdResponsavel
                            orderby tpr.Tipo_room_number
                            select new TarifasHotelModel
                            {
                                S_mercado_tarifa_status_nome = sts.S_mercado_tarifa_status_nome,
                                Tarif_categoria_nome = tcg.Tarif_categoria_nome,
                                Tipo_room_nome = tpr.Tipo_room_nome,
                                S_merc_tarif_tarifa_total = smt.S_merc_tarif_tarifa_total == null ? 0 : (decimal)smt.S_merc_tarif_tarifa_total,
                                S_merc_tarif_id = smt.S_merc_tarif_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                Tarif_categoria_id = tcg.Tarif_categoria_id,
                                Tipo_room_id = tpr.Tipo_room_id,
                                Tipo_room_number = (int)tpr.Tipo_room_number,
                                S_merc_tarif_porApartamento = smt.S_merc_tarif_porApartamento == true ? true : false,
                                EmailSupplier = smp.Supplier.S_email,
                                S_id = smp.S_id,
                                S_mercado_tipo_tarifa_nome = smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome,
                                TituloTarifa = "Sistema",
                                S_merc_periodo_fdsFrom = smp.S_merc_periodo_fdsFrom,
                                S_merc_periodo_fdsTo = smp.S_merc_periodo_fdsTo,
                                Pacote_nome = smp.Pacote_nome,
                                S_mercado_estacao_nome = Estacao,
                                Responsavel_id = smp.Responsavel_id == null ? 0 : (int)smp.Responsavel_id,
                                dtfrom = (DateTime)smp.S_merc_periodo_from,
                                dtTo = (DateTime)smp.S_merc_periodo_to
                            }).ToList();
                }
                else
                {
                    return (from smp in Con.S_Mercado_Periodo
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into joinDeptEmp
                            from sts in joinDeptEmp.DefaultIfEmpty()
                            where tcg.Tarif_categoria_id == IdCategoria &&
                                  smp.S_merc_periodo_from <= DataIn &&
                                  smp.S_merc_periodo_to >= DataOut &&
                                  (smp.S_id == IdSupplier) &&
                                    smp.S_merc_periodo_fdsFrom.Equals(Fds) &&
                                    smp.Responsavel_id == IdResponsavel
                            orderby tpr.Tipo_room_number
                            select new TarifasHotelModel
                            {
                                S_mercado_tarifa_status_nome = sts.S_mercado_tarifa_status_nome,
                                Tarif_categoria_nome = tcg.Tarif_categoria_nome,
                                Tipo_room_nome = tpr.Tipo_room_nome,
                                S_merc_tarif_tarifa_total = smt.S_merc_tarif_tarifa_total == null ? 0 : (decimal)smt.S_merc_tarif_tarifa_total,
                                S_merc_tarif_id = smt.S_merc_tarif_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                Tarif_categoria_id = tcg.Tarif_categoria_id,
                                Tipo_room_id = tpr.Tipo_room_id,
                                Tipo_room_number = (int)tpr.Tipo_room_number,
                                S_merc_tarif_porApartamento = smt.S_merc_tarif_porApartamento == true ? true : false,
                                EmailSupplier = smp.Supplier.S_email,
                                S_id = smp.S_id,
                                S_mercado_tipo_tarifa_nome = smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome,
                                TituloTarifa = "Sistema",
                                S_merc_periodo_fdsFrom = smp.S_merc_periodo_fdsFrom,
                                S_merc_periodo_fdsTo = smp.S_merc_periodo_fdsTo,
                                Pacote_nome = smp.Pacote_nome,
                                S_mercado_estacao_nome = Estacao,
                                Responsavel_id = smp.Responsavel_id == null ? 0 : (int)smp.Responsavel_id,
                                dtfrom = (DateTime)smp.S_merc_periodo_from,
                                dtTo = (DateTime)smp.S_merc_periodo_to
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosHoteisTarifasLodge(DateTime DataIn, DateTime DataOut, int IdSupplier, int IdCategoria, int NumBase, int IdEstacao, string Fds)
        {
            try
            {


                return (from smp in Con.S_Mercado_Periodo
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        join spp in Con.Supplier on smp.S_id equals spp.S_id
                        join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                        join sba in Con.S_Bases on spp.S_id equals sba.S_id
                        join stb in Con.S_Tarifa_Base on new { MID = (int)smt.S_merc_tarif_id, BID = (int)sba.Base_id } equals new { MID = (int)stb.S_merc_tarif_id, BID = (int)stb.Base_id }
                        join tro in Con.Tipo_Room on smt.Tipo_room_id equals tro.Tipo_room_id
                        join taf in Con.Tarif_Categoria on smt.Tarif_categoria_id equals taf.Tarif_categoria_id
                        join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                        where smp.S_merc_periodo_from == DataIn &&
                              smp.S_merc_periodo_to == DataOut &&
                              spp.S_id == IdSupplier &&
                              sme.S_mercado_estacao_id == IdEstacao &&
                              smt.Tarif_categoria_id == IdCategoria &&
                              sba.Base_de <= NumBase &&
                              sba.Base_ate >= NumBase
                        select new
                        {
                            smt.S_merc_tarif_id,
                            mod.Moeda_id,
                            mod.Moeda_sigla,
                            spp.S_id,
                            tro.Tipo_room_id,
                            spp.S_email,
                            smt.S_merc_tarif_tarifa_total,
                            smp.S_merc_periodo_from,
                            smp.S_merc_periodo_to,
                            spp.S_nome,
                            sba.Base_de,
                            sba.Base_ate,
                            stb.Base_id,
                            tro.Tipo_room_nome,
                            taf.Tarif_categoria_nome,
                            taf.Tarif_categoria_id
                        }).ToList();



            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ListarTodosHoteisTarifasLista(DateTime DataIn, DateTime DataOut, int IdSupplier, int IdRoom, int IdCategoria)
        {
            try
            {

                return (from smp in Con.S_Mercado_Periodo
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                        join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                        join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                        where tpr.Tipo_room_id == IdRoom &&
                              smp.S_merc_periodo_from <= DataIn &&
                              smp.S_merc_periodo_to >= DataOut &&
                              tcg.Tarif_categoria_id == IdCategoria &&
                              (smp.S_id == IdSupplier)
                              ||
                              tpr.Tipo_room_id == IdRoom &&
                              smp.S_merc_periodo_from <= DataOut &&
                              smp.S_merc_periodo_to >= DataIn &&
                              tcg.Tarif_categoria_id == IdCategoria &&
                              (smp.S_id == IdSupplier)
                        select smt).FirstOrDefault();
                //select smt).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Tarifa> ListarTodos()
        {
            try
            {
                using (Model Con = new Model())
                {
                    return Con.S_Mercado_Tarifa.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Tarifa> ListarTodosAptoPessoa()
        {
            try
            {
                using (Model Con = new Model())
                {
                    return Con.S_Mercado_Tarifa.Where(s => s.S_merc_tarif_porApartamento == null && s.S_merc_tarif_porPessoa == null).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Tarifa> ListarTodosLista(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Tarifa.Where(t => t.S_merc_periodo_id == IdPeriodo).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Tarifa> ListarTodosListaReport(int IdPeriodo)
        {
            try
            {
                return (from smt in Con.S_Mercado_Tarifa
                        join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into lfj
                        from sts in lfj.DefaultIfEmpty()
                        where smt.S_merc_periodo_id == IdPeriodo
                        select smt).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Periodo> ListarTudo()
        {
            try
            {


                return (from sup in Con.Supplier
                        join smp in Con.S_Mercado_Periodo on sup.S_id equals smp.S_id
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        select smp).ToList();

            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterPorId(int tarifaID)
        {
            try
            {
                //select * from S_Mercado_Tarifa where tarifaID = ?
                return Con.S_Mercado_Tarifa.Where(c => c.S_merc_tarif_id == tarifaID).Single();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaTarifaExistente(int IdPeriodo)
        {

            return Con.S_Mercado_Tarifa.Where(t => t.S_merc_periodo_id == IdPeriodo).Count() != 0;

        }

        public void Atualizar(S_Mercado_Tarifa registroNovo)
        {
            try
            {

                //Buscar na base de dados registro antigo
                S_Mercado_Tarifa registroAntigo = ObterPorId(registroNovo.S_merc_tarif_id);

                //Atualizando os dados
                registroAntigo.S_merc_tarif_tarifa = registroNovo.S_merc_tarif_tarifa;
                registroAntigo.S_merc_tarif_tarifa_total = registroNovo.S_merc_tarif_tarifa_total;
                registroAntigo.S_merc_tarif_porc_taxa = registroNovo.S_merc_tarif_porc_taxa;
                registroAntigo.S_merca_tarif_porc_imposto = registroNovo.S_merca_tarif_porc_imposto;
                registroAntigo.S_merca_tarif_porc_comissao = registroNovo.S_merca_tarif_porc_comissao;
                registroAntigo.S_merc_tarif_status_id = registroNovo.S_merc_tarif_status_id;
                registroAntigo.Tipo_room_id = registroNovo.Tipo_room_id;
                registroAntigo.Tarif_categoria_id = registroNovo.Tarif_categoria_id;
                registroAntigo.S_mercado_tipo_tarifa_id = registroNovo.S_mercado_tipo_tarifa_id;
                registroAntigo.S_merc_periodo_id = registroNovo.S_merc_periodo_id;
                registroAntigo.S_merc_tarif_porApartamento = registroNovo.S_merc_tarif_porApartamento;
                registroAntigo.S_merc_tarif_porPessoa = registroNovo.S_merc_tarif_porPessoa;

                //executa o commit
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }

        }

        public void AtualizarOrdem(S_Mercado_Tarifa novo)
        {
            try
            {
                S_Mercado_Tarifa antigo = ObterPorId(novo.S_merc_tarif_id);

                antigo.S_merc_tarif_ordem = novo.S_merc_tarif_ordem;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Mercado_Tarifa c)
        {
            try
            {
                Con.S_Mercado_Tarifa.Remove(c); //Prepara a deleção da S_Mercado_Tarifa
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(S_Mercado_Tarifa c)
        {
            try
            {
                Con.S_Mercado_Tarifa.Add(c);
                Con.SaveChanges();
            }
            catch 
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterPrimeira(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Tarifa.Where(s => s.S_merc_periodo_id == IdPeriodo).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterPrimeiraAptoPessoaNotNull(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Tarifa.Where(s => s.S_merc_periodo_id == IdPeriodo && s.S_merc_tarif_porPessoa != null ||
                                                       s.S_merc_periodo_id == IdPeriodo && s.S_merc_tarif_porApartamento != null).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterTarifa(int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote)
        {
            try
            {

                if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote)
                            select smt).FirstOrDefault();
                }
                else
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria)
                            //||
                            //spp.S_id == IdSupplier &&
                            //smp.S_merc_periodo_from <= dtTo &&
                            //smp.S_merc_periodo_to >= dtFrom &&
                            //tpr.Tipo_room_nome.Equals(room) &&
                            //tcg.Tarif_categoria_nome.Equals(categoria)
                            select smt).FirstOrDefault();
                }
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterTarifa2(int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, int BaseDe, int BaseAte)
        {
            try
            {
                if (!String.IsNullOrEmpty(pacote) && new SBasesDAL().ObterPorIdSupplier(IdSupplier).Count() == 0)
                {
                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote)
                                  ||
                                  spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtTo &&
                                  smp.S_merc_periodo_to >= dtFrom &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote)
                            select smt).FirstOrDefault();
                }
                else if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            join tpo in Con.Tipo_Room on smt.Tipo_room_id equals tpo.Tipo_room_id
                            join tca in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tca.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseDe &&
                                 sba.Base_ate >= BaseAte &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria)
                                 ||
                                 spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseAte &&
                                 sba.Base_ate >= BaseDe &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria)
                            orderby sba.Base_de
                            select smt).FirstOrDefault();
                }
                else
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria)
                            //||
                            //spp.S_id == IdSupplier &&
                            //smp.S_merc_periodo_from <= dtTo &&
                            //smp.S_merc_periodo_to >= dtFrom &&
                            //tpr.Tipo_room_nome.Equals(room) &&
                            //tcg.Tarif_categoria_nome.Equals(categoria)
                            select smt).FirstOrDefault();
                }
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterTarifa2_fds(string FdsFrom, int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, int BaseDe, int BaseAte)
        {
            try
            {

                if (!String.IsNullOrEmpty(pacote) && new SBasesDAL().ObterPorIdSupplier(IdSupplier).Count() != 0)
                {
                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            join tpo in Con.Tipo_Room on smt.Tipo_room_id equals tpo.Tipo_room_id
                            join tca in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tca.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseDe &&
                                 sba.Base_ate >= BaseAte &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 smp.S_merc_periodo_fdsFrom.Equals(FdsFrom) &&
                                 tca.Tarif_categoria_nome.Equals(categoria)
                                 ||
                                 spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseAte &&
                                 sba.Base_ate >= BaseDe &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 smp.S_merc_periodo_fdsFrom.Equals(FdsFrom) &&
                                 tca.Tarif_categoria_nome.Equals(categoria)
                            orderby sba.Base_de
                            select smt).FirstOrDefault();
                }
                else
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(FdsFrom) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria)
                            select smt).FirstOrDefault();
                }
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Tarifa> ObterTarifa3(int mercadoID, int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, int BaseDe, int BaseAte, int idResponsavel, string fdsFrom, string fdsTo, string TipoTarifa)
        {
            try
            {
                if (!String.IsNullOrEmpty(pacote) && new SBasesDAL().ObterPorIdSupplier(IdSupplier).Count() == 0)
                {
                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote) && //|| smp.Pacote_nome == null) &&
                                  smp.Responsavel_id == idResponsavel &&
                                  smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                  smp.Mercado_id == mercadoID
                            //||
                            //spp.S_id == IdSupplier &&
                            //smp.S_merc_periodo_from <= dtTo &&
                            //smp.S_merc_periodo_to >= dtFrom &&
                            //tpr.Tipo_room_nome.Equals(room) &&
                            //tcg.Tarif_categoria_nome.Equals(categoria) &&
                            //(smp.Pacote_nome.Equals(pacote) || smp.Pacote_nome == null) &&
                            //smp.Responsavel_id == idResponsavel &&
                            //smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                            //smp.Mercado_id == mercadoID
                            select smt).ToList();
                }
                else if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            join tpo in Con.Tipo_Room on smt.Tipo_room_id equals tpo.Tipo_room_id
                            join tca in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tca.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseDe &&
                                 sba.Base_ate >= BaseAte &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria) &&
                                 smp.Responsavel_id == idResponsavel &&
                                 smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                 smp.Mercado_id == mercadoID
                                 ||
                                 spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseAte &&
                                 sba.Base_ate >= BaseDe &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria) &&
                                 smp.Responsavel_id == idResponsavel &&
                                 smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                 smp.Mercado_id == mercadoID
                            orderby sba.Base_de
                            select smt).ToList();
                }
                else
                {

                    if (!String.IsNullOrEmpty(fdsFrom))
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      //smp.S_merc_periodo_fdsFrom == fdsFrom &&
                                      smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                      smp.Mercado_id == mercadoID
                                ||
                                      spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtTo &&
                                      smp.S_merc_periodo_to >= dtFrom &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      //smp.S_merc_periodo_fdsFrom == fdsFrom &&
                                      smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                      smp.Mercado_id == mercadoID
                                orderby smp.S_merc_periodo_from
                                select smt).ToList();
                    }
                    else
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                      smp.Mercado_id == mercadoID
                                ||
                                      spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtTo &&
                                      smp.S_merc_periodo_to >= dtFrom &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                      smp.Mercado_id == mercadoID
                                orderby smp.S_merc_periodo_from
                                select smt).ToList();
                    }
                }


            }
            catch
            {

                throw;
            }
        }


        public List<S_Mercado_Tarifa> ObterTarifa3(int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, int BaseDe, int BaseAte, int idResponsavel, string fdsFrom, string fdsTo)
        {
            try
            {
                if (!String.IsNullOrEmpty(pacote) && new SBasesDAL().ObterPorIdSupplier(IdSupplier).Count() == 0)
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote) &&
                                  smp.Responsavel_id == idResponsavel
                                  ||
                                  spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtTo &&
                                  smp.S_merc_periodo_to >= dtFrom &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote) &&
                                  smp.Responsavel_id == idResponsavel
                            select smt).ToList();
                }
                else if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            join tpo in Con.Tipo_Room on smt.Tipo_room_id equals tpo.Tipo_room_id
                            join tca in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tca.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseDe &&
                                 sba.Base_ate >= BaseAte &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria) &&
                                 smp.Responsavel_id == idResponsavel
                                 ||
                                 spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseAte &&
                                 sba.Base_ate >= BaseDe &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria) &&
                                 smp.Responsavel_id == idResponsavel
                            orderby sba.Base_de
                            select smt).ToList();
                }
                else
                {

                    if (!String.IsNullOrEmpty(fdsFrom))
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      smp.S_merc_periodo_fdsFrom == fdsFrom
                                ||
                                      spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtTo &&
                                      smp.S_merc_periodo_to >= dtFrom &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      smp.S_merc_periodo_fdsFrom == fdsFrom
                                orderby smp.S_merc_periodo_from
                                select smt).ToList();
                    }
                    else
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel
                                ||
                                      spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtTo &&
                                      smp.S_merc_periodo_to >= dtFrom &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel
                                orderby smp.S_merc_periodo_from
                                select smt).ToList();
                    }
                }

                //return (from spp in Con.Supplier
                //            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                //            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                //            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                //            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id                            
                //            where spp.S_id == IdSupplier &&
                //                  smp.S_merc_periodo_from <= dtFrom &&
                //                  smp.S_merc_periodo_to >= dtTo &&
                //                  tpr.Tipo_room_nome.Equals(room) &&
                //                  tcg.Tarif_categoria_nome.Equals(categoria)
                //            ||
                //                  spp.S_id == IdSupplier &&
                //                  smp.S_merc_periodo_from <= dtTo &&
                //                  smp.S_merc_periodo_to >= dtFrom &&
                //                  tpr.Tipo_room_nome.Equals(room) &&
                //                  tcg.Tarif_categoria_nome.Equals(categoria)
                //                  orderby smp.S_merc_periodo_from
                //            select smt).ToList();                
            }
            catch
            {

                throw;
            }
        }

        public List<S_Mercado_Tarifa> ObterTarifa4(int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, int BaseDe, int BaseAte, int idResponsavel, string fdsFrom, string fdsTo)
        {
            try
            {
                if (!String.IsNullOrEmpty(pacote) && new SBasesDAL().ObterPorIdSupplier(IdSupplier).Count() == 0)
                {

                    if (!String.IsNullOrEmpty(fdsFrom))
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&                                      
                                      smp.Responsavel_id == idResponsavel                                
                                select smt).ToList();
                    }

                        return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote) && //|| smp.Pacote_nome == null) &&
                                  smp.Responsavel_id == idResponsavel 
                            //||
                            //spp.S_id == IdSupplier &&
                            //smp.S_merc_periodo_from <= dtTo &&
                            //smp.S_merc_periodo_to >= dtFrom &&
                            //tpr.Tipo_room_nome.Equals(room) &&
                            //tcg.Tarif_categoria_nome.Equals(categoria) &&
                            //(smp.Pacote_nome.Equals(pacote) || smp.Pacote_nome == null) &&
                            //smp.Responsavel_id == idResponsavel &&
                            //smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                            //smp.Mercado_id == mercadoID
                            select smt).ToList();
                }
                else if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            join tpo in Con.Tipo_Room on smt.Tipo_room_id equals tpo.Tipo_room_id
                            join tca in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tca.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseDe &&
                                 sba.Base_ate >= BaseAte &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria) &&
                                 smp.Responsavel_id == idResponsavel 
                                 ||
                                 spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseAte &&
                                 sba.Base_ate >= BaseDe &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria) &&
                                 smp.Responsavel_id == idResponsavel 
                            orderby sba.Base_de
                            select smt).ToList();
                }
                else
                {

                    if (!String.IsNullOrEmpty(fdsFrom))
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel 
                                     
                                ||
                                      spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtTo &&
                                      smp.S_merc_periodo_to >= dtFrom &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel 
                                    
                                orderby smp.S_merc_periodo_from
                                select smt).ToList();
                    }
                    else
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel 
                                ||
                                      spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtTo &&
                                      smp.S_merc_periodo_to >= dtFrom &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel 
                                orderby smp.S_merc_periodo_from
                                select smt).ToList();
                    }
                }


            }
            catch
            {

                throw;
            }
        }

        public List<S_Mercado_Tarifa> ObterTarifa4(int mercadoID, int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, int BaseDe, int BaseAte, int idResponsavel, string fdsFrom, string fdsTo, string TipoTarifa, string estacao)
        {
            try
            {
                if (!String.IsNullOrEmpty(pacote) && new SBasesDAL().ObterPorIdSupplier(IdSupplier).Count() == 0)
                {
                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote) && //|| smp.Pacote_nome == null) &&
                                  smp.Responsavel_id == idResponsavel &&
                                  smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                  smp.Mercado_id == mercadoID
                            //||
                            //spp.S_id == IdSupplier &&
                            //smp.S_merc_periodo_from <= dtTo &&
                            //smp.S_merc_periodo_to >= dtFrom &&
                            //tpr.Tipo_room_nome.Equals(room) &&
                            //tcg.Tarif_categoria_nome.Equals(categoria) &&
                            //(smp.Pacote_nome.Equals(pacote) || smp.Pacote_nome == null) &&
                            //smp.Responsavel_id == idResponsavel &&
                            //smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                            //smp.Mercado_id == mercadoID
                            select smt).ToList();
                }
                else if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            join tpo in Con.Tipo_Room on smt.Tipo_room_id equals tpo.Tipo_room_id
                            join tca in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tca.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseDe &&
                                 sba.Base_ate >= BaseAte &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria) &&
                                 smp.Responsavel_id == idResponsavel &&
                                 smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                 smp.Mercado_id == mercadoID
                                 ||
                                 spp.S_id == IdSupplier &&
                                 smp.S_merc_periodo_from <= dtFrom &&
                                 smp.S_merc_periodo_to >= dtTo &&
                                 smp.Pacote_nome.Equals(pacote) &&
                                 sba.Base_de <= BaseAte &&
                                 sba.Base_ate >= BaseDe &&
                                 tpo.Tipo_room_nome.Equals(room) &&
                                 tca.Tarif_categoria_nome.Equals(categoria) &&
                                 smp.Responsavel_id == idResponsavel &&
                                 smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                 smp.Mercado_id == mercadoID
                            orderby sba.Base_de
                            select smt).ToList();
                }
                else if (!String.IsNullOrEmpty(estacao))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  smp.S_Mercado_Estacao.S_mercado_estacao_nome.Equals(estacao) &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Responsavel_id == idResponsavel &&
                                  smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                  smp.Mercado_id == mercadoID
                            ||
                                  spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtTo &&
                                  smp.S_merc_periodo_to >= dtFrom &&
                                  smp.S_Mercado_Estacao.S_mercado_estacao_nome.Equals(estacao) &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Responsavel_id == idResponsavel &&
                                  smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                  smp.Mercado_id == mercadoID
                            orderby smp.S_merc_periodo_from
                            select smt).ToList();

                }
                else
                {

                    if (!String.IsNullOrEmpty(fdsFrom))
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      //smp.S_merc_periodo_fdsFrom == fdsFrom &&
                                      smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                      smp.Mercado_id == mercadoID
                                ||
                                      spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtTo &&
                                      smp.S_merc_periodo_to >= dtFrom &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      //smp.S_merc_periodo_fdsFrom == fdsFrom &&
                                      smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                      smp.Mercado_id == mercadoID
                                orderby smp.S_merc_periodo_from
                                select smt).ToList();
                    }
                    else
                    {
                        return (from spp in Con.Supplier
                                join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                                join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                                join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                                join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                                where spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtFrom &&
                                      smp.S_merc_periodo_to >= dtTo &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                      smp.Mercado_id == mercadoID
                                ||
                                      spp.S_id == IdSupplier &&
                                      smp.S_merc_periodo_from <= dtTo &&
                                      smp.S_merc_periodo_to >= dtFrom &&
                                      tpr.Tipo_room_nome.Equals(room) &&
                                      tcg.Tarif_categoria_nome.Equals(categoria) &&
                                      smp.Responsavel_id == idResponsavel &&
                                      smt.S_Mercado_Tipo_Tarifa.S_mercado_tipo_tarifa_nome.Equals(TipoTarifa) &&
                                      smp.Mercado_id == mercadoID
                                orderby smp.S_merc_periodo_from
                                select smt).ToList();
                    }
                }


            }
            catch
            {

                throw;
            }
        }

        public S_Mercado_Tarifa ObterTarifa(int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, string fdsFrom)
        {
            try
            {

                if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(fdsFrom)
                            select smt).SingleOrDefault();
                }
                else
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(fdsFrom)
                                  ||
                                  spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtTo &&
                                  smp.S_merc_periodo_to >= dtFrom &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(fdsFrom)
                            select smt).SingleOrDefault();
                }
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterTarifaBase(int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, int BaseDe)
        {
            try
            {

                if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote) &&
                                  sba.Base_de <= BaseDe &&
                                  sba.Base_ate >= BaseDe
                            select smt).SingleOrDefault();
                }
                else
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  sba.Base_de <= BaseDe &&
                                  sba.Base_ate >= BaseDe
                                  ||
                                  spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtTo &&
                                  smp.S_merc_periodo_to >= dtFrom &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  sba.Base_de <= BaseDe &&
                                  sba.Base_ate >= BaseDe
                            select smt).SingleOrDefault();
                }
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterTarifaBase(int IdSupplier, DateTime dtFrom, DateTime dtTo, string room, string categoria, string pacote, string fdsFrom, int BaseDe)
        {
            try
            {
                if (!String.IsNullOrEmpty(pacote))
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.Pacote_nome.Equals(pacote) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(fdsFrom) &&
                                  sba.Base_de <= BaseDe &&
                                  sba.Base_ate >= BaseDe
                            select smt).SingleOrDefault();

                }
                else
                {

                    return (from spp in Con.Supplier
                            join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                            join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join stb in Con.S_Tarifa_Base on smt.S_merc_tarif_id equals stb.S_merc_tarif_id
                            join sba in Con.S_Bases on stb.Base_id equals sba.Base_id
                            where spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtFrom &&
                                  smp.S_merc_periodo_to >= dtTo &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(fdsFrom) &&
                                  sba.Base_de <= BaseDe &&
                                  sba.Base_ate >= BaseDe
                                  ||
                                  spp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from <= dtTo &&
                                  smp.S_merc_periodo_to >= dtFrom &&
                                  tpr.Tipo_room_nome.Equals(room) &&
                                  tcg.Tarif_categoria_nome.Equals(categoria) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(fdsFrom) &&
                                  sba.Base_de <= BaseDe &&
                                  sba.Base_ate >= BaseDe
                            select smt).SingleOrDefault();
                }
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Tarifa> ObterTarifa_OMNI(int idPeriodo, int idCategoria)
        {
            try
            {
                return Con.S_Mercado_Tarifa.Where(s => s.S_merc_periodo_id == idPeriodo &&
                                                       s.Tarif_categoria_id == idCategoria).ToList();
            }
            catch 
            {
                throw;
            }
        }

    }
}
