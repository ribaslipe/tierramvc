﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class OrdemPagamentoItemDAL
    {
        private Model Entity;

        public OrdemPagamentoItemDAL()
        {
            Entity = new Model();
        }

        public int Adicionar(Ordem_Pagamento_Itens opi)
        {
            Entity.Ordem_Pagamento_Itens.Add(opi);
            Entity.SaveChanges();

            return opi.OrdemPagamento_Itens_ID;
        }
    }
}
