﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;

namespace DAL.Persistencia
{
    public class FotoDAL
    {
        private Model Con;

        public FotoDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Image c)
        {
            try
            {

                //Rotinas prontas para o banco de dados
                Con.S_Image.Add(c); //insert into S_Image values...
                Con.SaveChanges(); //Executar a operação

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Image c)
        {
            try
            {
                Con.S_Image.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        public List<S_Image> ListarTodos()
        {
            try
            {
                //SQL -> select * from S_Mercado_Meal
                return Con.S_Image.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Image> ListarTodosLista(int IdSupplier)
        {
            try
            {

                return Con.S_Image.Where(i => i.S_id == IdSupplier).ToList();

            }
            catch
            {
                throw;
            }
        }

        public S_Image ObterPorSupplierCapa(int IdSupplier)
        {
            try
            {
                return Con.S_Image.Where(i => i.S_id == IdSupplier && i.Image_capa.Equals("S")).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(int IdSupplier)
        {
            try
            {
                //SQL -> select * from S_Mercado_Meal
                return Con.S_Image.Where(i => i.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public S_Image ObterPorId(int IdSimage)
        {
            try
            {
                //select * from TbCliente where IdCliente = ?
                return Con.S_Image.Where(c => c.Image_id == IdSimage).SingleOrDefault();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(S_Image registroNovo)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                S_Image registroAntigo = ObterPorId(registroNovo.Image_id);

                //Atualizando os dados
                registroAntigo.Image_nome = registroNovo.Image_nome;
                registroAntigo.Image_capa = registroNovo.Image_capa;
                registroAntigo.Image_memo = registroNovo.Image_memo;
                registroAntigo.Image_hupload = registroNovo.Image_hupload;
                registroAntigo.Image_descr = registroNovo.Image_descr;
                registroAntigo.S_id = registroNovo.S_id;

                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void ImagemPrincipal(int SupplierID)
        {
            try
            {
                FotoDAL i = new FotoDAL();

                S_Image Foto = Con.S_Image.Where(f => f.Image_capa.Equals("S") && f.S_id.Equals(SupplierID)).SingleOrDefault();

                if (Foto == null)
                {
                    Foto = Con.S_Image.Where(f => f.S_id.Equals(SupplierID)).First();
                    Foto.Image_capa = "S";
                    i.Atualizar(Foto);
                }
            }
            catch
            {
                throw;
            }
        }

        public void AddImagemPrincipal(int IdSupplier, int IdImagem)
        {
            try
            {

                FotoDAL f = new FotoDAL();

                S_Image FotoAntiga = Con.S_Image.Where(i => i.S_id == IdSupplier && i.Image_capa.Equals("S")).SingleOrDefault();

                if (FotoAntiga != null)
                {
                    FotoAntiga.Image_capa = "N";
                    f.Atualizar(FotoAntiga);
                }

                S_Image FotoNova = Con.S_Image.Where(i => i.Image_id == IdImagem).Single();
                FotoNova.Image_capa = "S";
                f.Atualizar(FotoNova);

            }
            catch
            {
                throw;
            }
        }

        public void AtualizarMemo(S_Image novo)
        {
            try
            {

                S_Image antigo = ObterPorId(novo.Image_id);

                antigo.Image_memo = novo.Image_memo;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

    }
}
