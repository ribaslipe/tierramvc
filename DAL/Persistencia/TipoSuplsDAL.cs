﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class TipoSuplsDAL
    {
        private Model Con;

        public TipoSuplsDAL()
        {
            Con = new Model();
        }

        public List<TipoSupl> ListarTodos()
        {
            try
            {
                return Con.TipoSupl.ToList().OrderBy(t => t.TIPOSUPL_nome).ToList();
            }
            catch 
            {

                throw;
            }
           
        }
    }
}
