﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;


namespace DAL.Persistencia
{
    public class DescritivoPacoteDAL
    {

        private Model Con;

        public DescritivoPacoteDAL()
        {
            Con = new Model();
        }

        public void Salvar(Descritivo_Pacote d)
        {
            try
            {
                Con.Descritivo_Pacote.Add(d);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Descritivo_Pacote ObterPorId(int IdDescr)
        {
            try
            {
                return Con.Descritivo_Pacote.Where(d => d.Descritivo_id == IdDescr).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public Descritivo_Pacote ObterPorIdPeriodo(int IdPeriodo)
        {
            try
            {
                return Con.Descritivo_Pacote.Where(d => d.S_merc_periodo_id == IdPeriodo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Descritivo_Pacote d)
        {
            try
            {
                Con.Descritivo_Pacote.Remove(d);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(Descritivo_Pacote novo)
        {
            try
            {
                Descritivo_Pacote antigo = ObterPorId(novo.Descritivo_id);

                antigo.Descritivo_descr0 = novo.Descritivo_descr0;
                antigo.Descritivo_descr1 = novo.Descritivo_descr1;
                antigo.Descritivo_descr2 = novo.Descritivo_descr2;
                antigo.Descritivo_descr3 = novo.Descritivo_descr3;
                antigo.Descritivo_descr4 = novo.Descritivo_descr4;
                antigo.Descritivo_descr5 = novo.Descritivo_descr5;
                antigo.Descritivo_descr6 = novo.Descritivo_descr6;
                antigo.Descritivo_descr7 = novo.Descritivo_descr7;
                antigo.Descritivo_descr8 = novo.Descritivo_descr8;
                antigo.Descritivo_descr9 = novo.Descritivo_descr9;

                antigo.Descritivo_descr10 = novo.Descritivo_descr10;
                antigo.Descritivo_descr11 = novo.Descritivo_descr11;
                antigo.Descritivo_descr12 = novo.Descritivo_descr12;
                antigo.Descritivo_descr13 = novo.Descritivo_descr13;
                antigo.Descritivo_descr14 = novo.Descritivo_descr14;
                antigo.Descritivo_descr15 = novo.Descritivo_descr15;
                antigo.Descritivo_descr16 = novo.Descritivo_descr16;
                antigo.Descritivo_descr17 = novo.Descritivo_descr17;
                antigo.Descritivo_descr18 = novo.Descritivo_descr18;
                antigo.Descritivo_descr19 = novo.Descritivo_descr19;
                antigo.Descritivo_descr20 = novo.Descritivo_descr20;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaPeriodo(int IdPeriodo)
        {
            try
            {
                return Con.Descritivo_Pacote.Where(d => d.S_merc_periodo_id == IdPeriodo).Count() != 0;
            }
            catch 
            {                
                throw;
            }
        }

        public Descritivo_Pacote ObterPacote(DateTime DataFrom, DateTime DataTo, int IdSupplier, string PacNome)
        {

            try
            {
                return (from smp in Con.S_Mercado_Periodo
                        join dpc in Con.Descritivo_Pacote on smp.S_merc_periodo_id equals dpc.S_merc_periodo_id
                        where smp.S_merc_periodo_from <= DataFrom &&
                              smp.S_merc_periodo_to >= DataTo &&
                              smp.S_id == IdSupplier &&
                              smp.Pacote_nome.Equals(PacNome)
                        select dpc).SingleOrDefault();
            }
            catch
            {
                
                throw;
            }

        }

    }
}
