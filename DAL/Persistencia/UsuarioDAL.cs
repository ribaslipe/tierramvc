﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class UsuarioDAL
    {
        private Model Con;

        public UsuarioDAL()
        {
            Con = new Model();
        }

        public Usuarios ObterPorId(int IdUsuario)
        {
            try
            {
                return Con.Usuarios.Where(u => u.US_id == IdUsuario).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Usuarios ObterPorEmail(string Email)
        {
            try
            {
                return Con.Usuarios.Where(u => u.US_email == Email).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Usuarios ObterUsuario(string email, string senha)
        {
            try
            {
                return Con.Usuarios.Where(u => u.US_email.Equals(email) &&
                            u.US_senha.Equals(senha)).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public int Salvar(Usuarios u)
        {
            try
            {
                Con.Usuarios.Add(u);
                Con.SaveChanges();
                return u.US_id;
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Usuarios novo)
        {
            try
            {
                Usuarios antigo = ObterPorId(novo.US_id);

                antigo.US_nome = novo.US_nome;
                antigo.US_senha = novo.US_senha;
                antigo.US_email = novo.US_email;
                antigo.US_status = novo.US_status;
                antigo.Perfil_id = novo.Perfil_id;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public bool VerificaEmail(string email)
        {
            try
            {
                return Con.Usuarios.Where(u => u.US_email.Equals(email)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public List<Usuarios> ListarTodos()
        {
            try
            {
                return Con.Usuarios.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Usuarios> ListarPesquisa(string nome)
        {
            try
            {
                if (nome.Trim().Equals(""))
                    return Con.Usuarios.ToList();
                else
                    return Con.Usuarios.Where(a => a.US_nome.Contains(nome)).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Excluir(Usuarios u)
        {
            try
            {
                Con.Usuarios.Remove(u);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Usuario_Perfil> ObterPerfis()
        {
            try
            {
                return Con.Usuario_Perfil.OrderBy(u => u.Perfil_tipo).ToList();
            }
            catch
            {
                throw;
            }
        }
    }
}
