﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Models;

namespace DAL.Persistencia
{
    public class FileTarifasDAL
    {
        private Model Con;

        public FileTarifasDAL()
        {
            Con = new Model();
        }

        public List<File_Tarifas> ListarTodos_Spp(int IdSupplier)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodosNew(int IdFile)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Range_id == null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodos(int IdFile)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Range_id == null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodosRangeZero(int IdFile)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Range_id != null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodosOpt(int IdFile, int Opt)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.OptQuote == Opt).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodosOrderBy(int IdFile)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Range_id == null).OrderBy(f => f.Data_From).ThenBy(f => f.Ordem).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodosPorOrdem(int IdFile, int Ordem)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Ordem >= Ordem).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodos(string SPnome)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.S_nome.Equals(SPnome)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizaTodosNomePac(string NPacAnt, string NPacNov, DateTime dtFrom, DateTime dtTo, int IdSupp)
        {
            try
            {

                //Con.File_Tarifas.Where(f => f.NomePacote.Equals(NPacAnt)).ToList().ForEach(f => f.NomePacote = NPacNov);

                Con.File_Tarifas.Where(f => f.NomePacote.Equals(NPacAnt) &&
                                            f.Data_From <= dtFrom &&
                                            f.Data_To >= dtTo &&
                                            f.S_id == IdSupp
                                            ||
                                            f.NomePacote.Equals(NPacAnt) &&
                                            f.Data_From <= dtTo &&
                                            f.Data_To >= dtFrom &&
                                            f.S_id == IdSupp).ToList().ForEach(f => f.NomePacote = NPacNov);
                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodos(int IdFile, int IdSupplier)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSuppliers(int IdFile)
        {
            try
            {
                return (from flt in Con.File_Tarifas
                        where flt.File_id == IdFile
                        select new
                        {
                            flt.S_nome,
                            flt.S_id
                        }).ToList().Distinct();
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(File_Tarifas f)
        {
            try
            {
                Con.File_Tarifas.Add(f);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public File_Tarifas ObterPorId(int IdFileTarifa)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_Tarifas_id == IdFileTarifa).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(File_Tarifas f)
        {
            try
            {
                Con.File_Tarifas.Remove(f);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNew(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Qtd_Tarifas = novo.Qtd_Tarifas;
                antigo.Paying_Pax = novo.Paying_Pax;
                antigo.Supp_Paying_Pax = novo.Supp_Paying_Pax;
                antigo.Paxs = novo.Paxs;               

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Qtd_Tarifas = novo.Qtd_Tarifas;
                antigo.markup = novo.markup;
                antigo.markupNet = novo.markupNet;
                antigo.descontoNet = novo.descontoNet;
                antigo.desconto = novo.desconto;
                antigo.S_merc_tarif_valor = novo.S_merc_tarif_valor;
                antigo.S_merc_tarif_venda = novo.S_merc_tarif_venda;
                antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.S_merc_tarif_total = novo.S_merc_tarif_total;
                antigo.UpdVenda = novo.UpdVenda;


                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarVenda(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.markup = novo.markup;
                antigo.S_merc_tarif_venda = novo.S_merc_tarif_venda;
                antigo.S_merc_tarif_total = novo.S_merc_tarif_total;
                antigo.UpdVenda = true;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarVendaTotal(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.S_merc_tarif_venda = novo.S_merc_tarif_venda;
                //antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.S_merc_tarif_total = novo.S_merc_tarif_total;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarStatus(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Status = novo.Status;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarBulk(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.TBulk = novo.TBulk;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOptional(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.TOptional = novo.TOptional;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarFlight(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Flights_id = novo.Flights_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarPaxs(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Qtd_Tarifas = novo.Qtd_Tarifas;
                antigo.Paxs = novo.Paxs;
                antigo.Paying_Pax = novo.Paying_Pax;
                antigo.Supp_Paying_Pax = novo.Supp_Paying_Pax;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOrdem(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Ordem = novo.Ordem;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarSNome(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.S_nome = novo.S_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarRange(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Range_id = novo.Range_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarDatas(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Data_From = novo.Data_From;
                antigo.Data_To = novo.Data_To;

                if (novo.NumNoites != null)
                    antigo.NumNoites = novo.NumNoites;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarPlatNome(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Plataforma = novo.Plataforma;                
            
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNNoites(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.NumNoites = novo.NumNoites;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem(int IdFile)
        {
            try
            {
                var item = Con.File_Tarifas.Where(f => f != null && f.File_id == IdFile).Max(f => f.Ordem);

                if (item != null)
                {
                    return Convert.ToInt32(item);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem_(int IdFile)
        {
            try
            {
                var item = Con.File_Tarifas.Where(f => f != null && f.File_id == IdFile).OrderByDescending(s => s.Ordem).FirstOrDefault();

                if (item != null)
                {
                    return Convert.ToInt32(item.Ordem);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMeal(int IdFile, int IdMeal)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Meal == true &&
                                                   f.S_meal_id == IdMeal).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa RetornaNovoValor(int IdSupp, string CategNome, string tipoRoom, DateTime dtfrom, DateTime dtto)
        {
            try
            {

                return (from spp in Con.Supplier
                        join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                        join tca in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tca.Tarif_categoria_id
                        where spp.S_id == IdSupp &&
                              tca.Tarif_categoria_nome.Equals(CategNome) &&
                              tpr.Tipo_room_nome.Equals(tipoRoom) &&
                              smp.S_merc_periodo_from <= dtfrom &&
                              smp.S_merc_periodo_to >= dtto
                        select smt).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public List<string> RetornaRooms(int IdFile)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Range_id == 0).Select(f => f.Room).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<string> RetornaRooms(int IdFile, int optQuote)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Range_id == 0 &&
                                                   f.OptQuote == optQuote &&
                                                   f.Meal == null).Select(f => f.Room).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<RoomsQuoteModel> RetornaRooms_(int IdFile, int optQuote)
        {
            try
            {
                return (from f in Con.File_Tarifas
                        where f.File_id == IdFile &&
                              f.Range_id == 0 &&
                              f.OptQuote == optQuote &&
                              f.Meal == null &&
                              f.Room != null
                        select new RoomsQuoteModel
                        {
                            idRoom = (int)f.Room_id,
                            NomeRoom = f.Room
                        }).OrderBy(s => s.idRoom).Distinct().ToList();


                //return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                //                                   f.Range_id == 0 &&
                //                                   f.OptQuote == optQuote &&
                //                                   f.Meal == null &&
                //                                   f.Room != null).OrderBy(s => s.Room_id).Select(f => f.Room).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public int[] RetornaRooms_id(int IdFile, int optQuote)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.Range_id == 0 &&
                                                   f.OptQuote == optQuote &&
                                                   f.Meal == null).Select(f => (int)f.Room_id).Distinct().ToArray();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarDatas_NumNoites(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Data_From = novo.Data_From;
                antigo.Data_To = novo.Data_To;
                antigo.NumNoites = novo.NumNoites;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarStatus_Plataforma(File_Tarifas novo)
        {
            try
            {
                File_Tarifas antigo = ObterPorId(novo.File_Tarifas_id);

                antigo.Status = novo.Status;
                antigo.Plataforma_id = novo.Plataforma_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> RetornaOmni_NoOpt(int idFile, int optQuote)
        {
            try
            {
                return Con.File_Tarifas.Where(s => s.Plataforma_id != null && s.OptQuote != optQuote).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_Tarifas> ListarTodosOptional(int IdFile, int OptQuote)
        {
            try
            {
                return Con.File_Tarifas.Where(f => f.File_id == IdFile &&
                                                     f.OptQuote == OptQuote &&
                                                     f.TOptional == true).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
