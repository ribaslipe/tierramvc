﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Persistencia;

namespace DAL.Persistencia
{
    public class CategDescricaoDAL
    {

        private Model Con;

        public CategDescricaoDAL()
        {
            Con = new Model();
        }

        public void Salvar(CategDescricao c)
        {
            try
            {

                Con.CategDescricao.Add(c);
                Con.SaveChanges();

            }
            catch 
            {                
                throw;
            }
        }

        public CategDescricao ObterPorId(int IdCategDesc)
        {
            try
            {
                return Con.CategDescricao.Where(c => c.CategDesc_id == IdCategDesc).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public CategDescricao ObterPorSupCateg(int IdSupplier, int IdCateg)
        {
            try
            {
                return Con.CategDescricao.Where(c => c.S_id == IdSupplier && c.Tarif_categoria_id == IdCateg).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(CategDescricao novo)
        {
            try
            {
                CategDescricao antigo = ObterPorId(novo.CategDesc_id);

                antigo.CategDesc = novo.CategDesc;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(CategDescricao c)
        {
            try
            {
                Con.CategDescricao.Remove(c);
                Con.SaveChanges();
            }
            catch
            {                
                throw;
            }
        }

    }

}
