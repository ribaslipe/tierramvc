﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class STarifaBaseDAL
    {

        private Model Con;

        public STarifaBaseDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Tarifa_Base s)
        {
            try
            {
                Con.S_Tarifa_Base.Add(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Tarifa_Base ObterPorId(int IdBase)
        {
            try
            {
                return Con.S_Tarifa_Base.Where(s => s.Base_id == IdBase).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Tarifa_Base ObterPorIdTarifa(int IdTarifa)
        {
            try
            {
                return Con.S_Tarifa_Base.Where(s => s.S_merc_tarif_id == IdTarifa).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Tarifa_Base ObterPorIdMeal(int IdMeal)
        {
            try
            {
                return Con.S_Tarifa_Base.Where(s => s.S_merc_meal_id == IdMeal).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(S_Tarifa_Base s)
        {
            try
            {
                Con.S_Tarifa_Base.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
