﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class OrdemPagamentoDAL
    {
        private Model Con;

        public OrdemPagamentoDAL()
        {
            Con = new Model();
        }

        public int Adicionar(Ordem_Pagamento op)
        {
            Con.Ordem_Pagamento.Add(op);
            Con.SaveChanges();

            return op.OrdemPagamento_ID;
        }
    }
}
