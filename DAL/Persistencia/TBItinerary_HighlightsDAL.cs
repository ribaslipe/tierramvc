﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class TBItinerary_HighlightsDAL
    {
        private Model Con;

        public TBItinerary_HighlightsDAL()
        {
            Con = new Model();
        }

        public void Adicionar(TBItinerary_Highlights tbih)
        {
            try
            {
                Con.TBItinerary_Highlights.Add(tbih);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public TBItinerary_Highlights Obter(int Id)
        {
            return Con.TBItinerary_Highlights.Where(a => a.Id == Id).SingleOrDefault();
        }

        public List<TBItinerary_Highlights> ListarTodos(string code, int optQuote)
        {
            return Con.TBItinerary_Highlights.Where(a => a.Code == code && a.optQuote == optQuote && a.Visible == true).ToList();
        }

        public bool CheckExists(string highlight, string quotationCode, int optQuote)
        {
            return Con.TBItinerary_Highlights.Count(a => a.Highlight == highlight && a.Code == quotationCode && a.optQuote == optQuote) > 0;
        }

        public List<TBItinerary_Highlights> ListarPorCidade_(int cityId, string _quotationCode, int optQuote)
        {
            try
            {
                return Con.TBItinerary_Highlights.Where(a => a.Cidade_ID == cityId
                                                            && a.Code == _quotationCode
                                                            && a.optQuote == optQuote).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TBItinerary_Highlights> ListarPorCidade(int cityId, string _quotationCode, int optQuote)
        {
            try
            {
                return Con.TBItinerary_Highlights.Where(a => a.Cidade_ID == cityId
                                                            && a.Code == _quotationCode
                                                            && a.optQuote == optQuote
                                                            && a.Visible == true).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TBItinerary_Highlights> ListarPorCidade_False(int cityId, string _quotationCode, int optQuote)
        {
            try
            {
                return Con.TBItinerary_Highlights.Where(a => a.Cidade_ID == cityId
                                                            && a.Code == _quotationCode
                                                            && a.optQuote == optQuote
                                                            && a.Visible == false).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TBItinerary_Highlights> ListarSemCidade(string _quotationCode, int optQuote)
        {
            return Con.TBItinerary_Highlights.Where(a =>    a.Code == _quotationCode && 
                                                            a.Visible == true && 
                                                            a.optQuote == optQuote &&
                                                            (a.Cidade_ID == null || a.Cidade_ID == 0)).ToList();
        }

        public void Editar(TBItinerary_Highlights tbih)
        {
            try
            {
                var tbih_old = Con.TBItinerary_Highlights.Where(a => a.Id == tbih.Id).SingleOrDefault();

                tbih_old.Highlight = tbih.Highlight;

                Con.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EditarVisible(TBItinerary_Highlights tbih)
        {
            try
            {
                var tbih_old = Con.TBItinerary_Highlights.Where(a => a.Id == tbih.Id).SingleOrDefault();

                tbih_old.Visible = tbih.Visible;

                Con.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Excluir(TBItinerary_Highlights tbih)
        {
            try
            {
                Con.TBItinerary_Highlights.Remove(tbih);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void DeletarPorCode(string code, int optQuote)
        {
            var lst = Con.TBItinerary_Highlights.Where(a => a.Code == code && a.optQuote == optQuote).ToList();

            foreach (var item in lst)
            {
                Con.TBItinerary_Highlights.Remove(item);
            }

            Con.SaveChanges();
        }

    }
}
