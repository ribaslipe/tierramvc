﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class MoedaDAL
    {
        private Model Con;

        public MoedaDAL()
        {
            Con = new Model();
        }

        public Moeda ObterPorId(int MoedaId)
        {
            try
            {
                return Con.Moeda.Where(m => m.Moeda_id == MoedaId).SingleOrDefault();   
            }
            catch 
            {
                throw;
            }

        }

        public void Salvar(Moeda b)
        {
            try
            {
                Con.Moeda.Add(b);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Moeda b)
        {
            try
            {
                Con.Moeda.Remove(b);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Moeda novo)
        {
            try
            {
                Moeda antigo = ObterPorId(novo.Moeda_id);

                antigo.Moeda_sigla = novo.Moeda_sigla;
                antigo.Moeda_nome = novo.Moeda_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Moeda> ListarTodos()
        {
            try
            {
                return Con.Moeda.OrderBy(c => c.Moeda_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Moeda> ListarTodos(string moeda)
        {
            try
            {
                return Con.Moeda.Where(s => s.Moeda_nome.Contains(moeda)).OrderBy(c => c.Moeda_nome).ToList();
            }
            catch
            {
                throw;
            }
        }


        public bool ObterPorNome(string nome)
        {
            try
            {
                return Con.Moeda.Where(s => s.Moeda_nome.Equals(nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public Moeda ObterPorNome_model(string nome)
        {
            try
            {
                return Con.Moeda.Where(s => s.Moeda_nome.Equals(nome)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Moeda RetornaIdPorTransfer(string Transf)
        {
            try
            {

                return (from ssv in Con.S_Servicos
                        join svc in Con.Servico_Completo on ssv.Servicos_Id equals svc.Servicos_Id
                        join mon in Con.Monta_Servico on svc.IdMServico equals mon.IdMServico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join sba in Con.S_Bases on sup.S_id equals sba.S_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where ssv.Servicos_Nome.Equals(Transf)
                        select mod).Distinct().FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }        

        public Moeda RetornaIdPorTransfer1(string Transf)
        {
            try
            {

                return (from ssv in Con.S_Servicos
                        join svc in Con.Servico_Completo on ssv.Servicos_Id equals svc.Servicos_Id
                        join mon in Con.Monta_Servico on svc.IdMServico_Temporada equals mon.IdMServico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join sba in Con.S_Bases on sup.S_id equals sba.S_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where ssv.Servicos_Nome.Equals(Transf) &&
                              sba.IdTipoTrans == 6
                        select mod).Distinct().FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public int? RetornaMoeda_hotel_periodo(int IdSupplier, DateTime dtFrom, DateTime dtTo)
        {
            try
            {
                return (from spp in Con.Supplier
                        join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                        where spp.S_id == IdSupplier &&
                              smp.S_merc_periodo_from <= dtFrom &&
                              smp.S_merc_periodo_to >= dtTo
                        select smp).FirstOrDefault().Moeda_id;
            }
            catch 
            {                
                throw;
            }
        }

        public Moeda Retorna_Sigla(string sigla)
        {
            try
            {
                return Con.Moeda.Where(s => s.Moeda_sigla.Equals(sigla)).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
