﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class SPoliticaCHDTarifasDAL
    {

        private Model Con;

        public SPoliticaCHDTarifasDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_PoliticaCHDTarifas s)
        {
            try
            {
                Con.S_PoliticaCHDTarifas.Add(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public S_PoliticaCHDTarifas ObterPorId(int IdTarifa)
        {
            try
            {
                return Con.S_PoliticaCHDTarifas.Where(s => s.S_merc_tarif_id == IdTarifa).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_PoliticaCHDTarifas ObterPorIdOP(int IdPolitica)
        {
            try
            {
                return Con.S_PoliticaCHDTarifas.Where(s => s.S_politicaCHD_id == IdPolitica).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public object ListarTodos(int IdTarif)
        {
            try
            {
                return (from spc in Con.S_PoliticaCHDTarifas
                        join smt in Con.S_Mercado_Tarifa on spc.S_merc_tarif_id equals smt.S_merc_tarif_id
                        join sph in Con.S_Politica_CHD on spc.S_politicaCHD_id equals sph.S_politicaCHD_id
                        where spc.S_merc_tarif_id == IdTarif
                        select new
                        {
                            sph.S_politicaCHD_qtd,
                            sph.S_politicaCHD_idadeDe,
                            sph.S_politicaCHD_free,
                            sph.S_politicaCHD_valor,
                            sph.S_politicaCHD_percent,
                            spc.S_PoliticaCHDTarifas_id
                        }).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_PoliticaCHDTarifas> ListarTodosLista(int IdTarif)
        {
            try
            {
                return Con.S_PoliticaCHDTarifas.Where(s => s.S_merc_tarif_id == IdTarif).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public S_PoliticaCHDTarifas ObterPorId(int IdTarifa, int IdPolitica)
        {
            try
            {
                return Con.S_PoliticaCHDTarifas.Where(s => s.S_merc_tarif_id == IdTarifa && s.S_politicaCHD_id == IdPolitica).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(S_PoliticaCHDTarifas s)
        {
            try
            {
                Con.S_PoliticaCHDTarifas.Add(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
