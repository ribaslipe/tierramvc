﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Persistencia;

namespace DAL.Persistencia
{
    public class MarkupGeralDAL
    {

        private Model Con;

        public MarkupGeralDAL()
        {
            Con = new Model();
        }

        public void Salvar(MarkupGeral m)
        {
            try
            {
                Con.MarkupGeral.Add(m);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public MarkupGeral ObterPorAno(DateTime dataDia)
        {
            try
            {
                return Con.MarkupGeral.Where(m => m.MarkupGeral_dataInicial <= dataDia &&
                                                  m.MarkupGeral_dataFinal >= dataDia).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
