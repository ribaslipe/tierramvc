﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class SMercadoTipoTarifaDAL
    {
        private Model Con;

        public SMercadoTipoTarifaDAL()
        {
            Con = new Model();
        }

        public List<S_Mercado_Tipo_Tarifa> ListarTodos()
        {
            try
            {
                return Con.S_Mercado_Tipo_Tarifa.ToList();
            }
            catch 
            {
                throw;
            }
        }
    }
}
