﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Persistencia;

namespace DAL.Persistencia
{
    public class QuoteServExtDAL
    {

        private Model Con;

        public QuoteServExtDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quote_ServExtra q)
        {
            try
            {
                Con.Quote_ServExtra.Add(q);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Quote_ServExtra ObterPorId(int IdQt)
        {
            try
            {
                return Con.Quote_ServExtra.Where(q => q.Quote_ServExtra_id == IdQt).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public List<Quote_ServExtra> ObterPorIdTC(int IdRange)
        {
            try
            {
                return Con.Quote_ServExtra.Where(q => q.Range_id == IdRange).ToList();
            }
            catch
            {
                throw;
            }
        }        

        public void ExcluirPorFile(int idFile)
        {
            try
            {
                var query = Con.Quote_ServExtra.Where(q => q.File_id == idFile).ToList();

                foreach (Quote_ServExtra item in query)
                {
                    Con.Quote_ServExtra.Remove(item);
                    Con.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public void ExcluirLista(List<Quote_ServExtra> lista)
        {
            try
            {
                foreach (Quote_ServExtra item in lista)
                {
                    Con.Quote_ServExtra.Remove(item);
                    Con.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNew(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra ftssnew = ObterPorId(novo.Quote_ServExtra_id);

                ftssnew.File_ServExtra_vendaNet = novo.File_ServExtra_vendaNet;
                ftssnew.File_ServExtra_venda = novo.File_ServExtra_venda;
                ftssnew.File_ServExtra_total = novo.File_ServExtra_total;
                ftssnew.File_ServExtra_valor = novo.File_ServExtra_valor;
                ftssnew.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                ftssnew.markup = novo.markup;
                ftssnew.markupNet = novo.markupNet;
                ftssnew.desconto = novo.desconto;
                ftssnew.descontoNet = novo.descontoNet;
                ftssnew.Qtd_ServExtr = novo.Qtd_ServExtr;
                ftssnew.Paxs = novo.Paxs;
                ftssnew.Paying_Pax = novo.Paying_Pax;
                ftssnew.Supp_Paying_Pax = novo.Supp_Paying_Pax;                

                Con.SaveChanges();
             
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Quote_ServExtra q)
        {
            try
            {
                Con.Quote_ServExtra.Remove(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Quote_ServExtra ObterTC(int IdFile, int take)
        {
            try
            {
                take = take - 1;

                return Con.Quote_ServExtra.Where(q => q.File_id == IdFile &&
                                                    q.TC == true)
                                                    .OrderBy(q => q.Data_From)
                                                    .ThenBy(q => q.Data_To)
                                                    .ThenBy(q => q.Quote_ServExtra_id)
                                                    .Skip(take)
                                                    .Take(1).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_ServExtra> ListarTodos(int IdFile)
        {
            try
            {
                return Con.Quote_ServExtra.Where(f => f.File_id == IdFile).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_ServExtra> ListarTodos()
        {
            try
            {
                return Con.Quote_ServExtra.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_ServExtra> ListarTodosSupplier(int idspp)
        {
            try
            {
                return Con.Quote_ServExtra.Where(s => s.S_id == idspp).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_ServExtra> ListarTodos(DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                return Con.Quote_ServExtra.Where(f => f.Data_From >= dataInicio && f.Data_From < dataFim).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_ServExtra> ListarTodos(int IdFile, int IdSupplier)
        {
            try
            {
                return Con.Quote_ServExtra.Where(f => f.File_id == IdFile &&
                                                   f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarBulk(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra antigo = ObterPorId(novo.Quote_ServExtra_id);

                antigo.TBulk = novo.TBulk;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarStatus(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra antigo = ObterPorId(novo.Quote_ServExtra_id);

                antigo.Status = novo.Status;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarFlight(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra antigo = ObterPorId(novo.Quote_ServExtra_id);

                antigo.Flights_id = novo.Flights_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOrdem(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra antigo = ObterPorId(novo.Quote_ServExtra_id);

                antigo.Ordem = novo.Ordem;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarDatas(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra antigo = ObterPorId(novo.Quote_ServExtra_id);
                antigo.Data_From = novo.Data_From;
                antigo.Data_To = novo.Data_To;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarValores(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra antigo = ObterPorId(novo.Quote_ServExtra_id);

                antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.File_ServExtra_vendaNet = novo.File_ServExtra_vendaNet;
                antigo.File_ServExtra_venda = novo.File_ServExtra_venda;
                antigo.File_ServExtra_total = novo.File_ServExtra_total;
                antigo.File_ServExtra_valor = novo.File_ServExtra_valor;
                antigo.markup = novo.markup;
                antigo.markupNet = novo.markupNet;
                antigo.desconto = novo.desconto;
                antigo.descontoNet = novo.descontoNet;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarRange_id(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra antigo = ObterPorId(novo.Quote_ServExtra_id);

                antigo.Range_id = novo.Range_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarSupplier(Quote_ServExtra novo)
        {
            try
            {
                Quote_ServExtra ftssnew = ObterPorId(novo.Quote_ServExtra_id);

                ftssnew.S_nome = novo.S_nome;
                ftssnew.S_id = novo.S_id;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public List<Quote_ServExtra> ListarTodosData(DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                return Con.Quote_ServExtra.Where(f => f.Data_From <= dataInicio && f.Data_To >= dataFim && (f.Status.Equals("OK") || f.Status.Equals("OP"))
                                                        ||
                                                       f.Data_From <= dataFim && f.Data_To >= dataInicio && (f.Status.Equals("OK") || f.Status.Equals("OP"))).ToList();

            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem_(int IdFile)
        {
            try
            {
                var item = Con.Quote_ServExtra.Where(f => f != null && f.File_id == IdFile).OrderByDescending(s => s.Ordem).FirstOrDefault();

                if (item != null)
                {
                    return Convert.ToInt32(item.Ordem);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }

    }
}
