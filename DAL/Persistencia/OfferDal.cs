﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class OfferDal
    {
        private Model Con;

        public OfferDal()
        {
            Con = new Model();
        }

        public void Salvar(OfferHash o)
        {
            try
            {
                Con.OfferHash.Add(o);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public OfferHash ObterPorCodes(string quote, int type, int price, int optquote, int ratear)
        {
            try
            {
                return Con.OfferHash.Where(s => s.OfferHash_quote.Equals(quote) && s.OfferHash_type == type && s.OfferHash_price == price && s.OfferHash_optQuote == optquote && s.OfferHash_ratear == ratear).SingleOrDefault(); 
            }
            catch
            {
                throw;
            }
        }

        public OfferHash ObterPorHash(string hash)
        {
            try
            {
                return Con.OfferHash.Where(s => s.OfferHash_hash.Equals(hash)).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
