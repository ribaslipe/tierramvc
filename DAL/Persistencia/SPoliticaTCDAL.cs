﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class SPoliticaTCDAL
    {
        private Model Con;

        public SPoliticaTCDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Politica_TC s)
        {
            try
            {
                Con.S_Politica_TC.Add(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public S_Politica_TC ObterPorId(int IdPol)
        {
            try
            {
                return Con.S_Politica_TC.Where(s => s.S_politicaTC_id == IdPol).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Politica_TC s)
        {
            try
            {
                Con.S_Politica_TC.Remove(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(S_Politica_TC novo)
        {
            try
            {
                S_Politica_TC antigo = ObterPorId(novo.S_politicaTC_id);

                antigo.S_politicaTC_qtd = novo.S_politicaTC_qtd;
                antigo.S_politicaTC_idade = novo.S_politicaTC_idade;
                antigo.S_politicaTC_free = novo.S_politicaTC_free;
                antigo.S_politicaTC_valor = novo.S_politicaTC_valor;
                antigo.S_politicaTC_percent = novo.S_politicaTC_percent;
                antigo.S_politicaTC_idadeDe = novo.S_politicaTC_idadeDe;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Politica_TC> ListarTodos()
        {
            try
            {
                return Con.S_Politica_TC.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Politica_TC> ListarTodos(int IdTarifa)
        {
            try
            {
                return (from spd in Con.S_Politica_TC
                        join spt in Con.S_PoliticaTCTarifas on spd.S_politicaTC_id equals spt.S_politicaTC_id
                        where spt.S_merc_tarif_id == IdTarifa
                        select spd).ToList();
            }
            catch
            {
                throw;
            }
        }

        public S_Politica_TC VerificaExiste(int qtd, int tipo, decimal valor, decimal percent)
        {
            try
            {

                S_Politica_TC spc = new S_Politica_TC();

                switch (tipo)
                {
                    case 0:
                        spc = Con.S_Politica_TC.Where(s => s.S_politicaTC_qtd == qtd && s.S_politicaTC_free != null).SingleOrDefault();
                        break;
                    case 1:
                        spc = Con.S_Politica_TC.Where(s => s.S_politicaTC_qtd == qtd && s.S_politicaTC_valor == valor).SingleOrDefault();
                        break;
                    case 2:
                        spc = Con.S_Politica_TC.Where(s => s.S_politicaTC_qtd == qtd && s.S_politicaTC_percent == percent).SingleOrDefault();
                        break;
                }

                return spc;

            }
            catch
            {
                throw;
            }

        }


    }
}
