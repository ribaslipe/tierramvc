﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class FotoServicoDAL
    {
        Model Con;

        public FotoServicoDAL()
        {
            Con = new Model();
        }

        public object ListarTodos(int IdServicos)
        {
            try
            {
                return Con.Servicos_Imagem.Where(s => s.Servicos_Id == IdServicos).ToList();
            }
            catch 
            {
                throw;
            }
        }

        public List<Servicos_Imagem> ListarTodos_model(int IdServicos)
        {
            try
            {
                return Con.Servicos_Imagem.Where(s => s.Servicos_Id == IdServicos).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Servicos_Imagem> ListarTodosLista(int IdServicos)
        {
            try
            {
                return Con.Servicos_Imagem.Where(s => s.Servicos_Id == IdServicos).ToList();
            }
            catch
            {
                throw;
            }
        }

        public Servicos_Imagem ObterPorId(int IdImage)
        {
            try
            {
                return Con.Servicos_Imagem.Where(s => s.Serv_img_id == IdImage).Single();
            }
            catch 
            {
                throw;
            }
        }

        public Servicos_Imagem ObterImgPrincipal(int IdServico)
        {
            try
            {
                return Con.Servicos_Imagem.Where(s => s.Servicos_Id == IdServico && s.Serv_img_capa.Equals("S")).SingleOrDefault();
            }
            catch 
            {
                throw;
            }
        }

        public void ImagemPrincipal(int IdServico)
        {
            try
            {
                Servicos_Imagem Foto = Con.Servicos_Imagem.Where(f => f.Serv_img_capa.Equals("S") && f.Servicos_Id == IdServico).SingleOrDefault();

                if (Foto == null)
                {
                    Foto = Con.Servicos_Imagem.Where(f => f.Servicos_Id == IdServico).First();
                    Foto.Serv_img_capa = "S";
                    Atualizar(Foto);
                }

            }
            catch
            {
                throw;
            }
        }

        public void AddImagemPrincipal(int IdServico, int IdImagem)
        {
            try
            {
                Servicos_Imagem FotoAntiga = Con.Servicos_Imagem.Where(i => i.Servicos_Id == IdServico && i.Serv_img_capa.Equals("S")).SingleOrDefault();

                if (FotoAntiga != null)
                {
                    FotoAntiga.Serv_img_capa = "N";
                    Atualizar(FotoAntiga);
                }

                Servicos_Imagem FotoNova = Con.Servicos_Imagem.Where(i => i.Serv_img_id == IdImagem).Single();
                FotoNova.Serv_img_capa = "S";
                Atualizar(FotoNova);
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Servicos_Imagem novo)
        {
            try
            {
                Servicos_Imagem antigo = ObterPorId(novo.Serv_img_id);

                antigo.Serv_img_nome = novo.Serv_img_nome;
                antigo.Serv_img_capa = novo.Serv_img_capa;
                antigo.Serv_img_memo = novo.Serv_img_memo;
                antigo.Serv_img_hupload = novo.Serv_img_hupload;
                antigo.Serv_img_descr = novo.Serv_img_descr;

                Con.SaveChanges();
            }
            catch 
            {
                throw;
            }

        }

        public void Salvar(Servicos_Imagem s)
        {
            try
            {
                Con.Servicos_Imagem.Add(s);
                Con.SaveChanges();
            }
            catch 
            {
                throw;
            }
        }

        public void Excluir(Servicos_Imagem s)
        {
            try
            {
                Con.Servicos_Imagem.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {
                throw;
            }
        }

    }
}
