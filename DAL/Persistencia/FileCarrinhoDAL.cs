﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Models;

namespace DAL.Persistencia
{
    public class FileCarrinhoDAL
    {
        private Model Con;

        public FileCarrinhoDAL()
        {
            Con = new Model();
        }

        public object MontaCarrinhoHoteis(int IdQuotGrupo)
        {
            try
            {
                return (from fc in Con.File_Carrinho
                        join ft in Con.File_Tarifas on fc.File_id equals ft.File_id
                        where fc.Quotation_Grupo_Id == IdQuotGrupo &&
                              ft.Range_id == null
                        orderby ft.Data_From, ft.Ordem
                        select new
                        {
                            ft.Ordem,
                            ft.S_meal_nome,
                            ft.S_meal_status,
                            NomeSupplier = ft.S_nome,
                            Categoria = ft.Categoria,
                            Room = ft.Room,
                            QtdTarifa = ft.Qtd_Tarifas,
                            ValorTarifa = ft.S_merc_tarif_valor,
                            Mod = ft.Moeda,
                            Markup = ft.markup,
                            Desconto = ft.desconto,
                            MarkupNet = ft.markupNet,
                            DescontoNet = ft.descontoNet,
                            ft.File_Tarifas_id,
                            ft.Paying_Pax,
                            ft.NumNoites,
                            DataFrom = ft.Data_From,
                            DataTo = ft.Data_To,
                            ft.Status,
                            ft.NomePacote
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object MontaCarrinhoHoteisNew(int IdQuotGrupo)
        {
            try
            {
                return (from fc in Con.File_Carrinho
                        join ft in Con.File_Tarifas on fc.File_id equals ft.File_id
                        where fc.Quotation_Grupo_Id == IdQuotGrupo &&
                              ft.Range_id != null
                        orderby ft.Data_From, ft.Ordem
                        select new
                        {
                            ft.Ordem,
                            ft.S_meal_nome,
                            ft.S_meal_status,
                            NomeSupplier = ft.S_nome,
                            Categoria = ft.Categoria,
                            Room = ft.Room,
                            QtdTarifa = ft.Qtd_Tarifas,
                            ValorTarifa = ft.S_merc_tarif_valor,
                            Mod = ft.Moeda,
                            Markup = ft.markup,
                            Desconto = ft.desconto,
                            MarkupNet = ft.markupNet,
                            DescontoNet = ft.descontoNet,
                            ft.File_Tarifas_id,
                            ft.Paying_Pax,
                            ft.NumNoites,
                            DataFrom = ft.Data_From,
                            DataTo = ft.Data_To,
                            ft.Status,
                            ft.NomePacote
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object MontaCarrinhoHoteisNewRange(int IdQuotGrupo)
        {
            try
            {


                return (from fc in Con.File_Carrinho
                        join ft in Con.File_Tarifas on fc.File_id equals ft.File_id
                        join rg in Con.Ranges on ft.File_Tarifas_id equals rg.FileTabelaId
                        where fc.Quotation_Grupo_Id == IdQuotGrupo &&
                              ft.Range_id != null
                        orderby ft.Data_From, ft.Ordem
                        select new
                        {
                            ft.Ordem,
                            ft.S_meal_nome,
                            ft.S_meal_status,
                            NomeSupplier = ft.S_nome,
                            Categoria = ft.Categoria,
                            Room = ft.Room,
                            QtdTarifa = ft.Qtd_Tarifas,
                            ValorTarifa = ft.S_merc_tarif_valor,
                            Mod = ft.Moeda,
                            Markup = ft.markup,
                            Desconto = ft.desconto,
                            MarkupNet = ft.markupNet,
                            DescontoNet = ft.descontoNet,
                            ft.File_Tarifas_id,
                            ft.Paying_Pax,
                            ft.NumNoites,
                            DataFrom = ft.Data_From,
                            DataTo = ft.Data_To,
                            ft.Status,
                            ft.NomePacote,
                            rg.Ranges_id
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object MontaCarrinhoTransfers(int IdQuotGrupo)
        {
            try
            {
                return (from fc in Con.File_Carrinho
                        join ft in Con.File_Transfers on fc.File_id equals ft.File_id
                        where fc.Quotation_Grupo_Id == IdQuotGrupo &&
                              ft.Range_id == null
                        orderby ft.Data_From, ft.Ordem
                        select new
                        {
                            ft.Ordem,
                            NomeSupplier = ft.S_nome,
                            TituloTransf = ft.Transf_nome,
                            QtdTarifa = ft.Qtd_Transf,
                            ValorTarifa = ft.Transf_valor,
                            Mod = ft.Moeda,
                            Markup = ft.markup,
                            Desconto = ft.desconto,
                            MarkupNet = ft.markupNet,
                            DescontoNet = ft.descontoNet,
                            ft.File_Transf_id,
                            ft.Paying_Pax,
                            DataFrom = ft.Data_From,
                            DataTo = ft.Data_To,
                            ft.Trf_Tours,
                            ft.Status,
                            ft.SubServico,
                            ft.S_id
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object MontaCarrinhoExtras(int IdQuotGrupo)
        {
            try
            {
                return (from fc in Con.File_Carrinho
                        join ft in Con.File_ServExtra on fc.File_id equals ft.File_id
                        where fc.Quotation_Grupo_Id == IdQuotGrupo &&
                              ft.Range_id == null
                        orderby ft.Data_From, ft.Ordem
                        select new
                        {
                            ft.Ordem,
                            NomeSupplier = ft.S_nome,
                            ft.File_ServExtra_nome,
                            ft.Qtd_ServExtr,
                            ft.File_ServExtra_id,
                            ft.Paying_Pax,
                            ValorTarifa = ft.File_ServExtra_valor,
                            Markup = ft.markup,
                            Desconto = ft.desconto,
                            MarkupNet = ft.markupNet,
                            DescontoNet = ft.descontoNet,
                            DataFrom = ft.Data_From,
                            DataTo = ft.Data_To,
                            Mod = ft.Moeda,
                            ft.Status
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object MontaCarrinhoNewFile(int IdQuotGrupo, int OptQuote)
        {
            try
            {
                return ((from fcr in Con.File_Carrinho
                         join fts in Con.File_Tarifas on fcr.File_id equals fts.File_id
                         join rgn in Con.Ranges on fts.File_Tarifas_id equals rgn.FileTabelaId
                         where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                               fts.Range_id != null &&
                               rgn.Flag.Equals("hotel") &&
                               rgn.OptQuote == OptQuote
                         select new
                         {
                             IdRetorna = fts.File_Tarifas_id,
                             dtFrom = fts.Data_From,
                             dtTo = fts.Data_To,
                             Nome = fts.S_nome,
                             NomeTr = fts.S_nome,
                             Room = fts.Room,
                             Categoria = fts.Categoria,
                             Flag = rgn.Flag,
                             MealNome = fts.S_meal_nome,
                             MealStatus = fts.S_meal_status,
                             Status = fts.Status,
                             DAY = rgn.DAY,
                             Bulk = fts.TBulk,
                             Optional = fts.TOptional,
                             SubServico = false,
                             Paying_Pax = fts.Paying_Pax,
                             Ordem = fts.Ordem
                         }).Distinct().ToList())
                        .Union
                        ((from fcr in Con.File_Carrinho
                          join fts in Con.File_Transfers on fcr.File_id equals fts.File_id
                          join rgn in Con.Ranges on fts.File_Transf_id equals rgn.FileTabelaId
                          where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                                fts.Range_id != null &&
                               rgn.Flag.Equals("servico") &&
                               rgn.OptQuote == OptQuote
                          select new
                          {
                              IdRetorna = fts.File_Transf_id,
                              dtFrom = fts.Data_From,
                              dtTo = fts.Data_To,
                              Nome = fts.S_nome,
                              NomeTr = fts.Transf_nome,
                              Room = "",
                              Categoria = "",
                              Flag = rgn.Flag,
                              MealNome = "",
                              MealStatus = "",
                              Status = fts.Status,
                              DAY = rgn.DAY,
                              Bulk = fts.TBulk,
                              Optional = fts.TOptional,
                              SubServico = fts.SubServico == true ? true : false,
                              Paying_Pax = fts.Paying_Pax,
                              Ordem = fts.Ordem
                          }).Distinct().ToList())
                          .Union
                          ((from fcr in Con.File_Carrinho
                            join fse in Con.File_ServExtra on fcr.File_id equals fse.File_id
                            join rgn in Con.Ranges on fse.File_ServExtra_id equals rgn.FileTabelaId
                            where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                                  fse.Range_id != null &&
                               rgn.Flag.Equals("extra") &&
                               rgn.OptQuote == OptQuote
                            select new
                            {
                                IdRetorna = fse.File_ServExtra_id,
                                dtFrom = fse.Data_From,
                                dtTo = fse.Data_To,
                                Nome = fse.File_ServExtra_nome,
                                NomeTr = "",
                                Room = "",
                                Categoria = "",
                                Flag = rgn.Flag,
                                MealNome = "",
                                MealStatus = "",
                                Status = fse.Status,
                                DAY = rgn.DAY,
                                Bulk = fse.TBulk,
                                Optional = fse.TOptional,
                                SubServico = false,
                                Paying_Pax = fse.Paying_Pax,
                                Ordem = fse.Ordem
                            }).Distinct().ToList()).OrderBy(a => a.dtFrom).ThenBy(a => a.Ordem).ThenBy(a => a.dtTo);

            }
            catch
            {
                throw;
            }
        }

        public object MontaCarrinhoNewFile(int IdQuotGrupo)
        {
            try
            {

                return ((from fcr in Con.File_Carrinho
                         join fts in Con.File_Tarifas on fcr.File_id equals fts.File_id
                         join rgn in Con.Ranges on fts.File_Tarifas_id equals rgn.FileTabelaId
                         where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                               fts.Range_id != null &&
                               rgn.Flag.Equals("hotel")
                         //orderby fts.Data_From, fts.Ordem
                         select new
                         {
                             IdRetorna = fts.File_Tarifas_id,
                             dtFrom = fts.Data_From,
                             dtTo = fts.Data_To,
                             Nome = fts.S_nome,
                             NomeTr = fts.S_nome,
                             Room = fts.Room,
                             Categoria = fts.Categoria,
                             Flag = rgn.Flag,
                             MealNome = fts.S_meal_nome,
                             MealStatus = fts.S_meal_status,
                             Status = fts.Status,
                             DAY = rgn.DAY,
                             Bulk = fts.TBulk,
                             SubServico = false,
                             Paying_Pax = fts.Paying_Pax
                         }).Distinct().ToList())
                        .Union
                        ((from fcr in Con.File_Carrinho
                          join fts in Con.File_Transfers on fcr.File_id equals fts.File_id
                          join rgn in Con.Ranges on fts.File_Transf_id equals rgn.FileTabelaId
                          where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                                fts.Range_id != null &&
                               rgn.Flag.Equals("servico")
                          //orderby fts.Data_From, fts.Ordem
                          select new
                          {
                              IdRetorna = fts.File_Transf_id,
                              dtFrom = fts.Data_From,
                              dtTo = fts.Data_To,
                              Nome = fts.S_nome,
                              NomeTr = fts.Transf_nome,
                              Room = "",
                              Categoria = "",
                              Flag = rgn.Flag,
                              MealNome = "",
                              MealStatus = "",
                              Status = fts.Status,
                              DAY = rgn.DAY,
                              Bulk = fts.TBulk,
                              SubServico = fts.SubServico == true ? true : false,
                              Paying_Pax = fts.Paying_Pax
                          }).Distinct().ToList())
                          .Union
                          ((from fcr in Con.File_Carrinho
                            join fse in Con.File_ServExtra on fcr.File_id equals fse.File_id
                            join rgn in Con.Ranges on fse.File_ServExtra_id equals rgn.FileTabelaId
                            where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                                  fse.Range_id != null &&
                               rgn.Flag.Equals("extra")
                            //orderby fse.Data_From, fse.Ordem
                            select new
                            {
                                IdRetorna = fse.File_ServExtra_id,
                                dtFrom = fse.Data_From,
                                dtTo = fse.Data_To,
                                Nome = fse.File_ServExtra_nome,
                                NomeTr = "",
                                Room = "",
                                Categoria = "",
                                Flag = rgn.Flag,
                                MealNome = "",
                                MealStatus = "",
                                Status = fse.Status,
                                DAY = rgn.DAY,
                                Bulk = fse.TBulk,
                                SubServico = false,
                                Paying_Pax = fse.Paying_Pax
                            }).Distinct().ToList()).OrderBy(a => a.dtFrom).ThenBy(a => a.dtTo);

            }
            catch
            {
                throw;
            }
        }

        public object MontaCarrinhoNewFileFinal(int IdFile)
        {
            try
            {

                return ((from fcr in Con.File_Carrinho
                         join fts in Con.Quote_Tarifas on fcr.File_id equals fts.File_id
                         where fcr.File_id == IdFile &&
                               fts.TC == false
                         select new
                         {
                             IdRetorna = fts.Quote_Tarifas_id,
                             dtFrom = fts.Data_From,
                             dtTo = fts.Data_To,
                             Nome = fts.S_nome,
                             NomeTr = fts.S_nome,
                             Room = fts.Room,
                             Categoria = fts.Categoria,
                             MealNome = fts.S_meal_nome,
                             MealStatus = fts.S_meal_status,
                             Status = fts.Status,
                             Bulk = fts.TBulk,
                             Flag = fts.Flag,
                             SubServico = false,
                             Paying_Pax = fts.Paying_Pax,
                             Ordem = fts.Ordem
                         }).Distinct().ToList())
                        .Union
                        ((from fcr in Con.File_Carrinho
                          join fts in Con.Quote_Transf on fcr.File_id equals fts.File_id
                          where fcr.File_id == IdFile &&
                                fts.TC == false
                          select new
                          {
                              IdRetorna = fts.Quote_Transf_id,
                              dtFrom = fts.Data_From,
                              dtTo = fts.Data_To,
                              Nome = fts.S_nome,
                              NomeTr = fts.Transf_nome,
                              Room = "",
                              Categoria = "",
                              MealNome = "",
                              MealStatus = "",
                              Status = fts.Status,
                              Bulk = fts.TBulk,
                              Flag = fts.Flag,
                              SubServico = fts.SubServico == true ? true : false,
                              Paying_Pax = fts.Paying_Pax,
                              Ordem = fts.Ordem
                          }).Distinct().ToList())
                          .Union
                          ((from fcr in Con.File_Carrinho
                            join fse in Con.Quote_ServExtra on fcr.File_id equals fse.File_id
                            where fcr.File_id == IdFile &&
                                  fse.TC == false
                            select new
                            {
                                IdRetorna = fse.Quote_ServExtra_id,
                                dtFrom = fse.Data_From,
                                dtTo = fse.Data_To,
                                Nome = fse.File_ServExtra_nome,
                                NomeTr = "",
                                Room = "",
                                Categoria = "",
                                MealNome = "",
                                MealStatus = "",
                                Status = fse.Status,
                                Bulk = fse.TBulk,
                                Flag = fse.Flag,
                                SubServico = false,
                                Paying_Pax = fse.Paying_Pax,
                                Ordem = fse.Ordem
                            }).Distinct().ToList()).OrderBy(a => a.dtFrom).ThenBy(a => a.Ordem).ThenBy(a => a.dtTo);

            }
            catch
            {
                throw;
            }
        }

        public List<GridQuoteModel> MontaCarrinhoNewFileFinal_Model(int IdFile)
        {
            try
            {

                return ((from fcr in Con.File_Carrinho
                         join fts in Con.Quote_Tarifas on fcr.File_id equals fts.File_id
                         where fcr.File_id == IdFile &&
                               fts.TC == false
                         select new GridQuoteModel
                         {
                             IdRetorna = fts.Quote_Tarifas_id,
                             Plataforma_id = fts.Plataforma_id == null ? 0 : (int)fts.Plataforma_id,
                             idRange = fts.Range_id == null ? 0 : (int)fts.Range_id,
                             S_id = fts.S_id == null ? 0 : (int)fts.S_id,
                             TC = fts.TC == true ? true : false,
                             dtFrom = fts.Data_From,
                             dtTo = fts.Data_To,
                             Nome = fts.S_nome,
                             NomeTr = fts.S_nome,
                             NomeSupp = fts.S_nome,
                             Room = fts.Room,
                             Categoria = fts.Categoria,
                             MealNome = fts.S_meal_nome,
                             MealStatus = fts.S_meal_status,
                             Status = fts.Status,
                             Bulk = fts.TBulk,
                             Flag = fts.Flag,
                             SubServico = false,
                             Paying_Pax = fts.Paying_Pax,
                             Flight_id = fts.Flights_id == null ? 0 : (int)fts.Flights_id,
                             Ordem = fts.Ordem,
                             VENDA = fts.S_merc_tarif_venda == null ? 0 : (decimal)fts.S_merc_tarif_venda,
                             VRNET = fts.S_merc_tarif_vendaNet == null ? 0 : (decimal)fts.S_merc_tarif_vendaNet,
                             VALOR = fts.S_merc_tarif_valor == null ? 0 : (decimal)fts.S_merc_tarif_valor,
                             VRTOTAL = fts.S_merc_tarif_total == null ? 0 : (decimal)fts.S_merc_tarif_total,
                             NomePacote = fts.NomePacote,
                             Provisorio = fts.Provisorio
                         }).Distinct().ToList())
                        .Union
                        ((from fcr in Con.File_Carrinho
                          join fts in Con.Quote_Transf on fcr.File_id equals fts.File_id
                          where fcr.File_id == IdFile &&
                                fts.TC == false
                          select new GridQuoteModel
                          {
                              IdRetorna = fts.Quote_Transf_id,
                              Plataforma_id = 0,
                              idRange = fts.Range_id == null ? 0 : (int)fts.Range_id,
                              S_id = fts.S_id == null ? 0 : (int)fts.S_id,
                              TC = fts.TC == true ? true : false,
                              dtFrom = fts.Data_From,
                              dtTo = fts.Data_To,
                              Nome = fts.S_nome,
                              NomeTr = fts.Transf_nome,
                              NomeSupp = fts.S_nome,
                              Room = "",
                              Categoria = "",
                              MealNome = "",
                              MealStatus = "",
                              Status = fts.Status,
                              Bulk = fts.TBulk,
                              Flag = fts.Flag,
                              SubServico = fts.SubServico == true ? true : false,
                              Paying_Pax = fts.Paying_Pax,
                              Flight_id = fts.Flights_id == null ? 0 : (int)fts.Flights_id,
                              Ordem = fts.Ordem,
                              VENDA = fts.Transf_venda == null ? 0 : (decimal)fts.Transf_venda,
                              VRNET = fts.Transf_vendaNet == null ? 0 : (decimal)fts.Transf_vendaNet,
                              VALOR = fts.Transf_valor == null ? 0 : (decimal)fts.Transf_valor,
                              VRTOTAL = fts.Transf_total == null ? 0 : (decimal)fts.Transf_total,
                              NomePacote = "",
                              Provisorio = false
                          }).Distinct().ToList())
                          .Union
                          ((from fcr in Con.File_Carrinho
                            join fse in Con.Quote_ServExtra on fcr.File_id equals fse.File_id
                            where fcr.File_id == IdFile &&
                                  fse.TC == false
                            select new GridQuoteModel
                            {
                                IdRetorna = fse.Quote_ServExtra_id,
                                Plataforma_id = 0,
                                idRange = fse.Range_id == null ? 0 : (int)fse.Range_id,
                                S_id = fse.S_id == null ? 0 : (int)fse.S_id,
                                TC = fse.TC == true ? true : false,
                                dtFrom = fse.Data_From,
                                dtTo = fse.Data_To,
                                Nome = fse.File_ServExtra_nome,
                                NomeTr = fse.File_ServExtra_nome,
                                NomeSupp = fse.S_nome,
                                Room = "",
                                Categoria = "",
                                MealNome = "",
                                MealStatus = "",
                                Status = fse.Status,
                                Bulk = fse.TBulk,
                                Flag = fse.Flag,
                                SubServico = false,
                                Paying_Pax = fse.Paying_Pax,
                                Flight_id = fse.Flights_id == null ? 0 : (int)fse.Flights_id,
                                Ordem = fse.Ordem,
                                VENDA = fse.File_ServExtra_venda == null ? 0 : (decimal)fse.File_ServExtra_venda,
                                VRNET = fse.File_ServExtra_vendaNet == null ? 0 : (decimal)fse.File_ServExtra_vendaNet,
                                VALOR = fse.File_ServExtra_valor == null ? 0 : (decimal)fse.File_ServExtra_valor,
                                VRTOTAL = fse.File_ServExtra_total == null ? 0 : (decimal)fse.File_ServExtra_total,
                                NomePacote = "",
                                Provisorio = false
                            }).Distinct().ToList()).OrderBy(a => a.dtFrom).ThenBy(a => a.Ordem).ThenBy(a => a.dtTo).ToList();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<GridQuoteModel> MontaCarrinhoNewFileFinal_Model_TC(int IdFile)
        {
            try
            {

                return ((from fcr in Con.File_Carrinho
                         join fts in Con.Quote_Tarifas on fcr.File_id equals fts.File_id
                         where fcr.File_id == IdFile &&
                               fts.TC == true
                         select new GridQuoteModel
                         {
                             IdRetorna = fts.Quote_Tarifas_id,
                             idRange = (int)fts.Range_id,
                             dtFrom = fts.Data_From,
                             dtTo = fts.Data_To,
                             Nome = fts.S_nome,
                             NomeTr = fts.S_nome,
                             Room = fts.Room,
                             Categoria = fts.Categoria,
                             MealNome = fts.S_meal_nome,
                             MealStatus = fts.S_meal_status,
                             Status = fts.Status,
                             Bulk = fts.TBulk,
                             Flag = fts.Flag,
                             SubServico = false,
                             Paying_Pax = fts.Paying_Pax,
                             Ordem = fts.Ordem,
                             VENDA = fts.S_merc_tarif_venda == null ? 0 : (decimal)fts.S_merc_tarif_venda,
                             VRNET = fts.S_merc_tarif_vendaNet == null ? 0 : (decimal)fts.S_merc_tarif_vendaNet,
                             VALOR = fts.S_merc_tarif_valor == null ? 0 : (decimal)fts.S_merc_tarif_valor,
                             VRTOTAL = fts.S_merc_tarif_total == null ? 0 : (decimal)fts.S_merc_tarif_total
                         }).Distinct().ToList())
                        .Union
                        ((from fcr in Con.File_Carrinho
                          join fts in Con.Quote_Transf on fcr.File_id equals fts.File_id
                          where fcr.File_id == IdFile &&
                                fts.TC == true
                          select new GridQuoteModel
                          {
                              IdRetorna = fts.Quote_Transf_id,
                              idRange = (int)fts.Range_id,
                              dtFrom = fts.Data_From,
                              dtTo = fts.Data_To,
                              Nome = fts.S_nome,
                              NomeTr = fts.Transf_nome,
                              Room = "",
                              Categoria = "",
                              MealNome = "",
                              MealStatus = "",
                              Status = fts.Status,
                              Bulk = fts.TBulk,
                              Flag = fts.Flag,
                              SubServico = fts.SubServico == true ? true : false,
                              Paying_Pax = fts.Paying_Pax,
                              Ordem = fts.Ordem,
                              VENDA = fts.Transf_venda == null ? 0 : (decimal)fts.Transf_venda,
                              VRNET = fts.Transf_vendaNet == null ? 0 : (decimal)fts.Transf_vendaNet,
                              VALOR = fts.Transf_valor == null ? 0 : (decimal)fts.Transf_valor,
                              VRTOTAL = fts.Transf_total == null ? 0 : (decimal)fts.Transf_total
                          }).Distinct().ToList())
                          .Union
                          ((from fcr in Con.File_Carrinho
                            join fse in Con.Quote_ServExtra on fcr.File_id equals fse.File_id
                            where fcr.File_id == IdFile &&
                                  fse.TC == true
                            select new GridQuoteModel
                            {
                                IdRetorna = fse.Quote_ServExtra_id,
                                idRange = (int)fse.Range_id,
                                dtFrom = fse.Data_From,
                                dtTo = fse.Data_To,
                                Nome = fse.File_ServExtra_nome,
                                NomeTr = "",
                                Room = "",
                                Categoria = "",
                                MealNome = "",
                                MealStatus = "",
                                Status = fse.Status,
                                Bulk = fse.TBulk,
                                Flag = fse.Flag,
                                SubServico = false,
                                Paying_Pax = fse.Paying_Pax,
                                Ordem = fse.Ordem,
                                VENDA = fse.File_ServExtra_venda == null ? 0 : (decimal)fse.File_ServExtra_venda,
                                VRNET = fse.File_ServExtra_vendaNet == null ? 0 : (decimal)fse.File_ServExtra_vendaNet,
                                VALOR = fse.File_ServExtra_valor == null ? 0 : (decimal)fse.File_ServExtra_valor,
                                VRTOTAL = fse.File_ServExtra_total == null ? 0 : (decimal)fse.File_ServExtra_total
                            }).Distinct().ToList()).OrderBy(a => a.dtFrom).ThenBy(a => a.Ordem).ThenBy(a => a.dtTo).ToList();

            }
            catch
            {
                throw;
            }
        }

        public object MontaCarrinhoHoteisBases(int IdFile, string BaseDe, string BaseAte)
        {
            try
            {

                return (from fc in Con.File_Carrinho
                        join ft in Con.File_Tarifas on fc.File_id equals ft.File_id
                        join rg in Con.Ranges on ft.Range_id equals rg.Ranges_id
                        where fc.File_id == IdFile &&
                              ft.Range_id != null &&
                              rg.Ranges_de.Equals(BaseDe) &&
                              rg.Ranges_ate.Equals(BaseAte)
                        orderby ft.Data_From, ft.Ordem
                        select new
                        {
                            ft.S_meal_nome,
                            ft.S_meal_status,
                            NomeSupplier = ft.S_nome,
                            Categoria = ft.Categoria,
                            Room = ft.Room,
                            QtdTarifa = ft.Qtd_Tarifas,
                            ValorTarifa = ft.S_merc_tarif_valor,
                            Mod = ft.Moeda,
                            Markup = ft.markup,
                            Desconto = ft.desconto,
                            MarkupNet = ft.markupNet,
                            DescontoNet = ft.descontoNet,
                            ft.File_Tarifas_id,
                            ft.Paying_Pax,
                            ft.NumNoites,
                            DataFrom = ft.Data_From,
                            DataTo = ft.Data_To,
                            ft.Status,
                            ft.NomePacote,
                            rg.Ranges_de,
                            rg.Ranges_ate,
                            ft.S_merc_tarif_venda,
                            ft.S_merc_tarif_total
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public int RetornaFileID(int IdQuotGrupo)
        {
            try
            {
                return Con.File_Carrinho.Where(f => f.Quotation_Grupo_Id == IdQuotGrupo).SingleOrDefault().File_id;
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(File_Carrinho f)
        {
            try
            {
                Con.File_Carrinho.Add(f);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public File_Carrinho ObterPorIdGrupo(int IdQuotGrupo)
        {
            try
            {
                return Con.File_Carrinho.Where(f => f.Quotation_Grupo_Id == IdQuotGrupo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public int RetornaFile(string Quote)
        {
            try
            {

                return (from qot in Con.Quotation
                        join qgr in Con.Quotation_Grupo on qot.Quotation_Id equals qgr.Quotation_Id
                        join fca in Con.File_Carrinho on qgr.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        where qot.Quotation_Code.Equals(Quote)
                        select fca).SingleOrDefault().File_id;

            }
            catch
            {
                throw;
            }
        }

        public int RetornaFile(string Quote, int GroupNbr)
        {
            try
            {

                return (from qot in Con.Quotation
                        join qgr in Con.Quotation_Grupo on qot.Quotation_Id equals qgr.Quotation_Id
                        join fca in Con.File_Carrinho on qgr.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        where qot.Quotation_Code.Equals(Quote) &&
                              qgr.Quotation_Grupo_NGrupo == GroupNbr
                        select fca).SingleOrDefault().File_id;

            }
            catch
            {
                throw;
            }
        }

        public File_Carrinho RetornaFileObj(string Quote)
        {
            try
            {

                return (from qot in Con.Quotation
                        join qgr in Con.Quotation_Grupo on qot.Quotation_Id equals qgr.Quotation_Id
                        join fca in Con.File_Carrinho on qgr.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        where qot.Quotation_Code.Equals(Quote)
                        select fca).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public File_Carrinho RetornaFileObj(string Quote, int GrupoNbr)
        {
            try
            {

                return (from qot in Con.Quotation
                        join qgr in Con.Quotation_Grupo on qot.Quotation_Id equals qgr.Quotation_Id
                        join fca in Con.File_Carrinho on qgr.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        where qot.Quotation_Code.Equals(Quote) &&
                              qgr.Quotation_Grupo_NGrupo == GrupoNbr
                        select fca).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public int RetornaQuote(int IdFile)
        {
            try
            {

                return (from qot in Con.Quotation
                        join qgr in Con.Quotation_Grupo on qot.Quotation_Id equals qgr.Quotation_Id
                        join fca in Con.File_Carrinho on qgr.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        where fca.File_id == IdFile
                        select qot).SingleOrDefault().Quotation_Id;

            }
            catch
            {
                throw;
            }
        }

        public Quotation RetornaQuoteObj(string QuotationCode)
        {
            try
            {

                return Con.Quotation.Where(q => q.Quotation_Code.Equals(QuotationCode)).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExisteFile(int IdFile)
        {
            try
            {

                return ((from qse in Con.Quote_ServExtra
                         where qse.File_id == IdFile
                         select new
                         {
                             ID = qse.Quote_ServExtra_id
                         }).Union(
                        from qts in Con.Quote_Tarifas
                        where qts.File_id == IdFile
                        select new
                        {
                            ID = qts.Quote_Tarifas_id
                        }).Union(
                        from qtr in Con.Quote_Transf
                        where qtr.File_id == IdFile
                        select new
                        {
                            ID = qtr.Quote_Transf_id
                        })).Count() != 0;

            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExisteTCQuotes(int IdFile)
        {
            try
            {

                return ((from qse in Con.Quote_ServExtra
                         where qse.TC == true &&
                               qse.File_id == IdFile
                         select new
                         {
                             ID = qse.Quote_ServExtra_id
                         }).Union(
                        from qts in Con.Quote_Tarifas
                        where qts.TC == true &&
                              qts.File_id == IdFile
                        select new
                        {
                            ID = qts.Quote_Tarifas_id
                        }).Union(
                        from qtr in Con.Quote_Transf
                        where qtr.TC == true &&
                              qtr.File_id == IdFile
                        select new
                        {
                            ID = qtr.Quote_Transf_id
                        })).Count() != 0;

            }
            catch
            {
                throw;
            }
        }

        public List<TotalFileModel> ValoresTotaisFile(int IdFile)
        {
            SqlCon cn = new SqlCon();
            try
            {

                string sql = "SELECT Transf_total AS pSoma, Transf_total * Paying_Pax  AS Total, Flag, '' AS Room ";
                sql = sql + "FROM Quote_Transf WHERE (File_id = @p1) UNION ALL ";
                sql = sql + "SELECT S_merc_tarif_total AS pSoma, S_merc_tarif_total * Paying_Pax AS Total, Flag, Room ";
                sql = sql + "FROM Quote_Tarifas WHERE (File_id = @p1) UNION ALL ";
                sql = sql + "SELECT File_ServExtra_total AS pSoma, File_ServExtra_total * Paying_Pax  AS Total, Flag, '' AS Room ";
                sql = sql + "FROM Quote_ServExtra WHERE (File_id = @p1) ";

                cn.AbrirConexao();
                cn.Cmd = new SqlCommand(sql, cn.Con);
                cn.Cmd.Parameters.AddWithValue("@p1", IdFile);

                cn.Dr = cn.Cmd.ExecuteReader();

                List<TotalFileModel> lista = new List<TotalFileModel>();

                while (cn.Dr.Read())
                {
                    TotalFileModel tfm = new TotalFileModel();


                    //Mudado para o bradley pois o total dele por pessoa campo alterado ["Total"]
                    //tfm.total = Convert.ToDecimal(cn.Dr["pSoma"]);
                    tfm.total = Convert.ToDecimal(BrancoZero(cn.Dr["pSoma"].ToString()));
                    //

                    tfm.room = cn.Dr["Room"].ToString();
                    tfm.flag = cn.Dr["Flag"].ToString();

                    lista.Add(tfm);
                }

                return lista;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.FecharConexao();
            }
        }

        protected String BrancoZero(string texto)
        {
            if (texto.Equals(""))
            {
                return "0";
            }
            else
            {
                return texto;
            }
        }

        public List<FinanceiroFilesPendentesModel> ListarFilesPendentes(DateTime dtFrom, DateTime dtTo)
        {
            try
            {

                return (from quo in Con.Quotation
                        join qgp in Con.Quotation_Grupo on quo.Quotation_Id equals qgp.Quotation_Id
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                        join qga in Con.Quotation_Grupo_Qtd_Adult on qgp.Quotation_Grupo_Id equals qga.Quotation_Grupo_Id
                        join qts in Con.Quotation_Status on qgp.Quotation_status_id equals qts.Quotation_status_id
                        join use in Con.Usuarios on qgp.US_id equals use.US_id
                        where qgp.dataIN <= dtFrom &&
                              qgp.dateOut >= dtTo
                              ||
                              qgp.dataIN <= dtTo &&
                              qgp.dateOut >= dtFrom
                        select new FinanceiroFilesPendentesModel
                        {
                            Cliente = cli.Cliente_nome,
                            dtFrom = qgp.dataIN,
                            dtTo = qgp.dateOut,
                            NumQuote = quo.Quotation_Code,
                            NumFile = fca.File_id,
                            NumPaxs = qga.Qtd,
                            Operador = use.US_nome,
                            PaxGroupName = qgp.Pax_Group_Name,
                            StatusFile = qts.Quotation_status_nome,
                            idQuoteGrupo = qgp.Quotation_Grupo_Id
                        }).Distinct().OrderBy(c => c.Cliente).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<PaxListModel> ListarFilesPaxList(DateTime dtFrom, DateTime dtTo)
        {
            try
            {

                return (from quo in Con.Quotation
                        join qgp in Con.Quotation_Grupo on quo.Quotation_Id equals qgp.Quotation_Id
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                        join qga in Con.Quotation_Grupo_Qtd_Adult on qgp.Quotation_Grupo_Id equals qga.Quotation_Grupo_Id
                        join qts in Con.Quotation_Status on qgp.Quotation_status_id equals qts.Quotation_status_id
                        join use in Con.Usuarios on qgp.US_id equals use.US_id
                        where qgp.dataIN <= dtFrom &&
                              qgp.dateOut >= dtTo
                              ||
                              qgp.dataIN <= dtTo &&
                              qgp.dateOut >= dtFrom
                        select new PaxListModel
                        {
                            Cliente = cli.Cliente_nome,
                            dtFrom = qgp.dataIN,
                            dtTo = qgp.dataQuote,
                            NumQuote = quo.Quotation_Code,
                            NumFile = fca.File_id,
                            NumPaxs = qga.Qtd,
                            Operador = use.US_nome,
                            PaxGroupName = qgp.Pax_Group_Name,
                            StatusFile = qts.Quotation_status_nome,
                            idQuoteGrupo = qgp.Quotation_Grupo_Id
                        }).Distinct().OrderBy(c => c.Cliente).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<QGeralReportModel> ListarFilesQGCliente(DateTime dtFrom, DateTime dtTo)
        {
            try
            {
                return (from quo in Con.Quotation
                        join qgp in Con.Quotation_Grupo on quo.Quotation_Id equals qgp.Quotation_Id
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                        join qga in Con.Quotation_Grupo_Qtd_Adult on qgp.Quotation_Grupo_Id equals qga.Quotation_Grupo_Id
                        join qts in Con.Quotation_Status on qgp.Quotation_status_id equals qts.Quotation_status_id
                        join use in Con.Usuarios on qgp.US_id equals use.US_id
                        where qgp.dataIN <= dtFrom &&
                              qgp.dateOut >= dtTo
                              ||
                              qgp.dataIN <= dtTo &&
                              qgp.dateOut >= dtFrom
                        select new QGeralReportModel
                        {
                            Cliente = cli.Cliente_nome,
                            idCliente = cli.Cliente_id,
                            dtFrom = qgp.dataIN,
                            dtTo = qgp.dateOut,
                            NumQuote = quo.Quotation_Code,
                            NumFile = fca.File_id,
                            NumPaxs = qga.Qtd,
                            idQuoteGrupo = qgp.Quotation_Grupo_Id
                        }).Distinct().OrderBy(c => c.Cliente).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<ApuracaoReceitaOPModel> ListarFileApuracaoReceita(DateTime dtFrom, DateTime dtTo)
        {
            try
            {

                return (from quo in Con.Quotation
                        join qgp in Con.Quotation_Grupo on quo.Quotation_Id equals qgp.Quotation_Id
                        //join cbf in Con.Cambio_File on qgp.Quotation_Grupo_Id equals cbf.Quotation_Grupo_Id
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                        join qga in Con.Quotation_Grupo_Qtd_Adult on qgp.Quotation_Grupo_Id equals qga.Quotation_Grupo_Id
                        join qts in Con.Quotation_Status on qgp.Quotation_status_id equals qts.Quotation_status_id
                        join use in Con.Usuarios on qgp.US_id equals use.US_id
                        where qgp.dataIN <= dtFrom &&
                              qgp.dateOut >= dtTo
                              ||
                              qgp.dataIN <= dtTo &&
                              qgp.dateOut >= dtFrom
                        select new ApuracaoReceitaOPModel
                        {
                            Cliente = cli.Cliente_nome,
                            dtFrom = qgp.dataIN,
                            dtTo = qgp.dateOut,
                            NumQuote = quo.Quotation_Code,
                            NumFile = fca.File_id,
                            NumPaxs = qga.Qtd,
                            Operador = use.US_nome,
                            PaxGroupName = qgp.Pax_Group_Name,
                            StatusFile = qts.Quotation_status_nome,
                            idQuoteGrupo = qgp.Quotation_Grupo_Id
                            //cambioFile = cbf.CambioFile == null ? 0 : (decimal)cbf.CambioFile
                        }).Distinct().OrderBy(c => c.Cliente).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<TarifasFileModel> ListarFilesPendentesValores(int IdFile)
        {
            try
            {

                return ((from qta in Con.Quote_Tarifas
                         where qta.File_id == IdFile
                         select new TarifasFileModel
                         {
                             Tarifa = qta.S_merc_tarif_total == null ? 0 : (decimal)qta.S_merc_tarif_total,
                             NumPaxItem = (int)qta.Paying_Pax,
                             numNoites = (int)qta.NumNoites,
                             Flag = "hotel"
                         }).ToList()).Union
                        ((from qtt in Con.Quote_Transf
                          where qtt.File_id == IdFile
                          select new TarifasFileModel
                          {
                              Tarifa = qtt.Transf_total == null ? 0 : (decimal)qtt.Transf_total,
                              NumPaxItem = (int)qtt.Paying_Pax,
                              numNoites = 1,
                              Flag = "servico"
                          }).ToList()).Union
                         ((from qse in Con.Quote_ServExtra
                           where qse.File_id == IdFile
                           select new TarifasFileModel
                           {
                               Tarifa = qse.File_ServExtra_total == null ? 0 : (decimal)qse.File_ServExtra_total,
                               NumPaxItem = (int)qse.Paying_Pax,
                               numNoites = 1,
                               Flag = "extra"
                           }).ToList()).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TarifasFileModel> ListarFilesPendentesValoresNet(int IdFile)
        {
            try
            {

                return ((from qta in Con.Quote_Tarifas
                         where qta.File_id == IdFile
                         select new TarifasFileModel
                         {
                             Tarifa = qta.S_merc_tarif_valor == null ? 0 : (decimal)qta.S_merc_tarif_valor,
                             NumPaxItem = (int)qta.Paying_Pax,
                             numNoites = (int)qta.NumNoites,
                             Flag = "hotel"
                         }).ToList()).Union
                        ((from qtt in Con.Quote_Transf
                          where qtt.File_id == IdFile
                          select new TarifasFileModel
                          {
                              Tarifa = qtt.Transf_valor == null ? 0 : (decimal)qtt.Transf_valor,
                              NumPaxItem = (int)qtt.Paying_Pax,
                              numNoites = 1,
                              Flag = "servico"
                          }).ToList()).Union
                         ((from qse in Con.Quote_ServExtra
                           where qse.File_id == IdFile
                           select new TarifasFileModel
                           {
                               Tarifa = qse.File_ServExtra_valor == null ? 0 : (decimal)qse.File_ServExtra_valor,
                               NumPaxItem = (int)qse.Paying_Pax,
                               numNoites = 1,
                               Flag = "extra"
                           }).ToList()).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<FileInfosModel> ListarFilesApuracaoReceita(int IdFile)
        {
            try
            {

                return ((from qta in Con.Quote_Tarifas
                         where qta.File_id == IdFile
                         select new FileInfosModel
                         {
                             IdTabela = qta.Quote_Tarifas_id,
                             TarifaNet = qta.S_merc_tarif_valor == null ? 0 : (decimal)qta.S_merc_tarif_valor,
                             VlrVenda = qta.S_merc_tarif_total == null ? 0 : (decimal)qta.S_merc_tarif_total,
                             Fornecedor = qta.S_nome,
                             idFornecedor = (int)qta.S_id,
                             MealStatus = qta.S_meal_status,
                             TipoServ = "Hotel",
                             room_meal = qta.Meal == true ? qta.S_meal_nome : qta.Categoria + " - " + qta.Room,
                             NumPaxItem = qta.Paying_Pax == null ? 0 : (int)qta.Paying_Pax,
                             numNoites = qta.NumNoites == null ? 0 : (int)qta.NumNoites,
                             Flag = "hotel",
                             NomeItem = qta.S_nome,
                             VlrConferido = qta.Valor_Conferir == null ? 0 : (decimal)qta.Valor_Conferir,
                             cambioConf = qta.Cambio == null ? 0 : (decimal)qta.Cambio,
                             SubServico = false
                         }).ToList()).Union
                        ((from qtt in Con.Quote_Transf
                          where qtt.File_id == IdFile
                          select new FileInfosModel
                          {
                              IdTabela = qtt.Quote_Transf_id,
                              TarifaNet = qtt.Transf_valor == null ? 0 : (decimal)qtt.Transf_valor,
                              VlrVenda = qtt.Transf_total == null ? 0 : (decimal)qtt.Transf_total,
                              Fornecedor = qtt.S_nome,
                              idFornecedor = (int)qtt.S_id,
                              room_meal = "",
                              MealStatus = "",
                              TipoServ = qtt.Trf_Tours,
                              NumPaxItem = qtt.Paying_Pax == null ? 0 : (int)qtt.Paying_Pax,
                              numNoites = 1,
                              Flag = "servico",
                              NomeItem = qtt.Transf_nome,
                              VlrConferido = qtt.Valor_Conferir == null ? 0 : (decimal)qtt.Valor_Conferir,
                              cambioConf = qtt.Cambio == null ? 0 : (decimal)qtt.Cambio,
                              SubServico = qtt.SubServico == true ? true : false
                          }).ToList()).Union
                         ((from qse in Con.Quote_ServExtra
                           where qse.File_id == IdFile
                           select new FileInfosModel
                           {
                               IdTabela = qse.Quote_ServExtra_id,
                               TarifaNet = qse.File_ServExtra_valor == null ? 0 : (decimal)qse.File_ServExtra_valor,
                               VlrVenda = qse.File_ServExtra_total == null ? 0 : (decimal)qse.File_ServExtra_total,
                               Fornecedor = qse.S_nome,
                               idFornecedor = (int)qse.S_id,
                               room_meal = "",
                               MealStatus = "",
                               TipoServ = "Extra",
                               NumPaxItem = qse.Paying_Pax == null ? 0 : (int)qse.Paying_Pax,
                               numNoites = 1,
                               Flag = "extra",
                               NomeItem = qse.File_ServExtra_nome,
                               VlrConferido = qse.Valor_Conferir == null ? 0 : (decimal)qse.Valor_Conferir,
                               cambioConf = qse.Cambio == null ? 0 : (decimal)qse.Cambio,
                               SubServico = false
                           }).ToList()).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<ListInvoiceModel> ListarRptInvoices(DateTime dtFrom, DateTime dtTo, int type)
        {
            try
            {

                if (type == 0)
                {
                    return (from inv in Con.Invoice
                            join qgp in Con.Quotation_Grupo on inv.Quotation_Grupo_Id equals qgp.Quotation_Grupo_Id
                            join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                            join quo in Con.Quotation on qgp.Quotation_Id equals quo.Quotation_Id
                            join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                            where qgp.dataIN <= dtFrom &&
                                  qgp.dateOut >= dtTo
                                  ||
                                  qgp.dataIN <= dtTo &&
                                  qgp.dateOut >= dtFrom
                            select new ListInvoiceModel
                            {
                                File_id = fca.File_id,
                                File = quo.Quotation_Code,
                                dtFrom = qgp.dataIN,
                                dtTo = qgp.dateOut,
                                InvNumber = inv.Invoice_id,
                                VrInv = (decimal)inv.Grand_Total,
                                dtEmissao = (DateTime)inv.DataEmissao,
                                Cliente = cli.Cliente_nome
                            }).OrderBy(s => s.dtFrom).ToList();
                }
                else
                {
                    return (from inv in Con.Invoice
                            join qgp in Con.Quotation_Grupo on inv.Quotation_Grupo_Id equals qgp.Quotation_Grupo_Id
                            join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                            join quo in Con.Quotation on qgp.Quotation_Id equals quo.Quotation_Id
                            join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                            where inv.DataEmissao >= dtFrom &&
                                  inv.DataEmissao <= dtTo
                            select new ListInvoiceModel
                            {
                                File_id = fca.File_id,
                                File = quo.Quotation_Code,
                                dtFrom = qgp.dataIN,
                                dtTo = qgp.dateOut,
                                InvNumber = inv.Invoice_id,
                                VrInv = (decimal)inv.Grand_Total,
                                dtEmissao = (DateTime)inv.DataEmissao,
                                Cliente = cli.Cliente_nome
                            }).OrderBy(s => s.dtFrom).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public List<ListInvoiceModel> ListarRptInvoices(DateTime dtFrom, DateTime dtTo, DateTime dtFromReceb, DateTime dtToReceb, int type)
        {
            try
            {

                if (type == 0)
                {
                    return (from inv in Con.Invoice
                            join qgp in Con.Quotation_Grupo on inv.Quotation_Grupo_Id equals qgp.Quotation_Grupo_Id
                            join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                            join quo in Con.Quotation on qgp.Quotation_Id equals quo.Quotation_Id
                            join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                            join ipg in Con.Invoice_Pagamento on inv.Invoice_id equals ipg.Invoice_id
                            where qgp.dataIN >= dtFrom &&
                                  qgp.dataIN <= dtTo &&
                                  ipg.data >= dtFromReceb &&
                                  ipg.data <= dtToReceb
                            select new ListInvoiceModel
                            {
                                File_id = fca.File_id,
                                File = quo.Quotation_Code,
                                dtFrom = qgp.dataIN,
                                dtTo = qgp.dateOut,
                                InvNumber = inv.Invoice_id,
                                VrInv = (decimal)inv.Grand_Total,
                                dtEmissao = (DateTime)inv.DataEmissao,
                                Cliente = cli.Cliente_nome
                            }).OrderBy(s => s.dtFrom).ToList();
                }
                else
                {
                    return (from inv in Con.Invoice
                            join qgp in Con.Quotation_Grupo on inv.Quotation_Grupo_Id equals qgp.Quotation_Grupo_Id
                            join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                            join quo in Con.Quotation on qgp.Quotation_Id equals quo.Quotation_Id
                            join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                            join ipg in Con.Invoice_Pagamento on inv.Invoice_id equals ipg.Invoice_id
                            where inv.DataEmissao >= dtFrom &&
                                  inv.DataEmissao <= dtTo &&
                                  ipg.data >= dtFromReceb &&
                                  ipg.data <= dtToReceb
                            select new ListInvoiceModel
                            {
                                File_id = fca.File_id,
                                File = quo.Quotation_Code,
                                dtFrom = qgp.dataIN,
                                dtTo = qgp.dateOut,
                                InvNumber = inv.Invoice_id,
                                VrInv = (decimal)inv.Grand_Total,
                                dtEmissao = (DateTime)inv.DataEmissao,
                                Cliente = cli.Cliente_nome
                            }).OrderBy(s => s.dtFrom).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public List<CreditListModel> ListarRptCreditList(DateTime dtFrom, DateTime dtTo, int idCliente)
        {
            try
            {

                if (idCliente == 0)
                {
                    return (from ivp in Con.Invoice_Pagamento
                            join inv in Con.Invoice on ivp.Invoice_id equals inv.Invoice_id
                            join qgp in Con.Quotation_Grupo on inv.Quotation_Grupo_Id equals qgp.Quotation_Grupo_Id
                            join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                            join quo in Con.Quotation on qgp.Quotation_Id equals quo.Quotation_Id
                            join fco in Con.F_Conta on ivp.F_Conta_id equals fco.id
                            where ivp.data >= dtFrom &&
                                  ivp.data <= dtTo
                            select new CreditListModel
                            {
                                dtRecebimento = (DateTime)ivp.data,
                                Cliente = cli.Cliente_nome,
                                NomeConta = fco.Nome,
                                File = quo.Quotation_Code,
                                InvoiceNbr = inv.Invoice_id,
                                VrPag = (decimal)ivp.Valor,
                                InvHistorico = ivp.obs,
                                PaxGroupName = qgp.Pax_Group_Name
                            }).ToList();
                }
                else
                {
                    return (from ivp in Con.Invoice_Pagamento
                            join inv in Con.Invoice on ivp.Invoice_id equals inv.Invoice_id
                            join qgp in Con.Quotation_Grupo on inv.Quotation_Grupo_Id equals qgp.Quotation_Grupo_Id
                            join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                            join quo in Con.Quotation on qgp.Quotation_Id equals quo.Quotation_Id
                            join fco in Con.F_Conta on ivp.F_Conta_id equals fco.id
                            where ivp.data >= dtFrom &&
                                  ivp.data <= dtTo &&
                                  qgp.Cliente_id == idCliente
                            select new CreditListModel
                            {
                                dtRecebimento = (DateTime)ivp.data,
                                Cliente = cli.Cliente_nome,
                                NomeConta = fco.Nome,
                                File = quo.Quotation_Code,
                                InvoiceNbr = inv.Invoice_id,
                                VrPag = (decimal)ivp.Valor,
                                InvHistorico = ivp.obs,
                                PaxGroupName = qgp.Pax_Group_Name
                            }).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public List<RoomNightsModel> ListarRptRoomNights(int IdFile)
        {
            try
            {
                return (from qta in Con.Quote_Tarifas
                        where qta.File_id == IdFile
                        select new RoomNightsModel
                        {
                            status = qta.Status.Equals("OK") ? true : false,
                            Hotel = qta.S_nome,
                            dtFrom = (DateTime)qta.Data_From,
                            dtTo = (DateTime)qta.Data_To,
                            idRoom = qta.Room_id == null ? 0 : (int)qta.Room_id,
                            qtdPax = qta.Paying_Pax == null ? 0 : (int)qta.Paying_Pax,
                            vrTotal = qta.S_merc_tarif_valor == null ? 0 : (decimal)qta.S_merc_tarif_valor
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<PrevPgtoForncModel> ListarRptPrevPgtoFornec(int IdFile)
        {
            try
            {

                return ((from qta in Con.Quote_Tarifas
                         where qta.File_id == IdFile
                         select new PrevPgtoForncModel
                         {
                             IdTabela = qta.Quote_Tarifas_id,
                             TarifaNet = qta.S_merc_tarif_valor == null ? 0 : (decimal)qta.S_merc_tarif_valor,
                             VlrVenda = qta.S_merc_tarif_total == null ? 0 : (decimal)qta.S_merc_tarif_total,
                             Fornecedor = qta.S_nome,
                             idFornecedor = (int)qta.S_id,
                             MealStatus = qta.S_meal_status,
                             TipoServ = "Hotel",
                             NumPaxItem = (int)qta.Paying_Pax,
                             numNoites = (int)qta.NumNoites,
                             Flag = "hotel",
                             NomeItem = qta.S_nome,
                             SubServico = false,
                             dtFrom = (DateTime)qta.Data_From,
                             dtTo = (DateTime)qta.Data_To,
                             status = qta.Status.Equals("OK") ? "OK" : "OR"
                         }).ToList()).Union
                        ((from qtt in Con.Quote_Transf
                          where qtt.File_id == IdFile
                          select new PrevPgtoForncModel
                          {
                              IdTabela = qtt.Quote_Transf_id,
                              TarifaNet = qtt.Transf_valor == null ? 0 : (decimal)qtt.Transf_valor,
                              VlrVenda = qtt.Transf_total == null ? 0 : (decimal)qtt.Transf_total,
                              Fornecedor = qtt.S_nome,
                              idFornecedor = (int)qtt.S_id,
                              MealStatus = "",
                              TipoServ = qtt.Trf_Tours,
                              NumPaxItem = (int)qtt.Paying_Pax,
                              numNoites = 1,
                              Flag = "servico",
                              NomeItem = qtt.Transf_nome,
                              SubServico = qtt.SubServico == true ? true : false,
                              dtFrom = (DateTime)qtt.Data_From,
                              dtTo = (DateTime)qtt.Data_To,
                              status = qtt.Status.Equals("OK") ? "OK" : "OR"
                          }).ToList()).Union
                         ((from qse in Con.Quote_ServExtra
                           where qse.File_id == IdFile
                           select new PrevPgtoForncModel
                           {
                               IdTabela = qse.Quote_ServExtra_id,
                               TarifaNet = qse.File_ServExtra_valor == null ? 0 : (decimal)qse.File_ServExtra_valor,
                               VlrVenda = qse.File_ServExtra_total == null ? 0 : (decimal)qse.File_ServExtra_total,
                               Fornecedor = qse.S_nome,
                               idFornecedor = (int)qse.S_id,
                               TipoServ = "Extra",
                               NumPaxItem = (int)qse.Paying_Pax,
                               numNoites = 1,
                               Flag = "extra",
                               NomeItem = qse.File_ServExtra_nome,
                               SubServico = false,
                               dtFrom = (DateTime)qse.Data_From,
                               dtTo = (DateTime)qse.Data_To,
                               status = qse.Status.Equals("OK") ? "OK" : "OR"
                           }).ToList()).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<QGeralReportModel> ListarFilesQGCliente_RetornaDistinct(DateTime dtFrom, DateTime dtTo)
        {
            try
            {
                return (from quo in Con.Quotation
                        join qgp in Con.Quotation_Grupo on quo.Quotation_Id equals qgp.Quotation_Id
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                        join qga in Con.Quotation_Grupo_Qtd_Adult on qgp.Quotation_Grupo_Id equals qga.Quotation_Grupo_Id
                        join qts in Con.Quotation_Status on qgp.Quotation_status_id equals qts.Quotation_status_id
                        join use in Con.Usuarios on qgp.US_id equals use.US_id
                        where qgp.dataIN >= dtFrom &&
                              qgp.dataIN <= dtTo
                        select new QGeralReportModel
                        {
                            Cliente = cli.Cliente_nome,
                            idCliente = cli.Cliente_id,
                            idPais = (int)cli.PAIS_id
                        }).Distinct().OrderBy(c => c.Cliente).ToList();
            }
            catch
            {
                throw;
            }
        }

        public string RetornaQuoteCode(int IdFile)
        {
            try
            {
                return (from qgr in Con.Quotation_Grupo
                        join qta in Con.Quotation on qgr.Quotation_Id equals qta.Quotation_Id
                        join fcr in Con.File_Carrinho on qgr.Quotation_Grupo_Id equals fcr.Quotation_Grupo_Id
                        where fcr.File_id == IdFile
                        select qta).SingleOrDefault().Quotation_Code;
            }
            catch
            {
                throw;
            }
        }


        public List<ApuracaoReceitaOPModel> ListarFileApuracaoReceita(DateTime dtFrom, DateTime dtTo, string File)
        {
            try
            {

                return (from quo in Con.Quotation
                        join qgp in Con.Quotation_Grupo on quo.Quotation_Id equals qgp.Quotation_Id
                        join cbf in Con.Cambio_File on qgp.Quotation_Grupo_Id equals cbf.Quotation_Grupo_Id into f
                        from m in f.DefaultIfEmpty()
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                        join qga in Con.Quotation_Grupo_Qtd_Adult on qgp.Quotation_Grupo_Id equals qga.Quotation_Grupo_Id
                        join qts in Con.Quotation_Status on qgp.Quotation_status_id equals qts.Quotation_status_id
                        join use in Con.Usuarios on qgp.US_id equals use.US_id
                        where qgp.dataIN <= dtFrom &&
                              qgp.dateOut >= dtTo &&
                              quo.Quotation_Code.Equals(File)
                              ||
                              qgp.dataIN <= dtTo &&
                              qgp.dateOut >= dtFrom &&
                              quo.Quotation_Code.Equals(File)
                        select new ApuracaoReceitaOPModel
                        {
                            Cliente = cli.Cliente_nome,
                            dtFrom = qgp.dataIN,
                            dtTo = qgp.dateOut,
                            NumQuote = quo.Quotation_Code,
                            NumFile = fca.File_id,
                            NumPaxs = qga.Qtd,
                            Operador = use.US_nome,
                            PaxGroupName = qgp.Pax_Group_Name,
                            StatusFile = qts.Quotation_status_nome,
                            idQuoteGrupo = qgp.Quotation_Grupo_Id,
                            cambioFile = m.CambioFile == null ? 0 : (decimal)m.CambioFile
                        }).Distinct().OrderBy(c => c.Cliente).ToList();

            }
            catch
            {
                throw;
            }
        }



        public List<TarifasFileModel> ListarFilesPendentesValores_Cliente(int idCliente, DateTime dtFrom, DateTime dtTo, int type)
        {
            try
            {
                if (type == 0)
                {
                    return ((from qta in Con.Quote_Tarifas
                             join fca in Con.File_Carrinho on qta.File_id equals fca.File_id
                             join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                             join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                             where quo.dataIN >= dtFrom &&
                                   quo.dataIN <= dtTo &&
                                   qta.Status.Equals("OK") &&
                                   quo.Cliente_id == idCliente
                             select new TarifasFileModel
                             {
                                 Tarifa = qta.S_merc_tarif_total == null ? 0 : (decimal)qta.S_merc_tarif_total,
                                 NumPaxItem = (int)qta.Paying_Pax,
                                 numNoites = (int)qta.NumNoites,
                                 Flag = "hotel"
                             }).ToList()).Union
                            ((from qtta in Con.Quote_Transf
                              join fca in Con.File_Carrinho on qtta.File_id equals fca.File_id
                              join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                              join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                              where quo.dataIN >= dtFrom &&
                                   quo.dataIN <= dtTo &&
                                   qtta.Status.Equals("OK") &&
                                   quo.Cliente_id == idCliente
                              select new TarifasFileModel
                              {
                                  Tarifa = qtta.Transf_total == null ? 0 : (decimal)qtta.Transf_total,
                                  NumPaxItem = (int)qtta.Paying_Pax,
                                  numNoites = 1,
                                  Flag = "servico"
                              }).ToList()).Union
                             ((from qse in Con.Quote_ServExtra
                               join fca in Con.File_Carrinho on qse.File_id equals fca.File_id
                               join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                               join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                               where quo.dataIN >= dtFrom &&
                                   quo.dataIN <= dtTo &&
                                   qse.Status.Equals("OK") &&
                                   quo.Cliente_id == idCliente
                               select new TarifasFileModel
                               {
                                   Tarifa = qse.File_ServExtra_total == null ? 0 : (decimal)qse.File_ServExtra_total,
                                   NumPaxItem = (int)qse.Paying_Pax,
                                   numNoites = 1,
                                   Flag = "extra"
                               }).ToList()).ToList();

                }
                else
                {
                    return ((from qta in Con.Quote_Tarifas
                             join fca in Con.File_Carrinho on qta.File_id equals fca.File_id
                             join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                             join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                             where quo.dataIN >= dtFrom &&
                                   quo.dataIN <= dtTo &&
                                   qta.Status.Equals("OP") &&
                                   quo.Cliente_id == idCliente
                             select new TarifasFileModel
                             {
                                 Tarifa = qta.S_merc_tarif_total == null ? 0 : (decimal)qta.S_merc_tarif_total,
                                 NumPaxItem = (int)qta.Paying_Pax,
                                 numNoites = (int)qta.NumNoites,
                                 Flag = "hotel"
                             }).ToList()).Union
                        ((from qtta in Con.Quote_Transf
                          join fca in Con.File_Carrinho on qtta.File_id equals fca.File_id
                          join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                          join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                          where quo.dataIN >= dtFrom &&
                               quo.dataIN <= dtTo &&
                               qtta.Status.Equals("OP") &&
                               quo.Cliente_id == idCliente
                          select new TarifasFileModel
                          {
                              Tarifa = qtta.Transf_total == null ? 0 : (decimal)qtta.Transf_total,
                              NumPaxItem = (int)qtta.Paying_Pax,
                              numNoites = 1,
                              Flag = "servico"
                          }).ToList()).Union
                         ((from qse in Con.Quote_ServExtra
                           join fca in Con.File_Carrinho on qse.File_id equals fca.File_id
                           join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                           join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                           where quo.dataIN >= dtFrom &&
                               quo.dataIN <= dtTo &&
                               qse.Status.Equals("OP") &&
                               quo.Cliente_id == idCliente
                           select new TarifasFileModel
                           {
                               Tarifa = qse.File_ServExtra_total == null ? 0 : (decimal)qse.File_ServExtra_total,
                               NumPaxItem = (int)qse.Paying_Pax,
                               numNoites = 1,
                               Flag = "extra"
                           }).ToList()).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<TarifasFileModel> ListarFilesPendentesValores_Cliente_MultPaxs(int idCliente, DateTime dtFrom, DateTime dtTo, int type)
        {
            try
            {
                if (type == 0)
                {
                    return ((from qta in Con.Quote_Tarifas
                             join fca in Con.File_Carrinho on qta.File_id equals fca.File_id
                             join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                             join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                             where quo.dataIN >= dtFrom &&
                                   quo.dataIN <= dtTo &&
                                   qta.Status.Equals("OK") &&
                                   quo.Cliente_id == idCliente
                             select new TarifasFileModel
                             {
                                 Tarifa = qta.S_merc_tarif_total == null ? 0 : (decimal)qta.S_merc_tarif_total * (int)qta.Paying_Pax,
                                 NumPaxItem = (int)qta.Paying_Pax,
                                 numNoites = (int)qta.NumNoites,
                                 Flag = "hotel"
                             }).ToList()).Union
                            ((from qtta in Con.Quote_Transf
                              join fca in Con.File_Carrinho on qtta.File_id equals fca.File_id
                              join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                              join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                              where quo.dataIN >= dtFrom &&
                                   quo.dataIN <= dtTo &&
                                   qtta.Status.Equals("OK") &&
                                   quo.Cliente_id == idCliente
                              select new TarifasFileModel
                              {
                                  Tarifa = qtta.Transf_total == null ? 0 : (decimal)qtta.Transf_total * (int)qtta.Paying_Pax,
                                  NumPaxItem = (int)qtta.Paying_Pax,
                                  numNoites = 1,
                                  Flag = "servico"
                              }).ToList()).Union
                             ((from qse in Con.Quote_ServExtra
                               join fca in Con.File_Carrinho on qse.File_id equals fca.File_id
                               join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                               join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                               where quo.dataIN >= dtFrom &&
                                   quo.dataIN <= dtTo &&
                                   qse.Status.Equals("OK") &&
                                   quo.Cliente_id == idCliente
                               select new TarifasFileModel
                               {
                                   Tarifa = qse.File_ServExtra_total == null ? 0 : (decimal)qse.File_ServExtra_total * (int)qse.Paying_Pax,
                                   NumPaxItem = (int)qse.Paying_Pax,
                                   numNoites = 1,
                                   Flag = "extra"
                               }).ToList()).ToList();

                }
                else
                {
                    return ((from qta in Con.Quote_Tarifas
                             join fca in Con.File_Carrinho on qta.File_id equals fca.File_id
                             join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                             join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                             where quo.dataIN >= dtFrom &&
                                   quo.dataIN <= dtTo &&
                                   qta.Status.Equals("OP") &&
                                   quo.Cliente_id == idCliente
                             select new TarifasFileModel
                             {
                                 Tarifa = qta.S_merc_tarif_total == null ? 0 : (decimal)qta.S_merc_tarif_total * (int)qta.Paying_Pax,
                                 NumPaxItem = (int)qta.Paying_Pax,
                                 numNoites = (int)qta.NumNoites,
                                 Flag = "hotel"
                             }).ToList()).Union
                        ((from qtta in Con.Quote_Transf
                          join fca in Con.File_Carrinho on qtta.File_id equals fca.File_id
                          join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                          join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                          where quo.dataIN >= dtFrom &&
                               quo.dataIN <= dtTo &&
                               qtta.Status.Equals("OP") &&
                               quo.Cliente_id == idCliente
                          select new TarifasFileModel
                          {
                              Tarifa = qtta.Transf_total == null ? 0 : (decimal)qtta.Transf_total * (int)qtta.Paying_Pax,
                              NumPaxItem = (int)qtta.Paying_Pax,
                              numNoites = 1,
                              Flag = "servico"
                          }).ToList()).Union
                         ((from qse in Con.Quote_ServExtra
                           join fca in Con.File_Carrinho on qse.File_id equals fca.File_id
                           join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                           join qtt in Con.Quotation on quo.Quotation_Id equals qtt.Quotation_Id
                           where quo.dataIN >= dtFrom &&
                               quo.dataIN <= dtTo &&
                               qse.Status.Equals("OP") &&
                               quo.Cliente_id == idCliente
                           select new TarifasFileModel
                           {
                               Tarifa = qse.File_ServExtra_total == null ? 0 : (decimal)qse.File_ServExtra_total * (int)qse.Paying_Pax,
                               NumPaxItem = (int)qse.Paying_Pax,
                               numNoites = 1,
                               Flag = "extra"
                           }).ToList()).ToList();
                }

            }
            catch
            {
                throw;
            }
        }


        public List<TarifasFileModel> ListarFilesPendentesValoresNet_NConferidos(int IdFile)
        {
            try
            {

                return ((from qta in Con.Quote_Tarifas
                         where qta.File_id == IdFile &&
                               qta.Valor_Conferir == null
                         select new TarifasFileModel
                         {
                             Tarifa = qta.S_merc_tarif_valor == null ? 0 : (decimal)qta.S_merc_tarif_valor,
                             NumPaxItem = (int)qta.Paying_Pax,
                             numNoites = (int)qta.NumNoites,
                             Flag = "hotel",
                             sub = false
                         }).ToList()).Union
                        ((from qtt in Con.Quote_Transf
                          where qtt.File_id == IdFile &&
                               qtt.Valor_Conferir == null
                          select new TarifasFileModel
                          {
                              Tarifa = qtt.Transf_valor == null ? 0 : (decimal)qtt.Transf_valor,
                              NumPaxItem = (int)qtt.Paying_Pax,
                              numNoites = 1,
                              Flag = "servico",
                              sub = qtt.SubServico == null ? false : true
                          }).ToList()).Union
                         ((from qse in Con.Quote_ServExtra
                           where qse.File_id == IdFile &&
                               qse.Valor_Conferir == null
                           select new TarifasFileModel
                           {
                               Tarifa = qse.File_ServExtra_valor == null ? 0 : (decimal)qse.File_ServExtra_valor,
                               NumPaxItem = (int)qse.Paying_Pax,
                               numNoites = 1,
                               Flag = "extra",
                               sub = false
                           }).ToList()).ToList();
            }
            catch
            {
                throw;
            }
        }


        public List<ItemsRoomsNightsModel> ListarFilesRoomNights(DateTime dtFrom, DateTime dtTo)
        {
            try
            {
                return (from qut in Con.Quote_Tarifas
                        join fca in Con.File_Carrinho on qut.File_id equals fca.File_id
                        join quo in Con.Quotation_Grupo on fca.Quotation_Grupo_Id equals quo.Quotation_Grupo_Id
                        join qot in Con.Quotation on quo.Quotation_Id equals qot.Quotation_Id
                        where qut.Data_From <= dtFrom &&
                              qut.Data_To >= dtTo
                              ||
                              qut.Data_From <= dtTo &&
                              qut.Data_To >= dtFrom
                        select new ItemsRoomsNightsModel
                        {
                            dtFrom = (DateTime)qut.Data_From,
                            dtTo = (DateTime)qut.Data_To,
                            idQuoteGrupo = quo.Quotation_Grupo_Id,
                            NumFile = fca.File_id,
                            NumPaxs = (int)qut.Paying_Pax,
                            NumQuote = qot.Quotation_Code,
                            status = qut.Status.Equals("OK") ? true : false
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }


        public List<PaxListModel> ListarFilesPaxListPrevPgto(DateTime dtFrom, DateTime dtTo)
        {
            try
            {

                return (from quo in Con.Quotation
                        join qgp in Con.Quotation_Grupo on quo.Quotation_Id equals qgp.Quotation_Id
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                        join qga in Con.Quotation_Grupo_Qtd_Adult on qgp.Quotation_Grupo_Id equals qga.Quotation_Grupo_Id
                        join qts in Con.Quotation_Status on qgp.Quotation_status_id equals qts.Quotation_status_id
                        join use in Con.Usuarios on qgp.US_id equals use.US_id
                        where qgp.dataIN >= dtFrom &&
                              qgp.dataIN <= dtTo
                        select new PaxListModel
                        {
                            idCliente = cli.Cliente_id,
                            Cliente = cli.Cliente_nome,
                            dtFrom = qgp.dataIN,
                            dtToFile = qgp.dateOut,
                            dtTo = qgp.dataQuote,
                            NumQuote = quo.Quotation_Code,
                            NumFile = fca.File_id,
                            NumPaxs = qga.Qtd,
                            Operador = use.US_nome,
                            PaxGroupName = qgp.Pax_Group_Name,
                            StatusFile = qts.Quotation_status_nome,
                            idQuoteGrupo = qgp.Quotation_Grupo_Id
                        }).Distinct().OrderBy(c => c.Cliente).ToList();

            }
            catch
            {
                throw;
            }
        }


        public List<PaxListModel> ListarFilesPaxListApuracaoNF(DateTime dtFrom, DateTime dtTo)
        {
            try
            {

                return (from quo in Con.Quotation
                        join qgp in Con.Quotation_Grupo on quo.Quotation_Id equals qgp.Quotation_Id
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgp.Cliente_id equals cli.Cliente_id
                        join qga in Con.Quotation_Grupo_Qtd_Adult on qgp.Quotation_Grupo_Id equals qga.Quotation_Grupo_Id
                        join qts in Con.Quotation_Status on qgp.Quotation_status_id equals qts.Quotation_status_id
                        join use in Con.Usuarios on qgp.US_id equals use.US_id
                        where qgp.dataIN >= dtFrom &&
                              qgp.dataIN <= dtTo
                        select new PaxListModel
                        {
                            idCliente = cli.Cliente_id,
                            Cliente = cli.Cliente_nome,
                            dtFrom = qgp.dataIN,
                            dtToFile = qgp.dateOut,
                            dtTo = qgp.dataQuote,
                            NumQuote = quo.Quotation_Code,
                            NumFile = fca.File_id,
                            NumPaxs = qga.Qtd,
                            Operador = use.US_nome,
                            PaxGroupName = qgp.Pax_Group_Name,
                            StatusFile = qts.Quotation_status_nome,
                            idQuoteGrupo = qgp.Quotation_Grupo_Id
                        }).Distinct().OrderBy(c => c.Cliente).ToList();

            }
            catch
            {
                throw;
            }
        }


        public List<QuoteIdsDelModel> MontaCarrinhoNewFileDel(int IdQuotGrupo, int OptQuote)
        {
            try
            {
                return ((from fcr in Con.File_Carrinho
                         join fts in Con.File_Tarifas on fcr.File_id equals fts.File_id
                         join rgn in Con.Ranges on fts.File_Tarifas_id equals rgn.FileTabelaId
                         where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                               fts.Range_id != null &&
                               rgn.Flag.Equals("hotel") &&
                               rgn.OptQuote == OptQuote
                         select new QuoteIdsDelModel
                         {
                             idRange = rgn.Ranges_id,
                             idTabela = (int)rgn.FileTabelaId,
                             flag = rgn.Flag,
                             dtFrom = (DateTime)fts.Data_From,
                             dtTo = (DateTime)fts.Data_To,
                             Ordem = (int)fts.Ordem
                         }).Distinct().ToList())
                        .Union
                        ((from fcr in Con.File_Carrinho
                          join fts in Con.File_Transfers on fcr.File_id equals fts.File_id
                          join rgn in Con.Ranges on fts.File_Transf_id equals rgn.FileTabelaId
                          where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                                fts.Range_id != null &&
                               rgn.Flag.Equals("servico") &&
                               rgn.OptQuote == OptQuote
                          select new QuoteIdsDelModel
                          {
                              idRange = rgn.Ranges_id,
                              idTabela = (int)rgn.FileTabelaId,
                              flag = rgn.Flag,
                              dtFrom = (DateTime)fts.Data_From,
                              dtTo = (DateTime)fts.Data_To,
                              Ordem = (int)fts.Ordem
                          }).Distinct().ToList())
                          .Union
                          ((from fcr in Con.File_Carrinho
                            join fse in Con.File_ServExtra on fcr.File_id equals fse.File_id
                            join rgn in Con.Ranges on fse.File_ServExtra_id equals rgn.FileTabelaId
                            where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                                  fse.Range_id != null &&
                               rgn.Flag.Equals("extra") &&
                               rgn.OptQuote == OptQuote
                            select new QuoteIdsDelModel
                            {
                                idRange = rgn.Ranges_id,
                                idTabela = (int)rgn.FileTabelaId,
                                flag = rgn.Flag,
                                dtFrom = (DateTime)fse.Data_From,
                                dtTo = (DateTime)fse.Data_To,
                                Ordem = (int)fse.Ordem
                            }).Distinct().ToList()).OrderBy(a => a.dtFrom).ThenBy(a => a.Ordem).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<TarifasFileModel> ListarFiles_Supps(int IdFile)
        {
            try
            {
                return ((from qta in Con.Quote_Tarifas
                         where qta.File_id == IdFile
                         select new TarifasFileModel
                         {
                             Flag = "hotel",
                             NomeSupp = qta.S_nome,
                             idSupp = (int)qta.S_id
                         }).Distinct().ToList()).Union
                        ((from qtt in Con.Quote_Transf
                          where qtt.File_id == IdFile &&
                                qtt.SubServico == null
                          select new TarifasFileModel
                          {
                              Flag = "servico",
                              NomeSupp = qtt.S_nome,
                              idSupp = (int)qtt.S_id
                          }).Distinct().ToList()).Union
                         ((from qse in Con.Quote_ServExtra
                           where qse.File_id == IdFile
                           select new TarifasFileModel
                           {
                               Flag = "extra",
                               NomeSupp = qse.S_nome,
                               idSupp = (int)qse.S_id
                           }).Distinct().ToList()).ToList();
            }
            catch
            {
                throw;
            }
        }


        public List<GridQuoteModel> MontaCarrinhoNewFile_Model(int IdQuotGrupo, int OptQuote)
        {
            try
            {
                return ((from fcr in Con.File_Carrinho
                         join fts in Con.File_Tarifas on fcr.File_id equals fts.File_id
                         join rgn in Con.Ranges on fts.File_Tarifas_id equals rgn.FileTabelaId
                         where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                               fts.Range_id != null &&
                               rgn.Flag.Equals("hotel") &&
                               rgn.OptQuote == OptQuote
                         select new GridQuoteModel
                         {
                             Plataforma_id = fts.Plataforma_id == null ? 0 : (int)fts.Plataforma_id,
                             Plataforma = fts.Plataforma != null ? fts.Plataforma : "",
                             IdRetorna = fts.File_Tarifas_id,
                             dtFrom = fts.Data_From,
                             dtTo = fts.Data_To,
                             Nome = fts.S_nome,
                             NomeTr = fts.S_nome,
                             NomeSupp = fts.S_nome,
                             Room = fts.Room,
                             Categoria = fts.Categoria,
                             Flag = rgn.Flag,
                             MealNome = fts.S_meal_nome,
                             MealStatus = fts.S_meal_status,
                             Status = fts.Status,
                             DAY = rgn.DAY,
                             Bulk = fts.TBulk,
                             Optional = fts.TOptional,
                             SubServico = false,
                             Paying_Pax = fts.Paying_Pax,
                             Ordem = fts.Ordem,
                             NomePacote = fts.NomePacote,
                             Flight_id = fts.Flights_id == null ? 0 : (int)fts.Flights_id,
                             idCidade = 0,
                             remarkservico = null,
                             Provisorio = fts.Provisorio,
                             S_id = fts.S_id == null ? 0 : (int)fts.S_id
                         }).Distinct().ToList())
                        .Union
                        ((from fcr in Con.File_Carrinho
                          join fts in Con.File_Transfers on fcr.File_id equals fts.File_id
                          join rgn in Con.Ranges on fts.File_Transf_id equals rgn.FileTabelaId
                          where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                                fts.Range_id != null &&
                               rgn.Flag.Equals("servico") &&
                               rgn.OptQuote == OptQuote
                          select new GridQuoteModel
                          {
                              Plataforma_id = 0,
                              Plataforma = "",
                              IdRetorna = fts.File_Transf_id,
                              dtFrom = fts.Data_From,
                              dtTo = fts.Data_To,
                              Nome = fts.Transf_nome,
                              NomeTr = fts.Transf_nome,
                              NomeSupp = fts.S_nome,
                              Room = "",
                              Categoria = "",
                              Flag = rgn.Flag,
                              MealNome = "",
                              MealStatus = "",
                              Status = fts.Status,
                              DAY = rgn.DAY,
                              Bulk = fts.TBulk,
                              Optional = fts.TOptional,
                              SubServico = fts.SubServico == true ? true : false,
                              Paying_Pax = fts.Paying_Pax,
                              Ordem = fts.Ordem,
                              NomePacote = "",
                              Flight_id = fts.Flights_id == null ? 0 : (int)fts.Flights_id,
                              idCidade = fts.Trf_CID_id == null ? 0 : (int)fts.Trf_CID_id,
                              Provisorio = false,
                              S_id = fts.S_id == null ? 0 : (int)fts.S_id
                          }).Distinct().ToList())
                          .Union
                          ((from fcr in Con.File_Carrinho
                            join fse in Con.File_ServExtra on fcr.File_id equals fse.File_id
                            join rgn in Con.Ranges on fse.File_ServExtra_id equals rgn.FileTabelaId
                            where fcr.Quotation_Grupo_Id == IdQuotGrupo &&
                                  fse.Range_id != null &&
                               rgn.Flag.Equals("extra") &&
                               rgn.OptQuote == OptQuote
                            select new GridQuoteModel
                            {
                                Plataforma_id = 0,
                                Plataforma = "",
                                IdRetorna = fse.File_ServExtra_id,
                                dtFrom = fse.Data_From,
                                dtTo = fse.Data_To,
                                Nome = fse.File_ServExtra_nome,
                                NomeTr = "",
                                NomeSupp = fse.S_nome,
                                Room = "",
                                Categoria = "",
                                Flag = rgn.Flag,
                                MealNome = "",
                                MealStatus = "",
                                Status = fse.Status,
                                DAY = rgn.DAY,
                                Bulk = fse.TBulk,
                                Optional = fse.TOptional,
                                SubServico = false,
                                Paying_Pax = fse.Paying_Pax,
                                Ordem = fse.Ordem,
                                NomePacote = "",
                                Flight_id = fse.Flights_id == null ? 0 : (int)fse.Flights_id,
                                idCidade = fse.Ext_CID_id == null ? 0 : (int)fse.Ext_CID_id,
                                remarkservico = null,
                                Provisorio = false,
                                S_id = fse.S_id == null ? 0 : (int)fse.S_id
                            }).Distinct().ToList()).OrderBy(a => a.dtFrom).ThenBy(a => a.Ordem).ThenBy(a => a.dtTo).ToList();

            }
            catch
            {
                throw;
            }
        }

        public class ReordenarQuoteModel
        {
            public int ID { get; set; }
            public int ordem { get; set; }
            public string flag { get; set; }
        }

        public bool VerificaOrdemRepetida(int IdFile, int opt)
        {
            try
            {
                var query = ((from qse in Con.File_Tarifas
                              where qse.File_id == IdFile &&
                                    qse.OptQuote == opt
                              select new ReordenarQuoteModel
                              {
                                  ID = qse.File_Tarifas_id,
                                  flag = "hotel",
                                  ordem = (int)qse.Ordem
                              }).Union(
                        from qts in Con.File_Transfers
                        where qts.File_id == IdFile &&
                               qts.OptQuote == opt
                        select new ReordenarQuoteModel
                        {
                            ID = qts.File_Transf_id,
                            flag = "servico",
                            ordem = (int)qts.Ordem
                        }).Union(
                        from qtr in Con.File_ServExtra
                        where qtr.File_id == IdFile &&
                               qtr.OptQuote == opt
                        select new ReordenarQuoteModel
                        {
                            ID = qtr.File_ServExtra_id,
                            flag = "extra",
                            ordem = (int)qtr.Ordem
                        })).OrderBy(s => s.ordem).ToList();               
              

                foreach (var item in query)
                {
                    if(query.Where(s => s.ordem == item.ordem).Count() > 1)
                    {
                        return false;
                    }
                }

                return true;

            }
            catch
            {
                throw;
            }
        }

        public List<ReordenarQuoteModel> ReordenarQuote(int IdFile, int opt)
        {
            try
            {
                return ((from qse in Con.File_Tarifas
                         where qse.File_id == IdFile &&
                               qse.OptQuote == opt
                         select new ReordenarQuoteModel
                         {
                             ID = qse.File_Tarifas_id,
                             flag = "hotel",
                             ordem = (int)qse.Ordem
                         }).Union(
                        from qts in Con.File_Transfers
                        where qts.File_id == IdFile &&
                               qts.OptQuote == opt
                        select new ReordenarQuoteModel
                        {
                            ID = qts.File_Transf_id,
                            flag = "servico",
                            ordem = (int)qts.Ordem
                        }).Union(
                        from qtr in Con.File_ServExtra
                        where qtr.File_id == IdFile &&
                               qtr.OptQuote == opt
                        select new ReordenarQuoteModel
                        {
                            ID = qtr.File_ServExtra_id,
                            flag = "extra",
                            ordem = (int)qtr.Ordem
                        })).OrderBy(s => s.ordem).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<ReordenarQuoteModel> ReordenarQuote_ordemclique(int IdFile, int opt, int ordemclique)
        {
            try
            {
                return ((from qse in Con.File_Tarifas
                         where qse.File_id == IdFile &&
                               qse.OptQuote == opt &&
                               qse.Ordem > ordemclique &&
                               qse.Ordem != 999999
                         select new ReordenarQuoteModel
                         {
                             ID = qse.File_Tarifas_id,
                             flag = "hotel",
                             ordem = (int)qse.Ordem
                         }).Union(
                         from qts in Con.File_Transfers
                         where qts.File_id == IdFile &&
                                qts.OptQuote == opt &&
                                qts.Ordem > ordemclique &&
                                qts.Ordem != 999999
                         select new ReordenarQuoteModel
                         {
                             ID = qts.File_Transf_id,
                             flag = "servico",
                             ordem = (int)qts.Ordem
                         }).Union(
                         from qtr in Con.File_ServExtra
                         where qtr.File_id == IdFile &&
                                qtr.OptQuote == opt &&
                                qtr.Ordem > ordemclique &&
                                qtr.Ordem != 999999
                         select new ReordenarQuoteModel
                         {
                             ID = qtr.File_ServExtra_id,
                             flag = "extra",
                             ordem = (int)qtr.Ordem
                         })).OrderBy(s => s.ordem).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<ReordenarQuoteModel> ReordenarQuote_ordemtarget(int IdFile, int opt, int ordemtarget)
        {
            try
            {
                return ((from qse in Con.File_Tarifas
                         where qse.File_id == IdFile &&
                               qse.OptQuote == opt &&
                               qse.Ordem >= ordemtarget &&
                               qse.Ordem != 999999
                         select new ReordenarQuoteModel
                         {
                             ID = qse.File_Tarifas_id,
                             flag = "hotel",
                             ordem = (int)qse.Ordem
                         }).Union(
                         from qts in Con.File_Transfers
                         where qts.File_id == IdFile &&
                                qts.OptQuote == opt &&
                                qts.Ordem >= ordemtarget &&
                                qts.Ordem != 999999
                         select new ReordenarQuoteModel
                         {
                             ID = qts.File_Transf_id,
                             flag = "servico",
                             ordem = (int)qts.Ordem
                         }).Union(
                         from qtr in Con.File_ServExtra
                         where qtr.File_id == IdFile &&
                                qtr.OptQuote == opt &&
                                qtr.Ordem >= ordemtarget &&
                                qtr.Ordem != 999999
                         select new ReordenarQuoteModel
                         {
                             ID = qtr.File_ServExtra_id,
                             flag = "extra",
                             ordem = (int)qtr.Ordem
                         })).OrderBy(s => s.ordem).ToList();

            }
            catch
            {
                throw;
            }
        }

    }
}
