﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class ClassificacaoLodgeDAL
    {
        private Model Con;

        public ClassificacaoLodgeDAL()
        {
            Con = new Model();
        }

        public void Salvar(ClassificacaoLodge c)
        {
            try
            {

                Con.ClassificacaoLodge.Add(c);
                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public ClassificacaoLodge ObterPorId(int ClassificacaoLodge_id)
        {
            try
            {
                return Con.ClassificacaoLodge.Where(c => c.ClassificacaoLodge_id == ClassificacaoLodge_id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(ClassificacaoLodge novo)
        {
            try
            {
                ClassificacaoLodge antigo = ObterPorId(novo.ClassificacaoLodge_id);

                antigo.ClassificacaoLodge_nome = novo.ClassificacaoLodge_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(ClassificacaoLodge c)
        {
            try
            {
                Con.ClassificacaoLodge.Remove(c);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<ClassificacaoLodge> ListarTodos()
        {
            try
            {                
                return Con.ClassificacaoLodge.ToList();
            }
            catch
            {
                throw;
            }
        }


    }
}
