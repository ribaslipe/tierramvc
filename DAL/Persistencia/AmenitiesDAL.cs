﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class AmenitiesDAL
    {
        
        private Model Con;

        public AmenitiesDAL()
        {
            Con = new Model();
        }

        public S_Amenities ObterPorID(int ID)
        {
            try
            {
                return Con.S_Amenities.Where(a => a.S_Amenities_id == ID).SingleOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Excluir(S_Amenities s)
        {
            try
            {
                Con.S_Amenities.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Amenities ObterPorNome(string S_Amenities_Nome)
        {
            try
            {
                return Con.S_Amenities.Where(a => a.S_Amenities_nome == S_Amenities_Nome).SingleOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<S_Amenities> ListarTodos()
        {
            try
            {
                return Con.S_Amenities.OrderBy(a => a.S_Amenities_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Amenities> ListarTodos(string nome)
        {
            try
            {
                return Con.S_Amenities.Where(s => s.S_Amenities_nome.Contains(nome)).OrderBy(a => a.S_Amenities_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosOj()
        {
            try
            {
                return (from amt in Con.S_Amenities
                        join agp in Con.S_GroupAmenities on amt.S_GroupAmenities_ID equals agp.S_GroupAmenities_ID
                        orderby amt.S_Amenities_nome, agp.S_GroupAmenities_Name
                        select new
                        {
                            amt.S_Amenities_id,
                            amt.S_Amenities_nome,
                            Group = agp.S_GroupAmenities_Name,
                            amt.S_Amenities_imgNome
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Amenities> ListarPorGrupo(int GroupID)
        {
            try
            {
                return Con.S_Amenities.Where(a => a.S_GroupAmenities_ID == GroupID).OrderBy(b => b.S_Amenities_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Amenities> ListarPorSupplier(int SupplierID)
        {
            try
            {
                var SupplierAmenities = Con.S_SupplierAmenities.Where(a => a.S_SupplierAmenities_Supplier_ID == SupplierID).ToList();

                List<S_Amenities> lstAmenities = new List<S_Amenities>();

                foreach (var sup_ame in SupplierAmenities)
                    lstAmenities.Add(Con.S_Amenities.Where(a => a.S_Amenities_id == sup_ame.S_SupplierAmenities_Amenities_ID).Single());

                return lstAmenities;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<int> ListarPorSupplier_SAmenities(int SupplierID)
        {
            try
            {
                return Con.S_SupplierAmenities.Where(a => a.S_SupplierAmenities_Supplier_ID == SupplierID).Select(s => (int)s.S_SupplierAmenities_Amenities_ID).ToList();
             
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Salvar(S_Amenities s)
        {
            try
            {
                Con.S_Amenities.Add(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void SalvarAppAmenities(S_SupplierAmenities s)
        {
            try
            {
                Con.S_SupplierAmenities.Add(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public S_SupplierAmenities ObterPorId_Spp(int idSpp, int idAment)
        {
            try
            {
                return Con.S_SupplierAmenities.Where(s => s.S_SupplierAmenities_Supplier_ID == idSpp &&
                                                          s.S_SupplierAmenities_Amenities_ID == idAment).SingleOrDefault();
            }
            catch 
            {
                throw;
            }
        }

        public void ExcluirSppAmenities(S_SupplierAmenities ssa)
        {
            try
            {
                Con.S_SupplierAmenities.Remove(ssa);
                Con.SaveChanges();
            }
            catch 
            {
                throw;
            }
        }

    }
}
