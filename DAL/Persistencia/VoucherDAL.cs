﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class VoucherDAL
    {
        private Model Con;

        public VoucherDAL()
        {
            Con = new Model();
        }

        public Voucher ObterPorQuotationGrupoId(int qgId, int optQuote)
        {
            return Con.Voucher.Where(a => a.QuotationGrupoId == qgId && a.optQuote == optQuote).SingleOrDefault();
        }

        public int AddVoucher(Voucher v)
        {
            try
            {
                Con.Voucher.Add(v);
                Con.SaveChanges();
                return v.Id;
            }
            catch (Exception ex)
            {
                System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
                return 0;
            }

        }

        public void AtualizaVoucher(Voucher v)
        {
            var old = Con.Voucher.Where(a => a.Id == v.Id).Single();
            old.AtYourDiscretionEdited = v.AtYourDiscretionEdited;
            old.DataInOut = v.DataInOut;
            old.ImportantRecommendationsEdited = v.ImportantRecommendationsEdited;
            old.IncludedServicesEdited = v.IncludedServicesEdited;
            old.NbrChildren = v.NbrChildren;
            old.NbrInfants = v.NbrInfants;
            old.NbrTravellers = v.NbrTravellers;
            old.SecurityCheck = v.SecurityCheck;
            old.TourCode = v.TourCode;
            old.TourName = v.TourName;
            old.Travellers = v.Travellers;

            Con.SaveChanges();
        }

        public void AtualizaTripEdited(int voucherId)
        {
            var old = Con.Voucher.First(a => a.Id == voucherId);
            old.TripEdited = true;
            Con.SaveChanges();
        }

        public void DeleteVoucher(int voucherId)
        {
            var voucher = Con.Voucher.FirstOrDefault(a => a.Id == voucherId);

            if ((voucher == null) || (voucher.Id == 0))
                return;

            var lvayd = Con.Voucher_AtYourDiscretion.Where(a => a.VoucherId == voucher.Id).ToList();

            foreach (Voucher_AtYourDiscretion vayd in lvayd)
                Con.Voucher_AtYourDiscretion.Remove(vayd);
            lvayd = null;

            var lvis = Con.Voucher_IncludedServices.Where(a => a.VoucherId == voucher.Id).ToList();

            foreach (Voucher_IncludedServices vis in lvis)
                Con.Voucher_IncludedServices.Remove(vis);
            lvis = null;

            var lvr = Con.Voucher_Recommendations.Where(a => a.VoucherId == voucher.Id).ToList();

            foreach (Voucher_Recommendations vr in lvr)
                Con.Voucher_Recommendations.Remove(vr);

            var lvt = Con.Voucher_Trip.Where(a => a.VoucherId == voucher.Id).ToList();

            foreach (Voucher_Trip vt in lvt)
                Con.Voucher_Trip.Remove(vt);

            Con.Voucher.Remove(voucher);

            Con.SaveChanges();
        }
    }
}