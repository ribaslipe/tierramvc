﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class GrupoQtdInfNomeDAL
    {

        private Model Con;

        public GrupoQtdInfNomeDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quotation_Grupo_Qtd_Baby_Nome q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Baby_Nome.Add(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Baby_Nome ObterPorId(int IdINFNome)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Baby_Nome.Where(q => q.Qtd_Baby_Nome_id == IdINFNome).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Quotation_Grupo_Qtd_Baby_Nome novo)
        {
            try
            {
                Quotation_Grupo_Qtd_Baby_Nome antigo = ObterPorId(novo.Qtd_Baby_Nome_id);

                antigo.Nome = novo.Nome;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Quotation_Grupo_Qtd_Baby_Nome q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Baby_Nome.Remove(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Grupo_Qtd_Baby_Nome> VerificaExiste(int IdQuotGrupo)
        {
            try
            {

                return (from qga in Con.Quotation_Grupo_Qtd_Baby_Nome
                        join qgq in Con.Quotation_Grupo_Qtd_Baby on qga.Qtd_Baby_id equals qgq.Qtd_Baby_id
                        where qgq.Quotation_Grupo_Id == IdQuotGrupo
                        select qga).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Grupo_Qtd_Baby_Nome> ListarPorInfs(int IdQtdInfs)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Baby_Nome.Where(q => q.Qtd_Baby_id == IdQtdInfs).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
