﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;


namespace DAL.Persistencia
{
    public class PeriodoBlackDAL
    {

        private Model Con = new Model();


        //Método para gravar um registro na tabela S_Mercado_Periodo_Black
        public void Salvar(S_Mercado_Periodo_Black c)
        {
            try
            {
                //Provedor de Conexão do EntityFramework
                //NewTierraEntities Con = new NewTierraEntities();

                //Rotinas prontas para o banco de dados
                Con.S_Mercado_Periodo_Black.Add(c); //insert into S_Mercado_Periodo_Black values...
                Con.SaveChanges(); //Executar a operação

            }
            catch
            {
                throw;
            }
        }

        //Método para Excluir o S_Mercado_Periodo_Black na base
        public void Excluir(S_Mercado_Periodo_Black c)
        {
            try
            {
                Con.S_Mercado_Periodo_Black.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        //Metodo para listar todos os S_Mercado_Periodo_Black cadastrados
        public List<S_Mercado_Periodo_Black> ListarTodos()
        {
            try
            {
                //SQL -> select * from S_Mercado_Periodo_Black
                return Con.S_Mercado_Periodo_Black.ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(Int32 SupplierID, Int32 MercadoID, Int32 BaseTarifariaID)
        {
            try
            {
                return Con.S_Mercado_Periodo_Black.Where(
                        c => c.S_id == SupplierID && c.Mercado_id == MercadoID && c.BaseTarifaria_id == BaseTarifariaID)
                        .OrderByDescending(p => p.S_merc_periodo_black_from).ToList();
            }
            catch
            {
                
                throw;
            }
        }

        public List<S_Mercado_Periodo_Black> ListarTodos_model(int SupplierID, int MercadoID, int BaseTarifariaID)
        {
            try
            {
                return Con.S_Mercado_Periodo_Black.Where(
                        c => c.S_id == SupplierID && c.Mercado_id == MercadoID && c.BaseTarifaria_id == BaseTarifariaID)
                        .OrderByDescending(p => p.S_merc_periodo_black_from).ToList();
            }
            catch
            {

                throw;
            }
        }


        //Método para obter 1 S_Mercado_Periodo pelo Id
        public S_Mercado_Periodo_Black ObterPorId(int IdMercadoPeriodoBlack)
        {
            try
            {
                //select * from TbCliente where IdCliente = ?
                return Con.S_Mercado_Periodo_Black.Where(c => c.S_merc_periodo_black_id == IdMercadoPeriodoBlack).Single();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        public Int32 VerificaTarifa(string from, string to, int Sid) //Sobrecarga de Métodos
        {
            try
            {
                //select * from TbProduto where Nome like ?
                //int cons1 = Con.S_Mercado_Periodo_Black.Where(p => p.S_merc_periodo_from.Equals(from) || p.S_merc_periodo_to.Equals(to) || p.S_id.Equals(Sid)).Count();

                Int32 cons1 = Con.S_Mercado_Periodo_Black.Where(p => p.S_id.Equals(Sid)).Count();
                Int32 cons2 = Con.S_Mercado_Periodo_Black.Where(p => p.S_merc_periodo_black_from.Equals(from)).Count();


                if (cons1 > 0)
                {

                }
                return cons1;

                //Expressões LAMBDA
                /*
                 * StartsWith   -> Começa com
                 * EndsWith     -> Termina com
                 * Contains     -> Contendo
                 * Equals       -> Exatamente igual
                 */
            }
            catch
            {
                throw; //Lançar a exceção adiante...
            }
        }

        //Método para atualizar os dados do S_Mercado_Periodo
        public void Atualizar(S_Mercado_Periodo_Black registroNovo)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                S_Mercado_Periodo_Black registroAntigo = ObterPorId(registroNovo.S_merc_periodo_black_id);

                //Atualizando os dados
                registroAntigo.S_merc_periodo_black_from = registroNovo.S_merc_periodo_black_from;
                registroAntigo.S_merc_periodo_black_to = registroNovo.S_merc_periodo_black_to;
                registroAntigo.S_merc_periodo_black_obs = registroNovo.S_merc_periodo_black_obs;
                registroAntigo.S_id = registroNovo.S_id;
                registroAntigo.Mercado_id = registroNovo.Mercado_id;

                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
}
