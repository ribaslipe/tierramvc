﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class QuotationGrupoDAL
    {

        private Model Con;

        public QuotationGrupoDAL()
        {
            Con = new Model();
        }

        public List<Quotation_Grupo> ListarTodos(int IdQuotation)
        {
            try
            {
                return Con.Quotation_Grupo.Where(q => q.Quotation_Id == IdQuotation).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public void Salvar(Quotation_Grupo q)
        {
            try
            {
                Con.Quotation_Grupo.Add(q);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo ObterPorId(int IdQGrupo)
        {
            try
            {
                return Con.Quotation_Grupo.Where(q => q.Quotation_Grupo_Id == IdQGrupo).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo ObterPorIdQuotation(int IdQuotation)
        {
            try
            {
                return Con.Quotation_Grupo.Where(q => q.Quotation_Id == IdQuotation).First();
            }
            catch
            {
                throw;
            }
        }

        public Quotation_Grupo ObterPorIdQuotationLast(int IdQuotation)
        {
            try
            {
                return Con.Quotation_Grupo.Where(q => q.Quotation_Id == IdQuotation).AsEnumerable().LastOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public bool IfExists(int IdQuotation)
        {
            try
            {
                return Con.Quotation_Grupo.Any(q => q.Quotation_Id == IdQuotation);
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo ObterPorNome(string NomeGrupo)
        {
            try
            {
                return Con.Quotation_Grupo.Where(q => q.Pax_Group_Name.Equals(NomeGrupo)).First();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaSeries(int IdQuotation)
        {
            try
            {
                return Con.Quotation_Grupo.Where(q => q.Quotation_Id == IdQuotation &&
                                                      q.S_mercado_tipo_tarifa_id == 4).Count() != 0;
            }
            catch 
            {                
                throw;
            }
        }

        public void AtualizarNGrupo(Quotation_Grupo novo)
        {
            try
            {
                Quotation_Grupo antigo = ObterPorId(novo.Quotation_Grupo_Id);

                antigo.Quotation_Grupo_NGrupo = novo.Quotation_Grupo_NGrupo;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(Quotation_Grupo novo)
        {
            try
            {
                Quotation_Grupo antigo = ObterPorId(novo.Quotation_Grupo_Id);

                antigo.Cliente_id               = novo.Cliente_id;
                antigo.S_mercado_tipo_tarifa_id = novo.S_mercado_tipo_tarifa_id;
                antigo.US_id                    = novo.US_id;
                antigo.FLinguagem_id            = novo.FLinguagem_id;
                antigo.Moeda_id                 = novo.Moeda_id;
                antigo.Quotation_Grupo_NGrupo   = novo.Quotation_Grupo_NGrupo;
                antigo.Pax_Group_Name           = novo.Pax_Group_Name;
                antigo.dataQuote                = novo.dataQuote;
                antigo.dataIN                   = novo.dataIN;
                antigo.dateOut                  = novo.dateOut;
                antigo.tourCode                 = novo.tourCode;
                antigo.surname                  = novo.surname;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public int RetornaIdQuote_Grupo(string code)
        {
            try
            {

                return (from qto in Con.Quotation
                        join qtg in Con.Quotation_Grupo on qto.Quotation_Id equals qtg.Quotation_Id
                        where qto.Quotation_Code.Equals(code)
                        select qtg).SingleOrDefault().Quotation_Grupo_Id;

            }
            catch 
            {                
                throw;
            }
        }

        public void AtualizarStatus(Quotation_Grupo novo)
        {
            try
            {
                Quotation_Grupo antigo = ObterPorId(novo.Quotation_Grupo_Id);

                antigo.Quotation_status_id = novo.Quotation_status_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarcancelAll(Quotation_Grupo novo)
        {
            try
            {
                Quotation_Grupo antigo = ObterPorId(novo.Quotation_Grupo_Id);

                antigo.cancelAll = novo.cancelAll;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<string> ListarPaxsGroup(int idUsuario, int idCliente, string prefixText)
        {
            try
            {

                List<string> s = new List<string>();

                if (idUsuario > 0 && idCliente > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = (from qt in Con.Quotation_Grupo
                             join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                             where qt.Cliente_id == idCliente &&
                                   qt.US_id == idUsuario
                             orderby qn.Quotation_Code
                             select qt.Pax_Group_Name).Distinct().ToList();

                    }
                    else
                    {
                        s = (from qt in Con.Quotation_Grupo
                             join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                             where qt.Pax_Group_Name.Contains(prefixText) &&
                                   qt.Cliente_id == idCliente &&
                                   qt.US_id == idUsuario
                             orderby qn.Quotation_Code
                             select qt.Pax_Group_Name).Distinct().ToList();
                    }
                }
                else if (idUsuario > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = (from qt in Con.Quotation_Grupo
                             join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                             where qt.US_id == idUsuario
                             orderby qn.Quotation_Code
                             select qt.Pax_Group_Name).Distinct().ToList();
                    }
                    else
                    {
                        s = (from qt in Con.Quotation_Grupo
                             join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                             where qt.Pax_Group_Name.Contains(prefixText) &&
                                   qt.US_id == idUsuario
                             orderby qn.Quotation_Code
                             select qt.Pax_Group_Name).Distinct().ToList();
                    }
                }
                else if (idCliente > 0)
                {
                    if (prefixText.Equals("*"))
                    {
                        s = (from qt in Con.Quotation_Grupo
                             join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                             where qt.Cliente_id == idCliente
                             orderby qn.Quotation_Code
                             select qt.Pax_Group_Name).Distinct().ToList();
                    }
                    else
                    {
                        s = (from qt in Con.Quotation_Grupo
                             join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                             where qt.Pax_Group_Name.Contains(prefixText) &&
                                   qt.Cliente_id == idCliente
                             orderby qn.Quotation_Code
                             select qt.Pax_Group_Name).Distinct().ToList();
                    }
                }
                else
                {
                    if (prefixText.Equals("*"))
                    {
                        s = (from qt in Con.Quotation_Grupo
                             join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                             orderby qn.Quotation_Code
                             select qt.Pax_Group_Name).ToList();
                    }
                    else
                    {
                        s = (from qt in Con.Quotation_Grupo
                             join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                             where qt.Pax_Group_Name.Contains(prefixText)
                             orderby qn.Quotation_Code
                             select qt.Pax_Group_Name).Distinct().ToList();
                    }
                }

                if (s.Count == 0)
                {
                    s.Add("Não existe file cadastrado com esses parâmetros.");

                    return s;
                }

                return s;            

            }
            catch 
            {
                throw;
            }
        }

        public string ObterCode_PaxGroup(string PaxGroup)
        {
            try
            {
                //SELECT Quotation.Quotation_Code
                //FROM            Quotation INNER JOIN
                //Quotation_Grupo ON Quotation.Quotation_Id = Quotation_Grupo.Quotation_Id
                //WHERE(Quotation_Grupo.Pax_Group_Name = 'Beautiful Brazil')

                return (from quo in Con.Quotation
                        join qgr in Con.Quotation_Grupo on quo.Quotation_Id equals qgr.Quotation_Id
                        where qgr.Pax_Group_Name.Equals(PaxGroup)
                        select quo).First().Quotation_Code;

            }
            catch 
            {
                throw;
            }
        }

    }
}
