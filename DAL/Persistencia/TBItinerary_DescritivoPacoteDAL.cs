﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class TBItinerary_DescritivoPacoteDAL
    {
        private Model Con;

        public TBItinerary_DescritivoPacoteDAL()
        {
            Con = new Model();
        }       

        public void Salvar(TBItinerary_DescritivoPacote d)
        {
            try
            {
                Con.TBItinerary_DescritivoPacote.Add(d);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public TBItinerary_DescritivoPacote ObterPorId(int IdDescr)
        {      
            try
            {
                return Con.TBItinerary_DescritivoPacote.Where(d => d.Descritivo_id == IdDescr).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public TBItinerary_DescritivoPacote ObterPorIdPeriodo(int IdPeriodo)
        {
            try
            {
                return Con.TBItinerary_DescritivoPacote.Where(d => d.S_merc_periodo_id == IdPeriodo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(TBItinerary_DescritivoPacote d)
        {
            try
            {
                Con.TBItinerary_DescritivoPacote.Remove(d);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(TBItinerary_DescritivoPacote novo)
        {
            try
            {
                TBItinerary_DescritivoPacote antigo = ObterPorId(novo.Descritivo_id);

                antigo.Descritivo_descr0 = novo.Descritivo_descr0;
                antigo.Descritivo_descr1 = novo.Descritivo_descr1;
                antigo.Descritivo_descr2 = novo.Descritivo_descr2;
                antigo.Descritivo_descr3 = novo.Descritivo_descr3;
                antigo.Descritivo_descr4 = novo.Descritivo_descr4;
                antigo.Descritivo_descr5 = novo.Descritivo_descr5;
                antigo.Descritivo_descr6 = novo.Descritivo_descr6;
                antigo.Descritivo_descr7 = novo.Descritivo_descr7;
                antigo.Descritivo_descr8 = novo.Descritivo_descr8;
                antigo.Descritivo_descr9 = novo.Descritivo_descr9;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaPeriodo(int IdPeriodo)
        {
            try
            {
                return Con.TBItinerary_DescritivoPacote.Where(d => d.S_merc_periodo_id == IdPeriodo).Count() != 0;
            }
            catch 
            {                
                throw;
            }
        }

        public TBItinerary_DescritivoPacote ObterPacote(DateTime DataFrom, DateTime DataTo, int IdSupplier, string PacNome)
        {

            try
            {
                return (from smp in Con.S_Mercado_Periodo
                        join dpc in Con.TBItinerary_DescritivoPacote on smp.S_merc_periodo_id equals dpc.S_merc_periodo_id
                        where smp.S_merc_periodo_from <= DataFrom &&
                              smp.S_merc_periodo_to >= DataTo &&
                              smp.S_id == IdSupplier &&
                              smp.Pacote_nome.Equals(PacNome)
                        select dpc).SingleOrDefault();
            }
            catch
            {
                
                throw;
            }

        }
    }
}
