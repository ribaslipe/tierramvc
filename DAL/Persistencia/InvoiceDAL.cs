﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using System.Data;

namespace DAL.Persistencia
{
    public class InvoiceDAL
    {

        private Model Con;

        public InvoiceDAL()
        {
            Con = new Model();
        }

        public void Salvar(Invoice i)
        {
            try
            {
                Con.Invoice.Add(i);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Invoice> listaInvoicesPorQuotationGrupo(int quotationGrupo)
        {
            try
            {
                return Con.Invoice.Where(d => d.Quotation_Grupo_Id == quotationGrupo).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object listaInvoicesPorQuotationGrupo_obj(int quotationGrupo)
        {
            try
            {
                //return Con.Invoice.Where(d => d.Quotation_Grupo_Id == quotationGrupo).ToList();

                return (from inv in Con.Invoice
                        join ipg in Con.Invoice_Pagamento on inv.Invoice_id equals ipg.Invoice_id
                        where inv.Quotation_Grupo_Id == quotationGrupo
                        select new
                        {
                            inv.Invoice_id,
                            inv.DataEmissao,
                            inv.Grand_Total,
                            inv.CaminhoPDF
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public decimal? soma_inv(int idInv)
        {
            try
            {
                return Con.Invoice_Pagamento.Where(i => i.Invoice_id == idInv).ToList().Sum(s => s.Valor);
            }
            catch 
            {                
                throw;
            }
        }

        public Invoice ultimoInvoice(int quotationGroup)
        {
            return Con.Invoice.Where(d => d.Quotation_Grupo_Id == quotationGroup).OrderByDescending(c => c.DataEmissao).FirstOrDefault();
        }

        public decimal? SomaInvoices(int QuotGrupo)
        {
            try
            {
                //return Con.Invoice.Where(i => i.Quotation_Grupo_Id == QuotGrupo).Sum(i => i.Grand_Total).Value;

                return Con.Invoice.Where(i => i.Quotation_Grupo_Id == QuotGrupo).Select(i => i.Grand_Total).DefaultIfEmpty(null).Sum();
            }
            catch
            {
                throw;
            }
        }

        public decimal? SomaInvoices_Cliente(int idCliente, DateTime dtFrom, DateTime dtTo)
        {
            try
            {
                return (from inv in Con.Invoice
                        join qtr in Con.Quotation_Grupo on inv.Quotation_Grupo_Id equals qtr.Quotation_Grupo_Id
                        where qtr.Cliente_id == idCliente &&
                              qtr.dataIN >= dtFrom &&
                              qtr.dataIN <= dtTo
                        select new
                        {
                            total = inv.Grand_Total
                        }).ToList().Sum(s => s.total);
            }
            catch 
            {                
                throw;
            }
        }

        

    }
}
