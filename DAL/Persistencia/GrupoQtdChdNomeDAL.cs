﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class GrupoQtdChdNomeDAL
    {
        private Model Con;

        public GrupoQtdChdNomeDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quotation_Grupo_Qtd_Chd_Nome q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Chd_Nome.Add(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Chd_Nome ObterPorId(int IdCHDNome)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Chd_Nome.Where(q => q.Qtd_Chd_Nome_id == IdCHDNome).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Quotation_Grupo_Qtd_Chd_Nome q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Chd_Nome.Remove(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Quotation_Grupo_Qtd_Chd_Nome novo)
        {
            try
            {
                Quotation_Grupo_Qtd_Chd_Nome antigo = ObterPorId(novo.Qtd_Chd_Nome_id);

                antigo.Nome = novo.Nome;
                antigo.Idade = novo.Idade;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Grupo_Qtd_Chd_Nome> VerificaExiste(int IdQuotGrupo)
        {
            try
            {

                return (from qga in Con.Quotation_Grupo_Qtd_Chd_Nome
                        join qgq in Con.Quotation_Grupo_Qtd_Chd on qga.Qtd_Chd_id equals qgq.Qtd_Chd_id
                        where qgq.Quotation_Grupo_Id == IdQuotGrupo
                        select qga).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Grupo_Qtd_Chd_Nome> ListarPorChds(int IdQtdAChds)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Chd_Nome.Where(q => q.Qtd_Chd_id == IdQtdAChds).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
