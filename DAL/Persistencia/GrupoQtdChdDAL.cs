﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class GrupoQtdChdDAL
    {

        private Model Con;

        public GrupoQtdChdDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quotation_Grupo_Qtd_Chd q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Chd.Add(q);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Chd ObterPorId(int IdGQChd)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Chd.Where(q => q.Qtd_Chd_id == IdGQChd).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Chd ObterPorIdGrupo(int IdGQChd)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Chd.Where(q => q.Quotation_Grupo_Id == IdGQChd).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Quotation_Grupo_Qtd_Chd novo)
        {
            try
            {
                Quotation_Grupo_Qtd_Chd antigo = ObterPorId(novo.Qtd_Chd_id);

                antigo.Qtd = novo.Qtd;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
