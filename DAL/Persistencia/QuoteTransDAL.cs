﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Persistencia;
using DAL.Models;

namespace DAL.Persistencia
{
    public class QuoteTransDAL
    {

        private Model Con;

        public QuoteTransDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quote_Transf q)
        {
            try
            {
                Con.Quote_Transf.Add(q);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Quote_Transf ObterPorId(int IdQt)
        {
            try
            {
                return Con.Quote_Transf.Where(q => q.Quote_Transf_id == IdQt).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public List<Quote_Transf> ObterPorIdTC(int IdRange)
        {
            try
            {
                return Con.Quote_Transf.Where(q => q.Range_id == IdRange).ToList();
            }
            catch
            {
                throw;
            }
        }        

        public void ExcluirPorFile(int idFile)
        {
            try
            {
                var query = Con.Quote_Transf.Where(q => q.File_id == idFile).ToList();

                foreach (Quote_Transf item in query)
                {
                    Con.Quote_Transf.Remove(item);
                    Con.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public void ExcluirLista(List<Quote_Transf> lista)
        {
            try
            {
                foreach (Quote_Transf item in lista)
                {
                    Con.Quote_Transf.Remove(item);
                    Con.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNew(Quote_Transf novo)
        {
            try
            {

                Quote_Transf ftrnew = ObterPorId(novo.Quote_Transf_id);

                ftrnew.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                ftrnew.Transf_vendaNet = novo.Transf_vendaNet;
                ftrnew.Transf_venda = novo.Transf_venda;
                ftrnew.Transf_total = novo.Transf_total;
                ftrnew.Transf_valor = novo.Transf_valor;
                ftrnew.markup = novo.markup;
                ftrnew.markupNet = novo.markupNet;
                ftrnew.desconto = novo.desconto;
                ftrnew.descontoNet = novo.descontoNet;
                ftrnew.Qtd_Transf = novo.Qtd_Transf;
                ftrnew.Paxs = novo.Paxs;
                ftrnew.Paying_Pax = novo.Paying_Pax;
                ftrnew.Supp_Paying_Pax = novo.Supp_Paying_Pax;

                Con.SaveChanges();

            }
            catch 
            {                
                throw;
            }
        }

        public void AtualizarNew_VrNet(Quote_Transf novo)
        {
            try
            {

                Quote_Transf ftrnew = ObterPorId(novo.Quote_Transf_id);

                ftrnew.Transf_vendaNet = novo.Transf_vendaNet;                

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void AtualizarSupplier(Quote_Transf novo)
        {
            try
            {

                Quote_Transf ftrnew = ObterPorId(novo.Quote_Transf_id);

                ftrnew.S_nome = novo.S_nome;
                ftrnew.S_id = novo.S_id;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Quote_Transf q)
        {
            try
            {
                Con.Quote_Transf.Remove(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Quote_Transf ObterTC(int IdFile, int take)
        {
            try
            {
                take = take - 1;

                return Con.Quote_Transf.Where(q => q.File_id == IdFile &&
                                                    q.TC == true)
                                                    .OrderBy(q => q.Data_From)
                                                    .ThenBy(q => q.Data_To)
                                                    .ThenBy(q => q.Quote_Transf_id)
                                                    .Skip(take)
                                                    .Take(1).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Transf> ListarTodos(int IdFile)
        {
            try
            {
                return Con.Quote_Transf.Where(f => f.File_id == IdFile).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Transf> ListarTodos()
        {
            try
            {
                return Con.Quote_Transf.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Transf> ListarTodos(DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                return Con.Quote_Transf.Where(f => f.Data_From >= dataInicio && f.Data_From < dataFim).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Transf> ListarTodos(int IdFile, int IdSupplier)
        {
            try
            {
                return Con.Quote_Transf.Where(f => f.File_id == IdFile &&
                                                   f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Transf> ListarTodosMapaServicos(DateTime dataInicio, DateTime dataFim, int CID_id)
        {
            try
            {
                if (CID_id == 0)
                {
                    return Con.Quote_Transf.Where(f => f.Data_From <= dataInicio && f.Data_To >= dataFim && (f.Status.Equals("OK") || f.Status.Equals("OP"))
                                                        ||
                                                       f.Data_From <= dataFim && f.Data_To >= dataInicio && (f.Status.Equals("OK") || f.Status.Equals("OP"))).ToList();
                }
                else
                {
                    return Con.Quote_Transf.Where(f => f.Data_From <= dataInicio && f.Data_To >= dataFim && f.Trf_CID_id == CID_id && (f.Status.Equals("OK") || f.Status.Equals("OP"))
                                                        ||
                                                       f.Data_From <= dataFim && f.Data_To >= dataInicio && f.Trf_CID_id == CID_id && (f.Status.Equals("OK") || f.Status.Equals("OP"))).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarBulk(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.TBulk = novo.TBulk;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarValores(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.Transf_vendaNet = novo.Transf_vendaNet;
                antigo.Transf_venda = novo.Transf_venda;
                antigo.Transf_total = novo.Transf_total;
                antigo.Transf_valor = novo.Transf_valor;
                antigo.markup = novo.markup;
                antigo.markupNet = novo.markupNet;
                antigo.desconto = novo.desconto;
                antigo.descontoNet = novo.descontoNet;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarValores_Pax(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.Transf_vendaNet = novo.Transf_vendaNet;
                antigo.Transf_venda = novo.Transf_venda;
                antigo.Transf_total = novo.Transf_total;
                antigo.Transf_valor = novo.Transf_valor;
                antigo.markup = novo.markup;
                antigo.markupNet = novo.markupNet;
                antigo.desconto = novo.desconto;
                antigo.descontoNet = novo.descontoNet;
                antigo.Paxs = novo.Paxs;
                antigo.Paying_Pax = novo.Paying_Pax;
                antigo.Supp_Paying_Pax = novo.Supp_Paying_Pax;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }


        public void AtualizarStatus(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.Status = novo.Status;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarFlight(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.Flights_id = novo.Flights_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOrdem(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.Ordem = novo.Ordem;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Transf> ListarTodosQuoteTransf(int IdFile)
        {
            try
            {
                return Con.Quote_Transf.Where(f => f.File_id == IdFile).ToList();
            }

            catch
            {
                throw;
            }
        }

        public void AtualizarDatas(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.Data_From = novo.Data_From;
                antigo.Data_To = novo.Data_To;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarRange_id(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.Range_id = novo.Range_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos_new(int IdFile)
        {
            try
            {

                return (from flt in Con.Quote_Transf
                        where flt.File_id == IdFile
                        select new
                        {
                            flt.S_nome,
                            flt.S_id
                        }).ToList().Distinct();
            }
            catch
            {
                throw;
            }
        }

        public List<NomesGridEmail> ListarTodos_new_(int IdFile)
        {
            try
            {

                return (from flt in Con.Quote_Transf
                        where flt.File_id == IdFile
                        select new NomesGridEmail
                        {
                            S_nome = flt.S_nome,
                            S_id = (int)flt.S_id
                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Transf> ListarTodos(string nome)
        {
            try
            {
                return Con.Quote_Transf.Where(s => s.Transf_nome.Equals(nome)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNome(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.Transf_nome = novo.Transf_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNomeCidade(Quote_Transf novo)
        {
            try
            {
                Quote_Transf antigo = ObterPorId(novo.Quote_Transf_id);

                antigo.Transf_nome = novo.Transf_nome;
                antigo.Trf_CID_id = novo.Trf_CID_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Transf> ListarTodos_Subs(int IdFile)
        {
            try
            {
                return Con.Quote_Transf.Where(f => f.File_id == IdFile &&
                                                   f.SubServico == true).ToList();
            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem_(int IdFile)
        {
            try
            {
                var item = Con.Quote_Transf.Where(f => f != null && f.File_id == IdFile).OrderByDescending(s => s.Ordem).FirstOrDefault();

                if (item != null)
                {
                    return Convert.ToInt32(item.Ordem);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }




    }
}
