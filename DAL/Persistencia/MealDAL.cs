﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;
using DAL.Models;

namespace DAL.Persistencia
{
    public class MealDAL
    {

        private Model Con;

        public MealDAL()
        {
            Con = new Model();
        }

        //NewTierraEntities Con = new NewTierraEntities();

        public void Salvar(S_Mercado_Meal c)
        {
            try
            {

                //Rotinas prontas para o banco de dados
                Con.S_Mercado_Meal.Add(c); //insert into S_Mercado_Meal values...
                Con.SaveChanges(); //Executar a operação

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Mercado_Meal c)
        {
            try
            {
                Con.S_Mercado_Meal.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Meal> ListarTodos()
        {
            try
            {
                //SQL -> select * from S_Mercado_Meal
                return Con.S_Mercado_Meal.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Meal> ListarTodosLista(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Meal.Where(m => m.S_merc_periodo_id == IdPeriodo).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Meal> ListarTodos_SMeal()
        {
            try
            {
                return Con.S_Meal.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Meal> ListarTodos_SMeal(string nome)
        {
            try
            {
                return Con.S_Meal.Where(s => s.S_meal_nome.Contains(nome)).OrderBy(s => s.S_meal_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(Int32 PeriodoID)
        {
            try
            {
                var query = from m in Con.S_Meal
                            join mm in Con.S_Mercado_Meal on m.S_meal_id equals mm.S_meal_id
                            join mts in Con.S_Mercado_Tarifa_Status on mm.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mm.S_merc_periodo_id == PeriodoID
                            orderby m.S_meal_nome ascending
                            select new
                            {
                                mm.S_merc_meal_tarifa_total,
                                mm.S_merc_meal_id,
                                m.S_meal_nome,
                                mm.S_merc_meal_tarifa,
                                lf.S_mercado_tarifa_status_nome,
                                mm.AddFile
                            };
                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<MealsCadastradasPeriodoModel> ListarTodos_model(Int32 PeriodoID)
        {
            try
            {
                var query = from m in Con.S_Meal
                            join mm in Con.S_Mercado_Meal on m.S_meal_id equals mm.S_meal_id
                            join mts in Con.S_Mercado_Tarifa_Status on mm.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mm.S_merc_periodo_id == PeriodoID
                            orderby m.S_meal_nome ascending
                            select new MealsCadastradasPeriodoModel
                            {
                                S_merc_meal_tarifa_total = mm.S_merc_meal_tarifa_total == null ? 0 : (decimal)mm.S_merc_meal_tarifa_total,
                                S_merc_meal_id = mm.S_merc_meal_id,
                                S_meal_nome = m.S_meal_nome,
                                S_merc_meal_tarifa = mm.S_merc_meal_tarifa == null ? 0 : (decimal)mm.S_merc_meal_tarifa,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                AddFile = mm.AddFile != true ? false : true,
                                MealUmaVez = mm.MealUmaVez != true ? false : true
                            };
                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(int PeriodoID, int IdBase)
        {
            try
            {
                var query = from m in Con.S_Meal
                            join mm in Con.S_Mercado_Meal on m.S_meal_id equals mm.S_meal_id
                            join tb in Con.S_Tarifa_Base on mm.S_merc_meal_id equals tb.S_merc_meal_id
                            join mts in Con.S_Mercado_Tarifa_Status on mm.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mm.S_merc_periodo_id == PeriodoID &&
                                  tb.Base_id == IdBase
                            orderby m.S_meal_nome ascending
                            select new
                            {
                                mm.S_merc_meal_tarifa_total,
                                mm.S_merc_meal_id,
                                m.S_meal_nome,
                                mm.S_merc_meal_tarifa,
                                lf.S_mercado_tarifa_status_nome,
                                mm.AddFile
                            };
                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<MealsCadastradasPeriodoModel> ListarTodos_model(int PeriodoID, int IdBase)
        {
            try
            {
                var query = from m in Con.S_Meal
                            join mm in Con.S_Mercado_Meal on m.S_meal_id equals mm.S_meal_id
                            join tb in Con.S_Tarifa_Base on mm.S_merc_meal_id equals tb.S_merc_meal_id
                            join mts in Con.S_Mercado_Tarifa_Status on mm.S_merc_tarif_status_id equals mts.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where mm.S_merc_periodo_id == PeriodoID &&
                                  tb.Base_id == IdBase
                            orderby m.S_meal_nome ascending
                            select new MealsCadastradasPeriodoModel
                            {
                                S_merc_meal_tarifa_total = mm.S_merc_meal_tarifa_total == null ? 0 : (decimal)mm.S_merc_meal_tarifa_total,
                                S_merc_meal_id = mm.S_merc_meal_id,
                                S_meal_nome = m.S_meal_nome,
                                S_merc_meal_tarifa = mm.S_merc_meal_tarifa == null ? 0 : (decimal)mm.S_merc_meal_tarifa,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                AddFile = mm.AddFile != true ? false : true,
                                MealUmaVez = mm.MealUmaVez != true ? false : true
                            };
                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Meal ObterPorId(int IdMercadoMeal)
        {
            try
            {
                //select * from TbCliente where IdCliente = ?
                return Con.S_Mercado_Meal.Where(c => c.S_merc_meal_id == IdMercadoMeal).SingleOrDefault();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMealCadastrado(S_Mercado_Meal c)
        {
            try
            {

                return Con.S_Mercado_Meal.Where(m => m.S_meal_id == c.S_meal_id &&
                                                m.S_merc_tarif_status_id == c.S_merc_tarif_status_id &&
                                                m.S_merc_periodo_id == c.S_merc_periodo_id)
                                                .Count() != 0;

            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(S_Mercado_Meal registroNovo)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                S_Mercado_Meal registroAntigo = ObterPorId(registroNovo.S_merc_meal_id);

                //Atualizando os dados
                registroAntigo.S_meal_id = registroNovo.S_meal_id;
                registroAntigo.S_merc_meal_tarifa = registroNovo.S_merc_meal_tarifa;
                registroAntigo.S_merc_meal_tarifa_total = registroNovo.S_merc_meal_tarifa_total;
                registroAntigo.S_merc_tarif_status_id = registroNovo.S_merc_tarif_status_id;
                registroAntigo.S_merc_periodo_id = registroNovo.S_merc_periodo_id;


                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarAddFile(S_Mercado_Meal registroNovo)
        {
            try
            {
                S_Mercado_Meal registroAntigo = ObterPorId(registroNovo.S_merc_meal_id);

                registroAntigo.AddFile = registroNovo.AddFile;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarAddFileUmaVez(S_Mercado_Meal registroNovo)
        {
            try
            {
                S_Mercado_Meal registroAntigo = ObterPorId(registroNovo.S_merc_meal_id);

                registroAntigo.MealUmaVez = registroNovo.MealUmaVez;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<MealsHotelModel> ListarMealsPorPH(int IdSupplier, DateTime DataFrom, DateTime DataTo, string estacao, string Fds, string nomePacote)
        {
            try
            {
                if (!String.IsNullOrEmpty(nomePacote) && Fds.Equals(""))
                {
                    return (from sml in Con.S_Meal
                            join smm in Con.S_Mercado_Meal on sml.S_meal_id equals smm.S_meal_id
                            join smp in Con.S_Mercado_Periodo on smm.S_merc_periodo_id equals smp.S_merc_periodo_id
                            join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smm.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where smp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from == DataFrom &&
                                  smp.S_merc_periodo_to == DataTo &&
                                  sme.S_mercado_estacao_nome.Equals(estacao) &&
                                  smp.Pacote_nome.Equals(nomePacote)
                            select new MealsHotelModel
                            {
                                S_merc_periodo_from = (DateTime)smp.S_merc_periodo_from,
                                S_merc_periodo_to = (DateTime)smp.S_merc_periodo_to,
                                S_meal_nome = sml.S_meal_nome,
                                S_merc_meal_id = smm.S_merc_meal_id,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                S_merc_meal_tarifa = smm.S_merc_meal_tarifa != null ? (decimal)smm.S_merc_meal_tarifa : 0,
                                S_merc_meal_tarifa_total = smm.S_merc_meal_tarifa_total != null ? (decimal)smm.S_merc_meal_tarifa_total : 0,
                                S_mercado_estacao_nome = sme.S_mercado_estacao_nome,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_nome = mod.Moeda_nome,
                                Moeda_id = mod.Moeda_id,
                                S_id = smp.S_id
                            }).ToList();
                }
                else if (Fds.Equals(""))
                {
                    return (from sml in Con.S_Meal
                            join smm in Con.S_Mercado_Meal on sml.S_meal_id equals smm.S_meal_id
                            join smp in Con.S_Mercado_Periodo on smm.S_merc_periodo_id equals smp.S_merc_periodo_id
                            join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smm.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where smp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from == DataFrom &&
                                  smp.S_merc_periodo_to == DataTo &&
                                  sme.S_mercado_estacao_nome.Equals(estacao)
                            select new MealsHotelModel
                            {
                                S_merc_periodo_from = (DateTime)smp.S_merc_periodo_from,
                                S_merc_periodo_to = (DateTime)smp.S_merc_periodo_to,
                                S_meal_nome = sml.S_meal_nome,
                                S_merc_meal_id = smm.S_merc_meal_id,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                S_merc_meal_tarifa = smm.S_merc_meal_tarifa != null ? (decimal)smm.S_merc_meal_tarifa : 0,
                                S_merc_meal_tarifa_total = smm.S_merc_meal_tarifa_total != null ? (decimal)smm.S_merc_meal_tarifa_total : 0,
                                S_mercado_estacao_nome = sme.S_mercado_estacao_nome,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_nome = mod.Moeda_nome,
                                Moeda_id = mod.Moeda_id,
                                S_id = smp.S_id
                            }).ToList();
                }
                else if (!Fds.Equals("") && !String.IsNullOrEmpty(nomePacote))
                {
                    return (from sml in Con.S_Meal
                            join smm in Con.S_Mercado_Meal on sml.S_meal_id equals smm.S_meal_id
                            join smp in Con.S_Mercado_Periodo on smm.S_merc_periodo_id equals smp.S_merc_periodo_id
                            join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smm.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where smp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from == DataFrom &&
                                  smp.S_merc_periodo_to == DataTo &&
                                  sme.S_mercado_estacao_nome.Equals(estacao) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(Fds) &&
                                  smp.Pacote_nome.Equals(nomePacote)
                            select new MealsHotelModel
                            {
                                S_merc_periodo_from = (DateTime)smp.S_merc_periodo_from,
                                S_merc_periodo_to = (DateTime)smp.S_merc_periodo_to,
                                S_meal_nome = sml.S_meal_nome,
                                S_merc_meal_id = smm.S_merc_meal_id,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                S_merc_meal_tarifa = smm.S_merc_meal_tarifa != null ? (decimal)smm.S_merc_meal_tarifa : 0,
                                S_merc_meal_tarifa_total = smm.S_merc_meal_tarifa_total != null ? (decimal)smm.S_merc_meal_tarifa_total : 0,
                                S_mercado_estacao_nome = sme.S_mercado_estacao_nome,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_nome = mod.Moeda_nome,
                                Moeda_id = mod.Moeda_id,
                                S_id = smp.S_id
                            }).ToList();
                }
                else
                {
                    return (from sml in Con.S_Meal
                            join smm in Con.S_Mercado_Meal on sml.S_meal_id equals smm.S_meal_id
                            join smp in Con.S_Mercado_Periodo on smm.S_merc_periodo_id equals smp.S_merc_periodo_id
                            join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sms in Con.S_Mercado_Tarifa_Status on smm.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where smp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from == DataFrom &&
                                  smp.S_merc_periodo_to == DataTo &&
                                  sme.S_mercado_estacao_nome.Equals(estacao) &&
                                  smp.S_merc_periodo_fdsFrom.Equals(Fds)
                            select new MealsHotelModel
                            {
                                S_merc_periodo_from = (DateTime)smp.S_merc_periodo_from,
                                S_merc_periodo_to = (DateTime)smp.S_merc_periodo_to,
                                S_meal_nome = sml.S_meal_nome,
                                S_merc_meal_id = smm.S_merc_meal_id,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                S_merc_meal_tarifa = smm.S_merc_meal_tarifa != null ? (decimal)smm.S_merc_meal_tarifa : 0,
                                S_merc_meal_tarifa_total = smm.S_merc_meal_tarifa_total != null ? (decimal)smm.S_merc_meal_tarifa_total : 0,
                                S_mercado_estacao_nome = sme.S_mercado_estacao_nome,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_nome = mod.Moeda_nome,
                                Moeda_id = mod.Moeda_id,
                                S_id = smp.S_id
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<MealsHotelModel> ListarMealsPorPH(int IdSupplier, DateTime DataFrom, DateTime DataTo, string estacao, int numPaxs, string nomePacote)
        {
            try
            {

                if (String.IsNullOrEmpty(nomePacote))
                {

                    return (from sml in Con.S_Meal
                            join smm in Con.S_Mercado_Meal on sml.S_meal_id equals smm.S_meal_id
                            join smp in Con.S_Mercado_Periodo on smm.S_merc_periodo_id equals smp.S_merc_periodo_id
                            join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sba in Con.S_Bases on mod.Moeda_id equals sba.Moeda_id
                            join stb in Con.S_Tarifa_Base on new { SID = (int)smm.S_merc_meal_id, BAS = (int)sba.Base_id } equals new { SID = (int)stb.S_merc_meal_id, BAS = (int)stb.Base_id }
                            join sms in Con.S_Mercado_Tarifa_Status on smm.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where smp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from == DataFrom &&
                                  smp.S_merc_periodo_to == DataTo &&
                                  sme.S_mercado_estacao_nome.Equals(estacao) &&
                                  sba.Base_de <= numPaxs &&
                                  sba.Base_ate >= numPaxs
                            select new MealsHotelModel
                            {
                                S_merc_periodo_from = (DateTime)smp.S_merc_periodo_from,
                                S_merc_periodo_to = (DateTime)smp.S_merc_periodo_to,
                                S_meal_nome = sml.S_meal_nome,
                                S_merc_meal_id = smm.S_merc_meal_id,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                S_merc_meal_tarifa = smm.S_merc_meal_tarifa != null ? (decimal)smm.S_merc_meal_tarifa : 0,
                                S_merc_meal_tarifa_total = smm.S_merc_meal_tarifa_total != null ? (decimal)smm.S_merc_meal_tarifa_total : 0,
                                S_mercado_estacao_nome = sme.S_mercado_estacao_nome,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_nome = mod.Moeda_nome,
                                Moeda_id = mod.Moeda_id,
                                S_id = smp.S_id
                            }).ToList();
                }
                else
                {
                    return (from sml in Con.S_Meal
                            join smm in Con.S_Mercado_Meal on sml.S_meal_id equals smm.S_meal_id
                            join smp in Con.S_Mercado_Periodo on smm.S_merc_periodo_id equals smp.S_merc_periodo_id
                            join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sba in Con.S_Bases on mod.Moeda_id equals sba.Moeda_id
                            join stb in Con.S_Tarifa_Base on new { SID = (int)smm.S_merc_meal_id, BAS = (int)sba.Base_id } equals new { SID = (int)stb.S_merc_meal_id, BAS = (int)stb.Base_id }
                            join sms in Con.S_Mercado_Tarifa_Status on smm.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into leftjoin
                            from lf in leftjoin.DefaultIfEmpty()
                            where smp.S_id == IdSupplier &&
                                  smp.S_merc_periodo_from == DataFrom &&
                                  smp.S_merc_periodo_to == DataTo &&
                                  sme.S_mercado_estacao_nome.Equals(estacao) &&
                                  smp.Pacote_nome.Equals(nomePacote) &&
                                  sba.Base_de <= numPaxs &&
                                  sba.Base_ate >= numPaxs
                            select new MealsHotelModel
                            {
                                S_merc_periodo_from = (DateTime)smp.S_merc_periodo_from,
                                S_merc_periodo_to = (DateTime)smp.S_merc_periodo_to,
                                S_meal_nome = sml.S_meal_nome,
                                S_merc_meal_id = smm.S_merc_meal_id,
                                S_mercado_tarifa_status_nome = lf.S_mercado_tarifa_status_nome,
                                S_merc_meal_tarifa = smm.S_merc_meal_tarifa != null ? (decimal)smm.S_merc_meal_tarifa : 0,
                                S_merc_meal_tarifa_total = smm.S_merc_meal_tarifa_total != null ? (decimal)smm.S_merc_meal_tarifa_total : 0,
                                S_mercado_estacao_nome = sme.S_mercado_estacao_nome,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_nome = mod.Moeda_nome,
                                Moeda_id = mod.Moeda_id,
                                S_id = smp.S_id
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<S_Mercado_Meal> ListarAddFile(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Meal.Where(m => m.S_merc_periodo_id == IdPeriodo && m.AddFile == true).ToList();
            }
            catch
            {
                throw;
            }
        }

        public S_Meal ObterPorNome(string meal)
        {
            try
            {
                return Con.S_Meal.Where(s => s.S_meal_nome.Equals(meal)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_Meal ObterPrimeiro()
        {
            try
            {
                return Con.S_Meal.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        
        public List<S_Mercado_Meal> ListarMeals_PeriodoSpp_AddFile(int IdSupplier, DateTime DataFrom, DateTime DataTo)
        {
            try
            {

                #region query                
                //SELECT S_Mercado_Periodo.S_merc_periodo_id, S_Mercado_Periodo.S_merc_periodo_from, S_Mercado_Periodo.S_merc_periodo_to, S_Mercado_Periodo.Pacote_nome, 
                //S_Mercado_Meal.S_merc_meal_tarifa, 
                //S_Mercado_Meal.S_merc_tarif_status_id, S_Mercado_Meal.S_merc_meal_tarifa_total, S_Mercado_Meal.AddFile, S_Mercado_Meal.S_meal_id
                //FROM            S_Mercado_Periodo INNER JOIN
                //S_Mercado_Meal ON S_Mercado_Periodo.S_merc_periodo_id = S_Mercado_Meal.S_merc_periodo_id
                //WHERE(S_Mercado_Periodo.S_merc_periodo_from <= '20180115') AND(S_Mercado_Periodo.S_merc_periodo_to >= '20180116') AND(S_Mercado_Periodo.S_id = 43) AND(S_Mercado_Meal.AddFile = 'True') OR
                //(S_Mercado_Periodo.S_merc_periodo_from <= '20180116') AND(S_Mercado_Periodo.S_merc_periodo_to >= '20180115') AND(S_Mercado_Periodo.S_id = 43) AND(S_Mercado_Meal.AddFile = 'True')
                #endregion

                return (from smp in Con.S_Mercado_Periodo
                        join smm in Con.S_Mercado_Meal on smp.S_merc_periodo_id equals smm.S_merc_periodo_id
                        where smp.S_merc_periodo_from <= DataFrom
                           && smp.S_merc_periodo_to >= DataTo
                           && (smp.S_id == IdSupplier)
                           && (smm.AddFile == true)
                           ||
                           smp.S_merc_periodo_from <= DataTo
                           && smp.S_merc_periodo_to >= DataFrom
                           && (smp.S_id == IdSupplier)
                           && (smm.AddFile == true)
                        select smm).ToList();

                        //{
                        //    S_merc_periodo_id = smp.S_merc_periodo_id,
                        //    S_merc_periodo_from = smp.S_merc_periodo_from,
                        //    S_merc_periodo_to = smp.S_merc_periodo_to,
                        //    Pacote_nome = smp.Pacote_nome,
                        //    S_merc_meal_tarifa = smm.S_merc_meal_tarifa,
                        //    S_merc_tarif_status_id = smm.S_merc_tarif_status_id,
                        //    S_merc_meal_tarifa_total = smm.S_merc_meal_tarifa_total,
                        //    S_meal_id = smm.S_meal_id
                        //}).ToList();

            }
            catch
            {
                throw;
            }
        }

    }
}
