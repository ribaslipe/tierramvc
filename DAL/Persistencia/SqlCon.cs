﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace DAL.Persistencia
{
    public class SqlCon
    {

        public SqlConnection Con;
        public SqlCommand Cmd;
        public SqlDataAdapter Sda;
        public SqlDataReader Dr;   

        public void AbrirConexao()
        {
            //string[] pars = ConfigurationManager.ConnectionStrings["Conexao"].ToString().Split('\"');
            string conexao = ConfigurationManager.ConnectionStrings["Conexao"].ToString();
            Con = new SqlConnection(@conexao);            
            Con.Open(); 
        }

        public void FecharConexao()
        {
            if(Con != null) 
            {
                Con.Close(); 
            }
        }


    }
}
