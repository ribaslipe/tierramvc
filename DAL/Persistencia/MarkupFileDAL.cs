﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Persistencia;
using DAL.Entity;
using System.Data;

namespace DAL.Persistencia
{
    public class MarkupFileDAL
    {

        private Model Con;

        public MarkupFileDAL()
        {
            Con = new Model();
        }

        public void Salvar(MarkupFile m)
        {
            try
            {
                Con.MarkupFile.Add(m);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public MarkupFile ObterPorId(int IdMarkupFile)
        {
            try
            {
                return Con.MarkupFile.Where(m => m.MarkupFile_id == IdMarkupFile).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public MarkupFile ObterPorQGrupo(int IdQuotGrupo)
        {
            try
            {
                return Con.MarkupFile.Where(m => m.Quotation_Grupo_Id == IdQuotGrupo).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public MarkupFile ObterPorQGrupo(int IdQuotGrupo, DateTime dtIn)
        {
            try
            {
                return Con.MarkupFile.Where(m => m.Quotation_Grupo_Id == IdQuotGrupo &&
                                                 m.MarkupFile_dataInicial <= dtIn &&
                                                 m.MarkupFile_dataFinal >= dtIn).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }        

        public MarkupFile ObterPorQGrupoFirst(int IdQuotGrupo)
        {
            try
            {
                return Con.MarkupFile.Where(m => m.Quotation_Grupo_Id == IdQuotGrupo).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<MarkupFile> LisarPorQuot(int IdQuotGrupo)
        {
            try
            {
                return Con.MarkupFile.Where(m => m.Quotation_Grupo_Id == IdQuotGrupo).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(MarkupFile novo)
        {
            try
            {
                MarkupFile antigo = ObterPorId(novo.MarkupFile_id);

                antigo.MarkupFile_dataInicial = novo.MarkupFile_dataInicial;
                antigo.MarkupFile_dataFinal = novo.MarkupFile_dataFinal;
                antigo.MarkupFile_hotel = novo.MarkupFile_hotel;
                antigo.MarkupFile_descontoHotel = novo.MarkupFile_descontoHotel;
                antigo.MarkupFile_servico = novo.MarkupFile_servico;
                antigo.MarkupFile_descontoServico = novo.MarkupFile_descontoServico;
                antigo.MarkupFile_seasonal = novo.MarkupFile_seasonal;
                antigo.MarkupFile_meals = novo.MarkupFile_meals;

                Con.SaveChanges();
                            
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(MarkupFile m)
        {
            try
            {
                Con.MarkupFile.Remove(m);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
