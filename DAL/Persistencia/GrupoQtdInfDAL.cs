﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class GrupoQtdInfDAL
    {

        private Model Con;

        public GrupoQtdInfDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quotation_Grupo_Qtd_Baby q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Baby.Add(q);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Baby ObterPorId(int IdGQBaby)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Baby.Where(q => q.Qtd_Baby_id == IdGQBaby).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Baby ObterPorIdGrupo(int IdGQBaby)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Baby.Where(q => q.Quotation_Grupo_Id == IdGQBaby).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Quotation_Grupo_Qtd_Baby novo)
        {
            try
            {
                Quotation_Grupo_Qtd_Baby antigo = ObterPorId(novo.Qtd_Baby_id);

                antigo.Qtd = novo.Qtd;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
