﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class PacoteFotoDAL
    {

        private Model Con;

        public PacoteFotoDAL()
        {
            Con = new Model();
        }

        public void Salvar(Pacote_Foto c)
        {
            try
            {

                //Rotinas prontas para o banco de dados
                Con.Pacote_Foto.Add(c); //insert into S_Image values...
                Con.SaveChanges(); //Executar a operação

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Pacote_Foto c)
        {
            try
            {
                Con.Pacote_Foto.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        public List<Pacote_Foto> ListarTodos()
        {
            try
            {
                return Con.Pacote_Foto.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Pacote_Foto> ListarTodosLista(string Code)
        {
            try
            {
                return Con.Pacote_Foto.Where(i => i.Pacote_Code.Equals(Code)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public Pacote_Foto ObterPorSupplierCapa(string Code)
        {
            try
            {
                return Con.Pacote_Foto.Where(i => i.Pacote_Code.Equals(Code) && i.Pacote_Foto_capa.Equals("S")).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Pacote_Foto ObterPorId(int IdSimage)
        {
            try
            {
                return Con.Pacote_Foto.Where(c => c.Pacote_Foto_id == IdSimage).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Pacote_Foto registroNovo)
        {
            try
            {
                Pacote_Foto registroAntigo = ObterPorId(registroNovo.Pacote_Foto_id);

                registroAntigo.Pacote_Foto_nome = registroNovo.Pacote_Foto_nome;
                registroAntigo.Pacote_Foto_capa = registroNovo.Pacote_Foto_capa;
                registroAntigo.Pacote_Foto_memo = registroNovo.Pacote_Foto_memo;
                registroAntigo.Pacote_Foto_hupload = registroNovo.Pacote_Foto_hupload;
                registroAntigo.Pacote_Foto_descr = registroNovo.Pacote_Foto_descr;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void ImagemPrincipal(string Code)
        {
            try
            {
                Pacote_Foto Foto = Con.Pacote_Foto.Where(f => f.Pacote_Foto_capa.Equals("S") && f.Pacote_Code.Equals(Code)).SingleOrDefault();

                if (Foto == null)
                {
                    Foto = Con.Pacote_Foto.Where(f => f.Pacote_Code.Equals(Code)).First();
                    Foto.Pacote_Foto_capa = "S";
                    Atualizar(Foto);
                }
            }
            catch
            {
                throw;
            }
        }

        public void AddImagemPrincipal(string Code, int IdImagem)
        {
            try
            {
                Pacote_Foto FotoAntiga = Con.Pacote_Foto.Where(i => i.Pacote_Code.Equals(Code) && i.Pacote_Foto_capa.Equals("S")).SingleOrDefault();

                FotoAntiga.Pacote_Foto_capa = "N";
                Atualizar(FotoAntiga);

                Pacote_Foto FotoNova = Con.Pacote_Foto.Where(i => i.Pacote_Foto_id == IdImagem).Single();
                FotoNova.Pacote_Foto_capa = "S";
                Atualizar(FotoNova);
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarMemo(Pacote_Foto novo)
        {
            try
            {
                Pacote_Foto antigo = ObterPorId(novo.Pacote_Foto_id);

                antigo.Pacote_Foto_memo = novo.Pacote_Foto_memo;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
}
