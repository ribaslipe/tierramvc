﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Models;

namespace DAL.Persistencia
{
    public class ServicoCompletoDAL
    {

        private Model Con;

        public ServicoCompletoDAL()
        {
            Con = new Model();
        }

        public void Salvar(Servico_Completo s)
        {
            try
            {
                Con.Servico_Completo.Add(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Servico_Completo ObterPorId(int IdServico)
        {
            try
            {
                return Con.Servico_Completo.Where(s => s.ServicoCompleto_id == IdServico).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Servico_Completo ObterPorIdTemp(int IdMServicoTemp)
        {
            try
            {
                return Con.Servico_Completo.Where(s => s.IdMServico_Temporada == IdMServicoTemp).First();
            }
            catch
            {
                throw;
            }
        }

        public Servico_Completo ObterPorIdMServicoTemp(int IdMServico, int IdMServicoTemp, int index)
        {
            try
            {
                return Con.Servico_Completo.Where(s => s.IdMServico == IdMServico &&
                                                       s.IdMServico_Temporada == IdMServicoTemp &&
                                                       s.Base_Index == index &&
                                                       s.SubServico == null).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Servico_Completo> ListarTodos()
        {
            try
            {
                return Con.Servico_Completo.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Servico_Completo> ListarTodosTemporada(int IdMServicoTemp)
        {
            try
            {
                return Con.Servico_Completo.Where(s => s.IdMServico_Temporada == IdMServicoTemp).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Servico_Completo> ListarTodos(int IdServico)
        {
            try
            {
                return Con.Servico_Completo.Where(s => s.Servicos_Id == IdServico).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Servico_Completo> ListarTodosSubServico(int IdServico, int IdMServicoTemp)
        {
            try
            {
                return Con.Servico_Completo.Where(s => s.Servicos_Id == IdServico &&
                                                       s.IdMServico_Temporada == IdMServicoTemp &&
                                                       s.SubServico == null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<GridServPorSuppModel> ListarTodosServPorSupplier_model(int IdSupplier)
        {
            try
            {

                return (from ssv in Con.S_Servicos
                        join scp in Con.Servico_Completo on ssv.Servicos_Id equals scp.Servicos_Id
                        join mon in Con.Monta_Servico on scp.IdMServico equals mon.IdMServico
                        where mon.S_id == IdSupplier
                        select new GridServPorSuppModel
                        {
                            Servicos_Nome = ssv.Servicos_Nome,
                            MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                            MServico_DataTo = (DateTime)mon.MServico_DataTo
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosServPorSupplier(int IdSupplier)
        {
            try
            {

                return (from ssv in Con.S_Servicos
                        join scp in Con.Servico_Completo on ssv.Servicos_Id equals scp.Servicos_Id
                        join mon in Con.Monta_Servico on scp.IdMServico equals mon.IdMServico
                        where mon.S_id == IdSupplier
                        select new
                        {
                            ssv.Servicos_Nome,
                            mon.MServico_DataFrom,
                            mon.MServico_DataTo
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Servico_Completo s)
        {
            try
            {
                Con.Servico_Completo.Remove(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Servico_Completo novo)
        {
            try
            {
                Servico_Completo antigo = ObterPorId(novo.ServicoCompleto_id);

                antigo.ServicoCompleto_de = novo.ServicoCompleto_de;
                antigo.ServicoCompleto_ate = novo.ServicoCompleto_ate;

                if(novo.IdMServico != null)
                    antigo.IdMServico = novo.IdMServico;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMServicoTemp(int IdMServico, int IdMServicoTemp)
        {
            try
            {
                return Con.Servico_Completo.Where(s => s.IdMServico == IdMServico &&
                                                       s.IdMServico_Temporada == IdMServicoTemp).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMServico(int IdMServico, int IdServico)
        {
            try
            {
                return Con.Servico_Completo.Where(s => s.IdMServico == IdMServico &&
                                                       s.Servicos_Id == IdServico).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public Servico_Completo VerificaPorNome(string nomeServico, DateTime dtFrom)
        {
            try
            {
                #region query                
                //SELECT DISTINCT
                //S_Servicos.Servicos_Nome, Servico_Completo.SubServico, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, Servico_Completo.ServicoCompleto_id, 
                //Servico_Completo.IdMServico_Temporada
                //FROM            S_Servicos INNER JOIN
                //Servico_Completo ON S_Servicos.Servicos_Id = Servico_Completo.Servicos_Id INNER JOIN
                //Monta_Servico ON Servico_Completo.IdMServico = Monta_Servico.IdMServico
                //WHERE(S_Servicos.Servicos_Nome = 'Cabo Frio-Rio de Janeiro In Group (3h)') AND('20160628' BETWEEN Monta_Servico.MServico_DataFrom AND Monta_Servico.MServico_DataTo)
                #endregion

                return (from sse in Con.S_Servicos
                        join sco in Con.Servico_Completo on sse.Servicos_Id equals sco.Servicos_Id
                        join mon in Con.Monta_Servico on sco.IdMServico equals mon.IdMServico
                        where sse.Servicos_Nome.Equals(nomeServico) &&
                              (dtFrom >= mon.MServico_DataFrom && dtFrom <= mon.MServico_DataTo)
                        select sco).FirstOrDefault();

            }
            catch 
            {
                throw;
            }
        }

        public Servico_Completo VerificaPorNome_sub(string nomeServico, DateTime dtFrom)
        {
            try
            {
                #region query                
                //SELECT DISTINCT
                //S_Servicos.Servicos_Nome, Servico_Completo.SubServico, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, Servico_Completo.ServicoCompleto_id, 
                //Servico_Completo.IdMServico_Temporada
                //FROM            S_Servicos INNER JOIN
                //Servico_Completo ON S_Servicos.Servicos_Id = Servico_Completo.Servicos_Id INNER JOIN
                //Monta_Servico ON Servico_Completo.IdMServico = Monta_Servico.IdMServico
                //WHERE(S_Servicos.Servicos_Nome = 'Cabo Frio-Rio de Janeiro In Group (3h)') AND('20160628' BETWEEN Monta_Servico.MServico_DataFrom AND Monta_Servico.MServico_DataTo)
                #endregion

                return (from sse in Con.S_Servicos
                        join sco in Con.Servico_Completo on sse.Servicos_Id equals sco.Servicos_Id
                        join mon in Con.Monta_Servico on sco.IdMServico equals mon.IdMServico
                        where sse.Servicos_Nome.Equals(nomeServico) &&
                              sco.SubServico == null &&
                              (dtFrom >= mon.MServico_DataFrom && dtFrom <= mon.MServico_DataTo)
                        select sco).FirstOrDefault();

            }
            catch
            {
                throw;
            }
        }

    }
}
