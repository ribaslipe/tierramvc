﻿using DAL.Entity;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class SupplierXMLDAL
    {
        private Model Con;

        public SupplierXMLDAL()
        {
            Con = new Model();
        }

        public Supplier_XML Obter(int ID)
        {
            return Con.Supplier_XML.SingleOrDefault(a => a.Id == ID);
        }

        public List<Supplier_XML> Listar()
        {
            return Con.Supplier_XML.ToList();
        }

        public Supplier_XML ObterPorSppPro(int idSpp)
        {
            try
            {
                return Con.Supplier_XML.SingleOrDefault(s => s.SupplierId_Prosoftware == idSpp);
            }
            catch
            {
                throw;
            }
        }

        public Supplier_XML ObterPorSppPro(int idSpp, int idPlat)
        {
            try
            {
                return Con.Supplier_XML.SingleOrDefault(s => s.SupplierId_Prosoftware == idSpp && s.Id_Plataforma == idPlat);
            }
            catch
            {
                throw;
            }
        }

        public Supplier_XML ObterPorSppPL(string idSpp)
        {
            try
            {
                return Con.Supplier_XML.SingleOrDefault(s => s.SupplerId_Plataforma == idSpp);
            }
            catch
            {
                throw;
            }
        }

        public Supplier_XML ObterPorSppPorCidade(string idSuppPlat, int cidade)
        {
            try
            {
                return Con.Supplier_XML.SingleOrDefault(s => s.SupplerId_Plataforma == idSuppPlat && s.Supplier.CID_id == cidade);
            }
            catch
            {
                throw;
            }
        }

        public Supplier_XML ObterPorSppPL(string idSpp, int idCidade)
        {
            try
            {
                return Con.Supplier_XML.SingleOrDefault(s => s.SupplerId_Plataforma == idSpp &&
                                                   s.Supplier.CID_id == idCidade);
            }
            catch
            {
                throw;
            }
        }


        public void Salvar(Supplier_XML p)
        {
            try
            {
                Con.Supplier_XML.Add(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Supplier_XML p)
        {
            try
            {
                //remover primeiramente as categorias mescladas para esse hotel
                var tarifCategoriaXmls = Con.Tarif_Categoria_XML.Where(x => x.S_XMLId == p.Id).ToList();
                foreach (var item in tarifCategoriaXmls)
                {
                    Con.Tarif_Categoria_XML.Remove(item);
                    Con.SaveChanges();
                }
                
                Con.Supplier_XML.Remove(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Supplier_XML novo)
        {
            try
            {
                Supplier_XML antigo = Obter(novo.Id);

                antigo.Id_Plataforma = novo.Id_Plataforma;
                antigo.SupplerId_Plataforma = novo.SupplerId_Plataforma;
                antigo.SupplierId_Prosoftware = novo.SupplierId_Prosoftware;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Supplier_XML> ListarTodos()
        {
            try
            {
                return Con.Supplier_XML.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<HoteisOmniModel> ObterTodos()
        {
            try
            {

                return (from pxm in Con.Supplier_XML
                        join pas in Con.Supplier on pxm.SupplierId_Prosoftware equals pas.S_id
                        join pla in Con.Plataforma_XML on pxm.Id_Plataforma equals pla.Plataforma_id
                        select new HoteisOmniModel
                        {
                            S_nome = pas.S_nome,
                            Plataforma_nome = pla.Plataforma_nome,
                            SupplerId_Plataforma = pxm.SupplerId_Plataforma,
                            SupplierId_Prosoftware = pxm.SupplierId_Prosoftware,
                            Id = pxm.Id
                        }).ToList();

            }
            catch
            {
                throw;
            }
        }
        

        public object ObterTodos_()
        {
            try
            {

                return (from pxm in Con.Supplier_XML
                        join pas in Con.Supplier on pxm.SupplierId_Prosoftware equals pas.S_id
                        join pla in Con.Plataforma_XML on pxm.Id_Plataforma equals pla.Plataforma_id
                        select new
                        {
                            pas.S_nome,
                            pla.Plataforma_nome,
                            pxm.SupplerId_Plataforma,
                            pxm.SupplierId_Prosoftware,
                            ID = pxm.Id,
                            Nome = pxm.Supplier.S_nome
                        }).OrderBy(s => s.S_nome).ToList();

            }
            catch
            {
                throw;
            }
        }

    }
}
