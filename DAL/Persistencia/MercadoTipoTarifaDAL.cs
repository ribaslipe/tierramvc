﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class MercadoTipoTarifaDAL
    {

        private Model Con;

        public MercadoTipoTarifaDAL()
        {
            Con = new Model();
        }

        public List<S_Mercado_Tipo_Tarifa> ListarTodos()
        {
            try
            {
                return Con.S_Mercado_Tipo_Tarifa.ToList();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
