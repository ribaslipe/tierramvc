﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class TBItinerary_PricesDAL
    {
        private Model Con;

        public TBItinerary_PricesDAL()
        {
            Con = new Model();
        }

        public List<TBItinerary_Prices> ObterPorQuote(string quotationCode, int optQuote)
        {
            return Con.TBItinerary_Prices.Where(a => a.QuotationCode == quotationCode && a.optQuote == optQuote).ToList();
        }

        public TBItinerary_Prices ObterPorBaseAndRoom(string quotationCode, string Base, string room, int optQuote)
        {
            return Con.TBItinerary_Prices.Where(a =>    a.QuotationCode == quotationCode && 
                                                        a.Base == Base &&
                                                        a.optQuote == optQuote &&
                                                        a.TipoRoom == room).SingleOrDefault();
        }

        public TBItinerary_Prices ObterPorBaseAndRoom_(string quotationCode, int optQuote)
        {
            return Con.TBItinerary_Prices.Where(a => a.QuotationCode == quotationCode &&                                                       
                                                        a.optQuote == optQuote &&                                                      
                                                        !String.IsNullOrEmpty(a.Preco_Notes)).FirstOrDefault();
        }

        public TBItinerary_Prices ObterPorBaseAndRoom_Link(string quotationCode, int optQuote)
        {
            return Con.TBItinerary_Prices.Where(a => a.QuotationCode == quotationCode &&
                                                        a.optQuote == optQuote &&
                                                        !String.IsNullOrEmpty(a.Preco_Link)).FirstOrDefault();
        }

        public void Salvar(TBItinerary_Prices tbp)
        {
            Con.TBItinerary_Prices.Add(tbp);
            Con.SaveChanges();
        }

        public void Atualizar(TBItinerary_Prices tbp)
        {
            TBItinerary_Prices old = ObterPorBaseAndRoom(tbp.QuotationCode, tbp.Base, tbp.TipoRoom, tbp.optQuote);

            old.PrecoAlterado = tbp.PrecoAlterado;
            old.Preco_Notes = tbp.Preco_Notes;
            old.Preco_Link = tbp.Preco_Link;
            old.visible = tbp.visible;

            Con.SaveChanges();
        }
    }
}
