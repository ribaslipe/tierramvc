﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class GrupoQtdAdultNomeDAL
    {

        private Model Con;

        public GrupoQtdAdultNomeDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quotation_Grupo_Qtd_Adult_Nome q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Adult_Nome.Add(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Adult_Nome ObterPorId(int IdAdultNome)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Adult_Nome.Where(q => q.Qtd_Adult_Nome_id == IdAdultNome).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Quotation_Grupo_Qtd_Adult_Nome q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Adult_Nome.Remove(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Quotation_Grupo_Qtd_Adult_Nome novo)
        {
            try
            {
                Quotation_Grupo_Qtd_Adult_Nome antigo = ObterPorId(novo.Qtd_Adult_Nome_id);

                antigo.Nome = novo.Nome;
                antigo.Sobrenome = novo.Sobrenome;
                antigo.Tratamento = novo.Tratamento;
                antigo.Idade = novo.Idade;
                antigo.Principal_Indicador = novo.Principal_Indicador;
                antigo.voucher = novo.voucher;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNoVoucher(Quotation_Grupo_Qtd_Adult_Nome novo)
        {
            try
            {
                Quotation_Grupo_Qtd_Adult_Nome antigo = ObterPorId(novo.Qtd_Adult_Nome_id);

                antigo.Nome = novo.Nome;
                antigo.Sobrenome = novo.Sobrenome;
                antigo.Tratamento = novo.Tratamento;
                antigo.Idade = novo.Idade;
                antigo.Principal_Indicador = novo.Principal_Indicador;
                

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Grupo_Qtd_Adult_Nome> VerificaExiste(int IdQuotGrupo)
        {
            try
            {

                return (from qga in Con.Quotation_Grupo_Qtd_Adult_Nome
                        join qgq in Con.Quotation_Grupo_Qtd_Adult on qga.Qtd_Adult_id equals qgq.Qtd_Adult_id
                        where qgq.Quotation_Grupo_Id == IdQuotGrupo
                        select qga).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Grupo_Qtd_Adult_Nome> VerificaExiste(int IdQuotGrupo, int grpnumber)
        {
            try
            {

                return (from qga in Con.Quotation_Grupo_Qtd_Adult_Nome
                        join qgq in Con.Quotation_Grupo_Qtd_Adult on qga.Qtd_Adult_id equals qgq.Qtd_Adult_id
                        where qgq.Quotation_Grupo_Id == IdQuotGrupo &&
                              qgq.grp_number == grpnumber
                        select qga).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Grupo_Qtd_Adult_Nome> ListarPorAdultos(int IdQtdAdult)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Adult_Nome.Where(q => q.Qtd_Adult_id == IdQtdAdult).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar_Primary(Quotation_Grupo_Qtd_Adult_Nome novo)
        {
            try
            {
                Quotation_Grupo_Qtd_Adult_Nome antigo = ObterPorId(novo.Qtd_Adult_Nome_id);
                antigo.Principal_Indicador = novo.Principal_Indicador;
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
}
