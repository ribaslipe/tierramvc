﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class Voucher_IncludedServicesDAL
    {
        private Model Con;

        public Voucher_IncludedServicesDAL()
        {
            Con = new Model();
        }

        public List<Voucher_IncludedServices> ObterPorVoucher(int voucherId)
        {
            return Con.Voucher_IncludedServices.Where(a => a.VoucherId == voucherId).ToList();
        }

        public Voucher_IncludedServices ObterPorId(int Id)
        {
            return Con.Voucher_IncludedServices.FirstOrDefault(a => a.Id == Id);
        }

        public List<Voucher_IncludedServices> AddDefault(int voucherId)
        {
            bool isEdited = false;

            List<Voucher_IncludedServices> lvr = new List<Voucher_IncludedServices>();
            for (int i = 0; i < 4; i++)
            {
                Voucher_IncludedServices vis = new Voucher_IncludedServices();
                vis.VoucherId = voucherId;

                if (i == 0)
                    vis.Service = "All overnight stays at the mentioned accommodation types.";
                else if (i == 1)
                    vis.Service = "Daily breakfast.";
                else if (i == 2)
                    vis.Service = "Transfers and tours as mentioned with English-speaking guide (unless noted).";
                else if (i == 3)
                    vis.Service = "Entrance fees for visits as mentioned";

                if (voucherId > 0)
                {
                    Con.Voucher_IncludedServices.Add(vis);
                    isEdited = true;
                }
                else
                {
                    vis.Id = i;
                    lvr.Add(vis);
                }
            }

            if (isEdited)
            {
                var v = Con.Voucher.Where(a => a.Id == voucherId).Single();
                v.IncludedServicesEdited = true;
            }

            Con.SaveChanges();

            if (voucherId > 0)
                return Con.Voucher_IncludedServices.Where(a => a.VoucherId == voucherId).ToList();
            else
                return lvr;
        }

        public int Adicionar(Voucher_IncludedServices vis)
        {
            Con.Voucher_IncludedServices.Add(vis);
            Con.SaveChanges();
            return vis.Id;
        }

        public void Update(Voucher_IncludedServices vis)
        {
            var old = Con.Voucher_IncludedServices.First(a => a.Id == vis.Id);
            old.Service = vis.Service;
            Con.SaveChanges();
        }

        public void Excluir(Voucher_IncludedServices vis)
        {
            Con.Voucher_IncludedServices.Remove(vis);
            Con.SaveChanges();
        }

        public void ExcluirTodos(int voucherId)
        {
            foreach (var item in ObterPorVoucher(voucherId))
            {
                Con.Voucher_IncludedServices.Remove(item);
            }

            Con.SaveChanges();
        }
    }
}
