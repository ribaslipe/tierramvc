﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class ServicoTipoCategDAL
    {

        Model Con;

        public ServicoTipoCategDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Servicos_Tipo_Categ s)
        {
            try
            {
                Con.S_Servicos_Tipo_Categ.Add(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Servicos_Tipo_Categ ObterPorId(int IdServ)
        {
            try
            {
                return Con.S_Servicos_Tipo_Categ.Where(s => s.Tipo_categ_id == IdServ).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Servicos_Tipo_Categ ObterPorNome(string nome)
        {
            try
            {
                return Con.S_Servicos_Tipo_Categ.Where(s => s.Tipo_categ_nome == nome).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(S_Servicos_Tipo_Categ novo)
        {
            try
            {
                S_Servicos_Tipo_Categ antigo = ObterPorId(novo.Tipo_categ_id);

                antigo.Tipo_categ_nome = novo.Tipo_categ_nome;
                antigo.Tipo_Id = novo.Tipo_Id;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(S_Servicos_Tipo_Categ s)
        {
            try
            {
                Con.S_Servicos_Tipo_Categ.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaExiste(int IdTipoServico)
        {
            try
            {
                return Con.S_Servicos_Tipo_Categ.Where(s => s.Tipo_Id == IdTipoServico).Count() != 0;
            }
            catch 
            {
                throw;
            }
        }

        public List<S_Servicos_Tipo_Categ> ListarTodos(int IdTipo)
        {
            try
            {
                return Con.S_Servicos_Tipo_Categ.Where(s => s.Tipo_Id == IdTipo).OrderBy(s => s.Tipo_categ_nome).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Servicos_Tipo_Categ> ListarTodos()
        {
            try
            {
                return Con.S_Servicos_Tipo_Categ.OrderBy(s => s.Tipo_categ_nome).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Servicos_Tipo_Categ> ListarTodos(string nome)
        {
            try
            {
                return Con.S_Servicos_Tipo_Categ.Where(s => s.Tipo_categ_nome.Contains(nome)).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaNome(string Nome)
        {
            try
            {
                return Con.S_Servicos_Tipo_Categ.Where(s => s.Tipo_categ_nome.Equals(Nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

    }
}
