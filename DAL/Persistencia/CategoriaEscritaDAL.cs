﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class CategoriaEscritaDAL
    {

        private Model Con;

        public CategoriaEscritaDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Categoria_Escrita s)
        {
            try
            {
                Con.S_Categoria_Escrita.Add(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Categoria_Escrita ObterPorId(int IdSCaregoria)
        {
            try
            {
                return Con.S_Categoria_Escrita.Where(s => s.CategoriaEscrita_id == IdSCaregoria).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(S_Categoria_Escrita novo)
        {
            try
            {
                S_Categoria_Escrita antigo = ObterPorId(novo.CategoriaEscrita_id);

                antigo.CategoriaEscrita_nome = novo.CategoriaEscrita_nome;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(S_Categoria_Escrita s)
        {
            try
            {
                Con.S_Categoria_Escrita.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Categoria_Escrita> ListarTodos(string nome)
        {
            try
            {
                if (nome.Equals(""))
                {
                    return Con.S_Categoria_Escrita.OrderBy(p => p.CategoriaEscrita_nome).ToList();
                }
                else
                {
                    return Con.S_Categoria_Escrita.Where(s => s.CategoriaEscrita_nome.Contains(nome)).OrderBy(p => p.CategoriaEscrita_nome).ToList();
                }
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaAgregado(int IdCategoriaEscrita)
        {
            try
            {
                return Con.Supplier.Any(c => c.CategoriaEscrita_id == IdCategoriaEscrita);
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExistente(string nome)
        {
            try
            {

                return Con.S_Categoria_Escrita.Where(t => t.CategoriaEscrita_nome.Equals(nome)).Count() != 0;

            }
            catch
            {
                throw;
            }
        }

        public S_Categoria_Escrita ObterPorNome(string nome)
        {
            try
            {

                return Con.S_Categoria_Escrita.Where(t => t.CategoriaEscrita_nome.Equals(nome)).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

    }
}
