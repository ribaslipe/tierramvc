﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class TarifCategoriaDAL
    {

        private Model Con;

        public TarifCategoriaDAL()
        {
            Con = new Model();
        }

        public List<Tarif_Categoria> ListarTodos()
        {
            try
            {

                return Con.Tarif_Categoria.OrderBy(s => s.Tarif_categoria_nome).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<Tarif_Categoria> ListarTodos(string nome)
        {
            try
            {

                if (nome.Equals(""))
                {
                    return Con.Tarif_Categoria.OrderBy(c => c.Tarif_categoria_nome).ToList();
                }
                else
                {
                    return Con.Tarif_Categoria.Where(t => t.Tarif_categoria_nome.Contains(nome)).OrderBy(o => o.Tarif_categoria_nome).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(int IdSupplier)
        {
            try
            {

                return (from tcg in Con.Tarif_Categoria
                        join smt in Con.S_Mercado_Tarifa on tcg.Tarif_categoria_id equals smt.Tarif_categoria_id
                        join smp in Con.S_Mercado_Periodo on smt.S_merc_periodo_id equals smp.S_merc_periodo_id
                        where smp.S_id == IdSupplier
                        select new
                        {
                            tcg.Tarif_categoria_id,
                            tcg.Tarif_categoria_nome
                        }).Distinct().ToList();

            }
            catch 
            {
                
                throw;
            }
        }

        public List<Tarif_Categoria> ListarTodos_model(int IdSupplier)
        {
            try
            {

                return (from tcg in Con.Tarif_Categoria
                        join smt in Con.S_Mercado_Tarifa on tcg.Tarif_categoria_id equals smt.Tarif_categoria_id
                        join smp in Con.S_Mercado_Periodo on smt.S_merc_periodo_id equals smp.S_merc_periodo_id
                        where smp.S_id == IdSupplier
                        select tcg).Distinct().ToList();

            }
            catch
            {

                throw;
            }
        }

        public bool VerificaExistente(string nome)
        {
            try
            {

                return Con.Tarif_Categoria.Where(t => t.Tarif_categoria_nome.Equals(nome)).Count() != 0;

            }
            catch
            {
                throw;
            }
        }

        public Tarif_Categoria ObterPorNome(string nome)
        {
            try
            {

                return Con.Tarif_Categoria.Where(t => t.Tarif_categoria_nome.Equals(nome)).FirstOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosJaCadastrados(int IdPeriodo)
        {
            try
            {
                var query = (from tc in Con.Tarif_Categoria
                             join mt in Con.S_Mercado_Tarifa on tc.Tarif_categoria_id equals mt.Tarif_categoria_id
                             where mt.S_merc_periodo_id == IdPeriodo
                             select new
                             {
                                 tc.Tarif_categoria_nome,
                                 tc.Tarif_categoria_id

                             }).Distinct().ToList();

                return query;

            }
            catch
            {
                throw;
            }
        }

        public void Salvar(Tarif_Categoria c)
        {
            try
            {

                Con.Tarif_Categoria.Add(c);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Tarif_Categoria ObterPorId(int IdCategoria)
        {
            try
            {

                return Con.Tarif_Categoria.Where(d => d.Tarif_categoria_id == IdCategoria).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Tarif_Categoria c)
        {
            try
            {

                Con.Tarif_Categoria.Remove(c);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Tarif_Categoria novo)
        {
            try
            {
                Tarif_Categoria antigo = ObterPorId(novo.Tarif_categoria_id);

                antigo.Tarif_categoria_nome = novo.Tarif_categoria_nome;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public List<Tipo_Room> QtdRooms_Categ(int Sid, DateTime dtfrom, DateTime dtto, int trID)
        {
            try
            {
                //SELECT DISTINCT Tipo_Room.Tipo_room_nome, Tipo_Room.Tipo_room_number, Tarif_Categoria.Tarif_categoria_nome, Tarif_Categoria.Tarif_categoria_id
                //FROM            Supplier INNER JOIN
                //Supplier_XML ON Supplier.S_id = Supplier_XML.SupplierId_Prosoftware INNER JOIN
                //S_Mercado_Periodo ON Supplier.S_id = S_Mercado_Periodo.S_id INNER JOIN
                //S_Mercado_Tarifa ON S_Mercado_Periodo.S_merc_periodo_id = S_Mercado_Tarifa.S_merc_periodo_id INNER JOIN
                //Tipo_Room ON S_Mercado_Tarifa.Tipo_room_id = Tipo_Room.Tipo_room_id INNER JOIN
                //Tarif_Categoria ON S_Mercado_Tarifa.Tarif_categoria_id = Tarif_Categoria.Tarif_categoria_id
                //WHERE(Supplier.S_id = 2030) AND(S_Mercado_Periodo.S_merc_periodo_from = '20170101') AND(S_Mercado_Periodo.S_merc_periodo_to = '20171228') AND(S_Mercado_Tarifa.Tarif_categoria_id = 105)


                return (from spp in Con.Supplier
                        join spx in Con.Supplier_XML on spp.S_id equals spx.SupplierId_Prosoftware
                        join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        join tro in Con.Tipo_Room on smt.Tipo_room_id equals tro.Tipo_room_id
                        join trc in Con.Tarif_Categoria on smt.Tarif_categoria_id equals trc.Tarif_categoria_id
                        where spp.S_id == Sid &&
                              smp.S_merc_periodo_from == dtfrom &&
                              smp.S_merc_periodo_to == dtto &&
                              smt.Tarif_categoria_id == trID
                        select tro).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
