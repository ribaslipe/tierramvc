﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class HolidaysDatesDAL
    {

        private Model Con;

        public HolidaysDatesDAL()
        {
            Con = new Model();
        }

        public List<Holidays_Dates> ObterPorIdHolidays(int IdHol)
        {
            try
            {
                return Con.Holidays_Dates.Where(h => h.Holidays_id == IdHol).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public void Salvar(Holidays_Dates h)
        {
            try
            {
                Con.Holidays_Dates.Add(h);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(Holidays_Dates h)
        {
            try
            {
                Con.Holidays_Dates.Remove(h);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Holidays_Dates ObterPorId(int IdHoliDates)
        {
            try
            {
                return Con.Holidays_Dates.Where(h => h.Holidays_Dates_id == IdHoliDates).SingleOrDefault();   
            }
            catch 
            {                
                throw;
            }
        }

    }
}
