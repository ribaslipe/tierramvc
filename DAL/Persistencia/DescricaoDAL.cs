﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class DescricaoDAL
    {
        Model Con = new Model();

        //Método para gravar um registro na tabela Periodo
        public void Salvar(S_Descricao c)
        {
            try
            {

                //Rotinas prontas para o banco de dados
                Con.S_Descricao.Add(c); //insert into S_Descricao values...
                Con.SaveChanges(); //Executar a operação

            }
            catch
            {
                throw;
            }
        }

        //Método para Excluir o S_Descricao na base
        public void Excluir(S_Descricao c)
        {
            try
            {
                Con.S_Descricao.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        //Metodo para listar todos os S_Descricao cadastrados
        public List<S_Descricao> ListarTodos()
        {
            try
            {
                //SQL -> select * from S_Descricao
                return Con.S_Descricao.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Metodo para listar todos os S_Descricao cadastrados por periodo ID e left join
        //public IEnumerable<Tuple<Supplier, S_Descricao, S_Image>> ListarTodos(Int32 SupplierID)
        //{
        //    try
        //    {

        //        var query = from s in Con.Suppliers
        //                    join d in Con.S_Descricao on s.S_id equals d.S_id
        //                    join i in Con.S_Image on s.S_id equals i.S_id
        //                    where d.S_id == SupplierID
        //                    select new Tuple<Supplier, S_Descricao, S_Image>
        //                        (
        //                            s,
        //                            d,
        //                            i
        //                        );

        //        return query;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public Supplier ListarTodos(Int32 SupplierID)
        {
            try
            {
                return Con.Supplier.SingleOrDefault(s => s.S_id == SupplierID);
            }
            catch
            {
                throw;
            }
        }

        //Método para obter 1 S_Descricao pelo Id
        public S_Descricao ObterPorId(int IdDescricao)
        {
            try
            {
                //select * from TbCliente where IdCliente = ?
                return Con.S_Descricao.Where(c => c.Desc_id == IdDescricao).Single();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        //Método para atualizar os dados do S_Descricao
        public void Atualizar(S_Descricao registroNovo)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                S_Descricao registroAntigo = ObterPorId(registroNovo.Desc_id);

                //Atualizando os dados
                registroAntigo.Desc_id = registroNovo.Desc_id;
                registroAntigo.Desc_short = registroNovo.Desc_short;
                registroAntigo.Desc_long = registroNovo.Desc_long;
                registroAntigo.Desc_remarks = registroNovo.Desc_remarks;
                registroAntigo.Desc_cliente = registroNovo.Desc_cliente;
                registroAntigo.Desc_voucher = registroNovo.Desc_voucher;
                registroAntigo.Desc_nRooms = registroNovo.Desc_nRooms;
                registroAntigo.Desc_HoraCheckIn = registroNovo.Desc_HoraCheckIn;
                registroAntigo.Desc_HoraCheckOut = registroNovo.Desc_HoraCheckOut;
                registroAntigo.Desc_LnkAdvisor = registroNovo.Desc_LnkAdvisor;
                registroAntigo.S_id = registroNovo.S_id;


                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public S_Descricao ObterPorIdSupp(int IdSupplier)
        {
            try
            {

                return Con.S_Descricao.Where(c => c.S_id == IdSupplier).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

    }
}
