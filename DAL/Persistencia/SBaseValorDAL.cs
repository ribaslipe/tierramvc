﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class SBaseValorDAL
    {

        private Model Con;

        public SBaseValorDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Bases_Valor s)
        {
            try
            {
                Con.S_Bases_Valor.Add(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public S_Bases_Valor ObterPorId(int IdSBaseValor)
        {
            try
            {
                return Con.S_Bases_Valor.Where(s => s.S_Bases_Valor_id == IdSBaseValor).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_Bases_Valor ObterPorIdBase(int IdBase)
        {
            try
            {
                return Con.S_Bases_Valor.Where(s => s.Base_id == IdBase).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_Bases_Valor ObterPorIds(int IdMServico, int IdBase)
        {
            try
            {
                return Con.S_Bases_Valor.Where(s => s.IdMServico == IdMServico &&
                                                                     s.Base_id == IdBase).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_Bases_Valor ObterPorIdMServ(int IdMServico)
        {
            try
            {
                return Con.S_Bases_Valor.Where(s => s.IdMServico == IdMServico).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(S_Bases_Valor novo)
        {
            try
            {

                S_Bases_Valor antigo = ObterPorId(novo.S_Bases_Valor_id);

                antigo.S_Bases_Valor_valor = novo.S_Bases_Valor_valor;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Bases_Valor s)
        {
            try
            {
                Con.S_Bases_Valor.Remove(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Bases_Valor> ListarTodos(int IdMServicoTempo)
        {
            try
            {
                return Con.S_Bases_Valor.Where(s => s.IdMServico == IdMServicoTempo).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Bases_Valor ObterValorSub(int IdSupplier, string ServNome, DateTime DTfrom, DateTime DTto, int BaseDe)
        {
            try
            {

                return (from mon in Con.Monta_Servico
                        join sco in Con.Servico_Completo on mon.IdMServico equals sco.IdMServico_Temporada
                        join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                        join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                        join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                        where mon.S_id == IdSupplier &&
                              sse.Servicos_Nome.Equals(ServNome) &&
                              mon.MServico_DataFrom <= DTfrom &&
                              mon.MServico_DataTo >= DTto &&
                              sba.Base_de <= BaseDe &&
                              sba.Base_ate >= BaseDe
                        select sbv).Distinct().SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
