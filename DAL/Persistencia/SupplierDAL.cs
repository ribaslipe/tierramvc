﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;


namespace DAL.Persistencia
{
    public class SupplierDAL
    {
        private Model Con;

        public SupplierDAL()
        {
            Con = new Model();
        }

        //Método para gravar um registro na tabela Supplier
        public void Salvar(Supplier c)
        {
            try
            {

                //Rotinas prontas para o banco de dados
                Con.Supplier.Add(c); //insert into Supplier values...
                Con.SaveChanges(); //Executar a operação
            }
            catch
            {
                throw;
            }
        }

        //Método para Excluir o Supplier na base
        public void Excluir(Supplier c)
        {
            try
            {
                Con.Supplier.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        //Metodo para listar todos os Supplier cadastrados 
        public List<Supplier> ListarTodos()
        {
            try
            {
                //SQL -> select * from Supplier
                return Con.Supplier.OrderBy(s => s.S_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> ListarTodos(int IdTipoSupl, int IdCid)
        {
            try
            {
                return Con.Supplier.Where(s => s.TIPOSUPL_id == IdTipoSupl &&
                                                s.CID_id == IdCid).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> ListarTodosTrfs(int IdCid)
        {
            try
            {
                return Con.Supplier.Where(s => s.CID_id == IdCid &&
                                                s.TIPOSUPL_id == 3 ||
                                                s.CID_id == IdCid &&
                                                s.TIPOSUPL_id == 4 ||
                                                s.CID_id == IdCid &&
                                                s.TIPOSUPL_id == 5).ToList();

                //return (from spp in Con.Supplier
                //        join mon in Con.Monta_Servico on spp.S_id equals mon.S_id
                //        join sco in Con.Servico_Completo on mon.IdMServico equals sco.IdMServico
                //        join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                //        where sse.Servicos_transfer != null &&
                //              spp.CID_id == IdCid
                //        select spp).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> ListarTodosTrf(int idCid)
        {

            #region query            
            //SELECT DISTINCT Supplier.S_nome, Monta_Servico.S_id, S_Servicos.Servicos_transfer

            //FROM            S_Servicos INNER JOIN
            //Servico_Completo ON S_Servicos.Servicos_Id = Servico_Completo.Servicos_Id INNER JOIN
            //Monta_Servico ON Servico_Completo.IdMServico = Monta_Servico.IdMServico INNER JOIN
            //Supplier ON Monta_Servico.S_id = Supplier.S_id
            //WHERE(S_Servicos.Cid_Id = 631) AND(Supplier.TIPOSUPL_id = 3) AND(S_Servicos.Servicos_transfer = 'S') OR
            //(S_Servicos.Cid_Id = 631) AND(Supplier.TIPOSUPL_id = 4) AND(S_Servicos.Servicos_transfer = 'S') OR
            //(S_Servicos.Cid_Id = 631) AND(Supplier.TIPOSUPL_id = 5) AND(S_Servicos.Servicos_transfer = 'S')

            //UNION ALL
            //SELECT DISTINCT Supplier_1.S_nome, Monta_Servico_1.S_id, S_Servicos_1.Servicos_transfer

            //FROM            S_Servicos AS S_Servicos_1 INNER JOIN
            //Servico_Completo AS Servico_Completo_1 ON S_Servicos_1.Servicos_Id = Servico_Completo_1.Servicos_Id INNER JOIN
            //Monta_Servico AS Monta_Servico_1 ON Servico_Completo_1.IdMServico_Temporada = Monta_Servico_1.IdMServico INNER JOIN
            //Supplier AS Supplier_1 ON Monta_Servico_1.S_id = Supplier_1.S_id
            //WHERE(S_Servicos_1.Cid_Id = 631) AND(Supplier_1.TIPOSUPL_id = 3) AND(S_Servicos_1.Servicos_transfer = 'S') OR
            //(S_Servicos_1.Cid_Id = 631) AND(Supplier_1.TIPOSUPL_id = 4) AND(S_Servicos_1.Servicos_transfer = 'S') OR
            //(S_Servicos_1.Cid_Id = 631) AND(Supplier_1.TIPOSUPL_id = 5) AND(S_Servicos_1.Servicos_transfer = 'S')
            #endregion

            try
            {

                return ((from ssv in Con.S_Servicos
                         join scp in Con.Servico_Completo on ssv.Servicos_Id equals scp.Servicos_Id
                         join mon in Con.Monta_Servico on scp.IdMServico equals mon.IdMServico
                         join s in Con.Supplier on mon.S_id equals s.S_id
                         where ssv.Cid_Id == idCid &&
                               s.TIPOSUPL_id == 3 &&
                               ssv.Servicos_transfer.Equals("S") ||
                               ssv.Cid_Id == idCid &&
                               s.TIPOSUPL_id == 4 &&
                               ssv.Servicos_transfer.Equals("S") ||
                               ssv.Cid_Id == idCid &&
                               s.TIPOSUPL_id == 5 &&
                               ssv.Servicos_transfer.Equals("S")
                         select s).ToList()
                        .Union(from ssv in Con.S_Servicos
                               join scp in Con.Servico_Completo on ssv.Servicos_Id equals scp.Servicos_Id
                               join mon in Con.Monta_Servico on scp.IdMServico_Temporada equals mon.IdMServico
                               join s in Con.Supplier on mon.S_id equals s.S_id
                               where ssv.Cid_Id == idCid &&
                                     s.TIPOSUPL_id == 3 &&
                                     ssv.Servicos_transfer.Equals("S") ||
                                     ssv.Cid_Id == idCid &&
                                     s.TIPOSUPL_id == 4 &&
                                     ssv.Servicos_transfer.Equals("S") ||
                                     ssv.Cid_Id == idCid &&
                                     s.TIPOSUPL_id == 5 &&
                                     ssv.Servicos_transfer.Equals("S")
                               select s).ToList()).ToList();

            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Supplier> ListarTodosTour(int idCid)
        {

           
            try
            {

                return ((from ssv in Con.S_Servicos
                         join scp in Con.Servico_Completo on ssv.Servicos_Id equals scp.Servicos_Id
                         join mon in Con.Monta_Servico on scp.IdMServico equals mon.IdMServico
                         join s in Con.Supplier on mon.S_id equals s.S_id
                         where ssv.Cid_Id == idCid &&
                               s.TIPOSUPL_id == 3 &&
                               ssv.Servicos_transfer == null ||
                               ssv.Cid_Id == idCid &&
                               s.TIPOSUPL_id == 4 &&
                               ssv.Servicos_transfer == null ||
                               ssv.Cid_Id == idCid &&
                               s.TIPOSUPL_id == 5 &&
                               ssv.Servicos_transfer == null
                         select s).ToList()
                        .Union(from ssv in Con.S_Servicos
                               join scp in Con.Servico_Completo on ssv.Servicos_Id equals scp.Servicos_Id
                               join mon in Con.Monta_Servico on scp.IdMServico_Temporada equals mon.IdMServico
                               join s in Con.Supplier on mon.S_id equals s.S_id
                               where ssv.Cid_Id == idCid &&
                                     s.TIPOSUPL_id == 3 &&
                                     ssv.Servicos_transfer == null ||
                                     ssv.Cid_Id == idCid &&
                                     s.TIPOSUPL_id == 4 &&
                                     ssv.Servicos_transfer == null ||
                                     ssv.Cid_Id == idCid &&
                                     s.TIPOSUPL_id == 5 &&
                                     ssv.Servicos_transfer == null
                               select s).ToList()).ToList();

            }
            catch (Exception)
            {

                throw;
            }
        }

        public object ListarTodos(DateTime dataIn, DateTime dataOut, int IdCidade, int IdSupplier)
        {
            try
            {

                if (IdSupplier > 0)
                {
                    return (from smt in Con.S_Mercado_Tarifa
                            join smp in Con.S_Mercado_Periodo on smt.S_merc_periodo_id equals smp.S_merc_periodo_id
                            join spl in Con.Supplier on smp.S_id equals spl.S_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join img in Con.S_Image on spl.S_id equals img.S_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                            where smp.S_merc_periodo_from <= (dataIn) &&
                                  smp.S_merc_periodo_to >= (dataOut) &&
                                  tpr.Tipo_room_id == 2 &&
                                  spl.CID_id == IdCidade &&
                                  spl.S_id == IdSupplier &&
                                  img.Image_capa.Equals("S")
                                  ||
                                  smp.S_merc_periodo_from <= (dataOut) &&
                                  smp.S_merc_periodo_to >= (dataIn) &&
                                  tpr.Tipo_room_id == 2 &&
                                  spl.CID_id == IdCidade &&
                                  spl.S_id == IdSupplier &&
                                  img.Image_capa.Equals("S")
                            orderby smp.S_merc_periodo_fdsFrom
                            select new
                            {
                                spl.S_id,
                                spl.S_nome,
                                spl.S_endereco,
                                spl.S_telefone,
                                tpr.Tipo_room_nome,
                                tcg.Tarif_categoria_nome,
                                tcg.Tarif_categoria_id,
                                smt.S_merc_tarif_tarifa_total,
                                smp.S_merc_periodo_from,
                                smp.S_merc_periodo_to,
                                smp.S_merc_periodo_fdsFrom,
                                smp.S_merc_periodo_fdsTo,
                                img.Image_nome,
                                mod.Moeda_sigla,
                                sme.S_mercado_estacao_nome
                            }).ToList();
                }
                else
                {
                    return (from smt in Con.S_Mercado_Tarifa
                            join smp in Con.S_Mercado_Periodo on smt.S_merc_periodo_id equals smp.S_merc_periodo_id
                            join spl in Con.Supplier on smp.S_id equals spl.S_id
                            join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                            join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                            join img in Con.S_Image on spl.S_id equals img.S_id
                            join mod in Con.Moeda on smp.Moeda_id equals mod.Moeda_id
                            join sme in Con.S_Mercado_Estacao on smp.S_mercado_estacao_id equals sme.S_mercado_estacao_id
                            where smp.S_merc_periodo_from <= (dataIn) &&
                                  smp.S_merc_periodo_to >= (dataOut) &&
                                  tpr.Tipo_room_id == 2 &&
                                  spl.CID_id == IdCidade &&
                                  img.Image_capa.Equals("S")
                                  ||
                                  smp.S_merc_periodo_from <= (dataOut) &&
                                  smp.S_merc_periodo_to >= (dataIn) &&
                                  tpr.Tipo_room_id == 2 &&
                                  spl.CID_id == IdCidade &&
                                  img.Image_capa.Equals("S")
                            orderby smp.S_merc_periodo_fdsFrom
                            select new
                            {
                                spl.S_id,
                                spl.S_nome,
                                spl.S_endereco,
                                spl.S_telefone,
                                tpr.Tipo_room_nome,
                                tcg.Tarif_categoria_nome,
                                tcg.Tarif_categoria_id,
                                smt.S_merc_tarif_tarifa_total,
                                smp.S_merc_periodo_from,
                                smp.S_merc_periodo_to,
                                smp.S_merc_periodo_fdsFrom,
                                smp.S_merc_periodo_fdsTo,
                                img.Image_nome,
                                mod.Moeda_sigla,
                                sme.S_mercado_estacao_nome
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> ListarTodosReport()
        {
            try
            {
                return Con.Supplier.Where(s => s.TIPOSUPL_id == 1).OrderBy(s => s.S_nome).ToList();

                //var query = (from spp in Con.Supplier
                //        join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                //        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                //        join tcg in Con.Tarif_Categoria on smt.Tarif_categoria_id equals tcg.Tarif_categoria_id
                //        join stt in Con.S_Mercado_Tipo_Tarifa on smt.S_mercado_tipo_tarifa_id equals stt.S_mercado_tipo_tarifa_id
                //        join tpr in Con.Tipo_Room on smt.Tipo_room_id equals tpr.Tipo_room_id
                //        join sms in Con.S_Mercado_Tarifa_Status on smt.S_merc_tarif_status_id equals sms.S_mercado_tarifa_status_id into jlf
                //        from m in jlf.DefaultIfEmpty()
                //        select new
                //        {
                //            spp.S_nome,
                //            smp.S_merc_periodo_from,
                //            smp.S_merc_periodo_to,
                //            m.S_mercado_tarifa_status_nome,
                //            smt.S_merc_tarif_tarifa_total,
                //            tcg.Tarif_categoria_nome,
                //            stt.S_mercado_tipo_tarifa_nome,
                //            tpr.Tipo_room_nome
                //        });

                //return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Método para Consultar todos os Supplier por status
        public List<Supplier> FindAllStatus(int IdStatus)
        {
            try
            {
                //select * from TbProduto where Nome like ?
                return Con.Supplier.Where(p => p.STATUS_id.Equals(IdStatus)).ToList();
                //Expressões LAMBDA
                /*
                 * StartsWith   -> Começa com
                 * EndsWith     -> Termina com
                 * Contains     -> Contendo
                 * Equals       -> Exatamente igual
                 */
            }
            catch
            {
                throw; //Lançar a exceção adiante...
            }
        }

        //Método para Consultar todos os Supplier por tipo
        public List<Supplier> FindAllTipo(int IdTipo)
        {
            try
            {
                //select * from TbProduto where Nome like ?
                return Con.Supplier.Where(p => p.TIPOSUPL_id.Equals(IdTipo)).ToList();
                //Expressões LAMBDA
                /*
                 * StartsWith   -> Começa com
                 * EndsWith     -> Termina com
                 * Contains     -> Contendo
                 * Equals       -> Exatamente igual
                 */
            }
            catch
            {
                throw; //Lançar a exceção adiante...
            }
        }

        public List<Supplier> FindAllTipo(string nome, int IdTipo)
        {
            try
            {

                return Con.Supplier.Where(p => p.TIPOSUPL_id == IdTipo &&
                                                p.S_nome.Contains(nome)).OrderBy(p => p.S_nome).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> FindAllTipo(string nome, int IdTipo, int IdCidade)
        {
            try
            {

                return Con.Supplier.Where(p => p.TIPOSUPL_id == IdTipo &&
                                                p.S_nome.Contains(nome) &&
                                                p.CID_id == IdCidade).OrderBy(p => p.S_nome).ToList();

            }
            catch
            {
                throw;
            }
        }

        //Método para Consultar todos os Supplier por tipo
        public Supplier FindAllNome(string nome)
        {
            try
            {
                //select * from TbProduto where Nome like ?
                return Con.Supplier.Where(p => p.S_nome.Contains(nome)).SingleOrDefault();
                //Expressões LAMBDA
                /*
                 * StartsWith   -> Começa com
                 * EndsWith     -> Termina com
                 * Contains     -> Contendo
                 * Equals       -> Exatamente igual
                 */
            }
            catch
            {
                throw; //Lançar a exceção adiante...
            }
        }

        public Supplier ObterSupplierPorNome(string nome)
        {
            try
            {

                return Con.Supplier.Where(p => p.S_nome.Equals(nome)).SingleOrDefault();
            }
            catch
            {
                throw; //Lançar a exceção adiante...
            }
        }

        public Supplier ObterSupplierPorNome(string nome, int IdCidade)
        {
            try
            {

                return Con.Supplier.Where(p => p.S_nome.Equals(nome) && p.CID_id == IdCidade).SingleOrDefault();
            }
            catch
            {
                throw; //Lançar a exceção adiante...
            }
        }

        public List<Supplier> ListarTodos(string nome)
        {
            try
            {
                return Con.Supplier.Where(t => t.S_nome.Contains(nome)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> ListarTodosMontaServ(string nome)
        {
            try
            {
                return Con.Supplier.Where(t => t.S_nome.Equals(nome)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> ListarTodos(string nome, int tiposupl)
        {
            try
            {
                return Con.Supplier.Where(t => t.S_nome.Contains(nome) && t.TIPOSUPL_id != tiposupl).ToList();
            }
            catch
            {
                throw;
            }
        }

        //Método para obter 1 Supplier pelo Id
        public Supplier ObterPorId(int IdSupplier)
        {
            try
            {
                //select * from TbCliente where IdCliente = ?
                return Con.Supplier.Where(c => c.S_id == IdSupplier).SingleOrDefault();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        public bool ObterPorNome(string nome)
        {
            try
            {
                return Con.Supplier.Where(s => s.S_nome.Equals(nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        //Método para atualizar os dados do Supplier
        public void Atualizar(Supplier registroNovo)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                Supplier registroAntigo = ObterPorId(registroNovo.S_id);

                //Atualizando os dados
                registroAntigo.S_nome = registroNovo.S_nome;
                registroAntigo.S_razaosocial = registroNovo.S_razaosocial;
                registroAntigo.S_cnpj = registroNovo.S_cnpj;
                registroAntigo.S_endereco = registroNovo.S_endereco;
                registroAntigo.S_teleplantao = registroNovo.S_teleplantao;
                registroAntigo.S_telefone = registroNovo.S_telefone;
                registroAntigo.S_endereco = registroNovo.S_endereco;
                registroAntigo.S_http = registroNovo.S_http;
                registroAntigo.S_email = registroNovo.S_email;
                registroAntigo.S_faturar = registroNovo.S_faturar;
                registroAntigo.S_reservas = registroNovo.S_reservas;
                registroAntigo.STATUS_id = registroNovo.STATUS_id;
                registroAntigo.TIPOSUPL_id = registroNovo.TIPOSUPL_id;
                registroAntigo.CID_id = registroNovo.CID_id;
                registroAntigo.PAIS_id = registroNovo.PAIS_id;
                registroAntigo.Categ_id = registroNovo.Categ_id;
                registroAntigo.Rede_id = registroNovo.Rede_id;
                registroAntigo.CategoriaEscrita_id = registroNovo.CategoriaEscrita_id;
                registroAntigo.Responsavel_id = registroNovo.Responsavel_id;
                registroAntigo.S_cep = registroNovo.S_cep;
                registroAntigo.S_fax = registroNovo.S_fax;
                registroAntigo.S_qtdpessoas = registroNovo.S_qtdpessoas;
                registroAntigo.S_pessoaOUapt = registroNovo.S_pessoaOUapt;
                registroAntigo.Recomended = registroNovo.Recomended;

                registroAntigo.Latitude = registroNovo.Latitude;
                registroAntigo.Longitude = registroNovo.Longitude;

                registroAntigo.ClassificacaoLodge_id = registroNovo.ClassificacaoLodge_id;

                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        //Método para atualizar os dados do Supplier
        public void Atualizar(Int32 SupplierID, Int32 categoria)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                Supplier registroAntigo = ObterPorId(SupplierID);

                //Atualizando os dados
                registroAntigo.Categ_id = categoria;

                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaAgregado(int IdCidade)
        {
            try
            {
                return Con.Supplier.Any(c => c.CID_id == IdCidade);
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExiste(string nome, int IdCidade, int IdSupplier)
        {
            try
            {
                if (IdSupplier > 0)
                {
                    return Con.Supplier.Where(s => s.S_nome.Equals(nome) && s.CID_id == IdCidade && s.S_id != IdSupplier).Count() != 0;
                }
                else
                {
                    return Con.Supplier.Where(s => s.S_nome.Equals(nome) && s.CID_id == IdCidade).Count() != 0;
                }
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExiste(string nome, int IdSupplier)
        {
            try
            {
                if (IdSupplier != 0)
                {
                    return Con.Supplier.Where(s => s.S_nome.Equals(nome) && s.S_id != IdSupplier).Count() != 0;
                }
                else
                {
                    return Con.Supplier.Where(s => s.S_nome.Equals(nome)).Count() != 0;
                }
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaLodge(int IdSupplier)
        {
            try
            {
                return Con.Supplier.Where(s => s.S_id == IdSupplier &&
                                                s.S_Categoria_Escrita.CategoriaEscrita_nome.Equals("Lodge")
                                                ||
                                                s.S_id == IdSupplier &&
                                                s.S_Categoria_Escrita.CategoriaEscrita_nome.Equals("Jungle Lodge")
                                                ||
                                                s.S_id == IdSupplier &&
                                                s.S_Categoria_Escrita.CategoriaEscrita_nome.Equals("Cruise")).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public Supplier RetornaPorMServicoValores(int IdMServico, int IdBase)
        {
            try
            {

                return (from mov in Con.Monta_Servico_Valores
                        join mon in Con.Monta_Servico on mov.IdMServico equals mon.IdMServico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        where mov.IdMServico == IdMServico &&
                              mov.Base_id == IdBase
                        select sup).Distinct().SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }


        public List<Supplier> ListarTodosGuias()
        {
            try
            {
                return Con.Supplier.Where(s => s.TipoSupl.TIPOSUPL_nome.Equals("Guias")).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> ListarHoteis()
        {
            try
            {
                return Con.Supplier.Where(s => s.TipoSupl.TIPOSUPL_nome.Equals("Hotel", StringComparison.OrdinalIgnoreCase)).OrderBy(s => s.S_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Supplier> ListarTodos_FatSup()
        {
            try
            {
                //                SELECT DISTINCT Supplier.S_nome
                //FROM            Supplier INNER JOIN
                //                         FaturaSupplier ON Supplier.S_id = FaturaSupplier.Supplier_id

                return (from spp in Con.Supplier
                        join fat in Con.FaturaSupplier on spp.S_id equals fat.Supplier_id
                        select spp).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }


        public List<string> ListarSupplier_Filtros(string prefixText, int idStatus, int idTipoSupplier, int TipoPesquisa, int idCidade)
        {
            try
            {

                List<Supplier> s = new List<Supplier>();

                if (TipoPesquisa == 0)
                {
                    if (idTipoSupplier > 0 && idStatus > 0 && idCidade > 0)
                    {
                        if (prefixText.Equals("*"))
                        {
                            s = Con.Supplier.Where(p => p.TIPOSUPL_id == idTipoSupplier
                                            && p.STATUS_id == idStatus
                                            && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                        }
                        else
                        {
                            s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText)
                                                && p.TIPOSUPL_id == idTipoSupplier
                                                && p.STATUS_id == idStatus
                                                && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                        }
                    }
                    else if (idTipoSupplier > 0 && idStatus > 0)
                    {
                        if (prefixText.Equals("*"))
                        {
                            s = Con.Supplier.Where(p => p.TIPOSUPL_id == idTipoSupplier && p.STATUS_id == idStatus).OrderBy(z => z.S_nome).ToList();
                        }
                        else
                        {
                            s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText) && p.TIPOSUPL_id == idTipoSupplier && p.STATUS_id == idStatus).OrderBy(z => z.S_nome).ToList();
                        }
                    }
                    else if (idTipoSupplier > 0 && idCidade > 0)
                    {
                        if (prefixText.Equals("*"))
                        {
                            s = Con.Supplier.Where(p => p.TIPOSUPL_id == idTipoSupplier && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                        }
                        else
                        {
                            s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText) && p.TIPOSUPL_id == idTipoSupplier && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                        }
                    }
                    else if (idStatus > 0 && idCidade > 0)
                    {
                        if (prefixText.Equals("*"))
                        {
                            s = Con.Supplier.Where(p => p.STATUS_id == idStatus && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                        }
                        else
                        {
                            s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText) && p.STATUS_id == idStatus && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                        }
                    }
                    else if (idTipoSupplier > 0)
                    {
                        if (prefixText.Equals("*"))
                        {
                            s = Con.Supplier.Where(p => p.TIPOSUPL_id == idTipoSupplier).OrderBy(z => z.S_nome).ToList();
                        }
                        else
                        {
                            s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText) && p.TIPOSUPL_id == idTipoSupplier).OrderBy(z => z.S_nome).ToList();
                        }
                    }
                    else if (idStatus > 0)
                    {
                        if (prefixText.Equals("*"))
                        {
                            s = Con.Supplier.Where(p => p.STATUS_id == idStatus).OrderBy(z => z.S_nome).ToList();
                        }
                        else
                        {
                            s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText) && p.STATUS_id == idStatus).OrderBy(z => z.S_nome).ToList();
                        }
                    }
                    else if (idCidade > 0)
                    {
                        if (prefixText.Equals("*"))
                        {
                            s = Con.Supplier.Where(p => p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                        }
                        else
                        {
                            s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText) && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                        }
                    }
                    else
                    {
                        if (prefixText.Equals("*"))
                        {
                            s = Con.Supplier.OrderBy(z => z.S_nome).ToList();
                        }
                        else
                        {
                            s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText)).OrderBy(z => z.S_nome).ToList();
                        }
                    }
                }
                else
                {
                    if (idTipoSupplier > 0 && idStatus > 0 && idCidade > 0)
                    {
                        s = Con.Supplier.Where(p => p.S_endereco.Contains(prefixText)
                                            && p.TIPOSUPL_id == idTipoSupplier
                                            && p.STATUS_id == idStatus
                                            && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                    }
                    else if (idTipoSupplier > 0 && idStatus > 0)
                    {
                        s = Con.Supplier.Where(p => p.S_endereco.Contains(prefixText) && p.TIPOSUPL_id == idTipoSupplier && p.STATUS_id == idStatus).OrderBy(z => z.S_nome).ToList();
                    }
                    else if (idTipoSupplier > 0 && idCidade > 0)
                    {
                        s = Con.Supplier.Where(p => p.S_endereco.Contains(prefixText) && p.TIPOSUPL_id == idTipoSupplier && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                    }
                    else if (idStatus > 0 && idCidade > 0)
                    {
                        s = Con.Supplier.Where(p => p.S_nome.Contains(prefixText) && p.STATUS_id == idStatus && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                    }
                    else if (idTipoSupplier > 0)
                    {
                        s = Con.Supplier.Where(p => p.S_endereco.Contains(prefixText) && p.TIPOSUPL_id == idTipoSupplier).OrderBy(z => z.S_nome).ToList();
                    }
                    else if (idStatus > 0)
                    {
                        s = Con.Supplier.Where(p => p.S_endereco.Contains(prefixText) && p.STATUS_id == idStatus).OrderBy(z => z.S_nome).ToList();
                    }
                    else if (idCidade > 0)
                    {
                        s = Con.Supplier.Where(p => p.S_endereco.Contains(prefixText) && p.CID_id == idCidade).OrderBy(z => z.S_nome).ToList();
                    }
                    else
                    {
                        s = Con.Supplier.Where(p => p.S_endereco.Contains(prefixText)).OrderBy(z => z.S_nome).ToList();
                    }
                }


                List<string> lista = new List<string>();

                if (s.Count == 0)
                {
                    lista.Add("Não existe supplier cadastrado com esse nome.");

                    return lista;
                }

                foreach (Supplier p in s)
                {
                    lista.Add(p.S_nome + " ° " + p.Cidade.CID_nome);
                }

                return lista;

            }
            catch 
            {
                throw;
            }
        }


        public List<string> ListarResponsaveis(int sid)
        {
            try
            {
                //SELECT DISTINCT Supplier_1.S_nome
                //FROM            Supplier INNER JOIN
                //S_Mercado_Periodo ON Supplier.S_id = S_Mercado_Periodo.S_id INNER JOIN
                //Supplier AS Supplier_1 ON S_Mercado_Periodo.Responsavel_id = Supplier_1.S_id
                //WHERE(Supplier.S_id <> Supplier.Responsavel_id) AND(Supplier.S_id = 404143)

                return (from spp in Con.Supplier
                        join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                        join spr in Con.Supplier on smp.Responsavel_id equals spr.S_id
                        where spp.S_id == sid
                        select spr.S_nome).Distinct().ToList();

            }
            catch 
            {
                throw;
            }
        }


    }
}
