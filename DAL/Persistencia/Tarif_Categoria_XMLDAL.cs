﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Models;

namespace DAL.Persistencia
{
    public class Tarif_Categoria_XMLDAL
    {
        private Model Con;

        public Tarif_Categoria_XMLDAL()
        {
            Con = new Model();
        }

        public Tarif_Categoria_XML Obter(int ID)
        {
            return Con.Tarif_Categoria_XML.Where(a => a.Id == ID).SingleOrDefault();
        }

        public Tarif_Categoria_XML ObterPorTipoRoomCategoria(string tipoRoom, string categoria)
        {
            int trID = Con.Tipo_Room.Where(a => a.Tipo_room_nome == tipoRoom).Single().Tipo_room_id;
            int cID = Con.Tarif_Categoria.Where(a => a.Tarif_categoria_nome == categoria).Single().Tarif_categoria_id;

            return Con.Tarif_Categoria_XML.Where(a => a.Id_TipoRoom == trID && a.Id_TarifCategoria == cID).SingleOrDefault();
        }

        public Tarif_Categoria_XML ObterPorCategoriaIDPlataforma(int catID, int plataformaID)
        {
            return Con.Tarif_Categoria_XML.Where(a => a.Plataforma_Id == plataformaID && a.Plataforma_CatId == catID).SingleOrDefault();
        }

        public void UpdateProsoftwareTarifCategoriaXML(int code, string roomName, string ageQualifyCode, string roomDescription, int maxOccupancy, int minOccupancy)
        {
            try
            {
                if (Con.Tarif_Categoria_XML.Count(a => a.Plataforma_CatId == code) <= 0)
                {
                    Tarif_Categoria_XML tcx = new Tarif_Categoria_XML();
                    tcx.Descricao = roomName;
                    tcx.Plataforma_Id = Con.Plataforma_XML.Where(a => a.Plataforma_nome == "Omnibees").Single().Plataforma_id;
                    tcx.Plataforma_CatId = code;
                    tcx.AgeQualifyCode = ageQualifyCode;
                    tcx.Description = roomDescription;
                    tcx.MaxOccupancy = maxOccupancy;
                    tcx.MinOccupancy = minOccupancy;

                    Con.Tarif_Categoria_XML.Add(tcx);
                    Con.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Tarif_Categoria_XML ObterPorPlataformaCategoria(int idPlataforma, int idCategoria)
        {
            try
            {
                return Con.Tarif_Categoria_XML.Where(s => s.Plataforma_Id == idPlataforma &&
                                                          s.Id_TarifCategoria == idCategoria).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Tarif_Categoria_XML ObterPorPlataformaCategoria_OMNI(int idPlataforma, int idCategoria)
        {
            try
            {
                return Con.Tarif_Categoria_XML.Where(s => s.Plataforma_Id == idPlataforma &&
                                                          s.Plataforma_CatId == idCategoria).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Tarif_Categoria_XML ObterPorPlataformaCategoria(int idPlataforma, int idCategoria, int idSupplier)
        {
            try
            {
                return Con.Tarif_Categoria_XML.Where(s => s.Plataforma_Id == idPlataforma &&
                                                          s.Id_TarifCategoria == idCategoria &&
                                                          s.Supplier_XML.SupplierId_Prosoftware == idSupplier).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Tarif_Categoria_XML ObterPorPlataformaCategoria(int idPlataforma, int idCategoria, int idSupplier, int idCategoriaPlataforma)
        {
            try
            {
                return Con.Tarif_Categoria_XML.Where(s => s.Plataforma_Id == idPlataforma &&
                                                          s.Id_TarifCategoria == idCategoria &&
                                                          s.Plataforma_CatId == idCategoriaPlataforma &&
                                                          s.Supplier_XML.SupplierId_Prosoftware == idSupplier).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Tarif_Categoria_XML> ObterPorPlataformaCategoria_List(int idPlataforma, int idCategoria)
        {
            try
            {
                return Con.Tarif_Categoria_XML.Where(s => s.Plataforma_Id == idPlataforma &&
                                                          s.Id_TarifCategoria == idCategoria).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Tarif_Categoria_XML> ObterPorPlataformaCategoria_List(int idPlataforma, int idCategoria, int idSupplier)
        {
            try
            {
                return Con.Tarif_Categoria_XML.Where(s => s.Plataforma_Id == idPlataforma &&
                                                          s.Id_TarifCategoria == idCategoria &&
                                                          s.Supplier_XML.SupplierId_Prosoftware == idSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public class ItensOmniReserva
        {
            public int idCategoriaOmni { get; set; }
            public string idHotelOmni { get; set; }
        }

        public ItensOmniReserva ObterItensAPI(int S_id, int idCategoria, int idPlat)
        {
            try
            {

                //SELECT Supplier.S_nome, Tarif_Categoria_XML.Id_TipoRoom, Tarif_Categoria_XML.Id_TarifCategoria, Tarif_Categoria_XML.Plataforma_Id, 
                //Tarif_Categoria_XML.Plataforma_CatId, 
                //Supplier_XML.SupplerId_Plataforma
                //FROM            Tarif_Categoria_XML INNER JOIN
                //Supplier_XML ON Tarif_Categoria_XML.S_XMLId = Supplier_XML.Id INNER JOIN
                //Supplier ON Supplier_XML.SupplierId_Prosoftware = Supplier.S_id
                //WHERE(Supplier_XML.SupplierId_Prosoftware = 1698) AND(Tarif_Categoria_XML.Id_TarifCategoria = 106) AND(Tarif_Categoria_XML.Plataforma_Id = 1)

                return (from trf in Con.Tarif_Categoria_XML
                        join spx in Con.Supplier_XML on trf.S_XMLId equals spx.Id
                        join spp in Con.Supplier on spx.SupplierId_Prosoftware equals spp.S_id
                        where spx.SupplierId_Prosoftware == S_id &&
                              trf.Id_TarifCategoria == idCategoria &&
                              trf.Plataforma_Id == idPlat
                        select new ItensOmniReserva
                        {
                            idCategoriaOmni = trf.Plataforma_CatId,
                            idHotelOmni = spx.SupplerId_Plataforma
                        }).OrderByDescending(s => s.idCategoriaOmni).First();
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(Tarif_Categoria_XML p)
        {
            try
            {
                Con.Tarif_Categoria_XML.Add(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Tarif_Categoria_XML p)
        {
            try
            {
                Con.Tarif_Categoria_XML.Remove(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Tarif_Categoria_XML novo)
        {
            try
            {
                Tarif_Categoria_XML antigo = Obter(novo.Id);

                antigo.Plataforma_Id = novo.Plataforma_Id;
                antigo.Id_TarifCategoria = novo.Id_TarifCategoria;
                antigo.Plataforma_CatId = novo.Plataforma_CatId;
                antigo.Descricao = novo.Descricao;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Tarif_Categoria_XML> ListarTodos()
        {
            try
            {
                return Con.Tarif_Categoria_XML.ToList();
            }
            catch
            {
                throw;
            }
        }


        public List<TarifCategoriaXmlModel> ObterTodos()
        {
            try
            {

                return (from pxm in Con.Tarif_Categoria_XML
                        join pas in Con.Tarif_Categoria on pxm.Id_TarifCategoria equals pas.Tarif_categoria_id
                        join pla in Con.Plataforma_XML on pxm.Plataforma_Id equals pla.Plataforma_id
                        join spp in Con.Supplier_XML on pxm.S_XMLId equals spp.Id
                        select new TarifCategoriaXmlModel()
                        {
                            CategoriaNome = pas.Tarif_categoria_nome,
                            Plataforma = pla.Plataforma_nome,
                            IdPlataforma = pxm.Plataforma_CatId,
                            IdTierra = pxm.Id_TarifCategoria.Value,
                            Id = pxm.Id,
                            HotelNome = spp.Supplier.S_nome
                        }).OrderBy(x=> x.HotelNome).ToList();

            }
            catch
            {
                throw;
            }
        }


        public object ObterTodos(int idHotel)
        {
            try
            {

                return (from pxm in Con.Tarif_Categoria_XML
                        join pas in Con.Tarif_Categoria on pxm.Id_TarifCategoria equals pas.Tarif_categoria_id
                        join pla in Con.Plataforma_XML on pxm.Plataforma_Id equals pla.Plataforma_id
                        join spp in Con.Supplier_XML on pxm.S_XMLId equals spp.Id
                        where spp.SupplierId_Prosoftware == idHotel
                        select new
                        {
                            pas.Tarif_categoria_nome,
                            pla.Plataforma_nome,
                            pxm.Plataforma_CatId,
                            pxm.Id_TarifCategoria,
                            pxm.Id,
                            Hotel = spp.Supplier.S_nome
                        }).ToList();

            }
            catch
            {
                throw;
            }
        }

    }
}
