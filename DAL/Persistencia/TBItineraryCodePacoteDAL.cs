﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class TBItineraryCodePacoteDAL
    {

        private Model Con;

        public TBItineraryCodePacoteDAL()
        {
            Con = new Model();
        }

        public TBItinerary_CodePacote obterpacote(int idTabela, int optQuote, string Quotation_Code)
        {
            try
            {
                return Con.TBItinerary_CodePacote.Where(s => s.idTabela == idTabela && s.optQuote == optQuote && s.Quotation_Code.Equals(Quotation_Code)).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(TBItinerary_CodePacote m)
        {
            try
            {
                Con.TBItinerary_CodePacote.Add(m);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(TBItinerary_CodePacote novo)
        {
            try
            {
                var antigo = obterpacote(novo.idTabela, novo.optQuote, novo.Quotation_Code);
                antigo.TBItinerary_codePacote = novo.TBItinerary_codePacote;
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary_CodePacote> ObterPorCode(string Quotation_Code, int optQuote)
        {
            try
            {
                return Con.TBItinerary_CodePacote.Where(s => s.optQuote == optQuote && s.Quotation_Code.Equals(Quotation_Code)).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
