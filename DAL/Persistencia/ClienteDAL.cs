﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using System.Data.SqlClient;

namespace DAL.Persistencia
{
    public class ClienteDAL
    {

        Model Con;

        public ClienteDAL()
        {
            Con = new Model();
        }

        public void Salvar(Cliente c)
        {
            try
            {
                Con.Cliente.Add(c);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Cliente c)
        {
            try
            {
                Con.Cliente.Remove(c);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Cliente ObterPorId(int IdCliente)
        {
            try
            {
                return Con.Cliente.Where(c => c.Cliente_id == IdCliente).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Cliente novo)
        {
            Cliente antigo = ObterPorId(novo.Cliente_id);

            antigo.Cliente_nome = novo.Cliente_nome;
            antigo.Cliente_endereco = novo.Cliente_endereco;
            antigo.Cliente_http = novo.Cliente_http;
            antigo.Cliente_email = novo.Cliente_email;
            antigo.Cliente_cep = novo.Cliente_cep;
            antigo.Cliente_telefone = novo.Cliente_telefone;
            antigo.Cliente_fax = novo.Cliente_fax;
            antigo.Cliente_since = novo.Cliente_since;
            antigo.Representante_id = novo.Representante_id;
            antigo.Mercado_id = novo.Mercado_id;
            antigo.BaseTarifaria_id = novo.BaseTarifaria_id;
            antigo.CentroCusto_id = novo.CentroCusto_id;
            antigo.PAIS_id = novo.PAIS_id;
            antigo.CID_id = novo.CID_id;

            Con.SaveChanges();
        }

        public Cliente ObterPorNome(string nome)
        {
            try
            {
                var query = (from c in Con.Cliente
                             join m in Con.Mercado on c.Mercado_id equals m.Mercado_id
                             join b in Con.BaseTarifaria on c.BaseTarifaria_id equals b.BaseTarifaria_id
                             join cc in Con.Centro_Custo on c.CentroCusto_id equals cc.CentroCusto_id
                             where c.Cliente_nome.Equals(nome)
                             select c).SingleOrDefault();

                return query;
            }
            catch
            {
                throw;
            }
        }

        public Cliente ObterPorNome_(string nome)
        {
            try
            {
                return Con.Cliente.Where(s => s.Cliente_nome.Equals(nome)).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaRepresentante(int IdCliente)
        {
            try
            {
                return Con.Cliente.Where(c => c.Representante_id == IdCliente).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public List<Cliente> ListarTodos()
        {
            try
            {
                return Con.Cliente.OrderBy(c => c.Cliente_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Cliente> ListarTodos(string nome)
        {
            try
            {
                return Con.Cliente.Where(c => c.Cliente_nome.Contains(nome)).OrderBy(c => c.Cliente_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Cliente> ListarPorUsuario(int IdUsuario)
        {
            try
            {
                List<Cliente> lstCli = new List<Cliente>();

                return Con.Cliente.Where(a => a.US_id == IdUsuario).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int[] ObterMercado(Cliente c)
        {
            try
            {
                var query = (from cl in Con.Cliente
                             where cl.Cliente_id == c.Cliente_id
                             select new
                             {
                                 cl.Mercado_id,
                                 cl.BaseTarifaria_id
                             }).SingleOrDefault();

                int[] t = new int[2];
                t[0] = Convert.ToInt32(query.Mercado_id);
                t[1] = Convert.ToInt32(query.BaseTarifaria_id);
                return t;
            }
            catch
            {
                throw;
            }
        }

        public int QtdeFiles(int IdCliente)
        {
            try
            {
                return (from qgp in Con.Quotation_Grupo
                        join quo in Con.Quotation on qgp.Quotation_Id equals quo.Quotation_Id
                        join fca in Con.File_Carrinho on qgp.Quotation_Grupo_Id equals fca.Quotation_Grupo_Id
                        where qgp.Cliente_id == IdCliente
                        select quo.Quotation_Code).Distinct().Count();
            }
            catch
            {
                throw;
            }
        }

        public List<Cliente> ListarTodos_File()
        {
            try
            {

                SqlCon Consql = new SqlCon();

                Consql.AbrirConexao();

                string sql = "";
                sql = sql + "SELECT DISTINCT Cliente_nome, ID ";
                sql = sql + "FROM (SELECT DISTINCT Cliente.Cliente_nome, Cliente.Cliente_id AS ID ";
                sql = sql + "FROM Quotation_Grupo INNER JOIN ";
                sql = sql + "Cliente ON Quotation_Grupo.Cliente_id = Cliente.Cliente_id INNER JOIN ";
                sql = sql + "File_Carrinho ON Quotation_Grupo.Quotation_Grupo_Id = File_Carrinho.Quotation_Grupo_Id INNER JOIN ";
                sql = sql + "Quote_Tarifas ON File_Carrinho.File_id = Quote_Tarifas.File_id WHERE (Quote_Tarifas.Status = 'OK') ";
                sql = sql + "UNION ALL ";
                sql = sql + "SELECT DISTINCT Cliente_1.Cliente_nome, Cliente_1.Cliente_id AS ID ";
                sql = sql + "FROM            Quotation_Grupo AS Quotation_Grupo_1 INNER JOIN ";
                sql = sql + "Cliente AS Cliente_1 ON Quotation_Grupo_1.Cliente_id = Cliente_1.Cliente_id INNER JOIN ";
                sql = sql + "File_Carrinho AS File_Carrinho_1 ON Quotation_Grupo_1.Quotation_Grupo_Id = File_Carrinho_1.Quotation_Grupo_Id INNER JOIN ";
                sql = sql + "Quote_Transf ON File_Carrinho_1.File_id = Quote_Transf.File_id WHERE        (Quote_Transf.Status = 'OK') ";
                sql = sql + "UNION ALL ";
                sql = sql + "SELECT DISTINCT Cliente_2.Cliente_nome, Cliente_2.Cliente_id AS ID ";
                sql = sql + "FROM            Quotation_Grupo AS Quotation_Grupo_2 INNER JOIN ";
                sql = sql + "Cliente AS Cliente_2 ON Quotation_Grupo_2.Cliente_id = Cliente_2.Cliente_id INNER JOIN ";
                sql = sql + "File_Carrinho AS File_Carrinho_2 ON Quotation_Grupo_2.Quotation_Grupo_Id = File_Carrinho_2.Quotation_Grupo_Id INNER JOIN ";
                sql = sql + "Quote_ServExtra ON File_Carrinho_2.File_id = Quote_ServExtra.File_id WHERE        (Quote_ServExtra.Status = 'OK')) AS derivedtbl_1 ORDER BY ID ";


                Consql.Cmd = new SqlCommand(sql, Consql.Con);               
                Consql.Dr = Consql.Cmd.ExecuteReader();

                List<Cliente> lst = new List<Cliente>();

                while (Consql.Dr.Read())
                {
                    lst.Add(new Cliente { Cliente_id = Convert.ToInt32(Consql.Dr["ID"]), Cliente_nome = Consql.Dr["Cliente_nome"].ToString() });        
                }

                return lst;
            }
            catch 
            {
                throw;
            }
        }

    }
}
