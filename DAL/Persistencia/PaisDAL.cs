﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class PaisDAL
    {

        private Model Con;

        public PaisDAL()
        {
            Con = new Model();
        }

        public void Salvar(Pais p)
        {
            try
            {
                Con.Pais.Add(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Pais ObterPorId(int IdPais)
        {
            try
            {
                return Con.Pais.Where(p => p.PAIS_id == IdPais).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Pais p)
        {
            try
            {
                Con.Pais.Remove(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Pais novo)
        {
            try
            {
                Pais antigo = ObterPorId(novo.PAIS_id);

                antigo.PAIS_nome = novo.PAIS_nome;
                antigo.PAIS_uf = novo.PAIS_uf;
                antigo.PAIS_mercosul = novo.PAIS_mercosul;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Pais> ListarTodos()
        {
            try
            {
                return Con.Pais.OrderBy(p => p.PAIS_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Pais> ListarTodosComSupplier()
        {
            try
            {
                return (from pais in Con.Pais
                        join spp in Con.Supplier on pais.PAIS_id equals spp.PAIS_id
                        select pais).Distinct().OrderBy(s => s.PAIS_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Pais> ListarTodosComServicos()
        {
            try
            {
                return (from pais in Con.Pais
                        join spp in Con.S_Servicos on pais.PAIS_id equals spp.Pais_Id
                        select pais).Distinct().OrderBy(s => s.PAIS_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Pais> ListarTodos(string nome)
        {
            try
            {
                if (nome.Equals(""))
                {
                    return Con.Pais.OrderBy(p => p.PAIS_nome).OrderBy(p => p.PAIS_nome).ToList();
                }
                else
                {
                    return Con.Pais.Where(p => p.PAIS_nome.Contains(nome)).OrderBy(p => p.PAIS_nome).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public bool ObterPorNome(string nome)
        {
            try
            {
                return Con.Pais.Where(s => s.PAIS_nome.Equals(nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public Pais ObterPorNome_(string nome)
        {
            try
            {
                return Con.Pais.Where(s => s.PAIS_nome.Equals(nome)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Pais ObterPorCidade(string cidade)
        {
            try
            {
                return (from cid in Con.Cidade
                        join pai in Con.Pais on cid.PAIS_id equals pai.PAIS_id
                        where cid.CID_nome.Equals(cidade)
                        select pai).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
