﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class CidadeXMLDAL
    {

        private Model Con;

        public CidadeXMLDAL()
        {
            Con = new Model();
        }


        public void Salvar(Cidade_XML p)
        {
            try
            {
                Con.Cidade_XML.Add(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Cidade_XML ObterPorId(int id)
        {
            try
            {
                return Con.Cidade_XML.Where(p => p.id == id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Cidade_XML ObterPorId_Sistema(int id)
        {
            try
            {
                return Con.Cidade_XML.Where(p => p.Cid_id_Prosoftware == id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Cidade_XML p)
        {
            try
            {
                Con.Cidade_XML.Remove(p);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Cidade_XML novo)
        {
            try
            {
                Cidade_XML antigo = ObterPorId(novo.id);

                antigo.id_Plataforma = novo.id_Plataforma;
                antigo.Cid_id_Plataforma = novo.Cid_id_Plataforma;
                antigo.Cid_id_Prosoftware = novo.Cid_id_Prosoftware;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Cidade_XML> ListarTodos()
        {
            try
            {
                return Con.Cidade_XML.ToList();
            }
            catch
            {
                throw;
            }
        }


        public object ObterTodos()
        {
            try
            {

                return (from pxm in Con.Cidade_XML
                        join pas in Con.Cidade on pxm.Cid_id_Prosoftware equals pas.CID_id
                        join pla in Con.Plataforma_XML on pxm.id_Plataforma equals pla.Plataforma_id
                        select new
                        {
                            pas.CID_nome,
                            pla.Plataforma_nome,
                            pxm.Cid_id_Plataforma,
                            pxm.Cid_id_Prosoftware,
                            pxm.id
                        }).ToList();

            }
            catch
            {
                throw;
            }
        }

    }
}
