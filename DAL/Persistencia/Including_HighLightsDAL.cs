﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class Including_HighLightsDAL
    {
        private Model Con;

        public Including_HighLightsDAL()
        {
            Con = new Model();
        }

        public Including_HighLights Obter(int ID)
        {
            try
            {
                return Con.Including_HighLights.Where(m => m.ID == ID).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }
        public Including_HighLights ObterPorDescQuot(string desc, string quotationcode, int optQuote)
        {
            try
            {
                return Con.Including_HighLights.Where(m => m.Descricao == desc && m.Quotation_Code == quotationcode && m.optQuote == optQuote).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }
        public List<Including_HighLights> ObterPorCode(string quotationcode, int optQuote)
        {
            try
            {
                return Con.Including_HighLights.Where(m => m.Quotation_Code == quotationcode && m.optQuote == optQuote).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Including_HighLights> ListarPQuotation(string quotationcode, int optQuote)
        {
            try
            {
                return Con.Including_HighLights.Where(m => m.Quotation_Code == quotationcode && m.Visible == true && m.optQuote == optQuote).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void InsertDefault(string _quotationCode, int optQuote)
        {
            try
            {
                List<String> listValues = new List<string>();
                listValues.Add("Daily Breakfast & Additional Meals as Listed");
                listValues.Add("24/7 Phone Support in Each Country");
                listValues.Add("Tours in Private or Group (as noted in itinerary)");

                foreach (string item in listValues)
                {
                    Including_HighLights i = new Including_HighLights();
                    i.Visible = true;
                    i.Quotation_Code = _quotationCode;
                    i.optQuote = optQuote;
                    i.Descricao = item;

                    Salvar(i);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void InsertDefault_New(string _quotationCode, int optQuote, int days)
        {
            try
            {
                List<String> listValues = new List<string>();
                listValues.Add(string.Format("{0} days / {1} nights", days, (days - 1)));
                listValues.Add("Transfers in private with English-speaking guide (unless noted)");
                listValues.Add("Daily Breakfast & Additional Meals as Listed");
                listValues.Add("24/7 Phone Support in Each Country");
                listValues.Add("Tours in Private or Group (as noted in itinerary)");

                foreach (string item in listValues)
                {
                    Including_HighLights i = new Including_HighLights();
                    i.Visible = true;
                    i.Quotation_Code = _quotationCode;
                    i.optQuote = optQuote;
                    i.Descricao = item;

                    Salvar(i);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Salvar(Including_HighLights i)
        {
            try
            {
                Con.Including_HighLights.Add(i);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Including_HighLights i)
        {
            try
            {
                Con.Including_HighLights.Remove(i);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void ExcluirTodosCode(string code, int optQuote)
        {
            var lst = Con.Including_HighLights.Where(a => a.Quotation_Code == code && a.optQuote == optQuote).ToList();

            foreach (var item in lst)
            {
                Con.Including_HighLights.Remove(item);
            }

            Con.SaveChanges();
        }

        public void Edit(Including_HighLights i)
        {
            try
            {
                var incluid_Old = Obter(i.ID);
                incluid_Old.Descricao = i.Descricao;
                Con.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
