﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class QGStatusDAL
    {
        private Model Con;

        public QGStatusDAL()
        {
            Con = new Model();
        }

        public Quotation_Status ObterPorId(int IdQStatus)
        {
            try
            {
                return Con.Quotation_Status.Where(m => m.Quotation_status_id == IdQStatus).SingleOrDefault();   
            }
            catch 
            {
                throw;
            }

        }

        public void Salvar(Quotation_Status b)
        {
            try
            {
                Con.Quotation_Status.Add(b);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Quotation_Status b)
        {
            try
            {
                Con.Quotation_Status.Remove(b);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Quotation_Status novo)
        {
            try
            {
                Quotation_Status antigo = ObterPorId(novo.Quotation_status_id);

                antigo.Quotation_status_nome = novo.Quotation_status_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Status> ListarTodos()
        {
            try
            {
                return Con.Quotation_Status.ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool ObterPorNome(string nome)
        {
            try
            {
                return Con.Quotation_Status.Where(s => s.Quotation_status_nome.Equals(nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public Quotation_Status ObterPorNomeQS(string nome)
        {
            try
            {
                return Con.Quotation_Status.Where(s => s.Quotation_status_nome.Equals(nome)).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
