﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class SMercadoEstacaoDAL
    {
        private Model Con;

        public SMercadoEstacaoDAL()
        {
            Con = new Model();
        }

        public List<S_Mercado_Estacao> ListarTodos()
        {
            try
            {
                return Con.S_Mercado_Estacao.ToList();
            }
            catch 
            {
                throw;
            }
        }

        public S_Mercado_Estacao ObterPorNome(string temporada)
        {
            try
            {
                return Con.S_Mercado_Estacao.Where(s => s.S_mercado_estacao_nome.Equals(temporada)).FirstOrDefault();
            }
            catch 
            {
                throw;
            }
        }
    }
}
