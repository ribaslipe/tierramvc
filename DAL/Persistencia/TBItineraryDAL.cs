﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DAL.Entity;
using DAL.Persistencia;
using System.Data;
using DAL.Models;

namespace DAL.Persistencia
{
    public class TBItineraryDAL
    {
        private Model Con;

        public TBItineraryDAL()
        {
            Con = new Model();
        }

        public void Salvar(TBItinerary t)
        {
            try
            {
                Con.TBItinerary.Add(t);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosCode(string Code)
        {
            try
            {
                //return Con.TBItinerary.Where(t => t.Code.Equals(Code)).OrderBy(t => t.DateFrom).ToList();

                return (from tbi in Con.TBItinerary
                        where tbi.Code.Equals(Code)
                        orderby tbi.DateFrom, tbi.Ordem
                        select tbi).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosCodeOpt(string Code, int optQuote)
        {
            try
            {
                //return Con.TBItinerary.Where(t => t.Code.Equals(Code)).OrderBy(t => t.DateFrom).ToList();

                return (from tbi in Con.TBItinerary
                        where tbi.Code.Equals(Code) &&
                              tbi.OptQuote == optQuote
                        orderby tbi.DateFrom, tbi.Ordem
                        select tbi).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosCode(string Code, int Ordem)
        {
            try
            {
                //return Con.TBItinerary.Where(t => t.Code.Equals(Code)).OrderBy(t => t.DateFrom).ToList();

                return (from tbi in Con.TBItinerary
                        where tbi.Code.Equals(Code)
                        orderby tbi.DateFrom, tbi.Ordem
                        select tbi).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosHOTEL(string Code)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Code.Equals(Code) && t.Flag.Equals("HOTEL") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("MEAL")).OrderBy(t => t.DateFrom).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosHOTELDistinct(string Code)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Code.Equals(Code) && t.Flag.Equals("HOTEL") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("MEAL")).OrderBy(t => t.DateFrom).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosTRANSFER(string Code)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Code.Equals(Code) && t.Flag.Equals("TRANSFER") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("SubServ")).OrderBy(t => t.DateFrom).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosTOUR(string Code)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Code.Equals(Code) && t.Flag.Equals("TOUR")).OrderBy(t => t.DateFrom).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosEXTRA(string Code)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Code.Equals(Code) && t.Flag.Equals("EXTRA")).OrderBy(t => t.DateFrom).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosSERVICOS(string Code)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Code.Equals(Code) && t.Flag.Equals("TRANSFER") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("SubServ") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("TOUR") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("EXTRA"))
                                                  .OrderBy(t => t.DateFrom).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosSERVICOS_Cidade(string Code, string Cidade)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Cidade.Equals(Cidade) && t.Code.Equals(Code) && t.Flag.Equals("TRANSFER") ||
                                                  t.Cidade.Equals(Cidade) && t.Code.Equals(Code) && t.Flag.Equals("SubServ") ||
                                                  t.Cidade.Equals(Cidade) && t.Code.Equals(Code) && t.Flag.Equals("TOUR") ||
                                                  t.Cidade.Equals(Cidade) && t.Code.Equals(Code) && t.Flag.Equals("EXTRA"))
                                                  .OrderBy(t => t.DateFrom).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosSERVICOS_CidadeSemTransfer(string Code, string Cidade)
        {
            try
            {
                List<TBItinerary> lstit = Con.TBItinerary.Where(t =>
                                                t.Cidade.Equals(Cidade) &&
                                                t.Code.Equals(Code) &&
                                                (t.Flag.Equals("TOUR") || t.Flag.Equals("EXTRA") || t.Flag.Equals("HOTEL")))
                                                //!t.Flag.Equals("SubServ"))
                                                .OrderBy(t => t.DateFrom).Distinct().ToList();

                foreach (var itemHT in lstit)
                {
                    if (itemHT.Flag.Equals("HOTEL"))
                    {
                        var segment = new FileTarifasDAL().ObterPorId((int)itemHT.idTabela);
                        if (segment.NomePacote == null)
                        {
                            lstit = lstit.Where(s => s.idTabela != itemHT.idTabela).ToList();
                        }
                    }
                }

                foreach (var item in Con.TBItinerary.Where(a =>
                                        a.Cidade.Equals(Cidade) &&
                                        a.Code.Equals(Code) &&
                                        a.Flag.Equals("SubServ")).ToList())
                {
                    if (Con.File_Transfers.Count(b => b.File_Transf_id == item.idTabela && b.Trf_Tours.Equals("TRS")) > 0)
                    {
                        lstit.Add(item);
                    }
                }

                return lstit;
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ListarTodosSERVICOS_CidadeSemTransfer(string Code, string Cidade, int optquote)
        {
            try
            {
                List<TBItinerary> lstit = Con.TBItinerary.Where(t =>
                                                t.Cidade.Equals(Cidade) &&
                                                t.Code.Equals(Code) &&
                                                t.OptQuote == optquote &&
                                                (t.Flag.Equals("TOUR") || t.Flag.Equals("EXTRA") || t.Flag.Equals("HOTEL")))
                                                //!t.Flag.Equals("SubServ"))
                                                .OrderBy(t => t.DateFrom).Distinct().ToList();

                foreach (var itemHT in lstit)
                {
                    if (itemHT.Flag.Equals("HOTEL"))
                    {
                        var segment = new FileTarifasDAL().ObterPorId((int)itemHT.idTabela);
                        if (segment.NomePacote == null)
                        {
                            lstit = lstit.Where(s => s.idTabela != itemHT.idTabela).ToList();
                        }
                    }
                }

                foreach (var item in Con.TBItinerary.Where(a =>
                                        a.Cidade.Equals(Cidade) &&
                                        a.Code.Equals(Code) &&
                                        a.OptQuote == optquote &&
                                        a.Flag.Equals("SubServ")).ToList())
                {
                    if (Con.File_Transfers.Count(b => b.File_Transf_id == item.idTabela && b.Trf_Tours.Equals("TRS")) > 0)
                    {
                        lstit.Add(item);
                    }
                }

                return lstit;
            }
            catch
            {
                throw;
            }
        }

        public DataTable ListarTodosCidades(string Code)
        {
            try
            {
                var query = Con.TBItinerary.Where(t => t.Code.Equals(Code) && t.Flag.Equals("TRANSFER") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("SubServ") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("TOUR") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("EXTRA"))
                                                  .Distinct()
                                                  .OrderBy(t => t.DateFrom)
                                                  .ToList();

                DataTable dummy = new DataTable();
                dummy.Columns.Add("Cidade");

                foreach (var item in query)
                {
                    string search = item.Cidade;
                    bool contains = dummy.AsEnumerable()
                                   .Any(row => search == row.Field<String>("Cidade"));

                    if (!contains)
                        dummy.Rows.Add(item.Cidade);
                }

                return dummy;
            }
            catch
            {
                throw;
            }
        }

        public DataTable ListarTodosCidadesComHotels(string Code)
        {
            try
            {
                var query = Con.TBItinerary.Where(t => t.Code.Equals(Code))
                                                  .Distinct()
                                                  .OrderBy(t => t.DateFrom)
                                                  .ToList();

                DataTable dummy = new DataTable();
                dummy.Columns.Add("Cidade");

                foreach (var item in query)
                {
                    string search = item.Cidade;
                    bool contains = dummy.AsEnumerable()
                                   .Any(row => search == row.Field<String>("Cidade"));

                    if (!contains)
                    {
                        dummy.Rows.Add(item.Cidade);
                    }
                }

                return dummy;
            }
            catch
            {
                throw;
            }
        }

        public List<string> ListarTodosCidadesComHotels_String(string Code)
        {
            try
            {
                var query = Con.TBItinerary.Where(t => t.Code.Equals(Code))
                                                  .Distinct()
                                                  .OrderBy(t => t.DateFrom)
                                                  .ToList();

                List<string> lst = new List<string>();
                foreach (var item in query)
                {
                    string search = item.Cidade;

                    if (!lst.Contains(item.Cidade))
                    {
                        lst.Add(item.Cidade);
                    }
                }

                return lst;
            }
            catch
            {
                throw;
            }
        }


        public class CidadeHigh
        {
            public int idCidade { get; set; }
            public string Cidade { get; set; }            
        }

        public List<CidadeHigh> ListarTodosCidadesComHotels_(string Code)
        {
            try
            {

                CidadeDAL cDal = new CidadeDAL();

                var query = Con.TBItinerary.Where(t => t.Code.Equals(Code))
                                                  .Distinct()
                                                  .OrderBy(t => t.DateFrom)
                                                  .ToList();

                List<CidadeHigh> lst = new List<CidadeHigh>();

                foreach (var item in query)
                {
                    if (!lst.Any(str => str.Cidade.Contains(item.Cidade)))
                    {
                        Cidade cid = cDal.ObterPorNome(item.Cidade);
                        lst.Add(new CidadeHigh() { Cidade = item.Cidade, idCidade = cid.CID_id });
                    }
                }

                return lst;
            }
            catch
            {
                throw;
            }
        }

        public List<CidadeHigh> ListarTodosCidadesComHotels_(string Code, int optQuote)
        {
            try
            {

                CidadeDAL cDal = new CidadeDAL();

                var query = Con.TBItinerary.Where(t => t.Code.Equals(Code) &&
                                                  t.OptQuote == optQuote)
                                                  .Distinct()
                                                  .OrderBy(t => t.DateFrom)
                                                  .ToList();

                List<CidadeHigh> lst = new List<CidadeHigh>();

                foreach (var item in query)
                {
                    if (!lst.Any(str => str.Cidade.Contains(item.Cidade)))
                    {
                        Cidade cid = cDal.ObterPorNome(item.Cidade);
                        lst.Add(new CidadeHigh() { Cidade = item.Cidade, idCidade = cid.CID_id });
                    }
                }

                return lst;
            }
            catch
            {
                throw;
            }
        }

        public List<CidadeHigh> ListarTodosCidadesComHotels_Teste(string Code)
        {
            try
            {
                var query = Con.TBItinerary.Where(t => t.Code.Equals(Code))
                                                  .Distinct()
                                                  .OrderBy(t => t.DateFrom)
                                                  .ToList();

                List<CidadeHigh> lst = new List<CidadeHigh>();

                foreach (var item in query)
                {
                    if (!lst.Any(str => str.Cidade.Contains(item.Cidade)))
                    {
                        lst.Add(new CidadeHigh() { Cidade = item.Cidade });
                    }
                }

                return lst;
                //DataTable dummy = new DataTable();
                //dummy.Columns.Add("Cidade");

                //foreach (var item in query)
                //{
                //    string search = item.Cidade;
                //    bool contains = dummy.AsEnumerable()
                //                   .Any(row => search == row.Field<String>("Cidade"));

                //    if (!contains)
                //        dummy.Rows.Add(item.Cidade);
                //}

                //return dummy;
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ObterPorQuote(string Quote)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Quotation_Code.Equals(Quote)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ObterPorQuote(string Quote, int optquote)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Quotation_Code.Equals(Quote) && t.OptQuote == optquote).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<TBItinerary> ObterPorQuoteOrder(string Quote, int optquote)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Quotation_Code.Equals(Quote) && t.OptQuote == optquote).OrderBy(s => s.DateFrom).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(TBItinerary t)
        {
            try
            {
                Con.TBItinerary.Remove(t);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public TBItinerary ObterPorId(int IdTbItine)
        {
            try
            {
                return Con.TBItinerary.Where(t => t.Itinerary_id == IdTbItine).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(TBItinerary novo, int numRange)
        {
            try
            {

                TBItinerary antigo = ObterPorId(novo.Itinerary_id);

                switch (numRange)
                {
                    case 1:
                        antigo.Range01 = novo.Range01;
                        antigo.Base01 = novo.Base01;
                        break;
                    case 2:
                        antigo.Range02 = novo.Range02;
                        antigo.Base02 = novo.Base02;
                        break;
                    case 3:
                        antigo.Range03 = novo.Range03;
                        antigo.Base03 = novo.Base03;
                        break;
                    case 4:
                        antigo.Range04 = novo.Range04;
                        antigo.Base04 = novo.Base04;
                        break;
                    case 5:
                        antigo.Range05 = novo.Range05;
                        antigo.Base05 = novo.Base05;
                        break;
                    case 6:
                        antigo.Range06 = novo.Range06;
                        antigo.Base06 = novo.Base06;
                        break;
                    case 7:
                        antigo.Range07 = novo.Range07;
                        antigo.Base07 = novo.Base07;
                        break;
                    case 8:
                        antigo.Range08 = novo.Range08;
                        antigo.Base08 = novo.Base08;
                        break;
                }

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(string range, string bse, decimal val, string bases, int IdItinerary)
        {
            SqlCon cn = new SqlCon();
            try
            {
                string valor = val.ToString().Replace(',', '.');

                string sql = "UPDATE TBItinerary SET ";
                sql = sql + range + " = " + valor + ", ";
                sql = sql + bse + " = '" + bases + "'";
                sql = sql + " where Itinerary_id = " + IdItinerary;

                cn.AbrirConexao();
                cn.Cmd = new SqlCommand(sql, cn.Con);
                cn.Cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.FecharConexao();
            }
        }

        public void AtualizarDAY(TBItinerary novo)
        {
            try
            {
                TBItinerary antigo = ObterPorId(novo.Itinerary_id);

                antigo.DAY = novo.DAY;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void AtualizarORDEM(TBItinerary novo)
        {
            try
            {
                TBItinerary antigo = ObterPorId(novo.Itinerary_id);

                antigo.Ordem = novo.Ordem;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public int QtdRooms(string Code)
        {
            try
            {
                return Con.TBItinerary.Where(a => a.Code.Equals(Code) &&
                                                  a.Room != null).Select(a => a.Room).Distinct().Count();
            }
            catch
            {
                throw;
            }
        }

        public int QtdRooms(string Code, int optquote)
        {
            try
            {
                return Con.TBItinerary.Where(a => a.Code.Equals(Code) &&
                                                  a.OptQuote == optquote &&
                                                  a.Room != null).Select(a => a.Room).Distinct().Count();
            }
            catch
            {
                throw;
            }
        }

        public List<string> ObterRoomsCode(string Code)
        {
            try
            {
                return Con.TBItinerary.Where(a => a.Code.Equals(Code) &&
                                                  a.Room != null).Select(a => a.Room).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<string> ObterRoomsCode(string Code, int optquote)
        {
            try
            {
                return Con.TBItinerary.Where(a => a.Code.Equals(Code) && 
                                                  a.OptQuote == optquote &&
                                                  a.Room != null).Select(a => a.Room).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public int ListarTodosTRANSFERCount(string Code)
        {
            try
            {
                var lstItinerary = Con.TBItinerary.Where(t => t.Quotation_Code.Equals(Code) && t.Flag.Equals("TRANSFER") ||
                                                  t.Quotation_Code.Equals(Code) && t.Flag.Equals("SubServ")
                                                  ).OrderBy(t => t.DateFrom).ToList();

                int count = lstItinerary.Count;
                foreach (var item in lstItinerary)
                {
                    if (item.Flag.Equals("SubServ"))
                    {
                        var f = new FileTransfersDAL().ObterPorId((int)item.idTabela);
                        var serv = new ServicoDAL().ObterPorNome(f.Transf_nome, (int)f.Trf_CID_id);

                        if (serv.Servicos_transfer == null)
                            count = count - 1;
                    }
                }

                return count;
            }
            catch
            {
                throw;
            }
        }

        public DataTable ListarTodosCidadesSemTransfer(string Code)
        {
            try
            {
                var query = Con.TBItinerary.Where(t => t.Code.Equals(Code) && t.Flag.Equals("TOUR") ||
                                                  t.Code.Equals(Code) && t.Flag.Equals("EXTRA"))
                                                  .Distinct()
                                                  .OrderBy(t => t.DateFrom)
                                                  .ToList();

                DataTable dummy = new DataTable();
                dummy.Columns.Add("Cidade");

                foreach (var item in query)
                {
                    string search = item.Cidade;
                    bool contains = dummy.AsEnumerable()
                                   .Any(row => search == row.Field<String>("Cidade"));

                    if (!contains)
                        dummy.Rows.Add(item.Cidade);
                }

                return dummy;
            }
            catch
            {
                throw;
            }
        }

        public List<string> ListarPorData_(string Quotation_Code, DateTime DateFrom)
        {
            try
            {
                //SELECT        Cidade
                //FROM            TBItinerary
                //WHERE        (Quotation_Code = N'09mai74055') AND (DateFrom = '20170223')
                //ORDER BY DateFrom, Ordem

                return Con.TBItinerary.Where(s => s.Quotation_Code.Equals(Quotation_Code) && s.DateFrom == DateFrom).OrderBy(s => s.DateFrom).ThenBy(s => s.Ordem).ToList().Select(s => s.Cidade).Distinct().ToList();

            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<itineraryModel> ListarItinerary(string code, int optquote)
        {
            try
            {

                return (from tbit in Con.TBItinerary
                        where tbit.Quotation_Code.Equals(code) &&
                              tbit.OptQuote == optquote
                        select new itineraryModel
                        {
                            cidade = tbit.Cidade,
                            dtfrom = (DateTime)tbit.DateFrom,
                            dtTo = (DateTime)tbit.DateTo,
                            titulo = tbit.SuppNome != null && !tbit.Flag.Equals("EXTRA") ? tbit.SuppNome : tbit.ServNome != null ? tbit.ServNome : tbit.ServExtrNome,
                            Flag = tbit.Flag,
                            ordem = (int)tbit.Ordem,
                            idTabela = (int)tbit.idTabela,
                            Sid = (int)tbit.S_id,
                            categoria = tbit.Categoria,
                            room = tbit.Room,
                            tbItineraryID = tbit.Itinerary_id,
                            Code = tbit.Quotation_Code,
                            optquote = (int)tbit.OptQuote,
                            NomePacote = tbit.NomePacote,
                            FlightVoo = tbit.Flight_voo,
                            FlightHora = tbit.Flight_hora,
                            MealNome = tbit.MealNome
                        }).OrderBy(s => s.dtfrom).ThenBy(s => s.ordem).ToList();

            }
            catch
            {
                throw;
            }
        }

        public TBItinerary ObterPorParam(string code, int optquote, int idtabela)
        {
            try
            {
                return Con.TBItinerary.Where(s => s.Quotation_Code.Equals(code) &&
                                                  s.OptQuote == optquote &&
                                                  s.idTabela == idtabela).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}