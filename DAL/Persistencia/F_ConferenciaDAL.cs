﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class F_ConferenciaDAL
    {
        private Model Con;

        public F_ConferenciaDAL()
        {
            Con = new Model();
        }


        public void inserir(F_Conferencia fc)
        {

            try
            {
                Con.F_Conferencia.Add(fc);
                Con.SaveChanges();
            }
            catch
            {
                
                throw;
            }
            
        }

        public void alterar(F_Conferencia novo)
        {
            try
            {
                F_Conferencia velho = consultarPorId(novo.id);

                velho.id_servico = novo.id_servico;
                velho.Reais = novo.Reais;
                //velho.Saldo_Conferir = novo.Saldo_Conferir;
                velho.tabela_referencia = novo.tabela_referencia;
                velho.USD = novo.USD;
                velho.status = novo.status;

                Con.SaveChanges();

            }
            catch 
            {
                
                throw;
            }



        }



        public F_Conferencia consultarPorId(int id)
        {
            try
            {
                return Con.F_Conferencia.Where(s => s.id == id).SingleOrDefault();
            }
            catch 
            {
                throw;
            }
        }

        public bool VerificaConferencia(int idServico, string tabela)
        {
            return (from fts in Con.FaturaSupplier
                    join fco in Con.F_Conferencia on fts.Fatura_id equals fco.id_fatura
                    where fco.status == false &&
                          fco.id_servico == idServico &&
                          fco.tabela_referencia.Equals(tabela)
                    select fco).Count() != 0;
        }

        public List<F_Conferencia> ObterSoma_Parciais(int idSpp, string File)
        {
            return (from spp in Con.Supplier
                    join fts in Con.FaturaSupplier on spp.S_id equals fts.Supplier_id
                    join fco in Con.F_Conferencia on fts.Fatura_id equals fco.id_fatura
                    where spp.S_id == idSpp &&
                          fco.nome_file.Equals(File)
                    select fco).ToList();
        }

    }
}
