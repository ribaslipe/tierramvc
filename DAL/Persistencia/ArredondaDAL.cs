﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class ArredondaDAL
    {

        private Model Con;

        public ArredondaDAL()
        {
            Con = new Model();
        }


        public Arredonda ObterArr(int valor)
        {
            try
            {
                return Con.Arredonda.Where(a => a.Arredonda_de <= valor &&
                                                a.Arredonda_para >= valor).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
