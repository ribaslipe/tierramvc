﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class PacoteDescricaoDAL
    {

        private Model Con;

        public PacoteDescricaoDAL()
        {
            Con = new Model();
        }

        public Pacote_Descricao ObterPorCode(string Code)
        {
            try
            {
                return Con.Pacote_Descricao.Where(p => p.Pacote_Code.Equals(Code)).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(Pacote_Descricao novo)
        {
            try
            {
                Pacote_Descricao antigo = ObterPorCode(novo.Pacote_Code);

                antigo.Pacote_descr = novo.Pacote_descr;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Salvar(Pacote_Descricao pd)
        {
            try
            {
                Con.Pacote_Descricao.Add(pd);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

    }
}
