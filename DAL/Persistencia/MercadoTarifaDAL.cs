﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;

namespace DAL.Persistencia
{
    public class MercadoTarifaDAL
    {

        private Model Con;

        public MercadoTarifaDAL()
        {
            Con = new Model();
        }

        //Método para gravar um registro na tabela Periodo
        public void Salvar(S_Mercado_Tarifa c)
        {
            try
            {
                //Provedor de Conexão do EntityFramework
                //NewTierraEntities Con = new NewTierraEntities();

                //Rotinas prontas para o banco de dados
                Con.S_Mercado_Tarifa.Add(c); //insert into S_Mercado_Tarifa values...
                Con.SaveChanges(); //Executar a operação

            }
            catch
            {
                throw;
            }
        }

        //Método para Excluir o S_Mercado_Tarifa na base
        public void Excluir(S_Mercado_Tarifa c)
        {
            try
            {
                Con.S_Mercado_Tarifa.Remove(c); //Prepara a deleção
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        //Metodo para listar todos os S_Mercado_Tarifa cadastrados
        public List<S_Mercado_Tarifa> ListarTodos()
        {
            try
            {
                //SQL -> select * from S_Mercado_Periodo
                return Con.S_Mercado_Tarifa.ToList();
            }
            catch
            {
                throw;
            }
        }       

        public List<S_Mercado_Tarifa> ListarTodosPorPeriodo(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Tarifa.Where(s => s.S_merc_periodo_id == IdPeriodo).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        //Método para obter 1 S_Mercado_Tarifa pelo Id
        public S_Mercado_Tarifa ObterPorId(int IdMercadoTarifa)
        {
            try
            {
                //select * from TbCliente where IdCliente = ?
                return Con.S_Mercado_Tarifa.Where(c => c.S_merc_tarif_id == IdMercadoTarifa).SingleOrDefault();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaTarifaCadastrada(S_Mercado_Tarifa c)
        {
            try
            {               
                    return Con.S_Mercado_Tarifa.Where(t => t.Tipo_room_id == c.Tipo_room_id && 
                                                      t.Tarif_categoria_id == c.Tarif_categoria_id && 
                                                      t.S_mercado_tipo_tarifa_id == c.S_mercado_tipo_tarifa_id &&
                                                      t.S_merc_periodo_id == c.S_merc_periodo_id)
                                                      .Count() != 0;                
            }
            catch
            {
                throw;
            }
        }

        public S_Mercado_Tarifa ObterPrimeiroPorPeriodo(int IdPeriodo)
        {
            try
            {
                return Con.S_Mercado_Tarifa.Where(s => s.S_merc_periodo_id == IdPeriodo).FirstOrDefault();

            }
            catch 
            {                
                throw;
            }
        }

        //Método para atualizar os dados do S_Mercado_Tarifa
        public void Atualizar(S_Mercado_Tarifa registroNovo)
        {
            try
            {
                //Buscar na base de dados o Registro antigo
                S_Mercado_Tarifa registroAntigo = ObterPorId(registroNovo.S_merc_tarif_id);

                //Atualizando os dados
                registroAntigo.S_merc_tarif_tarifa = registroNovo.S_merc_tarif_tarifa;
                registroAntigo.S_merc_tarif_status_id = registroNovo.S_merc_tarif_status_id;
                registroAntigo.Tipo_room_id = registroNovo.Tipo_room_id;
                registroAntigo.Tarif_categoria_id = registroNovo.Tarif_categoria_id;
                registroAntigo.S_mercado_tipo_tarifa_id = registroNovo.S_mercado_tipo_tarifa_id;
                registroAntigo.S_merc_periodo_id = registroNovo.S_merc_periodo_id;
                

                //Executo a transação
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }    

    }
}
