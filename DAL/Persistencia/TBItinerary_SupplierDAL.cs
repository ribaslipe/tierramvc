﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class TBItinerary_SupplierDAL
    {
        private Model Con;

        public TBItinerary_SupplierDAL()
        {
            Con = new Model();
        }

        public void Adicionar(TBItinerary_Supplier tbis)
        {
            Con.TBItinerary_Supplier.Add(tbis);
            Con.SaveChanges();
        }

        public void Atualizar(TBItinerary_Supplier tbis)
        {
            var old = Con.TBItinerary_Supplier.Where(a => a.Id == tbis.Id).Single();

            old.S_Name = tbis.S_Name;
            old.DateFrom = tbis.DateFrom;
            old.idTabela = tbis.idTabela;

            Con.SaveChanges();
        }

        public TBItinerary_Supplier ObterPorSupplierId(int SId, string code, int optQuote)
        {
            return Con.TBItinerary_Supplier.Where(a => a.S_Id == SId && a.Code == code && a.optQuote == optQuote).SingleOrDefault();
        }

        public TBItinerary_Supplier ObterPorSupplierId(int Id, string code, int idTabela, int optQuote)
        {
            return Con.TBItinerary_Supplier.Where(a => a.S_Id == Id && a.Code == code && a.idTabela == idTabela && a.optQuote == optQuote).SingleOrDefault();
        }

        public int Verifica(int sId, string code, int idTabela, int optQuote)
        {
            var t = Con.TBItinerary_Supplier.Where(a => a.S_Id == sId && a.Code == code && a.idTabela == idTabela && a.optQuote == optQuote).SingleOrDefault();

            if (t == null) return 0;

            else return t.Id;
        }

        public List<TBItinerary_Supplier> ObterTodos(string code, int optQuote)
        {
            try
            {
                return Con.TBItinerary_Supplier.Where(s => s.Code.Equals(code) && s.optQuote == optQuote).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
