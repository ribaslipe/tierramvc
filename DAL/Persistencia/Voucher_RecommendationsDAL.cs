﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class Voucher_RecommendationsDAL
    {
        private Model Con;

        public Voucher_RecommendationsDAL()
        {
            Con = new Model();
        }

        public Voucher_Recommendations ObterPorId(int Id)
        {
            return Con.Voucher_Recommendations.FirstOrDefault(a => a.Id == Id);
        }

        public List<Voucher_Recommendations> ObterPorVoucher(int voucherId)
        {
            return Con.Voucher_Recommendations.Where(a => a.VoucherId == voucherId).ToList();
        }

        public List<Voucher_Recommendations> AddDefault(int voucherId)
        {
            bool isEdited = false;
            List<Voucher_Recommendations> lvr = new List<Voucher_Recommendations>();
            for (int i = 0; i < 7; i++)
            {
                Voucher_Recommendations vr = new Voucher_Recommendations();
                vr.VoucherId = voucherId;

                if (i == 0)
                    vr.Recommendation = "Reconfirm international flights directly with the airline 24 hours prior to departure";
                else if (i == 1)
                    vr.Recommendation = "Check luggage restrictions/allowance with each airline";
                else if (i == 2)
                    vr.Recommendation = "Check your visa requirements for each country (more info at www.SouthAmerica.travel/visas)";
                else if (i == 3)
                    vr.Recommendation = "Travel Visas are required for North Americans, Australians and others to Brazil and Bolivia!";
                else if (i == 4)
                    vr.Recommendation = "An Entry Card is required for North Americans and Australians traveling to Argentina.";
                else if (i == 5)
                    vr.Recommendation = "Flight canceled or re-routed? Contact emergency number in your destination country ASAP.";
                else if (i == 6)
                    vr.Recommendation = "Please review our booking conditions at www.SouthAmerica.travel/terms";

                if (voucherId > 0)
                {
                    Con.Voucher_Recommendations.Add(vr);
                    isEdited = true;
                }
                else
                {
                    vr.Id = i;
                    lvr.Add(vr);
                }
            }

            if (isEdited)
            {
                var voucher = Con.Voucher.Where(a => a.Id == voucherId).Single();
                voucher.ImportantRecommendationsEdited = true;
            }

            Con.SaveChanges();

            if (voucherId > 0)
                return Con.Voucher_Recommendations.Where(a => a.VoucherId == voucherId).ToList();
            else
                return lvr;
        }

        public List<Voucher_Recommendations> AddDefault(int voucherId, bool ecuador, bool argentina, bool brasil, bool bolivia)
        {
            bool isEdited = false;
            List<Voucher_Recommendations> lvr = new List<Voucher_Recommendations>();
            for (int i = 0; i < 10; i++)
            {
                Voucher_Recommendations vr = new Voucher_Recommendations();
                vr.VoucherId = voucherId;

                if (i == 0)
                {
                    vr.Recommendation = "Reconfirm international flights directly with the airline 24 hours prior to departure";
                }
                else if (i == 1)
                {
                    vr.Recommendation = "Check luggage restrictions/allowance with each airline";
                }
                else if (i == 2)
                {
                    vr.Recommendation = "Check your visa requirements for each country (more info at www.SouthAmerica.travel/visas)";
                }
                else if (i == 3)
                {
                    if (brasil || bolivia)
                        vr.Recommendation = "Travel Visas are required for North Americans, Australians and others to Brazil and Bolivia!";

                }
                else if (i == 4)
                {
                    //if (argentina)
                        //vr.Recommendation = "An Entry Card is required for Canadians and Australians traveling to Argentina.";
                    //"An Entry Card is required for North Americans and Australians traveling to Argentina.";

                }
                else if (i == 5)
                {
                    vr.Recommendation = "Flight canceled or re-routed? Contact emergency number in your destination country ASAP.";
                }
                else if (i == 6)
                {
                    if (ecuador)
                        vr.Recommendation = "Proof of health insurance covering travel abroad.";

                    //vr.Recommendation = "Travel Insurance is required for entry into Ecuador";
                }
                else if (i == 7)
                {
                    vr.Recommendation = "Please review our booking conditions at www.SouthAmerica.travel/terms";
                }
                else if (i == 8)
                {
                    vr.Recommendation = "Passports must have a minimum validity of 6 months during entire travel period to South America";
                }
                else if (i == 9)
                {
                    vr.Recommendation = "Consult with your doctor about travel immunizations and check the CDC website for the latest requirements.";
                }



                if (voucherId > 0)
                {
                    Con.Voucher_Recommendations.Add(vr);
                    isEdited = true;
                }
                else
                {
                    vr.Id = i;
                    lvr.Add(vr);
                }
            }

            if (isEdited)
            {
                var voucher = Con.Voucher.Where(a => a.Id == voucherId).Single();
                voucher.ImportantRecommendationsEdited = true;
            }

            Con.SaveChanges();

            if (voucherId > 0)
                return Con.Voucher_Recommendations.Where(a => a.VoucherId == voucherId).ToList();
            else
                return lvr;
        }

        public int Adicionar(Voucher_Recommendations vr)
        {
            Con.Voucher_Recommendations.Add(vr);
            Con.SaveChanges();
            return vr.Id;
        }

        public void Update(Voucher_Recommendations vr)
        {
            var old = Con.Voucher_Recommendations.First(a => a.Id == vr.Id);
            old.Recommendation = vr.Recommendation;
            Con.SaveChanges();
        }

        public void Excluir(Voucher_Recommendations vr)
        {
            Con.Voucher_Recommendations.Remove(vr);
            Con.SaveChanges();
        }

        public void ExcluirTodos(int voucherId)
        {
            foreach (var item in ObterPorVoucher(voucherId))
            {
                Con.Voucher_Recommendations.Remove(item);
            }

            Con.SaveChanges();
        }
    }
}
