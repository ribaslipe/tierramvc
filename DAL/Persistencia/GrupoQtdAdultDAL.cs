﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class GrupoQtdAdultDAL
    {

        private Model Con;

        public GrupoQtdAdultDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quotation_Grupo_Qtd_Adult q)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Adult.Add(q);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Adult ObterPorId(int IdGQAdult)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Adult.Where(q => q.Qtd_Adult_id == IdGQAdult).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Adult ObterPorIdGrupo(int IdGQAdult, int numbergrp)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Adult.Where(q => q.Quotation_Grupo_Id == IdGQAdult && q.grp_number == numbergrp).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Adult ObterPorIdGrupo(int IdGQAdult)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Adult.Where(q => q.Quotation_Grupo_Id == IdGQAdult).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Quotation_Grupo_Qtd_Adult> ObterPorIdGrupoLST(int IdGQAdult)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Adult.Where(q => q.Quotation_Grupo_Id == IdGQAdult).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Quotation_Grupo_Qtd_Adult novo)
        {
            try
            {
                Quotation_Grupo_Qtd_Adult antigo = ObterPorId(novo.Qtd_Adult_id);

                antigo.Qtd = novo.Qtd;
                antigo.grp_number = novo.grp_number;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(Quotation_Grupo_Qtd_Adult item)
        {
            try
            {
                Con.Quotation_Grupo_Qtd_Adult.Remove(item);
                Con.SaveChanges();
            }
            catch 
            {
                throw;
            }
        }

        public Quotation_Grupo_Qtd_Adult ObterPorIdGrupoGrp(int IdGQAdult, int grp)
        {
            try
            {
                return Con.Quotation_Grupo_Qtd_Adult.Where(q => q.Quotation_Grupo_Id == IdGQAdult && q.grp_number == grp).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
