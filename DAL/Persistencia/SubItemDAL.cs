﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class SubItemDAL
    {

        private Model Con;

        public SubItemDAL()
        {
            Con = new Model();
        }

        public void Salvar(SubItem i)
        {
            try
            {
                Con.SubItem.Add(i);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public SubItem ObterPorId(int IdItem)
        {
            try
            {
                return Con.SubItem.Where(i => i.SubItem_id == IdItem).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(SubItem novo)
        {
            try
            {
                SubItem antigo = ObterPorId(novo.SubItem_id);

                antigo.SubItem_nome = novo.SubItem_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(SubItem i)
        {
            try
            {
                Con.SubItem.Remove(i);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<SubItem> ListarTodos()
        {
            try
            {
                return Con.SubItem.OrderBy(i => i.SubItem_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<SubItem> ListarTodos(string nome)
        {
            try
            {
                return Con.SubItem.Where(s => s.SubItem_nome.Contains(nome)).OrderBy(i => i.SubItem_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaNome(string nome)
        {
            try
            {
                return Con.SubItem.Where(i => i.SubItem_nome.Equals(nome)).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public SubItem ObterPorNome(string nome)
        {
            try
            {
                return Con.SubItem.Where(s => s.SubItem_nome == nome).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
