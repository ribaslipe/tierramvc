﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class SPoliticaTCTarifasDAL
    {
        private Model Con;

        public SPoliticaTCTarifasDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_PoliticaTCTarifas s)
        {
            try
            {
                Con.S_PoliticaTCTarifas.Add(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public S_PoliticaTCTarifas ObterPorId(int IdTarifa)
        {
            try
            {
                return Con.S_PoliticaTCTarifas.Where(s => s.S_merc_tarif_id == IdTarifa).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public S_PoliticaTCTarifas ObterPorIdOP(int IdPolitica)
        {
            try
            {
                return Con.S_PoliticaTCTarifas.Where(s => s.S_politicaTC_id == IdPolitica).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public object ListarTodos(int IdTarif)
        {
            try
            {
                return (from spc in Con.S_PoliticaTCTarifas
                        join smt in Con.S_Mercado_Tarifa on spc.S_merc_tarif_id equals smt.S_merc_tarif_id
                        join sph in Con.S_Politica_TC on spc.S_politicaTC_id equals sph.S_politicaTC_id
                        where spc.S_merc_tarif_id == IdTarif
                        select new
                        {
                            sph.S_politicaTC_qtd,
                            sph.S_politicaTC_idadeDe,
                            sph.S_politicaTC_free,
                            sph.S_politicaTC_valor,
                            sph.S_politicaTC_percent,
                            spc.S_PoliticaTCTarifas_id
                        }).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_PoliticaTCTarifas> ListarTodosLista(int IdTarif)
        {
            try
            {
                return Con.S_PoliticaTCTarifas.Where(s => s.S_merc_tarif_id == IdTarif).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public S_PoliticaTCTarifas ObterPorId(int IdTarifa, int IdPolitica)
        {
            try
            {
                return Con.S_PoliticaTCTarifas.Where(s => s.S_merc_tarif_id == IdTarifa && s.S_politicaTC_id == IdPolitica).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(S_PoliticaTCTarifas s)
        {
            try
            {
                Con.S_PoliticaTCTarifas.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }
    }
}
