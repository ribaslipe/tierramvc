﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Models;

namespace DAL.Persistencia
{
    public class MontaServicoDAL
    {

        private Model Con;

        public MontaServicoDAL()
        {
            Con = new Model();
        }

        public void Salvar(Monta_Servico m)
        {
            try
            {
                Con.Monta_Servico.Add(m);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Monta_Servico ObterPorId(int IdMonta)
        {
            try
            {
                return Con.Monta_Servico.Where(m => m.IdMServico == IdMonta).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Monta_Servico m)
        {
            try
            {
                Con.Monta_Servico.Remove(m);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Monta_Servico novo)
        {
            try
            {
                Monta_Servico antigo = ObterPorId(novo.IdMServico);

                antigo.MServico_Bulk_Porc_Taxa = novo.MServico_Bulk_Porc_Taxa;
                antigo.MServico_Bulk_Porc_Imposto = novo.MServico_Bulk_Porc_Imposto;
                antigo.MServico_Bulk_Porc_Comissao = novo.MServico_Bulk_Porc_Comissao;
                antigo.MServico_DataFrom = novo.MServico_DataFrom;
                antigo.MServico_DataTo = novo.MServico_DataTo;
                antigo.MServico_Obs = novo.MServico_Obs;
                antigo.S_id = novo.S_id;
                antigo.Tipo_Id = novo.Tipo_Id;
                antigo.Tipo_categ_id = novo.Tipo_categ_id;
                antigo.ItemSubServico_id = novo.ItemSubServico_id;
                antigo.SubItem_id = novo.SubItem_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<Monta_Servico> ListarTodos()
        {
            try
            {
                return Con.Monta_Servico.OrderBy(m => m.S_Servicos_Tipo.Tipo_Nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMS(int IdSupplier)
        {
            try
            {
                return Con.Monta_Servico.Where(s => s.S_id == IdSupplier).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public List<Servico_Completo> ListarTodosSubServicoPorTemporadaLista(int IdTemporada, int NumPaxs)
        {
            try
            {

                return (from sco in Con.Servico_Completo
                        join mon in Con.Monta_Servico on sco.IdMServico equals mon.IdMServico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join sba in Con.S_Bases on sup.S_id equals sba.S_id
                        join msv in Con.Monta_Servico_Valores on new { SID = (int)mon.IdMServico, BAS = (int)sba.Base_id } equals new { SID = (int)msv.IdMServico, BAS = (int)msv.Base_id }
                        where sco.IdMServico_Temporada == IdTemporada &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs
                        select sco).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<LstSubServs> ListarTodosSubServicoPorTemporada(int IdTemporada, int NumPaxs)
        {
            try
            {

                return (from sco in Con.Servico_Completo
                        join mon in Con.Monta_Servico on sco.IdMServico equals mon.IdMServico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join sba in Con.S_Bases on sup.S_id equals sba.S_id
                        join msv in Con.Monta_Servico_Valores on new { SID = (int)mon.IdMServico, BAS = (int)sba.Base_id } equals new { SID = (int)msv.IdMServico, BAS = (int)msv.Base_id }
                        where sco.IdMServico_Temporada == IdTemporada &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs
                        select new LstSubServs
                        {
                            S_nome = sup.S_nome,
                            Base_de = sba.Base_de,
                            Base_ate = sba.Base_ate,
                            MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total
                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<LstSubServs> ListarTodosSubServicoPorTemporada_Nova(int IdTemporada, int NumPaxs)
        {
            try
            {

                return (from sco in Con.Servico_Completo
                        join mon in Con.Monta_Servico on sco.IdMServico equals mon.IdMServico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join sba in Con.S_Bases on sup.S_id equals sba.S_id
                        join msv in Con.Monta_Servico_Valores on new { SID = (int)mon.IdMServico, BAS = (int)sba.Base_id } equals new { SID = (int)msv.IdMServico, BAS = (int)msv.Base_id }
                        where sco.IdMServico_Temporada == IdTemporada &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              sco.ServicoCompleto_de <= NumPaxs &&
                              sco.ServicoCompleto_ate >= NumPaxs
                        select new LstSubServs
                        {
                            S_nome = sup.S_nome,
                            sID = sup.S_id,
                            itemsubservid = (int)mon.ItemSubServico_id,
                            subitemid = (int)mon.SubItem_id,
                            Base_de = sco.ServicoCompleto_de,
                            Base_ate = sco.ServicoCompleto_ate,
                            MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                            TipoBase = sba.Tipo_Base.TipoBase_nome,
                            ItemNome = mon.ItemSubServico.ItemSubServico_nome,
                            SubItemNome = mon.SubItem.SubItem_nome,
                            Moeda = sba.Moeda.Moeda_sigla
                        }).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfersSubServico(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Servicos_Id == IdServico
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfersSubServicoNomesServs(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Servicos_Id == IdServico
                            select new LstTransfersModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier
                            select new LstTransfersModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade
                            select new LstTransfersModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfersSubServicoNomesServsRecomm(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Servicos_Id == IdServico &&
                                      sse.Recommended == true
                            select new LstTransfersModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Recommended == true
                            select new LstTransfersModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      sse.Recommended == true
                            select new LstTransfersModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosToursSubServicoNomesServs(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Servicos_Id == IdServico
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                       sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                       sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosToursSubServicoNomesServsRecomm(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Servicos_Id == IdServico &&
                                      sse.Recommended == true
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Recommended == true
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      sse.Recommended == true
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }


        public List<LstTransfersModel> ListarTodosTransfersSubServicoRecomm(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Servicos_Id == IdServico &&
                                      sse.Recommended == true
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Recommended == true
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Cid_Id == IdCidade &&
                                      sse.Recommended == true
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosTransfersSubServico(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome)
        {
            try
            {

                return (from sco in Con.Servico_Completo
                        join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                        join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                        join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                        join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                        join spp in Con.Supplier on mon.S_id equals spp.S_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  sco.SubServico == null &&
                                  sse.Servicos_transfer != null &&
                                  sse.Servicos_Nome.Equals(nome)
                        select new
                        {
                            mon.IdMServico,
                            sse.Servicos_Nome,
                            sbv.S_Bases_Valor_valor,
                            sba.Base_de,
                            sba.Base_ate,
                            sse.Recommended,
                            sco.IdMServico_Temporada,
                            spp.S_id,
                            spp.S_nome,
                            mod.Moeda_id,
                            mod.Moeda_sigla,
                            mon.MServico_DataFrom,
                            mon.MServico_DataTo
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosToursSubServico(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Servicos_Id == IdServico
                            select new LstToursModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier
                            select new LstToursModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade
                            select new LstToursModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }


            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosToursSubServicoRecomm(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Servicos_Id == IdServico &&
                                      sse.Recommended == true
                            select new LstToursModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      mon.S_id == IdSupplier &&
                                      sse.Recommended == true
                            select new LstToursModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Cid_Id == IdCidade &&
                                      sse.Recommended == true
                            select new LstToursModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }


            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosToursSubServico(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome)
        {
            try
            {
                return (from sco in Con.Servico_Completo
                        join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                        join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                        join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                        join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                        join spp in Con.Supplier on mon.S_id equals spp.S_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  sco.SubServico == null &&
                                  sse.Servicos_transfer == null &&
                                  sse.Servicos_Nome.Equals(nome)
                        select new
                        {
                            mon.IdMServico,
                            sse.Servicos_Nome,
                            sbv.S_Bases_Valor_valor,
                            sse.Recommended,
                            sba.Base_de,
                            sba.Base_ate,
                            sco.IdMServico_Temporada,
                            spp.S_id,
                            spp.S_nome,
                            mod.Moeda_id,
                            mod.Moeda_sigla,
                            mon.MServico_DataFrom,
                            mon.MServico_DataTo
                        }).Distinct().ToList();


            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosToursSubServico(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade)
        {
            try
            {
                return (from sco in Con.Servico_Completo
                        join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                        join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                        join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                        join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                        join spp in Con.Supplier on mon.S_id equals spp.S_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  sco.SubServico == null &&
                                  sse.Servicos_transfer == null &&
                                  sse.Servicos_Nome.Equals(nome) &&
                                  sse.Cid_Id == IdCidade
                        select new LstToursModel
                        {
                            IdMServico = mon.IdMServico,
                            Servicos_Nome = sse.Servicos_Nome,
                            Servicos_Id = sse.Servicos_Id,
                            S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                            Base_de = sba.Base_de,
                            Base_ate = sba.Base_ate,
                            TipoBase = sba.Tipo_Base.TipoBase_nome,
                            TipoBaseId = (int)sba.TipoBase_id,
                            IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                            S_id = spp.S_id,
                            S_nome = spp.S_nome,
                            Moeda_id = mod.Moeda_id,
                            Moeda_sigla = mod.Moeda_sigla,
                            MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                            MServico_DataTo = (DateTime)mon.MServico_DataTo,
                            SubServico = true
                        }).Distinct().ToList();


            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosToursSubServicoRecomm(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade)
        {
            try
            {
                return (from sco in Con.Servico_Completo
                        join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                        join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                        join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                        join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                        join spp in Con.Supplier on mon.S_id equals spp.S_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  sco.SubServico == null &&
                                  sse.Servicos_transfer == null &&
                                  sse.Servicos_Nome.Equals(nome) &&
                                  sse.Cid_Id == IdCidade &&
                                  sse.Recommended == true
                        select new LstToursModel
                        {
                            IdMServico = mon.IdMServico,
                            Servicos_Nome = sse.Servicos_Nome,
                            Servicos_Id = sse.Servicos_Id,
                            S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                            Base_de = sba.Base_de,
                            Base_ate = sba.Base_ate,
                            TipoBase = sba.Tipo_Base.TipoBase_nome,
                            TipoBaseId = (int)sba.TipoBase_id,
                            IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                            S_id = spp.S_id,
                            S_nome = spp.S_nome,
                            Moeda_id = mod.Moeda_id,
                            Moeda_sigla = mod.Moeda_sigla,
                            MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                            MServico_DataTo = (DateTime)mon.MServico_DataTo,
                            SubServico = true
                        }).Distinct().ToList();


            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosToursSubServico(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, int idSupplier)
        {
            try
            {
                if (idSupplier != 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Servicos_Nome.Equals(nome) &&
                                      sse.Cid_Id == IdCidade &&
                                      spp.S_id == idSupplier
                            select new
                            {
                                mon.IdMServico,
                                sse.Servicos_Nome,
                                sbv.S_Bases_Valor_valor,
                                sba.Base_de,
                                sba.Base_ate,
                                sco.IdMServico_Temporada,
                                spp.S_id,
                                spp.S_nome,
                                mod.Moeda_id,
                                mod.Moeda_sigla,
                                mon.MServico_DataFrom,
                                mon.MServico_DataTo
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer == null &&
                                      sse.Servicos_Nome.Equals(nome) &&
                                      sse.Cid_Id == IdCidade
                            select new
                            {
                                mon.IdMServico,
                                sse.Servicos_Nome,
                                sbv.S_Bases_Valor_valor,
                                sba.Base_de,
                                sba.Base_ate,
                                sco.IdMServico_Temporada,
                                spp.S_id,
                                spp.S_nome,
                                mod.Moeda_id,
                                mod.Moeda_sigla,
                                mon.MServico_DataFrom,
                                mon.MServico_DataTo
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosTransfers(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade)
        {
            try
            {

                return (from mon in Con.Monta_Servico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                        join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataFrom &&
                              mon.MServico_DataTo >= DataTo &&
                              ssv.Servicos_transfer != null &&
                              ssv.Cid_Id == IdCidade &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              scp.SubServico != null &&
                              ssv.Servicos_Nome.Equals(nome)
                        select new
                        {
                            ssv.Servicos_Nome,
                            sup.S_nome,
                            sup.S_id,
                            mon.MServico_DataFrom,
                            mon.MServico_DataTo,
                            mod.Moeda_sigla,
                            mod.Moeda_id,
                            msv.MServico_Valores_Bulk_Total,
                            sba.Base_de,
                            sba.Base_ate
                        }).ToList();
                //}

            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfers(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, int idSupplier)
        {
            try
            {

                if (idSupplier != 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Nome.Equals(nome) &&
                                  sup.S_id == idSupplier &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = (decimal)msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }
                else
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Nome.Equals(nome) &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = (decimal)msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfersRecomm(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, int idSupplier)
        {
            try
            {

                if (idSupplier != 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Nome.Equals(nome) &&
                                  sup.S_id == idSupplier &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = (decimal)msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }
                else
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Nome.Equals(nome) &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = (decimal)msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosTransfersSubServico(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade)
        {
            try
            {

                return (from sco in Con.Servico_Completo
                        join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                        join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                        join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                        join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                        join spp in Con.Supplier on mon.S_id equals spp.S_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  sco.SubServico == null &&
                                  sse.Servicos_transfer != null &&
                                  sse.Servicos_Nome.Equals(nome) &&
                                  sse.Cid_Id == IdCidade
                        select new
                        {
                            mon.IdMServico,
                            sse.Servicos_Nome,
                            sbv.S_Bases_Valor_valor,
                            sba.Base_de,
                            sba.Base_ate,
                            sco.IdMServico_Temporada,
                            spp.S_id,
                            spp.S_nome,
                            mod.Moeda_id,
                            mod.Moeda_sigla,
                            mon.MServico_DataFrom,
                            mon.MServico_DataTo
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }


        public List<LstTransfersModel> ListarTodosTransfersSubServico(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, int idSupplier)
        {
            try
            {
                if (idSupplier != 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Servicos_Nome.Equals(nome) &&
                                      sse.Cid_Id == IdCidade &&
                                      spp.S_id == idSupplier
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Servicos_Nome.Equals(nome) &&
                                      sse.Cid_Id == IdCidade
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfersSubServicoNoValue(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, int idSupplier)
        {
            try
            {
                if (idSupplier != 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Servicos_Nome.Equals(nome) &&
                                      sse.Cid_Id == IdCidade &&
                                      spp.S_id == idSupplier
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Servicos_Nome.Equals(nome) &&
                                      sse.Cid_Id == IdCidade
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfersSubServicoRecomm(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, int idSupplier)
        {
            try
            {
                if (idSupplier != 0)
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Servicos_Nome.Equals(nome) &&
                                      sse.Cid_Id == IdCidade &&
                                      spp.S_id == idSupplier &&
                                      sse.Recommended == true
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }
                else
                {
                    return (from sco in Con.Servico_Completo
                            join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                            join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                            join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                            join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataTo &&
                                      mon.MServico_DataTo >= DataFrom &&
                                      sba.Base_de <= NumPaxs &&
                                      sba.Base_ate >= NumPaxs &&
                                      sco.SubServico == null &&
                                      sse.Servicos_transfer != null &&
                                      sse.Servicos_Nome.Equals(nome) &&
                                      sse.Cid_Id == IdCidade &&
                                      sse.Recommended == true
                            select new LstTransfersModel
                            {
                                IdMServico = mon.IdMServico,
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                S_Bases_Valor_valor = sbv.S_Bases_Valor_valor,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                IdMServico_Temporada = (int)sco.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                Moeda_id = mod.Moeda_id,
                                Moeda_sigla = mod.Moeda_sigla,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true
                            }).Distinct().ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarSubsTempoReal(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, bool recomm)
        {
            try
            {
                #region query
                //SELECT S_Servicos.Servicos_Id, S_Servicos.Servicos_Nome, Servico_Completo.ServicoCompleto_de, Servico_Completo.ServicoCompleto_ate, Servico_Completo.SubServico, Monta_Servico.MServico_DataFrom, 
                //Monta_Servico.MServico_DataTo, Servico_Completo.ServicoCompleto_id, Servico_Completo.IdMServico, Servico_Completo.IdMServico_Temporada, Supplier.S_nome
                //FROM            S_Servicos INNER JOIN
                //Servico_Completo ON S_Servicos.Servicos_Id = Servico_Completo.Servicos_Id INNER JOIN
                //Monta_Servico ON Servico_Completo.IdMServico_Temporada = Monta_Servico.IdMServico INNER JOIN
                //Supplier ON Monta_Servico.S_id = Supplier.S_id
                //WHERE(S_Servicos.Servicos_Nome = 'Rio de Janeiro (Hotels)-Barra') AND(Monta_Servico.MServico_DataFrom <= '20180530') AND(Monta_Servico.MServico_DataTo >= '20180530') AND(Servico_Completo.SubServico IS NULL) AND
                //(Servico_Completo.ServicoCompleto_de <= 4) AND(Servico_Completo.ServicoCompleto_ate >= 4) AND (S_Servicos.Cid_Id = 631)
                #endregion

                if (recomm)
                {
                    return (from sse in Con.S_Servicos
                            join scm in Con.Servico_Completo on sse.Servicos_Id equals scm.Servicos_Id
                            join mon in Con.Monta_Servico on scm.IdMServico_Temporada equals mon.IdMServico
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            where sse.Servicos_Nome.Equals(nome) &&
                                  mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  scm.SubServico == null &&
                                  scm.ServicoCompleto_de <= NumPaxs &&
                                  scm.ServicoCompleto_ate >= NumPaxs &&
                                  sse.Cid_Id == IdCidade &&
                                  sse.Recommended == true &&
                                  sse.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                IdMServico_Temporada = (int)scm.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true,
                                servCompetoID = scm.ServicoCompleto_id
                            }).ToList();
                }
                else
                {
                    return (from sse in Con.S_Servicos
                            join scm in Con.Servico_Completo on sse.Servicos_Id equals scm.Servicos_Id
                            join mon in Con.Monta_Servico on scm.IdMServico_Temporada equals mon.IdMServico
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            where sse.Servicos_Nome.Equals(nome) &&
                                  mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  scm.SubServico == null &&
                                  scm.ServicoCompleto_de <= NumPaxs &&
                                  scm.ServicoCompleto_ate >= NumPaxs &&
                                  sse.Cid_Id == IdCidade &&
                                  sse.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                IdMServico_Temporada = (int)scm.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true,
                                servCompetoID = scm.ServicoCompleto_id
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarSubsTempoRealTrs(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, bool recomm)
        {
            try
            {
                #region query
                //SELECT S_Servicos.Servicos_Id, S_Servicos.Servicos_Nome, Servico_Completo.ServicoCompleto_de, Servico_Completo.ServicoCompleto_ate, Servico_Completo.SubServico, Monta_Servico.MServico_DataFrom, 
                //Monta_Servico.MServico_DataTo, Servico_Completo.ServicoCompleto_id, Servico_Completo.IdMServico, Servico_Completo.IdMServico_Temporada, Supplier.S_nome
                //FROM            S_Servicos INNER JOIN
                //Servico_Completo ON S_Servicos.Servicos_Id = Servico_Completo.Servicos_Id INNER JOIN
                //Monta_Servico ON Servico_Completo.IdMServico_Temporada = Monta_Servico.IdMServico INNER JOIN
                //Supplier ON Monta_Servico.S_id = Supplier.S_id
                //WHERE(S_Servicos.Servicos_Nome = 'Rio de Janeiro (Hotels)-Barra') AND(Monta_Servico.MServico_DataFrom <= '20180530') AND(Monta_Servico.MServico_DataTo >= '20180530') AND(Servico_Completo.SubServico IS NULL) AND
                //(Servico_Completo.ServicoCompleto_de <= 4) AND(Servico_Completo.ServicoCompleto_ate >= 4) AND (S_Servicos.Cid_Id = 631)
                #endregion

                if (recomm)
                {
                    return (from sse in Con.S_Servicos
                            join scm in Con.Servico_Completo on sse.Servicos_Id equals scm.Servicos_Id
                            join mon in Con.Monta_Servico on scm.IdMServico_Temporada equals mon.IdMServico
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            where sse.Servicos_Nome.Equals(nome) &&
                                  mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  scm.SubServico == null &&
                                  scm.ServicoCompleto_de <= NumPaxs &&
                                  scm.ServicoCompleto_ate >= NumPaxs &&
                                  sse.Cid_Id == IdCidade &&
                                  sse.Recommended == true &&
                                  sse.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                IdMServico_Temporada = (int)scm.IdMServico_Temporada,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true,
                                servCompetoID = scm.ServicoCompleto_id,
                                bseIndex = scm.Base_Index,
                                obs = mon.MServico_Obs
                            }).ToList();
                }
                else
                {
                    return (from sse in Con.S_Servicos
                            join scm in Con.Servico_Completo on sse.Servicos_Id equals scm.Servicos_Id
                            join mon in Con.Monta_Servico on scm.IdMServico_Temporada equals mon.IdMServico
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            where sse.Servicos_Nome.Equals(nome) &&
                                  mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  scm.SubServico == null &&
                                  scm.ServicoCompleto_de <= NumPaxs &&
                                  scm.ServicoCompleto_ate >= NumPaxs &&
                                  sse.Cid_Id == IdCidade &&
                                  sse.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                IdMServico_Temporada = (int)scm.IdMServico_Temporada,
                                IdMServico = (int)scm.IdMServico,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true,
                                servCompetoID = scm.ServicoCompleto_id,
                                bseIndex = scm.Base_Index,
                                obs = mon.MServico_Obs
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarNoSubsTempoRealTrs(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, bool recomm)
        {
            try
            {
                #region query
                //SELECT S_Servicos.Servicos_Id, S_Servicos.Servicos_Nome, Servico_Completo.ServicoCompleto_de, Servico_Completo.ServicoCompleto_ate, Servico_Completo.SubServico, Monta_Servico.MServico_DataFrom, 
                //Monta_Servico.MServico_DataTo, Servico_Completo.ServicoCompleto_id, Servico_Completo.IdMServico, Servico_Completo.IdMServico_Temporada, Supplier.S_nome
                //FROM            S_Servicos INNER JOIN
                //Servico_Completo ON S_Servicos.Servicos_Id = Servico_Completo.Servicos_Id INNER JOIN
                //Monta_Servico ON Servico_Completo.IdMServico_Temporada = Monta_Servico.IdMServico INNER JOIN
                //Supplier ON Monta_Servico.S_id = Supplier.S_id
                //WHERE(S_Servicos.Servicos_Nome = 'Rio de Janeiro (Hotels)-Barra') AND(Monta_Servico.MServico_DataFrom <= '20180530') AND(Monta_Servico.MServico_DataTo >= '20180530') AND(Servico_Completo.SubServico IS NULL) AND
                //(Servico_Completo.ServicoCompleto_de <= 4) AND(Servico_Completo.ServicoCompleto_ate >= 4) AND (S_Servicos.Cid_Id = 631)
                #endregion

                if (recomm)
                {
                    return (from sse in Con.S_Servicos
                            join scm in Con.Servico_Completo on sse.Servicos_Id equals scm.Servicos_Id
                            join mon in Con.Monta_Servico on scm.IdMServico equals mon.IdMServico
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            where sse.Servicos_Nome.Equals(nome) &&
                                  mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  scm.SubServico == null &&
                                  scm.ServicoCompleto_de <= NumPaxs &&
                                  scm.ServicoCompleto_ate >= NumPaxs &&
                                  sse.Cid_Id == IdCidade &&
                                  sse.Recommended == true &&
                                  sse.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                IdMServico = (int)scm.IdMServico,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true,
                                servCompetoID = scm.ServicoCompleto_id,
                                bseIndex = scm.Base_Index,
                                obs = mon.MServico_Obs
                            }).ToList();
                }
                else
                {
                    return (from sse in Con.S_Servicos
                            join scm in Con.Servico_Completo on sse.Servicos_Id equals scm.Servicos_Id
                            join mon in Con.Monta_Servico on scm.IdMServico equals mon.IdMServico
                            join spp in Con.Supplier on mon.S_id equals spp.S_id
                            where sse.Servicos_Nome.Equals(nome) &&
                                  mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  scm.SubServico == null &&
                                  scm.ServicoCompleto_de <= NumPaxs &&
                                  scm.ServicoCompleto_ate >= NumPaxs &&
                                  sse.Cid_Id == IdCidade &&
                                  sse.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = sse.Servicos_Nome,
                                Servicos_Id = sse.Servicos_Id,
                                Recomended = sse.Recommended,
                                IdMServico = (int)scm.IdMServico,
                                S_id = spp.S_id,
                                S_nome = spp.S_nome,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                SubServico = true,
                                servCompetoID = scm.ServicoCompleto_id,
                                bseIndex = scm.Base_Index,
                                obs = mon.MServico_Obs
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public class retornoGetValoresSubsModel
        {
            public decimal MServico_Valores_Bulk_Total { get; set; }
            public int TipoBase_id { get; set; }
            public int Moeda_id { get; set; }
            public int Base_de { get; set; }
            public int Base_ate { get; set; }
            public int ItemID { get; set; }
            public int SubItemID { get; set; }
            public int SID { get; set; }

            public string ItemNome { get; set; }
            public string SubItemNome { get; set; }
        }

        public retornoGetValoresSubsModel GetValoresSubs(int ServCompletoID, int NumPax)
        {
            try
            {
                #region query
                //SELECT DISTINCT Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_Bases.TipoBase_id, S_Bases.Moeda_id, S_Bases.Base_de, S_Bases.Base_ate
                //FROM            Monta_Servico INNER JOIN
                //Servico_Completo ON Monta_Servico.IdMServico = Servico_Completo.IdMServico INNER JOIN
                //Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN
                //S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id
                //WHERE(Servico_Completo.ServicoCompleto_id = 184543) AND(S_Bases.Base_de <= 4) AND(S_Bases.Base_ate >= 4)
                #endregion

                return (from mon in Con.Monta_Servico
                        join sco in Con.Servico_Completo on mon.IdMServico equals sco.IdMServico
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on msv.Base_id equals sba.Base_id
                        where sco.ServicoCompleto_id == ServCompletoID &&
                              sba.Base_de <= NumPax &&
                              sba.Base_ate >= NumPax
                        select new retornoGetValoresSubsModel
                        {
                            MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total == null ? 0 : (decimal)msv.MServico_Valores_Bulk_Total,
                            Base_ate = (int)sba.Base_ate,
                            Base_de = (int)sba.Base_de,
                            Moeda_id = sba.Moeda_id,
                            TipoBase_id = (int)sba.TipoBase_id,
                            ItemID = (int)mon.ItemSubServico_id,
                            SubItemID = (int)mon.SubItem_id,
                            SID = mon.S_id,
                            ItemNome = mon.ItemSubServico.ItemSubServico_nome,
                            SubItemNome = mon.SubItem.SubItem_nome
                        }).FirstOrDefault();


            }
            catch
            {
                throw;
            }
        }

        public retornoGetValoresSubsModel GetValoresSubsInto(int ItemSubServico_id, int SubItem_id, int Base_de, int Base_ate, DateTime dtFrom, DateTime dtTo, int S_id)
        {
            try
            {
                #region query                
                //SELECT DISTINCT
                //Monta_Servico.IdMServico, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, ItemSubServico.ItemSubServico_nome, SubItem.SubItem_nome, Supplier.S_nome, 
                //Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_Bases.Base_de, S_Bases.Base_ate
                //FROM            Monta_Servico INNER JOIN
                //ItemSubServico ON Monta_Servico.ItemSubServico_id = ItemSubServico.ItemSubServico_id INNER JOIN
                //SubItem ON Monta_Servico.SubItem_id = SubItem.SubItem_id INNER JOIN
                //Supplier ON Monta_Servico.S_id = Supplier.S_id INNER JOIN
                //Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN
                //S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id
                //WHERE(Monta_Servico.ItemSubServico_id = 2) AND(Monta_Servico.SubItem_id = 46508) AND(S_Bases.Base_de <= 2) AND(S_Bases.Base_ate >= 2) AND(Monta_Servico.MServico_DataFrom <= '20190710') AND
                //(Monta_Servico.MServico_DataTo >= '20190710') AND(Monta_Servico.S_id = 470024)



                //SELECT DISTINCT Monta_Servico.IdMServico, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_Bases.Base_de, S_Bases.Base_ate
                //FROM            Monta_Servico INNER JOIN
                //Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN
                //S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id
                //WHERE(Monta_Servico.ItemSubServico_id = 2) AND(Monta_Servico.SubItem_id = 46508) AND(S_Bases.Base_de <= 2) AND(S_Bases.Base_ate >= 2) AND(Monta_Servico.MServico_DataFrom <= '20190710') AND
                //(Monta_Servico.MServico_DataTo >= '20190710') AND(Monta_Servico.S_id = 470024)
                #endregion

                return (from mon in Con.Monta_Servico
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on msv.Base_id equals sba.Base_id
                        where mon.ItemSubServico_id == ItemSubServico_id &&
                              mon.SubItem_id == SubItem_id &&
                              sba.Base_de <= Base_de &&
                              sba.Base_ate >= Base_ate &&
                              mon.MServico_DataFrom <= dtFrom &&
                              mon.MServico_DataTo >= dtTo &&
                              mon.S_id == S_id
                        select new retornoGetValoresSubsModel
                        {
                            MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total == null ? 0 : (decimal)msv.MServico_Valores_Bulk_Total,
                            TipoBase_id = (int)sba.TipoBase_id,
                            Moeda_id = sba.Moeda_id
                        }).Distinct().FirstOrDefault();



            }
            catch
            {
                throw;
            }
        }

        public retornoGetValoresSubsModel GetValoresSubsInto(int ItemSubServico_id, int SubItem_id, int Base_de, int Base_ate, DateTime dtFrom, DateTime dtTo, int S_id, int index)
        {
            try
            {
                #region query                
                //SELECT DISTINCT
                //Monta_Servico.IdMServico, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, ItemSubServico.ItemSubServico_nome, SubItem.SubItem_nome, Supplier.S_nome, 
                //Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_Bases.Base_de, S_Bases.Base_ate
                //FROM            Monta_Servico INNER JOIN
                //ItemSubServico ON Monta_Servico.ItemSubServico_id = ItemSubServico.ItemSubServico_id INNER JOIN
                //SubItem ON Monta_Servico.SubItem_id = SubItem.SubItem_id INNER JOIN
                //Supplier ON Monta_Servico.S_id = Supplier.S_id INNER JOIN
                //Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN
                //S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id
                //WHERE(Monta_Servico.ItemSubServico_id = 2) AND(Monta_Servico.SubItem_id = 46508) AND(S_Bases.Base_de <= 2) AND(S_Bases.Base_ate >= 2) AND(Monta_Servico.MServico_DataFrom <= '20190710') AND
                //(Monta_Servico.MServico_DataTo >= '20190710') AND(Monta_Servico.S_id = 470024)



                //SELECT DISTINCT Monta_Servico.IdMServico, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_Bases.Base_de, S_Bases.Base_ate
                //FROM            Monta_Servico INNER JOIN
                //Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN
                //S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id
                //WHERE(Monta_Servico.ItemSubServico_id = 2) AND(Monta_Servico.SubItem_id = 46508) AND(S_Bases.Base_de <= 2) AND(S_Bases.Base_ate >= 2) AND(Monta_Servico.MServico_DataFrom <= '20190710') AND
                //(Monta_Servico.MServico_DataTo >= '20190710') AND(Monta_Servico.S_id = 470024)
                #endregion

                return (from mon in Con.Monta_Servico
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on msv.Base_id equals sba.Base_id
                        where mon.ItemSubServico_id == ItemSubServico_id &&
                              mon.SubItem_id == SubItem_id &&
                              sba.Base_de <= Base_de &&
                              sba.Base_ate >= Base_ate &&
                              mon.MServico_DataFrom <= dtFrom &&
                              mon.MServico_DataTo >= dtTo &&
                              mon.S_id == S_id
                        select new retornoGetValoresSubsModel
                        {
                            MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total == null ? 0 : (decimal)msv.MServico_Valores_Bulk_Total,
                            TipoBase_id = (int)sba.TipoBase_id,
                            Moeda_id = sba.Moeda_id
                        }).Distinct().FirstOrDefault();



            }
            catch
            {
                throw;
            }
        }

        public retornoGetValoresSubsModel GetValoresSubsInto(int ItemSubServico_id, int SubItem_id, int Base_de, int Base_ate, DateTime dtFrom, DateTime dtTo, int S_id, int index, string obs)
        {
            try
            {
                #region query                
                //SELECT DISTINCT
                //Monta_Servico.IdMServico, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, ItemSubServico.ItemSubServico_nome, SubItem.SubItem_nome, Supplier.S_nome, 
                //Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_Bases.Base_de, S_Bases.Base_ate
                //FROM            Monta_Servico INNER JOIN
                //ItemSubServico ON Monta_Servico.ItemSubServico_id = ItemSubServico.ItemSubServico_id INNER JOIN
                //SubItem ON Monta_Servico.SubItem_id = SubItem.SubItem_id INNER JOIN
                //Supplier ON Monta_Servico.S_id = Supplier.S_id INNER JOIN
                //Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN
                //S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id
                //WHERE(Monta_Servico.ItemSubServico_id = 2) AND(Monta_Servico.SubItem_id = 46508) AND(S_Bases.Base_de <= 2) AND(S_Bases.Base_ate >= 2) AND(Monta_Servico.MServico_DataFrom <= '20190710') AND
                //(Monta_Servico.MServico_DataTo >= '20190710') AND(Monta_Servico.S_id = 470024)



                //SELECT DISTINCT Monta_Servico.IdMServico, Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, Monta_Servico_Valores.MServico_Valores_Bulk_Total, S_Bases.Base_de, S_Bases.Base_ate
                //FROM            Monta_Servico INNER JOIN
                //Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN
                //S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id
                //WHERE(Monta_Servico.ItemSubServico_id = 2) AND(Monta_Servico.SubItem_id = 46508) AND(S_Bases.Base_de <= 2) AND(S_Bases.Base_ate >= 2) AND(Monta_Servico.MServico_DataFrom <= '20190710') AND
                //(Monta_Servico.MServico_DataTo >= '20190710') AND(Monta_Servico.S_id = 470024)
                #endregion

                return (from mon in Con.Monta_Servico
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on msv.Base_id equals sba.Base_id
                        where mon.ItemSubServico_id == ItemSubServico_id &&
                              mon.SubItem_id == SubItem_id &&
                              sba.Base_de <= Base_de &&
                              sba.Base_ate >= Base_ate &&
                              mon.MServico_DataFrom <= dtFrom &&
                              mon.MServico_DataTo >= dtTo &&
                              mon.S_id == S_id &&
                              mon.MServico_Obs.Equals(obs)
                        select new retornoGetValoresSubsModel
                        {
                            MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total == null ? 0 : (decimal)msv.MServico_Valores_Bulk_Total,
                            TipoBase_id = (int)sba.TipoBase_id,
                            Moeda_id = sba.Moeda_id
                        }).Distinct().FirstOrDefault();



            }
            catch
            {
                throw;
            }
        }

        public retornoGetValoresSubsModel GetValoresSubsIntoNew(int ItemSubServico_id, int SubItem_id, int Base_de, int Base_ate, DateTime dtFrom, DateTime dtTo, int S_id)
        {
            try
            {
                #region query                
                //SELECT Monta_Servico.MServico_DataFrom, Monta_Servico.MServico_DataTo, Monta_Servico.MServico_Obs, Monta_Servico_Valores.MServico_Valores_Bulk_Total, 
                //S_Bases.Base_de, S_Bases.Base_ate, Supplier.S_nome, 
                //Supplier.S_id
                //FROM            Monta_Servico INNER JOIN
                //Monta_Servico_Valores ON Monta_Servico.IdMServico = Monta_Servico_Valores.IdMServico INNER JOIN
                //S_Bases ON Monta_Servico_Valores.Base_id = S_Bases.Base_id INNER JOIN
                //Supplier ON Monta_Servico.S_id = Supplier.S_id
                //WHERE(Monta_Servico.SubItem_id = 5) AND(Monta_Servico.ItemSubServico_id = 47) AND(Monta_Servico.MServico_DataFrom <= '20190209') AND(Monta_Servico.MServico_DataTo >= '20190209') AND(S_Bases.Base_de <= 2) AND
                //(S_Bases.Base_ate >= 2)
                #endregion

                return (from mon in Con.Monta_Servico
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on msv.Base_id equals sba.Base_id
                        join spp in Con.Supplier on mon.S_id equals spp.S_id
                        where mon.SubItem_id == SubItem_id &&
                               mon.ItemSubServico_id == ItemSubServico_id &&
                               mon.MServico_DataFrom <= dtFrom &&
                               mon.MServico_DataTo >= dtTo &&
                               sba.Base_de <= Base_de &&
                               sba.Base_ate >= Base_ate &&
                               spp.S_id == S_id
                        select new retornoGetValoresSubsModel
                        {
                            MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total == null ? 0 : (decimal)msv.MServico_Valores_Bulk_Total,
                            TipoBase_id = (int)sba.TipoBase_id,
                            Moeda_id = sba.Moeda_id
                        }).Distinct().FirstOrDefault();



            }
            catch
            {
                throw;
            }
        }


        public object ListarTodosTransfers(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome)
        {
            try
            {

                return (from mon in Con.Monta_Servico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                        join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataFrom &&
                              mon.MServico_DataTo >= DataTo &&
                              ssv.Servicos_transfer != null &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              scp.SubServico != null &&
                              ssv.Servicos_Nome.Equals(nome)
                        select new
                        {
                            ssv.Servicos_Nome,
                            sup.S_nome,
                            sup.S_id,
                            ssv.Recommended,
                            mon.MServico_DataFrom,
                            mon.MServico_DataTo,
                            mod.Moeda_sigla,
                            mod.Moeda_id,
                            msv.MServico_Valores_Bulk_Total,
                            sba.Base_de,
                            sba.Base_ate
                        }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfers(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {

                if (IdServico > 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Id == IdServico &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Id == IdServico &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                Recomended = sup.Recomended,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                Recomended = sup.Recomended,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }
                else
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                Recomended = sup.Recomended,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstTransfersModel> ListarTodosTransfersRecomm(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {

                if (IdServico > 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Id == IdServico &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Id == IdServico &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                Recomended = sup.Recomended,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                Recomended = sup.Recomended,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }
                else
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Servicos_transfer != null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstTransfersModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                Recomended = sup.Recomended,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<S_Servicos> ListarNomeTranfers(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs)
        {
            try
            {

                var query = (from mon in Con.Monta_Servico
                             join sup in Con.Supplier on mon.S_id equals sup.S_id
                             join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                             join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                             join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                             join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                             join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                             where mon.MServico_DataFrom <= DataFrom &&
                                   mon.MServico_DataTo >= DataTo &&
                                   ssv.Servicos_transfer != null &&
                                   ssv.Cid_Id == IdCidade &&
                                   sup.S_id == IdSupplier &&
                                   sba.Base_de <= NumPaxs &&
                                   sba.Base_ate >= NumPaxs &&
                                   scp.SubServico != null &&
                                   ssv.STATUS_id == 1
                                   ||
                                   mon.MServico_DataFrom <= DataTo &&
                                   mon.MServico_DataTo >= DataFrom &&
                                   ssv.Servicos_transfer != null &&
                                   ssv.Cid_Id == IdCidade &&
                                   sup.S_id == IdSupplier &&
                                   sba.Base_de <= NumPaxs &&
                                   sba.Base_ate >= NumPaxs &&
                                   scp.SubServico != null &&
                                   ssv.STATUS_id == 1
                             select ssv).ToList();


                var query2 = (from sco in Con.Servico_Completo
                              join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                              join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                              join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                              join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                              join spp in Con.Supplier on mon.S_id equals spp.S_id
                              join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                              where mon.MServico_DataFrom <= DataTo &&
                                        mon.MServico_DataTo >= DataFrom &&
                                        sba.Base_de <= NumPaxs &&
                                        sba.Base_ate >= NumPaxs &&
                                        sco.SubServico == null &&
                                        sse.Servicos_transfer != null &&
                                        sse.Cid_Id == IdCidade &&
                                        mon.S_id == IdSupplier &&
                                        sse.STATUS_id == 1
                              select sse).ToList();


                query.AddRange(query2);

                var lst = query.OrderBy(s => s.Servicos_Nome).ToList();

                return lst;
            }
            catch
            {
                throw;
            }
        }

        public object ListarNomeTranfersCidade(int IdCidade, DateTime dtfrom, DateTime dtto, int NumPaxs)
        {
            try
            {

                return ((from spp in Con.Supplier
                         join sba in Con.S_Bases on spp.S_id equals sba.S_id
                         join mon in Con.Monta_Servico on spp.S_id equals mon.S_id
                         join sev in Con.Servico_Completo on mon.IdMServico equals sev.IdMServico
                         join sse in Con.S_Servicos on sev.Servicos_Id equals sse.Servicos_Id
                         where mon.MServico_DataFrom <= dtfrom &&
                               mon.MServico_DataTo >= dtto &&
                               sse.Servicos_transfer != null &&
                               sse.Cid_Id == IdCidade &&
                               sba.Base_de <= NumPaxs &&
                               sba.Base_ate >= NumPaxs &&
                               sev.SubServico != null
                         orderby sse.Servicos_Nome
                         select sse).Distinct().ToList())
                        .Union
                ((from spp in Con.Supplier
                  join sba in Con.S_Bases on spp.S_id equals sba.S_id
                  join mon in Con.Monta_Servico on spp.S_id equals mon.S_id
                  join sev in Con.Servico_Completo on mon.IdMServico equals sev.IdMServico_Temporada
                  join sse in Con.S_Servicos on sev.Servicos_Id equals sse.Servicos_Id
                  where mon.MServico_DataFrom <= dtfrom &&
                        mon.MServico_DataTo >= dtto &&
                        sse.Cid_Id == IdCidade &&
                        sev.SubServico == null &&
                        sse.Servicos_transfer != null
                  orderby sse.Servicos_Nome
                  select sse).Distinct().ToList());
            }
            catch
            {
                throw;
            }
        }

        public List<S_Servicos> ListarNomeTours(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs)
        {
            try
            {

                var query = (from mon in Con.Monta_Servico
                             join sup in Con.Supplier on mon.S_id equals sup.S_id
                             join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                             join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                             join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                             join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                             join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                             where mon.MServico_DataFrom <= DataFrom &&
                                   mon.MServico_DataTo >= DataTo &&
                                   ssv.Servicos_transfer == null &&
                                   ssv.Cid_Id == IdCidade &&
                                   sup.S_id == IdSupplier &&
                                   sba.Base_de <= NumPaxs &&
                                   sba.Base_ate >= NumPaxs &&
                                   scp.SubServico != null &&
                                   ssv.STATUS_id == 1
                                   ||
                                   mon.MServico_DataFrom <= DataTo &&
                                   mon.MServico_DataTo >= DataFrom &&
                                   ssv.Servicos_transfer == null &&
                                   ssv.Cid_Id == IdCidade &&
                                   sup.S_id == IdSupplier &&
                                   sba.Base_de <= NumPaxs &&
                                   sba.Base_ate >= NumPaxs &&
                                   scp.SubServico != null &&
                                   ssv.STATUS_id == 1
                             select ssv).ToList();


                var query2 = (from sco in Con.Servico_Completo
                              join mon in Con.Monta_Servico on sco.IdMServico_Temporada equals mon.IdMServico
                              join sse in Con.S_Servicos on sco.Servicos_Id equals sse.Servicos_Id
                              join sbv in Con.S_Bases_Valor on mon.IdMServico equals sbv.IdMServico
                              join sba in Con.S_Bases on sbv.Base_id equals sba.Base_id
                              join spp in Con.Supplier on mon.S_id equals spp.S_id
                              join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                              where mon.MServico_DataFrom <= DataTo &&
                                        mon.MServico_DataTo >= DataFrom &&
                                        sba.Base_de <= NumPaxs &&
                                        sba.Base_ate >= NumPaxs &&
                                        sco.SubServico == null &&
                                        sse.Servicos_transfer == null &&
                                        sse.Cid_Id == IdCidade &&
                                        mon.S_id == IdSupplier &&
                                        sse.STATUS_id == 1
                              select sse).ToList();

                query.AddRange(query2);

                var lst = query.OrderBy(s => s.Servicos_Nome).ToList();
                return lst;
            }
            catch
            {
                throw;
            }
        }

        public object ListarNomeToursCidade(int IdCidade, DateTime dtfrom, DateTime dtto, int NumPaxs)
        {
            try
            {

                return ((from spp in Con.Supplier
                         join sba in Con.S_Bases on spp.S_id equals sba.S_id
                         join mon in Con.Monta_Servico on spp.S_id equals mon.S_id
                         join sev in Con.Servico_Completo on mon.IdMServico equals sev.IdMServico
                         join sse in Con.S_Servicos on sev.Servicos_Id equals sse.Servicos_Id
                         where mon.MServico_DataFrom <= dtfrom &&
                               mon.MServico_DataTo >= dtto &&
                               sse.Servicos_transfer == null &&
                               sse.Cid_Id == IdCidade &&
                               sba.Base_de <= NumPaxs &&
                               sba.Base_ate >= NumPaxs &&
                               sev.SubServico != null
                         orderby sse.Servicos_Nome
                         select sse).Distinct().ToList())
                        .Union
                ((from spp in Con.Supplier
                  join sba in Con.S_Bases on spp.S_id equals sba.S_id
                  join mon in Con.Monta_Servico on spp.S_id equals mon.S_id
                  join sev in Con.Servico_Completo on mon.IdMServico equals sev.IdMServico_Temporada
                  join sse in Con.S_Servicos on sev.Servicos_Id equals sse.Servicos_Id
                  where mon.MServico_DataFrom <= dtfrom &&
                        mon.MServico_DataTo >= dtto &&
                        sse.Cid_Id == IdCidade &&
                        sev.SubServico == null &&
                        sse.Servicos_transfer == null
                  orderby sse.Servicos_Nome
                  select sse).Distinct().ToList());

            }
            catch
            {
                throw;
            }
        }

        public Monta_Servico_Valores ObterValorTransfers(DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, string nomeServ)
        {
            try
            {

                return (from mon in Con.Monta_Servico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                        join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on msv.Base_id equals sba.Base_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataFrom &&
                              mon.MServico_DataTo >= DataTo &&
                              sup.S_id == IdSupplier &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              ssv.Servicos_Nome.Equals(nomeServ) &&
                              scp.SubServico != null
                              ||
                              mon.MServico_DataFrom <= DataTo &&
                              mon.MServico_DataTo >= DataFrom &&
                              sup.S_id == IdSupplier &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              ssv.Servicos_Nome.Equals(nomeServ) &&
                              scp.SubServico != null
                        select msv).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public Monta_Servico_Valores ObterValorTransfers(DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, string nomeServ, int IdCidade)
        {
            try
            {

                return (from mon in Con.Monta_Servico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                        join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on msv.Base_id equals sba.Base_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataFrom &&
                              mon.MServico_DataTo >= DataTo &&
                              sup.S_id == IdSupplier &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              ssv.Servicos_Nome.Equals(nomeServ) &&
                              scp.SubServico != null &&
                              ssv.Cid_Id == IdCidade
                              ||
                              mon.MServico_DataFrom <= DataTo &&
                              mon.MServico_DataTo >= DataFrom &&
                              sup.S_id == IdSupplier &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              ssv.Servicos_Nome.Equals(nomeServ) &&
                              scp.SubServico != null &&
                              ssv.Cid_Id == IdCidade
                        select msv).Distinct().SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public Monta_Servico_Valores ObterValorTransfers_(DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, string nomeServ, int IdCidade)
        {
            try
            {
                return (from mon in Con.Monta_Servico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                        join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on msv.Base_id equals sba.Base_id
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        where mon.MServico_DataFrom <= DataFrom &&
                              mon.MServico_DataTo >= DataTo &&
                              sup.S_id == IdSupplier &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              ssv.Servicos_Nome.Equals(nomeServ) &&
                              scp.SubServico != null &&
                              ssv.Cid_Id == IdCidade
                              ||
                              mon.MServico_DataFrom <= DataTo &&
                              mon.MServico_DataTo >= DataFrom &&
                              sup.S_id == IdSupplier &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              ssv.Servicos_Nome.Equals(nomeServ) &&
                              scp.SubServico != null &&
                              ssv.Cid_Id == IdCidade
                        select msv).Distinct().SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosTours(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Id == IdServico &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Id == IdServico &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                Recomended = sup.Recomended,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                Recomended = sup.Recomended,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }
                else
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Cid_Id == IdCidade &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Servicos_transfer == null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                Recomended = sup.Recomended,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosToursRecomm(int IdCidade, DateTime DataFrom, DateTime DataTo, int IdSupplier, int NumPaxs, int IdServico)
        {
            try
            {
                if (IdServico > 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Id == IdServico &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Id == IdServico &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                Recomended = sup.Recomended,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }
                else
                    if (IdSupplier > 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == IdSupplier &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                Recomended = sup.Recomended,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }
                else
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Cid_Id == IdCidade &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                                  ||
                                  mon.MServico_DataFrom <= DataTo &&
                                  mon.MServico_DataTo >= DataFrom &&
                                  ssv.Servicos_transfer == null &&
                                  ssv.Cid_Id == IdCidade &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                Recomended = sup.Recomended,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosTours(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade)
        {
            try
            {
                return (from mon in Con.Monta_Servico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                        join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                        join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                        where mon.MServico_DataFrom <= DataFrom &&
                              mon.MServico_DataTo >= DataTo &&
                              ssv.Servicos_transfer == null &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              scp.SubServico != null &&
                              ssv.Servicos_Nome.Equals(nome) &&
                              ssv.Cid_Id == IdCidade
                        select new
                        {
                            ssv.Servicos_Nome,
                            sup.S_nome,
                            sup.S_id,
                            mon.MServico_DataFrom,
                            mon.MServico_DataTo,
                            mod.Moeda_sigla,
                            mod.Moeda_id,
                            msv.MServico_Valores_Bulk_Total,
                            sba.Base_de,
                            sba.Base_ate,
                            sst.Tipo_Nome,
                            stg.Tipo_categ_nome
                        }).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosTours(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, int idSupplier)
        {
            try
            {

                if (idSupplier != 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Nome.Equals(nome) &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == idSupplier &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }
                else
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Nome.Equals(nome) &&
                                  ssv.Cid_Id == IdCidade &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public List<LstToursModel> ListarTodosToursRecomm(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome, int IdCidade, int idSupplier)
        {
            try
            {

                if (idSupplier != 0)
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Nome.Equals(nome) &&
                                  ssv.Cid_Id == IdCidade &&
                                  sup.S_id == idSupplier &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }
                else
                {
                    return (from mon in Con.Monta_Servico
                            join sup in Con.Supplier on mon.S_id equals sup.S_id
                            join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                            join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                            join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                            join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                            join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                            join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                            join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                            where mon.MServico_DataFrom <= DataFrom &&
                                  mon.MServico_DataTo >= DataTo &&
                                  ssv.Servicos_transfer == null &&
                                  sba.Base_de <= NumPaxs &&
                                  sba.Base_ate >= NumPaxs &&
                                  scp.SubServico != null &&
                                  ssv.Servicos_Nome.Equals(nome) &&
                                  ssv.Cid_Id == IdCidade &&
                                  ssv.Recommended == true &&
                                  ssv.STATUS_id == 1
                            select new LstToursModel
                            {
                                Servicos_Nome = ssv.Servicos_Nome,
                                Servicos_Id = ssv.Servicos_Id,
                                S_nome = sup.S_nome,
                                S_id = sup.S_id,
                                MServico_DataFrom = (DateTime)mon.MServico_DataFrom,
                                MServico_DataTo = (DateTime)mon.MServico_DataTo,
                                Moeda_sigla = mod.Moeda_sigla,
                                Moeda_id = mod.Moeda_id,
                                MServico_Valores_Bulk_Total = msv.MServico_Valores_Bulk_Total,
                                Base_de = sba.Base_de,
                                Base_ate = sba.Base_ate,
                                TipoBase = sba.Tipo_Base.TipoBase_nome,
                                TipoBaseId = (int)sba.TipoBase_id,
                                Tipo_Nome = sst.Tipo_Nome,
                                Tipo_categ_nome = stg.Tipo_categ_nome
                            }).ToList();
                }

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosTours(DateTime DataFrom, DateTime DataTo, int NumPaxs, string nome)
        {
            try
            {
                return (from mon in Con.Monta_Servico
                        join sup in Con.Supplier on mon.S_id equals sup.S_id
                        join scp in Con.Servico_Completo on mon.IdMServico equals scp.IdMServico
                        join ssv in Con.S_Servicos on scp.Servicos_Id equals ssv.Servicos_Id
                        join msv in Con.Monta_Servico_Valores on mon.IdMServico equals msv.IdMServico
                        join sba in Con.S_Bases on new { SID = (int)sup.S_id, BAS = (int)msv.Base_id } equals new { SID = (int)sba.S_id, BAS = (int)sba.Base_id }
                        join mod in Con.Moeda on sba.Moeda_id equals mod.Moeda_id
                        join sst in Con.S_Servicos_Tipo on mon.Tipo_Id equals sst.Tipo_Id
                        join stg in Con.S_Servicos_Tipo_Categ on mon.Tipo_categ_id equals stg.Tipo_categ_id
                        where mon.MServico_DataFrom <= DataFrom &&
                              mon.MServico_DataTo >= DataTo &&
                              ssv.Servicos_transfer == null &&
                              sba.Base_de <= NumPaxs &&
                              sba.Base_ate >= NumPaxs &&
                              scp.SubServico != null &&
                              ssv.Servicos_Nome.Equals(nome)
                        select new
                        {
                            ssv.Servicos_Nome,
                            sup.S_nome,
                            sup.S_id,
                            ssv.Recommended,
                            mon.MServico_DataFrom,
                            mon.MServico_DataTo,
                            mod.Moeda_sigla,
                            mod.Moeda_id,
                            msv.MServico_Valores_Bulk_Total,
                            sba.Base_de,
                            sba.Base_ate,
                            sst.Tipo_Nome,
                            stg.Tipo_categ_nome
                        }).ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodos(int IdSupplier)
        {
            try
            {
                //return Con.Monta_Servico.Where(m => m.S_id == IdSupplier).ToList();

                return (from ms in Con.Monta_Servico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        where ms.S_id == IdSupplier
                        select new
                        {
                            ms.IdMServico,
                            ms.MServico_Obs,
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            st.Tipo_categ_nome,
                            ss.Tipo_Nome,
                            tt.TipoTrans_nome,
                            S_nome = ms.Supplier.S_nome,
                            S_telefone = ms.Supplier.S_telefone,
                            S_email = ms.Supplier.S_email
                            //bs.Base_id
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSubServico(int IdSupplier)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        where ms.S_id == IdSupplier &&
                              ms.Tipo_categ_id == null
                        select new
                        {
                            ms.IdMServico,
                            ms.MServico_Obs,
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            ss.Tipo_Nome,
                            S_nome = ms.Supplier.S_nome,
                            S_telefone = ms.Supplier.S_telefone,
                            S_email = ms.Supplier.S_email
                            //bs.Base_id
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSubServicoSub(int IdSupplier)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        where ms.S_id == IdSupplier &&
                              ms.Tipo_categ_id == null
                        select new
                        {
                            ms.IdMServico,
                            ms.MServico_Obs,
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            S_nome = ms.Supplier.S_nome,
                            S_telefone = ms.Supplier.S_telefone,
                            S_email = ms.Supplier.S_email
                            //bs.Base_id
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<GridCadastradosSubModel> ListarTodosSubServicoSub_model(int IdSupplier, int IdServico)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico_Temporada
                        where ms.Tipo_categ_id == null &&
                              ms.S_id == IdSupplier &&
                              sc.Servicos_Id == IdServico
                        select new GridCadastradosSubModel
                        {
                            IdMServico = ms.IdMServico,
                            MServico_Obs = ms.MServico_Obs,
                            MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                            MServico_DataTo = (DateTime)ms.MServico_DataTo,
                            S_nome = ms.Supplier.S_nome,
                            S_telefone = ms.Supplier.S_telefone,
                            S_email = ms.Supplier.S_email

                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSubServicoSub(int IdSupplier, int IdServico)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico_Temporada
                        where ms.Tipo_categ_id == null &&
                              ms.S_id == IdSupplier &&
                              sc.Servicos_Id == IdServico
                        select new
                        {
                            ms.IdMServico,
                            ms.MServico_Obs,
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            S_nome = ms.Supplier.S_nome,
                            S_telefone = ms.Supplier.S_telefone,
                            S_email = ms.Supplier.S_email

                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosCondicaoMServico(int IdSupplier, DateTime From, DateTime To)
        {
            try
            {
                return (from ms in Con.Monta_Servico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        join mo in Con.Moeda on bs.Moeda_id equals mo.Moeda_id
                        where ms.S_id == IdSupplier &&
                              ms.MServico_DataFrom <= From &&
                              ms.MServico_DataTo >= To
                              ||
                              ms.S_id == IdSupplier &&
                              ms.MServico_DataFrom <= To &&
                              ms.MServico_DataTo >= From
                        select new
                        {
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            st.Tipo_categ_nome,
                            ms.MServico_Obs,
                            ms.IdMServico,
                            mo.Moeda_sigla
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosCondicaoMServicoSub(int IdSupplier, DateTime From, DateTime To)
        {
            try
            {
                return (from ms in Con.Monta_Servico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        join mo in Con.Moeda on bs.Moeda_id equals mo.Moeda_id
                        where ms.S_id == IdSupplier &&
                              ms.MServico_DataFrom <= From &&
                              ms.MServico_DataTo >= To
                              ||
                              ms.S_id == IdSupplier &&
                              ms.MServico_DataFrom <= To &&
                              ms.MServico_DataTo >= From
                        select new
                        {
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            ms.MServico_Obs,
                            ItemSubServico = ms.ItemSubServico.ItemSubServico_nome,
                            SubItem = ms.SubItem.SubItem_nome,
                            ms.IdMServico,
                            mo.Moeda_sigla,
                            bs.Base_index
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<SubsPorSupplierModel> ListarTodosCondicaoMServicoSub(int IdSupplier, DateTime From, DateTime To, int IndexBase)
        {
            try
            {
                return (from ms in Con.Monta_Servico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        join mo in Con.Moeda on bs.Moeda_id equals mo.Moeda_id
                        where ms.S_id == IdSupplier &&
                              ms.MServico_DataFrom <= From &&
                              ms.MServico_DataTo >= To &&
                              bs.Base_index == IndexBase
                              ||
                              ms.S_id == IdSupplier &&
                              ms.MServico_DataFrom <= To &&
                              ms.MServico_DataTo >= From &&
                              bs.Base_index == IndexBase
                        select new SubsPorSupplierModel
                        {
                            MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                            MServico_DataTo = (DateTime)ms.MServico_DataTo,
                            MServico_Obs = ms.MServico_Obs,
                            ItemSubServico = ms.ItemSubServico.ItemSubServico_nome,
                            SubItem = ms.SubItem.SubItem_nome,
                            IdMServico = ms.IdMServico,
                            Moeda_sigla = mo.Moeda_sigla,
                            Base_index = (int)bs.Base_index
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosCondicaoPorServico(int IdServico)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        join su in Con.Supplier on ms.S_id equals su.S_id
                        where sc.Servicos_Id == IdServico &&
                        sc.SubServico == false
                        select new
                        {
                            su.S_nome,
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<PeriodoSimplesModel> ListarTodosCondicaoPorServico_model(int IdServico)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        join su in Con.Supplier on ms.S_id equals su.S_id
                        where sc.Servicos_Id == IdServico &&
                        sc.SubServico == false
                        select new PeriodoSimplesModel
                        {
                            S_nome = su.S_nome,
                            MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                            MServico_DataTo = (DateTime)ms.MServico_DataTo
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosCondicaoPorSubServico(int IdServico)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico_Temporada
                        where sc.Servicos_Id == IdServico &&
                              sc.SubServico == null
                        select new
                        {
                            ms.Supplier.S_nome,
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo
                        }).Distinct().ToList();


            }
            catch
            {
                throw;
            }
        }

        public List<PeriodoSimplesModel> ListarTodosCondicaoPorSubServico_model(int IdServico)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico_Temporada
                        where sc.Servicos_Id == IdServico &&
                              sc.SubServico == null
                        select new PeriodoSimplesModel
                        {
                            S_nome = ms.Supplier.S_nome,
                            MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                            MServico_DataTo = (DateTime)ms.MServico_DataTo
                        }).Distinct().ToList();


            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosCondicaoMServico(int IdSupplier, int IdServico)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        where ms.S_id == IdSupplier &&
                              sc.Servicos_Id == IdServico
                        select new
                        {
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            st.Tipo_categ_nome,
                            ms.MServico_Obs,
                            ms.IdMServico,
                            bs.Mercado.Mercado_nome,
                            bs.Mercado_id,
                            bs.BaseTarifaria_id,
                            bs.TipoBase_id,
                            bs.Moeda_id
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<PeriodosCadastradosSozinhoModel> ListarTodosCondicaoMServico_model(int IdSupplier, int IdServico)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        where ms.S_id == IdSupplier &&
                              sc.Servicos_Id == IdServico
                        select new PeriodosCadastradosSozinhoModel
                        {
                            MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                            MServico_DataTo = (DateTime)ms.MServico_DataTo,
                            Tipo_categ_nome = st.Tipo_categ_nome,
                            MServico_Obs = ms.MServico_Obs,
                            IdMServico = ms.IdMServico,
                            Mercado_nome = bs.Mercado.Mercado_nome,
                            Mercado_id = bs.Mercado_id,
                            BaseTarifaria_id = bs.BaseTarifaria_id,
                            TipoBase_id = bs.TipoBase_id == null ? 0 : (int)bs.TipoBase_id,
                            Moeda_id = bs.Moeda_id
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosCondicaoMServico(int IdSupplier)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join sc in Con.Servico_Completo on ms.IdMServico equals sc.IdMServico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        where ms.S_id == IdSupplier
                        select new
                        {
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            st.Tipo_categ_nome,
                            ms.MServico_Obs,
                            ms.IdMServico
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosCondicaoMS(int IdSupplier)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        where ms.S_id == IdSupplier
                        select new
                        {
                            ms.MServico_DataFrom,
                            ms.MServico_DataTo,
                            st.Tipo_categ_nome,
                            ms.MServico_Obs,
                            ms.IdMServico
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<GridSubServicosModel> ListarTodosCondicaoMSCustos(int IdSupplier)
        {
            try
            {

                return (from ms in Con.Monta_Servico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        where ms.S_id == IdSupplier
                        select new GridSubServicosModel
                        {
                            MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                            MServico_DataTo = (DateTime)ms.MServico_DataTo,
                            MServico_Obs = ms.MServico_Obs,
                            ItemSubServico_nome = ms.ItemSubServico.ItemSubServico_nome,
                            SubItem_nome = ms.SubItem.SubItem_nome,
                            IdMServico = ms.IdMServico,
                            Base_index = (int)bs.Base_index
                        }).Distinct().OrderByDescending(s => s.MServico_DataFrom).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<GridSubServicosModel> ListarTodosCondicaoMSCustos(int IdSupplier, DateTime From, DateTime To, int IdItemSubServico, int IdSubItem, int IndexBase)
        {
            try
            {

                var query = (from ms in Con.Monta_Servico
                             join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                             join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                             join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                             where ms.S_id == IdSupplier
                             where ms.MServico_DataFrom <= (From) &&
                                   ms.MServico_DataTo >= (To) &&
                                   ms.ItemSubServico_id == IdItemSubServico &&
                                   ms.SubItem_id == IdSubItem &&
                                   bs.Base_index == IndexBase
                                   ||
                                   ms.MServico_DataFrom <= (To) &&
                                   ms.MServico_DataTo >= (From) &&
                                   ms.ItemSubServico_id == IdItemSubServico &&
                                   ms.SubItem_id == IdSubItem &&
                                   bs.Base_index == IndexBase
                             select new GridSubServicosModel
                             {
                                 MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                                 MServico_DataTo = (DateTime)ms.MServico_DataTo,
                                 MServico_Obs = ms.MServico_Obs,
                                 ItemSubServico_nome = ms.ItemSubServico.ItemSubServico_nome,
                                 SubItem_nome = ms.SubItem.SubItem_nome,
                                 IdMServico = ms.IdMServico,
                                 Base_index = (int)bs.Base_index
                             }).Distinct().OrderByDescending(s => s.MServico_DataFrom).ToList();

                var query2 = (from ms in Con.Monta_Servico
                              join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                              join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                              join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                              where ms.S_id == IdSupplier
                              where ms.MServico_DataFrom <= (From) &&
                                    ms.MServico_DataTo >= (To) &&
                                    ms.ItemSubServico_id == IdItemSubServico &&
                                    bs.Base_index == IndexBase
                                    ||
                                    ms.MServico_DataFrom <= (To) &&
                                    ms.MServico_DataTo >= (From) &&
                                    ms.ItemSubServico_id == IdItemSubServico &&
                                    bs.Base_index == IndexBase
                              select new GridSubServicosModel
                              {
                                  MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                                  MServico_DataTo = (DateTime)ms.MServico_DataTo,
                                  MServico_Obs = ms.MServico_Obs,
                                  ItemSubServico_nome = ms.ItemSubServico.ItemSubServico_nome,
                                  SubItem_nome = ms.SubItem.SubItem_nome,
                                  IdMServico = ms.IdMServico,
                                  Base_index = (int)bs.Base_index
                              }).Distinct().OrderByDescending(s => s.MServico_DataFrom).ToList();

                var query3 = (from ms in Con.Monta_Servico
                              join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                              join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                              join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                              where ms.S_id == IdSupplier
                              where ms.MServico_DataFrom <= (From) &&
                                    ms.MServico_DataTo >= (To) &&
                                    ms.SubItem_id == IdSubItem &&
                                    bs.Base_index == IndexBase
                                    ||
                                    ms.MServico_DataFrom <= (To) &&
                                    ms.MServico_DataTo >= (From) &&
                                    ms.SubItem_id == IdSubItem &&
                                    bs.Base_index == IndexBase
                              select new GridSubServicosModel
                              {
                                  MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                                  MServico_DataTo = (DateTime)ms.MServico_DataTo,
                                  MServico_Obs = ms.MServico_Obs,
                                  ItemSubServico_nome = ms.ItemSubServico.ItemSubServico_nome,
                                  SubItem_nome = ms.SubItem.SubItem_nome,
                                  IdMServico = ms.IdMServico,
                                  Base_index = (int)bs.Base_index
                              }).Distinct().OrderByDescending(s => s.MServico_DataFrom).ToList();

                var query4 = (from ms in Con.Monta_Servico
                              join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                              join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                              join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                              where ms.S_id == IdSupplier
                              where ms.MServico_DataFrom <= (From) &&
                                    ms.MServico_DataTo >= (To) &&
                                   bs.Base_index == IndexBase
                                    ||
                                    ms.MServico_DataFrom <= (To) &&
                                    ms.MServico_DataTo >= (From) &&
                                   bs.Base_index == IndexBase
                              select new GridSubServicosModel
                              {
                                  MServico_DataFrom = (DateTime)ms.MServico_DataFrom,
                                  MServico_DataTo = (DateTime)ms.MServico_DataTo,
                                  MServico_Obs = ms.MServico_Obs,
                                  ItemSubServico_nome = ms.ItemSubServico.ItemSubServico_nome,
                                  SubItem_nome = ms.SubItem.SubItem_nome,
                                  IdMServico = ms.IdMServico,
                                  Base_index = (int)bs.Base_index
                              }).Distinct().OrderByDescending(s => s.MServico_DataFrom).ToList();


                if (IdItemSubServico > 0 && IdSubItem > 0)
                {
                    return query;
                }
                else if (IdItemSubServico > 0 && IdSubItem == 0)
                {
                    return query2;
                }
                else if (IdItemSubServico == 0 && IdSubItem > 0)
                {
                    return query3;
                }
                else
                {
                    return query4;
                }

            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosComBase(int IdSupplier)
        {
            try
            {
                //return Con.Monta_Servico.Where(m => m.S_id == IdSupplier).ToList();

                return (from ms in Con.Monta_Servico
                        join mv in Con.Monta_Servico_Valores on ms.IdMServico equals mv.IdMServico
                        join bs in Con.S_Bases on mv.Base_id equals bs.Base_id
                        join st in Con.S_Servicos_Tipo_Categ on ms.Tipo_categ_id equals st.Tipo_categ_id
                        join ss in Con.S_Servicos_Tipo on ms.Tipo_Id equals ss.Tipo_Id
                        join tt in Con.Tipo_Transporte on bs.IdTipoTrans equals tt.IdTipoTrans
                        where ms.S_id == IdSupplier
                        select new
                        {
                            ms.IdMServico,
                            ms.MServico_Obs,
                            st.Tipo_categ_nome,
                            ss.Tipo_Nome,
                            tt.TipoTrans_nome,
                            S_nome = ms.Supplier.S_nome,
                            S_telefone = ms.Supplier.S_telefone,
                            S_email = ms.Supplier.S_email
                            //mv.MServico_Valores_Bulk_Total,
                            //bs.Base_de,
                            //bs.Base_ate,
                            //bs.Base_id
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMS(int IdSupplier, int IdTipo, int IdTipoCateg)
        {
            try
            {
                return Con.Monta_Servico.Where(m => m.S_id == IdSupplier
                                                && m.Tipo_Id == IdTipo
                                                && m.Tipo_categ_id == IdTipoCateg).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMSCustos(int IdSupplier, int IdItemSubServ, int IdSubItem)
        {
            try
            {
                return Con.Monta_Servico.Where(m => m.S_id == IdSupplier
                                                && m.ItemSubServico_id == IdItemSubServ
                                                && m.SubItem_id == IdSubItem).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMS(int IdSupplier, int IdServico)
        {
            try
            {
                return (from sc in Con.Servico_Completo
                        join ms in Con.Monta_Servico on sc.IdMServico equals ms.IdMServico
                        where ms.S_id == IdSupplier &&
                              sc.Servicos_Id == IdServico &&
                              sc.SubServico == false
                        select sc).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosOb()
        {
            try
            {

                var query = (from ms in Con.Monta_Servico
                             join st in Con.S_Servicos_Tipo on ms.Tipo_Id equals st.Tipo_Id
                             join sc in Con.S_Servicos_Tipo_Categ on st.Tipo_Id equals sc.Tipo_Id
                             select new
                             {
                                 ms.IdMServico,
                                 ms.MServico_Bulk_Porc_Taxa,
                                 ms.MServico_Bulk_Porc_Imposto,
                                 ms.MServico_Bulk_Porc_Comissao,
                                 ms.MServico_PorPessoa,
                                 ms.MServico_DataFrom,
                                 ms.MServico_DataTo,
                                 ms.MServico_Obs,
                                 st.Tipo_Nome,
                                 sc.Tipo_categ_nome

                             }).ToList().OrderBy(m => m.Tipo_categ_nome);

                return query;

            }
            catch
            {
                throw;
            }
        }

        public int QtdMServico(int IdSupplier, int index)
        {
            try
            {
                return (from ms in Con.Monta_Servico
                        join sb in Con.S_Bases on ms.S_id equals sb.S_id
                        where ms.S_id == IdSupplier
                           && sb.Base_index == index
                        select ms).Count();
            }
            catch
            {
                throw;
            }
        }

        public Array ListarTodosSubServicoPorTemporadaArray(int IdTemporada, int NumPaxs)
        {
            try
            {

                var query = (from sco in Con.Servico_Completo
                             join mon in Con.Monta_Servico on sco.IdMServico equals mon.IdMServico
                             join sup in Con.Supplier on mon.S_id equals sup.S_id
                             join sba in Con.S_Bases on sup.S_id equals sba.S_id
                             join msv in Con.Monta_Servico_Valores on new { SID = (int)mon.IdMServico, BAS = (int)sba.Base_id } equals new { SID = (int)msv.IdMServico, BAS = (int)msv.Base_id }
                             join its in Con.ItemSubServico on mon.ItemSubServico_id equals its.ItemSubServico_id
                             join sbi in Con.SubItem on mon.SubItem_id equals sbi.SubItem_id
                             where sco.IdMServico_Temporada == IdTemporada &&
                                   sba.Base_de <= NumPaxs &&
                                   sba.Base_ate >= NumPaxs &&
                                   sco.ServicoCompleto_de <= NumPaxs &&
                                   sco.ServicoCompleto_ate >= NumPaxs
                             select new
                             {
                                 sup.S_nome,
                                 sba.Base_de,
                                 sba.Base_ate,
                                 msv.MServico_Valores_Bulk_Total,
                                 its.ItemSubServico_nome,
                                 sbi.SubItem_nome,
                                 sba.Moeda_id,
                                 sba.Moeda.Moeda_nome,
                                 sba.TipoBase_id
                             }).Distinct().ToList();


                return query.ToArray();
            }
            catch
            {
                throw;
            }
        }

        public Monta_Servico ObterPeriodo(int Sid, int ItemSub, int SubItem, DateTime dtFrom, DateTime dtTo)
        {
            try
            {
                return Con.Monta_Servico.Where(s => s.S_id == Sid &&
                                                    s.ItemSubServico_id == ItemSub &&
                                                    s.SubItem_id == SubItem &&
                                                    s.MServico_DataFrom <= (dtFrom) &&
                                                    s.MServico_DataTo >= (dtTo)
                                                    ||
                                                    s.S_id == Sid &&
                                                    s.ItemSubServico_id == ItemSub &&
                                                    s.SubItem_id == SubItem &&
                                                    s.MServico_DataFrom <= (dtTo) &&
                                                    s.MServico_DataTo >= (dtFrom)).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Monta_Servico ObterPeriodo_New(int Sid, int ItemSub, int SubItem, DateTime dtFrom, DateTime dtTo, int baseindex)
        {
            try
            {
                return (from mon in Con.Monta_Servico
                        join ser in Con.Servico_Completo on mon.IdMServico equals ser.IdMServico
                        where mon.S_id == Sid &&
                              mon.ItemSubServico_id == ItemSub &&
                              mon.SubItem_id == SubItem &&
                              mon.MServico_DataFrom <= (dtFrom) &&
                              mon.MServico_DataTo >= (dtTo) &&
                              ser.Base_Index == baseindex
                               ||
                              mon.S_id == Sid &&
                              mon.ItemSubServico_id == ItemSub &&
                              mon.SubItem_id == SubItem &&
                              mon.MServico_DataFrom <= (dtTo) &&
                              mon.MServico_DataTo >= (dtFrom) &&
                              ser.Base_Index == baseindex
                        select mon).Distinct().SingleOrDefault();



            }
            catch
            {
                throw;
            }
        }

        public Monta_Servico ObterPeriodo_NewFirst(int Sid, int ItemSub, int SubItem, DateTime dtFrom, DateTime dtTo, int baseindex)
        {
            try
            {
                return (from mon in Con.Monta_Servico
                        join ser in Con.Servico_Completo on mon.IdMServico equals ser.IdMServico
                        where mon.S_id == Sid &&
                              mon.ItemSubServico_id == ItemSub &&
                              mon.SubItem_id == SubItem &&
                              mon.MServico_DataFrom <= (dtFrom) &&
                              mon.MServico_DataTo >= (dtTo) &&
                              ser.Base_Index == baseindex
                               ||
                              mon.S_id == Sid &&
                              mon.ItemSubServico_id == ItemSub &&
                              mon.SubItem_id == SubItem &&
                              mon.MServico_DataFrom <= (dtTo) &&
                              mon.MServico_DataTo >= (dtFrom) &&
                              ser.Base_Index == baseindex
                        select mon).Distinct().FirstOrDefault();



            }
            catch
            {
                throw;
            }
        }

        public Monta_Servico ObterPeriodo_NewObs(int Sid, int ItemSub, int SubItem, DateTime dtFrom, DateTime dtTo, int baseindex, string obs)
        {
            try
            {
                return (from mon in Con.Monta_Servico
                        join ser in Con.Servico_Completo on mon.IdMServico equals ser.IdMServico
                        where mon.S_id == Sid &&
                              mon.ItemSubServico_id == ItemSub &&
                              mon.SubItem_id == SubItem &&
                              mon.MServico_DataFrom <= (dtFrom) &&
                              mon.MServico_DataTo >= (dtTo) &&
                              ser.Base_Index == baseindex &&
                              mon.MServico_Obs.Equals(obs)
                               ||
                              mon.S_id == Sid &&
                              mon.ItemSubServico_id == ItemSub &&
                              mon.SubItem_id == SubItem &&
                              mon.MServico_DataFrom <= (dtTo) &&
                              mon.MServico_DataTo >= (dtFrom) &&
                              ser.Base_Index == baseindex &&
                              mon.MServico_Obs.Equals(obs)
                        select mon).Distinct().FirstOrDefault();



            }
            catch
            {
                throw;
            }
        }

    }
}
