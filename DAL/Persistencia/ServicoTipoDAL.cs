﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class ServicoTipoDAL
    {

        private Model Con;

        public ServicoTipoDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Servicos_Tipo s)
        {
            try
            {
                Con.S_Servicos_Tipo.Add(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public S_Servicos_Tipo ObterPorId(int IdTipoServ)
        {
            try
            {
                return Con.S_Servicos_Tipo.Where(s => s.Tipo_Id == IdTipoServ).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Servicos_Tipo> ListarTodos()
        {
            try
            {
                return Con.S_Servicos_Tipo.OrderBy(s => s.Tipo_Nome).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<S_Servicos_Tipo> ListarTodos(string nome)
        {
            try
            {
                return Con.S_Servicos_Tipo.Where(s => s.Tipo_Nome.Contains(nome)).OrderBy(s => s.Tipo_Nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(S_Servicos_Tipo novo)
        {
            try
            {

                S_Servicos_Tipo antigo = ObterPorId(novo.Tipo_Id);

                antigo.Tipo_Nome = novo.Tipo_Nome;

                Con.SaveChanges();

            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(S_Servicos_Tipo s)
        {
            try
            {
                Con.S_Servicos_Tipo.Remove(s);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaNome(string Nome)
        {
            try
            {
                return Con.S_Servicos_Tipo.Where(s => s.Tipo_Nome.Equals(Nome)).Count() != 0;
            }
            catch 
            {                
                throw;
            }
        }

        public S_Servicos_Tipo ObterPorNome(string nome)
        {
            try
            {

                return Con.S_Servicos_Tipo.Where(t => t.Tipo_Nome.Equals(nome)).SingleOrDefault();

            }
            catch
            {
                throw;
            }
        }

    }
}
