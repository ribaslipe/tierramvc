﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class Voucher_AtYourDiscretionDAL
    {
        private Model Con;

        public Voucher_AtYourDiscretionDAL()
        {
            Con = new Model();
        }

        public List<Voucher_AtYourDiscretion> ObterPorVoucher(int voucherId)
        {
            return Con.Voucher_AtYourDiscretion.Where(a => a.VoucherId == voucherId).ToList();
        }

        public List<Voucher_AtYourDiscretion> AddDefault(int voucherId)
        {
            bool isEdited = false;
            List<Voucher_AtYourDiscretion> lvr = new List<Voucher_AtYourDiscretion>();
            for (int i = 0; i < 4; i++)
            {
                Voucher_AtYourDiscretion vayd = new Voucher_AtYourDiscretion();
                vayd.VoucherId = voucherId;

                if (i == 0)
                    vayd.Description = "Spending money & Tips";
                else if (i == 1)
                    vayd.Description = "Flights & Airport Taxes (quoted separately, except where noted)";
                else if (i == 2)
                    vayd.Description = "Drinks & Meals except where listed";
                else if (i == 3)
                    vayd.Description = "Extra Services, Optional Activities or Changes to your itinerary beyond our control";

                if (voucherId > 0)
                {
                    isEdited = true;
                    Con.Voucher_AtYourDiscretion.Add(vayd);
                }
                else
                {
                    vayd.Id = i;
                    lvr.Add(vayd);
                }
            }

            if (isEdited)
            {
                var v = Con.Voucher.Where(a => a.Id == voucherId).Single();
                v.AtYourDiscretionEdited = true;
            }

            Con.SaveChanges();

            if (voucherId > 0)
                return Con.Voucher_AtYourDiscretion.Where(a => a.VoucherId == voucherId).ToList();
            else
                return lvr;
        }

        public Voucher_AtYourDiscretion ObterPorId(int vayId)
        {
            return Con.Voucher_AtYourDiscretion.FirstOrDefault(a => a.Id == vayId);
        }

        public void Adicionar(Voucher_AtYourDiscretion vay)
        {
            Con.Voucher_AtYourDiscretion.Add(vay);
            Con.SaveChanges();
        }

        public void Update(Voucher_AtYourDiscretion vay)
        {
            var old = Con.Voucher_AtYourDiscretion.First(a => a.Id == vay.Id);
            old.Description = vay.Description;

            Con.SaveChanges();
        }

        public void Excluir(Voucher_AtYourDiscretion vay)
        {
            Con.Voucher_AtYourDiscretion.Remove(vay);
            Con.SaveChanges();
        }

        public void ExcluirTodos(int voucherId)
        {
            foreach (var item in ObterPorVoucher(voucherId))
            {
                Con.Voucher_AtYourDiscretion.Remove(item);
            }

            Con.SaveChanges();
        }
    }
}
