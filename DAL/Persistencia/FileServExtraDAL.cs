﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class FileServExtraDAL
    {
        private Model Con;

        public FileServExtraDAL()
        {
            Con = new Model();
        }

        public List<File_ServExtra> ListarTodos_Spp(int IdSupplier)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public int Salvar(File_ServExtra f)
        {
            try
            {
                Con.File_ServExtra.Add(f);
                Con.SaveChanges();
                return f.File_ServExtra_id;
            }
            catch 
            {                
                throw;
            }
        }

        public File_ServExtra ObterPorId(int IdFileExtra)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.File_ServExtra_id == IdFileExtra).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void AtualizarSupplier(File_ServExtra novo)
        {
            try
            {
                File_ServExtra ftssnew = ObterPorId(novo.File_ServExtra_id);

                ftssnew.S_nome = novo.S_nome;
                ftssnew.S_id = novo.S_id;
                ftssnew.Ext_CID_id = novo.Ext_CID_id;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.File_ServExtra_id        = novo.File_ServExtra_id;
                antigo.File_ServExtra_venda     = novo.File_ServExtra_venda;
                antigo.File_ServExtra_vendaNet  = novo.File_ServExtra_vendaNet;
                antigo.File_ServExtra_total     = novo.File_ServExtra_total;
                antigo.File_ServExtra_valor     = novo.File_ServExtra_valor;
                antigo.markup                   = novo.markup;
                antigo.markupNet                = novo.markupNet;
                antigo.desconto                 = novo.desconto;
                antigo.descontoNet              = novo.descontoNet;
                antigo.Qtd_ServExtr             = novo.Qtd_ServExtr;
                antigo.UpdVenda                 = novo.UpdVenda;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(File_ServExtra f)
        {
            try
            {
                Con.File_ServExtra.Remove(f);
                Con.SaveChanges();
            }
            catch 
            {
                
                throw;
            }
        }

        public void AtualizarStatus(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.Status = novo.Status;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarBulk(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.TBulk = novo.TBulk;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOptional(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.TOptional = novo.TOptional;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarFlight(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.Flights_id = novo.Flights_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarPaxs(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.Qtd_ServExtr = novo.Qtd_ServExtr;
                antigo.Paxs = novo.Paxs;
                antigo.Paying_Pax = novo.Paying_Pax;
                antigo.Supp_Paying_Pax = novo.Supp_Paying_Pax;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<File_ServExtra> ListarTodos(int IdFile, int IdSupplier)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.File_id == IdFile &&
                                                   f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_ServExtra> ListarTodos(int IdFile)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.File_id == IdFile &&
                                                     f.Range_id == null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_ServExtra> ListarTodosSpp(int idspp)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.S_id == idspp).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_ServExtra> ListarTodosRangeZero(int IdFile)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.File_id == IdFile &&
                                                     f.Range_id != null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_ServExtra> ListarTodosNew(int IdFile)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.File_id == IdFile && f.Range_id == null).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_ServExtra> ListarTodosPorOrdem(int IdFile, int Ordem)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.File_id == IdFile &&
                                                     f.Ordem >= Ordem).ToList();
            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem(int IdFile)
        {
            try
            {
                var item = Con.File_ServExtra.Where(f => f != null && f.File_id == IdFile).Max(f => f.Ordem);

                if (item != null)
                {
                    return Convert.ToInt32(item);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem_(int IdFile)
        {
            try
            {
                var item = Con.File_ServExtra.Where(f => f != null && f.File_id == IdFile).OrderByDescending(s => s.Ordem).FirstOrDefault();

                if (item != null)
                {
                    return Convert.ToInt32(item.Ordem);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOrdem(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.Ordem = novo.Ordem;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarVenda(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.File_ServExtra_vendaNet = novo.File_ServExtra_vendaNet;
                antigo.markup = novo.markup;
                antigo.File_ServExtra_venda = novo.File_ServExtra_venda;
                antigo.File_ServExtra_total = novo.File_ServExtra_total;
                antigo.UpdVenda = true;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarVendaTotal(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.File_ServExtra_venda = novo.File_ServExtra_venda;
                antigo.File_ServExtra_total = novo.File_ServExtra_total;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarDatas(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.Data_From = novo.Data_From;
                antigo.Data_To = novo.Data_To;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarRange(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.Range_id = novo.Range_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNew(File_ServExtra novo)
        {
            try
            {
                File_ServExtra antigo = ObterPorId(novo.File_ServExtra_id);

                antigo.Qtd_ServExtr = novo.Qtd_ServExtr;
                antigo.Paying_Pax = novo.Paying_Pax;
                antigo.Supp_Paying_Pax = novo.Supp_Paying_Pax;
                antigo.Paxs = novo.Paxs;               

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<File_ServExtra> ListarTodosOpt(int IdFile, int OptQuote)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.File_id == IdFile &&
                                                     f.OptQuote == OptQuote).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<File_ServExtra> ListarTodosOptional(int IdFile, int OptQuote)
        {
            try
            {
                return Con.File_ServExtra.Where(f => f.File_id == IdFile &&
                                                     f.OptQuote == OptQuote &&
                                                     f.TOptional == true).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
