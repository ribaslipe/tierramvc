﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class TBItinerary_HighlightsImagesDAL
    {
        private Model Con;

        public TBItinerary_HighlightsImagesDAL()
        {
            Con = new Model();
        }

        public int Adicionar(TBItinerary_HighlightsImages thi)
        {
            try
            {
                Con.TBItinerary_HighlightsImages.Add(thi);
                Con.SaveChanges();
                return thi.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Atualizar(TBItinerary_HighlightsImages thi)
        {
            try
            {
                var old = Con.TBItinerary_HighlightsImages.Where(a => a.Id == thi.Id).Single();
                old.FirstImage = thi.FirstImage;
                old.LastImage = thi.LastImage;

                Con.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TBItinerary_HighlightsImages ObterPorCode(string code, int optQuote)
        {
            return Con.TBItinerary_HighlightsImages.Where(a => a.Code == code && a.optQuote == optQuote).FirstOrDefault();
        }

        public void DeletarPorCode(string code, int optQuote)
        {
            var lst = Con.TBItinerary_HighlightsImages.Where(a => a.Code == code && a.optQuote == optQuote).ToList();

            foreach (var item in lst)
            {
                Con.TBItinerary_HighlightsImages.Remove(item);
            }

            Con.SaveChanges();
        }
    }
}
