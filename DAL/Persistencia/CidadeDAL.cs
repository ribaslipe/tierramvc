﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Entity;

namespace DAL.Persistencia
{
    public class CidadeDAL
    {
        private Model Con;

        public CidadeDAL()
        {
            Con = new Model();
        }

        //Gravar registro de cidade na tabela
        public void Salvar(Cidade c)
        {
            try
            {
                //Rotina prontas para banco
                Con.Cidade.Add(c);//insere cidade
                Con.SaveChanges();

            }
            catch
            {
                
                throw;
            }
        }

        //Método para Excluir o Cidade na base
        public void Excluir(Cidade c)
        {
            try
            {
                Con.Cidade.Remove(c); //Prepara a deleção da cidade
                Con.SaveChanges(); //Executa a transação (commit)
            }
            catch
            {
                throw;
            }
        }

        //Metodo para listar todos os Cidade cadastradas
        public List<Cidade> ListarTodos()
        {
            try
            {
                //SQL -> select * from Cidade
                return Con.Cidade.OrderBy(s => s.CID_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Cidade> ListarTodos(string nome)
        {
            try
            {
                if (nome.Equals(""))
                {
                    return Con.Cidade.OrderBy(c => c.CID_nome).OrderBy(c => c.CID_nome).ToList();
                }
                else
                {
                    return Con.Cidade.Where(c => c.CID_nome.Contains(nome)).OrderBy(c => c.CID_nome).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        //Método para obter 1 cidade pelo Id
        public Cidade ObterPorId(int IdCidade)
        {
            try
            {
                //select * from Cidade where IdCidade = ?
                return Con.Cidade.Where(c => c.CID_id == IdCidade).SingleOrDefault();

                //Single -> Retorna apenas 1 Registro e dá erro para qualquer
                //          resultado diferente de 1 Registro.
            }
            catch
            {
                throw;
            }
        }

        public List<Cidade> ListarTodos(int IdPais)
        {
            try
            {
                return Con.Cidade.Where(c => c.PAIS_id == IdPais).OrderBy(c => c.CID_nome).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<Cidade> ListarTodosSupplier(int IdPais)
        {
            try
            {

                //SELECT DISTINCT Cidade.CID_nome
                //FROM            Cidade INNER JOIN
                //Supplier ON Cidade.CID_id = Supplier.CID_id
                //ORDER BY Cidade.CID_nome

                return (from cid in Con.Cidade
                        join spp in Con.Supplier on cid.CID_id equals spp.CID_id
                        where cid.PAIS_id == IdPais
                        select cid).Distinct().OrderBy(s => s.CID_nome).ToList();


            }
            catch
            {
                throw;
            }
        }

        public List<Cidade> ListarTodosServico(int IdPais)
        {
            try
            {

                //SELECT DISTINCT Cidade.CID_nome
                //FROM            Cidade INNER JOIN
                //S_Servicos ON Cidade.CID_id = S_Servicos.Cid_Id
                //ORDER BY Cidade.CID_nome

                return (from cid in Con.Cidade
                        join srv in Con.S_Servicos on cid.CID_id equals srv.Cid_Id
                        where cid.PAIS_id == IdPais
                        select cid).Distinct().OrderBy(s => s.CID_nome).ToList();


            }
            catch
            {
                throw;
            }
        }

        public Cidade ObterPorNome(string nome)
        {
            try
            {
                return Con.Cidade.Where(c => c.CID_nome.Equals(nome)).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public bool VerificaNome(string CidNome)
        {
            try
            {
                return Con.Cidade.Where(c => c.CID_nome.Equals(CidNome)).Count() != 0;
            }
            catch 
            {                
                throw;
            }
        }

        //Método para atualizar os dados da Cidade
        public void Atualizar(Cidade registroNovo)
        {
            try
            {
                //Buscar na base de dados registro antigo
                Cidade registroAntigo = ObterPorId(registroNovo.CID_id);

                //Atualizando os dados
                registroAntigo.CID_cod_Iata = registroNovo.CID_cod_Iata;
                registroAntigo.CID_nome = registroNovo.CID_nome;
                registroAntigo.CID_uf = registroNovo.CID_uf;
                registroAntigo.PAIS_id = registroNovo.PAIS_id;
                registroAntigo.ObsTarifario = registroNovo.ObsTarifario;

                //executa o commit
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }

        }

        public Cidade ObterPorTarifa(int IdTarifa)
        {
            try
            {

                return (from spp in Con.Supplier
                        join smp in Con.S_Mercado_Periodo on spp.S_id equals smp.S_id
                        join smt in Con.S_Mercado_Tarifa on smp.S_merc_periodo_id equals smt.S_merc_periodo_id
                        join cid in Con.Cidade on spp.CID_id equals cid.CID_id
                        where smt.S_merc_tarif_id == IdTarifa
                        select cid).SingleOrDefault();

            }
            catch 
            {                
                throw;
            }
        }

        public Cidade ObterPorNomeSupplier(string supplier)
        {
            try
            {

                return (from spp in Con.Supplier
                        join cid in Con.Cidade on spp.CID_id equals cid.CID_id
                        where spp.S_nome.Equals(supplier)
                        select cid).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public Cidade ObterPorIDSupplier(int idsupplier)
        {
            try
            {

                return (from spp in Con.Supplier
                        join cid in Con.Cidade on spp.CID_id equals cid.CID_id
                        where spp.S_id.Equals(idsupplier)
                        select cid).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Cidade ObterPorNomeServico(string servico)
        {
            try
            {

                return (from cid in Con.Cidade
                        join ser in Con.S_Servicos on cid.CID_id equals ser.Cid_Id
                        where ser.Servicos_Nome.Equals(servico)
                        select cid).FirstOrDefault();
            }
            catch 
            {
                
                throw;
            }
        }

        public Cidade ObterPorIata(string iata)
        {
            try
            {
                return Con.Cidade.Where(c => c.CID_cod_Iata.Equals(iata)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
