﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class PromocaoDAL
    {
        private Model Con;

        public PromocaoDAL()
        {
            Con = new Model();
        }

        public List<Promocao> ListarTodos()
        {
            try
            {
                return Con.Promocao.ToList();
            }
            catch 
            {
                throw;
            }
        }
    }
}
