﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class ItemSubServicoDAL
    {

        private Model Con;

        public ItemSubServicoDAL()
        {
            Con = new Model();
        }

        public void Salvar(ItemSubServico i)
        {
            try
            {
                Con.ItemSubServico.Add(i);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public ItemSubServico ObterPorId(int IdItem)
        {
            try
            {
                return Con.ItemSubServico.Where(i => i.ItemSubServico_id == IdItem).SingleOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(ItemSubServico novo)
        {
            try
            {
                ItemSubServico antigo = ObterPorId(novo.ItemSubServico_id);

                antigo.ItemSubServico_nome = novo.ItemSubServico_nome;

                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(ItemSubServico i)
        {
            try
            {
                Con.ItemSubServico.Remove(i);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public List<ItemSubServico> ListarTodos()
        {
            try
            {
                return Con.ItemSubServico.OrderBy(i => i.ItemSubServico_nome).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public List<ItemSubServico> ListarTodos(string nome)
        {
            try
            {
                return Con.ItemSubServico.Where(s => s.ItemSubServico_nome.Contains(nome)).OrderBy(i => i.ItemSubServico_nome).ToList();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaNome(string nome)
        {
            try
            {
                return Con.ItemSubServico.Where(i => i.ItemSubServico_nome.Equals(nome)).Count() != 0;
            }
            catch 
            {                
                throw;
            }
        }

        public ItemSubServico ObterPorNome(string nome)
        {
            try
            {
                return Con.ItemSubServico.Where(s => s.ItemSubServico_nome == nome).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
