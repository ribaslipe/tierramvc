﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistencia
{
    public class TipoTransporteDAL
    {

        private Model Con;

        public TipoTransporteDAL()
        {
            Con = new Model();
        }

        public List<Tipo_Transporte> ListarTodos()
        {
            try
            {
                return Con.Tipo_Transporte.OrderBy(t => t.TipoTrans_nome).ToList();
            }
            catch 
            {
                throw;
            }
        }

    }
}
