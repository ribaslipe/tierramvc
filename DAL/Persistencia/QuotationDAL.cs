﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class QuotationDAL
    {

        private Model Con;

        public QuotationDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quotation q)
        {
            try
            {
                Con.Quotation.Add(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Quotation ObterPorId(int IdQuotation)
        {
            try
            {
                return Con.Quotation.Where(q => q.Quotation_Id == IdQuotation).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Quotation ObterPorCode(string Code)
        {
            try
            {
                return Con.Quotation.Where(q => q.Quotation_Code.Equals(Code)).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public Quotation ObterPorId(string QuotationCode)
        {
            try
            {
                return Con.Quotation.Where(q => q.Quotation_Code.Equals(QuotationCode)).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(Quotation novo)
        {
            try
            {
                Quotation antigo = ObterPorId(novo.Quotation_Id);

                antigo.Quotation_Code = novo.Quotation_Code;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public string ObterLast()
        {
            try
            {
                var result = (from t in Con.Quotation
                              orderby t.Quotation_Id descending
                              select t.Quotation_Code).First();

                return result;
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Quotation q)
        {
            try
            {
                Con.Quotation.Remove(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<string> ListarTodosString(string term, int idUser)
        {
            try
            {
                if (idUser > 0)
                {
                    if (term.Equals("*"))
                    {
                        return (from qt in Con.Quotation_Grupo
                                join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                                where qt.US_id == idUser &&
                                      qn.File_new == true
                                orderby qn.Quotation_Code
                                select qn).Select(s => s.Quotation_Code).Distinct().ToList();
                    }
                    else
                    {
                        return (from qt in Con.Quotation_Grupo
                                join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                                where qn.Quotation_Code.Contains(term) &&
                                   qt.US_id == idUser &&
                                   qn.File_new == true
                                orderby qn.Quotation_Code
                                select qn).Select(s => s.Quotation_Code).Distinct().ToList();
                    }
                }
                else
                {
                    if (term.Equals("*"))
                    {
                        return (from qt in Con.Quotation_Grupo
                                join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                                where qn.File_new == true
                                orderby qn.Quotation_Code
                                select qn).Select(s => s.Quotation_Code).Distinct().ToList();
                    }
                    else
                    {
                        return (from qt in Con.Quotation_Grupo
                                join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                                where qn.Quotation_Code.Contains(term) &&
                                      qn.File_new == true
                                orderby qn.Quotation_Code
                                select qn).Select(s => s.Quotation_Code).Distinct().ToList();
                    }
                }

                //return Con.Quotation.OrderBy(s => s.Quotation_Code).Select(s => s.Quotation_Code).Where(s => s.Contains(term)).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<string> ListarTodosString(string term)
        {
            try
            {

                if (term.Equals("*"))
                {
                    return (from qt in Con.Quotation_Grupo
                            join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                            where qn.File_new == true
                            orderby qn.Quotation_Code
                            select qn).Select(s => s.Quotation_Code).Distinct().ToList();
                }
                else
                {
                    return (from qt in Con.Quotation_Grupo
                            join qn in Con.Quotation on qt.Quotation_Id equals qn.Quotation_Id
                            where qn.Quotation_Code.Contains(term) &&
                                  qn.File_new == true
                            orderby qn.Quotation_Code
                            select qn).Select(s => s.Quotation_Code).Distinct().ToList();
                }


                //return Con.Quotation.OrderBy(s => s.Quotation_Code).Select(s => s.Quotation_Code).Where(s => s.Contains(term)).Distinct().ToList();
            }
            catch
            {
                throw;
            }
        }



    }
}

