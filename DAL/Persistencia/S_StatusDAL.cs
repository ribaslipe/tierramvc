﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class S_StatusDAL
    {
        private Model Con;

        public S_StatusDAL()
        {
            Con = new Model();
        }

        public List<S_Status> ListarTodos()
        {
            try
            {
                return Con.S_Status.ToList().OrderBy(t => t.STATUS_nome).ToList();
            }
            catch 
            {

                throw;
            }
           
        }
    }
}
