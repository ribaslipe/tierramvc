﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Persistencia
{
    public class Voucher_TripDAL
    {
        private Model Con;

        public Voucher_TripDAL()
        {
            Con = new Model();
        }

        public Voucher_Trip ObterPorId(int id, int voucherId)
        {
            return Con.Voucher_Trip.FirstOrDefault(a => a.Id == id && a.VoucherId == voucherId);
        }

        public List<Voucher_Trip> ListarPorVoucher(int voucherId)
        {
            return Con.Voucher_Trip.Where(a => a.VoucherId == voucherId).ToList();
        }

        public int Adicionar(Voucher_Trip vt)
        {
            Con.Voucher_Trip.Add(vt);
            Con.SaveChanges();
            return vt.Id;
        }

        public void Atualizar(Voucher_Trip vt)
        {
            var old = Con.Voucher_Trip.First(a => a.Id == vt.Id);
            old.DateRange = vt.DateRange;
            old.Description = vt.Description;
            old.Destination = vt.Destination;
            Con.SaveChanges();
        }

        public void Deletar(Voucher_Trip vt)
        {
            Con.Voucher_Trip.Remove(vt);
            Con.SaveChanges();
        }

        public void DeletarTodosVoucher(int voucherId)
        {
            var lvt = ListarPorVoucher(voucherId);

            foreach (var vt in lvt)
            {
                Con.Voucher_Trip.Remove(vt);
            }

            Con.SaveChanges();
        }
    }
}
