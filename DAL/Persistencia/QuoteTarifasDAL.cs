﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;
using DAL.Persistencia;
using DAL.Models;

namespace DAL.Persistencia
{
    public class QuoteTarifasDAL
    {

        private Model Con;

        public QuoteTarifasDAL()
        {
            Con = new Model();
        }

        public void Salvar(Quote_Tarifas q)
        {
            try
            {
                Con.Quote_Tarifas.Add(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Quote_Tarifas ObterPorId(int IdQt)
        {
            try
            {
                return Con.Quote_Tarifas.Where(q => q.Quote_Tarifas_id == IdQt).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Tarifas> ObterPorIdTC(int IdRange)
        {
            try
            {
                return Con.Quote_Tarifas.Where(q => q.Range_id == IdRange).ToList();
            }
            catch
            {
                throw;
            }
        }        

        public void ExcluirPorFile(int idFile)
        {
            try
            {
                var query = Con.Quote_Tarifas.Where(q => q.File_id == idFile).ToList();

                foreach (Quote_Tarifas item in query)
                {
                    Con.Quote_Tarifas.Remove(item);
                    Con.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public void ExcluirLista(List<Quote_Tarifas> lista)
        {
            try
            {
                foreach (Quote_Tarifas item in lista)
                {
                    Con.Quote_Tarifas.Remove(item);
                    Con.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNew(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.S_merc_tarif_venda = novo.S_merc_tarif_venda;
                antigo.S_merc_tarif_total = novo.S_merc_tarif_total;
                antigo.S_merc_tarif_valor = novo.S_merc_tarif_valor;
                antigo.markup = novo.markup;
                antigo.markupNet = novo.markupNet;
                antigo.desconto = novo.desconto;
                antigo.descontoNet = novo.descontoNet;
                antigo.Qtd_Tarifas = novo.Qtd_Tarifas;
                antigo.Paxs = novo.Paxs;
                antigo.Paying_Pax = novo.Paying_Pax;
                antigo.Supp_Paying_Pax = novo.Supp_Paying_Pax;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Quote_Tarifas q)
        {
            try
            {
                Con.Quote_Tarifas.Remove(q);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Quote_Tarifas ObterTC(int IdFile, int take)
        {
            try
            {
                take = take - 1;

                return Con.Quote_Tarifas.Where(q => q.File_id == IdFile &&
                                                    q.TC == true)
                                                    .OrderBy(q => q.Data_From)
                                                    .ThenBy(q => q.Data_To)
                                                    .ThenBy(q => q.Quote_Tarifas_id)
                                                    .Skip(take)
                                                    .Take(1).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Tarifas> ListarTodos(int IdFile)
        {
            try
            {
                return Con.Quote_Tarifas.Where(f => f.File_id == IdFile).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Tarifas> ListarTodos(int IdFile, int IdSupplier)
        {
            try
            {
                return Con.Quote_Tarifas.Where(f => f.File_id == IdFile &&
                                                   f.S_id == IdSupplier).ToList();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSuppliersQuoteTarifas(int IdFile)
        {
            try
            {
                return (from flt in Con.Quote_Tarifas
                        where flt.File_id == IdFile
                        select new
                        {
                            flt.S_nome,
                            flt.S_id
                        }).ToList().Distinct();
            }
            catch
            {
                throw;
            }
        }

        public QuoteModelRange AddModelRange(QuoteModelRange q)
        {
            try
            {
                QuoteModelRange qr = new QuoteModelRange();

                qr.valor = q.valor;
                qr.markup = q.markup;
                qr.desconto = q.desconto;
                qr.markupNet = q.markupNet;
                qr.descontoNet = q.descontoNet;
                qr.vendaNet = q.vendaNet;

                return qr;
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarValores(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.S_merc_tarif_vendaNet = novo.S_merc_tarif_vendaNet;
                antigo.S_merc_tarif_venda = novo.S_merc_tarif_venda;
                antigo.S_merc_tarif_total = novo.S_merc_tarif_total;
                antigo.S_merc_tarif_valor = novo.S_merc_tarif_valor;
                antigo.markup = novo.markup;
                antigo.markupNet = novo.markupNet;
                antigo.desconto = novo.desconto;
                antigo.descontoNet = novo.descontoNet;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaMeal(int IdFile, int IdMeal)
        {
            try
            {
                return Con.Quote_Tarifas.Where(f => f.File_id == IdFile &&
                                                    f.Meal == true &&
                                                    f.S_meal_id == IdMeal).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public bool VerificaExisteTC(int IdFile)
        {
            try
            {
                return Con.Quote_Tarifas.Where(q => q.File_id == IdFile &&
                                                    q.TC == true).Count() != 0;
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarBulk(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.TBulk = novo.TBulk;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarStatus(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.Status = novo.Status;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarFlight(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.Flights_id = novo.Flights_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarOrdem(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.Ordem = novo.Ordem;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarDatas(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.Data_From = novo.Data_From;
                antigo.Data_To = novo.Data_To;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarRange_id(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.Range_id = novo.Range_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public object ListarTodosSuppliers(int IdFile)
        {
            try
            {
                return (from flt in Con.Quote_Tarifas
                        where flt.File_id == IdFile
                        select new
                        {
                            flt.S_nome,
                            flt.S_id
                        }).ToList().Distinct();
            }
            catch
            {
                throw;
            }
        }

      

        public List<NomesGridEmail> ListarTodosSuppliers_(int IdFile)
        {
            try
            {
                return (from flt in Con.Quote_Tarifas
                        where flt.File_id == IdFile
                        select new NomesGridEmail
                        {
                            S_nome = flt.S_nome,
                            S_id = (int)flt.S_id
                        }).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<Quote_Tarifas> ListarPorNome(string nome)
        {
            try
            {
                return Con.Quote_Tarifas.Where(s => s.S_nome.Equals(nome)).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarNome(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.S_nome = novo.S_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarStatus_Plataforma(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.Status = novo.Status;
                antigo.Plataforma_id = novo.Plataforma_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarDatas_NumNoites(Quote_Tarifas novo)
        {
            try
            {
                Quote_Tarifas antigo = ObterPorId(novo.Quote_Tarifas_id);

                antigo.Data_From = novo.Data_From;
                antigo.Data_To = novo.Data_To;
                antigo.NumNoites = novo.NumNoites;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public int RetornaUltimaOrdem_(int IdFile)
        {
            try
            {
                var item = Con.Quote_Tarifas.Where(f => f != null && f.File_id == IdFile).OrderByDescending(s => s.Ordem).FirstOrDefault();

                if (item != null)
                {
                    return Convert.ToInt32(item.Ordem);
                }
                else
                {
                    return 0;
                }

            }
            catch
            {
                throw;
            }
        }

    }
}
