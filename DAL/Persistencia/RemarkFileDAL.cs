﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class RemarkFileDAL
    {

        private Model Con;

        public RemarkFileDAL()
        {
            Con = new Model();
        }

        public void Salvar(Remark_File r)
        {
            try
            {
                Con.Remark_File.Add(r);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public Remark_File ObterPorQuote(int IdQuot)
        {
            try
            {
                return Con.Remark_File.Where(r => r.Quotation_Grupo_Id == IdQuot).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public Remark_File ObterPorId(int IdRemark)
        {
            try
            {
                return Con.Remark_File.Where(r => r.remarkfile_id == IdRemark).FirstOrDefault();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualizar(Remark_File novo)
        {
            try
            {
                Remark_File antigo = ObterPorId(novo.remarkfile_id);

                antigo.remarkfile_text = novo.remarkfile_text;

                Con.SaveChanges();

            }
            catch 
            {                
                throw;
            }
        }

    }
}
