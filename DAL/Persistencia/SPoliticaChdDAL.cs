﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class SPoliticaChdDAL
    {

        private Model Con;

        public SPoliticaChdDAL()
        {
            Con = new Model();
        }

        public void Salvar(S_Politica_CHD s)
        {
            try
            {
                Con.S_Politica_CHD.Add(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public S_Politica_CHD ObterPorId(int IdPolChd)
        {
            try
            {
                return Con.S_Politica_CHD.Where(s => s.S_politicaCHD_id == IdPolChd).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(S_Politica_CHD s)
        {
            try
            {
                Con.S_Politica_CHD.Remove(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(S_Politica_CHD novo)
        {
            try
            {
                S_Politica_CHD antigo = ObterPorId(novo.S_politicaCHD_id);

                antigo.S_politicaCHD_qtd = novo.S_politicaCHD_qtd;
                antigo.S_politicaCHD_idade = novo.S_politicaCHD_idade;
                antigo.S_politicaCHD_free = novo.S_politicaCHD_free;
                antigo.S_politicaCHD_valor = novo.S_politicaCHD_valor;
                antigo.S_politicaCHD_percent = novo.S_politicaCHD_percent;
                antigo.S_politicaCHD_idadeDe = novo.S_politicaCHD_idadeDe;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }       

        public List<S_Politica_CHD> ListarTodos()
        {
            try
            {
                return Con.S_Politica_CHD.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<S_Politica_CHD> ListarTodos(int IdTarifa)
        {
            try
            {
                return (from spd in Con.S_Politica_CHD
                        join spt in Con.S_PoliticaCHDTarifas on spd.S_politicaCHD_id equals spt.S_politicaCHD_id
                        where spt.S_merc_tarif_id == IdTarifa
                        select spd).ToList();
            }
            catch
            {
                throw;
            }
        }

        public S_Politica_CHD VerificaExiste(int qtd, int idade, int idadeDe, int tipo, decimal valor, decimal percent)
        {
            try
            {

                S_Politica_CHD spc = new S_Politica_CHD();

                switch (tipo)
                {
                    case 0:
                        spc = Con.S_Politica_CHD.Where(s => s.S_politicaCHD_qtd == qtd && s.S_politicaCHD_idade == idade && s.S_politicaCHD_idadeDe == idadeDe && s.S_politicaCHD_free != null).SingleOrDefault();
                        break;
                    case 1:
                        spc =  Con.S_Politica_CHD.Where(s => s.S_politicaCHD_qtd == qtd && s.S_politicaCHD_idade == idade && s.S_politicaCHD_idadeDe == idadeDe && s.S_politicaCHD_valor == valor).SingleOrDefault();
                        break;
                    case 2:
                        spc = Con.S_Politica_CHD.Where(s => s.S_politicaCHD_qtd == qtd && s.S_politicaCHD_idade == idade && s.S_politicaCHD_idadeDe == idadeDe && s.S_politicaCHD_percent == percent).SingleOrDefault();
                        break;
                }

                return spc;

            }
            catch
            {
                throw;
            }

        }
      

    }
}
