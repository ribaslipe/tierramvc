﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class HolidaysPeriodosDAL
    {

        private Model Con;

        public HolidaysPeriodosDAL()
        {
            Con = new Model();
        }

        public List<Holidays_Periodo> ObterPorIdHolidays(int IdHol)
        {
            try
            {
                return Con.Holidays_Periodo.Where(h => h.Holidays_id == IdHol).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(Holidays_Periodo h)
        {
            try
            {
                Con.Holidays_Periodo.Add(h);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(Holidays_Periodo h)
        {
            try
            {
                Con.Holidays_Periodo.Remove(h);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Holidays_Periodo ObterPorId(int IdHoliPeriodos)
        {
            try
            {
                return Con.Holidays_Periodo.Where(h => h.HoliDays_Periodo_id == IdHoliPeriodos).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

    }
}
