﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class SubServicosFileDAL
    {

        private Model Con;

        public SubServicosFileDAL()
        {
            Con = new Model();
        }

        public void Salvar(SubServicosFile s)
        {
            try
            {
                Con.SubServicosFile.Add(s);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<SubServicosFile> ListarTodosRange(int IdRange, int IdQTransf)
        {
            try
            {
                if (IdRange != 0)
                    return Con.SubServicosFile.Where(s => s.Ranges_id == IdRange).ToList();
                else
                    return Con.SubServicosFile.Where(s => s.Quote_Transf_id == IdQTransf).ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<SubServicosFile> ListarTodosQTrsf(int IdQTrans)
        {
            try
            {

                return Con.SubServicosFile.Where(s => s.Quote_Transf_id == IdQTrans).ToList();

            }
            catch
            {
                throw;
            }
        }

        public decimal? TotalNaoPago(int idQtransf)
        {
            try
            {

                return Con.SubServicosFile.Where(c => c.Quote_Transf_id == idQtransf &&
                                                      c.Valor_Conferir == null &&
                                                      c.Cambio == null &&
                                                      c.FTC == null).ToList().Select(c => c.Valor).Sum();
            }
            catch 
            {               
                throw;
            }
        }

        public decimal QTDTodosQTrsf(int IdQTrans)
        {
            try
            {

                //return Con.SubServicosFile.Where(s => s.Quote_Transf_id == IdQTrans).Count();
                return (decimal)Con.SubServicosFile.Where(s => s.Quote_Transf_id == IdQTrans).ToList().Sum(s => s.Valor * Convert.ToInt32(s.ServDe));

            }
            catch
            {
                throw;
            }
        }

        public decimal SomaTotal(int IdRange, int IdQTrans)
        {
            try
            {
                if (IdRange != 0)
                    return Convert.ToDecimal(Con.SubServicosFile.Where(s => s.Ranges_id == IdRange).ToList().Select(c => c.Valor).Sum());
                else
                    return Convert.ToDecimal(Con.SubServicosFile.Where(s => s.Quote_Transf_id == IdQTrans).ToList().Select(c => c.Valor).Sum());
            }
            catch
            {
                throw;
            }
        }

        public SubServicosFile ObterPorId(int Id)
        {
            try
            {
                return Con.SubServicosFile.Where(s => s.SubServicosFile_id == Id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Atualiza(SubServicosFile novo)
        {
            try
            {
                SubServicosFile antigo = ObterPorId(novo.SubServicosFile_id);

                antigo.NomeFantasia = novo.NomeFantasia;
                antigo.NomeSupplier = novo.NomeSupplier;
                antigo.NomeSubItem = novo.NomeSubItem;
                antigo.ServDe = novo.ServDe;
                antigo.ServAte = novo.ServAte;
                antigo.Valor = novo.Valor;
                antigo.Moeda = novo.Moeda;
                antigo.Moeda_id = novo.Moeda_id;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void ExcluirLista(List<SubServicosFile> lista)
        {
            try
            {
                foreach (var item in lista)
                {
                    Con.SubServicosFile.Remove(item);
                    Con.SaveChanges();
                }
            }
            catch 
            {                
                throw;
            }
        }

        public void Excluir(SubServicosFile sub)
        {
            try
            {
                Con.SubServicosFile.Remove(sub);
                Con.SaveChanges();
            }
            catch 
            {                
                throw;
            }
        }

        public List<SubServicosFile> ListarTodosNome_supplier(string nome)
        {
            try
            {
                return Con.SubServicosFile.Where(s => s.NomeSupplier.Equals(nome)).ToList();
            }
            catch 
            {                
                throw;
            }
        }

        public void Atualiza_supplier(string nome, string nomeSubstituir, string Quote, int type)
        {
            try
            {
                if (type == 0)
                {
                    var query = (from quo in Con.Quotation
                                 join qtg in Con.Quotation_Grupo on quo.Quotation_Id equals qtg.Quotation_Id
                                 join fil in Con.File_Carrinho on qtg.Quotation_Grupo_Id equals fil.Quotation_Grupo_Id
                                 join qtr in Con.Quote_Transf on fil.File_id equals qtr.File_id
                                 join sub in Con.SubServicosFile on qtr.Quote_Transf_id equals sub.Quote_Transf_id
                                 where quo.Quotation_Code.Equals(Quote) &&
                                       sub.NomeSupplier.Equals(nome)
                                 select new
                                 {
                                     IDS = sub.SubServicosFile_id
                                 }).ToList();

                    foreach (var item in query)
                    {
                        SubServicosFile s = ObterPorId(item.IDS);
                        s.NomeSupplier = nomeSubstituir;
                        Atualiza(s);
                    }
                }
                else
                {
                    var query = (from quo in Con.Quotation
                                 join rge in Con.Ranges on quo.Quotation_Id equals rge.Quotation_Id
                                 join sub in Con.SubServicosFile on rge.Ranges_id equals sub.Ranges_id
                                 where quo.Quotation_Code.Equals(Quote) &&
                                       sub.NomeSupplier.Equals(nome)
                                 select new
                                 {
                                     IDS = sub.SubServicosFile_id
                                 }).ToList();

                    foreach (var item in query)
                    {
                        SubServicosFile s = ObterPorId(item.IDS);
                        s.NomeSupplier = nomeSubstituir;
                        Atualiza(s);
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public List<SubServicosFile> Soma_Subs(string codeFile, int idCliente)
        {
            try
            {

                return (from quo in Con.Quotation
                        join qgr in Con.Quotation_Grupo on quo.Quotation_Id equals qgr.Quotation_Id
                        join fcr in Con.File_Carrinho on qgr.Quotation_Grupo_Id equals fcr.Quotation_Grupo_Id
                        join cli in Con.Cliente on qgr.Cliente_id equals cli.Cliente_id
                        join qtr in Con.Quote_Transf on fcr.File_id equals qtr.File_id
                        join sub in Con.SubServicosFile on qtr.Quote_Transf_id equals sub.Quote_Transf_id
                        where quo.Quotation_Code.Equals(codeFile) &&
                              qgr.Cliente_id == idCliente &&
                              sub.Valor_Conferir == null
                        select sub).ToList();                      

            }
            catch 
            {                
                throw;
            }
        }


        public List<string> ListarTodosQTrsf_Spps(int IdQTrans)
        {
            try
            {

                return Con.SubServicosFile.Where(s => s.Quote_Transf_id == IdQTrans).Select(s => s.NomeSupplier).Distinct().ToList();

            }
            catch
            {
                throw;
            }
        }

        public List<SubServicosFile> Listar_PorSupplier(string supplier, int idQTrf)
        {
            try
            {
                return Con.SubServicosFile.Where(s => s.NomeSupplier.Equals(supplier) && s.Quote_Transf_id == idQTrf).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
