﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entity;
using DAL.Models;

namespace DAL.Persistencia
{
    public class CentroCustoDAL
    {
        private Model Con;


        public CentroCustoDAL()
        {
            Con = new Model();
        }



        public List<C_CentroCusto> consutlarTodos()
        {
            try
            {
                return Con.C_CentroCusto.OrderBy(c => c.CentroCusto_nome).ToList();
            }
            catch
            {
                throw;
            }
        }


        public C_CentroCusto pesquisarPorNome(string nome)
        {
            try
            {
                return Con.C_CentroCusto.Where(c => c.CentroCusto_nome.Equals(nome)).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }



        public C_CentroCusto pesquisarPorId(int id)
        {
            try
            {
                return Con.C_CentroCusto.Where(c => c.CentroCusto_id == id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }


        public void incluir(C_CentroCusto c)
        {

            try
            {
                Con.C_CentroCusto.Add(c);
                Con.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }


        public List<DespesasPagasModel> ListarRetornarDespesasPagas(DateTime dtFrom, DateTime dtTo)
        {
            return null;
        }


        public C_CentroCusto ObterPorId(int id)
        {
            try
            {
                return Con.C_CentroCusto.Where(s => s.CentroCusto_id == id).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }


        public void Atualizar(C_CentroCusto novo)
        {
            try
            {
                C_CentroCusto antigo = ObterPorId(novo.CentroCusto_id);

                antigo.CentroCusto_nome = novo.CentroCusto_nome;

                Con.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void Excluir(C_CentroCusto c)
        {
            try
            {

                Con.C_CentroCusto.Remove(c);
                Con.SaveChanges();

            }
            catch
            {

                throw;
            }
        }

        public List<Centro_Custo> ListarTodosC_C()
        {
            return Con.Centro_Custo.ToList();
        }


    }
}
