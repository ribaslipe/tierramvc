﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entity;

namespace DAL.Persistencia
{
    public class FLinguagemDAL
    {

        private Model Con;

        public FLinguagemDAL()
        {
            Con = new Model();
        }

        public List<File_Liguagem> ListarTodos()
        {
            try
            {
                return Con.File_Liguagem.ToList();
            }
            catch
            {
                throw;
            }
        }

        public File_Liguagem ObterPorId(int idLing)
        {
            try
            {
                return Con.File_Liguagem.Where(w => w.FLinguagem_id == idLing).SingleOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public void Salvar(File_Liguagem l)
        {
            try
            {
                Con.File_Liguagem.Add(l);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Atualizar(File_Liguagem novo)
        {
            try
            {
                File_Liguagem antigo = ObterPorId(novo.FLinguagem_id);

                antigo.FLinguagem_nome = novo.FLinguagem_nome;

                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(File_Liguagem l)
        {
            try
            {
                Con.File_Liguagem.Remove(l);
                Con.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
}
