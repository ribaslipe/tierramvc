﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using DAL.Persistencia;

namespace BLL.Utils
{
    public class Utils
    {

        public static string AdjustDateTime(string paramp, int Omda)
        {
            char[] splitter1 = { ' ' };
            char[] splitterD = { '/' };
            char[] splitterH = { ':' };
            string saida = "";

            string[] auxp = paramp.Split(splitter1);
            string[] auxpD = auxp[0].Split(splitterD);
            string[] auxpH = new string[3];
            if (auxpD.Length != 3)
            {
                splitterD[0] = '-';
                auxpD = auxp[0].Split(splitterD);
                if (auxpD.Length != 3)
                {
                    splitterD[0] = '.';
                    auxpD = auxp[0].Split(splitterD);
                    if (auxpD.Length != 3)
                    {
                        throw new FormatException();
                    }
                }
            }
            if (auxpD.Length == 3)
            {
                for (int i = 0; i <= 2; i++)
                {
                    if (auxpD[i].Length == 1)
                    {
                        auxpD[i] = "0" + auxpD[i];
                    }
                }
                saida = auxpD[2] + "/" + auxpD[1] + "/" + auxpD[0];
            }
            if (auxp.Length > 1)
            {
                auxpH = auxp[1].Split(splitterH);
                if (auxpH.Length == 3)
                {
                    for (int i = 0; i <= 2; i++)
                    {
                        if (auxpH[i].Length == 1)
                        {
                            auxpH[i] = "0" + auxpH[i];
                        }
                    }
                    if (auxp.Length > 2 && Omda == 0)
                    {
                        int auxHPM = Convert.ToInt32(auxpH[0]);
                        if (auxp[2] == "PM")
                        {
                            if (auxHPM != 12 && auxHPM != 0)
                            {
                                auxHPM += 12;
                            }
                            auxpH[0] = auxHPM.ToString();
                        }
                        else
                        {
                            if (auxHPM == 12)
                            {
                                auxpH[0] = "00";
                            }
                        }
                    }
                    if (Omda == 0)
                    {
                        saida += " " + auxpH[0] + ":" + auxpH[1] + ":" + auxpH[2];
                    }
                }
                else
                {
                    throw new FormatException();
                }
            }
            if (saida == "01/01/1900 00:00:00")
            {
                saida = "";
            }
            return saida;
        }

        public string AdjustDate(string data)
        {
            if (data.Contains('/'))
            {
                return data;
            }
            else if (data.Contains('-'))
            {
                return data.Replace('-', '/');
            }
            else if (data.Contains('.'))
            {
                return data.Replace('.', '/');
            }
            else
            {
                return data;
            }
        }

        public string pMoneyToIMoney(string moneyp)
        {
            moneyp = moneyp.Replace(".","#");
            moneyp = moneyp.Replace("#",",");
            return moneyp;
        }        

        public bool ValidaTelefone(string telefone)
        {
            Regex rx = new Regex(@"(^(?:(?([0-9]{2}))?[-. ]?)?([0-9]{4})[-. ]?([0-9]{4})$)");
            return rx.IsMatch(telefone);
        }

        public bool ValidaCnpj(string cnpj)
        {
            Regex rx = new Regex(@"(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)");
            return rx.IsMatch(cnpj);
        }

        public bool ValidaNumeroComVirgula(string numero)
        {
            Regex rx = new Regex(@"^[+-]?[0-9]{1,3}(?:[0-9]*(?:[.,][0-9]{2})?|(?:,[0-9]{3})*(?:\.[0-9]{2})?|(?:\.[0-9]{3})*(?:,[0-9]{2})?)$");
            return rx.IsMatch(numero);
        }

        public bool ValidaNumeroComVirgulaInteiro(string numero)
        {
            Regex rx = new Regex(@"^[0-9]\d{0,9}(\.\d{1,3})?%?$");
            return rx.IsMatch(numero);
        }
        
        public bool ValidaLetrasNumeros(string texto)
        {
            Regex rx = new Regex("^[a-zA-ZÀ-Üà-ü0-9\\r\\n?!,. ]*$");
            return rx.IsMatch(texto);
        }

        public bool ValidaBola(string texto)
        {
            Regex rx = new Regex("^[°]*$");
            return rx.IsMatch(texto);
        }

        public bool ValidaNumerosInteiros(string texto)
        {
            Regex rx = new Regex(@"\d+");
            return rx.IsMatch(texto);
        }

        public decimal CalcularCambio(int IdMoedaDe, int IdMoedaPara)
        {
            try
            {
                CambioDAL cambDal = new CambioDAL();

                DAL.Entity.Cambio camb = cambDal.ObterPorIdMoedaPara(IdMoedaDe, IdMoedaPara);

                return Convert.ToDecimal(camb.Cambio1);
            }
            catch 
            {                
                throw;
            }
        }

        public DayOfWeek ReturnDay(string day)
        {
            switch (day)
            {
                case "Monday":
                    return DayOfWeek.Monday;
                case "Tuesday":
                    return DayOfWeek.Tuesday;
                case "Wednesday":
                    return DayOfWeek.Wednesday;
                case "Thursday":
                    return DayOfWeek.Thursday;
                case "Friday":
                    return DayOfWeek.Friday;
                case "Saturday":
                    return DayOfWeek.Saturday;
                case "Sunday":
                    return DayOfWeek.Sunday;
                default:
                    return DayOfWeek.Sunday;
            }
        }

        public DayOfWeek ReturnDayPT(string day)
        {
            switch (day)
            {
                case "Seg":
                    return DayOfWeek.Monday;
                case "Ter":
                    return DayOfWeek.Tuesday;
                case "Qua":
                    return DayOfWeek.Wednesday;
                case "Qui":
                    return DayOfWeek.Thursday;
                case "Sex":
                    return DayOfWeek.Friday;
                case "Sab":
                    return DayOfWeek.Saturday;
                case "Dom":
                    return DayOfWeek.Sunday;
                default:
                    return DayOfWeek.Sunday;
            }
        }

        public DayOfWeek ReturnDayNext(string day)
        {
            switch (day)
            {
                case "Seg":
                    return DayOfWeek.Tuesday;
                case "Ter":
                    return DayOfWeek.Wednesday;
                case "Qua":
                    return DayOfWeek.Thursday;
                case "Qui":
                    return DayOfWeek.Friday;
                case "Sex":
                    return DayOfWeek.Saturday;
                case "Sab":
                    return DayOfWeek.Sunday;
                case "Dom":
                    return DayOfWeek.Monday;
                default:
                    return DayOfWeek.Sunday;
            }
        }

        public string Retorna(string valor)
        {
            if (valor.Equals(""))
            {
                return null;
            }
            else
            {
                return valor;
            }
        }

    }
}