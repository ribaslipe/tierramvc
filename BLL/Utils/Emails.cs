﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace BLL.Utils
{
    public class Emails
    {

        private string Conta = ConfigurationManager.AppSettings["email"];
        private string Senha = ConfigurationManager.AppSettings["senhaemail"];

        private string Smtp = ConfigurationManager.AppSettings["smtp"];
        private int Porta = Convert.ToInt32(ConfigurationManager.AppSettings["porta"]);

        public void EnviarMensagem(string Dest, string Assunto, string Msg, string EmailUsuLogado, string EmailUserFile)
        {
            try
            {
                //Montar o Email
                MailMessage m = new MailMessage(Conta, Dest);

                //m.ReplyToList.Add(new System.Net.Mail.MailAddress(EmailUsuLogado, ""));
                //m.ReplyToList.Add(new System.Net.Mail.MailAddress(EmailUserFile, ""));

                MailAddress copy = new MailAddress(EmailUsuLogado, EmailUserFile);

                m.CC.Add(copy);
                
                m.Subject = Assunto;
                m.Body = Msg;
                m.IsBodyHtml = true;

                //Enviar o Email
                SmtpClient s = new SmtpClient(Smtp, Porta);

                s.UseDefaultCredentials = true;

                s.EnableSsl = true; //Enviar com criptografia                
                
                s.Credentials = new NetworkCredential(Conta, Senha);
                
                s.Send(m); //Enviar a Mensagem
            }
            catch
            {
                throw;
            }
        }

    }
}
